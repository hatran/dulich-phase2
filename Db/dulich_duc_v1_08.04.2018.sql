/*
Navicat MySQL Data Transfer

Source Server         : LOCALHOST
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : dulich

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-04-08 18:44:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for achievementfiles
-- ----------------------------
DROP TABLE IF EXISTS `achievementfiles`;
CREATE TABLE `achievementfiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `achievementfiles_memberid_foreign` (`memberId`),
  KEY `achievementfiles_fileid_foreign` (`fileId`),
  CONSTRAINT `achievementfiles_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `achievementfiles_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of achievementfiles
-- ----------------------------

-- ----------------------------
-- Table structure for branches
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `payment_info` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `note` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `option_code` varchar(10) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(10) NOT NULL,
  `images` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of branches
-- ----------------------------
INSERT INTO `branches` VALUES ('1', 'M001', 'Branches 01', 'HN', '948489696', 'laven9696@gmail.com', 'Info', 'Note', '22', 'BRANCHES01', null, '2018-03-22 21:56:18', '2018-03-23 09:25:57', '1', null);
INSERT INTO `branches` VALUES ('2', 'M002', 'Branches 02', 'HN', '0948489696', 'laven@gmai.com', 'Info', 'Note', '22', 'BRANCHES01', null, '2018-03-22 21:56:18', '2018-03-22 21:56:20', '1', null);
INSERT INTO `branches` VALUES ('3', 'M003', 'Branches 03', 'HN', '0948489696', 'laven@gmai.com', 'Info', 'Note', '22', 'BRANCHES01', null, '2018-03-22 21:56:18', '2018-03-22 21:56:20', '1', null);
INSERT INTO `branches` VALUES ('4', 'M004', 'Branches 04', 'HN', '0948489696', 'laven@gmai.com', 'Info', 'Note', '22', 'BRANCHES01', null, '2018-03-22 21:56:18', '2018-03-22 21:56:20', '1', null);
INSERT INTO `branches` VALUES ('17', 'ma001', 'Branches 05', '111111', '948489696', 'laven9696@gmail.com', null, null, '22', 'BRANCHES01', null, '2018-03-22 18:18:30', '2018-03-22 18:18:30', '1', null);
INSERT INTO `branches` VALUES ('18', 'ma0012', 'Branches 06', 'duy đức', '948489696', 'laven9696@gmail.com', null, null, '22', 'BRANCHES01', null, '2018-03-22 18:19:44', '2018-03-23 10:01:39', '1', null);
INSERT INTO `branches` VALUES ('19', 'ma0012111', 'Branches 07', 'duy đức', '948489696', 'laven9696@gmail.com', null, null, '22', 'BRANCHES01', null, '2018-03-22 18:19:49', '2018-03-23 10:00:52', '1', null);
INSERT INTO `branches` VALUES ('20', 'ma005', 'Clb 111', 'HN', '948489696', 'laven9696@gmail.com', null, null, '1', 'cl2211', null, '2018-03-31 09:09:52', '2018-03-31 18:22:53', '1', null);
INSERT INTO `branches` VALUES ('21', 'ma006666', 'Cau lac bo 2', '1111', '09263645333', 'laven9696@gmail.com', null, '22222', '1', 'BRANCHES02', null, '2018-04-01 11:10:13', '2018-04-01 12:09:40', '1', null);
INSERT INTO `branches` VALUES ('22', 'maha001', 'VP HN', null, null, null, null, null, null, null, null, null, null, '1', null);
INSERT INTO `branches` VALUES ('23', 'maDN', 'VP DN', null, null, null, null, null, null, null, null, null, null, '1', null);
INSERT INTO `branches` VALUES ('24', 'maHCM', 'VP HCM', null, null, null, null, null, null, null, null, null, null, '1', null);
INSERT INTO `branches` VALUES ('25', 'Ma0012222', 'Câu lạc bộ tiếng nhật', 'hn', '948489696', 'laven9696@gmail.com', null, null, '1', 'CLB01', null, '2018-04-01 16:07:26', '2018-04-01 16:07:26', '1', null);
INSERT INTO `branches` VALUES ('26', 'Ma222', 'duc2', 'hn2', '948489692', 'laven96962@gmail.com', null, null, '23', 'ch000', null, '2018-04-01 16:08:48', '2018-04-01 17:06:01', '1', null);
INSERT INTO `branches` VALUES ('27', 'wPhSDYnL', 'CLB 01', 'hn', '948489696', 'laven9696@gmail.com', null, null, '2', 'BRANCHES02', null, '2018-04-02 02:36:08', '2018-04-02 02:36:08', '1', null);
INSERT INTO `branches` VALUES ('28', 'yN7QSu4F', 'CLB 02', 'Hà Nội', '948489696', 'laven9696@gmail.com', null, 'Ghi chú', '2', 'BRANCHES02', null, '2018-04-02 02:37:45', '2018-04-02 02:37:45', '1', null);
INSERT INTO `branches` VALUES ('29', 'pmweCPUX', 'CLB 03', 'Địa chỉ', '0985556666', 'clb@gmail.com', null, 'Ghi chú jsss', '4', 'BRANCHES02', null, '2018-04-02 02:41:59', '2018-04-03 13:24:51', '1', null);
INSERT INTO `branches` VALUES ('30', 'NMjq8lbL', 'CLB 04', '112 ba đình', '0976665555', 'clb@gmail.com', null, '<script>alert(\"xxxx\")</script>', '2', 'BRANCHES02', null, '2018-04-02 02:42:54', '2018-04-03 13:05:02', '1', null);
INSERT INTO `branches` VALUES ('31', 'p1mP7kTi', 'Câu lạc bộ tiếng nga', 'Hà nội2', '0911222332', 'clb02@gmail.com', null, 'Ghi chú2', '1', 'CLB05', null, '2018-04-04 14:09:39', '2018-04-04 15:16:57', '1', null);

-- ----------------------------
-- Table structure for code
-- ----------------------------
DROP TABLE IF EXISTS `code`;
CREATE TABLE `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` char(10) DEFAULT NULL,
  `current_code` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of code
-- ----------------------------
INSERT INTO `code` VALUES ('1', 'C', '47', '2018-03-13 03:29:20', '2018-03-19 16:27:28');
INSERT INTO `code` VALUES ('2', '3', '5', '2018-03-13 07:37:31', '2018-03-26 14:55:31');
INSERT INTO `code` VALUES ('3', '1', '4', '2018-03-13 08:06:16', '2018-03-26 14:37:41');
INSERT INTO `code` VALUES ('4', '2', '23', '2018-03-13 08:06:47', '2018-03-22 16:09:46');

-- ----------------------------
-- Table structure for educationbranches
-- ----------------------------
DROP TABLE IF EXISTS `educationbranches`;
CREATE TABLE `educationbranches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branchName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of educationbranches
-- ----------------------------
INSERT INTO `educationbranches` VALUES ('1', 'Du lịch', '2017-12-01 00:08:35', '2017-12-01 00:08:35', '1');
INSERT INTO `educationbranches` VALUES ('2', 'Kinh tế/Khoa học xã hội', '2017-12-01 00:08:35', '2017-12-01 00:08:35', '1');
INSERT INTO `educationbranches` VALUES ('3', 'Kỹ thuật/Khoa học tự nhiên', '2017-12-30 00:00:00', '2017-12-30 00:00:00', '1');

-- ----------------------------
-- Table structure for educationdegrees
-- ----------------------------
DROP TABLE IF EXISTS `educationdegrees`;
CREATE TABLE `educationdegrees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `degree` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of educationdegrees
-- ----------------------------
INSERT INTO `educationdegrees` VALUES ('1', 'Trung cấp sửa', '2017-12-01 00:08:35', '2017-12-01 00:08:35', '0');
INSERT INTO `educationdegrees` VALUES ('2', 'Cao đẳng', '2017-12-01 00:08:35', '2017-12-01 00:08:35', '1');
INSERT INTO `educationdegrees` VALUES ('3', 'Đại học', '2017-12-01 09:26:24', '2017-12-30 10:27:30', '1');
INSERT INTO `educationdegrees` VALUES ('4', 'Trên Đại học', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for educations
-- ----------------------------
DROP TABLE IF EXISTS `educations`;
CREATE TABLE `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `branchId` int(10) unsigned NOT NULL DEFAULT '1',
  `degreeId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `educations_memberid_foreign` (`memberId`),
  KEY `educations_branchid_foreign` (`branchId`),
  KEY `educations_degreeid_foreign` (`degreeId`),
  KEY `educations_fileid_foreign` (`fileId`),
  CONSTRAINT `educations_branchid_foreign` FOREIGN KEY (`branchId`) REFERENCES `educationbranches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `educations_degreeid_foreign` FOREIGN KEY (`degreeId`) REFERENCES `educationdegrees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `educations_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `educations_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of educations
-- ----------------------------
INSERT INTO `educations` VALUES ('1', '1', '3', '4', null, '2018-03-13 09:53:09', '2018-03-16 17:10:07');
INSERT INTO `educations` VALUES ('2', '2', '3', '4', null, '2018-03-13 09:59:56', '2018-03-13 09:59:56');
INSERT INTO `educations` VALUES ('3', '3', '1', '4', null, '2018-03-13 10:05:04', '2018-03-13 10:05:04');
INSERT INTO `educations` VALUES ('4', '4', '3', '3', null, '2018-03-13 10:10:03', '2018-03-13 10:10:03');
INSERT INTO `educations` VALUES ('5', '5', '2', '4', null, '2018-03-13 10:11:17', '2018-03-13 10:11:17');
INSERT INTO `educations` VALUES ('6', '6', '2', '4', null, '2018-03-13 10:11:40', '2018-03-13 10:11:40');
INSERT INTO `educations` VALUES ('7', '7', '3', '4', null, '2018-03-13 10:14:55', '2018-03-13 10:14:55');
INSERT INTO `educations` VALUES ('8', '8', '1', '2', null, '2018-03-13 10:35:52', '2018-03-13 10:35:52');
INSERT INTO `educations` VALUES ('9', '9', '2', '4', null, '2018-03-13 11:42:24', '2018-03-13 11:42:24');
INSERT INTO `educations` VALUES ('10', '10', '2', '4', null, '2018-03-13 13:57:24', '2018-03-13 13:57:24');
INSERT INTO `educations` VALUES ('11', '11', '2', '2', null, '2018-03-13 14:00:48', '2018-03-13 14:00:48');
INSERT INTO `educations` VALUES ('12', '12', '2', '2', null, '2018-03-13 14:13:06', '2018-03-13 14:13:06');
INSERT INTO `educations` VALUES ('13', '13', '2', '4', null, '2018-03-13 14:16:53', '2018-03-13 14:16:53');
INSERT INTO `educations` VALUES ('14', '14', '2', '3', null, '2018-03-13 14:20:27', '2018-03-13 14:20:27');
INSERT INTO `educations` VALUES ('15', '15', '2', '3', null, '2018-03-13 14:22:15', '2018-03-13 14:22:15');
INSERT INTO `educations` VALUES ('16', '16', '2', '2', null, '2018-03-13 22:29:33', '2018-03-16 15:39:20');
INSERT INTO `educations` VALUES ('17', '17', '1', '3', null, '2018-03-14 09:37:17', '2018-03-19 08:50:33');
INSERT INTO `educations` VALUES ('18', '18', '2', '2', null, '2018-03-14 09:42:54', '2018-03-14 09:42:54');
INSERT INTO `educations` VALUES ('19', '19', '2', '2', null, '2018-03-14 10:31:48', '2018-03-14 10:31:48');
INSERT INTO `educations` VALUES ('20', '20', '2', '2', null, '2018-03-14 10:34:11', '2018-03-14 10:34:11');
INSERT INTO `educations` VALUES ('21', '21', '2', '3', null, '2018-03-14 10:37:23', '2018-03-14 10:37:23');
INSERT INTO `educations` VALUES ('22', '22', '2', '3', null, '2018-03-14 10:40:40', '2018-03-14 10:40:40');
INSERT INTO `educations` VALUES ('23', '23', '2', '2', null, '2018-03-14 10:58:44', '2018-03-14 10:58:44');
INSERT INTO `educations` VALUES ('24', '24', '2', '3', null, '2018-03-14 11:01:34', '2018-03-14 11:01:34');
INSERT INTO `educations` VALUES ('25', '25', '2', '2', null, '2018-03-14 11:03:19', '2018-03-14 11:03:19');
INSERT INTO `educations` VALUES ('26', '26', '2', '3', null, '2018-03-14 11:04:03', '2018-03-14 11:04:03');
INSERT INTO `educations` VALUES ('27', '27', '2', '2', null, '2018-03-14 11:05:08', '2018-03-14 11:05:08');
INSERT INTO `educations` VALUES ('28', '28', '2', '2', null, '2018-03-14 11:08:33', '2018-03-14 11:08:33');
INSERT INTO `educations` VALUES ('29', '29', '2', '3', null, '2018-03-14 11:10:14', '2018-03-14 11:10:14');
INSERT INTO `educations` VALUES ('30', '30', '3', '3', null, '2018-03-14 11:11:30', '2018-03-14 11:11:30');
INSERT INTO `educations` VALUES ('31', '31', '2', '4', null, '2018-03-14 11:12:00', '2018-03-16 16:28:16');
INSERT INTO `educations` VALUES ('32', '32', '2', '3', null, '2018-03-14 11:15:27', '2018-03-14 11:15:27');
INSERT INTO `educations` VALUES ('33', '33', '3', '3', null, '2018-03-14 11:17:53', '2018-03-14 11:17:53');
INSERT INTO `educations` VALUES ('34', '34', '2', '2', null, '2018-03-14 11:20:04', '2018-03-14 11:20:04');
INSERT INTO `educations` VALUES ('35', '35', '1', '3', null, '2018-03-14 11:22:42', '2018-03-14 11:22:42');
INSERT INTO `educations` VALUES ('36', '36', '1', '2', null, '2018-03-14 11:25:18', '2018-03-14 11:25:18');
INSERT INTO `educations` VALUES ('37', '37', '1', '2', null, '2018-03-14 17:29:23', '2018-03-14 17:29:23');
INSERT INTO `educations` VALUES ('38', '38', '2', '2', null, '2018-03-14 17:30:16', '2018-03-14 17:30:16');
INSERT INTO `educations` VALUES ('39', '38', '2', '2', null, '2018-03-14 17:31:14', '2018-03-14 17:31:14');
INSERT INTO `educations` VALUES ('40', '39', '3', '4', null, '2018-03-15 10:53:49', '2018-03-15 10:53:49');
INSERT INTO `educations` VALUES ('41', '40', '2', '2', null, '2018-03-15 11:00:38', '2018-03-15 11:00:38');
INSERT INTO `educations` VALUES ('42', '40', '2', '2', null, '2018-03-15 11:01:06', '2018-03-15 11:01:06');
INSERT INTO `educations` VALUES ('43', '41', '1', '2', null, '2018-03-15 11:29:04', '2018-03-15 11:29:04');
INSERT INTO `educations` VALUES ('44', '42', '1', '2', null, '2018-03-15 11:29:59', '2018-03-15 11:29:59');
INSERT INTO `educations` VALUES ('45', '43', '2', '2', null, '2018-03-15 11:31:40', '2018-03-15 11:31:40');
INSERT INTO `educations` VALUES ('46', '41', '1', '2', null, '2018-03-15 11:32:31', '2018-03-15 11:32:31');
INSERT INTO `educations` VALUES ('47', '44', '2', '2', null, '2018-03-15 11:36:10', '2018-03-15 11:36:10');
INSERT INTO `educations` VALUES ('48', '45', '2', '4', null, '2018-03-15 11:38:59', '2018-03-15 11:38:59');
INSERT INTO `educations` VALUES ('49', '46', '2', '3', null, '2018-03-15 11:39:11', '2018-03-15 11:39:11');
INSERT INTO `educations` VALUES ('50', '47', '2', '4', null, '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `educations` VALUES ('51', '48', '3', '3', null, '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `educations` VALUES ('52', '49', '1', '2', null, '2018-03-15 13:38:43', '2018-03-15 13:38:43');
INSERT INTO `educations` VALUES ('53', '50', '2', '3', null, '2018-03-15 13:44:37', '2018-03-15 13:44:37');
INSERT INTO `educations` VALUES ('54', '51', '1', '4', null, '2018-03-15 13:53:07', '2018-03-15 13:53:07');
INSERT INTO `educations` VALUES ('55', '51', '1', '4', null, '2018-03-15 13:54:19', '2018-03-15 13:54:19');
INSERT INTO `educations` VALUES ('56', '52', '1', '2', null, '2018-03-15 14:30:20', '2018-03-15 14:30:20');
INSERT INTO `educations` VALUES ('57', '53', '2', '4', null, '2018-03-15 14:33:57', '2018-03-16 11:31:30');
INSERT INTO `educations` VALUES ('58', '54', '3', '4', null, '2018-03-15 14:38:02', '2018-03-15 14:38:02');
INSERT INTO `educations` VALUES ('59', '55', '1', '3', null, '2018-03-15 16:18:01', '2018-03-15 16:18:01');
INSERT INTO `educations` VALUES ('60', '55', '1', '3', null, '2018-03-15 16:22:22', '2018-03-15 16:22:22');
INSERT INTO `educations` VALUES ('61', '56', '1', '2', null, '2018-03-15 16:27:16', '2018-03-15 16:27:16');
INSERT INTO `educations` VALUES ('62', '57', '2', '2', null, '2018-03-15 16:27:39', '2018-03-15 16:27:39');
INSERT INTO `educations` VALUES ('63', '58', '1', '2', null, '2018-03-15 16:28:35', '2018-03-15 16:28:35');
INSERT INTO `educations` VALUES ('64', '58', '1', '2', null, '2018-03-15 16:30:48', '2018-03-15 16:30:48');
INSERT INTO `educations` VALUES ('65', '58', '1', '2', null, '2018-03-15 16:31:06', '2018-03-15 16:31:06');
INSERT INTO `educations` VALUES ('66', '58', '1', '2', null, '2018-03-15 16:31:29', '2018-03-15 16:31:29');
INSERT INTO `educations` VALUES ('67', '59', '1', '2', null, '2018-03-16 09:29:02', '2018-03-16 09:29:02');
INSERT INTO `educations` VALUES ('68', '60', '2', '3', null, '2018-03-16 09:36:06', '2018-03-16 09:36:06');
INSERT INTO `educations` VALUES ('69', '61', '1', '2', null, '2018-03-16 11:00:54', '2018-03-16 11:00:54');
INSERT INTO `educations` VALUES ('70', '62', '1', '2', null, '2018-03-16 11:09:11', '2018-03-16 11:09:11');
INSERT INTO `educations` VALUES ('71', '63', '1', '2', null, '2018-03-16 11:34:15', '2018-03-16 11:34:15');
INSERT INTO `educations` VALUES ('72', '64', '3', '4', null, '2018-03-16 11:34:40', '2018-03-16 11:34:40');
INSERT INTO `educations` VALUES ('73', '65', '2', '2', null, '2018-03-16 13:32:45', '2018-03-16 13:32:45');
INSERT INTO `educations` VALUES ('74', '66', '2', '2', null, '2018-03-16 13:33:40', '2018-03-16 13:33:40');
INSERT INTO `educations` VALUES ('75', '67', '2', '2', null, '2018-03-16 13:34:19', '2018-03-16 13:34:19');
INSERT INTO `educations` VALUES ('76', '68', '2', '2', null, '2018-03-16 13:35:12', '2018-03-16 13:35:12');
INSERT INTO `educations` VALUES ('77', '69', '2', '2', null, '2018-03-16 13:39:48', '2018-03-16 13:39:48');
INSERT INTO `educations` VALUES ('78', '70', '2', '2', null, '2018-03-16 13:39:49', '2018-03-16 15:37:33');
INSERT INTO `educations` VALUES ('79', '71', '2', '2', null, '2018-03-16 13:44:56', '2018-03-16 13:44:56');
INSERT INTO `educations` VALUES ('80', '71', '2', '2', null, '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `educations` VALUES ('81', '72', '2', '2', null, '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `educations` VALUES ('82', '71', '2', '2', null, '2018-03-16 13:46:39', '2018-03-16 13:46:39');
INSERT INTO `educations` VALUES ('83', '71', '2', '2', null, '2018-03-16 13:47:45', '2018-03-16 13:47:45');
INSERT INTO `educations` VALUES ('84', '71', '2', '2', null, '2018-03-16 13:48:09', '2018-03-16 13:48:09');
INSERT INTO `educations` VALUES ('85', '73', '2', '2', null, '2018-03-16 14:25:47', '2018-03-16 14:25:47');
INSERT INTO `educations` VALUES ('86', '73', '2', '2', null, '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `educations` VALUES ('87', '73', '2', '2', null, '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `educations` VALUES ('88', '73', '2', '2', null, '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `educations` VALUES ('89', '73', '2', '2', null, '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `educations` VALUES ('90', '73', '2', '2', null, '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `educations` VALUES ('91', '73', '2', '2', null, '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `educations` VALUES ('92', '73', '2', '2', null, '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `educations` VALUES ('93', '73', '2', '2', null, '2018-03-16 14:26:41', '2018-03-16 14:26:41');
INSERT INTO `educations` VALUES ('94', '73', '2', '2', null, '2018-03-16 14:27:04', '2018-03-16 14:27:04');
INSERT INTO `educations` VALUES ('95', '72', '2', '2', null, '2018-03-16 14:28:15', '2018-03-16 14:28:15');
INSERT INTO `educations` VALUES ('96', '74', '2', '2', null, '2018-03-16 14:56:22', '2018-03-16 14:56:22');
INSERT INTO `educations` VALUES ('97', '75', '2', '2', null, '2018-03-16 15:01:23', '2018-03-16 15:01:23');
INSERT INTO `educations` VALUES ('98', '75', '2', '2', null, '2018-03-16 15:01:48', '2018-03-16 15:01:48');
INSERT INTO `educations` VALUES ('99', '76', '2', '2', null, '2018-03-16 15:18:48', '2018-03-16 15:18:48');
INSERT INTO `educations` VALUES ('100', '77', '2', '2', null, '2018-03-16 15:19:23', '2018-03-16 15:19:23');
INSERT INTO `educations` VALUES ('101', '78', '2', '2', null, '2018-03-16 15:24:01', '2018-03-16 15:24:01');
INSERT INTO `educations` VALUES ('102', '79', '1', '2', null, '2018-03-16 16:09:19', '2018-03-16 17:06:28');
INSERT INTO `educations` VALUES ('103', '80', '2', '2', null, '2018-03-16 17:37:11', '2018-03-16 17:37:11');
INSERT INTO `educations` VALUES ('104', '81', '3', '4', null, '2018-03-19 11:37:54', '2018-03-19 11:37:54');
INSERT INTO `educations` VALUES ('105', '81', '3', '4', null, '2018-03-19 11:38:39', '2018-03-19 11:38:39');
INSERT INTO `educations` VALUES ('106', '82', '3', '4', null, '2018-03-19 11:44:32', '2018-03-19 11:44:32');
INSERT INTO `educations` VALUES ('107', '83', '3', '4', null, '2018-03-19 15:42:18', '2018-03-19 15:59:51');

-- ----------------------------
-- Table structure for elements
-- ----------------------------
DROP TABLE IF EXISTS `elements`;
CREATE TABLE `elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `typeGuideId` int(10) unsigned NOT NULL,
  `groupSizeId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `elements_memberid_foreign` (`memberId`),
  KEY `elements_typeguideid_foreign` (`typeGuideId`),
  KEY `elements_groupsizeid_foreign` (`groupSizeId`),
  CONSTRAINT `elements_groupsizeid_foreign` FOREIGN KEY (`groupSizeId`) REFERENCES `groupsizes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `elements_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `elements_typeguideid_foreign` FOREIGN KEY (`typeGuideId`) REFERENCES `typeguides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of elements
-- ----------------------------
INSERT INTO `elements` VALUES ('1', '1', '7', '5', '2018-03-13 09:53:09', '2018-03-16 17:10:07');
INSERT INTO `elements` VALUES ('2', '2', '7', '5', '2018-03-13 09:59:56', '2018-03-13 09:59:56');
INSERT INTO `elements` VALUES ('3', '3', '3', '5', '2018-03-13 10:05:04', '2018-03-13 10:05:04');
INSERT INTO `elements` VALUES ('4', '4', '5', '4', '2018-03-13 10:10:03', '2018-03-13 10:10:03');
INSERT INTO `elements` VALUES ('5', '5', '7', '5', '2018-03-13 10:11:17', '2018-03-13 10:11:17');
INSERT INTO `elements` VALUES ('6', '6', '6', '2', '2018-03-13 10:11:40', '2018-03-13 10:11:40');
INSERT INTO `elements` VALUES ('7', '7', '7', '4', '2018-03-13 10:14:55', '2018-03-13 10:14:55');
INSERT INTO `elements` VALUES ('8', '8', '8', '2', '2018-03-13 10:35:52', '2018-03-13 10:35:52');
INSERT INTO `elements` VALUES ('9', '9', '7', '3', '2018-03-13 11:42:24', '2018-03-13 11:42:24');
INSERT INTO `elements` VALUES ('10', '10', '7', '4', '2018-03-13 13:57:24', '2018-03-13 13:57:24');
INSERT INTO `elements` VALUES ('11', '11', '4', '2', '2018-03-13 14:00:48', '2018-03-13 14:00:48');
INSERT INTO `elements` VALUES ('12', '12', '6', '3', '2018-03-13 14:13:06', '2018-03-13 14:13:06');
INSERT INTO `elements` VALUES ('13', '13', '7', '4', '2018-03-13 14:16:53', '2018-03-13 14:16:53');
INSERT INTO `elements` VALUES ('14', '14', '6', '3', '2018-03-13 14:20:27', '2018-03-13 14:20:27');
INSERT INTO `elements` VALUES ('15', '15', '6', '4', '2018-03-13 14:22:15', '2018-03-13 14:22:15');
INSERT INTO `elements` VALUES ('16', '16', '3', '4', '2018-03-13 22:29:33', '2018-03-16 15:39:20');
INSERT INTO `elements` VALUES ('17', '17', '2', '4', '2018-03-14 09:37:17', '2018-03-19 08:50:33');
INSERT INTO `elements` VALUES ('18', '18', '7', '2', '2018-03-14 09:42:54', '2018-03-14 09:42:54');
INSERT INTO `elements` VALUES ('19', '19', '3', '2', '2018-03-14 10:31:48', '2018-03-14 10:31:48');
INSERT INTO `elements` VALUES ('20', '20', '7', '2', '2018-03-14 10:34:11', '2018-03-14 10:34:11');
INSERT INTO `elements` VALUES ('21', '21', '7', '2', '2018-03-14 10:37:23', '2018-03-14 10:37:23');
INSERT INTO `elements` VALUES ('22', '22', '5', '4', '2018-03-14 10:40:40', '2018-03-14 10:40:40');
INSERT INTO `elements` VALUES ('23', '23', '2', '2', '2018-03-14 10:58:44', '2018-03-14 10:58:44');
INSERT INTO `elements` VALUES ('24', '24', '2', '4', '2018-03-14 11:01:34', '2018-03-14 11:01:34');
INSERT INTO `elements` VALUES ('25', '25', '7', '2', '2018-03-14 11:03:19', '2018-03-14 11:03:19');
INSERT INTO `elements` VALUES ('26', '26', '6', '5', '2018-03-14 11:04:03', '2018-03-14 11:04:03');
INSERT INTO `elements` VALUES ('27', '27', '7', '2', '2018-03-14 11:05:08', '2018-03-14 11:05:08');
INSERT INTO `elements` VALUES ('28', '28', '3', '3', '2018-03-14 11:08:33', '2018-03-14 11:08:33');
INSERT INTO `elements` VALUES ('29', '29', '2', '2', '2018-03-14 11:10:14', '2018-03-14 11:10:14');
INSERT INTO `elements` VALUES ('30', '30', '6', '5', '2018-03-14 11:11:30', '2018-03-14 11:11:30');
INSERT INTO `elements` VALUES ('31', '31', '8', '5', '2018-03-14 11:12:00', '2018-03-16 16:28:16');
INSERT INTO `elements` VALUES ('32', '32', '6', '5', '2018-03-14 11:15:27', '2018-03-14 11:15:27');
INSERT INTO `elements` VALUES ('33', '33', '6', '5', '2018-03-14 11:17:53', '2018-03-14 11:17:53');
INSERT INTO `elements` VALUES ('34', '34', '5', '2', '2018-03-14 11:20:04', '2018-03-14 11:20:04');
INSERT INTO `elements` VALUES ('35', '35', '5', '2', '2018-03-14 11:22:42', '2018-03-14 11:22:42');
INSERT INTO `elements` VALUES ('36', '36', '5', '4', '2018-03-14 11:25:18', '2018-03-14 11:25:18');
INSERT INTO `elements` VALUES ('37', '37', '5', '2', '2018-03-14 17:29:23', '2018-03-14 17:29:23');
INSERT INTO `elements` VALUES ('38', '38', '2', '3', '2018-03-14 17:30:16', '2018-03-14 17:30:16');
INSERT INTO `elements` VALUES ('39', '38', '2', '3', '2018-03-14 17:31:14', '2018-03-14 17:31:14');
INSERT INTO `elements` VALUES ('40', '39', '4', '4', '2018-03-15 10:53:49', '2018-03-15 10:53:49');
INSERT INTO `elements` VALUES ('41', '40', '5', '3', '2018-03-15 11:00:38', '2018-03-15 11:00:38');
INSERT INTO `elements` VALUES ('42', '40', '5', '3', '2018-03-15 11:01:06', '2018-03-15 11:01:06');
INSERT INTO `elements` VALUES ('43', '41', '7', '4', '2018-03-15 11:29:04', '2018-03-15 11:29:04');
INSERT INTO `elements` VALUES ('44', '42', '8', '5', '2018-03-15 11:29:59', '2018-03-15 11:29:59');
INSERT INTO `elements` VALUES ('45', '43', '2', '3', '2018-03-15 11:31:40', '2018-03-15 11:31:40');
INSERT INTO `elements` VALUES ('46', '41', '7', '4', '2018-03-15 11:32:31', '2018-03-15 11:32:31');
INSERT INTO `elements` VALUES ('47', '44', '7', '3', '2018-03-15 11:36:10', '2018-03-15 11:36:10');
INSERT INTO `elements` VALUES ('48', '45', '2', '2', '2018-03-15 11:38:59', '2018-03-15 11:38:59');
INSERT INTO `elements` VALUES ('49', '46', '2', '2', '2018-03-15 11:39:11', '2018-03-15 11:39:11');
INSERT INTO `elements` VALUES ('50', '47', '3', '4', '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `elements` VALUES ('51', '48', '5', '2', '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `elements` VALUES ('52', '49', '6', '3', '2018-03-15 13:38:43', '2018-03-15 13:38:43');
INSERT INTO `elements` VALUES ('53', '50', '3', '5', '2018-03-15 13:44:37', '2018-03-15 13:44:37');
INSERT INTO `elements` VALUES ('54', '51', '7', '5', '2018-03-15 13:53:07', '2018-03-15 13:53:07');
INSERT INTO `elements` VALUES ('55', '51', '7', '5', '2018-03-15 13:54:19', '2018-03-15 13:54:19');
INSERT INTO `elements` VALUES ('56', '52', '7', '3', '2018-03-15 14:30:20', '2018-03-15 14:30:20');
INSERT INTO `elements` VALUES ('57', '53', '2', '5', '2018-03-15 14:33:57', '2018-03-16 11:31:30');
INSERT INTO `elements` VALUES ('58', '54', '8', '3', '2018-03-15 14:38:02', '2018-03-15 14:38:02');
INSERT INTO `elements` VALUES ('59', '55', '5', '4', '2018-03-15 16:18:01', '2018-03-15 16:18:01');
INSERT INTO `elements` VALUES ('60', '55', '5', '4', '2018-03-15 16:22:22', '2018-03-15 16:22:22');
INSERT INTO `elements` VALUES ('61', '56', '5', '3', '2018-03-15 16:27:16', '2018-03-15 16:27:16');
INSERT INTO `elements` VALUES ('62', '57', '2', '2', '2018-03-15 16:27:39', '2018-03-15 16:27:39');
INSERT INTO `elements` VALUES ('63', '58', '5', '3', '2018-03-15 16:28:35', '2018-03-15 16:28:35');
INSERT INTO `elements` VALUES ('64', '58', '5', '3', '2018-03-15 16:30:48', '2018-03-15 16:30:48');
INSERT INTO `elements` VALUES ('65', '58', '5', '3', '2018-03-15 16:31:06', '2018-03-15 16:31:06');
INSERT INTO `elements` VALUES ('66', '58', '5', '3', '2018-03-15 16:31:29', '2018-03-15 16:31:29');
INSERT INTO `elements` VALUES ('67', '59', '7', '4', '2018-03-16 09:29:02', '2018-03-16 09:29:02');
INSERT INTO `elements` VALUES ('68', '60', '2', '3', '2018-03-16 09:36:06', '2018-03-16 09:36:06');
INSERT INTO `elements` VALUES ('69', '61', '7', '4', '2018-03-16 11:00:54', '2018-03-16 11:00:54');
INSERT INTO `elements` VALUES ('70', '62', '7', '4', '2018-03-16 11:09:11', '2018-03-16 11:09:11');
INSERT INTO `elements` VALUES ('71', '63', '7', '4', '2018-03-16 11:34:15', '2018-03-16 11:34:15');
INSERT INTO `elements` VALUES ('72', '64', '5', '4', '2018-03-16 11:34:40', '2018-03-16 11:34:40');
INSERT INTO `elements` VALUES ('73', '65', '6', '2', '2018-03-16 13:32:45', '2018-03-16 13:32:45');
INSERT INTO `elements` VALUES ('74', '66', '6', '2', '2018-03-16 13:33:40', '2018-03-16 13:33:40');
INSERT INTO `elements` VALUES ('75', '67', '6', '2', '2018-03-16 13:34:19', '2018-03-16 13:34:19');
INSERT INTO `elements` VALUES ('76', '68', '6', '2', '2018-03-16 13:35:12', '2018-03-16 13:35:12');
INSERT INTO `elements` VALUES ('77', '69', '2', '2', '2018-03-16 13:39:48', '2018-03-16 13:39:48');
INSERT INTO `elements` VALUES ('78', '70', '6', '2', '2018-03-16 13:39:49', '2018-03-16 15:37:33');
INSERT INTO `elements` VALUES ('79', '71', '4', '5', '2018-03-16 13:44:56', '2018-03-16 13:44:56');
INSERT INTO `elements` VALUES ('80', '71', '4', '5', '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `elements` VALUES ('81', '72', '6', '2', '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `elements` VALUES ('82', '71', '4', '5', '2018-03-16 13:46:39', '2018-03-16 13:46:39');
INSERT INTO `elements` VALUES ('83', '71', '4', '5', '2018-03-16 13:47:45', '2018-03-16 13:47:45');
INSERT INTO `elements` VALUES ('84', '71', '4', '5', '2018-03-16 13:48:09', '2018-03-16 13:48:09');
INSERT INTO `elements` VALUES ('85', '73', '6', '5', '2018-03-16 14:25:47', '2018-03-16 14:25:47');
INSERT INTO `elements` VALUES ('86', '73', '6', '5', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `elements` VALUES ('87', '73', '6', '5', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `elements` VALUES ('88', '73', '6', '5', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `elements` VALUES ('89', '73', '6', '5', '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `elements` VALUES ('90', '73', '6', '5', '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `elements` VALUES ('91', '73', '6', '5', '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `elements` VALUES ('92', '73', '6', '5', '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `elements` VALUES ('93', '73', '6', '5', '2018-03-16 14:26:41', '2018-03-16 14:26:41');
INSERT INTO `elements` VALUES ('94', '73', '6', '5', '2018-03-16 14:27:04', '2018-03-16 14:27:04');
INSERT INTO `elements` VALUES ('95', '72', '6', '2', '2018-03-16 14:28:15', '2018-03-16 14:28:15');
INSERT INTO `elements` VALUES ('96', '74', '6', '2', '2018-03-16 14:56:22', '2018-03-16 14:56:22');
INSERT INTO `elements` VALUES ('97', '75', '6', '2', '2018-03-16 15:01:23', '2018-03-16 15:01:23');
INSERT INTO `elements` VALUES ('98', '75', '6', '2', '2018-03-16 15:01:48', '2018-03-16 15:01:48');
INSERT INTO `elements` VALUES ('99', '76', '6', '2', '2018-03-16 15:18:48', '2018-03-16 15:18:48');
INSERT INTO `elements` VALUES ('100', '77', '6', '2', '2018-03-16 15:19:24', '2018-03-16 15:19:24');
INSERT INTO `elements` VALUES ('101', '78', '6', '2', '2018-03-16 15:24:01', '2018-03-16 15:24:01');
INSERT INTO `elements` VALUES ('102', '79', '8', '5', '2018-03-16 16:09:19', '2018-03-16 17:06:28');
INSERT INTO `elements` VALUES ('103', '80', '2', '2', '2018-03-16 17:37:11', '2018-03-16 17:37:11');
INSERT INTO `elements` VALUES ('104', '81', '7', '5', '2018-03-19 11:37:54', '2018-03-19 11:37:54');
INSERT INTO `elements` VALUES ('105', '81', '7', '5', '2018-03-19 11:38:39', '2018-03-19 11:38:39');
INSERT INTO `elements` VALUES ('106', '82', '7', '5', '2018-03-19 11:44:32', '2018-03-19 11:44:32');
INSERT INTO `elements` VALUES ('107', '83', '5', '2', '2018-03-19 15:42:18', '2018-03-19 15:59:51');

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `option_code` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employees
-- ----------------------------
INSERT INTO `employees` VALUES ('1', 'Nguyễn Văn A', 'https://media.forgecdn.net/avatars/124/768/636424778749237239.jpeg', '1990-10-26 19:43:44', '1', '1', 'laven9696@gmail.com', '098765421', 'HN', null, '2018-03-29 19:45:33', '2018-03-29 19:45:39', 'Company A');
INSERT INTO `employees` VALUES ('2', 'Nguyễn Văn B', 'https://media.forgecdn.net/avatars/124/768/636424778749237239.jpeg', '1990-10-26 19:43:44', '1', '1', 'laven9695@gmail.com', '098765422', 'HN', null, '2018-03-29 19:45:33', '2018-03-29 19:45:39', 'Company A');
INSERT INTO `employees` VALUES ('3', 'Hà Trung Dũng', 'https://media.forgecdn.net/avatars/124/768/636424778749237239.jpeg', '1990-10-26 19:43:44', '1', '1', 'dung@gmail.com', '098765423', 'HN', null, '2018-03-29 19:45:33', '2018-03-29 19:45:39', 'Company A');
INSERT INTO `employees` VALUES ('6', 'Nguyễn Anh Tuấn', 'https://media.forgecdn.net/avatars/124/768/636424778749237239.jpeg', '1990-10-26 19:43:44', '1', '1', 'anhtuan@gmail.com', '098765424', 'HN', null, '2018-03-29 19:45:33', '2018-03-29 19:45:39', 'Company A');
INSERT INTO `employees` VALUES ('7', 'do duy duc', null, '1990-10-26 00:00:00', null, '4', 'laven9696@gmail.com', '0948489696', null, '2018-03-31 13:43:11', '2018-03-31 12:53:33', '2018-03-31 13:43:11', null);
INSERT INTO `employees` VALUES ('8', 'do duy duc12', null, '1990-11-26 00:00:00', null, '4', '122223333', '0948489695', null, '2018-03-31 13:42:41', '2018-03-31 13:25:17', '2018-03-31 13:42:41', null);
INSERT INTO `employees` VALUES ('9', '11111', null, '2018-03-31 11:11:00', null, '4', 'laven9696@gmail.com', '2222', null, null, '2018-03-31 14:18:42', '2018-03-31 14:18:42', null);
INSERT INTO `employees` VALUES ('10', 'do duy duc', null, '2018-03-31 11:11:00', null, '5', 'laven9696@gmail.com', '11111', null, null, '2018-03-31 14:27:25', '2018-03-31 14:27:25', null);
INSERT INTO `employees` VALUES ('11', 'thu ha', null, '1990-10-26 00:00:00', null, '5', 'a@gmail.com', '0948489595', null, null, '2018-03-31 15:12:07', '2018-03-31 15:12:07', null);
INSERT INTO `employees` VALUES ('12', 'do duy duc', null, '1990-10-26 00:00:00', null, '4', 'laven9696@gmail.com', '0948499696', null, null, '2018-03-31 16:33:15', '2018-03-31 16:33:15', null);
INSERT INTO `employees` VALUES ('13', 'do duy duc 2222', null, '1990-10-02 00:00:00', null, '4', 'lavne9696@gmail.com', '0932424111', null, null, '2018-04-01 13:19:54', '2018-04-01 13:19:54', null);
INSERT INTO `employees` VALUES ('14', 'do duy duc', null, '1990-10-26 00:00:00', null, 'cdddd', 'laven9696@gmail.com', '9884736322', null, null, '2018-04-01 16:33:24', '2018-04-01 16:33:24', null);
INSERT INTO `employees` VALUES ('15', 'User 01', null, '1990-01-01 00:00:00', null, 'cdddd', 'user01@gmail.com', '0912223333', null, null, '2018-04-02 02:48:37', '2018-04-02 02:48:37', null);
INSERT INTO `employees` VALUES ('16', 'duc', null, '1990-10-26 00:00:00', null, 'ddg33', 'la@co.cc', '0948489696', null, null, '2018-04-02 04:48:02', '2018-04-02 04:48:02', null);
INSERT INTO `employees` VALUES ('17', 'do duy', null, '1970-01-01 00:00:00', null, 'cdddd', 'laven96@gmail.com', '2222', null, null, '2018-04-02 04:48:26', '2018-04-02 04:48:26', null);
INSERT INTO `employees` VALUES ('18', 'user 01', null, '1990-10-26 00:00:00', null, 'cdddd', 'laven9696@gmail.com', '0975847333', null, null, '2018-04-02 15:32:20', '2018-04-02 15:32:20', null);
INSERT INTO `employees` VALUES ('19', 'user 02', null, '1990-10-25 00:00:00', null, 'cdddd', 'laven9696@gmail.com', '0968643332', null, null, '2018-04-02 15:34:50', '2018-04-02 15:34:50', null);
INSERT INTO `employees` VALUES ('20', 'nguyen kien', null, '1990-10-10 00:00:00', null, 'cdddd', 'ca.ccc@c.co', '0948489696', null, null, '2018-04-02 15:45:00', '2018-04-02 15:45:00', null);
INSERT INTO `employees` VALUES ('21', 'duyd', null, '1990-10-24 00:00:00', null, 'ddg33', 'lav@c.cc', '0948489696', null, null, '2018-04-02 15:46:13', '2018-04-02 15:46:13', null);
INSERT INTO `employees` VALUES ('22', 'tvt', null, '1990-10-24 00:00:00', null, 'cdddd', 'lvc@cc.com', '111111111', null, null, '2018-04-02 15:47:35', '2018-04-02 15:47:35', null);
INSERT INTO `employees` VALUES ('23', 'Thu hà', null, '1990-08-03 00:00:00', null, 'cdddd', 'thuha@gmail.com', '0985647322', null, null, '2018-04-03 15:12:32', '2018-04-03 15:12:32', null);

-- ----------------------------
-- Table structure for employee_position
-- ----------------------------
DROP TABLE IF EXISTS `employee_position`;
CREATE TABLE `employee_position` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) DEFAULT NULL,
  `employee_id` int(10) DEFAULT NULL,
  `option_code` varchar(10) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employee_position
-- ----------------------------
INSERT INTO `employee_position` VALUES ('1', '25', '6', 'cdddd', null, '2018-03-31 12:51:33', '2018-03-31 12:51:33');
INSERT INTO `employee_position` VALUES ('2', '25', '7', 'cdddd', '2018-03-31 13:43:11', '2018-03-31 12:53:33', '2018-03-31 13:43:11');
INSERT INTO `employee_position` VALUES ('3', '25', '8', 'cdddd', '2018-03-31 13:42:41', '2018-03-31 13:25:17', '2018-03-31 13:42:41');
INSERT INTO `employee_position` VALUES ('4', '25', '9', 'cdddd', null, '2018-03-31 14:18:42', '2018-03-31 14:18:42');
INSERT INTO `employee_position` VALUES ('5', '25', '10', 'cdddd', null, '2018-03-31 14:27:25', '2018-03-31 14:27:25');
INSERT INTO `employee_position` VALUES ('6', '25', '11', 'cdddd', null, '2018-03-31 15:12:07', '2018-03-31 15:12:07');
INSERT INTO `employee_position` VALUES ('7', '25', '12', 'cdddd', null, '2018-03-31 16:33:15', '2018-03-31 16:33:15');
INSERT INTO `employee_position` VALUES ('8', '25', '13', 'cdddd', null, '2018-04-01 13:19:54', '2018-04-01 13:19:54');
INSERT INTO `employee_position` VALUES ('9', '25', '14', 'cdddd', null, '2018-04-01 16:33:24', '2018-04-01 16:33:24');
INSERT INTO `employee_position` VALUES ('10', '25', '15', 'cdddd', null, '2018-04-02 02:48:37', '2018-04-02 02:48:37');
INSERT INTO `employee_position` VALUES ('11', '25', '16', 'ddg33', null, '2018-04-02 04:48:02', '2018-04-02 04:48:02');
INSERT INTO `employee_position` VALUES ('12', '25', '17', 'cdddd', null, '2018-04-02 04:48:26', '2018-04-02 04:48:26');
INSERT INTO `employee_position` VALUES ('13', '25', '18', 'cdddd', null, '2018-04-02 15:32:20', '2018-04-02 15:32:20');
INSERT INTO `employee_position` VALUES ('14', '25', '19', 'cdddd', null, '2018-04-02 15:34:50', '2018-04-02 15:34:50');
INSERT INTO `employee_position` VALUES ('15', '25', '20', 'cdddd', null, '2018-04-02 15:45:00', '2018-04-02 15:45:00');
INSERT INTO `employee_position` VALUES ('16', '25', '21', 'ddg33', null, '2018-04-02 15:46:13', '2018-04-02 15:46:13');
INSERT INTO `employee_position` VALUES ('17', '25', '22', 'cdddd', null, '2018-04-02 15:47:35', '2018-04-02 15:47:35');
INSERT INTO `employee_position` VALUES ('18', '25', '23', 'cdddd', null, '2018-04-03 15:12:32', '2018-04-03 15:12:32');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `originalName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `absPath` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relativeUrl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=389 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of files
-- ----------------------------
INSERT INTO `files` VALUES ('1', 'NGUYEN DUC HANH.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451167_5a90fc1fcf16c3.79462674.jpeg', 'collection/2018/02/24/img_1519451167_5a90fc1fcf16c3.79462674.jpeg', 'image/jpeg', '243185', '2018-02-24 12:46:07', '2018-02-24 12:46:07', '1');
INSERT INTO `files` VALUES ('2', 'TranVanTrinh.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451203_5a90fc431d1248.79172703.jpeg', 'collection/2018/02/24/img_1519451203_5a90fc431d1248.79172703.jpeg', 'image/jpeg', '34932', '2018-02-24 12:46:43', '2018-02-24 12:46:43', '1');
INSERT INTO `files` VALUES ('3', 'DAO DUY KHANH TUNG.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451321_5a90fcb93a5dd7.69756380.jpeg', 'collection/2018/02/24/img_1519451321_5a90fcb93a5dd7.69756380.jpeg', 'image/jpeg', '100723', '2018-02-24 12:48:41', '2018-02-24 12:48:41', '1');
INSERT INTO `files` VALUES ('4', 'NGUYEN DUC HANH.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451412_5a90fd1467a600.08515929.jpeg', 'collection/2018/02/24/img_1519451412_5a90fd1467a600.08515929.jpeg', 'image/jpeg', '243185', '2018-02-24 12:50:12', '2018-02-24 12:50:12', '1');
INSERT INTO `files` VALUES ('5', 'NGUYEN DUC HANH.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451419_5a90fd1b3e49c1.66191795.jpeg', 'collection/2018/02/24/img_1519451419_5a90fd1b3e49c1.66191795.jpeg', 'image/jpeg', '243185', '2018-02-24 12:50:19', '2018-02-24 12:50:19', '1');
INSERT INTO `files` VALUES ('6', 'tải xuống (1).jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519454500_5a910924ecf468.76533821.jpeg', 'collection/2018/02/24/img_1519454500_5a910924ecf468.76533821.jpeg', 'image/jpeg', '8797', '2018-02-24 13:41:40', '2018-02-24 13:41:40', '1');
INSERT INTO `files` VALUES ('7', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519456638_5a91117eaae866.31338780.png', 'collection/2018/02/24/img_1519456638_5a91117eaae866.31338780.png', 'image/png', '23489', '2018-02-24 14:17:18', '2018-02-24 14:17:18', '1');
INSERT INTO `files` VALUES ('8', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519457700_5a9115a402da64.77237649.png', 'collection/2018/02/24/img_1519457700_5a9115a402da64.77237649.png', 'image/png', '23489', '2018-02-24 14:35:00', '2018-02-24 14:35:00', '1');
INSERT INTO `files` VALUES ('9', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519457805_5a91160d546610.77412253.png', 'collection/2018/02/24/img_1519457805_5a91160d546610.77412253.png', 'image/png', '23489', '2018-02-24 14:36:45', '2018-02-24 14:36:45', '1');
INSERT INTO `files` VALUES ('10', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519457897_5a9116690a10b2.79534223.png', 'collection/2018/02/24/img_1519457897_5a9116690a10b2.79534223.png', 'image/png', '23489', '2018-02-24 14:38:17', '2018-02-24 14:38:17', '1');
INSERT INTO `files` VALUES ('11', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519457998_5a9116ce0174b1.44027310.png', 'collection/2018/02/24/img_1519457998_5a9116ce0174b1.44027310.png', 'image/png', '23489', '2018-02-24 14:39:58', '2018-02-24 14:39:58', '1');
INSERT INTO `files` VALUES ('12', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519458202_5a91179a078ce9.90395819.png', 'collection/2018/02/24/img_1519458202_5a91179a078ce9.90395819.png', 'image/png', '23489', '2018-02-24 14:43:22', '2018-02-24 14:43:22', '1');
INSERT INTO `files` VALUES ('13', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519458335_5a91181f296032.75680834.png', 'collection/2018/02/24/img_1519458335_5a91181f296032.75680834.png', 'image/png', '23489', '2018-02-24 14:45:35', '2018-02-24 14:45:35', '1');
INSERT INTO `files` VALUES ('14', '5a92dd894598e_1519574409_20180225.jpg', 'files/decision/2018/02/5a92dd894598e_1519574409_20180225.jpg', 'files/decision/2018/02/5a92dd894598e_1519574409_20180225.jpg', 'image/jpeg', '11122', '2018-02-25 23:00:09', '2018-02-25 23:00:09', '1');
INSERT INTO `files` VALUES ('15', '5a92eaba9dfa8_1519577786_20180225.jpg', 'files/decision/2018/02/5a92eaba9dfa8_1519577786_20180225.jpg', 'files/decision/2018/02/5a92eaba9dfa8_1519577786_20180225.jpg', 'image/jpeg', '1934362', '2018-02-25 23:56:26', '2018-02-25 23:56:26', '1');
INSERT INTO `files` VALUES ('16', '15-thumb.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519611441_5a936e31e6e684.04251287.jpeg', 'collection/2018/02/26/img_1519611441_5a936e31e6e684.04251287.jpeg', 'image/jpeg', '126194', '2018-02-26 09:17:21', '2018-02-26 09:17:21', '1');
INSERT INTO `files` VALUES ('17', '13.png', '/var/www/html/public/files/collection/2018/02/26/img_1519612808_5a9373889c4be5.03437785.png', 'collection/2018/02/26/img_1519612808_5a9373889c4be5.03437785.png', 'image/png', '596463', '2018-02-26 09:40:08', '2018-02-26 09:40:08', '1');
INSERT INTO `files` VALUES ('18', '13.png', '/var/www/html/public/files/collection/2018/02/26/img_1519612975_5a93742f2634e5.81380154.png', 'collection/2018/02/26/img_1519612975_5a93742f2634e5.81380154.png', 'image/png', '596463', '2018-02-26 09:42:55', '2018-02-26 09:42:55', '1');
INSERT INTO `files` VALUES ('19', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621769_5a939689d6e5a8.05789966.png', 'collection/2018/02/26/img_1519621769_5a939689d6e5a8.05789966.png', 'image/png', '188588', '2018-02-26 12:09:29', '2018-02-26 12:09:29', '1');
INSERT INTO `files` VALUES ('20', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621778_5a939692bc4006.44694624.png', 'collection/2018/02/26/img_1519621778_5a939692bc4006.44694624.png', 'image/png', '188588', '2018-02-26 12:09:38', '2018-02-26 12:09:38', '1');
INSERT INTO `files` VALUES ('21', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621826_5a9396c2891a51.61705432.png', 'collection/2018/02/26/img_1519621826_5a9396c2891a51.61705432.png', 'image/png', '188588', '2018-02-26 12:10:26', '2018-02-26 12:10:26', '1');
INSERT INTO `files` VALUES ('22', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621857_5a9396e18ce861.51794698.png', 'collection/2018/02/26/img_1519621857_5a9396e18ce861.51794698.png', 'image/png', '188588', '2018-02-26 12:10:57', '2018-02-26 12:10:57', '1');
INSERT INTO `files` VALUES ('23', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621863_5a9396e7f279c7.11585345.png', 'collection/2018/02/26/img_1519621863_5a9396e7f279c7.11585345.png', 'image/png', '188588', '2018-02-26 12:11:04', '2018-02-26 12:11:04', '1');
INSERT INTO `files` VALUES ('24', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519622879_5a939adfba43e7.76143131.png', 'collection/2018/02/26/img_1519622879_5a939adfba43e7.76143131.png', 'image/png', '188588', '2018-02-26 12:27:59', '2018-02-26 12:27:59', '1');
INSERT INTO `files` VALUES ('25', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519622901_5a939af55eae35.29623507.png', 'collection/2018/02/26/img_1519622901_5a939af55eae35.29623507.png', 'image/png', '188588', '2018-02-26 12:28:21', '2018-02-26 12:28:21', '1');
INSERT INTO `files` VALUES ('26', '5a93c167bb189_1519632743_20180226.png', 'files/decision/2018/02/5a93c167bb189_1519632743_20180226.png', 'files/decision/2018/02/5a93c167bb189_1519632743_20180226.png', 'image/png', '90744', '2018-02-26 15:12:23', '2018-02-26 15:12:23', '1');
INSERT INTO `files` VALUES ('27', '5a93cb152a704_1519635221_20180226.JPG', 'files/decision/2018/02/5a93cb152a704_1519635221_20180226.JPG', 'files/decision/2018/02/5a93cb152a704_1519635221_20180226.JPG', 'image/jpeg', '1726372', '2018-02-26 15:53:41', '2018-02-26 15:53:41', '1');
INSERT INTO `files` VALUES ('28', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519635918_5a93cdce6a29a8.70429118.jpeg', 'collection/2018/02/26/img_1519635918_5a93cdce6a29a8.70429118.jpeg', 'image/jpeg', '879394', '2018-02-26 16:05:18', '2018-02-26 16:05:18', '1');
INSERT INTO `files` VALUES ('29', '5a93d055b90ea_1519636565_20180226.PNG', 'files/decision/2018/02/5a93d055b90ea_1519636565_20180226.PNG', 'files/decision/2018/02/5a93d055b90ea_1519636565_20180226.PNG', 'image/png', '30222', '2018-02-26 16:16:05', '2018-02-26 16:16:05', '1');
INSERT INTO `files` VALUES ('30', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519637964_5a93d5cc4ebe85.50448720.jpeg', 'collection/2018/02/26/img_1519637964_5a93d5cc4ebe85.50448720.jpeg', 'image/jpeg', '879394', '2018-02-26 16:39:24', '2018-02-26 16:39:24', '1');
INSERT INTO `files` VALUES ('31', '5a93d78a3a07e_1519638410_20180226.PNG', 'files/decision/2018/02/5a93d78a3a07e_1519638410_20180226.PNG', 'files/decision/2018/02/5a93d78a3a07e_1519638410_20180226.PNG', 'image/png', '30222', '2018-02-26 16:46:50', '2018-02-26 16:46:50', '1');
INSERT INTO `files` VALUES ('32', 'Desert.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519638578_5a93d832b2d622.92349533.jpeg', 'collection/2018/02/26/img_1519638578_5a93d832b2d622.92349533.jpeg', 'image/jpeg', '845941', '2018-02-26 16:49:38', '2018-02-26 16:49:38', '1');
INSERT INTO `files` VALUES ('33', '5a93dc658ff7f_1519639653_20180226.png', 'files/decision/2018/02/5a93dc658ff7f_1519639653_20180226.png', 'files/decision/2018/02/5a93dc658ff7f_1519639653_20180226.png', 'image/png', '90744', '2018-02-26 17:07:33', '2018-02-26 17:07:33', '1');
INSERT INTO `files` VALUES ('34', 'hinh-nen-phim-angry-bird-dep-nhat-250x320.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519662393_5a94353995ab98.32311091.jpeg', 'collection/2018/02/26/img_1519662393_5a94353995ab98.32311091.jpeg', 'image/jpeg', '16340', '2018-02-26 23:26:33', '2018-02-26 23:26:33', '1');
INSERT INTO `files` VALUES ('35', 'hinh-nen-phim-angry-bird-dep-nhat-250x320.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519662527_5a9435bf0cc753.06918980.jpeg', 'collection/2018/02/26/img_1519662527_5a9435bf0cc753.06918980.jpeg', 'image/jpeg', '16340', '2018-02-26 23:28:47', '2018-02-26 23:28:47', '1');
INSERT INTO `files` VALUES ('36', 'hinh-nen-phim-angry-bird-dep-nhat-250x320.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519662653_5a94363db4a4f6.38715833.jpeg', 'collection/2018/02/26/img_1519662653_5a94363db4a4f6.38715833.jpeg', 'image/jpeg', '16340', '2018-02-26 23:30:53', '2018-02-26 23:30:53', '1');
INSERT INTO `files` VALUES ('37', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519702580_5a94d2341625e4.46964606.jpeg', 'collection/2018/02/27/img_1519702580_5a94d2341625e4.46964606.jpeg', 'image/jpeg', '879394', '2018-02-27 10:36:20', '2018-02-27 10:36:20', '1');
INSERT INTO `files` VALUES ('38', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519702751_5a94d2df1d22e8.75918553.jpeg', 'collection/2018/02/27/img_1519702751_5a94d2df1d22e8.75918553.jpeg', 'image/jpeg', '879394', '2018-02-27 10:39:11', '2018-02-27 10:39:11', '1');
INSERT INTO `files` VALUES ('39', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519703063_5a94d4176b1e72.42922819.jpeg', 'collection/2018/02/27/img_1519703063_5a94d4176b1e72.42922819.jpeg', 'image/jpeg', '879394', '2018-02-27 10:44:23', '2018-02-27 10:44:23', '1');
INSERT INTO `files` VALUES ('40', '109.png', '/var/www/html/public/files/collection/2018/02/27/img_1519703299_5a94d503e87d72.99676817.png', 'collection/2018/02/27/img_1519703299_5a94d503e87d72.99676817.png', 'image/png', '545911', '2018-02-27 10:48:19', '2018-02-27 10:48:19', '1');
INSERT INTO `files` VALUES ('41', 'computername.png', '/var/www/html/public/files/collection/2018/02/27/img_1519703841_5a94d721c317e6.56836703.png', 'collection/2018/02/27/img_1519703841_5a94d721c317e6.56836703.png', 'image/png', '37785', '2018-02-27 10:57:21', '2018-02-27 10:57:21', '1');
INSERT INTO `files` VALUES ('42', '300[1].jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519704996_5a94dba480c453.62476810.jpeg', 'collection/2018/02/27/img_1519704996_5a94dba480c453.62476810.jpeg', 'image/jpeg', '11479', '2018-02-27 11:16:36', '2018-02-27 11:16:36', '1');
INSERT INTO `files` VALUES ('43', '300[1].jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519705532_5a94ddbc840759.43046652.jpeg', 'collection/2018/02/27/img_1519705532_5a94ddbc840759.43046652.jpeg', 'image/jpeg', '11479', '2018-02-27 11:25:32', '2018-02-27 11:25:32', '1');
INSERT INTO `files` VALUES ('44', 'IMG-0350 (1).jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519705848_5a94def8cf33e4.52246967.jpeg', 'collection/2018/02/27/img_1519705848_5a94def8cf33e4.52246967.jpeg', 'image/jpeg', '506056', '2018-02-27 11:30:49', '2018-02-27 11:30:49', '1');
INSERT INTO `files` VALUES ('45', 'TranVanTrinh.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519706052_5a94dfc4a389c9.40438360.jpeg', 'collection/2018/02/27/img_1519706052_5a94dfc4a389c9.40438360.jpeg', 'image/jpeg', '34932', '2018-02-27 11:34:12', '2018-02-27 11:34:12', '1');
INSERT INTO `files` VALUES ('46', 'IMG-0350.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519706197_5a94e0550265f1.25549632.jpeg', 'collection/2018/02/27/img_1519706197_5a94e0550265f1.25549632.jpeg', 'image/jpeg', '506056', '2018-02-27 11:36:37', '2018-02-27 11:36:37', '1');
INSERT INTO `files` VALUES ('47', 'TranVanTrinh.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519706346_5a94e0ea076d65.00773363.jpeg', 'collection/2018/02/27/img_1519706346_5a94e0ea076d65.00773363.jpeg', 'image/jpeg', '34932', '2018-02-27 11:39:06', '2018-02-27 11:39:06', '1');
INSERT INTO `files` VALUES ('48', '5a9512b4d99ee_1519719092_20180227.jpg', 'files/decision/2018/02/5a9512b4d99ee_1519719092_20180227.jpg', 'files/decision/2018/02/5a9512b4d99ee_1519719092_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:11:32', '2018-02-27 15:11:32', '1');
INSERT INTO `files` VALUES ('49', '5a951337ba1c4_1519719223_20180227.jpg', 'files/decision/2018/02/5a951337ba1c4_1519719223_20180227.jpg', 'files/decision/2018/02/5a951337ba1c4_1519719223_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:13:43', '2018-02-27 15:13:43', '1');
INSERT INTO `files` VALUES ('50', '5a95134a8e271_1519719242_20180227.jpg', 'files/decision/2018/02/5a95134a8e271_1519719242_20180227.jpg', 'files/decision/2018/02/5a95134a8e271_1519719242_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:14:02', '2018-02-27 15:14:02', '1');
INSERT INTO `files` VALUES ('51', '5a95139bd06d6_1519719323_20180227.jpg', 'files/decision/2018/02/5a95139bd06d6_1519719323_20180227.jpg', 'files/decision/2018/02/5a95139bd06d6_1519719323_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:15:23', '2018-02-27 15:15:23', '1');
INSERT INTO `files` VALUES ('52', '5a95140ef2a15_1519719438_20180227.jpg', 'files/decision/2018/02/5a95140ef2a15_1519719438_20180227.jpg', 'files/decision/2018/02/5a95140ef2a15_1519719438_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:17:18', '2018-02-27 15:17:18', '1');
INSERT INTO `files` VALUES ('53', '5a95142cb7476_1519719468_20180227.jpg', 'files/decision/2018/02/5a95142cb7476_1519719468_20180227.jpg', 'files/decision/2018/02/5a95142cb7476_1519719468_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:17:48', '2018-02-27 15:17:48', '1');
INSERT INTO `files` VALUES ('54', '5a95143963828_1519719481_20180227.jpg', 'files/decision/2018/02/5a95143963828_1519719481_20180227.jpg', 'files/decision/2018/02/5a95143963828_1519719481_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:18:01', '2018-02-27 15:18:01', '1');
INSERT INTO `files` VALUES ('55', '5a95144fba0fd_1519719503_20180227.jpg', 'files/decision/2018/02/5a95144fba0fd_1519719503_20180227.jpg', 'files/decision/2018/02/5a95144fba0fd_1519719503_20180227.jpg', 'image/jpeg', '780831', '2018-02-27 15:18:23', '2018-02-27 15:18:23', '1');
INSERT INTO `files` VALUES ('56', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519721016_5a951a3864be80.41599715.jpeg', 'collection/2018/02/27/img_1519721016_5a951a3864be80.41599715.jpeg', 'image/jpeg', '879394', '2018-02-27 15:43:36', '2018-02-27 15:43:36', '1');
INSERT INTO `files` VALUES ('57', 'Desert.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519721229_5a951b0d0db026.89933087.jpeg', 'collection/2018/02/27/img_1519721229_5a951b0d0db026.89933087.jpeg', 'image/jpeg', '845941', '2018-02-27 15:47:09', '2018-02-27 15:47:09', '1');
INSERT INTO `files` VALUES ('58', '5a9530c5cc500_1519726789_20180227.jpg', 'files/decision/2018/02/5a9530c5cc500_1519726789_20180227.jpg', 'files/decision/2018/02/5a9530c5cc500_1519726789_20180227.jpg', 'image/jpeg', '222158', '2018-02-27 17:19:49', '2018-02-27 17:19:49', '1');
INSERT INTO `files` VALUES ('59', '5a9536d63d9dd_1519728342_20180227.png', 'files/decision/2018/02/5a9536d63d9dd_1519728342_20180227.png', 'files/decision/2018/02/5a9536d63d9dd_1519728342_20180227.png', 'image/png', '48978', '2018-02-27 17:45:42', '2018-02-27 17:45:42', '1');
INSERT INTO `files` VALUES ('60', '5a9536ee7ca3e_1519728366_20180227.png', 'files/decision/2018/02/5a9536ee7ca3e_1519728366_20180227.png', 'files/decision/2018/02/5a9536ee7ca3e_1519728366_20180227.png', 'image/png', '206138', '2018-02-27 17:46:06', '2018-02-27 17:46:06', '1');
INSERT INTO `files` VALUES ('61', '5a95370e3187a_1519728398_20180227.png', 'files/decision/2018/02/5a95370e3187a_1519728398_20180227.png', 'files/decision/2018/02/5a95370e3187a_1519728398_20180227.png', 'image/png', '206138', '2018-02-27 17:46:38', '2018-02-27 17:46:38', '1');
INSERT INTO `files` VALUES ('62', '5a953718dc0ab_1519728408_20180227.png', 'files/decision/2018/02/5a953718dc0ab_1519728408_20180227.png', 'files/decision/2018/02/5a953718dc0ab_1519728408_20180227.png', 'image/png', '206138', '2018-02-27 17:46:48', '2018-02-27 17:46:48', '1');
INSERT INTO `files` VALUES ('63', '5a954afd5d6cc_1519733501_20180227.png', 'files/decision/2018/02/5a954afd5d6cc_1519733501_20180227.png', 'files/decision/2018/02/5a954afd5d6cc_1519733501_20180227.png', 'image/png', '206138', '2018-02-27 19:11:41', '2018-02-27 19:11:41', '1');
INSERT INTO `files` VALUES ('64', '5a954b14a1e90_1519733524_20180227.png', 'files/decision/2018/02/5a954b14a1e90_1519733524_20180227.png', 'files/decision/2018/02/5a954b14a1e90_1519733524_20180227.png', 'image/png', '57589', '2018-02-27 19:12:04', '2018-02-27 19:12:04', '1');
INSERT INTO `files` VALUES ('65', '5a954b57e2084_1519733591_20180227.png', 'files/decision/2018/02/5a954b57e2084_1519733591_20180227.png', 'files/decision/2018/02/5a954b57e2084_1519733591_20180227.png', 'image/png', '206138', '2018-02-27 19:13:11', '2018-02-27 19:13:11', '1');
INSERT INTO `files` VALUES ('66', '5a954b86ebc85_1519733638_20180227.png', 'files/decision/2018/02/5a954b86ebc85_1519733638_20180227.png', 'files/decision/2018/02/5a954b86ebc85_1519733638_20180227.png', 'image/png', '581436', '2018-02-27 19:13:58', '2018-02-27 19:13:58', '1');
INSERT INTO `files` VALUES ('67', '5a961e2481702_1519787556_20180228.jpg', 'files/decision/2018/02/5a961e2481702_1519787556_20180228.jpg', 'files/decision/2018/02/5a961e2481702_1519787556_20180228.jpg', 'image/jpeg', '162122', '2018-02-28 10:12:36', '2018-02-28 10:12:36', '1');
INSERT INTO `files` VALUES ('68', '5a962068c6efa_1519788136_20180228.jpg', 'files/decision/2018/02/5a962068c6efa_1519788136_20180228.jpg', 'files/decision/2018/02/5a962068c6efa_1519788136_20180228.jpg', 'image/jpeg', '879394', '2018-02-28 10:22:16', '2018-02-28 10:22:16', '1');
INSERT INTO `files` VALUES ('69', '5a96210d59811_1519788301_20180228.jpg', 'files/decision/2018/02/5a96210d59811_1519788301_20180228.jpg', 'files/decision/2018/02/5a96210d59811_1519788301_20180228.jpg', 'image/jpeg', '595284', '2018-02-28 10:25:01', '2018-02-28 10:25:01', '1');
INSERT INTO `files` VALUES ('70', '5a96211e40765_1519788318_20180228.jpg', 'files/decision/2018/02/5a96211e40765_1519788318_20180228.jpg', 'files/decision/2018/02/5a96211e40765_1519788318_20180228.jpg', 'image/jpeg', '595284', '2018-02-28 10:25:18', '2018-02-28 10:25:18', '1');
INSERT INTO `files` VALUES ('71', '5a975c7840310_1519869048_20180301.mp4', 'files/decision/2018/03/5a975c7840310_1519869048_20180301.mp4', 'files/decision/2018/03/5a975c7840310_1519869048_20180301.mp4', 'video/mp4', '1892700', '2018-02-28 10:25:34', '2018-03-01 08:50:48', '1');
INSERT INTO `files` VALUES ('72', 'thamdinh_2acc.jpg', '/var/www/html/public/files/collection/2018/02/28/img_1519790073_5a9627f99d18e3.65271982.jpeg', 'collection/2018/02/28/img_1519790073_5a9627f99d18e3.65271982.jpeg', 'image/jpeg', '222158', '2018-02-28 10:54:33', '2018-02-28 10:54:33', '1');
INSERT INTO `files` VALUES ('73', 'computername.png', '/var/www/html/public/files/collection/2018/02/28/img_1519803974_5a965e46111852.39977560.png', 'collection/2018/02/28/img_1519803974_5a965e46111852.39977560.png', 'image/png', '37785', '2018-02-28 14:46:14', '2018-02-28 14:46:14', '1');
INSERT INTO `files` VALUES ('74', 'IMG_8091.JPG', '/var/www/html/public/files/collection/2018/02/28/img_1519804473_5a9660394e9774.52846602.jpeg', 'collection/2018/02/28/img_1519804473_5a9660394e9774.52846602.jpeg', 'image/jpeg', '1729938', '2018-02-28 14:54:33', '2018-02-28 14:54:33', '1');
INSERT INTO `files` VALUES ('75', 'baoloithieuanhchandungmacduco.png', '/var/www/html/public/files/collection/2018/02/28/img_1519813781_5a9684953e7a18.76667414.png', 'collection/2018/02/28/img_1519813781_5a9684953e7a18.76667414.png', 'image/png', '90744', '2018-02-28 17:29:41', '2018-02-28 17:29:41', '1');
INSERT INTO `files` VALUES ('76', '5a96856f540d9_1519813999_20180228.mp4', 'files/decision/2018/02/5a96856f540d9_1519813999_20180228.mp4', 'files/decision/2018/02/5a96856f540d9_1519813999_20180228.mp4', 'video/mp4', '1892700', '2018-02-28 17:33:19', '2018-02-28 17:33:19', '1');
INSERT INTO `files` VALUES ('77', 'IMG_8091.JPG', '/var/www/html/public/files/collection/2018/02/28/img_1519814507_5a96876b9df6b9.29143005.jpeg', 'collection/2018/02/28/img_1519814507_5a96876b9df6b9.29143005.jpeg', 'image/jpeg', '1729938', '2018-02-28 17:41:47', '2018-02-28 17:41:47', '1');
INSERT INTO `files` VALUES ('78', '5a97629692d8b_1519870614_20180301.jpg', 'files/decision/2018/03/5a97629692d8b_1519870614_20180301.jpg', 'files/decision/2018/03/5a97629692d8b_1519870614_20180301.jpg', 'image/jpeg', '845941', '2018-03-01 09:16:54', '2018-03-01 09:16:54', '1');
INSERT INTO `files` VALUES ('79', '5a9762b500fdd_1519870645_20180301.jpg', 'files/decision/2018/03/5a9762b500fdd_1519870645_20180301.jpg', 'files/decision/2018/03/5a9762b500fdd_1519870645_20180301.jpg', 'image/jpeg', '879394', '2018-03-01 09:17:25', '2018-03-01 09:17:25', '1');
INSERT INTO `files` VALUES ('80', 'baoloithieuanhchandungmacduco.png', '/var/www/html/public/files/collection/2018/03/01/img_1519875635_5a977633c84bf9.55579465.png', 'collection/2018/03/01/img_1519875635_5a977633c84bf9.55579465.png', 'image/png', '90744', '2018-03-01 10:40:35', '2018-03-01 10:40:35', '1');
INSERT INTO `files` VALUES ('81', 'computername.png', '/var/www/html/public/files/collection/2018/03/01/img_1519875656_5a977648844045.42114736.png', 'collection/2018/03/01/img_1519875656_5a977648844045.42114736.png', 'image/png', '37785', '2018-03-01 10:40:56', '2018-03-01 10:40:56', '1');
INSERT INTO `files` VALUES ('82', '5a9781c03d16b_1519878592_20180301.jpg', 'files/decision/2018/03/5a9781c03d16b_1519878592_20180301.jpg', 'files/decision/2018/03/5a9781c03d16b_1519878592_20180301.jpg', 'image/jpeg', '879394', '2018-03-01 11:29:52', '2018-03-01 11:29:52', '1');
INSERT INTO `files` VALUES ('83', '5a9781f89e50b_1519878648_20180301.jpg', 'files/decision/2018/03/5a9781f89e50b_1519878648_20180301.jpg', 'files/decision/2018/03/5a9781f89e50b_1519878648_20180301.jpg', 'image/jpeg', '845941', '2018-03-01 11:30:48', '2018-03-01 11:30:48', '1');
INSERT INTO `files` VALUES ('84', 'IMG-0350.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519887729_5a97a5714277b6.59899769.jpeg', 'collection/2018/03/01/img_1519887729_5a97a5714277b6.59899769.jpeg', 'image/jpeg', '506056', '2018-03-01 14:02:09', '2018-03-01 14:02:09', '1');
INSERT INTO `files` VALUES ('85', 'DAO DUY KHANH TUNG.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519887849_5a97a5e9555bb6.64068158.jpeg', 'collection/2018/03/01/img_1519887849_5a97a5e9555bb6.64068158.jpeg', 'image/jpeg', '100723', '2018-03-01 14:04:09', '2018-03-01 14:04:09', '1');
INSERT INTO `files` VALUES ('86', 'NGUYEN DUC HANH.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519888022_5a97a6966870c5.30132912.jpeg', 'collection/2018/03/01/img_1519888022_5a97a6966870c5.30132912.jpeg', 'image/jpeg', '243185', '2018-03-01 14:07:02', '2018-03-01 14:07:02', '1');
INSERT INTO `files` VALUES ('87', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519888234_5a97a76a3b4628.73415917.jpeg', 'collection/2018/03/01/img_1519888234_5a97a76a3b4628.73415917.jpeg', 'image/jpeg', '777835', '2018-03-01 14:10:34', '2018-03-01 14:10:34', '1');
INSERT INTO `files` VALUES ('88', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519888824_5a97a9b89d2943.40766449.jpeg', 'collection/2018/03/01/img_1519888824_5a97a9b89d2943.40766449.jpeg', 'image/jpeg', '777835', '2018-03-01 14:20:24', '2018-03-01 14:20:24', '1');
INSERT INTO `files` VALUES ('89', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519888861_5a97a9dd207b75.98475229.jpeg', 'collection/2018/03/01/img_1519888861_5a97a9dd207b75.98475229.jpeg', 'image/jpeg', '780831', '2018-03-01 14:21:01', '2018-03-01 14:21:01', '1');
INSERT INTO `files` VALUES ('90', 'Baoloithieu anh chan dung.png', '/var/www/html/public/files/collection/2018/03/01/img_1519889778_5a97ad72d32397.88163598.png', 'collection/2018/03/01/img_1519889778_5a97ad72d32397.88163598.png', 'image/png', '84240', '2018-03-01 14:36:18', '2018-03-01 14:36:18', '1');
INSERT INTO `files` VALUES ('91', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519890382_5a97afce2a7c66.32480494.jpeg', 'collection/2018/03/01/img_1519890382_5a97afce2a7c66.32480494.jpeg', 'image/jpeg', '879394', '2018-03-01 14:46:22', '2018-03-01 14:46:22', '1');
INSERT INTO `files` VALUES ('92', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519890607_5a97b0afba1529.38901662.jpeg', 'collection/2018/03/01/img_1519890607_5a97b0afba1529.38901662.jpeg', 'image/jpeg', '845941', '2018-03-01 14:50:07', '2018-03-01 14:50:07', '1');
INSERT INTO `files` VALUES ('93', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519890845_5a97b19dd5cd36.40972633.jpeg', 'collection/2018/03/01/img_1519890845_5a97b19dd5cd36.40972633.jpeg', 'image/jpeg', '845941', '2018-03-01 14:54:05', '2018-03-01 14:54:05', '1');
INSERT INTO `files` VALUES ('94', '5a97b2c1e41c2_1519891137_20180301.jpg', 'files/decision/2018/03/5a97b2c1e41c2_1519891137_20180301.jpg', 'files/decision/2018/03/5a97b2c1e41c2_1519891137_20180301.jpg', 'image/jpeg', '879394', '2018-03-01 14:58:57', '2018-03-01 14:58:57', '1');
INSERT INTO `files` VALUES ('95', '5a97b2cec51a6_1519891150_20180301.jpg', 'files/decision/2018/03/5a97b2cec51a6_1519891150_20180301.jpg', 'files/decision/2018/03/5a97b2cec51a6_1519891150_20180301.jpg', 'image/jpeg', '845941', '2018-03-01 14:59:10', '2018-03-01 14:59:10', '1');
INSERT INTO `files` VALUES ('96', '5a97b2d99dc40_1519891161_20180301.jpg', 'files/decision/2018/03/5a97b2d99dc40_1519891161_20180301.jpg', 'files/decision/2018/03/5a97b2d99dc40_1519891161_20180301.jpg', 'image/jpeg', '775702', '2018-03-01 14:59:21', '2018-03-01 14:59:21', '1');
INSERT INTO `files` VALUES ('97', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519892904_5a97b9a8e800e4.01452158.jpeg', 'collection/2018/03/01/img_1519892904_5a97b9a8e800e4.01452158.jpeg', 'image/jpeg', '780831', '2018-03-01 15:28:24', '2018-03-01 15:28:24', '1');
INSERT INTO `files` VALUES ('98', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519962556_5a98c9bc9d8475.67006973.jpeg', 'collection/2018/03/02/img_1519962556_5a98c9bc9d8475.67006973.jpeg', 'image/jpeg', '879394', '2018-03-02 10:49:16', '2018-03-02 10:49:16', '1');
INSERT INTO `files` VALUES ('99', '5a98ca9e8bb6e_1519962782_20180302.png', 'files/decision/2018/03/5a98ca9e8bb6e_1519962782_20180302.png', 'files/decision/2018/03/5a98ca9e8bb6e_1519962782_20180302.png', 'image/png', '32898', '2018-03-02 10:53:02', '2018-03-02 10:53:02', '1');
INSERT INTO `files` VALUES ('100', '5a98cb72948c1_1519962994_20180302.jpg', 'files/decision/2018/03/5a98cb72948c1_1519962994_20180302.jpg', 'files/decision/2018/03/5a98cb72948c1_1519962994_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 10:56:34', '2018-03-02 10:56:34', '1');
INSERT INTO `files` VALUES ('101', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519964559_5a98d18f08f574.04424515.jpeg', 'collection/2018/03/02/img_1519964559_5a98d18f08f574.04424515.jpeg', 'image/jpeg', '879394', '2018-03-02 11:22:39', '2018-03-02 11:22:39', '1');
INSERT INTO `files` VALUES ('102', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519964678_5a98d206bd9f80.79874393.jpeg', 'collection/2018/03/02/img_1519964678_5a98d206bd9f80.79874393.jpeg', 'image/jpeg', '879394', '2018-03-02 11:24:38', '2018-03-02 11:24:38', '1');
INSERT INTO `files` VALUES ('103', '5a98d509f3b9b_1519965449_20180302.png', 'files/decision/2018/03/5a98d509f3b9b_1519965449_20180302.png', 'files/decision/2018/03/5a98d509f3b9b_1519965449_20180302.png', 'image/png', '32898', '2018-03-02 11:37:29', '2018-03-02 11:37:29', '1');
INSERT INTO `files` VALUES ('104', '5a98f4f2d10a0_1519973618_20180302.jpg', 'files/decision/2018/03/5a98f4f2d10a0_1519973618_20180302.jpg', 'files/decision/2018/03/5a98f4f2d10a0_1519973618_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 13:53:38', '2018-03-02 13:53:38', '1');
INSERT INTO `files` VALUES ('105', '5a98f95f8ba01_1519974751_20180302.jpg', 'files/decision/2018/03/5a98f95f8ba01_1519974751_20180302.jpg', 'files/decision/2018/03/5a98f95f8ba01_1519974751_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 14:12:31', '2018-03-02 14:12:31', '1');
INSERT INTO `files` VALUES ('106', '5a98fc98e9b06_1519975576_20180302.jpg', 'files/decision/2018/03/5a98fc98e9b06_1519975576_20180302.jpg', 'files/decision/2018/03/5a98fc98e9b06_1519975576_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 14:26:16', '2018-03-02 14:26:16', '1');
INSERT INTO `files` VALUES ('107', '5a98fe4d40d03_1519976013_20180302.jpg', 'files/decision/2018/03/5a98fe4d40d03_1519976013_20180302.jpg', 'files/decision/2018/03/5a98fe4d40d03_1519976013_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 14:33:33', '2018-03-02 14:33:33', '1');
INSERT INTO `files` VALUES ('108', '5a98fe5882cb1_1519976024_20180302.jpg', 'files/decision/2018/03/5a98fe5882cb1_1519976024_20180302.jpg', 'files/decision/2018/03/5a98fe5882cb1_1519976024_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 14:33:44', '2018-03-02 14:33:44', '1');
INSERT INTO `files` VALUES ('109', '5a98fe65689ac_1519976037_20180302.jpg', 'files/decision/2018/03/5a98fe65689ac_1519976037_20180302.jpg', 'files/decision/2018/03/5a98fe65689ac_1519976037_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 14:33:57', '2018-03-02 14:33:57', '1');
INSERT INTO `files` VALUES ('110', '5a98fe700dfdb_1519976048_20180302.jpg', 'files/decision/2018/03/5a98fe700dfdb_1519976048_20180302.jpg', 'files/decision/2018/03/5a98fe700dfdb_1519976048_20180302.jpg', 'image/jpeg', '775702', '2018-03-02 14:34:08', '2018-03-02 14:34:08', '1');
INSERT INTO `files` VALUES ('111', '5a990219c9643_1519976985_20180302.jpg', 'files/decision/2018/03/5a990219c9643_1519976985_20180302.jpg', 'files/decision/2018/03/5a990219c9643_1519976985_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 14:49:45', '2018-03-02 14:49:45', '1');
INSERT INTO `files` VALUES ('112', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519978071_5a99065796ac33.92452609.jpeg', 'collection/2018/03/02/img_1519978071_5a99065796ac33.92452609.jpeg', 'image/jpeg', '620888', '2018-03-02 15:07:51', '2018-03-02 15:07:51', '1');
INSERT INTO `files` VALUES ('113', 'chu thanh 2 dong.png', '/var/www/html/public/files/collection/2018/03/02/img_1519978716_5a9908dc1e12c1.64640357.png', 'collection/2018/03/02/img_1519978716_5a9908dc1e12c1.64640357.png', 'image/png', '32898', '2018-03-02 15:18:36', '2018-03-02 15:18:36', '1');
INSERT INTO `files` VALUES ('114', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519978846_5a99095e945dd7.04743105.jpeg', 'collection/2018/03/02/img_1519978846_5a99095e945dd7.04743105.jpeg', 'image/jpeg', '879394', '2018-03-02 15:20:46', '2018-03-02 15:20:46', '1');
INSERT INTO `files` VALUES ('115', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519982195_5a99167343c113.40505644.jpeg', 'collection/2018/03/02/img_1519982195_5a99167343c113.40505644.jpeg', 'image/jpeg', '879394', '2018-03-02 16:16:35', '2018-03-02 16:16:35', '1');
INSERT INTO `files` VALUES ('116', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519982317_5a9916edbbf7b6.45274410.jpeg', 'collection/2018/03/02/img_1519982317_5a9916edbbf7b6.45274410.jpeg', 'image/jpeg', '879394', '2018-03-02 16:18:37', '2018-03-02 16:18:37', '1');
INSERT INTO `files` VALUES ('117', '5a99189cc5369_1519982748_20180302.jpg', 'files/decision/2018/03/5a99189cc5369_1519982748_20180302.jpg', 'files/decision/2018/03/5a99189cc5369_1519982748_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 16:25:48', '2018-03-02 16:25:48', '1');
INSERT INTO `files` VALUES ('118', '5a991a4993d0a_1519983177_20180302.jpg', 'files/decision/2018/03/5a991a4993d0a_1519983177_20180302.jpg', 'files/decision/2018/03/5a991a4993d0a_1519983177_20180302.jpg', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '128991', '2018-03-02 16:32:57', '2018-03-02 16:32:57', '1');
INSERT INTO `files` VALUES ('119', '5a991a7734daa_1519983223_20180302.jpg', 'files/decision/2018/03/5a991a7734daa_1519983223_20180302.jpg', 'files/decision/2018/03/5a991a7734daa_1519983223_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 16:33:43', '2018-03-02 16:33:43', '1');
INSERT INTO `files` VALUES ('120', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520221215_5a9cbc1f4d24a9.49649968.jpeg', 'collection/2018/03/05/img_1520221215_5a9cbc1f4d24a9.49649968.jpeg', 'image/jpeg', '845941', '2018-03-05 10:40:15', '2018-03-05 10:40:15', '1');
INSERT INTO `files` VALUES ('121', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520222416_5a9cc0d0b827d3.51786539.jpeg', 'collection/2018/03/05/img_1520222416_5a9cc0d0b827d3.51786539.jpeg', 'image/jpeg', '845941', '2018-03-05 11:00:16', '2018-03-05 11:00:16', '1');
INSERT INTO `files` VALUES ('122', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520225153_5a9ccb8176ff14.25095687.jpeg', 'collection/2018/03/05/img_1520225153_5a9ccb8176ff14.25095687.jpeg', 'image/jpeg', '845941', '2018-03-05 11:45:53', '2018-03-05 11:45:53', '1');
INSERT INTO `files` VALUES ('123', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520233065_5a9cea69edb9e8.98169429.jpeg', 'collection/2018/03/05/img_1520233065_5a9cea69edb9e8.98169429.jpeg', 'image/jpeg', '595284', '2018-03-05 13:57:46', '2018-03-05 13:57:46', '1');
INSERT INTO `files` VALUES ('124', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520237365_5a9cfb350571c7.04321761.jpeg', 'collection/2018/03/05/img_1520237365_5a9cfb350571c7.04321761.jpeg', 'image/jpeg', '845941', '2018-03-05 15:09:25', '2018-03-05 15:09:25', '1');
INSERT INTO `files` VALUES ('125', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520237472_5a9cfba0d8b1f6.88339861.jpeg', 'collection/2018/03/05/img_1520237472_5a9cfba0d8b1f6.88339861.jpeg', 'image/jpeg', '845941', '2018-03-05 15:11:12', '2018-03-05 15:11:12', '1');
INSERT INTO `files` VALUES ('126', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520242374_5a9d0ec617bca7.04319948.jpeg', 'collection/2018/03/05/img_1520242374_5a9d0ec617bca7.04319948.jpeg', 'image/jpeg', '845941', '2018-03-05 16:32:54', '2018-03-05 16:32:54', '1');
INSERT INTO `files` VALUES ('127', '5a9e0a78205dd_1520306808_20180306.jpg', 'files/decision/2018/03/5a9e0a78205dd_1520306808_20180306.jpg', 'files/decision/2018/03/5a9e0a78205dd_1520306808_20180306.jpg', 'image/jpeg', '879394', '2018-03-06 10:26:48', '2018-03-06 10:26:48', '1');
INSERT INTO `files` VALUES ('128', '5a9e0bd7e02fc_1520307159_20180306.jpg', 'files/decision/2018/03/5a9e0bd7e02fc_1520307159_20180306.jpg', 'files/decision/2018/03/5a9e0bd7e02fc_1520307159_20180306.jpg', 'image/jpeg', '879394', '2018-03-06 10:32:39', '2018-03-06 10:32:39', '1');
INSERT INTO `files` VALUES ('129', '5a9e179607d4e_1520310166_20180306.jpg', 'files/decision/2018/03/5a9e179607d4e_1520310166_20180306.jpg', 'files/decision/2018/03/5a9e179607d4e_1520310166_20180306.jpg', 'image/jpeg', '845941', '2018-03-06 11:22:46', '2018-03-06 11:22:46', '1');
INSERT INTO `files` VALUES ('130', '5a9e1a8293b6d_1520310914_20180306.jpg', 'files/decision/2018/03/5a9e1a8293b6d_1520310914_20180306.jpg', 'files/decision/2018/03/5a9e1a8293b6d_1520310914_20180306.jpg', 'image/jpeg', '845941', '2018-03-06 11:35:14', '2018-03-06 11:35:14', '1');
INSERT INTO `files` VALUES ('131', '5a9e1ae3f00f2_1520311011_20180306.jpg', 'files/decision/2018/03/5a9e1ae3f00f2_1520311011_20180306.jpg', 'files/decision/2018/03/5a9e1ae3f00f2_1520311011_20180306.jpg', 'image/jpeg', '879394', '2018-03-06 11:36:51', '2018-03-06 11:36:51', '1');
INSERT INTO `files` VALUES ('132', 'chu thanh 2 dong.png', '/var/www/html/public/files/collection/2018/03/07/img_1520385353_5a9f3d49bee204.24124243.png', 'collection/2018/03/07/img_1520385353_5a9f3d49bee204.24124243.png', 'image/png', '32898', '2018-03-07 08:15:53', '2018-03-07 08:15:53', '1');
INSERT INTO `files` VALUES ('133', '5a9f9783e075e_1520408451_20180307.jpg', 'files/decision/2018/03/5a9f9783e075e_1520408451_20180307.jpg', 'files/decision/2018/03/5a9f9783e075e_1520408451_20180307.jpg', 'image/jpeg', '595284', '2018-03-07 14:40:51', '2018-03-07 14:40:51', '1');
INSERT INTO `files` VALUES ('134', '5a9fa202af223_1520411138_20180307.jpg', 'files/decision/2018/03/5a9fa202af223_1520411138_20180307.jpg', 'files/decision/2018/03/5a9fa202af223_1520411138_20180307.jpg', 'image/jpeg', '845941', '2018-03-07 15:25:38', '2018-03-07 15:25:38', '1');
INSERT INTO `files` VALUES ('135', '5a9fa20e16f24_1520411150_20180307.jpg', 'files/decision/2018/03/5a9fa20e16f24_1520411150_20180307.jpg', 'files/decision/2018/03/5a9fa20e16f24_1520411150_20180307.jpg', 'image/jpeg', '595284', '2018-03-07 15:25:50', '2018-03-07 15:25:50', '1');
INSERT INTO `files` VALUES ('136', '5a9fa52512c13_1520411941_20180307.gif', 'files/decision/2018/03/5a9fa52512c13_1520411941_20180307.gif', 'files/decision/2018/03/5a9fa52512c13_1520411941_20180307.gif', 'image/gif', '18061', '2018-03-07 15:39:01', '2018-03-07 15:39:01', '1');
INSERT INTO `files` VALUES ('137', '5a9fa5828fd19_1520412034_20180307.jpg', 'files/decision/2018/03/5a9fa5828fd19_1520412034_20180307.jpg', 'files/decision/2018/03/5a9fa5828fd19_1520412034_20180307.jpg', 'image/jpeg', '15784', '2018-03-07 15:40:34', '2018-03-07 15:40:34', '1');
INSERT INTO `files` VALUES ('138', 'nuocmam.jpg', '/var/www/html/public/files/collection/2018/03/07/img_1520416075_5a9fb54bc99425.40677675.jpeg', 'collection/2018/03/07/img_1520416075_5a9fb54bc99425.40677675.jpeg', 'image/jpeg', '9188', '2018-03-07 16:47:55', '2018-03-07 16:47:55', '1');
INSERT INTO `files` VALUES ('139', 'lavi.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520565145_5aa1fb99e43d28.94729610.jpeg', 'collection/2018/03/09/img_1520565145_5aa1fb99e43d28.94729610.jpeg', 'image/jpeg', '6221', '2018-03-09 10:12:25', '2018-03-09 10:12:25', '1');
INSERT INTO `files` VALUES ('140', 'cakhonhanhau.png', '/var/www/html/public/files/collection/2018/03/09/img_1520568031_5aa206dfd094f3.69540712.png', 'collection/2018/03/09/img_1520568031_5aa206dfd094f3.69540712.png', 'image/png', '5727', '2018-03-09 11:00:31', '2018-03-09 11:00:31', '1');
INSERT INTO `files` VALUES ('141', 'miendong.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520568468_5aa20894eabc77.22518487.jpeg', 'collection/2018/03/09/img_1520568468_5aa20894eabc77.22518487.jpeg', 'image/jpeg', '9704', '2018-03-09 11:07:48', '2018-03-09 11:07:48', '1');
INSERT INTO `files` VALUES ('142', '`~!@#$%^&()-=_+{};\',.lavi.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520569489_5aa20c9148c256.75396917.jpeg', 'collection/2018/03/09/img_1520569489_5aa20c9148c256.75396917.jpeg', 'image/jpeg', '6221', '2018-03-09 11:24:49', '2018-03-09 11:24:49', '1');
INSERT INTO `files` VALUES ('143', 'miendong.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520570294_5aa20fb67b0ea4.60357904.jpeg', 'collection/2018/03/09/img_1520570294_5aa20fb67b0ea4.60357904.jpeg', 'image/jpeg', '9704', '2018-03-09 11:38:14', '2018-03-09 11:38:14', '1');
INSERT INTO `files` VALUES ('144', '5aa22fea458b4_1520578538_20180309.jpg', 'files/decision/2018/03/5aa22fea458b4_1520578538_20180309.jpg', 'files/decision/2018/03/5aa22fea458b4_1520578538_20180309.jpg', 'image/jpeg', '845941', '2018-03-09 13:55:38', '2018-03-09 13:55:38', '1');
INSERT INTO `files` VALUES ('145', 'daiwoo.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520578941_5aa2317d3f5dc7.95437428.jpeg', 'collection/2018/03/09/img_1520578941_5aa2317d3f5dc7.95437428.jpeg', 'image/jpeg', '6699', '2018-03-09 14:02:21', '2018-03-09 14:02:21', '1');
INSERT INTO `files` VALUES ('146', 'miendong.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520579402_5aa2334a5c8508.72610169.jpeg', 'collection/2018/03/09/img_1520579402_5aa2334a5c8508.72610169.jpeg', 'image/jpeg', '9704', '2018-03-09 14:10:02', '2018-03-09 14:10:02', '1');
INSERT INTO `files` VALUES ('147', '5aa2368f69e0a_1520580239_20180309.jpg', 'files/decision/2018/03/5aa2368f69e0a_1520580239_20180309.jpg', 'files/decision/2018/03/5aa2368f69e0a_1520580239_20180309.jpg', 'image/jpeg', '595284', '2018-03-09 14:23:59', '2018-03-09 14:23:59', '1');
INSERT INTO `files` VALUES ('148', '5aa2379d5c714_1520580509_20180309.jpg', 'files/decision/2018/03/5aa2379d5c714_1520580509_20180309.jpg', 'files/decision/2018/03/5aa2379d5c714_1520580509_20180309.jpg', 'image/jpeg', '845941', '2018-03-09 14:28:29', '2018-03-09 14:28:29', '1');
INSERT INTO `files` VALUES ('149', '5aa237c693c9a_1520580550_20180309.jpg', 'files/decision/2018/03/5aa237c693c9a_1520580550_20180309.jpg', 'files/decision/2018/03/5aa237c693c9a_1520580550_20180309.jpg', 'image/jpeg', '845941', '2018-03-09 14:29:10', '2018-03-09 14:29:10', '1');
INSERT INTO `files` VALUES ('150', '5aa238a357a35_1520580771_20180309.jpg', 'files/decision/2018/03/5aa238a357a35_1520580771_20180309.jpg', 'files/decision/2018/03/5aa238a357a35_1520580771_20180309.jpg', 'image/jpeg', '845941', '2018-03-09 14:32:51', '2018-03-09 14:32:51', '1');
INSERT INTO `files` VALUES ('151', '5aa2395c9580d_1520580956_20180309.jpg', 'files/decision/2018/03/5aa2395c9580d_1520580956_20180309.jpg', 'files/decision/2018/03/5aa2395c9580d_1520580956_20180309.jpg', 'image/jpeg', '595284', '2018-03-09 14:35:56', '2018-03-09 14:35:56', '1');
INSERT INTO `files` VALUES ('152', '5aa23b35c76d2_1520581429_20180309.mp4', 'files/decision/2018/03/5aa23b35c76d2_1520581429_20180309.mp4', 'files/decision/2018/03/5aa23b35c76d2_1520581429_20180309.mp4', 'video/mp4', '1892700', '2018-03-09 14:43:49', '2018-03-09 14:43:49', '1');
INSERT INTO `files` VALUES ('153', '5aa23c2cb1a2e_1520581676_20180309.jpg', 'files/decision/2018/03/5aa23c2cb1a2e_1520581676_20180309.jpg', 'files/decision/2018/03/5aa23c2cb1a2e_1520581676_20180309.jpg', 'image/jpeg', '879394', '2018-03-09 14:47:56', '2018-03-09 14:47:56', '1');
INSERT INTO `files` VALUES ('154', '5aa23c4c6581e_1520581708_20180309.jpg', 'files/decision/2018/03/5aa23c4c6581e_1520581708_20180309.jpg', 'files/decision/2018/03/5aa23c4c6581e_1520581708_20180309.jpg', 'image/jpeg', '879394', '2018-03-09 14:48:28', '2018-03-09 14:48:28', '1');
INSERT INTO `files` VALUES ('155', '5aa23cadcd00f_1520581805_20180309.jpg', 'files/decision/2018/03/5aa23cadcd00f_1520581805_20180309.jpg', 'files/decision/2018/03/5aa23cadcd00f_1520581805_20180309.jpg', 'image/jpeg', '595284', '2018-03-09 14:50:05', '2018-03-09 14:50:05', '1');
INSERT INTO `files` VALUES ('156', '5aa335a9c9f04_1520645545_20180310.jpg', 'files/decision/2018/03/5aa335a9c9f04_1520645545_20180310.jpg', 'files/decision/2018/03/5aa335a9c9f04_1520645545_20180310.jpg', 'image/jpeg', '9188', '2018-03-10 08:32:25', '2018-03-10 08:32:25', '1');
INSERT INTO `files` VALUES ('157', 'nuocmam.jpg', '/var/www/html/public/files/collection/2018/03/10/img_1520647994_5aa33f3a725ab0.20033195.jpeg', 'collection/2018/03/10/img_1520647994_5aa33f3a725ab0.20033195.jpeg', 'image/jpeg', '9188', '2018-03-10 09:13:14', '2018-03-10 09:13:14', '1');
INSERT INTO `files` VALUES ('158', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/10/img_1520666850_5aa388e2407c94.18538097.jpeg', 'collection/2018/03/10/img_1520666850_5aa388e2407c94.18538097.jpeg', 'image/jpeg', '845941', '2018-03-10 14:27:30', '2018-03-10 14:27:30', '1');
INSERT INTO `files` VALUES ('159', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/10/img_1520667124_5aa389f4c405e1.87659274.jpeg', 'collection/2018/03/10/img_1520667124_5aa389f4c405e1.87659274.jpeg', 'image/jpeg', '845941', '2018-03-10 14:32:04', '2018-03-10 14:32:04', '1');
INSERT INTO `files` VALUES ('160', 'girl-bikini-4.jpg', '/var/www/html/public/files/collection/2018/03/11/img_1520786114_5aa55ac251b5c9.62013960.jpeg', 'collection/2018/03/11/img_1520786114_5aa55ac251b5c9.62013960.jpeg', 'image/jpeg', '37501', '2018-03-11 23:35:14', '2018-03-11 23:35:14', '1');
INSERT INTO `files` VALUES ('161', 'girl-bikini-4.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520820381_5aa5e09dd7f246.25060163.jpeg', 'collection/2018/03/12/img_1520820381_5aa5e09dd7f246.25060163.jpeg', 'image/jpeg', '37501', '2018-03-12 09:06:21', '2018-03-12 09:06:21', '1');
INSERT INTO `files` VALUES ('162', 'girl-bikini-4.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520820592_5aa5e170cbf3f5.94477133.jpeg', 'collection/2018/03/12/img_1520820592_5aa5e170cbf3f5.94477133.jpeg', 'image/jpeg', '37501', '2018-03-12 09:09:52', '2018-03-12 09:09:52', '1');
INSERT INTO `files` VALUES ('163', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/12/img_1520824009_5aa5eec9d528d2.10330981.png', 'collection/2018/03/12/img_1520824009_5aa5eec9d528d2.10330981.png', 'image/png', '98765', '2018-03-12 10:06:49', '2018-03-12 10:06:49', '1');
INSERT INTO `files` VALUES ('164', 'moi cau.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520824024_5aa5eed88f5bc8.16742004.jpeg', 'collection/2018/03/12/img_1520824024_5aa5eed88f5bc8.16742004.jpeg', 'image/jpeg', '452536', '2018-03-12 10:07:04', '2018-03-12 10:07:04', '1');
INSERT INTO `files` VALUES ('165', 'mat-bang-biet-thu-1024x683.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825011_5aa5f2b38129b8.09329099.jpeg', 'collection/2018/03/12/img_1520825011_5aa5f2b38129b8.09329099.jpeg', 'image/jpeg', '129581', '2018-03-12 10:23:31', '2018-03-12 10:23:31', '1');
INSERT INTO `files` VALUES ('166', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825338_5aa5f3fa1c05a8.34990960.jpeg', 'collection/2018/03/12/img_1520825338_5aa5f3fa1c05a8.34990960.jpeg', 'image/jpeg', '1806735', '2018-03-12 10:28:58', '2018-03-12 10:28:58', '1');
INSERT INTO `files` VALUES ('167', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825526_5aa5f4b6890063.11018833.jpeg', 'collection/2018/03/12/img_1520825526_5aa5f4b6890063.11018833.jpeg', 'image/jpeg', '1806735', '2018-03-12 10:32:06', '2018-03-12 10:32:06', '1');
INSERT INTO `files` VALUES ('168', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825550_5aa5f4ce8eb7f6.84416229.jpeg', 'collection/2018/03/12/img_1520825550_5aa5f4ce8eb7f6.84416229.jpeg', 'image/jpeg', '1806735', '2018-03-12 10:32:30', '2018-03-12 10:32:30', '1');
INSERT INTO `files` VALUES ('169', '20170421_155954.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825740_5aa5f58c1700c6.32845773.jpeg', 'collection/2018/03/12/img_1520825740_5aa5f58c1700c6.32845773.jpeg', 'image/jpeg', '2801905', '2018-03-12 10:35:40', '2018-03-12 10:35:40', '1');
INSERT INTO `files` VALUES ('170', '5aa5f627041c7_1520825895_20180312.png', 'files/decision/2018/03/5aa5f627041c7_1520825895_20180312.png', 'files/decision/2018/03/5aa5f627041c7_1520825895_20180312.png', 'image/png', '362909', '2018-03-12 10:38:15', '2018-03-12 10:38:15', '1');
INSERT INTO `files` VALUES ('171', '5aa5f64e1132b_1520825934_20180312.png', 'files/decision/2018/03/5aa5f64e1132b_1520825934_20180312.png', 'files/decision/2018/03/5aa5f64e1132b_1520825934_20180312.png', 'image/png', '362909', '2018-03-12 10:38:54', '2018-03-12 10:38:54', '1');
INSERT INTO `files` VALUES ('172', '5aa5f884ccdaf_1520826500_20180312.png', 'files/decision/2018/03/5aa5f884ccdaf_1520826500_20180312.png', 'files/decision/2018/03/5aa5f884ccdaf_1520826500_20180312.png', 'image/png', '362909', '2018-03-12 10:48:20', '2018-03-12 10:48:20', '1');
INSERT INTO `files` VALUES ('173', '5aa5f941397f3_1520826689_20180312.mp4', 'files/decision/2018/03/5aa5f941397f3_1520826689_20180312.mp4', 'files/decision/2018/03/5aa5f941397f3_1520826689_20180312.mp4', 'video/mp4', '1385735', '2018-03-12 10:51:29', '2018-03-12 10:51:29', '1');
INSERT INTO `files` VALUES ('174', '5aa5fd8262464_1520827778_20180312.exe', 'files/decision/2018/03/5aa5fd8262464_1520827778_20180312.exe', 'files/decision/2018/03/5aa5fd8262464_1520827778_20180312.exe', 'application/x-dosexec', '54272', '2018-03-12 11:09:38', '2018-03-12 11:09:38', '1');
INSERT INTO `files` VALUES ('175', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/12/img_1520829929_5aa605e93aa0a6.29410782.png', 'collection/2018/03/12/img_1520829929_5aa605e93aa0a6.29410782.png', 'image/png', '98765', '2018-03-12 11:45:29', '2018-03-12 11:45:29', '1');
INSERT INTO `files` VALUES ('176', '20170421_155726.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520830278_5aa60746b679e9.13138608.jpeg', 'collection/2018/03/12/img_1520830278_5aa60746b679e9.13138608.jpeg', 'image/jpeg', '3938333', '2018-03-12 11:51:19', '2018-03-12 11:51:19', '1');
INSERT INTO `files` VALUES ('177', '5aa620c993899_1520836809_20180312.png', 'files/decision/2018/03/5aa620c993899_1520836809_20180312.png', 'files/decision/2018/03/5aa620c993899_1520836809_20180312.png', 'image/png', '185324', '2018-03-12 13:40:09', '2018-03-12 13:40:09', '1');
INSERT INTO `files` VALUES ('178', '20170421_155726.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520837202_5aa622524a4552.42640908.jpeg', 'collection/2018/03/12/img_1520837202_5aa622524a4552.42640908.jpeg', 'image/jpeg', '3938333', '2018-03-12 13:46:42', '2018-03-12 13:46:42', '1');
INSERT INTO `files` VALUES ('179', '5aa624014f034_1520837633_20180312.png', 'files/decision/2018/03/5aa624014f034_1520837633_20180312.png', 'files/decision/2018/03/5aa624014f034_1520837633_20180312.png', 'image/png', '362909', '2018-03-12 13:53:53', '2018-03-12 13:53:53', '1');
INSERT INTO `files` VALUES ('180', '20170421_155954.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520838247_5aa62668005810.80383000.jpeg', 'collection/2018/03/12/img_1520838247_5aa62668005810.80383000.jpeg', 'image/jpeg', '2801905', '2018-03-12 14:04:08', '2018-03-12 14:04:08', '1');
INSERT INTO `files` VALUES ('181', 'cover.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520839618_5aa62bc2a2f2e6.95922032.jpeg', 'collection/2018/03/12/img_1520839618_5aa62bc2a2f2e6.95922032.jpeg', 'image/jpeg', '3960829', '2018-03-12 14:26:59', '2018-03-12 14:26:59', '1');
INSERT INTO `files` VALUES ('182', 'IMG_6492.JPG', '/var/www/html/public/files/collection/2018/03/12/img_1520840435_5aa62ef3af07e7.37981969.jpeg', 'collection/2018/03/12/img_1520840435_5aa62ef3af07e7.37981969.jpeg', 'image/jpeg', '3017162', '2018-03-12 14:40:36', '2018-03-12 14:40:36', '1');
INSERT INTO `files` VALUES ('183', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/12/img_1520841181_5aa631dd871cb0.81085817.png', 'collection/2018/03/12/img_1520841181_5aa631dd871cb0.81085817.png', 'image/png', '362909', '2018-03-12 14:53:01', '2018-03-12 14:53:01', '1');
INSERT INTO `files` VALUES ('184', 'Benh vien nhiet doi.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520841252_5aa632245c55e3.08665471.jpeg', 'collection/2018/03/12/img_1520841252_5aa632245c55e3.08665471.jpeg', 'image/jpeg', '2539128', '2018-03-12 14:54:12', '2018-03-12 14:54:12', '1');
INSERT INTO `files` VALUES ('185', '20170421_155954.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520841452_5aa632ec5c75c1.33950061.jpeg', 'collection/2018/03/12/img_1520841452_5aa632ec5c75c1.33950061.jpeg', 'image/jpeg', '2801905', '2018-03-12 14:57:32', '2018-03-12 14:57:32', '1');
INSERT INTO `files` VALUES ('186', 'cover.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520841643_5aa633ab1ea1f5.39662048.jpeg', 'collection/2018/03/12/img_1520841643_5aa633ab1ea1f5.39662048.jpeg', 'image/jpeg', '3960829', '2018-03-12 15:00:43', '2018-03-12 15:00:43', '1');
INSERT INTO `files` VALUES ('187', 'Screenshot from 2018-02-11 01-16-37.png', '/var/www/html/public/files/collection/2018/03/12/img_1520841954_5aa634e233f7a8.11462606.png', 'collection/2018/03/12/img_1520841954_5aa634e233f7a8.11462606.png', 'image/png', '31429', '2018-03-12 15:05:54', '2018-03-12 15:05:54', '1');
INSERT INTO `files` VALUES ('188', 'Screenshot from 2018-01-11 22-32-15.png', '/var/www/html/public/files/collection/2018/03/12/img_1520842313_5aa63649307cc6.91399541.png', 'collection/2018/03/12/img_1520842313_5aa63649307cc6.91399541.png', 'image/png', '19066', '2018-03-12 15:11:53', '2018-03-12 15:11:53', '1');
INSERT INTO `files` VALUES ('189', '5aa638d67b668_1520842966_20180312.png', 'files/decision/2018/03/5aa638d67b668_1520842966_20180312.png', 'files/decision/2018/03/5aa638d67b668_1520842966_20180312.png', 'image/png', '362909', '2018-03-12 15:22:46', '2018-03-12 15:22:46', '1');
INSERT INTO `files` VALUES ('190', '5aa641f298a7d_1520845298_20180312.png', 'files/decision/2018/03/5aa641f298a7d_1520845298_20180312.png', 'files/decision/2018/03/5aa641f298a7d_1520845298_20180312.png', 'image/png', '362909', '2018-03-12 16:01:38', '2018-03-12 16:01:38', '1');
INSERT INTO `files` VALUES ('191', '5aa641fcae0cd_1520845308_20180312.png', 'files/decision/2018/03/5aa641fcae0cd_1520845308_20180312.png', 'files/decision/2018/03/5aa641fcae0cd_1520845308_20180312.png', 'image/png', '362909', '2018-03-12 16:01:48', '2018-03-12 16:01:48', '1');
INSERT INTO `files` VALUES ('192', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/12/img_1520846677_5aa647559d2656.87996344.png', 'collection/2018/03/12/img_1520846677_5aa647559d2656.87996344.png', 'image/png', '15927', '2018-03-12 16:24:37', '2018-03-12 16:24:37', '1');
INSERT INTO `files` VALUES ('193', '20180224_213716.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520846704_5aa64770659461.73705221.jpeg', 'collection/2018/03/12/img_1520846704_5aa64770659461.73705221.jpeg', 'image/jpeg', '2292385', '2018-03-12 16:25:04', '2018-03-12 16:25:04', '1');
INSERT INTO `files` VALUES ('194', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/12/img_1520846893_5aa6482daf5ba3.20341892.png', 'collection/2018/03/12/img_1520846893_5aa6482daf5ba3.20341892.png', 'image/png', '362909', '2018-03-12 16:28:13', '2018-03-12 16:28:13', '1');
INSERT INTO `files` VALUES ('195', '20180224_213716.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520846903_5aa648372ddc10.92822141.jpeg', 'collection/2018/03/12/img_1520846903_5aa648372ddc10.92822141.jpeg', 'image/jpeg', '2292385', '2018-03-12 16:28:23', '2018-03-12 16:28:23', '1');
INSERT INTO `files` VALUES ('196', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/12/img_1520847355_5aa649fb7a3939.83603813.png', 'collection/2018/03/12/img_1520847355_5aa649fb7a3939.83603813.png', 'image/png', '15927', '2018-03-12 16:35:55', '2018-03-12 16:35:55', '1');
INSERT INTO `files` VALUES ('197', '5aa655d86542b_1520850392_20180312.png', 'files/decision/2018/03/5aa655d86542b_1520850392_20180312.png', 'files/decision/2018/03/5aa655d86542b_1520850392_20180312.png', 'image/png', '362909', '2018-03-12 17:26:32', '2018-03-12 17:26:32', '1');
INSERT INTO `files` VALUES ('198', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520907817_5aa736299ea2a8.76562326.jpeg', 'collection/2018/03/13/img_1520907817_5aa736299ea2a8.76562326.jpeg', 'image/jpeg', '1806735', '2018-03-13 09:23:37', '2018-03-13 09:23:37', '1');
INSERT INTO `files` VALUES ('199', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520909827_5aa73e031a8872.78818463.jpeg', 'collection/2018/03/13/img_1520909827_5aa73e031a8872.78818463.jpeg', 'image/jpeg', '1806735', '2018-03-13 09:57:07', '2018-03-13 09:57:07', '1');
INSERT INTO `files` VALUES ('200', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520910071_5aa73ef73e9a37.58212534.jpeg', 'collection/2018/03/13/img_1520910071_5aa73ef73e9a37.58212534.jpeg', 'image/jpeg', '1806735', '2018-03-13 10:01:11', '2018-03-13 10:01:11', '1');
INSERT INTO `files` VALUES ('201', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520910196_5aa73f74c77975.74480818.png', 'collection/2018/03/13/img_1520910196_5aa73f74c77975.74480818.png', 'image/png', '362909', '2018-03-13 10:03:16', '2018-03-13 10:03:16', '1');
INSERT INTO `files` VALUES ('202', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520910601_5aa741091ede56.60651325.png', 'collection/2018/03/13/img_1520910601_5aa741091ede56.60651325.png', 'image/png', '362909', '2018-03-13 10:10:01', '2018-03-13 10:10:01', '1');
INSERT INTO `files` VALUES ('203', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520910634_5aa7412a0c2a93.69600324.png', 'collection/2018/03/13/img_1520910634_5aa7412a0c2a93.69600324.png', 'image/png', '362909', '2018-03-13 10:10:34', '2018-03-13 10:10:34', '1');
INSERT INTO `files` VALUES ('204', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520910754_5aa741a2063095.01134530.jpeg', 'collection/2018/03/13/img_1520910754_5aa741a2063095.01134530.jpeg', 'image/jpeg', '1806735', '2018-03-13 10:12:34', '2018-03-13 10:12:34', '1');
INSERT INTO `files` VALUES ('205', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520912102_5aa746e6aa7ad6.23260536.jpeg', 'collection/2018/03/13/img_1520912102_5aa746e6aa7ad6.23260536.jpeg', 'image/jpeg', '1806735', '2018-03-13 10:35:02', '2018-03-13 10:35:02', '1');
INSERT INTO `files` VALUES ('206', '5aa74f0cdb888_1520914188_20180313.pdf', 'files/decision/2018/03/5aa74f0cdb888_1520914188_20180313.pdf', 'files/decision/2018/03/5aa74f0cdb888_1520914188_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:09:48', '2018-03-13 11:09:48', '1');
INSERT INTO `files` VALUES ('207', '5aa7509d7ae9d_1520914589_20180313.pdf', 'files/decision/2018/03/5aa7509d7ae9d_1520914589_20180313.pdf', 'files/decision/2018/03/5aa7509d7ae9d_1520914589_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:16:29', '2018-03-13 11:16:29', '1');
INSERT INTO `files` VALUES ('208', '5aa750b0e4fb0_1520914608_20180313.png', 'files/decision/2018/03/5aa750b0e4fb0_1520914608_20180313.png', 'files/decision/2018/03/5aa750b0e4fb0_1520914608_20180313.png', 'image/png', '163127', '2018-03-13 11:16:48', '2018-03-13 11:16:48', '1');
INSERT INTO `files` VALUES ('209', '5aa750bd5ec74_1520914621_20180313.png', 'files/decision/2018/03/5aa750bd5ec74_1520914621_20180313.png', 'files/decision/2018/03/5aa750bd5ec74_1520914621_20180313.png', 'image/png', '102121', '2018-03-13 11:17:01', '2018-03-13 11:17:01', '1');
INSERT INTO `files` VALUES ('210', '5aa7521ae41c7_1520914970_20180313.jpg', 'files/decision/2018/03/5aa7521ae41c7_1520914970_20180313.jpg', 'files/decision/2018/03/5aa7521ae41c7_1520914970_20180313.jpg', 'video/mp4', '1385735', '2018-03-13 11:21:07', '2018-03-13 11:22:50', '1');
INSERT INTO `files` VALUES ('211', '5aa7549ef1dbf_1520915614_20180313.pdf', 'files/decision/2018/03/5aa7549ef1dbf_1520915614_20180313.pdf', 'files/decision/2018/03/5aa7549ef1dbf_1520915614_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:33:34', '2018-03-13 11:33:34', '1');
INSERT INTO `files` VALUES ('212', '5aa754b10691c_1520915633_20180313.pdf', 'files/decision/2018/03/5aa754b10691c_1520915633_20180313.pdf', 'files/decision/2018/03/5aa754b10691c_1520915633_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:33:53', '2018-03-13 11:33:53', '1');
INSERT INTO `files` VALUES ('213', '5aa754bf3f2bb_1520915647_20180313.pdf', 'files/decision/2018/03/5aa754bf3f2bb_1520915647_20180313.pdf', 'files/decision/2018/03/5aa754bf3f2bb_1520915647_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:34:07', '2018-03-13 11:34:07', '1');
INSERT INTO `files` VALUES ('214', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520916141_5aa756ad53b774.87680943.png', 'collection/2018/03/13/img_1520916141_5aa756ad53b774.87680943.png', 'image/png', '362909', '2018-03-13 11:42:21', '2018-03-13 11:42:21', '1');
INSERT INTO `files` VALUES ('215', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520924242_5aa776520efc94.72533112.png', 'collection/2018/03/13/img_1520924242_5aa776520efc94.72533112.png', 'image/png', '362909', '2018-03-13 13:57:22', '2018-03-13 13:57:22', '1');
INSERT INTO `files` VALUES ('216', 'Benh vien nhiet doi.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520924322_5aa776a27461f1.96027366.jpeg', 'collection/2018/03/13/img_1520924322_5aa776a27461f1.96027366.jpeg', 'image/jpeg', '2539128', '2018-03-13 13:58:42', '2018-03-13 13:58:42', '1');
INSERT INTO `files` VALUES ('217', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520925126_5aa779c62515f7.62061228.png', 'collection/2018/03/13/img_1520925126_5aa779c62515f7.62061228.png', 'image/png', '362909', '2018-03-13 14:12:06', '2018-03-13 14:12:06', '1');
INSERT INTO `files` VALUES ('218', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520925411_5aa77ae338ecc5.08126084.png', 'collection/2018/03/13/img_1520925411_5aa77ae338ecc5.08126084.png', 'image/png', '362909', '2018-03-13 14:16:51', '2018-03-13 14:16:51', '1');
INSERT INTO `files` VALUES ('219', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520925624_5aa77bb8c88a67.25408616.png', 'collection/2018/03/13/img_1520925624_5aa77bb8c88a67.25408616.png', 'image/png', '362909', '2018-03-13 14:20:24', '2018-03-13 14:20:24', '1');
INSERT INTO `files` VALUES ('220', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520925662_5aa77bde6d8a10.88546578.png', 'collection/2018/03/13/img_1520925662_5aa77bde6d8a10.88546578.png', 'image/png', '362909', '2018-03-13 14:21:02', '2018-03-13 14:21:02', '1');
INSERT INTO `files` VALUES ('221', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520925796_5aa77c64b53198.22224956.jpeg', 'collection/2018/03/13/img_1520925796_5aa77c64b53198.22224956.jpeg', 'image/jpeg', '1806735', '2018-03-13 14:23:16', '2018-03-13 14:23:16', '1');
INSERT INTO `files` VALUES ('222', '5aa77f1047c79_1520926480_20180313.png', 'files/decision/2018/03/5aa77f1047c79_1520926480_20180313.png', 'files/decision/2018/03/5aa77f1047c79_1520926480_20180313.png', 'image/png', '362909', '2018-03-13 14:34:40', '2018-03-13 14:34:40', '1');
INSERT INTO `files` VALUES ('223', '5aa77f1b3b912_1520926491_20180313.png', 'files/decision/2018/03/5aa77f1b3b912_1520926491_20180313.png', 'files/decision/2018/03/5aa77f1b3b912_1520926491_20180313.png', 'image/png', '362909', '2018-03-13 14:34:51', '2018-03-13 14:34:51', '1');
INSERT INTO `files` VALUES ('224', '5aa77f4360c47_1520926531_20180313.png', 'files/decision/2018/03/5aa77f4360c47_1520926531_20180313.png', 'files/decision/2018/03/5aa77f4360c47_1520926531_20180313.png', 'image/png', '362909', '2018-03-13 14:35:31', '2018-03-13 14:35:31', '1');
INSERT INTO `files` VALUES ('225', '5aa77f4e4ed04_1520926542_20180313.png', 'files/decision/2018/03/5aa77f4e4ed04_1520926542_20180313.png', 'files/decision/2018/03/5aa77f4e4ed04_1520926542_20180313.png', 'image/png', '362909', '2018-03-13 14:35:42', '2018-03-13 14:35:42', '1');
INSERT INTO `files` VALUES ('226', '5aa77f59bae2a_1520926553_20180313.png', 'files/decision/2018/03/5aa77f59bae2a_1520926553_20180313.png', 'files/decision/2018/03/5aa77f59bae2a_1520926553_20180313.png', 'image/png', '362909', '2018-03-13 14:35:53', '2018-03-13 14:35:53', '1');
INSERT INTO `files` VALUES ('227', '5aa77f69c36cf_1520926569_20180313.png', 'files/decision/2018/03/5aa77f69c36cf_1520926569_20180313.png', 'files/decision/2018/03/5aa77f69c36cf_1520926569_20180313.png', 'image/png', '362909', '2018-03-13 14:36:09', '2018-03-13 14:36:09', '1');
INSERT INTO `files` VALUES ('228', '5aa77f763f0f6_1520926582_20180313.png', 'files/decision/2018/03/5aa77f763f0f6_1520926582_20180313.png', 'files/decision/2018/03/5aa77f763f0f6_1520926582_20180313.png', 'image/png', '362909', '2018-03-13 14:36:22', '2018-03-13 14:36:22', '1');
INSERT INTO `files` VALUES ('229', '5aa77f8da6c04_1520926605_20180313.png', 'files/decision/2018/03/5aa77f8da6c04_1520926605_20180313.png', 'files/decision/2018/03/5aa77f8da6c04_1520926605_20180313.png', 'image/png', '362909', '2018-03-13 14:36:45', '2018-03-13 14:36:45', '1');
INSERT INTO `files` VALUES ('230', '5aa782777deb7_1520927351_20180313.png', 'files/decision/2018/03/5aa782777deb7_1520927351_20180313.png', 'files/decision/2018/03/5aa782777deb7_1520927351_20180313.png', 'image/png', '362909', '2018-03-13 14:49:11', '2018-03-13 14:49:11', '1');
INSERT INTO `files` VALUES ('231', '5aa783448a9f3_1520927556_20180313.png', 'files/decision/2018/03/5aa783448a9f3_1520927556_20180313.png', 'files/decision/2018/03/5aa783448a9f3_1520927556_20180313.png', 'image/png', '362909', '2018-03-13 14:52:36', '2018-03-13 14:52:36', '1');
INSERT INTO `files` VALUES ('232', 'new-field-group.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520954904_5aa7ee18e17c42.56431147.jpeg', 'collection/2018/03/13/img_1520954904_5aa7ee18e17c42.56431147.jpeg', 'image/jpeg', '53097', '2018-03-13 22:28:24', '2018-03-13 22:28:24', '1');
INSERT INTO `files` VALUES ('233', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1520995033_5aa88ad9d7e3b4.77550918.png', 'collection/2018/03/14/img_1520995033_5aa88ad9d7e3b4.77550918.png', 'image/png', '98765', '2018-03-14 09:37:13', '2018-03-14 09:37:13', '1');
INSERT INTO `files` VALUES ('234', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1520995370_5aa88c2ad091d4.55577605.png', 'collection/2018/03/14/img_1520995370_5aa88c2ad091d4.55577605.png', 'image/png', '362909', '2018-03-14 09:42:50', '2018-03-14 09:42:50', '1');
INSERT INTO `files` VALUES ('235', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1520998304_5aa897a0b0f934.61956797.png', 'collection/2018/03/14/img_1520998304_5aa897a0b0f934.61956797.png', 'image/png', '362909', '2018-03-14 10:31:44', '2018-03-14 10:31:44', '1');
INSERT INTO `files` VALUES ('236', '5aa897a1c53ac_1520998305_20180314.pdf', 'files/decision/2018/03/5aa897a1c53ac_1520998305_20180314.pdf', 'files/decision/2018/03/5aa897a1c53ac_1520998305_20180314.pdf', 'application/pdf', '64955', '2018-03-14 10:31:45', '2018-03-14 10:31:45', '1');
INSERT INTO `files` VALUES ('237', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1520998448_5aa89830333874.71676028.png', 'collection/2018/03/14/img_1520998448_5aa89830333874.71676028.png', 'image/png', '362909', '2018-03-14 10:34:08', '2018-03-14 10:34:08', '1');
INSERT INTO `files` VALUES ('238', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1520998492_5aa8985d0022a8.21722310.jpeg', 'collection/2018/03/14/img_1520998492_5aa8985d0022a8.21722310.jpeg', 'image/jpeg', '1806735', '2018-03-14 10:34:53', '2018-03-14 10:34:53', '1');
INSERT INTO `files` VALUES ('239', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1520998764_5aa8996cd76048.57500578.jpeg', 'collection/2018/03/14/img_1520998764_5aa8996cd76048.57500578.jpeg', 'image/jpeg', '1806735', '2018-03-14 10:39:25', '2018-03-14 10:39:25', '1');
INSERT INTO `files` VALUES ('240', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1520999922_5aa89df264c614.53045359.png', 'collection/2018/03/14/img_1520999922_5aa89df264c614.53045359.png', 'image/png', '362909', '2018-03-14 10:58:42', '2018-03-14 10:58:42', '1');
INSERT INTO `files` VALUES ('241', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000074_5aa89e8ab8c867.46209850.jpeg', 'collection/2018/03/14/img_1521000074_5aa89e8ab8c867.46209850.jpeg', 'image/jpeg', '1806735', '2018-03-14 11:01:14', '2018-03-14 11:01:14', '1');
INSERT INTO `files` VALUES ('242', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000092_5aa89e9cab2b26.23505432.png', 'collection/2018/03/14/img_1521000092_5aa89e9cab2b26.23505432.png', 'image/png', '362909', '2018-03-14 11:01:32', '2018-03-14 11:01:32', '1');
INSERT INTO `files` VALUES ('243', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000157_5aa89edd3e2789.18421697.png', 'collection/2018/03/14/img_1521000157_5aa89edd3e2789.18421697.png', 'image/png', '362909', '2018-03-14 11:02:37', '2018-03-14 11:02:37', '1');
INSERT INTO `files` VALUES ('244', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000293_5aa89f6505ab40.45438842.jpeg', 'collection/2018/03/14/img_1521000293_5aa89f6505ab40.45438842.jpeg', 'image/jpeg', '1806735', '2018-03-14 11:04:53', '2018-03-14 11:04:53', '1');
INSERT INTO `files` VALUES ('245', 'Benh vien nhiet doi.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000303_5aa89f6f1dde48.51656426.jpeg', 'collection/2018/03/14/img_1521000303_5aa89f6f1dde48.51656426.jpeg', 'image/jpeg', '2539128', '2018-03-14 11:05:03', '2018-03-14 11:05:03', '1');
INSERT INTO `files` VALUES ('246', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000306_5aa89f721a09c5.11702751.png', 'collection/2018/03/14/img_1521000306_5aa89f721a09c5.11702751.png', 'image/png', '362909', '2018-03-14 11:05:06', '2018-03-14 11:05:06', '1');
INSERT INTO `files` VALUES ('247', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000499_5aa8a033538b97.16579189.png', 'collection/2018/03/14/img_1521000499_5aa8a033538b97.16579189.png', 'image/png', '362909', '2018-03-14 11:08:19', '2018-03-14 11:08:19', '1');
INSERT INTO `files` VALUES ('248', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000510_5aa8a03e918673.63219191.png', 'collection/2018/03/14/img_1521000510_5aa8a03e918673.63219191.png', 'image/png', '362909', '2018-03-14 11:08:30', '2018-03-14 11:08:30', '1');
INSERT INTO `files` VALUES ('249', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000611_5aa8a0a3be9418.75209168.png', 'collection/2018/03/14/img_1521000611_5aa8a0a3be9418.75209168.png', 'image/png', '362909', '2018-03-14 11:10:11', '2018-03-14 11:10:11', '1');
INSERT INTO `files` VALUES ('250', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000718_5aa8a10e36ca45.30924792.png', 'collection/2018/03/14/img_1521000718_5aa8a10e36ca45.30924792.png', 'image/png', '362909', '2018-03-14 11:11:58', '2018-03-14 11:11:58', '1');
INSERT INTO `files` VALUES ('251', '20170421_155726.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000776_5aa8a148b02fa4.58657616.jpeg', 'collection/2018/03/14/img_1521000776_5aa8a148b02fa4.58657616.jpeg', 'image/jpeg', '3938333', '2018-03-14 11:12:56', '2018-03-14 11:12:56', '1');
INSERT INTO `files` VALUES ('252', '20170421_155954.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000979_5aa8a213a90524.43442784.jpeg', 'collection/2018/03/14/img_1521000979_5aa8a213a90524.43442784.jpeg', 'image/jpeg', '2801905', '2018-03-14 11:16:19', '2018-03-14 11:16:19', '1');
INSERT INTO `files` VALUES ('253', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521001094_5aa8a286a6e802.64848611.png', 'collection/2018/03/14/img_1521001094_5aa8a286a6e802.64848611.png', 'image/png', '98765', '2018-03-14 11:18:14', '2018-03-14 11:18:14', '1');
INSERT INTO `files` VALUES ('254', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521001300_5aa8a354bf9383.26169011.png', 'collection/2018/03/14/img_1521001300_5aa8a354bf9383.26169011.png', 'image/png', '98765', '2018-03-14 11:21:40', '2018-03-14 11:21:40', '1');
INSERT INTO `files` VALUES ('255', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521001418_5aa8a3ca7cb000.08155502.png', 'collection/2018/03/14/img_1521001418_5aa8a3ca7cb000.08155502.png', 'image/png', '98765', '2018-03-14 11:23:38', '2018-03-14 11:23:38', '1');
INSERT INTO `files` VALUES ('256', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521001456_5aa8a3f0885c33.93377903.png', 'collection/2018/03/14/img_1521001456_5aa8a3f0885c33.93377903.png', 'image/png', '98765', '2018-03-14 11:24:16', '2018-03-14 11:24:16', '1');
INSERT INTO `files` VALUES ('257', '5aa8d0285e502_1521012776_20180314.png', 'files/decision/2018/03/5aa8d0285e502_1521012776_20180314.png', 'files/decision/2018/03/5aa8d0285e502_1521012776_20180314.png', 'image/png', '98765', '2018-03-14 14:32:56', '2018-03-14 14:32:56', '1');
INSERT INTO `files` VALUES ('258', '5aa8d32ac27e2_1521013546_20180314.png', 'files/decision/2018/03/5aa8d32ac27e2_1521013546_20180314.png', 'files/decision/2018/03/5aa8d32ac27e2_1521013546_20180314.png', 'image/png', '98765', '2018-03-14 14:45:46', '2018-03-14 14:45:46', '1');
INSERT INTO `files` VALUES ('259', '5aa8d536787de_1521014070_20180314.jpg', 'files/decision/2018/03/5aa8d536787de_1521014070_20180314.jpg', 'files/decision/2018/03/5aa8d536787de_1521014070_20180314.jpg', 'image/jpeg', '452536', '2018-03-14 14:54:30', '2018-03-14 14:54:30', '1');
INSERT INTO `files` VALUES ('260', '5aa8d61d0f5b1_1521014301_20180314.jpg', 'files/decision/2018/03/5aa8d61d0f5b1_1521014301_20180314.jpg', 'files/decision/2018/03/5aa8d61d0f5b1_1521014301_20180314.jpg', 'image/jpeg', '452536', '2018-03-14 14:58:21', '2018-03-14 14:58:21', '1');
INSERT INTO `files` VALUES ('261', '5aa8d89a75a77_1521014938_20180314.jpg', 'files/decision/2018/03/5aa8d89a75a77_1521014938_20180314.jpg', 'files/decision/2018/03/5aa8d89a75a77_1521014938_20180314.jpg', 'image/jpeg', '452536', '2018-03-14 15:05:45', '2018-03-14 15:08:58', '1');
INSERT INTO `files` VALUES ('262', '5aa8d9bc6a5cf_1521015228_20180314.png', 'files/decision/2018/03/5aa8d9bc6a5cf_1521015228_20180314.png', 'files/decision/2018/03/5aa8d9bc6a5cf_1521015228_20180314.png', 'image/png', '98765', '2018-03-14 15:13:48', '2018-03-14 15:13:48', '1');
INSERT INTO `files` VALUES ('263', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521023353_5aa8f979cbdc89.65386207.png', 'collection/2018/03/14/img_1521023353_5aa8f979cbdc89.65386207.png', 'image/png', '98765', '2018-03-14 17:29:13', '2018-03-14 17:29:13', '1');
INSERT INTO `files` VALUES ('264', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521023364_5aa8f9846a29a2.21228407.png', 'collection/2018/03/14/img_1521023364_5aa8f9846a29a2.21228407.png', 'image/png', '362909', '2018-03-14 17:29:24', '2018-03-14 17:29:24', '1');
INSERT INTO `files` VALUES ('265', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521023457_5aa8f9e192b5a3.78287630.png', 'collection/2018/03/14/img_1521023457_5aa8f9e192b5a3.78287630.png', 'image/png', '362909', '2018-03-14 17:30:57', '2018-03-14 17:30:57', '1');
INSERT INTO `files` VALUES ('266', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521081015_5aa9dab7d6bed4.36771609.jpeg', 'collection/2018/03/15/img_1521081015_5aa9dab7d6bed4.36771609.jpeg', 'image/jpeg', '114501', '2018-03-15 09:30:15', '2018-03-15 09:30:15', '1');
INSERT INTO `files` VALUES ('267', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/15/img_1521085962_5aa9ee0ad475d4.46558244.png', 'collection/2018/03/15/img_1521085962_5aa9ee0ad475d4.46558244.png', 'image/png', '15927', '2018-03-15 10:52:42', '2018-03-15 10:52:42', '1');
INSERT INTO `files` VALUES ('268', '1.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521086361_5aa9ef99a931c1.10860793.jpeg', 'collection/2018/03/15/img_1521086361_5aa9ef99a931c1.10860793.jpeg', 'image/jpeg', '138409', '2018-03-15 10:59:21', '2018-03-15 10:59:21', '1');
INSERT INTO `files` VALUES ('269', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521087489_5aa9f401692d25.28679830.jpeg', 'collection/2018/03/15/img_1521087489_5aa9f401692d25.28679830.jpeg', 'image/jpeg', '1806735', '2018-03-15 11:18:09', '2018-03-15 11:18:09', '1');
INSERT INTO `files` VALUES ('270', '1.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521087979_5aa9f5eb5da5c8.99504258.jpeg', 'collection/2018/03/15/img_1521087979_5aa9f5eb5da5c8.99504258.jpeg', 'image/jpeg', '138409', '2018-03-15 11:26:19', '2018-03-15 11:26:19', '1');
INSERT INTO `files` VALUES ('271', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/15/img_1521088297_5aa9f7296fcaf7.17732179.png', 'collection/2018/03/15/img_1521088297_5aa9f7296fcaf7.17732179.png', 'image/png', '362909', '2018-03-15 11:31:37', '2018-03-15 11:31:37', '1');
INSERT INTO `files` VALUES ('272', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/15/img_1521088561_5aa9f8318dfa69.34264909.png', 'collection/2018/03/15/img_1521088561_5aa9f8318dfa69.34264909.png', 'image/png', '362909', '2018-03-15 11:36:01', '2018-03-15 11:36:01', '1');
INSERT INTO `files` VALUES ('273', 'Benh vien nhiet doi.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521088608_5aa9f860021707.21099739.jpeg', 'collection/2018/03/15/img_1521088608_5aa9f860021707.21099739.jpeg', 'image/jpeg', '2539128', '2018-03-15 11:36:48', '2018-03-15 11:36:48', '1');
INSERT INTO `files` VALUES ('274', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/15/img_1521088654_5aa9f88e426f73.60831979.png', 'collection/2018/03/15/img_1521088654_5aa9f88e426f73.60831979.png', 'image/png', '362909', '2018-03-15 11:37:34', '2018-03-15 11:37:34', '1');
INSERT INTO `files` VALUES ('275', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521089366_5aa9fb5672a100.58102938.jpeg', 'collection/2018/03/15/img_1521089366_5aa9fb5672a100.58102938.jpeg', 'image/jpeg', '1806735', '2018-03-15 11:49:26', '2018-03-15 11:49:26', '1');
INSERT INTO `files` VALUES ('276', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521089519_5aa9fbef34ee43.91112308.jpeg', 'collection/2018/03/15/img_1521089519_5aa9fbef34ee43.91112308.jpeg', 'image/jpeg', '1806735', '2018-03-15 11:51:59', '2018-03-15 11:51:59', '1');
INSERT INTO `files` VALUES ('277', 'image1.png', '/var/www/html/public/files/collection/2018/03/15/img_1521089533_5aa9fbfdab41b7.34213664.png', 'collection/2018/03/15/img_1521089533_5aa9fbfdab41b7.34213664.png', 'image/png', '77529', '2018-03-15 11:52:13', '2018-03-15 11:52:13', '1');
INSERT INTO `files` VALUES ('278', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521089540_5aa9fc04cd84a2.49142164.jpeg', 'collection/2018/03/15/img_1521089540_5aa9fc04cd84a2.49142164.jpeg', 'image/jpeg', '1806735', '2018-03-15 11:52:20', '2018-03-15 11:52:20', '1');
INSERT INTO `files` VALUES ('279', '15489771400_688bffd72f_o.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521095690_5aaa140a1fbde8.22984340.jpeg', 'collection/2018/03/15/img_1521095690_5aaa140a1fbde8.22984340.jpeg', 'image/jpeg', '168052', '2018-03-15 13:34:50', '2018-03-15 13:34:50', '1');
INSERT INTO `files` VALUES ('280', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521095722_5aaa142ac1a529.28993798.jpeg', 'collection/2018/03/15/img_1521095722_5aaa142ac1a529.28993798.jpeg', 'image/jpeg', '777835', '2018-03-15 13:35:22', '2018-03-15 13:35:22', '1');
INSERT INTO `files` VALUES ('281', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/15/img_1521096217_5aaa16197fe6a9.26303312.png', 'collection/2018/03/15/img_1521096217_5aaa16197fe6a9.26303312.png', 'image/png', '15927', '2018-03-15 13:43:37', '2018-03-15 13:43:37', '1');
INSERT INTO `files` VALUES ('282', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096303_5aaa166f5b89b7.06286982.jpeg', 'collection/2018/03/15/img_1521096303_5aaa166f5b89b7.06286982.jpeg', 'image/jpeg', '114501', '2018-03-15 13:45:03', '2018-03-15 13:45:03', '1');
INSERT INTO `files` VALUES ('283', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096533_5aaa1755d502f9.89203985.jpeg', 'collection/2018/03/15/img_1521096533_5aaa1755d502f9.89203985.jpeg', 'image/jpeg', '114501', '2018-03-15 13:48:53', '2018-03-15 13:48:53', '1');
INSERT INTO `files` VALUES ('284', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096555_5aaa176b28b3c5.99191496.jpeg', 'collection/2018/03/15/img_1521096555_5aaa176b28b3c5.99191496.jpeg', 'image/jpeg', '114501', '2018-03-15 13:49:15', '2018-03-15 13:49:15', '1');
INSERT INTO `files` VALUES ('285', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096669_5aaa17ddb87994.10141649.jpeg', 'collection/2018/03/15/img_1521096669_5aaa17ddb87994.10141649.jpeg', 'image/jpeg', '114501', '2018-03-15 13:51:09', '2018-03-15 13:51:09', '1');
INSERT INTO `files` VALUES ('286', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096785_5aaa18516c5c95.82943798.jpeg', 'collection/2018/03/15/img_1521096785_5aaa18516c5c95.82943798.jpeg', 'image/jpeg', '114501', '2018-03-15 13:53:05', '2018-03-15 13:53:05', '1');
INSERT INTO `files` VALUES ('287', 'Dau hieu virus 1 contact.png', '/var/www/html/public/files/collection/2018/03/15/img_1521097127_5aaa19a7a8be14.49737443.png', 'collection/2018/03/15/img_1521097127_5aaa19a7a8be14.49737443.png', 'image/png', '63048', '2018-03-15 13:58:47', '2018-03-15 13:58:47', '1');
INSERT INTO `files` VALUES ('288', '2.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521098965_5aaa20d5570f70.46692602.jpeg', 'collection/2018/03/15/img_1521098965_5aaa20d5570f70.46692602.jpeg', 'image/jpeg', '37400', '2018-03-15 14:29:25', '2018-03-15 14:29:25', '1');
INSERT INTO `files` VALUES ('289', '2.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521098991_5aaa20efe7a8d8.49010293.jpeg', 'collection/2018/03/15/img_1521098991_5aaa20efe7a8d8.49010293.jpeg', 'image/jpeg', '37400', '2018-03-15 14:29:51', '2018-03-15 14:29:51', '1');
INSERT INTO `files` VALUES ('290', '2.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521099017_5aaa2109e09aa4.26538832.jpeg', 'collection/2018/03/15/img_1521099017_5aaa2109e09aa4.26538832.jpeg', 'image/jpeg', '37400', '2018-03-15 14:30:17', '2018-03-15 14:30:17', '1');
INSERT INTO `files` VALUES ('291', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/15/img_1521099182_5aaa21ae76da72.44122017.png', 'collection/2018/03/15/img_1521099182_5aaa21ae76da72.44122017.png', 'image/png', '15927', '2018-03-15 14:33:02', '2018-03-15 14:33:02', '1');
INSERT INTO `files` VALUES ('292', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/15/img_1521099437_5aaa22ade093d0.43878124.png', 'collection/2018/03/15/img_1521099437_5aaa22ade093d0.43878124.png', 'image/png', '15927', '2018-03-15 14:37:17', '2018-03-15 14:37:17', '1');
INSERT INTO `files` VALUES ('293', 'new-field-group.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521101693_5aaa2b7d29b8f8.00685716.jpeg', 'collection/2018/03/15/img_1521101693_5aaa2b7d29b8f8.00685716.jpeg', 'image/jpeg', '53097', '2018-03-15 15:14:53', '2018-03-15 15:14:53', '1');
INSERT INTO `files` VALUES ('294', '5aaa38c7b46a2_1521105095_20180315.pdf', 'files/decision/2018/03/5aaa38c7b46a2_1521105095_20180315.pdf', 'files/decision/2018/03/5aaa38c7b46a2_1521105095_20180315.pdf', 'application/pdf', '7945', '2018-03-15 16:11:35', '2018-03-15 16:11:35', '1');
INSERT INTO `files` VALUES ('295', '5aaa390fac4dd_1521105167_20180315.pdf', 'files/decision/2018/03/5aaa390fac4dd_1521105167_20180315.pdf', 'files/decision/2018/03/5aaa390fac4dd_1521105167_20180315.pdf', 'application/pdf', '7945', '2018-03-15 16:12:47', '2018-03-15 16:12:47', '1');
INSERT INTO `files` VALUES ('296', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521105233_5aaa3951ead131.12556780.jpeg', 'collection/2018/03/15/img_1521105233_5aaa3951ead131.12556780.jpeg', 'image/jpeg', '114501', '2018-03-15 16:13:53', '2018-03-15 16:13:53', '1');
INSERT INTO `files` VALUES ('297', '5aaa39529f8d2_1521105234_20180315.png', 'files/decision/2018/03/5aaa39529f8d2_1521105234_20180315.png', 'files/decision/2018/03/5aaa39529f8d2_1521105234_20180315.png', 'image/png', '362909', '2018-03-15 16:13:54', '2018-03-15 16:13:54', '1');
INSERT INTO `files` VALUES ('298', '5aaa397c33757_1521105276_20180315.png', 'files/decision/2018/03/5aaa397c33757_1521105276_20180315.png', 'files/decision/2018/03/5aaa397c33757_1521105276_20180315.png', 'image/png', '362909', '2018-03-15 16:14:36', '2018-03-15 16:14:36', '1');
INSERT INTO `files` VALUES ('299', '5aaa398f42a0a_1521105295_20180315.pdf', 'files/decision/2018/03/5aaa398f42a0a_1521105295_20180315.pdf', 'files/decision/2018/03/5aaa398f42a0a_1521105295_20180315.pdf', 'application/pdf', '7945', '2018-03-15 16:14:55', '2018-03-15 16:14:55', '1');
INSERT INTO `files` VALUES ('300', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521105634_5aaa3ae29770e2.95580527.jpeg', 'collection/2018/03/15/img_1521105634_5aaa3ae29770e2.95580527.jpeg', 'image/jpeg', '114501', '2018-03-15 16:20:34', '2018-03-15 16:20:34', '1');
INSERT INTO `files` VALUES ('301', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521105921_5aaa3c0160c8f4.25210043.jpeg', 'collection/2018/03/15/img_1521105921_5aaa3c0160c8f4.25210043.jpeg', 'image/jpeg', '114501', '2018-03-15 16:25:21', '2018-03-15 16:25:21', '1');
INSERT INTO `files` VALUES ('302', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/15/img_1521106057_5aaa3c89715ce3.22080173.png', 'collection/2018/03/15/img_1521106057_5aaa3c89715ce3.22080173.png', 'image/png', '362909', '2018-03-15 16:27:37', '2018-03-15 16:27:37', '1');
INSERT INTO `files` VALUES ('303', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521106656_5aaa3ee07148a0.82554474.jpeg', 'collection/2018/03/15/img_1521106656_5aaa3ee07148a0.82554474.jpeg', 'image/jpeg', '777835', '2018-03-15 16:37:36', '2018-03-15 16:37:36', '1');
INSERT INTO `files` VALUES ('304', '5aaa411992fa6_1521107225_20180315.png', 'files/decision/2018/03/5aaa411992fa6_1521107225_20180315.png', 'files/decision/2018/03/5aaa411992fa6_1521107225_20180315.png', 'image/png', '362909', '2018-03-15 16:47:05', '2018-03-15 16:47:05', '1');
INSERT INTO `files` VALUES ('305', '5aaa466472076_1521108580_20180315.pdf', 'files/decision/2018/03/5aaa466472076_1521108580_20180315.pdf', 'files/decision/2018/03/5aaa466472076_1521108580_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:09:40', '2018-03-15 17:09:40', '1');
INSERT INTO `files` VALUES ('306', '5aaa46cf88c11_1521108687_20180315.pdf', 'files/decision/2018/03/5aaa46cf88c11_1521108687_20180315.pdf', 'files/decision/2018/03/5aaa46cf88c11_1521108687_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:11:27', '2018-03-15 17:11:27', '1');
INSERT INTO `files` VALUES ('307', '5aaa46db5a225_1521108699_20180315.pdf', 'files/decision/2018/03/5aaa46db5a225_1521108699_20180315.pdf', 'files/decision/2018/03/5aaa46db5a225_1521108699_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:11:39', '2018-03-15 17:11:39', '1');
INSERT INTO `files` VALUES ('308', '5aaa46e7f38c4_1521108711_20180315.pdf', 'files/decision/2018/03/5aaa46e7f38c4_1521108711_20180315.pdf', 'files/decision/2018/03/5aaa46e7f38c4_1521108711_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:11:52', '2018-03-15 17:11:52', '1');
INSERT INTO `files` VALUES ('309', '5aaa470216e96_1521108738_20180315.pdf', 'files/decision/2018/03/5aaa470216e96_1521108738_20180315.pdf', 'files/decision/2018/03/5aaa470216e96_1521108738_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:12:18', '2018-03-15 17:12:18', '1');
INSERT INTO `files` VALUES ('310', '5aaa470d344ff_1521108749_20180315.pdf', 'files/decision/2018/03/5aaa470d344ff_1521108749_20180315.pdf', 'files/decision/2018/03/5aaa470d344ff_1521108749_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:12:29', '2018-03-15 17:12:29', '1');
INSERT INTO `files` VALUES ('311', '5aaa4718ca765_1521108760_20180315.pdf', 'files/decision/2018/03/5aaa4718ca765_1521108760_20180315.pdf', 'files/decision/2018/03/5aaa4718ca765_1521108760_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:12:40', '2018-03-15 17:12:40', '1');
INSERT INTO `files` VALUES ('312', 'new-field-group.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521111158_5aaa50762de761.55702363.jpeg', 'collection/2018/03/15/img_1521111158_5aaa50762de761.55702363.jpeg', 'image/jpeg', '53097', '2018-03-15 17:52:38', '2018-03-15 17:52:38', '1');
INSERT INTO `files` VALUES ('313', 'new-field-group.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521111199_5aaa509f2df997.43021921.jpeg', 'collection/2018/03/15/img_1521111199_5aaa509f2df997.43021921.jpeg', 'image/jpeg', '53097', '2018-03-15 17:53:19', '2018-03-15 17:53:19', '1');
INSERT INTO `files` VALUES ('314', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521167144_5aab2b28cfc735.46161204.jpeg', 'collection/2018/03/16/img_1521167144_5aab2b28cfc735.46161204.jpeg', 'image/jpeg', '114501', '2018-03-16 09:25:44', '2018-03-16 09:25:44', '1');
INSERT INTO `files` VALUES ('315', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521167738_5aab2d7a1fd1e4.21099877.png', 'collection/2018/03/16/img_1521167738_5aab2d7a1fd1e4.21099877.png', 'image/png', '362909', '2018-03-16 09:35:38', '2018-03-16 09:35:38', '1');
INSERT INTO `files` VALUES ('316', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521167765_5aab2d955deb76.02266190.png', 'collection/2018/03/16/img_1521167765_5aab2d955deb76.02266190.png', 'image/png', '362909', '2018-03-16 09:36:05', '2018-03-16 09:36:05', '1');
INSERT INTO `files` VALUES ('317', '5aab3200c5e35_1521168896_20180316.jpg', 'files/decision/2018/03/5aab3200c5e35_1521168896_20180316.jpg', 'files/decision/2018/03/5aab3200c5e35_1521168896_20180316.jpg', 'image/jpeg', '138409', '2018-03-16 09:54:56', '2018-03-16 09:54:56', '1');
INSERT INTO `files` VALUES ('318', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521172808_5aab4148712340.65793374.jpeg', 'collection/2018/03/16/img_1521172808_5aab4148712340.65793374.jpeg', 'image/jpeg', '114501', '2018-03-16 11:00:08', '2018-03-16 11:00:08', '1');
INSERT INTO `files` VALUES ('319', 'Capture1.PNG', '/var/www/html/public/files/collection/2018/03/16/img_1521173341_5aab435db17c56.13080195.png', 'collection/2018/03/16/img_1521173341_5aab435db17c56.13080195.png', 'image/png', '59903', '2018-03-16 11:09:01', '2018-03-16 11:09:01', '1');
INSERT INTO `files` VALUES ('320', 'Chrysanthemum@#$%^.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174390_5aab47766ec0a0.61279155.jpeg', 'collection/2018/03/16/img_1521174390_5aab47766ec0a0.61279155.jpeg', 'image/jpeg', '879394', '2018-03-16 11:26:30', '2018-03-16 11:26:30', '1');
INSERT INTO `files` VALUES ('321', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174396_5aab477c1b7df5.24878867.jpeg', 'collection/2018/03/16/img_1521174396_5aab477c1b7df5.24878867.jpeg', 'image/jpeg', '780831', '2018-03-16 11:26:36', '2018-03-16 11:26:36', '1');
INSERT INTO `files` VALUES ('322', 'Picture66.png', '/var/www/html/public/files/collection/2018/03/16/img_1521174682_5aab489aa02626.78994025.png', 'collection/2018/03/16/img_1521174682_5aab489aa02626.78994025.png', 'image/png', '286371', '2018-03-16 11:31:22', '2018-03-16 11:31:22', '1');
INSERT INTO `files` VALUES ('323', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174784_5aab49008a4d23.33491515.jpeg', 'collection/2018/03/16/img_1521174784_5aab49008a4d23.33491515.jpeg', 'image/jpeg', '561276', '2018-03-16 11:33:04', '2018-03-16 11:33:04', '1');
INSERT INTO `files` VALUES ('324', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521174851_5aab4943854cd0.01036815.png', 'collection/2018/03/16/img_1521174851_5aab4943854cd0.01036815.png', 'image/png', '185324', '2018-03-16 11:34:11', '2018-03-16 11:34:11', '1');
INSERT INTO `files` VALUES ('325', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174851_5aab4943cc0cf8.42700808.jpeg', 'collection/2018/03/16/img_1521174851_5aab4943cc0cf8.42700808.jpeg', 'image/jpeg', '114501', '2018-03-16 11:34:11', '2018-03-16 11:34:11', '1');
INSERT INTO `files` VALUES ('326', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174852_5aab494418e6e8.66516951.jpeg', 'collection/2018/03/16/img_1521174852_5aab494418e6e8.66516951.jpeg', 'image/jpeg', '777835', '2018-03-16 11:34:12', '2018-03-16 11:34:12', '1');
INSERT INTO `files` VALUES ('327', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521174950_5aab49a6934246.21878407.png', 'collection/2018/03/16/img_1521174950_5aab49a6934246.21878407.png', 'image/png', '185324', '2018-03-16 11:35:50', '2018-03-16 11:35:50', '1');
INSERT INTO `files` VALUES ('328', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174993_5aab49d14104c4.34316348.jpeg', 'collection/2018/03/16/img_1521174993_5aab49d14104c4.34316348.jpeg', 'image/jpeg', '620888', '2018-03-16 11:36:33', '2018-03-16 11:36:33', '1');
INSERT INTO `files` VALUES ('329', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521175470_5aab4bae6fbc81.99147213.jpeg', 'collection/2018/03/16/img_1521175470_5aab4bae6fbc81.99147213.jpeg', 'image/jpeg', '114501', '2018-03-16 11:44:30', '2018-03-16 11:44:30', '1');
INSERT INTO `files` VALUES ('330', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521175627_5aab4c4ba94ac0.32452118.jpeg', 'collection/2018/03/16/img_1521175627_5aab4c4ba94ac0.32452118.jpeg', 'image/jpeg', '114501', '2018-03-16 11:47:07', '2018-03-16 11:47:07', '1');
INSERT INTO `files` VALUES ('331', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521181719_5aab64179c2b26.25411238.jpeg', 'collection/2018/03/16/img_1521181719_5aab64179c2b26.25411238.jpeg', 'image/jpeg', '114501', '2018-03-16 13:28:39', '2018-03-16 13:28:39', '1');
INSERT INTO `files` VALUES ('332', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', 'collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', 'image/jpeg', '114501', '2018-03-16 13:32:39', '2018-03-16 13:32:39', '1');
INSERT INTO `files` VALUES ('333', 'Capture.PNG', '/var/www/html/public/files/collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', 'collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', 'image/png', '25040', '2018-03-16 13:35:06', '2018-03-16 13:35:06', '1');
INSERT INTO `files` VALUES ('334', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521182366_5aab669ec16966.41366917.png', 'collection/2018/03/16/img_1521182366_5aab669ec16966.41366917.png', 'image/png', '362909', '2018-03-16 13:39:26', '2018-03-16 13:39:26', '1');
INSERT INTO `files` VALUES ('335', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521182376_5aab66a82d3aa8.89669388.png', 'collection/2018/03/16/img_1521182376_5aab66a82d3aa8.89669388.png', 'image/png', '185324', '2018-03-16 13:39:36', '2018-03-16 13:39:36', '1');
INSERT INTO `files` VALUES ('336', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521182473_5aab67090e2454.09445286.jpeg', 'collection/2018/03/16/img_1521182473_5aab67090e2454.09445286.jpeg', 'image/jpeg', '777835', '2018-03-16 13:41:13', '2018-03-16 13:41:13', '1');
INSERT INTO `files` VALUES ('337', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521183696_5aab6bd0e2ed68.88136904.jpeg', 'collection/2018/03/16/img_1521183696_5aab6bd0e2ed68.88136904.jpeg', 'image/jpeg', '561276', '2018-03-16 14:01:36', '2018-03-16 14:01:36', '1');
INSERT INTO `files` VALUES ('338', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521183896_5aab6c980f06c1.70832777.jpeg', 'collection/2018/03/16/img_1521183896_5aab6c980f06c1.70832777.jpeg', 'image/jpeg', '777835', '2018-03-16 14:04:56', '2018-03-16 14:04:56', '1');
INSERT INTO `files` VALUES ('339', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521184448_5aab6ec0661652.77785329.jpeg', 'collection/2018/03/16/img_1521184448_5aab6ec0661652.77785329.jpeg', 'image/jpeg', '1806735', '2018-03-16 14:14:08', '2018-03-16 14:14:08', '1');
INSERT INTO `files` VALUES ('340', 'Chrysanthemum@#$%^.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521184477_5aab6edd38c1d1.61064554.jpeg', 'collection/2018/03/16/img_1521184477_5aab6edd38c1d1.61064554.jpeg', 'image/jpeg', '879394', '2018-03-16 14:14:37', '2018-03-16 14:14:37', '1');
INSERT INTO `files` VALUES ('341', 'nhietke.png', '/var/www/html/public/files/collection/2018/03/16/img_1521185072_5aab7130b485d3.12720080.png', 'collection/2018/03/16/img_1521185072_5aab7130b485d3.12720080.png', 'image/png', '112872', '2018-03-16 14:24:32', '2018-03-16 14:24:32', '1');
INSERT INTO `files` VALUES ('342', 'FullSizeRender.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521185137_5aab7171bfdb67.94907905.jpeg', 'collection/2018/03/16/img_1521185137_5aab7171bfdb67.94907905.jpeg', 'image/jpeg', '1097860', '2018-03-16 14:25:37', '2018-03-16 14:25:37', '1');
INSERT INTO `files` VALUES ('343', '5aab77d787b57_1521186775_20180316.jpg', 'files/decision/2018/03/5aab77d787b57_1521186775_20180316.jpg', 'files/decision/2018/03/5aab77d787b57_1521186775_20180316.jpg', 'video/mp4', '1385735', '2018-03-16 14:52:55', '2018-03-16 14:52:55', '1');
INSERT INTO `files` VALUES ('344', '5aab77f9301fd_1521186809_20180316.jpg', 'files/decision/2018/03/5aab77f9301fd_1521186809_20180316.jpg', 'files/decision/2018/03/5aab77f9301fd_1521186809_20180316.jpg', 'video/mp4', '1246482', '2018-03-16 14:53:29', '2018-03-16 14:53:29', '1');
INSERT INTO `files` VALUES ('345', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521186947_5aab7883575480.02318570.jpeg', 'collection/2018/03/16/img_1521186947_5aab7883575480.02318570.jpeg', 'image/jpeg', '114501', '2018-03-16 14:55:47', '2018-03-16 14:55:47', '1');
INSERT INTO `files` VALUES ('346', '5aab7885e8ebc_1521186949_20180316.jpg', 'files/decision/2018/03/5aab7885e8ebc_1521186949_20180316.jpg', 'files/decision/2018/03/5aab7885e8ebc_1521186949_20180316.jpg', 'video/mp4', '1246482', '2018-03-16 14:55:49', '2018-03-16 14:55:49', '1');
INSERT INTO `files` VALUES ('347', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521186969_5aab789974cb56.82377981.jpeg', 'collection/2018/03/16/img_1521186969_5aab789974cb56.82377981.jpeg', 'image/jpeg', '114501', '2018-03-16 14:56:09', '2018-03-16 14:56:09', '1');
INSERT INTO `files` VALUES ('348', '2.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', 'collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', 'image/jpeg', '37400', '2018-03-16 15:01:16', '2018-03-16 15:01:16', '1');
INSERT INTO `files` VALUES ('349', '5aab7a8e25366_1521187470_20180316.pdf', 'files/decision/2018/03/5aab7a8e25366_1521187470_20180316.pdf', 'files/decision/2018/03/5aab7a8e25366_1521187470_20180316.pdf', 'application/pdf', '6510534', '2018-03-16 15:04:30', '2018-03-16 15:04:30', '1');
INSERT INTO `files` VALUES ('350', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521188636_5aab7f1cb429a3.47387654.jpeg', 'collection/2018/03/16/img_1521188636_5aab7f1cb429a3.47387654.jpeg', 'image/jpeg', '114501', '2018-03-16 15:23:56', '2018-03-16 15:23:56', '1');
INSERT INTO `files` VALUES ('351', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521188702_5aab7f5ee36f92.84609281.jpeg', 'collection/2018/03/16/img_1521188702_5aab7f5ee36f92.84609281.jpeg', 'image/jpeg', '777835', '2018-03-16 15:25:02', '2018-03-16 15:25:02', '1');
INSERT INTO `files` VALUES ('352', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521191163_5aab88fbf27890.38064920.png', 'collection/2018/03/16/img_1521191163_5aab88fbf27890.38064920.png', 'image/png', '362909', '2018-03-16 16:06:04', '2018-03-16 16:06:04', '1');
INSERT INTO `files` VALUES ('353', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521191335_5aab89a77e0e21.51858982.png', 'collection/2018/03/16/img_1521191335_5aab89a77e0e21.51858982.png', 'image/png', '362909', '2018-03-16 16:08:55', '2018-03-16 16:08:55', '1');
INSERT INTO `files` VALUES ('354', 'images.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521191810_5aab8b82842903.69869790.jpeg', 'collection/2018/03/16/img_1521191810_5aab8b82842903.69869790.jpeg', 'image/jpeg', '6228', '2018-03-16 16:16:50', '2018-03-16 16:16:50', '1');
INSERT INTO `files` VALUES ('355', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521191874_5aab8bc2e75776.81906463.jpeg', 'collection/2018/03/16/img_1521191874_5aab8bc2e75776.81906463.jpeg', 'image/jpeg', '845941', '2018-03-16 16:17:54', '2018-03-16 16:17:54', '1');
INSERT INTO `files` VALUES ('356', 'images (1).jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521191949_5aab8c0d745545.49222775.jpeg', 'collection/2018/03/16/img_1521191949_5aab8c0d745545.49222775.jpeg', 'image/jpeg', '7035', '2018-03-16 16:19:09', '2018-03-16 16:19:09', '1');
INSERT INTO `files` VALUES ('357', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521192218_5aab8d1a872120.82776724.png', 'collection/2018/03/16/img_1521192218_5aab8d1a872120.82776724.png', 'image/png', '185324', '2018-03-16 16:23:38', '2018-03-16 16:23:38', '1');
INSERT INTO `files` VALUES ('358', 'bo-suu-tap-nhung-hinh-anh-hot-girl-mac-bikini-goi-cam-nhat-6.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521192314_5aab8d7aee0d45.45701283.jpeg', 'collection/2018/03/16/img_1521192314_5aab8d7aee0d45.45701283.jpeg', 'image/jpeg', '304531', '2018-03-16 16:25:15', '2018-03-16 16:25:15', '1');
INSERT INTO `files` VALUES ('359', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521192563_5aab8e734bf781.08818864.png', 'collection/2018/03/16/img_1521192563_5aab8e734bf781.08818864.png', 'image/png', '185324', '2018-03-16 16:29:23', '2018-03-16 16:29:23', '1');
INSERT INTO `files` VALUES ('360', 'images.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521192672_5aab8ee0bf9419.04846807.jpeg', 'collection/2018/03/16/img_1521192672_5aab8ee0bf9419.04846807.jpeg', 'image/jpeg', '6228', '2018-03-16 16:31:12', '2018-03-16 16:31:12', '1');
INSERT INTO `files` VALUES ('361', 'bo-suu-tap-nhung-hinh-anh-hot-girl-mac-bikini-goi-cam-nhat-6.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521194464_5aab95e0737ad9.13724145.jpeg', 'collection/2018/03/16/img_1521194464_5aab95e0737ad9.13724145.jpeg', 'image/jpeg', '304531', '2018-03-16 17:01:04', '2018-03-16 17:01:04', '1');
INSERT INTO `files` VALUES ('362', 'images (1).jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521194697_5aab96c9b86990.58245542.jpeg', 'collection/2018/03/16/img_1521194697_5aab96c9b86990.58245542.jpeg', 'image/jpeg', '7035', '2018-03-16 17:04:57', '2018-03-16 17:04:57', '1');
INSERT INTO `files` VALUES ('363', 'bo-suu-tap-nhung-hinh-anh-hot-girl-mac-bikini-goi-cam-nhat-6.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521194978_5aab97e2a61580.66076550.jpeg', 'collection/2018/03/16/img_1521194978_5aab97e2a61580.66076550.jpeg', 'image/jpeg', '304531', '2018-03-16 17:09:38', '2018-03-16 17:09:38', '1');
INSERT INTO `files` VALUES ('364', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521196544_5aab9e007646b7.97909790.png', 'collection/2018/03/16/img_1521196544_5aab9e007646b7.97909790.png', 'image/png', '362909', '2018-03-16 17:35:44', '2018-03-16 17:35:44', '1');
INSERT INTO `files` VALUES ('365', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521196610_5aab9e42252ea7.13632401.png', 'collection/2018/03/16/img_1521196610_5aab9e42252ea7.13632401.png', 'image/png', '362909', '2018-03-16 17:36:50', '2018-03-16 17:36:50', '1');
INSERT INTO `files` VALUES ('366', '2018-03-18_19-08-54.png', '/var/www/html/public/files/collection/2018/03/19/img_1521423836_5aaf15dc46dd99.83793664.png', 'collection/2018/03/19/img_1521423836_5aaf15dc46dd99.83793664.png', 'image/png', '39416', '2018-03-19 08:43:56', '2018-03-19 08:43:56', '1');
INSERT INTO `files` VALUES ('367', '2018-03-18_9-23-40.png', '/var/www/html/public/files/collection/2018/03/19/img_1521423977_5aaf166926f483.56938990.png', 'collection/2018/03/19/img_1521423977_5aaf166926f483.56938990.png', 'image/png', '7793', '2018-03-19 08:46:17', '2018-03-19 08:46:17', '1');
INSERT INTO `files` VALUES ('368', '2018-03-18_9-36-53.png', '/var/www/html/public/files/collection/2018/03/19/img_1521424051_5aaf16b3407510.12927624.png', 'collection/2018/03/19/img_1521424051_5aaf16b3407510.12927624.png', 'image/png', '16137', '2018-03-19 08:47:31', '2018-03-19 08:47:31', '1');
INSERT INTO `files` VALUES ('369', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521433815_5aaf3cd707fb17.31644168.jpeg', 'collection/2018/03/19/img_1521433815_5aaf3cd707fb17.31644168.jpeg', 'image/jpeg', '879394', '2018-03-19 11:30:15', '2018-03-19 11:30:15', '1');
INSERT INTO `files` VALUES ('370', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521434269_5aaf3e9dc877f9.89915900.jpeg', 'collection/2018/03/19/img_1521434269_5aaf3e9dc877f9.89915900.jpeg', 'image/jpeg', '879394', '2018-03-19 11:37:49', '2018-03-19 11:37:49', '1');
INSERT INTO `files` VALUES ('371', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521434638_5aaf400e6357f5.14271148.jpeg', 'collection/2018/03/19/img_1521434638_5aaf400e6357f5.14271148.jpeg', 'image/jpeg', '845941', '2018-03-19 11:43:58', '2018-03-19 11:43:58', '1');
INSERT INTO `files` VALUES ('372', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521434660_5aaf402439ffb1.99764789.jpeg', 'collection/2018/03/19/img_1521434660_5aaf402439ffb1.99764789.jpeg', 'image/jpeg', '595284', '2018-03-19 11:44:20', '2018-03-19 11:44:20', '1');
INSERT INTO `files` VALUES ('373', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521434670_5aaf402e31be81.55316577.jpeg', 'collection/2018/03/19/img_1521434670_5aaf402e31be81.55316577.jpeg', 'image/jpeg', '845941', '2018-03-19 11:44:30', '2018-03-19 11:44:30', '1');
INSERT INTO `files` VALUES ('374', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521448224_5aaf75206309e9.65263255.jpeg', 'collection/2018/03/19/img_1521448224_5aaf75206309e9.65263255.jpeg', 'image/jpeg', '595284', '2018-03-19 15:30:24', '2018-03-19 15:30:24', '1');
INSERT INTO `files` VALUES ('375', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521448853_5aaf77959221c5.35910421.jpeg', 'collection/2018/03/19/img_1521448853_5aaf77959221c5.35910421.jpeg', 'image/jpeg', '780831', '2018-03-19 15:40:53', '2018-03-19 15:40:53', '1');
INSERT INTO `files` VALUES ('376', '5ab3d525ad184_1521734949_20180322.png', 'files/decision/2018/03/5ab3d525ad184_1521734949_20180322.png', 'files/decision/2018/03/5ab3d525ad184_1521734949_20180322.png', 'image/png', '112534', '2018-03-22 16:09:09', '2018-03-22 16:09:09', '1');
INSERT INTO `files` VALUES ('377', '5ab3d535c1954_1521734965_20180322.png', 'files/decision/2018/03/5ab3d535c1954_1521734965_20180322.png', 'files/decision/2018/03/5ab3d535c1954_1521734965_20180322.png', 'image/png', '122410', '2018-03-22 16:09:25', '2018-03-22 16:09:25', '1');
INSERT INTO `files` VALUES ('378', '5ab482b00555f_1521779376_20180323.png', 'files/decision/2018/03/5ab482b00555f_1521779376_20180323.png', 'files/decision/2018/03/5ab482b00555f_1521779376_20180323.png', 'image/png', '179127', '2018-03-23 04:29:36', '2018-03-23 04:29:36', '1');
INSERT INTO `files` VALUES ('379', '5ab764fddb3ba_1521968381_20180325.png', 'files/decision/2018/03/5ab764fddb3ba_1521968381_20180325.png', 'files/decision/2018/03/5ab764fddb3ba_1521968381_20180325.png', 'image/png', '179127', '2018-03-25 08:59:41', '2018-03-25 08:59:41', '1');
INSERT INTO `files` VALUES ('380', '5ab90e021e52e_1522077186_20180326.jpg', 'files/decision/2018/03/5ab90e021e52e_1522077186_20180326.jpg', 'files/decision/2018/03/5ab90e021e52e_1522077186_20180326.jpg', 'image/jpeg', '15286', '2018-03-26 15:13:06', '2018-03-26 15:13:06', '1');
INSERT INTO `files` VALUES ('381', '5ab9182204046_1522079778_20180326.jpg', 'files/decision/2018/03/5ab9182204046_1522079778_20180326.jpg', 'files/decision/2018/03/5ab9182204046_1522079778_20180326.jpg', 'image/jpeg', '19452', '2018-03-26 15:56:18', '2018-03-26 15:56:18', '1');
INSERT INTO `files` VALUES ('382', 'Chrysanthemum.jpg', 'C:\\xampp\\htdocs\\dulich2\\code\\public\\files\\collection\\2018\\04\\02\\img_1522645426_5ac1b9b2a5de44.30749559.jpeg', 'collection/2018/04/02/img_1522645426_5ac1b9b2a5de44.30749559.jpeg', 'image/jpeg', '879394', '2018-04-02 05:03:46', '2018-04-02 05:03:46', '1');
INSERT INTO `files` VALUES ('383', 'Chrysanthemum.jpg', 'C:\\xampp\\htdocs\\dulich2\\code\\public\\files\\collection\\2018\\04\\02\\img_1522645533_5ac1ba1dd73784.85802772.jpeg', 'collection/2018/04/02/img_1522645533_5ac1ba1dd73784.85802772.jpeg', 'image/jpeg', '879394', '2018-04-02 05:05:33', '2018-04-02 05:05:33', '1');
INSERT INTO `files` VALUES ('384', 'Chrysanthemum.jpg', 'C:\\xampp\\htdocs\\dulich2\\code\\public\\files\\collection\\2018\\04\\02\\img_1522645566_5ac1ba3e1e1ec1.05594738.jpeg', 'collection/2018/04/02/img_1522645566_5ac1ba3e1e1ec1.05594738.jpeg', 'image/jpeg', '879394', '2018-04-02 05:06:06', '2018-04-02 05:06:06', '1');
INSERT INTO `files` VALUES ('385', 'Desert.jpg', 'C:\\xampp\\htdocs\\dulich2\\code\\public\\files\\collection\\2018\\04\\02\\img_1522645586_5ac1ba5204c360.55875451.jpeg', 'collection/2018/04/02/img_1522645586_5ac1ba5204c360.55875451.jpeg', 'image/jpeg', '845941', '2018-04-02 05:06:26', '2018-04-02 05:06:26', '1');
INSERT INTO `files` VALUES ('386', 'Chrysanthemum.jpg', 'C:\\xampp\\htdocs\\dulich2\\code\\public\\files\\collection\\2018\\04\\02\\img_1522645627_5ac1ba7b0baf10.95160668.jpeg', 'collection/2018/04/02/img_1522645627_5ac1ba7b0baf10.95160668.jpeg', 'image/jpeg', '879394', '2018-04-02 05:07:07', '2018-04-02 05:07:07', '1');
INSERT INTO `files` VALUES ('387', 'Desert.jpg', 'C:\\xampp\\htdocs\\dulich2\\code\\public\\files\\collection\\2018\\04\\02\\img_1522645855_5ac1bb5f95a421.22955299.jpeg', 'collection/2018/04/02/img_1522645855_5ac1bb5f95a421.22955299.jpeg', 'image/jpeg', '845941', '2018-04-02 05:10:55', '2018-04-02 05:10:55', '1');
INSERT INTO `files` VALUES ('388', 'Chrysanthemum.jpg', 'C:\\xampp\\htdocs\\dulich2\\code\\public\\files\\collection\\2018\\04\\02\\img_1522646049_5ac1bc2110d831.61379863.jpeg', 'collection/2018/04/02/img_1522646049_5ac1bc2110d831.61379863.jpeg', 'image/jpeg', '879394', '2018-04-02 05:14:09', '2018-04-02 05:14:09', '1');

-- ----------------------------
-- Table structure for groupsizes
-- ----------------------------
DROP TABLE IF EXISTS `groupsizes`;
CREATE TABLE `groupsizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupSize` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of groupsizes
-- ----------------------------
INSERT INTO `groupsizes` VALUES ('1', 'Khách lẻ mai sửa', '2017-12-01 00:08:37', '2017-12-01 00:08:37', '0');
INSERT INTO `groupsizes` VALUES ('2', 'Từ 04 đến 10 khách', '2017-12-01 00:08:38', '2017-12-01 00:08:38', '1');
INSERT INTO `groupsizes` VALUES ('3', 'Từ 11 đến 20 khách', '2017-12-01 20:52:52', '2017-12-01 19:50:47', '1');
INSERT INTO `groupsizes` VALUES ('4', 'Từ 21 đến 30 khách', '2017-12-01 22:50:53', '2017-12-01 21:53:53', '1');
INSERT INTO `groupsizes` VALUES ('5', 'Trên 30 khách', '2017-12-01 20:52:52', '2017-12-01 19:50:47', '1');

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of jobs
-- ----------------------------

-- ----------------------------
-- Table structure for languagelevels
-- ----------------------------
DROP TABLE IF EXISTS `languagelevels`;
CREATE TABLE `languagelevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `levelName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of languagelevels
-- ----------------------------
INSERT INTO `languagelevels` VALUES ('1', 'Cao đẳng ngoại ngữ', '2017-12-01 00:08:37', '2017-12-01 00:08:37', '0');
INSERT INTO `languagelevels` VALUES ('2', 'Đại học ngoại ngữ', '2017-12-01 00:08:37', '2017-12-01 00:08:37', '1');
INSERT INTO `languagelevels` VALUES ('3', 'Trên đại học ngoại ngữ', '2017-12-01 11:27:29', '2017-12-01 13:33:34', '1');
INSERT INTO `languagelevels` VALUES ('4', 'Cao đẳng chuyên ngành khác bằng tiếng nước ngoài', '2017-12-01 22:53:54', '2017-12-01 21:56:56', '1');
INSERT INTO `languagelevels` VALUES ('5', 'Đại học chuyên ngành khác bằng tiếng nước ngoài', '2017-12-01 11:27:29', '2017-12-01 13:33:34', '1');
INSERT INTO `languagelevels` VALUES ('6', 'Trên đại học chuyên ngành khác bằng tiếng nước ngoài', '2017-12-01 22:53:54', '2017-12-01 21:56:56', '1');
INSERT INTO `languagelevels` VALUES ('7', 'Chứng chỉ ngoại ngữ do cơ quan có thẩm quyền cấp', '2017-12-01 11:27:29', '2017-12-01 13:33:34', '1');
INSERT INTO `languagelevels` VALUES ('8', 'Chứng  chỉ sử dụng thành  thạo ngoại ngữ do cơ sở đào tạo cấp', '2017-12-01 22:53:54', '2017-12-01 21:56:56', '1');

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `languageName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('1', 'Tiếng Anh mai sửa', '2017-12-01 00:08:36', '2017-12-01 00:08:36', '1', '1');
INSERT INTO `languages` VALUES ('2', 'Tiếng Nhật', '2017-12-01 00:08:36', '2017-12-01 00:08:36', '1', '2');
INSERT INTO `languages` VALUES ('3', 'Tiếng Trung', '2017-12-01 07:30:30', '2017-12-01 13:32:37', '1', '3');
INSERT INTO `languages` VALUES ('4', 'Tiếng Đức', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '4');
INSERT INTO `languages` VALUES ('5', 'Tiếng Pháp', '2017-12-01 07:30:30', '2017-12-01 13:32:37', '1', '5');
INSERT INTO `languages` VALUES ('6', 'Tiếng Tây Ban Nha', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '6');
INSERT INTO `languages` VALUES ('7', 'Tiếng Ý', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '7');
INSERT INTO `languages` VALUES ('8', 'Tiếng Nga', '2017-12-01 07:30:30', '2017-12-01 13:32:37', '1', '8');
INSERT INTO `languages` VALUES ('9', 'Tiếng Hàn Quốc', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '9');
INSERT INTO `languages` VALUES ('10', 'Tiếng khác', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '10');

-- ----------------------------
-- Table structure for languageskills
-- ----------------------------
DROP TABLE IF EXISTS `languageskills`;
CREATE TABLE `languageskills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `languageId` int(10) unsigned NOT NULL,
  `levelId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `languageskills_memberid_foreign` (`memberId`),
  KEY `languageskills_languageid_foreign` (`languageId`),
  KEY `languageskills_levelid_foreign` (`levelId`),
  KEY `languageskills_fileid_foreign` (`fileId`),
  CONSTRAINT `languageskills_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `languageskills_languageid_foreign` FOREIGN KEY (`languageId`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `languageskills_levelid_foreign` FOREIGN KEY (`levelId`) REFERENCES `languagelevels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `languageskills_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of languageskills
-- ----------------------------
INSERT INTO `languageskills` VALUES ('1', '1', '1', '8', null, '2018-03-13 09:53:09', '2018-03-16 17:10:07');
INSERT INTO `languageskills` VALUES ('2', '2', '1', '8', null, '2018-03-13 09:59:56', '2018-03-13 09:59:56');
INSERT INTO `languageskills` VALUES ('3', '3', '3', '7', null, '2018-03-13 10:05:04', '2018-03-13 10:05:04');
INSERT INTO `languageskills` VALUES ('4', '5', '1', '8', null, '2018-03-13 10:11:17', '2018-03-13 10:11:17');
INSERT INTO `languageskills` VALUES ('5', '7', '1', '8', null, '2018-03-13 10:14:55', '2018-03-13 10:14:55');
INSERT INTO `languageskills` VALUES ('6', '8', '6', '3', null, '2018-03-13 10:35:52', '2018-03-13 10:35:52');
INSERT INTO `languageskills` VALUES ('7', '11', '1', '8', null, '2018-03-13 14:00:48', '2018-03-13 14:00:48');
INSERT INTO `languageskills` VALUES ('8', '16', '4', '5', null, '2018-03-13 22:29:33', '2018-03-16 15:39:20');
INSERT INTO `languageskills` VALUES ('9', '17', '2', '2', null, '2018-03-14 09:37:17', '2018-03-19 08:50:33');
INSERT INTO `languageskills` VALUES ('10', '19', '8', '6', null, '2018-03-14 10:31:48', '2018-03-14 10:31:48');
INSERT INTO `languageskills` VALUES ('11', '26', '8', '5', null, '2018-03-14 11:04:03', '2018-03-14 11:04:03');
INSERT INTO `languageskills` VALUES ('12', '30', '8', '5', null, '2018-03-14 11:11:30', '2018-03-14 11:11:30');
INSERT INTO `languageskills` VALUES ('13', '39', '2', '8', null, '2018-03-15 10:53:49', '2018-03-15 10:53:49');
INSERT INTO `languageskills` VALUES ('14', '42', '7', '4', null, '2018-03-15 11:29:59', '2018-03-15 11:29:59');
INSERT INTO `languageskills` VALUES ('15', '45', '5', '7', null, '2018-03-15 11:38:59', '2018-03-15 11:38:59');
INSERT INTO `languageskills` VALUES ('16', '47', '5', '2', null, '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `languageskills` VALUES ('17', '48', '5', '2', null, '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `languageskills` VALUES ('18', '49', '4', '6', null, '2018-03-15 13:38:43', '2018-03-15 13:38:43');
INSERT INTO `languageskills` VALUES ('19', '50', '5', '5', null, '2018-03-15 13:44:37', '2018-03-15 13:44:37');
INSERT INTO `languageskills` VALUES ('21', '51', '2', '2', null, '2018-03-15 13:54:19', '2018-03-15 13:54:19');
INSERT INTO `languageskills` VALUES ('22', '52', '4', '2', null, '2018-03-15 14:30:20', '2018-03-15 14:30:20');
INSERT INTO `languageskills` VALUES ('23', '53', '2', '2', null, '2018-03-15 14:33:57', '2018-03-16 11:31:30');
INSERT INTO `languageskills` VALUES ('24', '54', '9', '7', null, '2018-03-15 14:38:02', '2018-03-15 14:38:02');
INSERT INTO `languageskills` VALUES ('25', '56', '2', '5', null, '2018-03-15 16:27:16', '2018-03-15 16:27:16');
INSERT INTO `languageskills` VALUES ('26', '58', '2', '5', null, '2018-03-15 16:28:35', '2018-03-15 16:28:35');
INSERT INTO `languageskills` VALUES ('27', '58', '2', '5', null, '2018-03-15 16:30:48', '2018-03-15 16:30:48');
INSERT INTO `languageskills` VALUES ('28', '58', '2', '5', null, '2018-03-15 16:31:06', '2018-03-15 16:31:06');
INSERT INTO `languageskills` VALUES ('29', '58', '2', '5', null, '2018-03-15 16:31:29', '2018-03-15 16:31:29');
INSERT INTO `languageskills` VALUES ('30', '61', '3', '7', null, '2018-03-16 11:00:54', '2018-03-16 11:00:54');
INSERT INTO `languageskills` VALUES ('31', '62', '3', '7', null, '2018-03-16 11:09:11', '2018-03-16 11:09:11');
INSERT INTO `languageskills` VALUES ('32', '63', '3', '7', null, '2018-03-16 11:34:15', '2018-03-16 11:34:15');
INSERT INTO `languageskills` VALUES ('33', '64', '7', '6', null, '2018-03-16 11:34:40', '2018-03-16 11:34:40');
INSERT INTO `languageskills` VALUES ('34', '65', '2', '7', null, '2018-03-16 13:32:45', '2018-03-16 13:32:45');
INSERT INTO `languageskills` VALUES ('35', '66', '2', '7', null, '2018-03-16 13:33:40', '2018-03-16 13:33:40');
INSERT INTO `languageskills` VALUES ('36', '67', '2', '7', null, '2018-03-16 13:34:19', '2018-03-16 13:34:19');
INSERT INTO `languageskills` VALUES ('37', '68', '2', '7', null, '2018-03-16 13:35:12', '2018-03-16 13:35:12');
INSERT INTO `languageskills` VALUES ('38', '69', '10', '3', null, '2018-03-16 13:39:48', '2018-03-16 13:39:48');
INSERT INTO `languageskills` VALUES ('39', '70', '2', '7', null, '2018-03-16 13:39:49', '2018-03-16 15:37:33');
INSERT INTO `languageskills` VALUES ('40', '71', '2', '7', null, '2018-03-16 13:44:56', '2018-03-16 13:44:56');
INSERT INTO `languageskills` VALUES ('41', '71', '2', '7', null, '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `languageskills` VALUES ('42', '72', '2', '7', null, '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `languageskills` VALUES ('43', '71', '2', '7', null, '2018-03-16 13:46:39', '2018-03-16 13:46:39');
INSERT INTO `languageskills` VALUES ('44', '71', '2', '7', null, '2018-03-16 13:47:45', '2018-03-16 13:47:45');
INSERT INTO `languageskills` VALUES ('45', '71', '2', '7', null, '2018-03-16 13:48:09', '2018-03-16 13:48:09');
INSERT INTO `languageskills` VALUES ('46', '72', '2', '7', null, '2018-03-16 14:28:15', '2018-03-16 14:28:15');
INSERT INTO `languageskills` VALUES ('47', '74', '2', '7', null, '2018-03-16 14:56:22', '2018-03-16 14:56:22');
INSERT INTO `languageskills` VALUES ('48', '75', '2', '7', null, '2018-03-16 15:01:23', '2018-03-16 15:01:23');
INSERT INTO `languageskills` VALUES ('49', '75', '2', '7', null, '2018-03-16 15:01:48', '2018-03-16 15:01:48');
INSERT INTO `languageskills` VALUES ('50', '76', '2', '7', null, '2018-03-16 15:18:48', '2018-03-16 15:18:48');
INSERT INTO `languageskills` VALUES ('51', '77', '2', '7', null, '2018-03-16 15:19:23', '2018-03-16 15:19:23');
INSERT INTO `languageskills` VALUES ('52', '78', '2', '7', null, '2018-03-16 15:24:01', '2018-03-16 15:24:01');
INSERT INTO `languageskills` VALUES ('53', '79', '10', '3', null, '2018-03-16 16:09:19', '2018-03-16 17:06:28');
INSERT INTO `languageskills` VALUES ('54', '80', '7', '6', null, '2018-03-16 17:37:11', '2018-03-16 17:37:11');
INSERT INTO `languageskills` VALUES ('55', '81', '1', '8', null, '2018-03-19 11:37:54', '2018-03-19 11:37:54');
INSERT INTO `languageskills` VALUES ('56', '81', '1', '8', null, '2018-03-19 11:38:39', '2018-03-19 11:38:39');
INSERT INTO `languageskills` VALUES ('57', '82', '1', '8', null, '2018-03-19 11:44:32', '2018-03-19 11:44:32');
INSERT INTO `languageskills` VALUES ('58', '83', '1', '8', null, '2018-03-19 15:42:18', '2018-03-19 15:59:51');

-- ----------------------------
-- Table structure for leader_signing_decision
-- ----------------------------
DROP TABLE IF EXISTS `leader_signing_decision`;
CREATE TABLE `leader_signing_decision` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(10) DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deletedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of leader_signing_decision
-- ----------------------------
INSERT INTO `leader_signing_decision` VALUES ('3', 'Nguyễn Văn Linh', '1', null, null, null, null);
INSERT INTO `leader_signing_decision` VALUES ('4', 'Đào Thanh Hương', '1', null, null, null, null);

-- ----------------------------
-- Table structure for majors
-- ----------------------------
DROP TABLE IF EXISTS `majors`;
CREATE TABLE `majors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `majorName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of majors
-- ----------------------------
INSERT INTO `majors` VALUES ('1', 'Giấy chứng nhận khóa nghiệp vụ hướng dẫn du lịch nội địa', '2017-12-01 00:08:36', '2017-12-01 00:08:36', '1');
INSERT INTO `majors` VALUES ('2', 'Giấy chứng nhận khóa nghiệp vụ hướng dẫn du lịch quốc tế', '2017-12-01 00:08:36', '2017-12-01 00:08:36', '1');
INSERT INTO `majors` VALUES ('3', 'Trung cấp chuyên ngành hướng dẫn du lịch', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `majors` VALUES ('4', 'Cao đẳng chuyên ngành hướng dẫn du lịch', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `majors` VALUES ('7', 'Đaị học chuyên ngành hướng dẫn du lịch', '2017-12-01 15:27:33', '2017-12-01 15:27:33', '1');
INSERT INTO `majors` VALUES ('8', 'Đạt yêu cầu kiểm tra nghiệp vụ hướng dẫn viên du lịch tại điểm', '2017-12-16 09:26:27', '2017-12-16 12:27:34', '1');

-- ----------------------------
-- Table structure for majorskillfiles
-- ----------------------------
DROP TABLE IF EXISTS `majorskillfiles`;
CREATE TABLE `majorskillfiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `majorskillfiles_memberid_foreign` (`memberId`),
  KEY `majorskillfiles_fileid_foreign` (`fileId`),
  CONSTRAINT `majorskillfiles_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `majorskillfiles_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of majorskillfiles
-- ----------------------------

-- ----------------------------
-- Table structure for majorskills
-- ----------------------------
DROP TABLE IF EXISTS `majorskills`;
CREATE TABLE `majorskills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `majorId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `majorskills_memberid_foreign` (`memberId`),
  KEY `majorskills_majorid_foreign` (`majorId`),
  CONSTRAINT `majorskills_majorid_foreign` FOREIGN KEY (`majorId`) REFERENCES `majors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `majorskills_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of majorskills
-- ----------------------------
INSERT INTO `majorskills` VALUES ('1', '1', '3', '2018-03-13 09:53:09', '2018-03-16 17:10:07');
INSERT INTO `majorskills` VALUES ('2', '2', '3', '2018-03-13 09:59:56', '2018-03-13 09:59:56');
INSERT INTO `majorskills` VALUES ('3', '3', '7', '2018-03-13 10:05:04', '2018-03-13 10:05:04');
INSERT INTO `majorskills` VALUES ('4', '4', '1', '2018-03-13 10:10:03', '2018-03-13 10:10:03');
INSERT INTO `majorskills` VALUES ('5', '5', '2', '2018-03-13 10:11:17', '2018-03-13 10:11:17');
INSERT INTO `majorskills` VALUES ('6', '6', '7', '2018-03-13 10:11:40', '2018-03-13 10:11:40');
INSERT INTO `majorskills` VALUES ('7', '7', '3', '2018-03-13 10:14:55', '2018-03-13 10:14:55');
INSERT INTO `majorskills` VALUES ('8', '8', '4', '2018-03-13 10:35:52', '2018-03-13 10:35:52');
INSERT INTO `majorskills` VALUES ('9', '9', '2', '2018-03-13 11:42:24', '2018-03-13 11:42:24');
INSERT INTO `majorskills` VALUES ('10', '10', '8', '2018-03-13 13:57:24', '2018-03-13 13:57:24');
INSERT INTO `majorskills` VALUES ('11', '11', '8', '2018-03-13 14:00:48', '2018-03-13 14:00:48');
INSERT INTO `majorskills` VALUES ('12', '12', '7', '2018-03-13 14:13:06', '2018-03-13 14:13:06');
INSERT INTO `majorskills` VALUES ('13', '13', '7', '2018-03-13 14:16:53', '2018-03-13 14:16:53');
INSERT INTO `majorskills` VALUES ('14', '14', '7', '2018-03-13 14:20:27', '2018-03-13 14:20:27');
INSERT INTO `majorskills` VALUES ('15', '15', '8', '2018-03-13 14:22:15', '2018-03-13 14:22:15');
INSERT INTO `majorskills` VALUES ('16', '16', '4', '2018-03-13 22:29:33', '2018-03-16 15:39:20');
INSERT INTO `majorskills` VALUES ('17', '17', '7', '2018-03-14 09:37:17', '2018-03-19 08:50:33');
INSERT INTO `majorskills` VALUES ('18', '18', '7', '2018-03-14 09:42:54', '2018-03-14 09:42:54');
INSERT INTO `majorskills` VALUES ('19', '19', '7', '2018-03-14 10:31:48', '2018-03-14 10:31:48');
INSERT INTO `majorskills` VALUES ('20', '20', '8', '2018-03-14 10:34:11', '2018-03-14 10:34:11');
INSERT INTO `majorskills` VALUES ('21', '21', '4', '2018-03-14 10:37:23', '2018-03-14 10:37:23');
INSERT INTO `majorskills` VALUES ('22', '22', '7', '2018-03-14 10:40:40', '2018-03-14 10:40:40');
INSERT INTO `majorskills` VALUES ('23', '23', '7', '2018-03-14 10:58:44', '2018-03-14 10:58:44');
INSERT INTO `majorskills` VALUES ('24', '24', '7', '2018-03-14 11:01:34', '2018-03-14 11:01:34');
INSERT INTO `majorskills` VALUES ('25', '25', '7', '2018-03-14 11:03:19', '2018-03-14 11:03:19');
INSERT INTO `majorskills` VALUES ('26', '26', '7', '2018-03-14 11:04:03', '2018-03-14 11:04:03');
INSERT INTO `majorskills` VALUES ('27', '27', '7', '2018-03-14 11:05:08', '2018-03-14 11:05:08');
INSERT INTO `majorskills` VALUES ('28', '28', '4', '2018-03-14 11:08:33', '2018-03-14 11:08:33');
INSERT INTO `majorskills` VALUES ('29', '29', '1', '2018-03-14 11:10:14', '2018-03-14 11:10:14');
INSERT INTO `majorskills` VALUES ('30', '30', '7', '2018-03-14 11:11:30', '2018-03-14 11:11:30');
INSERT INTO `majorskills` VALUES ('31', '31', '2', '2018-03-14 11:12:00', '2018-03-16 16:28:16');
INSERT INTO `majorskills` VALUES ('32', '32', '7', '2018-03-14 11:15:27', '2018-03-14 11:15:27');
INSERT INTO `majorskills` VALUES ('33', '33', '7', '2018-03-14 11:17:53', '2018-03-14 11:17:53');
INSERT INTO `majorskills` VALUES ('34', '34', '2', '2018-03-14 11:20:04', '2018-03-14 11:20:04');
INSERT INTO `majorskills` VALUES ('35', '35', '4', '2018-03-14 11:22:42', '2018-03-14 11:22:42');
INSERT INTO `majorskills` VALUES ('36', '36', '7', '2018-03-14 11:25:18', '2018-03-14 11:25:18');
INSERT INTO `majorskills` VALUES ('37', '37', '4', '2018-03-14 17:29:23', '2018-03-14 17:29:23');
INSERT INTO `majorskills` VALUES ('38', '38', '7', '2018-03-14 17:30:16', '2018-03-14 17:30:16');
INSERT INTO `majorskills` VALUES ('39', '38', '7', '2018-03-14 17:31:14', '2018-03-14 17:31:14');
INSERT INTO `majorskills` VALUES ('40', '39', '4', '2018-03-15 10:53:49', '2018-03-15 10:53:49');
INSERT INTO `majorskills` VALUES ('41', '40', '7', '2018-03-15 11:00:38', '2018-03-15 11:00:38');
INSERT INTO `majorskills` VALUES ('44', '42', '4', '2018-03-15 11:29:59', '2018-03-15 11:29:59');
INSERT INTO `majorskills` VALUES ('45', '43', '7', '2018-03-15 11:31:40', '2018-03-15 11:31:40');
INSERT INTO `majorskills` VALUES ('46', '41', '4', '2018-03-15 11:32:31', '2018-03-15 11:32:31');
INSERT INTO `majorskills` VALUES ('47', '44', '8', '2018-03-15 11:36:10', '2018-03-15 11:36:10');
INSERT INTO `majorskills` VALUES ('48', '45', '3', '2018-03-15 11:38:59', '2018-03-15 11:38:59');
INSERT INTO `majorskills` VALUES ('49', '46', '1', '2018-03-15 11:39:11', '2018-03-15 11:39:11');
INSERT INTO `majorskills` VALUES ('50', '47', '4', '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `majorskills` VALUES ('51', '48', '8', '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `majorskills` VALUES ('52', '49', '1', '2018-03-15 13:38:43', '2018-03-15 13:38:43');
INSERT INTO `majorskills` VALUES ('53', '50', '4', '2018-03-15 13:44:37', '2018-03-15 13:44:37');
INSERT INTO `majorskills` VALUES ('55', '51', '8', '2018-03-15 13:54:19', '2018-03-15 13:54:19');
INSERT INTO `majorskills` VALUES ('56', '52', '8', '2018-03-15 14:30:20', '2018-03-15 14:30:20');
INSERT INTO `majorskills` VALUES ('57', '53', '7', '2018-03-15 14:33:57', '2018-03-16 11:31:30');
INSERT INTO `majorskills` VALUES ('58', '54', '4', '2018-03-15 14:38:02', '2018-03-15 14:38:02');
INSERT INTO `majorskills` VALUES ('59', '55', '4', '2018-03-15 16:18:01', '2018-03-15 16:18:01');
INSERT INTO `majorskills` VALUES ('60', '55', '4', '2018-03-15 16:22:22', '2018-03-15 16:22:22');
INSERT INTO `majorskills` VALUES ('61', '56', '4', '2018-03-15 16:27:16', '2018-03-15 16:27:16');
INSERT INTO `majorskills` VALUES ('62', '57', '7', '2018-03-15 16:27:39', '2018-03-15 16:27:39');
INSERT INTO `majorskills` VALUES ('63', '58', '4', '2018-03-15 16:28:35', '2018-03-15 16:28:35');
INSERT INTO `majorskills` VALUES ('64', '58', '4', '2018-03-15 16:30:48', '2018-03-15 16:30:48');
INSERT INTO `majorskills` VALUES ('65', '58', '4', '2018-03-15 16:31:06', '2018-03-15 16:31:06');
INSERT INTO `majorskills` VALUES ('66', '58', '4', '2018-03-15 16:31:29', '2018-03-15 16:31:29');
INSERT INTO `majorskills` VALUES ('67', '59', '4', '2018-03-16 09:29:02', '2018-03-16 09:29:02');
INSERT INTO `majorskills` VALUES ('68', '60', '7', '2018-03-16 09:36:06', '2018-03-16 09:36:06');
INSERT INTO `majorskills` VALUES ('69', '61', '4', '2018-03-16 11:00:54', '2018-03-16 11:00:54');
INSERT INTO `majorskills` VALUES ('70', '62', '4', '2018-03-16 11:09:11', '2018-03-16 11:09:11');
INSERT INTO `majorskills` VALUES ('71', '63', '4', '2018-03-16 11:34:15', '2018-03-16 11:34:15');
INSERT INTO `majorskills` VALUES ('72', '64', '2', '2018-03-16 11:34:40', '2018-03-16 11:34:40');
INSERT INTO `majorskills` VALUES ('73', '65', '7', '2018-03-16 13:32:45', '2018-03-16 13:32:45');
INSERT INTO `majorskills` VALUES ('74', '66', '7', '2018-03-16 13:33:40', '2018-03-16 13:33:40');
INSERT INTO `majorskills` VALUES ('75', '67', '7', '2018-03-16 13:34:19', '2018-03-16 13:34:19');
INSERT INTO `majorskills` VALUES ('76', '68', '7', '2018-03-16 13:35:12', '2018-03-16 13:35:12');
INSERT INTO `majorskills` VALUES ('77', '69', '4', '2018-03-16 13:39:48', '2018-03-16 13:39:48');
INSERT INTO `majorskills` VALUES ('78', '70', '7', '2018-03-16 13:39:49', '2018-03-16 15:37:33');
INSERT INTO `majorskills` VALUES ('79', '71', '8', '2018-03-16 13:44:56', '2018-03-16 13:44:56');
INSERT INTO `majorskills` VALUES ('80', '71', '8', '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `majorskills` VALUES ('81', '72', '7', '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `majorskills` VALUES ('82', '71', '8', '2018-03-16 13:46:39', '2018-03-16 13:46:39');
INSERT INTO `majorskills` VALUES ('83', '71', '8', '2018-03-16 13:47:45', '2018-03-16 13:47:45');
INSERT INTO `majorskills` VALUES ('84', '71', '8', '2018-03-16 13:48:09', '2018-03-16 13:48:09');
INSERT INTO `majorskills` VALUES ('85', '73', '8', '2018-03-16 14:25:47', '2018-03-16 14:25:47');
INSERT INTO `majorskills` VALUES ('86', '73', '8', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `majorskills` VALUES ('87', '73', '8', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `majorskills` VALUES ('88', '73', '8', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `majorskills` VALUES ('89', '73', '8', '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `majorskills` VALUES ('90', '73', '8', '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `majorskills` VALUES ('91', '73', '8', '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `majorskills` VALUES ('92', '73', '8', '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `majorskills` VALUES ('93', '73', '8', '2018-03-16 14:26:41', '2018-03-16 14:26:41');
INSERT INTO `majorskills` VALUES ('94', '73', '8', '2018-03-16 14:27:04', '2018-03-16 14:27:04');
INSERT INTO `majorskills` VALUES ('95', '72', '7', '2018-03-16 14:28:15', '2018-03-16 14:28:15');
INSERT INTO `majorskills` VALUES ('96', '74', '7', '2018-03-16 14:56:22', '2018-03-16 14:56:22');
INSERT INTO `majorskills` VALUES ('97', '75', '7', '2018-03-16 15:01:23', '2018-03-16 15:01:23');
INSERT INTO `majorskills` VALUES ('98', '75', '7', '2018-03-16 15:01:48', '2018-03-16 15:01:48');
INSERT INTO `majorskills` VALUES ('99', '76', '7', '2018-03-16 15:18:48', '2018-03-16 15:18:48');
INSERT INTO `majorskills` VALUES ('100', '77', '7', '2018-03-16 15:19:23', '2018-03-16 15:19:23');
INSERT INTO `majorskills` VALUES ('101', '78', '7', '2018-03-16 15:24:01', '2018-03-16 15:24:01');
INSERT INTO `majorskills` VALUES ('102', '79', '3', '2018-03-16 16:09:19', '2018-03-16 17:06:28');
INSERT INTO `majorskills` VALUES ('103', '80', '7', '2018-03-16 17:37:11', '2018-03-16 17:37:11');
INSERT INTO `majorskills` VALUES ('104', '81', '2', '2018-03-19 11:37:54', '2018-03-19 11:37:54');
INSERT INTO `majorskills` VALUES ('105', '81', '2', '2018-03-19 11:38:39', '2018-03-19 11:38:39');
INSERT INTO `majorskills` VALUES ('106', '82', '2', '2018-03-19 11:44:32', '2018-03-19 11:44:32');
INSERT INTO `majorskills` VALUES ('107', '83', '8', '2018-03-19 15:42:18', '2018-03-19 15:59:51');

-- ----------------------------
-- Table structure for memberavatars
-- ----------------------------
DROP TABLE IF EXISTS `memberavatars`;
CREATE TABLE `memberavatars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned NOT NULL,
  `current` tinyint(4) NOT NULL DEFAULT '0',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberavatars_memberid_foreign` (`memberId`),
  KEY `memberavatars_fileid_foreign` (`fileId`),
  CONSTRAINT `memberavatars_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `memberavatars_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of memberavatars
-- ----------------------------

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullName` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profileImg` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `touristGuideCode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `cmtCccd` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateIssued` datetime DEFAULT NULL,
  `issuedBy` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanentAddress` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `firstMobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondMobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstEmail` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondEmail` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeOfTravelGuide` tinyint(4) DEFAULT NULL,
  `typeOfPlace` tinyint(4) DEFAULT NULL,
  `experienceYear` int(11) DEFAULT NULL,
  `experienceLevel` tinyint(4) DEFAULT NULL,
  `otherSkills` text COLLATE utf8mb4_unicode_ci,
  `otherInformation` text COLLATE utf8mb4_unicode_ci,
  `touristGuideLevel` text COLLATE utf8mb4_unicode_ci,
  `achievements` text COLLATE utf8mb4_unicode_ci,
  `emailToken` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phoneToken` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailVerified` tinyint(4) NOT NULL DEFAULT '0',
  `phoneVerified` tinyint(4) NOT NULL DEFAULT '0',
  `acceptTermsAndPolicies` tinyint(4) NOT NULL DEFAULT '0',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `province_code` tinyint(4) NOT NULL,
  `is_verified` tinyint(4) DEFAULT NULL,
  `is_fee` int(4) DEFAULT NULL,
  `is_signed` int(4) DEFAULT NULL,
  `member_type` int(4) DEFAULT NULL,
  `member_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_qr_id` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_from` datetime DEFAULT NULL,
  `guideLanguage` tinyint(4) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `verified_by` int(11) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `inboundOutbound` tinyint(4) DEFAULT NULL,
  `deleteAt` timestamp NULL DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `file_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initial_stock` tinyint(11) DEFAULT NULL,
  `province` int(4) DEFAULT NULL,
  `user_type` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('1', 'Nguyễn Thị Mai E bình chi hà', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521194978_5aab97e2a61580.66076550.jpeg', '1987-06-12 00:00:00', '1', '156156156', '2018-04-04 00:00:00', '112098866', '2018-02-21 00:00:00', 'CA TP Hà Nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', null, 'maint18@viettel.com.vn', null, '1', '2', '2018', '31', 'các kỹ năng khác bình', 'Thông tin khác bình', 'Hạng hướng dẫn viên bình', 'Thành tích bình', 'O5Npx9Ms', '415125', '1', '1', '1', '2018-03-13 09:53:23', '2018-03-25 08:48:05', '13', '25', null, '1', null, '3', '100003', '155aaa4762e6ce6', '2018-03-15 10:13:54', '1', '17', '43', '2018-03-14 10:26:59', '2018-03-12 04:34:50', '1', null, null, 'C00031', null, '7', '0');
INSERT INTO `members` VALUES ('2', 'Nguyễn Thị Mai 13032018_outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520909827_5aa73e031a8872.78818463.jpeg', '1987-06-12 00:00:00', '2', '157157157', '2018-04-04 00:00:00', '112098866', '2018-02-23 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '1', '2', '2015', '31', null, null, null, null, 'Eh2BqgzR', '290805', '1', '1', '1', '2018-03-13 10:00:14', '2018-03-19 08:44:49', '10', '65', null, '1', null, null, null, null, null, '1', '44', '43', '2018-03-13 07:17:28', '2018-03-13 06:36:31', '2', null, null, 'C00010', null, '7', '0');
INSERT INTO `members` VALUES ('3', 'abc1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910196_5aa73f74c77975.74480818.png', '2008-05-06 00:00:00', '1', '111111119', '2018-04-04 00:00:00', '111111119', '2018-02-09 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '1', '1', '2014', '13', null, null, null, null, 'ZgOKvkOw', '040461', '1', '1', '1', '2018-03-13 10:05:23', '2018-03-19 08:46:50', '10', '1', null, '1', null, '3', '100002', '155aa7868902d5a', '2018-03-13 08:06:33', '4', '44', '43', '2018-03-13 03:45:55', '2018-03-13 03:44:34', '1', null, null, 'C00003', null, '1', '0');
INSERT INTO `members` VALUES ('4', 'abc2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910601_5aa741091ede56.60651325.png', '2009-02-06 00:00:00', '1', '111111118', '2018-04-05 00:00:00', '111111118', '2018-02-08 00:00:00', 'HN', 'sdfsdf', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '2', '2015', '2', null, null, null, null, 'yJiT0SRy', '537477', '1', '1', '1', '2018-03-13 10:10:18', '2018-03-16 15:00:39', '13', '25', null, '1', null, '3', '200001', '155aa786974b440', '2018-03-13 08:06:47', '4', '53', '52', '2018-03-13 03:29:57', '2018-03-13 03:22:59', null, null, null, 'C00002', null, '2', '0');
INSERT INTO `members` VALUES ('5', 'Nguyễn Thị Mai 12032018_nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910071_5aa73ef73e9a37.58212534.jpeg', '1987-06-12 00:00:00', '2', '158158158', '2018-04-04 00:00:00', '112098866', '2018-02-14 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.comend', '2', '1', '2015', '31', null, null, null, null, 'cDyZVslI', '377231', '1', '1', '1', '2018-03-13 10:11:38', '2018-03-25 05:59:22', '4', '1', null, null, null, '3', '200002', '155aa7869bb08b4', '2018-03-13 08:06:51', '6', '44', '43', '2018-03-13 07:14:23', '2018-03-13 06:36:38', null, '2018-03-25 05:30:48', '1', 'C00009', null, '7', '0');
INSERT INTO `members` VALUES ('6', 'abc3', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910634_5aa7412a0c2a93.69600324.png', '2012-01-27 00:00:00', '1', '111111117', '2018-04-04 00:00:00', '111111117', '2018-02-14 00:00:00', 'HN', 'sadfsdf', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '2', '2014', '7', null, null, null, null, 'wjE37fzM', '337336', '1', '1', '1', '2018-03-13 10:11:58', '2018-03-16 16:43:46', '13', '25', null, '1', null, '3', '200003', '155aa786a3ca16e', '2018-03-13 08:06:59', '9', '53', '52', '2018-03-13 03:29:20', '2018-03-13 03:22:52', null, null, null, 'C00001', null, '2', '0');
INSERT INTO `members` VALUES ('7', 'Nguyễn Thị Mai 13032018_tai diem', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910754_5aa741a2063095.01134530.jpeg', '1987-06-12 00:00:00', '2', '159159159', null, '112098866', '2017-04-04 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', null, 'maint18@viettel.com.vn', null, '3', '1', '2015', '5', null, null, null, null, 'itgNoKuH', '533886', '1', '1', '1', '2018-03-13 10:15:35', '2018-03-26 14:55:31', '9', '1', null, '1', null, '3', '300005', '155ab909e38f0a2', '2018-03-26 14:55:31', '1', '44', '43', '2018-03-13 07:14:10', '2018-03-13 06:36:46', null, null, null, 'C00008', null, '7', '0');
INSERT INTO `members` VALUES ('8', 'Nguyễn Thị mai 13032018_test ho so', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520912102_5aa746e6aa7ad6.23260536.jpeg', '2008-03-13 00:00:00', '2', '125125125', '2018-04-05 00:00:00', 'sdfasdfasdf', '2018-02-08 00:00:00', 'sdfasdf', 'ádfasdf', 'ádfasdfa', '0978429296', null, 'maint18@viettel.com.vn', null, '3', '2', '2018', '1', null, null, null, null, 'rZC02T4o', '918329', '1', '1', '1', '2018-03-13 10:36:30', '2018-03-16 14:59:59', '13', '25', null, '1', null, '3', '300002', '155aa786a995b2e', '2018-03-13 08:07:05', '1', '44', '43', '2018-03-13 07:13:56', '2018-03-13 06:36:53', '1', null, null, 'C00007', null, '2', '0');
INSERT INTO `members` VALUES ('9', 'abc4', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520916141_5aa756ad53b774.87680943.png', '2008-01-13 00:00:00', '2', '111111114', '2018-04-12 00:00:00', '111111114', '2018-02-08 00:00:00', 'hn', 'dsfdsf', 'sdfdsf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '2', '2015', '5', null, null, null, null, 'AuWSvo5q', '678269', '1', '1', '1', '2018-03-13 11:42:48', '2018-03-16 14:59:59', '13', '25', null, '1', null, '3', '200004', '155aa786acdb8c3', '2018-03-13 08:07:08', '2', '60', '43', '2018-03-13 07:11:06', '2018-03-13 06:37:01', null, null, null, 'C00005', null, '1', '0');
INSERT INTO `members` VALUES ('10', 'abc5', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520924242_5aa776520efc94.72533112.png', '2012-06-27 00:00:00', '1', '111111113', '2018-04-19 00:00:00', '111111113', '2018-02-22 00:00:00', 'HN', 'sdfsdf', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '2', '2015', '8', null, null, null, null, 'vgLq01YC', '750686', '1', '1', '1', '2018-03-13 14:00:42', '2018-03-16 14:59:59', '13', '25', null, '1', null, '3', '200005', '155aa786b066609', '2018-03-13 08:07:12', '9', '60', '59', '2018-03-13 07:10:52', '2018-03-13 07:10:09', null, null, null, 'C00004', null, '1', '0');
INSERT INTO `members` VALUES ('11', 'Nguyễn Thị Mai 1303 _td', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520924322_5aa776a27461f1.96027366.jpeg', '2009-03-13 00:00:00', '1', '145145145', '2018-04-05 00:00:00', '112098866', '2018-02-01 00:00:00', 'CA TP Hà nội', 'sdfasf', 'ádfasf', '0978429296', null, 'maint18@viettel.com.vn', null, '2', '1', '2015', '9', null, null, null, null, 'Rh7JgSKF', '053259', '1', '1', '1', '2018-03-13 14:01:07', '2018-03-14 16:41:26', '5', '1', null, '1', null, null, null, null, null, '6', '44', '43', '2018-03-14 08:47:01', '2018-03-13 09:37:43', null, null, '1', 'C00022', null, '4', '0');
INSERT INTO `members` VALUES ('12', 'abc6', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520925126_5aa779c62515f7.62061228.png', '2008-06-13 00:00:00', '2', '111111112', '2018-04-24 00:00:00', '111111112', '2018-02-15 00:00:00', 'hn', 'sdfsdfds', 'ádfsafd', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '2', '2015', '15', null, null, null, null, 'PC2EeI3N', '344651', '1', '1', '1', '2018-03-13 14:13:37', '2018-03-14 11:00:00', '13', '25', null, '1', null, '3', '200006', '155aa786b3d7f5a', '2018-03-13 08:07:15', '6', '60', '59', '2018-03-13 07:25:25', '2018-03-13 07:23:47', null, null, null, 'C00014', null, '1', '0');
INSERT INTO `members` VALUES ('13', 'abc7', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520925411_5aa77ae338ecc5.08126084.png', '2012-02-27 00:00:00', '1', '111111111', '2018-04-04 00:00:00', '111111111', '2018-01-31 00:00:00', 'hn', 'sdfsdfds', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '2', '2015', '4', null, null, null, null, 'AiXTC0KA', '966495', '1', '1', '1', '2018-03-13 14:17:14', '2018-03-14 11:00:00', '13', '25', null, '1', null, '3', '200007', '155aa786b76023d', '2018-03-13 08:07:19', '3', '60', '59', '2018-03-13 07:25:13', '2018-03-13 07:23:53', null, null, null, 'C00013', null, '1', '0');
INSERT INTO `members` VALUES ('14', 'abc8', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520925624_5aa77bb8c88a67.25408616.png', '2012-01-28 00:00:00', '1', '111111110', '2018-04-11 00:00:00', '11111110', '2018-02-13 00:00:00', 'HN', 'sdfsd', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2015', '12', null, null, null, null, 'ORtj0TUS', '357090', '1', '1', '1', '2018-03-13 14:20:42', '2018-03-14 11:00:00', '13', '25', null, '1', null, '3', '200008', '155aa7eb42355a4', '2018-03-13 15:16:18', '7', '60', '59', '2018-03-13 07:24:50', '2018-03-13 07:23:59', null, null, null, 'C00012', null, '1', '0');
INSERT INTO `members` VALUES ('15', 'abcd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520925662_5aa77bde6d8a10.88546578.png', '2008-01-10 00:00:00', '2', '101111111', '2018-04-11 00:00:00', '101111111', '2018-02-21 00:00:00', 'HN', 'sdfsdf', 'sdfsdfsd', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2014', '15', null, null, null, null, 'n3XJNpwD', '695511', '1', '1', '1', '2018-03-13 14:22:31', '2018-03-15 17:24:35', '13', '25', null, '1', null, '3', '200020', '155aaa475e2902b', '2018-03-15 10:13:50', '2', '60', '59', '2018-03-13 07:24:38', '2018-03-13 07:24:05', null, null, null, 'C00011', null, '11', '0');
INSERT INTO `members` VALUES ('16', 'tran thi thanh ha update bo sung yemail', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520954904_5aa7ee18e17c42.56431147.jpeg', '2013-06-07 00:00:00', '2', '111111421', null, '111111421', '2018-02-09 00:00:00', '111111421', '111111421', '111111421', '111111421', null, 'tranha084@gmail.com', null, '3', '1', '2018', '31', 'adadada', 'afafa', null, 'afaf', '8K8g3vDq', '230213', '1', '1', '1', '2018-03-13 22:30:01', '2018-03-16 15:39:20', '1', '1', null, null, null, null, null, null, null, '2', null, '24', null, '2018-03-16 08:38:19', null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('17', 'Viettel Test wwww', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/19/img_1521424051_5aaf16b3407510.12927624.png', '1980-01-14 00:00:00', '2', '100100011', '2019-07-28 00:00:00', '123456789', '2016-06-29 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '5', null, null, null, null, 'Ontt4jFC', '815776', '1', '1', '1', '2018-03-14 09:38:08', '2018-03-19 08:50:33', '13', '25', null, '1', null, '3', '200009', '155aa89959e7f6b', '2018-03-14 03:39:05', '2', '44', '43', '2018-03-14 03:04:01', '2018-03-14 02:54:47', null, null, null, 'C00015', null, '1', '0');
INSERT INTO `members` VALUES ('18', '123', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520995370_5aa88c2ad091d4.55577605.png', '2009-03-27 00:00:00', '1', '333333339', '2018-04-04 00:00:00', '33333339', '2018-01-31 00:00:00', 'HN', 'sfdsf', 'dfsdfd', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2013', '12', null, null, null, null, 'Kc0kmX9Y', '545354', '1', '1', '1', '2018-03-14 09:43:12', '2018-03-15 17:57:29', '13', '25', null, '1', null, '3', '200019', '155aaa47593d70a', '2018-03-15 10:13:45', '5', '44', '59', '2018-03-14 08:49:28', '2018-03-14 07:35:31', null, null, null, 'C00030', '4', '1', '0');
INSERT INTO `members` VALUES ('19', '123456', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520998304_5aa897a0b0f934.61956797.png', '2008-06-07 00:00:00', '2', '333333338', '2018-04-04 00:00:00', '333333338', '2018-02-07 00:00:00', 'hn', 'sdfsd', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '1', '1', '2014', '11', null, null, null, null, 'p1ZhVaOP', '352212', '1', '1', '1', '2018-03-14 10:32:20', '2018-03-25 04:57:57', '4', '1', null, null, null, null, null, null, null, '4', '60', '59', '2018-03-14 08:29:07', '2018-03-14 07:20:39', '1', null, '1', 'C00021', null, '1', '0');
INSERT INTO `members` VALUES ('20', '1234567', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520998448_5aa89830333874.71676028.png', '2008-05-06 00:00:00', '1', '333333337', '2018-04-04 00:00:00', '333333337', '2018-02-08 00:00:00', 'HN', 'sadasd', 'ádasd', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '14', null, null, null, null, 'FaprSEwt', '960842', '1', '1', '1', '2018-03-14 10:34:30', '2018-03-14 10:34:30', '1', '2', null, null, null, null, null, null, null, '4', null, null, null, null, null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('21', 'Nguyễn Thị Mai 1403 test nơi đăng ký sinh hoạt', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520998492_5aa8985d0022a8.21722310.jpeg', '2008-03-14 00:00:00', '1', '146146146', '2018-04-05 00:00:00', '112098866', '2018-02-09 00:00:00', 'sdfasffsd', 'ádfasdf', 'ádfasdf', '0978429296', null, 'maint18@viettel.com.vn', null, '2', '1', '2018', '31', null, null, null, null, 'l4b5Ol9d', '683828', '1', '1', '1', '2018-03-14 10:37:44', '2018-03-19 14:54:35', '8', '35', null, null, null, null, null, null, null, '1', null, '59', null, '2018-03-14 10:32:18', null, null, null, null, null, '15', '0');
INSERT INTO `members` VALUES ('22', 'Nguyễn Thị Mai 13032018_outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520998764_5aa8996cd76048.57500578.jpeg', '2009-02-01 00:00:00', '1', '136136136', '2018-04-04 00:00:00', '112098866', '2018-02-02 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '3', '1', '2018', '2', null, null, null, null, 'y22z88z7', '930569', '1', '1', '1', '2018-03-14 10:40:54', '2018-03-15 18:00:42', '13', '61', null, '1', null, '3', '300004', '155aaa475652803', '2018-03-15 10:13:42', '1', '44', '43', '2018-03-14 03:55:27', '2018-03-14 03:51:56', '2', null, null, 'C00016', null, '15', '0');
INSERT INTO `members` VALUES ('23', 'test1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520999922_5aa89df264c614.53045359.png', '2012-06-26 00:00:00', '1', '444444449', '2018-04-05 00:00:00', '444444449', '2018-02-01 00:00:00', 'Hà Nội', 'dsfsd', 'fsdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '8', null, null, null, null, '4sOoktYI', '453199', '1', '1', '1', '2018-03-14 10:59:01', '2018-03-16 10:43:03', '13', '1', null, '1', null, '3', '200018', '155aaa4753860b6', '2018-03-15 10:13:39', '8', '44', '59', '2018-03-14 08:49:18', '2018-03-14 06:59:11', null, null, null, 'C00029', null, '1', '0');
INSERT INTO `members` VALUES ('24', 'test2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000092_5aa89e9cab2b26.23505432.png', '2012-05-27 00:00:00', '1', '444444448', '2018-04-05 00:00:00', '444444448', '2018-02-15 00:00:00', 'HN', 'sdfsd', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '12', null, null, null, null, '2GkJkifL', '933453', '1', '1', '1', '2018-03-14 11:01:51', '2018-03-16 10:45:17', '13', '1', null, '1', null, '3', '200017', '155aaa4750c709c', '2018-03-15 10:13:36', '8', '44', '59', '2018-03-14 08:49:05', '2018-03-14 07:35:14', null, null, null, 'C00028', null, '1', '0');
INSERT INTO `members` VALUES ('25', 'test3', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000157_5aa89edd3e2789.18421697.png', '2009-06-27 00:00:00', '1', '444444447', '2018-04-04 00:00:00', '444444447', '2018-02-08 00:00:00', 'HN', 'dsfds', 'sdfds', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '14', null, null, null, null, 'V1yM0kts', '655372', '1', '1', '1', '2018-03-14 11:03:35', '2018-03-16 16:01:06', '5', '1', null, '1', null, '3', '200016', '155aaa474a1197d', '2018-03-15 10:13:30', '9', '44', '59', '2018-03-14 08:48:41', '2018-03-14 04:34:57', null, null, '1', 'C00027', null, '1', '0');
INSERT INTO `members` VALUES ('26', 'Nguyễn Thị mai 14032018_inbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000074_5aa89e8ab8c867.46209850.jpeg', '1988-06-12 00:00:00', '2', '135135135', '2018-04-05 00:00:00', '112098866', '2018-02-24 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', null, 'maint18@viettel.com.vn', null, '1', '1', '2018', '31', 'Các kỹ năng khác', 'Thông tin khác', 'Hạng hướng dẫn viên', 'Thành tích', 'UP3Kx9Uh', '836637', '1', '1', '1', '2018-03-14 11:04:18', '2018-03-15 16:36:47', '2', '18', null, null, null, null, null, null, null, '7', null, '43', null, '2018-03-15 09:36:47', '1', null, null, null, null, '8', '0');
INSERT INTO `members` VALUES ('27', 'test4', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000306_5aa89f721a09c5.11702751.png', '2012-02-07 00:00:00', '1', '444444446', '2018-04-27 00:00:00', '444444446', '2018-02-08 00:00:00', 'hn', 'dsfds', 'ssdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2014', '11', null, null, null, null, '09NH0ipZ', '836505', '1', '1', '1', '2018-03-14 11:05:23', '2018-03-16 10:49:43', '13', '3', null, '1', null, '3', '200015', '155aaa47476b302', '2018-03-15 10:13:27', '1', '44', '59', '2018-03-14 08:48:29', '2018-03-14 04:34:16', null, null, null, 'C00026', null, '1', '0');
INSERT INTO `members` VALUES ('28', 'test5', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000510_5aa8a03e918673.63219191.png', '2009-06-06 00:00:00', '1', '444444445', '2018-04-04 00:00:00', '44444445', '2018-02-08 00:00:00', 'hn', 'sdfsd', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '11', null, null, null, null, 'OlKGWayZ', '354102', '1', '1', '1', '2018-03-14 11:08:51', '2018-03-16 14:58:58', '13', '1', null, '1', null, '3', '200014', '155aaa47445d11e', '2018-03-15 10:13:24', '6', '44', '59', '2018-03-14 08:48:18', '2018-03-14 04:33:45', null, null, null, 'C00025', null, '1', '0');
INSERT INTO `members` VALUES ('29', 'test6', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000611_5aa8a0a3be9418.75209168.png', '2009-06-28 00:00:00', '2', '444444444', '2018-04-05 00:00:00', '444444444', '2018-02-07 00:00:00', 'HN', 'sdfsd', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '3', null, null, null, null, 'hIaUiJDZ', '218236', '1', '1', '1', '2018-03-14 11:10:31', '2018-03-26 16:12:04', '5', '1', null, '1', null, null, null, null, null, '9', '44', '59', '2018-03-14 08:48:05', '2018-03-14 04:33:07', null, null, null, 'C00024', null, '1', '0');
INSERT INTO `members` VALUES ('30', 'Nguyễn Thị Mai 14032018_outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000303_5aa89f6f1dde48.51656426.jpeg', '1988-06-01 00:00:00', '1', '126126126', '2018-04-04 00:00:00', '112098866', '2018-02-24 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', null, '1', '2', '2015', '8', 'Kỹ năng khác', 'Thông tin khác', 'Hạng hướng dẫn viên', 'Thành tích', 'U7Trx8k2', '715647', '1', '1', '1', '2018-03-14 11:11:48', '2018-03-16 10:10:16', '22', '65', null, null, null, null, null, null, null, '7', '44', '43', '2018-03-16 03:10:16', '2018-03-15 09:32:45', '2', null, null, null, null, '4', '0');
INSERT INTO `members` VALUES ('31', 'test7 _ chị hà xinh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000718_5aa8a10e36ca45.30924792.png', '2010-01-01 00:00:00', '2', '190190190', '2018-04-06 00:00:00', '112098866 sửa', '2018-01-03 00:00:00', 'hn sửa', 'sadfsd sửa', 'sdfsdf sửa', '0978429296', '0976831235', 'maint18@viettel.com.vn', 'thuyenht@viettel.com.vn', '3', '1', '2014', '31', '8 sửa', '9 sửa', '10 sửa', '11 sửa', 'knnwWBX1', '210779', '1', '1', '1', '2018-03-14 11:12:19', '2018-03-16 16:28:16', '13', '18', null, '1', null, '3', '200022', '155aab7aa623476', '2018-03-16 08:04:54', '8', '44', '59', '2018-03-14 08:47:14', '2018-03-14 04:32:26', null, null, null, 'C00023', null, '7', '0');
INSERT INTO `members` VALUES ('32', 'Nguyễn Thị Mai 14032018 nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000776_5aa8a148b02fa4.58657616.jpeg', '2007-02-09 00:00:00', '2', '178178178', '2018-04-05 00:00:00', '112098866', '2018-02-09 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '2', '1', '2014', '29', null, null, null, null, 'beskTTDx', '118388', '1', '1', '1', '2018-03-14 11:15:41', '2018-03-25 05:54:12', '4', '26', null, null, null, null, null, null, null, '7', '44', '59', '2018-03-16 02:41:55', '2018-03-15 09:31:01', null, null, '1', 'C00038', null, '4', '0');
INSERT INTO `members` VALUES ('33', 'Nguyễn Thị Mai 14032018_tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000979_5aa8a213a90524.43442784.jpeg', '2010-03-28 00:00:00', '1', '154154154', null, '112098866', '2018-02-04 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '3', '2', '2014', '31', null, null, null, null, 'CMdLSjUc', '786069', '1', '1', '1', '2018-03-14 11:18:13', '2018-03-16 15:00:11', '13', '65', null, '1', null, '3', '300003', '155aaa474190eb1', '2018-03-15 10:13:21', '7', '44', '43', '2018-03-14 07:45:44', '2018-03-14 07:23:46', null, null, null, 'C00020', null, '4', '0');
INSERT INTO `members` VALUES ('34', 'Viettel 1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521001094_5aa8a286a6e802.64848611.png', '2008-03-14 00:00:00', '1', '123456789', '2018-09-02 00:00:00', '123456789', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '5', null, null, null, null, 'Hvz8oAtX', '959846', '1', '1', '1', '2018-03-14 11:20:25', '2018-03-16 15:01:14', '13', '1', null, '1', null, '3', '200010', '155aa8e0bd5d225', '2018-03-14 08:43:41', '2', '44', '43', '2018-03-14 04:31:24', '2018-03-14 04:29:26', '1', null, null, 'C00018', null, '1', '0');
INSERT INTO `members` VALUES ('35', 'Viettel 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521001300_5aa8a354bf9383.26169011.png', '2008-03-14 00:00:00', '1', '123456781', '2018-05-06 00:00:00', '123456789', '2018-02-16 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '5', null, null, null, null, '0F8pxkH4', '728027', '1', '1', '1', '2018-03-14 11:22:59', '2018-03-16 15:01:14', '13', '1', null, '1', null, '3', '200011', '155aaa2a2f7690a', '2018-03-15 08:09:19', '8', '44', '43', '2018-03-14 04:31:40', '2018-03-14 04:29:38', null, null, null, 'C00019', null, '1', '0');
INSERT INTO `members` VALUES ('36', 'Viettel 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521001456_5aa8a3f0885c33.93377903.png', '1980-03-14 00:00:00', '1', '123456782', '2018-05-06 00:00:00', '123456789', '2018-02-24 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '5', null, null, null, null, 'q2Qgf7Vc', '543453', '1', '1', '1', '2018-03-14 11:25:35', '2018-03-16 15:01:14', '13', '1', null, '1', null, '3', '200013', '155aaa473e484cf', '2018-03-15 10:13:18', '2', '44', '43', '2018-03-14 04:31:04', '2018-03-14 04:29:45', null, null, null, 'C00017', null, '1', '0');
INSERT INTO `members` VALUES ('37', 'fdvfd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521023353_5aa8f979cbdc89.65386207.png', '2008-03-14 00:00:00', '1', '435345344', null, 'dgfdg', '2018-02-08 00:00:00', 'gvfdgdf', '4543', '345435', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '5', null, null, null, null, '4yEEbq6A', '391206', '1', '1', '1', '2018-03-14 17:30:04', '2018-03-25 06:12:17', '4', '1', null, null, null, null, null, null, null, '7', '44', '43', '2018-03-14 10:32:12', '2018-03-14 10:30:52', null, null, null, 'C00032', null, '1', '0');
INSERT INTO `members` VALUES ('38', 'aaaaaaaaaaa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521023457_5aa8f9e192b5a3.78287630.png', '2009-02-13 00:00:00', '1', '555555559', '2018-04-05 00:00:00', '555555559', '2018-02-08 00:00:00', 'hn', 'dsfsd', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2015', '14', null, null, null, null, 'ACTJNfDR', '564806', '1', '1', '1', '2018-03-14 17:31:33', '2018-03-22 16:09:46', '9', '1', null, '1', null, '3', '200023', '155ab3d54a0c39b', '2018-03-22 16:09:46', '3', '60', '59', '2018-03-16 03:53:14', '2018-03-14 10:32:08', null, null, null, 'C00044', null, '1', '0');
INSERT INTO `members` VALUES ('39', 'Tran Thi Thanh Ha', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521085962_5aa9ee0ad475d4.46558244.png', '2013-06-15 00:00:00', '2', '123567895', '2018-04-04 00:00:00', '123567895', '2018-02-25 00:00:00', '123567895123567895', '123567895', '123567895', '123567895', null, 'tranha084@gmail.com', null, '1', '1', '2018', '19', null, null, null, null, 'TmYqLtDo', '650053', '1', '1', '1', '2018-03-15 10:54:42', '2018-03-26 14:37:41', '9', '2', null, '1', null, '3', '100004', '155ab905b5ef300', '2018-03-26 14:37:41', '2', null, null, null, null, '2', null, null, null, null, '2', '0');
INSERT INTO `members` VALUES ('40', 'Viettel 4', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521086361_5aa9ef99a931c1.10860793.jpeg', '2013-03-22 00:00:00', '1', '123456543', '2018-05-06 00:00:00', '12345678', '2018-01-31 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2013', '31', null, null, null, null, 's5o3yuac', '766752', '1', '1', '1', '2018-03-15 11:21:28', '2018-03-15 11:42:56', '2', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('41', 'viettel 5', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521087979_5aa9f5eb5da5c8.99504258.jpeg', '2013-03-22 00:00:00', '1', '123122331', '2018-04-28 00:00:00', '21321312312321321321', '2018-02-17 00:00:00', 'Hà Nội', 'ê', 'fwfew', '0977635498', '0977635498', 'xuantoi90@gmail.com', 'thuyenht@viettel.com.vn', '2', '2', '2013', '30', null, null, null, null, 'tkfAAuof', '996285', '1', '1', '1', '2018-03-15 11:34:58', '2018-03-19 14:53:48', '14', '25', null, null, null, null, null, null, null, '6', null, '43', null, '2018-03-15 07:42:44', null, null, null, null, '3', '1', '0');
INSERT INTO `members` VALUES ('42', 'Nguyễn Thị Mai 15032018 _ inbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521087489_5aa9f401692d25.28679830.jpeg', '1987-06-12 00:00:00', '2', '167167167', '2018-04-05 00:00:00', '112098866', '2018-02-08 00:00:00', 'CA TP HN', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', null, 'maint18@viettel.com.vn', null, '1', '1', '2018', '31', 'Các kỹ năng chuyên môn khác', 'Thông tin khác', 'Hạng hướng dẫn viên', 'Thành tích', 'S1KWcUko', '341469', '1', '1', '1', '2018-03-15 11:30:19', '2018-03-25 06:51:20', '4', '1', null, null, null, null, null, null, null, '7', '44', '43', '2018-03-16 02:39:46', '2018-03-15 09:27:31', '1', null, null, 'C00036', null, '2', '0');
INSERT INTO `members` VALUES ('43', 'test bổ sung 1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521088297_5aa9f7296fcaf7.17732179.png', '2012-01-06 00:00:00', '1', '999999999', null, '999999999', '2018-01-31 00:00:00', 'Hà Nội', 'dsfsdfds', 'sdfsdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '3', '1', '2014', '31', null, null, null, null, 'Bl9PmX9m', '450974', '1', '1', '1', '2018-03-15 11:32:09', '2018-03-25 06:30:27', '4', '1', null, null, null, null, null, null, null, '4', '60', '59', '2018-03-16 03:48:31', '2018-03-15 07:31:15', null, null, null, 'C00042', null, '1', '0');
INSERT INTO `members` VALUES ('44', 'test bổ sung 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521088561_5aa9f8318dfa69.34264909.png', '2012-05-14 00:00:00', '1', '999999998', '2018-04-05 00:00:00', '999999998', '2018-01-31 00:00:00', 'Hà Nội', 'sdfsd', 'adfsadfds', '0976831235', null, 'tranha084@gmail.com', null, '2', '1', '2014', '16', null, null, null, null, 'IcQ4eNAG', '094893', '1', '1', '1', '2018-03-15 11:36:29', '2018-03-26 15:56:18', '6', '1', null, '1', null, null, null, null, null, '3', '60', '59', '2018-03-16 03:47:55', '2018-03-15 07:31:34', null, null, null, 'C00041', null, '1', '0');
INSERT INTO `members` VALUES ('45', 'Nguyễn Thị Mai 15032018', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521088608_5aa9f860021707.21099739.jpeg', '1988-06-01 00:00:00', '1', '187187187', '2018-04-04 00:00:00', '112098866', '2018-02-16 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '1', '2', '2018', '9', null, null, null, null, 'Cfq5CARm', '552927', '1', '1', '1', '2018-03-15 11:39:17', '2018-03-26 15:13:06', '6', '65', null, '1', null, null, null, null, null, '3', '44', '43', '2018-03-16 02:43:42', '2018-03-15 09:27:18', '2', null, null, 'C00039', null, '2', '0');
INSERT INTO `members` VALUES ('46', 'test bổ sung 3', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521088654_5aa9f88e426f73.60831979.png', '2012-06-27 00:00:00', '1', '999999997', '2018-04-26 00:00:00', '999999997', '2018-02-09 00:00:00', 'Hà Nội', 'sdasdas', 'ádasdasdasdasdas', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '14', null, null, null, null, 'mFBOxi7f', '415095', '1', '1', '1', '2018-03-15 11:39:27', '2018-03-25 06:37:04', '4', '1', null, null, null, null, null, null, null, '5', '60', '59', '2018-03-16 03:47:36', '2018-03-15 07:31:44', null, null, null, 'C00040', null, '1', '0');
INSERT INTO `members` VALUES ('47', 'sâs', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521089533_5aa9fbfdab41b7.34213664.png', '2013-06-15 00:00:00', '1', '535353211', null, '535353211', '2018-02-08 00:00:00', '535353211', '535353211', '535353211', '535353211', null, 'tranha084@gmail.com', null, '3', '1', '2014', '17', null, null, null, null, 'pL0mb4hR', '920641', '1', '0', '0', '2018-03-15 11:52:59', '2018-03-15 11:52:59', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('48', 'Nguyễn Thị Mai 15032018 _ Nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521089540_5aa9fc04cd84a2.49142164.jpeg', '1987-06-12 00:00:00', '2', '148148148', '2018-05-11 00:00:00', '112098866', '2018-02-09 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', null, '2', '1', '2017', '13', null, null, null, null, 'zyvjZwnQ', '078553', '1', '1', '1', '2018-03-15 11:53:16', '2018-03-25 06:53:27', '4', '18', null, null, null, null, null, null, null, '10', '44', '43', '2018-03-16 02:41:41', '2018-03-15 09:27:01', null, null, null, 'C00037', null, '7', '0');
INSERT INTO `members` VALUES ('49', 'Nguyễn Thị Mai 15032018 tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521095722_5aaa142ac1a529.28993798.jpeg', '1987-06-12 00:00:00', '2', '169169169', null, '112098866', '2018-02-08 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', null, '3', '1', '2010', '1', null, null, null, null, 'lYq26pg2', '396767', '1', '1', '1', '2018-03-15 13:40:14', '2018-03-26 16:52:16', '5', '9', null, '1', null, null, null, null, null, '7', '44', '43', '2018-03-16 02:39:13', '2018-03-01 09:22:33', null, null, null, 'C00035', null, '58', '0');
INSERT INTO `members` VALUES ('50', 'Tran Thi Thanh Ha - Check send email', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521096217_5aaa16197fe6a9.26303312.png', '2013-06-15 00:00:00', '1', '678978998', null, '678978998', '2018-02-23 00:00:00', '678978998', '678978998', '678978998', '678978998', null, 'tranha084@gmail.com', null, '3', '2', '2018', '19', null, null, null, null, 'hTEbItEa', '135998', '1', '1', '1', '2018-03-15 13:47:38', '2018-03-15 13:53:37', '14', '25', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, '1', null, '14', '1', '0');
INSERT INTO `members` VALUES ('51', 'viettel 6', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521096785_5aaa18516c5c95.82943798.jpeg', '2013-02-28 00:00:00', '1', '123456788', '2018-05-06 00:00:00', '23213123131321324234', '2018-02-17 00:00:00', 'Hà Nội', 'Hà Nam', 'Nam định', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '2', '1973', '31', 'dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fds', 'dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fds', 'dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fds', 'dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fds', 'ESDJa3gI', '943111', '1', '1', '1', '2018-03-15 13:56:04', '2018-03-19 14:53:25', '14', '25', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 03:19:13', null, null, null, null, '7', '1', '0');
INSERT INTO `members` VALUES ('52', 'viettel 7', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521099017_5aaa2109e09aa4.26538832.jpeg', '2013-03-15 00:00:00', '1', '255342153', '2018-05-06 00:00:00', '32423453543343453543', '2018-02-23 00:00:00', 'Hà Nội', 'hải dương', 'hải phòng', '0977635498', '097763549832432', 'nghianm.dev@gmail.com', 'thuyenht@dsfgdg.ewe', '1', '1', '2018', '30', 'êw', 'ewrwerew', 'ẻwerwe', 'rẻw', 'rrMJ4t0C', '943958', '1', '1', '1', '2018-03-15 14:30:54', '2018-03-15 16:14:40', '2', '1', null, null, null, null, null, null, null, '4', '17', '43', '2018-03-15 09:14:40', '2018-03-15 07:42:04', '1', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('53', 'Tran Thi Thanh Ha - Check send email', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521099182_5aaa21ae76da72.44122017.png', '2013-03-15 00:00:00', '2', '632222222', '2018-04-11 00:00:00', '632222222', '2018-02-23 00:00:00', '632222222632222222632222222', '632222222', '632222222', '632222222', null, 'tranha084@gmail.com', null, '1', '1', '2018', '18', null, null, null, null, 'h8drT2AT', '142559', '1', '1', '1', '2018-03-15 14:34:35', '2018-03-16 11:31:30', '10', '1', null, null, null, null, null, null, null, '2', null, '17', null, '2018-03-16 04:29:57', '1', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('54', 'Tran Thi Thanh Ha 222', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521099437_5aaa22ade093d0.43878124.png', '2013-06-15 00:00:00', '2', '323232323', '2018-04-04 00:00:00', '323232323', '2018-02-15 00:00:00', '323232323', '323232323', '323232323', '323232323', null, 'tranha084@gmail.com', null, '1', '1', '2018', '12', null, null, null, null, 'Ze8tuug8', '970938', '1', '1', '1', '2018-03-15 14:38:29', '2018-03-23 09:33:27', '4', '1', null, '1', null, null, null, null, null, '1', '60', '43', '2018-03-16 03:52:49', '2018-03-16 03:20:32', '2', null, null, 'C00043', null, '18', '0');
INSERT INTO `members` VALUES ('55', 'viettel 8', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521105233_5aaa3951ead131.12556780.jpeg', '2013-03-15 00:00:00', '1', '123456712', '2018-06-10 00:00:00', '678', '2018-02-11 00:00:00', 'Hà Nội', 'hà nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2018', '31', null, null, null, null, '61O4YPJ8', '069910', '1', '1', '1', '2018-03-15 16:22:42', '2018-03-25 06:31:44', '4', '1', null, null, null, null, null, null, null, '2', '60', '59', '2018-03-15 09:44:29', '2018-03-15 09:40:59', null, null, '1', null, null, '1', '0');
INSERT INTO `members` VALUES ('56', 'fdsfdsfsd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521105921_5aaa3c0160c8f4.25210043.jpeg', '2013-03-15 00:00:00', '1', '123456714', '2018-04-15 00:00:00', '34', '2018-02-01 00:00:00', 'ểwr', 'ewr', 'rrerwe', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2015', '12', null, null, null, null, 'vFxcZwIV', '986297', '1', '1', '0', '2018-03-15 16:27:16', '2018-03-15 16:27:52', '1', '1', null, null, null, null, null, null, null, '1', null, null, null, null, '2', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('57', 'thuyên_test1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521106057_5aaa3c89715ce3.22080173.png', '2008-10-13 00:00:00', '1', '888888888', '2018-04-05 00:00:00', '888888888', '2018-02-25 00:00:00', 'HN', 'dsfsdfsd', 'sdfsdfds', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '31', null, null, null, null, '4DmBHGOQ', '471309', '1', '1', '1', '2018-03-15 16:27:59', '2018-03-16 15:01:14', '13', '8', null, '1', null, '3', '200012', '155aaa41259421f', '2018-03-15 09:47:17', '3', '60', '59', '2018-03-15 09:45:45', '2018-03-15 09:37:57', null, null, null, 'C00033', null, '1', '0');
INSERT INTO `members` VALUES ('58', 'fdsfdsfsd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521105921_5aaa3c0160c8f4.25210043.jpeg', '2013-03-15 00:00:00', '1', '123456713', '2018-04-15 00:00:00', '34', '2018-02-01 00:00:00', 'ểwr', 'ewr', 'rrerwe', '0977635498', null, 'ngandt5@viettel.com.vn', 'gvdfdf@fdf.gdf', '1', '1', '2015', '12', null, null, null, null, 'zuS8YFhv', '092237', '1', '0', '0', '2018-03-15 16:28:35', '2018-03-25 07:13:57', '5', '4', null, '1', null, null, null, null, null, '1', null, null, null, null, '2', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('59', 'Viettel 9', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521167144_5aab2b28cfc735.46161204.jpeg', '2013-03-23 00:00:00', '1', '232332243', '2018-04-05 00:00:00', '65473', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2015', '31', '1', '2', '3', '4', 'cNwtglDw', '575706', '1', '1', '1', '2018-03-16 09:29:49', '2018-03-16 10:15:30', '13', '1', null, '1', null, '3', '200021', '155aab366059fc6', '2018-03-16 03:13:36', '2', '44', '43', '2018-03-16 02:35:54', '2018-03-16 02:31:12', null, null, null, 'C00034', null, '1', '0');
INSERT INTO `members` VALUES ('60', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt q', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521167765_5aab2d955deb76.02266190.png', '2012-05-27 00:00:00', '1', '555555551', '2018-04-05 00:00:00', '55555555945454545454', '2018-02-22 00:00:00', 'Hà Nội', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt qua được nỗi ám ảnh của cuộc thảm sát Mỹ Lai.\r\nMike Hastie, một cựu binh và cũng là phóng viên chiến trường thời chiến tranh Việt Nam, rút trong túi ra', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt qua được nỗi ám ảnh của cuộc thảm sát Mỹ Lai.\r\nMike Hastie, một cựu binh và cũng là phóng viên chiến trường thời chiến tranh Việt Nam, rút trong túi ra', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '12', null, null, null, null, 'go8ptAst', '332869', '1', '1', '1', '2018-03-16 09:36:30', '2018-03-16 09:44:35', '2', '8', null, null, null, null, null, null, null, '5', null, '59', null, '2018-03-16 02:44:35', null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('61', 'Viettel 9', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521172808_5aab4148712340.65793374.jpeg', '2008-03-01 00:00:00', '1', '232332241', '2018-04-29 00:00:00', '65473', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2013', '31', '1', '2', '3', '4', 'EFoT5Tsi', '252351', '1', '1', '1', '2018-03-16 11:01:13', '2018-03-16 11:03:18', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 04:03:18', '1', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('62', 'Viettel 10', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521173341_5aab435db17c56.13080195.png', '2008-03-01 00:00:00', '1', '232332240', '2018-04-29 00:00:00', '65473', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2013', '31', '1', '2', '3', '4', 'k8jXFrIb', '236050', '1', '1', '1', '2018-03-16 11:10:22', '2018-03-16 11:19:07', '7', '1', null, null, null, null, null, null, null, '2', null, '17', null, '2018-03-16 04:19:07', '2', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('63', 'Viettel 10', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521174851_5aab4943cc0cf8.42700808.jpeg', '2008-03-01 00:00:00', '1', '232332212', '2018-04-29 00:00:00', '65473', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2013', '31', '1', '2', '3', '4', 'yt72n8Wv', '333147', '1', '0', '0', '2018-03-16 11:34:15', '2018-03-16 11:42:48', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '2', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('64', 'Nguyễn Thị MaiNguyễn Thị MaiNguyễn Thị MaiNguyễEnd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521174852_5aab494418e6e8.66516951.jpeg', '2009-03-08 00:00:00', '1', '124124124', '2018-04-12 00:00:00', '11209886611209886610', '2018-02-07 00:00:00', 'CA TP Hà nộiCA TP Hà nộiCA TP Hà nộiCA TP Hà nộEnd', 'Địa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trEnđ', 'Địa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịEnd', '0978429296', null, 'maint18@viettel.com.vn', null, '1', '1', '2015', '31', null, null, null, null, '4bSdIkbC', '776172', '1', '0', '0', '2018-03-16 11:34:40', '2018-03-16 11:34:55', '1', '1', null, null, null, null, null, null, null, '6', null, null, null, null, '1', null, null, null, null, '16', '0');
INSERT INTO `members` VALUES ('65', 'Xem hồ sơ_Inbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', '2008-03-16 00:00:00', '1', '454654656', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'zVY40fHi', '623115', '1', '1', '1', '2018-03-16 13:33:04', '2018-03-16 16:02:47', '5', '1', null, null, null, null, null, null, null, '2', '44', '43', '2018-03-16 07:21:27', '2018-03-16 07:21:07', '1', null, null, 'C00045', null, '1', '0');
INSERT INTO `members` VALUES ('66', 'Xem hồ sơ_Outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', '2008-03-16 00:00:00', '1', '222222222', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'E5yrZhDT', '318929', '1', '1', '1', '2018-03-16 13:33:53', '2018-03-16 15:33:42', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 08:33:42', '2', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('67', 'Xem hồ sơ_Nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', '2008-03-16 00:00:00', '1', '333333333', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'HxclH76t', '934399', '1', '1', '1', '2018-03-16 13:34:35', '2018-03-16 15:33:27', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 08:33:27', '2', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('68', 'Xem hồ sơ_Tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', '2008-03-16 00:00:00', '1', '555555555', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'fTOn7UXx', '519087', '1', '1', '1', '2018-03-16 13:35:28', '2018-03-16 13:35:28', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('69', '11111111111111111111111111111111111111111111111111', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182376_5aab66a82d3aa8.89669388.png', '2012-06-28 00:00:00', '1', '121212121', '2018-04-11 00:00:00', '11111111111111111111', '2018-02-15 00:00:00', 'Hà Nội', 'sdfsdf', 'sdfsdfsd', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '1', '1', '2014', '31', null, null, null, null, 'j4N1p4F8', '406035', '1', '1', '1', '2018-03-16 13:40:08', '2018-03-16 13:40:08', '1', '37', null, null, null, null, null, null, null, '10', null, null, null, null, '2', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('70', 'Xem hồ sơ_Tại điểm sua', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', '2008-03-16 00:00:00', '1', '666666666', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'bLTuKZx0', '980429', '1', '1', '1', '2018-03-16 13:40:05', '2018-03-16 15:37:33', '1', '18', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 08:33:08', null, null, null, null, null, '11', '0');
INSERT INTO `members` VALUES ('71', 'Nguyễn Thị MaiNguyễn Thị MaiNguyễn Thị MaiNguyễEnd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182473_5aab67090e2454.09445286.jpeg', '2009-03-01 00:00:00', '1', '142142142', '2018-04-05 00:00:00', '11209886611209886610', '2018-02-21 00:00:00', 'CA TP Hà nộiCA TP Hà nộiCA TP Hà nộiCA dddddTPdEnđ', 'Địa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trEnd', 'Địa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịEnd', '0978429296', null, 'maint18@viettel.com.vn', 'sdfaseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeng@gmail.com', '1', '1', '2018', '31', 'Các chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên mEnđ', 'Thông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThôEnđ', 'Hạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dEnd', 'Thành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tEnđ', '4tNUre6d', '669195', '1', '1', '1', '2018-03-16 13:50:02', '2018-03-16 15:02:14', '5', '1', null, '1', null, null, null, null, null, '4', '44', '43', '2018-03-16 08:00:40', '2018-03-16 08:00:09', '1', null, null, 'C00046', null, '4', '0');
INSERT INTO `members` VALUES ('72', 'Xem hồ sơ_Tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', '2008-03-16 00:00:00', '1', '777777777', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'Rxy457NJ', '261062', '1', '1', '1', '2018-03-16 14:28:35', '2018-03-16 14:28:35', '1', '6', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '2', '0');
INSERT INTO `members` VALUES ('73', 'sdasdasd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521185137_5aab7171bfdb67.94907905.jpeg', '2009-03-01 00:00:00', '1', '196196196', null, '112098866', '2018-02-01 00:00:00', 'sdfasdf', 'ádfas', 'sdfasdfa', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '3', '2', '2015', '31', null, null, null, null, 'rhTqKZTs', '782645', '1', '1', '1', '2018-03-16 14:28:31', '2018-03-16 14:28:31', '1', '65', null, null, null, null, null, null, null, '4', null, null, null, null, null, null, null, null, null, '15', '0');
INSERT INTO `members` VALUES ('74', 'Xem hồ sơ_Tại điểm Xem hồ sơ_Tại điểmXem hồ sơ_Tại', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521186969_5aab789974cb56.82377981.jpeg', '2008-03-16 00:00:00', '1', '000000001', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'sDoYf1K2', '766303', '1', '1', '1', '2018-03-16 14:56:36', '2018-03-16 14:56:36', '1', '6', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('75', 'check lap du lieu', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', '2008-03-16 00:00:00', '1', '000000002', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'F0lH2NHD', '086954', '1', '1', '1', '2018-03-16 15:02:02', '2018-03-16 15:02:02', '1', '6', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('76', 'check BSHS tham dinh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', '2008-03-16 00:00:00', '1', '000000003', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'EMZn7cvD', '516109', '1', '1', '1', '2018-03-16 15:19:02', '2018-03-16 15:19:02', '1', '6', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('77', 'check BSHS tham dinh 1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', '2008-03-16 00:00:00', '1', '000000004', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'CUHXgn09', '283157', '1', '1', '1', '2018-03-16 15:19:42', '2018-03-16 15:20:05', '7', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 08:20:05', null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('78', 'check BSHS tham dinh 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521188636_5aab7f1cb429a3.47387654.jpeg', '2008-03-16 00:00:00', '1', '000000005', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'dinhngan.tk61@gmail.com', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'H38rpH1X', '057237', '1', '1', '1', '2018-03-16 15:24:34', '2018-03-19 14:54:55', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('79', 'bổ sung thẩm định lần 2bổ sung thẩm định lần 3', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521191335_5aab89a77e0e21.51858982.png', '2009-01-06 00:00:00', '1', '111131111', '2018-05-05 00:00:00', '11111111111sua3', '2018-01-01 00:00:00', '11111111111sua2', 'bổ sung thẩm định lần 3', 'bổ sung thẩm định lần 3', '0976831235', null, 'thuyenht@viettel.com.vn', null, '1', '1', '2012', '31', 'dsfsdfds sửa', 'dsfsdfds sửa', 'dsfsdfds sửa', 'dsfsdfds sửa', 'X06e98Xj', '300338', '1', '1', '1', '2018-03-16 16:09:44', '2018-03-19 15:00:16', '7', '61', null, null, null, null, null, null, null, '8', null, '43', null, '2018-03-19 08:00:16', '2', null, null, null, null, '7', '0');
INSERT INTO `members` VALUES ('80', 'bổ sung đăng ký', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521196610_5aab9e42252ea7.13632401.png', '2012-05-27 00:00:00', '1', '222322222', '2018-04-18 00:00:00', '222222222', '2018-02-13 00:00:00', 'Hà Nội', 'dsfsdfsd', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '1', '2', '2014', '14', null, null, null, null, 'hqyMmfRu', '061233', '1', '1', '1', '2018-03-16 17:37:31', '2018-03-19 14:54:50', '22', '25', null, null, null, null, null, null, null, '3', '60', '59', '2018-03-16 10:38:54', '2018-03-16 10:38:24', '2', null, null, null, null, '15', '0');
INSERT INTO `members` VALUES ('81', 'viettel 11', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/19/img_1521434269_5aaf3e9dc877f9.89915900.jpeg', '2008-03-19 00:00:00', '2', '131313131', '2018-05-06 00:00:00', '121212121212', '2018-02-02 00:00:00', 'CA Hà Nội', '1. Vào acc kế toán\r\n2. Vào Danh sách hồ sơ\r\n3. Click combo Nơi đăng ký sinh hoạt\r\n==> Mong muốn: hiển thị CLB thuộc hội… khi văn phòng đại diện là trống hoặc Hội Hà Nội', '1. Vào acc kế toán\r\n2. Vào Danh sách hồ sơ\r\n3. Click combo Nơi đăng ký sinh hoạt\r\n==> Mong muốn: hiển thị CLB thuộc hội… khi văn phòng đại diện là trống hoặc Hội Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'ngandt5@viettel.com.vn', '1', '1', '2013', '31', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'XQo7cF11', '712989', '1', '1', '1', '2018-03-19 11:43:25', '2018-03-19 11:43:25', '1', '1', null, null, null, null, null, null, null, '6', null, null, null, null, '1', null, null, null, null, '26', '0');
INSERT INTO `members` VALUES ('82', 'viettel 12_Outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/19/img_1521434670_5aaf402e31be81.55316577.jpeg', '2008-03-19 00:00:00', '2', '232332242', '2018-05-06 00:00:00', '121212121212', '2018-02-02 00:00:00', 'CA Hà Nội', '1. Vào acc kế toán\r\n2. Vào Danh sách hồ sơ\r\n3. Click combo Nơi đăng ký sinh hoạt\r\n==> Mong muốn: hiển thị CLB thuộc hội… khi văn phòng đại diện là trống hoặc Hội Hà Nội', '1. Vào acc kế toán\r\n2. Vào Danh sách hồ sơ\r\n3. Click combo Nơi đăng ký sinh hoạt\r\n==> Mong muốn: hiển thị CLB thuộc hội… khi văn phòng đại diện là trống hoặc Hội Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'ngandt5@viettel.com.vn', '1', '1', '2013', '31', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'TiblfGFo', '745825', '1', '1', '1', '2018-03-19 11:44:45', '2018-03-19 14:59:55', '2', '1', null, null, null, null, null, null, null, '6', null, '43', null, '2018-03-19 07:59:55', '2', null, null, null, null, '1', '0');
INSERT INTO `members` VALUES ('83', 'viettel 13_BSHS thẩm định', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/19/img_1521448853_5aaf77959221c5.35910421.jpeg', '2008-02-13 00:00:00', '2', '141415141', '2018-06-10 00:00:00', '15155', '2018-02-03 00:00:00', 'CA Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2016', '9', '8. Các kỹ năng chuyên môn khác (MC, Game show,...)', '9. Thông tin khác', '10. Hạng hướng dẫn viên', '11. Thành tích (Khen thưởng, giấy khen, bằng khen,...)', '7ZVAdJQP', '560714', '1', '1', '1', '2018-03-19 15:42:44', '2018-03-24 14:54:56', '5', '1', null, '1', null, null, null, null, null, '3', '44', '43', '2018-03-19 09:27:28', '2018-03-19 09:00:32', null, null, null, 'C00047', null, '1', '0');

-- ----------------------------
-- Table structure for members_tmp
-- ----------------------------
DROP TABLE IF EXISTS `members_tmp`;
CREATE TABLE `members_tmp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of members_tmp
-- ----------------------------
INSERT INTO `members_tmp` VALUES ('18', '2', '{\"fullName\":\"Nguy\\u1ec5n Th\\u1ecb Mai - update profile\",\"gender\":\"2\",\"birthday\":\"1987-06-12\",\"touristGuideCode\":\"157157157\",\"expirationDate\":\"2018-04-04\",\"cmtCccd\":\"112098866\",\"dateIssued\":\"2018-02-23\",\"issuedBy\":\"CA TP H\\u00e0 n\\u1ed9i\",\"permanentAddress\":\"\\u0110\\u1ecba ch\\u1ec9 th\\u01b0\\u1eddng tr\\u00fa\",\"address\":\"\\u0110\\u1ecba ch\\u1ec9 li\\u00ean h\\u1ec7\",\"firstMobile\":\"0978429296\",\"secondMobile\":\"0913207826\",\"firstEmail\":\"maint18@viettel.com.vn\",\"secondEmail\":\"mainguyen.it@gmail.com\",\"typeOfTravelGuide\":\"1\",\"typeOfPlace\":1,\"experienceYear\":\"2015\",\"inboundOutbound\":\"2\",\"experienceLevel\":\"31\",\"otherSkills\":null,\"otherInformation\":null,\"touristGuideLevel\":null,\"achievements\":null,\"status\":1,\"province_code\":\"1\",\"is_verified\":null,\"is_fee\":null,\"is_signed\":null,\"member_type\":null,\"guideLanguage\":\"1\",\"educations\":[{\"branchId\":\"3\",\"degreeId\":\"4\",\"file\":0}],\"majorSkills\":{\"ids\":\"3\",\"files\":[]},\"languageSkills\":[{\"languageId\":\"1\",\"levelId\":\"8\",\"file\":null}],\"elements\":[{\"groupSizeId\":\"5\",\"typeGuideId\":\"7\"}],\"workHistory\":[{\"elementName\":\"T\\u00ean doanh nghi\\u1ec7p 1\",\"typeOfContract\":\"1\",\"fromDate\":\"2014-01-01 00:00:00\",\"toDate\":\"2016-01-01 00:00:00\"},{\"elementName\":\"T\\u00ean doanh nghi\\u1ec7p 2\",\"typeOfContract\":\"2\",\"fromDate\":\"2015-01-01 00:00:00\",\"toDate\":\"2016-01-01 00:00:00\"}],\"avatar\":\"http:\\/\\/demo.hoihuongdanvien.vn\\/files\\/collection\\/2018\\/03\\/19\\/img_1521423836_5aaf15dc46dd99.83793664.png\",\"achievementFiles\":[],\"province\":\"7\",\"member_id\":\"2\"}', '2018-03-19 08:44:49', '2018-03-19 08:44:49');
INSERT INTO `members_tmp` VALUES ('19', '3', '{\"fullName\":\"Tran thi thanh ha\",\"gender\":\"1\",\"birthday\":\"2008-05-06\",\"touristGuideCode\":\"111111119\",\"expirationDate\":\"2018-04-04\",\"cmtCccd\":\"111111119\",\"dateIssued\":\"2018-02-09\",\"issuedBy\":\"H\\u00e0 N\\u1ed9i\",\"permanentAddress\":\"H\\u00e0 N\\u1ed9i\",\"address\":\"H\\u00e0 N\\u1ed9i\",\"firstMobile\":\"0976831235\",\"secondMobile\":\"0976831235\",\"firstEmail\":\"thuyenht@viettel.com.vn\",\"secondEmail\":\"thuyenht@viettel.com.vn\",\"typeOfTravelGuide\":\"1\",\"typeOfPlace\":1,\"experienceYear\":\"2014\",\"inboundOutbound\":\"1\",\"experienceLevel\":\"13\",\"otherSkills\":null,\"otherInformation\":null,\"touristGuideLevel\":null,\"achievements\":null,\"status\":1,\"province_code\":\"3\",\"is_verified\":null,\"is_fee\":null,\"is_signed\":null,\"member_type\":null,\"guideLanguage\":\"4\",\"educations\":[{\"branchId\":\"1\",\"degreeId\":\"4\",\"file\":0}],\"majorSkills\":{\"ids\":\"7\",\"files\":[]},\"languageSkills\":[{\"languageId\":\"3\",\"levelId\":\"7\",\"file\":null}],\"elements\":[{\"groupSizeId\":\"5\",\"typeGuideId\":\"3\"}],\"workHistory\":[{\"elementName\":\"abc1\",\"typeOfContract\":\"1\",\"fromDate\":\"2014-01-01 00:00:00\",\"toDate\":\"2014-01-01 00:00:00\"}],\"avatar\":\"http:\\/\\/demo.hoihuongdanvien.vn\\/files\\/collection\\/2018\\/03\\/19\\/img_1521423977_5aaf166926f483.56938990.png\",\"achievementFiles\":[],\"province\":\"1\",\"member_id\":\"3\"}', '2018-03-19 08:46:50', '2018-03-19 08:46:50');
INSERT INTO `members_tmp` VALUES ('20', '1', '{\"fullName\":\"Nguy\\u1ec5n Th\\u1ecb Mai E b\\u00ecnh chi h\\u00e0\",\"gender\":\"1\",\"birthday\":\"1987-06-12\",\"touristGuideCode\":\"156156156\",\"expirationDate\":\"2018-04-04\",\"cmtCccd\":\"112098866\",\"dateIssued\":\"2018-02-21\",\"issuedBy\":\"CA TP H\\u00e0 N\\u1ed9i\",\"permanentAddress\":\"\\u0110\\u1ecba ch\\u1ec9 th\\u01b0\\u1eddng tr\\u00fa\",\"address\":\"\\u0110\\u1ecba ch\\u1ec9 li\\u00ean h\\u1ec7\",\"firstMobile\":\"0978429296\",\"secondMobile\":null,\"firstEmail\":\"maint18@viettel.com.vn\",\"secondEmail\":null,\"typeOfTravelGuide\":\"1\",\"typeOfPlace\":1,\"experienceYear\":\"2018\",\"inboundOutbound\":\"1\",\"experienceLevel\":\"31\",\"otherSkills\":\"c\\u00e1c k\\u1ef9 n\\u0103ng kh\\u00e1c b\\u00ecnh\",\"otherInformation\":\"Th\\u00f4ng tin kh\\u00e1c b\\u00ecnh\",\"touristGuideLevel\":\"H\\u1ea1ng h\\u01b0\\u1edbng d\\u1eabn vi\\u00ean b\\u00ecnh\",\"achievements\":\"Th\\u00e0nh t\\u00edch b\\u00ecnh\",\"status\":1,\"province_code\":\"1\",\"is_verified\":null,\"is_fee\":null,\"is_signed\":null,\"member_type\":null,\"guideLanguage\":\"1\",\"educations\":[{\"branchId\":\"3\",\"degreeId\":\"4\",\"file\":0}],\"majorSkills\":{\"ids\":\"3\",\"files\":[]},\"languageSkills\":[{\"languageId\":\"1\",\"levelId\":\"8\",\"file\":null}],\"elements\":[{\"groupSizeId\":\"5\",\"typeGuideId\":\"7\"}],\"workHistory\":[{\"elementName\":\"T\\u00ean doanh nghi\\u1ec7p 123355\",\"typeOfContract\":\"1\",\"fromDate\":\"2014-01-01 00:00:00\",\"toDate\":\"2013-01-01 00:00:00\"},{\"elementName\":\"T\\u00ean doanh nghi\\u1ec7p 223355\",\"typeOfContract\":\"2\",\"fromDate\":\"2013-01-01 00:00:00\",\"toDate\":\"2015-01-01 00:00:00\"}],\"avatar\":\"http:\\/\\/demo.hoihuongdanvien.vn\\/files\\/collection\\/2018\\/03\\/16\\/img_1521194978_5aab97e2a61580.66076550.jpeg\",\"achievementFiles\":[],\"province\":\"7\",\"member_id\":\"1\"}', '2018-03-25 08:44:52', '2018-03-25 08:44:52');
INSERT INTO `members_tmp` VALUES ('21', '1', '{\"fullName\":\"Nguy\\u1ec5n Th\\u1ecb Mai E b\\u00ecnh chi h\\u00e0\",\"gender\":\"1\",\"birthday\":\"1987-06-12\",\"touristGuideCode\":\"156156156\",\"expirationDate\":\"2018-04-04\",\"cmtCccd\":\"112098866\",\"dateIssued\":\"2018-02-21\",\"issuedBy\":\"CA TP H\\u00e0 N\\u1ed9i\",\"permanentAddress\":\"\\u0110\\u1ecba ch\\u1ec9 th\\u01b0\\u1eddng tr\\u00fa\",\"address\":\"\\u0110\\u1ecba ch\\u1ec9 li\\u00ean h\\u1ec7\",\"firstMobile\":\"0978429296\",\"secondMobile\":null,\"firstEmail\":\"maint18@viettel.com.vn\",\"secondEmail\":null,\"typeOfTravelGuide\":\"1\",\"typeOfPlace\":1,\"experienceYear\":\"2018\",\"inboundOutbound\":\"1\",\"experienceLevel\":\"31\",\"otherSkills\":\"c\\u00e1c k\\u1ef9 n\\u0103ng kh\\u00e1c b\\u00ecnh\",\"otherInformation\":\"Th\\u00f4ng tin kh\\u00e1c b\\u00ecnh\",\"touristGuideLevel\":\"H\\u1ea1ng h\\u01b0\\u1edbng d\\u1eabn vi\\u00ean b\\u00ecnh\",\"achievements\":\"Th\\u00e0nh t\\u00edch b\\u00ecnh\",\"status\":1,\"province_code\":\"1\",\"is_verified\":null,\"is_fee\":null,\"is_signed\":null,\"member_type\":null,\"guideLanguage\":\"1\",\"educations\":[{\"branchId\":\"3\",\"degreeId\":\"4\",\"file\":0}],\"majorSkills\":{\"ids\":\"3\",\"files\":[]},\"languageSkills\":[{\"languageId\":\"1\",\"levelId\":\"8\",\"file\":null}],\"elements\":[{\"groupSizeId\":\"5\",\"typeGuideId\":\"7\"}],\"workHistory\":[{\"elementName\":\"T\\u00ean doanh nghi\\u1ec7p 123355\",\"typeOfContract\":\"1\",\"fromDate\":\"2014-01-01 00:00:00\",\"toDate\":\"2013-01-01 00:00:00\"},{\"elementName\":\"T\\u00ean doanh nghi\\u1ec7p 223355\",\"typeOfContract\":\"2\",\"fromDate\":\"2013-01-01 00:00:00\",\"toDate\":\"2015-01-01 00:00:00\"}],\"avatar\":\"http:\\/\\/demo.hoihuongdanvien.vn\\/files\\/collection\\/2018\\/03\\/16\\/img_1521194978_5aab97e2a61580.66076550.jpeg\",\"achievementFiles\":[],\"province\":\"7\",\"member_id\":\"1\"}', '2018-03-25 08:48:05', '2018-03-25 08:48:05');

-- ----------------------------
-- Table structure for members_tmp_bk
-- ----------------------------
DROP TABLE IF EXISTS `members_tmp_bk`;
CREATE TABLE `members_tmp_bk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `fullName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `touristGuideCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `cmtCccd` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateIssued` datetime DEFAULT NULL,
  `issuedBy` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanentAddress` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstMobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondMobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeOfTravelGuide` tinyint(4) DEFAULT NULL,
  `typeOfPlace` tinyint(4) DEFAULT NULL,
  `experienceYear` int(11) DEFAULT NULL,
  `experienceLevel` tinyint(4) DEFAULT NULL,
  `otherSkills` text COLLATE utf8mb4_unicode_ci,
  `otherInformation` text COLLATE utf8mb4_unicode_ci,
  `touristGuideLevel` text COLLATE utf8mb4_unicode_ci,
  `achievements` text COLLATE utf8mb4_unicode_ci,
  `emailToken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `province_code` tinyint(4) NOT NULL,
  `guideLanguage` tinyint(4) DEFAULT NULL,
  `inboundOutbound` tinyint(4) DEFAULT NULL,
  `province` int(4) DEFAULT NULL,
  `branchId_tmp` int(10) DEFAULT NULL,
  `degreeId_tmp` int(10) DEFAULT NULL,
  `majorId_tmp` int(10) DEFAULT NULL,
  `languageId_tmp` int(10) DEFAULT NULL,
  `levelId_tmp` int(10) DEFAULT NULL,
  `elementId_tmp` int(10) DEFAULT NULL,
  `typeGuideId_tmp` int(10) DEFAULT NULL,
  `groupSizeId_tmp` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of members_tmp_bk
-- ----------------------------

-- ----------------------------
-- Table structure for member_decision_file
-- ----------------------------
DROP TABLE IF EXISTS `member_decision_file`;
CREATE TABLE `member_decision_file` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `number_decisive` varchar(20) DEFAULT NULL,
  `leaderId` varchar(191) CHARACTER SET utf8mb4 DEFAULT NULL,
  `sign_date` date DEFAULT NULL,
  `fileId` int(10) DEFAULT NULL,
  `is_print` tinyint(4) DEFAULT NULL,
  `is_signed` tinyint(4) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleteAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`,`memberId`),
  KEY `memberId` (`memberId`),
  CONSTRAINT `member_decision_file_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member_decision_file
-- ----------------------------
INSERT INTO `member_decision_file` VALUES ('5', '3', '123', '3', '2018-03-13', '210', '1', '1', '2018-03-13 11:21:59', '2018-03-13 11:21:59', '2018-03-13 11:21:59', '0');
INSERT INTO `member_decision_file` VALUES ('7', '6', '123', '3', '2018-03-13', '213', '1', '1', '2018-03-13 11:34:07', '2018-03-13 11:34:07', '2018-03-13 11:34:07', '0');
INSERT INTO `member_decision_file` VALUES ('8', '4', '123', '3', '2018-03-13', '212', '1', '1', '2018-03-13 11:33:53', '2018-03-13 11:33:53', '2018-03-13 11:33:53', '0');
INSERT INTO `member_decision_file` VALUES ('9', '2', '123', '3', '2018-03-13', '223', '1', '1', '2018-03-13 14:34:51', '2018-03-13 14:34:51', '2018-03-13 14:34:51', '0');
INSERT INTO `member_decision_file` VALUES ('10', '1', 'www', '3', '2018-03-15', '308', '1', '1', '2018-03-15 17:11:52', '2018-03-15 17:11:52', '2018-03-15 17:11:52', '0');
INSERT INTO `member_decision_file` VALUES ('11', '7', '111', '3', '2018-03-23', '378', '1', '1', '2018-03-23 11:29:36', '2018-03-23 04:29:36', '2018-03-23 11:29:36', '0');
INSERT INTO `member_decision_file` VALUES ('12', '8', '123', '4', '2018-03-13', '225', '1', '1', '2018-03-13 14:35:42', '2018-03-13 14:35:42', '2018-03-13 14:35:42', '0');
INSERT INTO `member_decision_file` VALUES ('13', '9', '123', '4', '2018-03-13', '226', '1', '1', '2018-03-13 14:35:53', '2018-03-13 14:35:53', '2018-03-13 14:35:53', '0');
INSERT INTO `member_decision_file` VALUES ('14', '10', '123', '3', '2018-03-06', '227', '1', '1', '2018-03-13 14:36:09', '2018-03-13 14:36:09', '2018-03-13 14:36:09', '0');
INSERT INTO `member_decision_file` VALUES ('15', '12', '123', '3', '2018-03-06', '228', '1', '1', '2018-03-13 14:36:22', '2018-03-13 14:36:22', '2018-03-13 14:36:22', '0');
INSERT INTO `member_decision_file` VALUES ('16', '13', '1', '3', '2018-03-13', '229', '1', '1', '2018-03-13 14:36:45', '2018-03-13 14:36:45', '2018-03-13 14:36:45', '0');
INSERT INTO `member_decision_file` VALUES ('17', '14', '123', '3', '2018-03-13', '230', '1', '1', '2018-03-13 14:49:11', '2018-03-13 14:49:11', '2018-03-13 14:49:11', '0');
INSERT INTO `member_decision_file` VALUES ('18', '5', '123', '3', '2018-03-13', '231', '1', '1', '2018-03-13 14:52:36', '2018-03-13 14:52:36', '2018-03-13 14:52:36', '0');
INSERT INTO `member_decision_file` VALUES ('19', '15', '123aa', '4', '2018-03-14', '259', '1', '1', '2018-03-14 14:54:30', '2018-03-14 14:54:30', '2018-03-14 14:54:30', '0');
INSERT INTO `member_decision_file` VALUES ('20', '17', '21', '4', '2018-03-14', '236', '1', '1', '2018-03-14 10:31:45', '2018-03-14 10:31:45', '2018-03-14 10:31:45', '0');
INSERT INTO `member_decision_file` VALUES ('24', '34', '888888', '4', '2018-03-14', '261', '1', '1', '2018-03-14 15:07:36', '2018-03-14 15:07:36', '2018-03-14 15:07:36', '0');
INSERT INTO `member_decision_file` VALUES ('25', '35', '123456', '4', '2018-03-14', '262', '1', '1', '2018-03-14 15:13:48', '2018-03-14 15:13:48', '2018-03-14 15:13:48', '0');
INSERT INTO `member_decision_file` VALUES ('26', '28', 'sdsad', '3', '2018-03-15', '294', '1', '1', '2018-03-15 16:11:35', '2018-03-15 16:11:35', '2018-03-15 16:11:35', '0');
INSERT INTO `member_decision_file` VALUES ('27', '27', 'sadasd', '3', '2018-03-15', '297', '1', '1', '2018-03-15 16:13:54', '2018-03-15 16:13:54', '2018-03-15 16:13:54', '0');
INSERT INTO `member_decision_file` VALUES ('29', '57', '123', '3', '2018-02-26', '304', '1', '1', '2018-03-15 16:47:05', '2018-03-15 16:47:05', '2018-03-15 16:47:05', '0');
INSERT INTO `member_decision_file` VALUES ('30', '18', '123', '3', '2018-03-15', '306', '1', '1', '2018-03-15 17:11:27', '2018-03-15 17:11:27', '2018-03-15 17:11:27', '0');
INSERT INTO `member_decision_file` VALUES ('31', '22', 'sad', '3', '2018-03-15', '305', '1', '1', '2018-03-15 17:09:40', '2018-03-15 17:09:40', '2018-03-15 17:09:40', '0');
INSERT INTO `member_decision_file` VALUES ('32', '36', 'dsfds', '3', '2018-03-15', '307', '1', '1', '2018-03-15 17:11:39', '2018-03-15 17:11:39', '2018-03-15 17:11:39', '0');
INSERT INTO `member_decision_file` VALUES ('33', '33', '234', '4', '2018-03-15', '309', '1', '1', '2018-03-15 17:12:18', '2018-03-15 17:12:18', '2018-03-15 17:12:18', '0');
INSERT INTO `member_decision_file` VALUES ('34', '24', '123', '3', '2018-03-15', '310', '1', '1', '2018-03-15 17:12:29', '2018-03-15 17:12:29', '2018-03-15 17:12:29', '0');
INSERT INTO `member_decision_file` VALUES ('35', '23', '345', '3', '2018-03-15', '311', '1', '1', '2018-03-15 17:12:40', '2018-03-15 17:12:40', '2018-03-15 17:12:40', '0');
INSERT INTO `member_decision_file` VALUES ('36', '59', 'Q157', '4', '2018-03-16', '317', '1', '1', '2018-03-16 09:56:09', '2018-03-16 09:56:09', '2018-03-16 09:56:09', '0');
INSERT INTO `member_decision_file` VALUES ('38', '37', '122', '3', '2018-03-16', '344', '1', '1', '2018-03-16 14:53:29', '2018-03-16 14:53:29', '2018-03-16 14:53:29', '0');
INSERT INTO `member_decision_file` VALUES ('39', '31', 'dfds', '3', '2018-03-16', '346', '1', '1', '2018-03-16 14:55:49', '2018-03-16 14:55:49', '2018-03-16 14:55:49', '0');
INSERT INTO `member_decision_file` VALUES ('41', '46', null, null, null, null, '1', null, '2018-03-16 15:20:03', '2018-03-16 15:20:03', null, '0');
INSERT INTO `member_decision_file` VALUES ('42', '38', '111', '3', '2018-03-22', '376', '1', '1', '2018-03-22 23:09:09', '2018-03-22 16:09:09', '2018-03-22 23:09:09', '0');
INSERT INTO `member_decision_file` VALUES ('44', '39', '111', '4', '2018-02-22', '379', '1', '1', '2018-03-25 15:59:41', '2018-03-25 08:59:41', '2018-03-25 15:59:41', '0');
INSERT INTO `member_decision_file` VALUES ('45', '45', '111', '3', '2018-03-20', '380', '1', '1', '2018-03-26 22:13:06', '2018-03-26 15:13:06', '2018-03-26 22:13:06', '0');
INSERT INTO `member_decision_file` VALUES ('46', '44', '111', '3', '2018-03-26', '381', '1', '1', '2018-03-26 22:56:18', '2018-03-26 15:56:18', '2018-03-26 22:56:18', '0');
INSERT INTO `member_decision_file` VALUES ('47', '54', null, null, null, null, '1', null, '2018-03-26 15:56:32', '2018-03-26 15:56:32', null, '0');
INSERT INTO `member_decision_file` VALUES ('48', '29', null, null, null, null, '1', null, '2018-03-26 17:10:29', '2018-03-26 17:10:29', null, '0');

-- ----------------------------
-- Table structure for member_payments
-- ----------------------------
DROP TABLE IF EXISTS `member_payments`;
CREATE TABLE `member_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number_payment` varchar(100) NOT NULL,
  `date_payment` datetime NOT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `memberId` int(10) unsigned NOT NULL,
  `currency` varchar(100) NOT NULL,
  `currency_type` varchar(10) NOT NULL,
  `payment_method` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_content` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleteAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `memberId` (`memberId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member_payments
-- ----------------------------
INSERT INTO `member_payments` VALUES ('1', '111111118', '2018-03-13 00:00:00', '0', '4', '10023', 'vnd', 'payment', 'sdfsdf', 'sdfsdfsd', '2018-03-13 03:34:26', '2018-03-13 10:34:26', null);
INSERT INTO `member_payments` VALUES ('2', '100', '2018-03-13 00:00:00', '0', '6', '100', 'vnd', 'payment', 'sdfsdf', 'sdfsdf', '2018-03-13 03:36:07', '2018-03-13 10:36:07', null);
INSERT INTO `member_payments` VALUES ('3', 'sdf', '2018-03-01 00:00:00', '0', '3', '111', 'vnd', 'payment', 'ưerqw', 'qưerqwr', '2018-03-13 03:47:45', '2018-03-13 10:47:45', null);
INSERT INTO `member_payments` VALUES ('4', '12432432', '2018-03-13 00:00:00', '0', '9', '121323', 'vnd', 'payment', 'sdfsd', 'sdfsdf', '2018-03-13 07:26:45', '2018-03-13 14:26:45', null);
INSERT INTO `member_payments` VALUES ('5', '1223', '2018-03-13 00:00:00', '0', '14', '23234', 'vnd', 'payment', 'sdfdsf', null, '2018-03-13 07:27:31', '2018-03-13 14:27:31', null);
INSERT INTO `member_payments` VALUES ('6', '1213', '2018-03-13 00:00:00', '0', '10', '1213', 'vnd', 'payment', 'sdf', 'sdfsd', '2018-03-13 07:27:42', '2018-03-13 14:27:42', null);
INSERT INTO `member_payments` VALUES ('7', '123', '2018-03-13 00:00:00', '0', '12', '1213', 'vnd', 'payment', 'sdf', 'sdf', '2018-03-13 07:27:56', '2018-03-13 14:27:56', null);
INSERT INTO `member_payments` VALUES ('8', '23214243', '2018-03-13 00:00:00', '0', '13', '232434', 'vnd', 'payment', 'dsfdsf', 'sdfdsf', '2018-03-13 07:28:13', '2018-03-13 14:28:13', null);
INSERT INTO `member_payments` VALUES ('9', '23434343', '2018-03-13 00:00:00', '0', '8', '3434343', 'vnd', 'payment', 'sdfsdfdsf', null, '2018-03-13 07:28:44', '2018-03-13 14:28:44', null);
INSERT INTO `member_payments` VALUES ('10', 'UNC002', '2017-12-13 10:35:44', '0', '7', '500000', 'vnd', '1', 'Nộp tiền đăng ký hội viên', 'Nộp bổ sung do lần 1 nộp thiếu', '2018-03-16 10:35:44', '2018-03-16 17:35:44', '2018-03-16 17:35:44');
INSERT INTO `member_payments` VALUES ('11', '1235', '2018-03-13 00:00:00', '0', '1', '1234', 'vnd', 'payment', 'sdfsdf', 'sdfsdf', '2018-03-13 07:33:03', '2018-03-13 14:33:03', null);
INSERT INTO `member_payments` VALUES ('12', '123', '2018-03-13 00:00:00', '0', '2', '123', 'vnd', 'payment', 'sdfsdf', 'sdfsdf', '2018-03-13 07:33:16', '2018-03-13 14:33:16', null);
INSERT INTO `member_payments` VALUES ('15', '1', '2018-03-14 00:00:00', '0', '17', '1000000', 'vnd', 'payment', 'Nộp tiền đăng ký gia nhập hội', 'OK', '2018-03-14 03:18:02', '2018-03-14 10:18:02', null);
INSERT INTO `member_payments` VALUES ('16', '2', '2018-03-14 00:00:00', '0', '34', '1000000', 'vnd', 'payment', 'th', null, '2018-03-14 04:41:42', '2018-03-14 11:41:42', null);
INSERT INTO `member_payments` VALUES ('17', '3', '2018-03-14 00:00:00', '0', '35', '1500000', 'vnd', 'payment', '0', null, '2018-03-14 04:42:29', '2018-03-14 11:42:29', null);
INSERT INTO `member_payments` VALUES ('18', '3', '2018-03-14 00:00:00', '0', '36', '3000', 'vnd', 'payment', '00', null, '2018-03-14 04:42:56', '2018-03-14 11:42:56', null);
INSERT INTO `member_payments` VALUES ('19', '4545', '2018-03-07 00:00:00', '0', '15', '45', 'vnd', 'payment', '54', '67', '2018-03-14 06:23:11', '2018-03-14 13:23:11', null);
INSERT INTO `member_payments` VALUES ('20', '111111111111111111111111111119', '2018-03-14 00:00:00', '0', '22', '-234234', 'vnd', 'payment', 'Nội dung nộp tiền Nội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung End', 'Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi cEnd', '2018-03-14 08:10:14', '2018-03-14 15:10:14', null);
INSERT INTO `member_payments` VALUES ('23', '123456', '2018-03-12 00:00:00', '0', '33', '100', 'vnd', 'payment', 'Nội dung nộp tiền', 'Ghi chú', '2018-03-14 08:38:44', '2018-03-14 15:38:44', null);
INSERT INTO `member_payments` VALUES ('25', '1234', '2018-03-08 00:00:00', '0', '11', '5000000', 'vnd', 'payment', 'ádfasdfa', 'ádfsadfaf', '2018-03-14 09:41:26', '2018-03-14 16:41:26', null);
INSERT INTO `member_payments` VALUES ('26', '3233', '2018-03-06 00:00:00', '0', '18', '234234', 'vnd', 'payment', 'dfsdf', 'ádfa', '2018-03-14 10:08:44', '2018-03-14 17:08:44', null);
INSERT INTO `member_payments` VALUES ('27', '111111111111111111111111111111', '2018-03-14 00:00:00', '0', '23', '111111111', 'vnd', 'payment', '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '2018-03-14 11:40:13', '2018-03-14 18:40:13', null);
INSERT INTO `member_payments` VALUES ('28', '11111', '2018-03-07 00:00:00', '0', '24', '-1111111111', 'vnd', 'payment', 'ádadaf', 'âfafaf', '2018-03-14 11:43:06', '2018-03-14 18:43:06', null);
INSERT INTO `member_payments` VALUES ('29', '5445646', '2018-03-19 00:00:00', '0', '25', '2000000000', 'vnd', '1', 'Nộp tiền đăng ký hội viên65756756', null, '2018-03-19 01:57:27', '2018-03-19 08:57:27', '2018-03-19 08:57:27');
INSERT INTO `member_payments` VALUES ('31', 'ABD123', '2018-03-07 00:00:00', '0', '27', '2343', 'vnd', 'payment', 'Nội dung nộp tiền', null, '2018-03-15 04:03:35', '2018-03-15 11:03:35', null);
INSERT INTO `member_payments` VALUES ('32', 'aaaaa', '2018-03-15 00:00:00', '0', '28', '1111121212', 'vnd', 'payment', 'ládlkajdklajdlksajd', null, '2018-03-15 06:32:42', '2018-03-15 13:32:42', null);
INSERT INTO `member_payments` VALUES ('33', 'sdsds', '2018-03-01 00:00:00', '0', '57', '123', 'vnd', 'payment', 'dsfsd', 'sdfsd', '2018-03-15 09:46:15', '2018-03-15 16:46:15', null);
INSERT INTO `member_payments` VALUES ('34', 'lajldjaljfdlka', '2018-03-11 00:00:00', '0', '29', '111', 'vnd', 'payment', '1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafa', '212123 sad âd', '2018-03-19 02:58:35', '2018-03-19 09:58:35', '2018-03-19 09:58:35');
INSERT INTO `member_payments` VALUES ('35', 'CT157', '2018-03-16 00:00:00', '0', '59', '1000000', 'vnd', 'payment', 'thanh toán lệ phí', 'fdsfd', '2018-03-16 02:46:18', '2018-03-16 09:46:18', null);
INSERT INTO `member_payments` VALUES ('37', '123', '2018-03-16 00:00:00', '0', '31', '123', 'vnd', 'payment', '<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<scri', '<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<scri', '2018-03-16 04:22:54', '2018-03-16 11:22:54', null);
INSERT INTO `member_payments` VALUES ('39', '-----///////', '2018-03-16 00:00:00', '0', '46', '1133', 'vnd', 'payment', 'sadsadsa', 'sdasd', '2018-03-25 13:37:04', '2018-03-25 06:37:04', '2018-03-25 06:37:04');
INSERT INTO `member_payments` VALUES ('42', '123', '2018-03-16 00:00:00', '0', '38', '00000000000000000000000000000000000000000000000000', 'vnd', 'payment', 'dsfdsfdsfds', null, '2018-03-16 07:07:43', '2018-03-16 14:07:43', null);
INSERT INTO `member_payments` VALUES ('43', '123', '2018-03-16 00:00:00', '0', '49', '234', 'vnd', 'payment', 'fđfg', null, '2018-03-25 13:39:24', '2018-03-25 06:39:24', '2018-03-25 06:39:24');
INSERT INTO `member_payments` VALUES ('45', '123', '2018-03-16 00:00:00', '0', '44', '123', 'vnd', 'payment', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '\' + (SELECT\nTOP 1 FieldName FROM TableName) + \'', '2018-03-16 07:38:06', '2018-03-16 14:38:06', '2018-03-16 14:38:06');
INSERT INTO `member_payments` VALUES ('46', 'adb234123412', '2018-03-06 00:00:00', '0', '37', '234444', 'vnd', 'payment', 'ffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsd', 'ffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsd', '2018-03-25 13:29:29', '2018-03-25 13:29:29', null);
INSERT INTO `member_payments` VALUES ('47', 'eeeeeeeeeeeeeeeeeeeeeeeeeeeend', '2018-03-01 00:00:00', '0', '71', '12345678', 'vnd', 'payment', 'Nội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nEnd', 'Ghi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGhi chúGEnđ', '2018-03-16 08:07:08', '2018-03-16 15:07:08', '2018-03-16 15:07:08');
INSERT INTO `member_payments` VALUES ('50', '123fdsfd', '2018-03-16 00:00:00', '1', '48', '1234', 'vnd', 'payment', 'dss', null, '2018-03-25 13:53:27', '2018-03-25 06:53:27', '2018-03-25 13:53:27');
INSERT INTO `member_payments` VALUES ('52', 'aabbcc#!@#!', '2018-03-19 02:45:08', null, '43', '200000000', 'vnd', '1', 'Nộp tiền', 'Nt', '2018-03-19 02:45:08', '2018-03-19 09:45:08', '2018-03-19 09:45:08');
INSERT INTO `member_payments` VALUES ('54', 'aabbcc#!@#!', '2018-03-19 03:31:56', null, '32', '200000000', 'vnd', '1', 'Nộp tiền', 'Nt', '2018-03-25 12:54:12', '2018-03-25 05:54:12', '2018-03-25 05:54:12');
INSERT INTO `member_payments` VALUES ('55', '54654', '2018-03-19 00:00:00', '0', '45', '0', 'vnd', 'payment', '56756', null, '2018-03-19 03:50:19', '2018-03-19 10:50:19', null);
INSERT INTO `member_payments` VALUES ('57', '111', '2018-03-19 00:00:00', '0', '54', '2222', 'vnd', 'payment', '11111', null, '2018-03-23 09:33:27', '2018-03-23 09:33:27', null);
INSERT INTO `member_payments` VALUES ('58', '11222', '2018-03-23 00:00:00', '0', '83', '2222222222', 'vnd', 'payment', '<script>alert(\"xxxx\")</script>', '11111', '2018-03-24 14:54:56', '2018-03-24 14:54:56', null);
INSERT INTO `member_payments` VALUES ('61', 'ST12345', '2018-03-12 00:00:00', '0', '19', '1000000', 'vnd', 'payment', '222', '22', '2018-03-25 11:28:04', '2018-03-25 04:28:04', '2018-03-25 11:28:04');
INSERT INTO `member_payments` VALUES ('62', '11222', '2018-03-24 00:00:00', '0', '5', '2222', 'vnd', 'payment', '11111', null, '2018-03-25 12:52:52', '2018-03-25 05:52:52', '2018-03-25 05:52:52');
INSERT INTO `member_payments` VALUES ('64', 'M111222', '2018-03-18 00:00:00', '0', '58', '1000000', 'vnd', 'payment', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', null, '2018-03-25 07:13:57', '2018-03-25 07:13:57', null);
INSERT INTO `member_payments` VALUES ('65', 'M1234', '2018-03-20 00:00:00', '0', '39', '1000000', 'vnd', 'payment', '1111', null, '2018-03-25 14:33:05', '2018-03-25 07:33:05', '2018-03-25 14:33:05');

-- ----------------------------
-- Table structure for member_position
-- ----------------------------
DROP TABLE IF EXISTS `member_position`;
CREATE TABLE `member_position` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member_position
-- ----------------------------
INSERT INTO `member_position` VALUES ('2', '30', '1', null, '2018-03-23 20:05:47', '2018-03-23 20:05:49');
INSERT INTO `member_position` VALUES ('4', '30', '3', null, '2018-03-23 20:05:59', '2018-03-23 20:06:02');
INSERT INTO `member_position` VALUES ('33', '1', '4', null, '2018-03-29 17:41:10', '2018-03-29 17:45:47');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_11_27_000000_create_files_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_11_28_000000_create_members_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_11_28_000001_create_educationBranches_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_11_28_000002_create_educationDegrees_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_11_28_000003_create_educations_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_11_28_000004_create_languages_table', '1');
INSERT INTO `migrations` VALUES ('9', '2017_11_28_000005_create_languageLevels_table', '1');
INSERT INTO `migrations` VALUES ('10', '2017_11_28_000006_create_languageSkills_table', '1');
INSERT INTO `migrations` VALUES ('11', '2017_11_28_000007_create_works_table', '1');
INSERT INTO `migrations` VALUES ('12', '2017_11_28_000008_create_workHistory_table', '1');
INSERT INTO `migrations` VALUES ('13', '2017_11_28_000009_create_typeGuides_table', '1');
INSERT INTO `migrations` VALUES ('14', '2017_11_28_000010_create_groupSizes_table', '1');
INSERT INTO `migrations` VALUES ('15', '2017_11_28_000011_create_elements_table', '1');
INSERT INTO `migrations` VALUES ('16', '2017_11_28_000012_create_majors_table', '1');
INSERT INTO `migrations` VALUES ('17', '2017_11_28_000013_create_majorSkills_table', '1');
INSERT INTO `migrations` VALUES ('18', '2017_11_28_000014_create_majorSkillFiles_table', '1');
INSERT INTO `migrations` VALUES ('19', '2017_11_28_000015_create_achievementFiles_table', '1');
INSERT INTO `migrations` VALUES ('20', '2017_12_24_071226_create_notes_table', '1');
INSERT INTO `migrations` VALUES ('21', '2018_02_27_161019_create_jobs_table', '2');
INSERT INTO `migrations` VALUES ('22', '2018_02_27_170752_create_failed_jobs_table', '2');
INSERT INTO `migrations` VALUES ('23', '2018_03_06_034605_add_column_to_members_table', '3');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `thumnail_image` varchar(255) DEFAULT NULL,
  `main_image` varchar(255) DEFAULT NULL,
  `short_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `content` text CHARACTER SET utf8,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: lưu nháp, 1: hoạt động',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `option_code` varchar(10) DEFAULT NULL,
  `branch_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', 'Về ch&uacute;ng t&ocirc;i', null, null, null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<div id=\"vechungtoi\" class=\"table-content active\">\r\n<div class=\"main-content\" style=\"font-family: \'Times New Roman\',\'serif\';\">\r\n<h1 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; padding-bottom: 15px; line-height: 115%;\">GIỚI THIỆU HỘI HƯỚNG DẪN VI&Ecirc;N DU LỊCH VIỆT NAM</h1>\r\n<p>Hướng dẫn vi&ecirc;n l&agrave; đội qu&acirc;n ti&ecirc;n phong trong ng&agrave;nh du lịch, l&agrave; những đại sứ c&oacute; vai tr&ograve; rất lớn g&oacute;p phần x&acirc;y dựng h&igrave;nh ảnh của du lịch Việt Nam trong mắt bạn b&egrave; trong nước v&agrave; quốc tế. Tuy nhi&ecirc;n hiện nay, phần lớn c&aacute;c hướng dẫn vi&ecirc;n đều h&agrave;nh nghề tự do m&agrave; kh&ocirc;ng thuộc qu&acirc;n số đơn vị n&agrave;o.</p>\r\n<p>Theo quy định của Luật Du lịch sửa đổi năm 2017, c&oacute; hiệu lực từ ng&agrave;y 01/01/2018, hướng dẫn vi&ecirc;n chỉ được h&agrave;nh nghề khi đ&aacute;p ứng c&aacute;c điều kiện: 1. C&oacute; thẻ hướng dẫn vi&ecirc;n du lịch; 2. C&oacute; hợp đồng lao động với doanh nghiệp kinh doanh dịch vụ lữ h&agrave;nh, doanh nghiệp cung cấp dịch vụ hướng dẫn du lịch hoặc l&agrave; hội vi&ecirc;n của tổ chức x&atilde; hội - nghề nghiệp về hướng dẫn du lịch đối với hướng dẫn vi&ecirc;n du lịch quốc tế v&agrave; hướng dẫn vi&ecirc;n du lịch nội địa; 3. C&oacute; hợp đồng hướng dẫn với doanh nghiệp kinh doanh dịch vụ lữ h&agrave;nh hoặc văn bản ph&acirc;n c&ocirc;ng hướng dẫn theo chương tr&igrave;nh du lịch; đối với hướng dẫn vi&ecirc;n du lịch tại điểm, phải c&oacute; ph&acirc;n c&ocirc;ng của tổ chức, c&aacute; nh&acirc;n quản l&yacute; khu du lịch, điểm du lịch.</p>\r\n<p>Với sứ mệnh thu h&uacute;t v&agrave; đ&agrave;o tạo nh&acirc;n t&agrave;i trong ng&agrave;nh hướng dẫn đồng thời n&acirc;ng tầm hướng dẫn vi&ecirc;n Việt Nam s&aacute;nh ngang với khu vực v&agrave; quốc tế, <strong>Hội hướng dẫn vi&ecirc;n du lịch Việt Nam</strong> được th&agrave;nh lập ng&agrave;y 10-10- 2017, l&agrave; tổ chức x&atilde; hội nghề nghiệp của những c&ocirc;ng d&acirc;n Việt Nam hoạt động trong lĩnh vực hướng dẫn du lịch; đại diện cho quyền v&agrave; lợi &iacute;ch hợp ph&aacute;p của Hội vi&ecirc;n l&agrave; HDV du lịch Việt Nam. Đồng thời, g&oacute;p phần hỗ trợ hoạt động, bồi dưỡng, cập nhật kiến thức, chuy&ecirc;n m&ocirc;n, nghiệp vụ, kỹ năng h&agrave;nh nghề hướng dẫn cho c&aacute;c hội vi&ecirc;n để n&acirc;ng cao tr&igrave;nh độ nghiệp vụ v&agrave; chất lượng dịch vụ hướng dẫn du lịch; v&agrave; ph&aacute;t triển c&aacute;c hoạt động li&ecirc;n kết, hợp t&aacute;c quốc tế để đ&agrave;o tạo ph&aacute;t triển năng lực đội ngũ HDV du lịch.</p>\r\n<p>T&ocirc;n chỉ v&agrave; gi&aacute; trị cốt l&otilde;i trong c&aacute;c hoạt động của Hội hướng dẫn vi&ecirc;n du lịch Việt Nam l&agrave; <strong>sự minh bạch, hiệu quả v&agrave; bền vững</strong>. Sự minh bạch trong việc x&acirc;y dựng cơ chế v&agrave; quản l&iacute; t&agrave;i ch&iacute;nh; <strong><em>c&aacute;c hoạt động t&agrave;i ch&iacute;nh sẽ được kiểm to&aacute;n v&agrave; gi&aacute;m s&aacute;t bởi c&ocirc;ng ty kiểm to&aacute;n quốc tế Deloitte v&agrave; được c&ocirc;ng bố c&ocirc;ng khai h&agrave;ng năm</em></strong></p>\r\n<p>B&ecirc;n cạnh đ&oacute;, t&iacute;nh hiệu quả, bền vững cũng được đề cao để đảm bảo tất cả c&aacute;choạt động tập trung, tạo gi&aacute; trị cho đội ngũ hướng dẫn vi&ecirc;n; đồng thời t&igrave;m kiếm hợp t&aacute;cvới c&aacute;c đối t&aacute;c uy t&iacute;n trong nước v&agrave; quốc tế nhằm hỗ trợ v&agrave; ph&aacute;t triển năng lực hội vi&ecirc;n. Một số đối t&aacute;c đ&atilde; đồng h&agrave;nh c&ugrave;ng Hội bao gồm: Cộng đồng ch&acirc;u &Acirc;u EU, Viettel...</p>\r\n<p>Hệ thống th&ocirc;ng tin hướng dẫn du lịch của Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam ra đời sẽ hỗ trợ hiệu quả cho c&aacute;c hướng dẫn vi&ecirc;n trong qu&aacute; tr&igrave;nh t&aacute;c nghiệp, đồng thời tạo kết nối chặt chẽ giữa hướng dẫn vi&ecirc;n với c&aacute;c doanh nghiệp du lịch. Hệ thống n&agrave;y cũng sẽ tạo điều kiện thuận lợi cho việc n&acirc;ng cao tr&igrave;nh độ nghiệp vụ, tạo s&agrave;n giao dịch t&igrave;m kiếm c&ocirc;ng việc, tạo sự li&ecirc;n kết giữa doanh nghiệp với hướng dẫn vi&ecirc;n, g&oacute;p phần n&acirc;ng cao vị thế của đội ngũ hướng dẫn vi&ecirc;n trong ng&agrave;nh du lịch, x&acirc;y dựng h&igrave;nh ảnh du lịch Việt Nam trong mắt bạn b&egrave; quốc tế.</p>\r\n</div>\r\n</div>\r\n</body>\r\n</html>', '1', '2018-04-04 00:00:00', '2018-06-09 00:00:00', '1', null, '2018-04-04 20:50:26', '2018-04-04 20:50:26', null);
INSERT INTO `news` VALUES ('2', 'Hướng dẫn đăng k&yacute; hội vi&ecirc;n', null, null, null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<div id=\"huongdandangkyhoivien\" class=\"table-content active\">\r\n<div class=\"main-content\">\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">HƯỚNG DẪN ĐĂNG K&Yacute; HỘI VI&Ecirc;N</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; padding-bottom: 15px; line-height: 115%;\">HỘI HƯỚNG DẪN VI&Ecirc;N DU LỊCH VIỆT NAM</h2>\r\n<p>Bước 1: Chuẩn bị c&aacute;c giấy tờ cần thiết khi đăng k&yacute; gia nhập Hội vi&ecirc;n</p>\r\n<p>Bước 2: V&agrave;o Website: <a href=\"http://www.hoihuongdanvien.vn\">http://www.hoihuongdanvien.vn</a> rồi v&agrave;o mục Đăng k&yacute; hội vi&ecirc;n hoặc click <a href=\"http://www.hoihuongdanvien.vn/member/register\">đ&acirc;y</a></p>\r\n<p>Bước 3: Người đăng k&yacute; gia nhập khai FORM trực tuyến( Đơn đăng k&yacute; gia nhập Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam, Phiếu th&ocirc;ng tin hội vi&ecirc;n). Người đăng k&yacute; gia nhập upload bản chụp Thẻ Hướng dẫn vi&ecirc;n, CMT/CCCD, c&aacute;c văn bằng, chứng chỉ, nhấn n&uacute;t &ldquo; HO&Agrave;N TH&Agrave;NH&rdquo;</p>\r\n<p>Bước 4: Hội kiểm tra th&ocirc;ng tin v&agrave; th&ocirc;ng b&aacute;o Hồ sơ đ&atilde; được ph&ecirc; duyệt qua email cho người đăng k&yacute; gia nhập</p>\r\n<p>Bước 5: Người đăng k&yacute; gia nhập download FORM từ email, in ra, d&aacute;n ảnh ch&acirc;n dung mầu cỡ 3cm x 4 cm, k&yacute; t&ecirc;n v&agrave; gửi về Hội/Văn ph&ograve;ng đại diện</p>\r\n<p>Bước 6: Người đăng k&yacute; gia nhập đ&oacute;ng lệ ph&iacute; gia nhập v&agrave; hội ph&iacute;</p>\r\n<p>Hội tiếp nhận hồ sơ đăng k&yacute; gia nhập gốc, cấp thẻ hội vi&ecirc;n</p>\r\n<div style=\"margin-top: 30px;\">\r\n<p><span style=\"font-size: 10.0pt; font-weight: bold;\">* C&aacute;c giấy tờ cần chuẩn bị khi đăng k&yacute; gia nhập Hội vi&ecirc;n:</span></p>\r\n<p>1. Thẻ hướng dẫn vi&ecirc;n Du lịch (bản sao)</p>\r\n<p>2. Chứng minh thư/căn cước c&ocirc;ng d&acirc;n (bản sao )</p>\r\n</div>\r\n</div>\r\n</div>\r\n</body>\r\n</html>', '1', '2018-04-04 00:00:00', null, '5', null, '2018-04-04 20:56:01', '2018-04-04 20:56:01', null);
INSERT INTO `news` VALUES ('3', 'Quy định', null, null, null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">HỘI HƯỚNG DẪN VI&Ecirc;N DU LỊCH VIỆT NAM</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">MỘT SỐ QUY ĐỊNH ĐỐI VỚI HỘI VI&Ecirc;N</h2>\r\n<h2 style=\"font-size: 10.0pt; font-weight: bold; text-align: center; padding-bottom: 15px; line-height: 115%;\">(v/v đăng k&yacute; gia nhập Hội, tham gia Hệ thống quản l&yacute; v&agrave; hỗ trợ hoạt động của Hướng dẫn vi&ecirc;n)</h2>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: left; line-height: 115%;\">I-Quy định chung</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Hội vi&ecirc;n thường xuy&ecirc;n nghi&ecirc;n cứu th&ocirc;ng tin, xem c&aacute;c th&ocirc;ng b&aacute;o tr&ecirc;n trang web: hoihuongdanvien.vn . Hội vi&ecirc;n tự cập nhật th&ocirc;ng tin tr&ecirc;n profile c&aacute; nh&acirc;n tr&ecirc;n trang website của Hội khi c&oacute; thay đổi về c&aacute;c th&ocirc;ng tin sau: học vấn, nghiệp vụ hướng dẫn, tr&igrave;nh độ ngoại ngữ, qu&aacute; tr&igrave;nh h&agrave;nh nghề hướng dẫn, sở trường hướng dẫn.</li>\r\n<li>Th&ocirc;ng b&aacute;o bằng văn bản hoặc email cho Hội khi thay đổi, email, địa chỉ li&ecirc;n hệ trong 10 ng&agrave;y kể từ khi c&oacute; sự thay đổi.</li>\r\n<li>Thực hiện đầy đủ, c&oacute; tr&aacute;ch nhiệm c&aacute;c quy định trong hợp đồng hướng dẫn, trong bản ph&acirc;n c&ocirc;ng c&ocirc;ng việc của c&aacute;c doanh nghiệp, c&aacute;c quy định về th&ocirc;ng tin v&agrave; b&aacute;o c&aacute;o kết quả c&ocirc;ng việc.</li>\r\n<li>Tham gia v&agrave;o c&aacute;c chương tr&igrave;nh đ&agrave;o tạo, tập huấn, hội họp, sinh hoạt chuy&ecirc;n m&ocirc;n v&agrave; c&aacute;c buổi chia sẻ kinh nghiệm, trải nghiệm với c&aacute;c chuy&ecirc;n gia, c&aacute;c nh&agrave; nghi&ecirc;n cứu, c&aacute;c hướng dẫn vi&ecirc;n giỏi&hellip; do Hội tổ chức.</li>\r\n<li>C&oacute; tinh thần hợp t&aacute;c, hỗ trợ v&agrave; chia sẻ với c&aacute;c hội vi&ecirc;n kh&aacute;c về nghiệp vụ, kinh nghiệm v&agrave; kiến thức nghề nghiệp.</li>\r\n<li>Kh&ocirc;ng vi phạm c&aacute;c quy định về hướng dẫn du lịch trong Luật Du lịch, c&aacute;c quy định của doanh nghiệp kinh doanh dịch vụ lữ h&agrave;nh về t&agrave;i ch&iacute;nh, về mua b&aacute;n h&agrave;ng h&oacute;a, vận chuyển h&agrave;ng h&oacute;a cho kh&aacute;ch du lịch trong thời gian đi hướng dẫn du lịch.</li>\r\n<li>Tu&acirc;n thủ quy tắc đạo đức nghề nghiệp hướng dẫn do Hội ban h&agrave;nh v&agrave; thực hiện ứng xử văn minh khi đi hướng dẫn du lịch.</li>\r\n<li>Chịu sự kiểm tra của Ban Kiểm tra Hội trong khi h&agrave;nh nghề.</li>\r\n<li>Khuyến kh&iacute;ch Hội vi&ecirc;n đăng k&yacute; 01 số điện thoại Viettel.Viettel c&oacute; g&oacute;i cước ưu đ&atilde;i cao cho c&aacute;c Hội vi&ecirc;n.</li>\r\n<li>Sử dụng ứng dụng App của Hội tr&ecirc;n điện thoại để nhận tin tức, th&ocirc;ng tin của Hội v&agrave; thực hiện giao dịch giữa Hội v&agrave; hội vi&ecirc;n, giữa hội vi&ecirc;n với c&aacute;c doanh nghiệp.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: left; line-height: 115%;\">II-Lệ ph&iacute; gia nhập Hội v&agrave; hội ph&iacute;</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Lệ ph&iacute; gia nhập Hội: 500.000 đ( đ&oacute;ng một lần khi đăng k&yacute; Hội vi&ecirc;n)</li>\r\n<li>Hội ph&iacute;: 500.000 đ/năm</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: left; line-height: 115%;\">III- Hỗ trợ hội vi&ecirc;n</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Hỗ trợ của Hội HDV Du lịch Việt Nam. <br />Hội sẽ hỗ trợ mỗi hội vi&ecirc;n 120.000 đ tiền ph&iacute; h&ograve;a mạng Viettel v&agrave; ph&iacute; chuyển đổi g&oacute;i cước Viettel</li>\r\n<li>Hỗ trợ của Viettel: <br />Ưu đ&atilde;i g&oacute;i cước của Viettel cho Hội vi&ecirc;n Hội HDV Du lịch Việt Nam như sau: <br />\r\n<table class=\"rule\" style=\"width: 100%;\">\r\n<tbody>\r\n<tr>\r\n<th class=\"rule\" style=\"width: 8%;\" rowspan=\"2\">Chương tr&igrave;nh</th>\r\n<th class=\"rule\">Ph&iacute; tham gia</th>\r\n<th class=\"rule\" style=\"padding: 10px 0;\" colspan=\"2\">Ưu đ&atilde;i khuyến mại trong 12 th&aacute;ng</th>\r\n</tr>\r\n<tr>\r\n<th class=\"rule\">(Đ&atilde; c&oacute; VAT v&agrave; cước thu&ecirc; bao th&aacute;ng)</th>\r\n<th class=\"rule\">Ưu đ&atilde;i thoại</th>\r\n<th class=\"rule\">Ưu đ&atilde;i data</th>\r\n</tr>\r\n<tr>\r\n<td class=\"rule table-center\">CT1</td>\r\n<td class=\"rule table-center\">180.000đ</td>\r\n<td class=\"rule\" style=\"padding: 10px;\">1000 ph&uacute;t thoại nội mạng, miễn ph&iacute; thoại nội nh&oacute;m Hội HDV Du lịch Việt Nam</td>\r\n<td class=\"rule\" style=\"padding: 10px;\">MIMAX4G (3GB hết 3GB b&oacute;p băng th&ocirc;ng về tốc độ 256/256 Kbps), MCA</td>\r\n</tr>\r\n<tr>\r\n<td class=\"rule table-center\">CT2</td>\r\n<td class=\"rule table-center\">300.000đ</td>\r\n<td class=\"rule\" style=\"padding: 10px;\">1500 ph&uacute;t thoại nội mạng, 100 ph&uacute;t ngoại mạng, miễn ph&iacute; thoại nội nh&oacute;m Hội HDV Du lịch Việt Nam</td>\r\n<td class=\"rule\" style=\"padding: 10px;\">4G200 (10GB, hết 10GB ngừng truy cập), MCA, Tặng thẻ Privilegde hạng th&acirc;n thiết</td>\r\n</tr>\r\n<tr>\r\n<td class=\"rule table-center\">CT3</td>\r\n<td class=\"rule table-center\">500.000đ</td>\r\n<td class=\"rule\" style=\"padding: 10px;\">5000 ph&uacute;t thoại nội mạng, 300 ph&uacute;t ngoại mạng, miễn ph&iacute; thoại nội nh&oacute;m Hội HDV Du lịch Việt Nam</td>\r\n<td class=\"rule\" style=\"padding: 10px;\">4G400 (20GB, hết 20GB ngừng truy cập), MCA, Tặng thẻ Privilegde hạng bạc</td>\r\n</tr>\r\n<tr>\r\n<td class=\"rule table-center\">CT4</td>\r\n<td class=\"rule table-center\">1.000.000đ</td>\r\n<td class=\"rule\" style=\"padding: 10px;\">Miễn ph&iacute; thoại nội mạng, 500 ph&uacute;t ngoại mạng, 1500 tin nhắn nội mạng,miễn ph&iacute; thoại nội nh&oacute;m Hội HDV Du lịch Việt Nam</td>\r\n<td class=\"rule\" style=\"padding: 10px;\">4G500 (30GB, hết 30GB ngừng truy cập), MCA, Tặng thẻ Privilegde hạng v&agrave;ng</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</li>\r\n</ol>\r\n</div>\r\n</body>\r\n</html>', '1', '2018-04-04 00:00:00', '2018-10-26 00:00:00', '3', null, '2018-04-04 20:56:56', '2018-04-04 20:56:56', null);
INSERT INTO `news` VALUES ('4', 'Quy chế hoạt động', null, null, null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<div id=\"quychehoatdong\" class=\"table-content active\">\r\n<div class=\"main-content\" style=\"overflow-x: auto; height: 800px; line-height: 24px;\">\r\n<div class=\"chuong\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;\">Chương I</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">NHỮNG QUY ĐỊNH CHUNG</h2>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 1. T&ecirc;n của Hội</h2>\r\n<p>Hội lấy t&ecirc;n: <strong>Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam</strong></p>\r\n<p>T&ecirc;n giao dịch tiếng Anh: <strong>Vietnam Tour Guide Association</strong></p>\r\n<p>T&ecirc;n viết tắt: <strong>VTGA</strong></p>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 2. T&ocirc;n chỉ, mục đ&iacute;ch của Hội</h2>\r\n<p>Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam (sau đ&acirc;y gọi tắt l&agrave; Hội) l&agrave; tổ chức x&atilde; hội nghề nghiệp về hướng dẫn du lịch, bao gồm c&aacute;c tổ chức, c&ocirc;ng d&acirc;n Việt Nam hoạt động hợp ph&aacute;p trong lĩnh vực hướng dẫn du lịch.</p>\r\n<p>Mục đ&iacute;ch của Hội l&agrave; bồi dưỡng cập nhật kiến thức, chuy&ecirc;n m&ocirc;n, nghiệp vụ, kỹ năng h&agrave;nh nghề hướng dẫn cho c&aacute;c hội vi&ecirc;n để n&acirc;ng cao tr&igrave;nh độ nghiệp vụ v&agrave; chất lượng dịch vụ hướng dẫn du lịch; li&ecirc;n kết, hợp t&aacute;c ph&aacute;t triển đội ngũ hướng dẫn vi&ecirc;n du lịch; bảo vệ quyền, lợi &iacute;ch hợp ph&aacute;p của hội vi&ecirc;n.</p>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 3. Phạm vi hoạt động của Hội</h2>\r\n<p>Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam hoạt động trong phạm vi cả nước v&agrave; ở nước ngo&agrave;i theo quy định của ph&aacute;p luật nước Cộng h&ograve;a XHCN Việt Nam v&agrave; ph&aacute;p luật nơi đến du lịch, Điều lệ Hiệp hội Du lịch Việt Nam v&agrave; Quy chế n&agrave;y.</p>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 4. Địa vị ph&aacute;p l&yacute;</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam l&agrave; một chi hội của Hiệp hội Du lịch Việt Nam do Hiệp hội Du lịch Việt Nam th&agrave;nh lập, c&oacute; biểu tượng ri&ecirc;ng, hoạt động dưới sự chỉ đạo trực tiếp của Hiệp hội Du lịch Việt Nam.</li>\r\n<li>Hiệp hội Du lịch Việt Nam giao cho Hiệp hội Lữ h&agrave;nh Việt Nam hỗ trợ c&aacute;c hoạt động nghề nghiệp, tham gia đ&agrave;o tạo, bồi dưỡng, cập nhật kiến thức chuy&ecirc;n m&ocirc;n, nghiệp vụ, kỹ năng h&agrave;nh nghề hướng dẫn cho c&aacute;c hội vi&ecirc;n Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam.</li>\r\n<li>Trụ sở ch&iacute;nh của Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam đặt tại H&agrave; Nội. Hội c&oacute; thể mở văn ph&ograve;ng đại diện, chi nh&aacute;nh trong nước khi c&oacute; nhu cầu.</li>\r\n<li>Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam được tham gia c&aacute;c tổ chức quốc tế c&ugrave;ng nghề nghiệp, được lập văn ph&ograve;ng đại diện ở nước ngo&agrave;i theo quy định của ph&aacute;p luật.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"chuong\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;\">Chương II</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">NHIỆM VỤ, QUYỀN HẠN CỦA HỘI</h2>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 5. Nhiệm vụ</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Tuy&ecirc;n truyền gi&aacute;o dục để hội vi&ecirc;n nhận thức v&agrave; thực hiện đ&uacute;ng đường lối, chủ trương, ch&iacute;nh s&aacute;ch của Đảng v&agrave; ph&aacute;p luật của Nh&agrave; nước trong kinh doanh v&agrave; cung cấp dịch vụ hướng dẫn du lịch; động vi&ecirc;n c&aacute;c hội vi&ecirc;n t&iacute;ch cực tham gia c&aacute;c hoạt động li&ecirc;n quan đến hướng dẫn du lịch, bồi dưỡng v&agrave; đ&agrave;o tạo hướng dẫn vi&ecirc;n du lịch.</li>\r\n<li>Tham gia c&aacute;c hoạt động tuy&ecirc;n truyền, quảng b&aacute; h&igrave;nh ảnh đất nước, danh lam thắng cảnh, con người, truyền thống văn h&oacute;a v&agrave; lịch sử của d&acirc;n tộc Việt Nam với bạn b&egrave; quốc tế; tham gia x&acirc;y dựng đường lối, ch&iacute;nh s&aacute;ch ph&aacute;t triển Du lịch Việt Nam khi được y&ecirc;u cầu.</li>\r\n<li>Đại diện cho hội vi&ecirc;n kiến nghị với Nh&agrave; nước về những chủ trương, ch&iacute;nh s&aacute;ch, biện ph&aacute;p khuyến kh&iacute;ch, tạo điều kiện ph&aacute;t triển ng&agrave;nh Du lịch, bảo vệ quyền v&agrave; lợi &iacute;ch ch&iacute;nh đ&aacute;ng của hội vi&ecirc;n, giải quyết c&aacute;c trường hợp, vụ việc g&acirc;y thiệt hại đến quyền lợi của Ng&agrave;nh v&agrave; của hội vi&ecirc;n theo thẩm quyền của Hội, thực hiện nghĩa vụ đối với Nh&agrave; nước theo quy định của ph&aacute;p luật.</li>\r\n<li>Động vi&ecirc;n sự nhiệt t&igrave;nh v&agrave; khả năng s&aacute;ng tạo của hội vi&ecirc;n, đo&agrave;n kết, hợp t&aacute;c, hỗ trợ, gi&uacute;p đỡ nhau trong hoạt động cung cấp dịch vụ hướng dẫn du lịch tr&ecirc;n cơ sở n&acirc;ng cao kiến thức, trao đổi, phổ biến kinh nghiệm v&agrave; ứng dụng c&aacute;c c&ocirc;ng nghệ ti&ecirc;n tiến v&agrave;o nghề hướng dẫn du lịch.</li>\r\n<li>Tư vấn, hỗ trợ cho c&aacute;c hội vi&ecirc;n trong ph&aacute;t triển chuy&ecirc;n m&ocirc;n nghiệp vụ, t&igrave;m kiếm việc l&agrave;m. C&oacute; giải ph&aacute;p cung cấp th&ocirc;ng tin cho c&aacute;c hội vi&ecirc;n. Vận động hội vi&ecirc;n thực hiện nghi&ecirc;m t&uacute;c đạo đức nghề nghiệp, quy tắc ứng xử văn minh trong du lịch.</li>\r\n<li>Quản l&yacute; hội vi&ecirc;n để hội vi&ecirc;n thực hiện đ&uacute;ng c&aacute;c quy định của Luật du lịch v&agrave; quy định kh&aacute;c của ph&aacute;p luật c&oacute; li&ecirc;n quan.</li>\r\n<li>Phối hợp với c&aacute;c tổ chức li&ecirc;n quan trong Hiệp hội Du lịch Việt Nam nhằm đảm bảo thực hiện t&ocirc;n chỉ, mục đ&iacute;ch của Hội, g&oacute;p phần cho Hiệp hội Du lịch Việt Nam ph&aacute;t triển, trở th&agrave;nh ng&ocirc;i nh&agrave; chung của c&aacute;c doanh nghiệp du lịch v&agrave; lao động trong lĩnh vực du lịch.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 6. Quyền của Hội</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Tổ chức c&aacute;c hội nghị, hội thảo để trao đổi kinh nghiệm nghề nghiệp, khuyến kh&iacute;ch hợp t&aacute;c li&ecirc;n kết giữa c&aacute;c Hội vi&ecirc;n để n&acirc;ng cao chất lượng hoạt động cung cấp dịch vụ hướng dẫn du lịch.</li>\r\n<li>Tổ chức c&aacute;c hoạt động đ&agrave;o tạo, dịch vụ, tư vấn v&agrave; c&aacute;c hoạt động kh&aacute;c theo quy định của ph&aacute;p luật, Điều lệ Hiệp Du lịch Việt Nam v&agrave; Quy chế n&agrave;y.</li>\r\n<li>Tổ chức c&aacute;c cuộc thi nghiệp vụ hướng dẫn, phong tặng danh hiệu hướng dẫn vi&ecirc;n.</li>\r\n<li>Nghi&ecirc;n cứu, tham gia g&oacute;p &yacute;, đề xuất kiến nghị với Ch&iacute;nh phủ v&agrave; c&aacute;c cơ quan Nh&agrave; nước những ch&iacute;nh s&aacute;ch, luật ph&aacute;p trong lĩnh vực du lịch n&oacute;i chung v&agrave; hướng dẫn du lịch n&oacute;i ri&ecirc;ng.</li>\r\n<li>Ph&aacute;t triển hội vi&ecirc;n, x&acirc;y dựng cơ sở vật chất v&agrave; ph&aacute;t triển c&aacute;c mối quan hệ của Hội với c&aacute;c tổ chức, c&aacute; nh&acirc;n trong nước v&agrave; ở nước ngo&agrave;i theo quy định của ph&aacute;p luật, nhằm n&acirc;ng cao hiệu quả v&agrave; vị thế của Hội.</li>\r\n<li>Xuất bản tập san, c&aacute;c ấn phẩm v&agrave; c&aacute;c t&agrave;i liệu tuy&ecirc;n truyền, quảng b&aacute; về du lịch Việt Nam, trọng t&acirc;m l&agrave; c&ocirc;ng t&aacute;c hướng dẫn du lịch; X&acirc;y dựng hệ thống quản l&yacute;, hỗ trợ hoạt động, cung cấp th&ocirc;ng tin, t&igrave;m kiếm việc l&agrave;m cho hội vi&ecirc;n.</li>\r\n<li>Được g&acirc;y quỹ Hội tr&ecirc;n cơ sở hội ph&iacute; của hội vi&ecirc;n v&agrave; c&aacute;c nguồn thu từ hoạt động, dịch vụ, được tham gia g&oacute;p vốn trong c&aacute;c tổ chức kinh tế hoạt động theo quy định của ph&aacute;p luật để tự trang trải về kinh ph&iacute; hoạt động.</li>\r\n<li>Th&agrave;nh lập c&aacute;c chi hội, c&acirc;u lạc bộ chuy&ecirc;n ng&agrave;nh trong lĩnh vực hướng dẫn du lịch.</li>\r\n<li>Thực hiện c&aacute;c quyền kh&aacute;c của Hội theo quy định của ph&aacute;p luật.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"chuong\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;\">Chương III</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">HỘI VI&Ecirc;N</h2>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 7. H&igrave;nh thức Hội vi&ecirc;n</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Hội vi&ecirc;n ch&iacute;nh thức\r\n<p>Những người c&oacute; thẻ hướng dẫn vi&ecirc;n du lịch Việt Nam, t&aacute;n th&agrave;nh Quy chế Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam, tự nguyện đăng k&yacute; tham gia Hội, đ&oacute;ng lệ ph&iacute; gia nhập v&agrave; hội ph&iacute; được c&ocirc;ng nhận l&agrave; hội vi&ecirc;n ch&iacute;nh thức của Hội. Hội vi&ecirc;n của Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam cũng đồng thời l&agrave; hội vi&ecirc;n của Hiệp hội Du lịch Việt Nam.</p>\r\n</li>\r\n<li>Hội vi&ecirc;n li&ecirc;n kết\r\n<p>C&aacute;c tổ chức, c&aacute; nh&acirc;n hoạt động trong lĩnh vực du lịch hoặc li&ecirc;n quan đến du lịch, c&oacute; đ&oacute;ng g&oacute;p v&agrave; hỗ trợ cho hoạt động của Hội, tự nguyện đăng k&yacute; tham gia Hội, đ&oacute;ng lệ ph&iacute; gia nhập v&agrave; hội ph&iacute; được c&ocirc;ng nhận l&agrave; hội vi&ecirc;n li&ecirc;n kết của Hội. Đối với c&aacute;c hội vi&ecirc;n li&ecirc;n kết l&agrave; tổ chức, người được cử thay mặt Hội vi&ecirc;n li&ecirc;n kết tham gia Hội phải l&agrave; người đại diện c&oacute; thẩm quyền trong c&aacute;c tổ chức đ&oacute;, trường hợp người được cử tham gia Hội nghỉ hưu hoặc chuyển c&ocirc;ng t&aacute;c kh&aacute;c, th&igrave; hội vi&ecirc;n li&ecirc;n kết l&agrave; tổ chức phải cử người đại diện c&oacute; thẩm quyền thay thế.</p>\r\n</li>\r\n<li>Hội vi&ecirc;n danh dự\r\n<p>C&ocirc;ng d&acirc;n, c&aacute;c nh&agrave; quản l&yacute;, nh&agrave; khoa học c&oacute; c&ocirc;ng lao đối với sự nghiệp ph&aacute;t triển Ng&agrave;nh du lịch n&oacute;i chung v&agrave; nghề Hướng dẫn du lịch n&oacute;i ri&ecirc;ng được BCH Hội mời l&agrave;m Hội vi&ecirc;n danh dự.</p>\r\n</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 8. Điều kiện trở th&agrave;nh hội vi&ecirc;n</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Tổ chức v&agrave; c&aacute; nh&acirc;n n&oacute;i tại khoản 1, khoản 2 Điều 7 của Quy chế n&agrave;y c&oacute; đơn đăng k&yacute; tham gia Hội, được Ban Thường trực Hội c&ocirc;ng nhận.</li>\r\n<li>Hồ sơ hội vi&ecirc;n ch&iacute;nh thức gồm c&oacute;:\r\n<p>- Đơn đăng k&yacute; gia nhập Hội ( theo mẫu)</p>\r\n<p>- Phiếu th&ocirc;ng tin hội vi&ecirc;n c&oacute; d&aacute;n ảnh ch&acirc;n dung mầu cỡ 3c x 4cm( theo mẫu)</p>\r\n<p>- Bản sao Thẻ hướng dẫn vi&ecirc;n Du lịch</p>\r\n<p>- Bản sao Chứng minh thư/Căn cước c&ocirc;ng d&acirc;n</p>\r\n</li>\r\n<li>Hồ sơ hội vi&ecirc;n li&ecirc;n kết gồm c&oacute;:\r\n<ol style=\"list-style-type: lower-alpha;\">\r\n<li>Hồ sơ hội li&ecirc;n kết l&agrave; c&aacute; nh&acirc;n:\r\n<p>- Đơn đăng k&yacute; gia nhập Hội (theo mẫu)</p>\r\n<p>- Phiếu th&ocirc;ng tin hội vi&ecirc;n c&oacute; d&aacute;n ảnh ch&acirc;n dung mầu cỡ 3c x 4cm ( theo mẫu)</p>\r\n<p>- Bản sao c&oacute; chứng thực Chứng minh thư/Căn cước c&ocirc;ng d&acirc;n</p>\r\n</li>\r\n<li>Hồ sơ hội li&ecirc;n kết l&agrave; c&aacute; nh&acirc;n:\r\n<p>- Đơn đăng k&yacute; gia nhập Hội (theo mẫu)</p>\r\n<p>- Phiếu th&ocirc;ng tin hội vi&ecirc;n c&oacute; d&aacute;n ảnh ch&acirc;n dung mầu cỡ 3c x 4cm ( theo mẫu)</p>\r\n<p>- Bản sao c&oacute; chứng thực Chứng minh thư/Căn cước c&ocirc;ng d&acirc;n</p>\r\n</li>\r\n</ol>\r\n</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 9. Quyền của hội vi&ecirc;n</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Được Hội x&aacute;c nhận tư c&aacute;ch hội vi&ecirc;n để c&oacute; đủ điều kiện h&agrave;nh nghề hướng dẫn du lịch.</li>\r\n<li>Được tham gia c&aacute;c tổ chức th&agrave;nh vi&ecirc;n của Hội, được tham gia c&aacute;c hoạt động chuy&ecirc;n m&ocirc;n của Hội. Được Hội thu xếp thủ tục li&ecirc;n quan đến đ&oacute;ng bảo hiểm x&atilde; hội nếu c&oacute; nhu cầu.</li>\r\n<li>Được kiến nghị, đề đạt &yacute; kiến của m&igrave;nh với c&aacute;c cơ quan Nh&agrave; nước th&ocirc;ng qua Hội.</li>\r\n<li>Được ứng cử, đề cử v&agrave; bầu cử Ban chấp h&agrave;nh Hội v&agrave; c&aacute;c tổ chức kh&aacute;c của Hội theo quy tr&igrave;nh tổ chức của Hội quy định tại Quy chế n&agrave;y.</li>\r\n<li>Được tham dự c&aacute;c kh&oacute;a phổ biến kinh nghiệm, bồi dưỡng nghề nghiệp, n&acirc;ng cao tr&igrave;nh độ, được cung cấp th&ocirc;ng tin, t&agrave;i liệu, dự hội thảo, c&aacute;c lớp đ&agrave;o tạo, huấn luyện, tr&igrave;nh diễn kỹ thuật, tham quan khảo s&aacute;t ở trong v&agrave; ngo&agrave;i nước theo quy tr&igrave;nh, kế hoạch v&agrave; quy định của Hội.</li>\r\n<li>Được Hội hỗ trợ, giới thiệu việc l&agrave;m</li>\r\n<li>Được bảo vệ quyền v&agrave; lợi &iacute;ch ch&iacute;nh đ&aacute;ng, hợp ph&aacute;p trong hoạt động hướng dẫn du lịch.</li>\r\n<li>Được hưởng sự trợ gi&uacute;p của c&aacute;c tổ chức, c&aacute; nh&acirc;n l&agrave; Hội vi&ecirc;n danh dự, Hội vi&ecirc;n li&ecirc;n kết của Hội.</li>\r\n<li>Được quyền ra khỏi Hội.</li>\r\n<li>Hội vi&ecirc;n li&ecirc;n kết v&agrave; Hội vi&ecirc;n danh dự được hưởng c&aacute;c quyền như hội vi&ecirc;n ch&iacute;nh thức, trừ c&aacute;c quyền ứng cử, bầu cử v&agrave; biểu quyết.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 10. Nghĩa vụ của Hội vi&ecirc;n</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Chấp h&agrave;nh đ&uacute;ng đường lối, ch&iacute;nh s&aacute;ch của Đảng. Nghi&ecirc;m t&uacute;c thực hiện c&aacute;c quy định của Luật Du lịch v&agrave; c&aacute;c quy định kh&aacute;c của ph&aacute;p luật c&oacute; li&ecirc;n quan, c&aacute;c quy định của Điều lệ Hiệp hội Du lịch Việt Nam, của Quy chế v&agrave; c&aacute;c quy định của Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam; thực hiện nghị quyết của Hội, tuy&ecirc;n truyền ph&aacute;t triển Hội vi&ecirc;n mới. Bảo vệ uy t&iacute;n của Hội, kh&ocirc;ng được nh&acirc;n danh Hội trong c&aacute;c quan hệ đối ngoại khi chưa được tổ chức c&oacute; thẩm quyền của Hội ủy nhiệm.</li>\r\n<li>Tham gia c&aacute;c hoạt động v&agrave; sinh hoạt của Hội, đo&agrave;n kết, hợp t&aacute;c với c&aacute;c hội vi&ecirc;n kh&aacute;c để x&acirc;y dựng Hội ng&agrave;y c&agrave;ng vững mạnh.</li>\r\n<li>Đeo thẻ Hội vi&ecirc;n trong khi h&agrave;nh nghề hướng dẫn du lịch.</li>\r\n<li>Tu&acirc;n thủ quy tắc đạo đức v&agrave; ứng xử nghề nghiệp hướng dẫn du lịch</li>\r\n<li>Cung cấp th&ocirc;ng tin, số liệu cần thiết phục vụ cho c&ocirc;ng t&aacute;c quản l&yacute;, gi&aacute;m s&aacute;t, hỗ trợ hướng dẫn vi&ecirc;n của Hội.</li>\r\n<li>Đ&oacute;ng lệ ph&iacute; gia nhập v&agrave; hội ph&iacute; đầy đủ.</li>\r\n<li>Hội vi&ecirc;n danh dự kh&ocirc;ng phải đ&oacute;ng lệ ph&iacute; gia nhập Hội v&agrave; Hội ph&iacute;.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 11. Chấm dứt tư c&aacute;ch hội vi&ecirc;n</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Chấm dứt tư c&aacute;ch hội vi&ecirc;n trong c&aacute;c trường hợp sau:\r\n<ol style=\"list-style-type: lower-alpha;\">\r\n<li>Hội vi&ecirc;n tự nguyện xin r&uacute;t ra khỏi Hội</li>\r\n<li>Hội vi&ecirc;n vi phạm c&aacute;c quy định của ph&aacute;p luật về hướng dẫn du lịch, c&aacute;c điều cấm trong Luật Du lịch; vi phạm nghi&ecirc;m trọng Quy chế v&agrave; c&aacute;c quy định của Hội, l&agrave;m ảnh hưởng đến uy t&iacute;n v&agrave; t&agrave;i ch&iacute;nh của Hội;</li>\r\n<li>Hội vi&ecirc;n kh&ocirc;ng đ&oacute;ng hội ph&iacute; một năm.</li>\r\n<li>Hội vi&ecirc;n bị thu hồi thẻ hướng dẫn vi&ecirc;n du lịch.</li>\r\n<li>Hội vi&ecirc;n c&oacute; thẻ hướng dẫn vi&ecirc;n hết hạn tr&ecirc;n 3 th&aacute;ng nhưng chưa được gia hạn hay cấp mới.</li>\r\n<li>Những hướng dẫn vi&ecirc;n bị chấm dứt tư c&aacute;ch hội vi&ecirc;n c&oacute; nguyện vọng gia nhập lại Hội l&agrave;m thủ tục theo quy định của Điều 8. Những hướng dẫn vi&ecirc;n bị chấm dứt tư c&aacute;ch hội vi&ecirc;n theo Mục a Điều 11 chỉ được l&agrave;m thủ tục gia nhập Hội sau 6 th&aacute;ng kể từ ng&agrave;y bị chấm dứt tư c&aacute;ch hội vi&ecirc;n. Những hướng dẫn vi&ecirc;n bị chấm dứt tư c&aacute;ch hội vi&ecirc;n theo Mục b, c, d Điều 11 chỉ được l&agrave;m thủ tục gia nhập Hội sau 12 th&aacute;ng kể từ ng&agrave;y bị chấm dứt tư c&aacute;ch hội vi&ecirc;n.</li>\r\n</ol>\r\n</li>\r\n<li>Tạm dừng tư c&aacute;ch hội vi&ecirc;n: Trường hợp thẻ hết hạn hội vi&ecirc;n bị tạm dừng tư c&aacute;ch hội vi&ecirc;n cho đến khi được cấp lại thẻ hướng dẫn.</li>\r\n<li>Hội vi&ecirc;n li&ecirc;n kết l&agrave; tổ chức c&oacute; những vi phạm nghi&ecirc;m trọng, bị cơ quan nh&agrave; nước c&oacute; thẩm quyền r&uacute;t giấy ph&eacute;p kinh doanh, đ&igrave;nh chỉ hoạt động, bị giải thể hoặc tuy&ecirc;n bố ph&aacute; sản. Trường hợp tổ chức bị đ&igrave;nh chỉ tạm thời hoạt động trong một thời hạn th&igrave; tư c&aacute;ch hội vi&ecirc;n li&ecirc;n kết được tiếp tục khi tổ chức được ph&eacute;p hoạt động trở lại.\r\n<p>Ban Thường trực Hội th&ocirc;ng b&aacute;o c&ocirc;ng khai danh s&aacute;ch hội vi&ecirc;n bị tạm dừng hoặc chấm dứt tư c&aacute;ch hội vi&ecirc;n tr&ecirc;n trang th&ocirc;ng tin điện tử của Hội.</p>\r\n<p>Quyền v&agrave; nghĩa vụ của hội vi&ecirc;n bị chấm dứt ngay sau khi Ban Thường trực Hội ra th&ocirc;ng b&aacute;o.,</p>\r\n</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"chuong\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;\">Chương IV</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">TỔ CHỨC HỘI</h2>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 12. Nguy&ecirc;n tắc tổ chức v&agrave; hoạt động của Hội</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam được tổ chức v&agrave; hoạt động theo nguy&ecirc;n tắc tự nguyện, tự quản, b&igrave;nh đẳng, tự trang trải về t&agrave;i ch&iacute;nh v&agrave; tự chịu tr&aacute;ch nhiệm trước ph&aacute;p luật.</li>\r\n<li>C&aacute;c cơ quan của Hội hoạt động tr&ecirc;n cơ sở b&agrave;n bạc d&acirc;n chủ, l&atilde;nh đạo tập thể, thiểu số phục t&ugrave;ng đa số.</li>\r\n<li>Chương tr&igrave;nh hoạt động h&agrave;ng năm của Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam được Hiệp hội Du lịch Việt Nam ph&ecirc; duyệt.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 13. Cơ cấu tổ chức của Hội.</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Đại hội Đại biểu to&agrave;n quốc.</li>\r\n<li>Ban Chấp h&agrave;nh.</li>\r\n<li>Ban Kiểm tra của Hội</li>\r\n<li>C&aacute;c ban chuy&ecirc;n m&ocirc;n</li>\r\n<li>Văn ph&ograve;ng Hội</li>\r\n<li>C&aacute;c chi hội Hướng dẫn vi&ecirc;n du lịch tỉnh, th&agrave;nh phố</li>\r\n<li>C&aacute;c c&acirc;u lạc bộ hướng dẫn vi&ecirc;n (theo ng&ocirc;n ngữ, thị trường, loại h&igrave;nh)</li>\r\n<li>C&aacute;c đơn vị trực thuộc Hội</li>\r\n<li>C&aacute;c văn ph&ograve;ng đại diện, chi nh&aacute;nh</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 14. Đại hội đại biểu to&agrave;n quốc v&agrave; Đại hội bất thường.</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Đại hội đại biểu to&agrave;n quốc l&agrave; cơ quan L&atilde;nh đạo, c&oacute; thẩm quyền cao nhất của Hội. Đại hội tổ chức theo nhiệm kỳ 5 năm. Nhiệm vụ ch&iacute;nh của Đại hội:\r\n<ol style=\"list-style-type: lower-alpha;\">\r\n<li>Thảo luận v&agrave; th&ocirc;ng qua b&aacute;o c&aacute;o tổng kết nhiệm kỳ v&agrave; phương hướng, chương tr&igrave;nh, kế hoạch hoạt động của Hội nhiệm kỳ tới.</li>\r\n<li>Kiến nghị sửa đổi v&agrave; bổ sung Quy chế Hội (nếu c&oacute;).</li>\r\n<li>Thảo luận v&agrave; quyết định một số vấn đề quan trọng của Hội vượt qu&aacute; thẩm quyền giải quyết của Ban chấp h&agrave;nh Hội.</li>\r\n<li>Th&ocirc;ng qua b&aacute;o c&aacute;o t&agrave;i ch&iacute;nh của Hội nhiệm kỳ cũ v&agrave; th&ocirc;ng qua dự to&aacute;n kế hoạch t&agrave;i ch&iacute;nh của Hội nhiệm kỳ tới.</li>\r\n<li>Bầu Ban Chấp h&agrave;nh Hội</li>\r\n<li>Bầu Ban Kiểm tra Hội</li>\r\n</ol>\r\n</li>\r\n<li>Đại hội bất thường: để giải quyết những vấn đề cấp b&aacute;ch của Hội, Ban chấp h&agrave;nh Hội c&oacute; thể hỏi &yacute; kiến của từng hội vi&ecirc;n hoặc triệu tập Đại hội bất thường. Đại hội bất thường được triệu tập khi c&oacute; 2/3 ủy vi&ecirc;n Ban chấp h&agrave;nh Hội hoặc tr&ecirc;n &frac12; số hội vi&ecirc;n y&ecirc;u cầu.</li>\r\n<li>C&aacute;c Nghị quyết của Đại hội, Hội nghị, được th&ocirc;ng qua theo nguy&ecirc;n tắc tập trung d&acirc;n chủ, thiểu số phục t&ugrave;ng đa số.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 15. Ban Chấp h&agrave;nh Hội.</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Ban Chấp h&agrave;nh (BCH) Hội l&agrave; cơ quan l&atilde;nh đạo điều h&agrave;nh mọi hoạt động của Hội giữa 2 kỳ Đại hội. Nhiệm kỳ của Ban chấp h&agrave;nh theo nhiệm kỳ Đại hội. Cơ cấu của Ban Chấp h&agrave;nh c&oacute; đại diện Hiệp hội Du lịch Việt Nam, l&atilde;nh đạo hiệp hội du lịch một số địa phương; l&atilde;nh đạo hoặc người phụ tr&aacute;ch bộ phận hướng dẫn vi&ecirc;n của c&aacute;c doanh nghiệp kinh doanh dịch vụ lữ h&agrave;nh ti&ecirc;u biểu; c&aacute;c hướng dẫn vi&ecirc;n c&oacute; uy t&iacute;n, c&oacute; kinh nghiệm. Số lượng ủy vi&ecirc;n Ban chấp h&agrave;nh Hội do Đại hội quyết định. Ban Chấp h&agrave;nh được bầu trực tiếp bằng phiếu k&iacute;n hoặc biểu quyết bằng giơ tay v&agrave; phải đạt tr&ecirc;n 50% số đại biểu dự đại hội nhất tr&iacute;. Ban Chấp h&agrave;nh Hội ch&iacute;nh thức hoạt động sau khi c&oacute; quyết định ph&ecirc; duyệt của Hiệp hội Du lịch Việt Nam.</li>\r\n<li>Ủy vi&ecirc;n Ban Chấp h&agrave;nh Hội, phải l&agrave; người c&oacute; t&acirc;m huyết với Hội, c&oacute; đủ tr&igrave;nh độ v&agrave; khả năng điều h&agrave;nh, c&oacute; phẩm chất đạo đức, ch&iacute;nh trị tốt, c&oacute; sức khỏe v&agrave; thời gian để g&aacute;nh v&aacute;c c&aacute;c nhiệm vụ được giao.</li>\r\n<li>Đối với Ủy vi&ecirc;n Ban Chấp h&agrave;nh Hội l&agrave; đại diện tổ chức ph&aacute;p nh&acirc;n, khi về hưu hoặc chuyển c&ocirc;ng t&aacute;c kh&aacute;c sẽ được thay thế bằng một người kh&aacute;c của tổ chức ph&aacute;p nh&acirc;n đ&oacute;.</li>\r\n<li>Nhiệm vụ, quyền hạn của Ban Chấp h&agrave;nh Hội:\r\n<ol style=\"list-style-type: lower-alpha;\">\r\n<li>Quyết định biện ph&aacute;p thực hiện Nghị quyết, chương tr&igrave;nh hoạt động trong nhiệm kỳ của Đại hội.</li>\r\n<li>Quyết định chương tr&igrave;nh, kế hoạch c&ocirc;ng t&aacute;c h&agrave;ng năm v&agrave; th&ocirc;ng b&aacute;o kết quả hoạt động của Ban Chấp h&agrave;nh Hội cho c&aacute;c hội vi&ecirc;n, Chi hội, đơn vị trực thuộc Hội biết.</li>\r\n<li>Ph&ecirc; duyệt kế hoạch v&agrave; quyết to&aacute;n t&agrave;i ch&iacute;nh h&agrave;ng năm.</li>\r\n<li>Th&ocirc;ng qua c&aacute;c quy định tổ chức v&agrave; hoạt động c&aacute;c Ban Chuy&ecirc;n m&ocirc;n, Văn ph&ograve;ng Hội, Văn ph&ograve;ng đại diện của Hội tại c&aacute;c khu vực. Th&ocirc;ng qua c&aacute;c quy định về hoạt động của hội vi&ecirc;n.</li>\r\n<li>Bầu cử v&agrave; b&atilde;i miễn chức danh L&atilde;nh đạo của Hội: Chủ tịch, Ph&oacute; Chủ tịch v&agrave; Tổng thư k&yacute;, Trưởng Ban Kiểm tra tr&igrave;nh Hiệp hội Du lịch Việt Nam ph&ecirc; duyệt.</li>\r\n<li>Th&agrave;nh lập c&aacute;c đơn vị trực thuộc, c&aacute;c Ban Chuy&ecirc;n m&ocirc;n v&agrave; đại diện của Hội ở c&aacute;c khu vực.</li>\r\n<li>Th&ocirc;ng qua nội dung, chương tr&igrave;nh, c&aacute;c t&agrave;i liệu tr&igrave;nh Đại hội.</li>\r\n<li>Quyết định triệu tập Đại hội nhiệm kỳ.</li>\r\n<li>Quy định mức Lệ ph&iacute; gia nhập Hội v&agrave; Hội ph&iacute; của Hội vi&ecirc;n.</li>\r\n<li>Bầu bổ sung ủy vi&ecirc;n BCH, thường trực BKT (nếu thiếu).</li>\r\n<li>Th&ocirc;ng qua danh s&aacute;ch th&agrave;nh vi&ecirc;n Hội đồng chuy&ecirc;n m&ocirc;n.</li>\r\n</ol>\r\n</li>\r\n<li>C&aacute;c Quyết định của BCH Hội được lấy biểu quyết theo đa số th&agrave;nh vi&ecirc;n dự họp. Trong trường hợp số phiếu ngang nhau th&igrave; quyết định thuộc về Chủ tịch Hội.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 16: Ban Thường trực Hội.</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Ban Thường trực Hội c&oacute; Chủ tịch, c&aacute;c Ph&oacute; Chủ tịch, Tổng thư k&yacute; v&agrave; c&aacute;c ủy vi&ecirc;n thường trực kh&aacute;c do Ban Chấp h&agrave;nh bầu.</li>\r\n<li>Nhiệm vụ, quyền hạn của Ban Thường trực:\r\n<ol style=\"list-style-type: lower-alpha;\">\r\n<li>Thay mặt Ban Chấp h&agrave;nh chỉ đạo, điều h&agrave;nh c&aacute;c hoạt động của Hội giữa 2 kỳ họp của Ban Chấp h&agrave;nh.</li>\r\n<li>Quyết định những c&ocirc;ng việc khẩn cấp, sau đ&oacute; b&aacute;o c&aacute;o với Ban Chấp h&agrave;nh trong kỳ họp gần nhất.</li>\r\n<li>Ph&ecirc; duyệt bổ nhiệm, miễn nhiệm Chủ tịch, Ph&oacute; Chủ tịch c&aacute;c chi hội, c&aacute;c c&acirc;u lạc bộ; Trưởng, Ph&oacute; trưởng c&aacute;c ban chuy&ecirc;n m&ocirc;n, văn ph&ograve;ng đại diện, chi nh&aacute;nh.</li>\r\n<li>X&eacute;t kết nạp hội vi&ecirc;n, tạm dừng, chấm dứt tư c&aacute;ch hội vi&ecirc;n</li>\r\n<li>X&eacute;t khen thưởng, kỷ luật hội vi&ecirc;n</li>\r\n</ol>\r\n</li>\r\n<li>C&aacute;c Quyết định của Ban Thường trực Hội được lấy biểu quyết theo đa số th&agrave;nh vi&ecirc;n dự họp. Trong trường hợp số phiếu ngang nhau th&igrave; quyết định thuộc về Chủ tịch Hội.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 17. Ban Kiểm tra Hội</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Cơ cấu Ban Kiểm tra gồm:\r\n<ol style=\"list-style-type: lower-alpha;\">\r\n<li>Thường trực Ban Kiểm tra do Đại hội đại biểu Hội trực tiếp bầu ra. Số lượng ủy vi&ecirc;n thường trực Ban Kiểm tra do Đại hội quyết định.</li>\r\n<li>C&aacute;c th&agrave;nh vi&ecirc;n Ban Kiểm tra tại c&aacute;c chi hội do l&atilde;nh đạo c&aacute;c chi hội đề nghị, tại c&aacute;c đơnvị kh&aacute;c do Tổng thư k&yacute; Hội đề nghị, Chủ tịch Hội k&yacute; quyết định c&ocirc;ng nhận</li>\r\n</ol>\r\n</li>\r\n<li>Nhiệm kỳ của Thường trực Ban Kiểm tra theo nhiệm kỳ của Ban Chấp h&agrave;nh</li>\r\n<li>Thường trực Ban Kiểm tra c&oacute; nhiệm vụ kiểm tra, gi&aacute;m s&aacute;t hoạt động của Hội v&agrave; của Hội vi&ecirc;n. C&aacute;c th&agrave;nh vi&ecirc;n Ban Kiểm tra c&oacute; nhiệm vụ kiểm tra, gi&aacute;m s&aacute;t hoạt động của Hội vi&ecirc;n trong việc tu&acirc;n thủ Ph&aacute;p luật v&agrave; quy định của Nh&agrave; nước trong lĩnh vực hướng dẫn du lịch, trong việc thực hiện Quy chế v&agrave; c&aacute;c quy định của Hội.</li>\r\n<li>Ban Kiểm tra hoạt động theo Quy chế do Hiệp hội Du lịch Việt Nam ph&ecirc; duyệt. Trưởng Ban Kiểm tra x&acirc;y dựng Quy chế hoạt động tr&igrave;nh l&atilde;nh đạo Hiệp hội Du lịch Việt Nam ph&ecirc; duyệt.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 18. Chủ tịch v&agrave; Ph&oacute; Chủ tịch</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Chủ tịch Hội\r\n<ol style=\"list-style-type: lower-alpha;\">\r\n<li>Chủ tịch Hội l&agrave; người quản l&yacute;, điều h&agrave;nh to&agrave;n bộ hoạt động của Hội, tổ chức triển khai thực hiện c&aacute;c Nghị quyết của Đại hội, c&aacute;c Nghị quyết, quyết định của Ban Chấp h&agrave;nh Hội, quyết định Kế hoạch c&ocirc;ng t&aacute;c của Hiệp hội Du lịch Việt Nam.</li>\r\n<li>L&agrave; đại diện trước ph&aacute;p luật của Hội</li>\r\n<li>Triệu tập v&agrave; chủ tr&igrave; c&aacute;c cuộc họp của Ban Chấp h&agrave;nh Hội</li>\r\n<li>Trực tiếp chỉ đạo Tổng Thư k&yacute; Hội</li>\r\n<li>Ph&ecirc; duyệt nh&acirc;n sự của Văn ph&ograve;ng Hội v&agrave; c&aacute;c tổ chức, đơn vị kh&aacute;c do Hội th&agrave;nh lập, ph&ecirc; duyệt nh&acirc;n sự l&agrave; th&agrave;nh vi&ecirc;n Ban Kiểm tra.</li>\r\n<li>Chịu tr&aacute;ch nhiệm trước Hiệp hội Du lịch Việt Nam, trước Ban Chấp h&agrave;nh Hội v&agrave; to&agrave;n thể Hội vi&ecirc;n về c&aacute;c hoạt động của Hội.</li>\r\n</ol>\r\n</li>\r\n<li>Ph&oacute; Chủ tịch Hội</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 19.Tổng thư k&yacute; Hội.</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>L&agrave; người tổ chức điều h&agrave;nh trực tiếp mọi hoạt động của Văn ph&ograve;ng Hội, chịu tr&aacute;ch nhiệm trước Chủ tịch Hội, trước Ban Chấp h&agrave;nh Hội v&agrave; trước ph&aacute;p luật về mọi hoạt động của Văn ph&ograve;ng Hội.</li>\r\n<li>X&acirc;y dựng Quy chế hoạt động của Văn ph&ograve;ng Hội v&agrave; c&aacute;c Ban chuy&ecirc;n m&ocirc;n của Hội, Quy chế quản l&yacute; t&agrave;i ch&iacute;nh, t&agrave;i sản của Hội tr&igrave;nh Ban Chấp h&agrave;nh Hội ph&ecirc; duyệt.</li>\r\n<li>Định kỳ b&aacute;o c&aacute;o BCH Hội về c&aacute;c hoạt động của Hội v&agrave; của cơ quan Hội.</li>\r\n<li>Lập b&aacute;o c&aacute;o h&agrave;ng năm, b&aacute;o c&aacute;o nhiệm kỳ của Ban Chấp h&agrave;nh Hội</li>\r\n<li>Quản l&yacute; danh s&aacute;ch, hồ sơ v&agrave; t&agrave;i liệu về c&aacute;c hội vi&ecirc;n v&agrave; c&aacute;c tổ chức trực thuộc</li>\r\n<li>Chuẩn bị c&aacute;c cuộc họp của Ban Chấp h&agrave;nh Hội, c&aacute;c Hội nghị, Hội thảo do Hội tổ chức v&agrave; tổ chức triển khai c&aacute;c hoạt động kh&aacute;c theo Nghị quyết, quyết định của Ban Chấp h&agrave;nh Hội.</li>\r\n<li>Thực hiện c&aacute;c c&ocirc;ng việc do Chủ tịch Hội ủy quyền</li>\r\n<li>Gi&uacute;p việc Tổng thư k&yacute; c&oacute; một hoặc một số Ph&oacute; Tổng thư k&yacute;, do Tổng thư k&yacute; đề xuất, Chủ tịch Hội k&yacute; bổ nhiệm.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 20. Cơ quan Hội.</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Cơ quan Hội bao gồm Văn ph&ograve;ng Hội, c&aacute;c ban chuy&ecirc;n m&ocirc;n, văn ph&ograve;ng đại diện, chi nh&aacute;nh của Hội. C&aacute;c đơn vị trong cơ quan Hội được tổ chức v&agrave; hoạt động theo Quy chế do Tổng thư k&yacute; tr&igrave;nh Ban Chấp h&agrave;nh Hội ph&ecirc; duyệt.</li>\r\n<li>C&aacute;c nh&acirc;n vi&ecirc;n được tuyển dụng v&agrave; l&agrave;m việc theo quy định của Luật Lao động v&agrave; Quy định của Hiệp hội Du lịch Việt Nam.</li>\r\n<li>Kế hoạch kinh ph&iacute; hoạt động của Cơ quan Hội, Chi hội do Chủ tịch Hội x&acirc;y dựng, tr&igrave;nh L&atilde;nh đạo Hiệp hội Du lịch Việt Nam ph&ecirc; duyệt.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 21. C&acirc;u lạc bộ chuy&ecirc;n ng&agrave;nh Hướng dẫn Du lịch.</h2>\r\n<p>T&ugrave;y theo nhu cầu hoạt động, Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam th&agrave;nh lập c&aacute;c c&acirc;u lạc bộ chuy&ecirc;n ng&agrave;nh hướng dẫn theo ng&ocirc;n ngữ, theo thị trường, theo loại h&igrave;nh du lịch hoặc theo địa b&agrave;n.</p>\r\n<p>C&acirc;u lạc bộ chuy&ecirc;n ng&agrave;nh Hướng dẫn Du lịch được th&agrave;nh lập v&agrave; hoạt động trong cơ cấu thống nhất của Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam. C&acirc;u lạc bộ hoạt động theo quy định của ph&aacute;p luật, Quy chế Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam v&agrave; Điều lệ Hiệp hội Du lịch Việt Nam.</p>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 22. Chi hội Hướng dẫn vi&ecirc;n Du lịch tại c&aacute;c tỉnh, th&agrave;nh phố trực thuộc Trung ương .</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Chi hội Hướng dẫn vi&ecirc;n Du lịch tại c&aacute;c tỉnh, th&agrave;nh phố trực thuộc Trung ương l&agrave; tổ chức trực thuộc Hội Hướng dẫn vi&ecirc;n Du lịch Việt Nam, được Hiệp hội Du lịch Việt Nam th&agrave;nh lập tr&ecirc;n cơ sở đề xuất của Hiệp hội du lịch c&aacute;c tỉnh, th&agrave;nh phố.</li>\r\n<li>Hiệp hội du lịch c&aacute;c tỉnh, th&agrave;nh phố trực tiếp quản l&yacute; chi hội Hướng dẫn vi&ecirc;n Du lịch về tổ chức, nh&acirc;n lực; hướng dẫn hoạt động của chi hội tr&ecirc;n địa b&agrave;n; đề xuất nh&acirc;n sự với Hiệp hội Du lịch Việt Nam để Hiệp hội Du lịch Việt Nam bổ nhiệm l&atilde;nh đạo chi hội.</li>\r\n<li>Ban l&atilde;nh đạo chi hội c&oacute; Chủ tịch, c&aacute;c Ph&oacute; Chủ tịch, Trưởng bộ phận phụ tr&aacute;ch c&ocirc;ng t&aacute;c bồi dưỡng nghiệp vụ hướng dẫn, c&ocirc;ng t&aacute;c kiểm tra, hướng dẫn vi&ecirc;n quốc tế, hướng dẫn vi&ecirc;n du lịch nội địa</li>\r\n<li>Chi hội đề xuất nh&acirc;n sự tham gia Ban Kiểm tra để phối hợp với c&aacute;c cơ quan quản l&yacute; nh&agrave; nước về du lịch địa phương kiểm tra, gi&aacute;m s&aacute;t c&aacute;c hoạt động hướng dẫn của Hội vi&ecirc;n tr&ecirc;n địa b&agrave;n.</li>\r\n<li>Chi hội c&oacute; nhiệm vụ đ&ocirc;n đốc, hướng dẫn hội vi&ecirc;n thực hiện Quy chế, quy định, nghị quyết, kế hoạch hoạt động của Hội hướng dẫn vi&ecirc;n Du lịch Việt Nam; giải quyết những c&ocirc;ng việc đột xuất theo chỉ đạo của Hội hướng dẫn vi&ecirc;n Du lịch Việt Nam.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 23: Hội đồng chuy&ecirc;n m&ocirc;n.</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Hội đồng chuy&ecirc;n m&ocirc;n gồm c&aacute;c chuy&ecirc;n gia trong lĩnh vực du lịch v&agrave; hướng dẫn</li>\r\n<li>Hội đồng chuy&ecirc;n m&ocirc;n c&oacute; nhiệm vụ tư vấn cho Hội trong c&aacute;c hoạt động theo nhiệm vụ, quyền hạn của Hội</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"chuong\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;\">Chương V</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">T&Agrave;I CH&Iacute;NH, T&Agrave;I SẢN CỦA HỘI</h2>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 24. Nguồn thu của Hội</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Lệ ph&iacute; gia nhập Hội.</li>\r\n<li>Hội ph&iacute; của hội vi&ecirc;n đ&oacute;ng g&oacute;p theo quy định.</li>\r\n<li>T&agrave;i trợ của c&aacute;c tổ chức v&agrave; c&aacute; nh&acirc;n trong v&agrave; ngo&agrave;i nước theo quy định của ph&aacute;p luật.</li>\r\n<li>C&aacute;c khoản thu hợp ph&aacute;p kh&aacute;c</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 25. C&aacute;c khoản chi của Hội</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Chi một phần hội ph&iacute; cho hoạt động của chi hội hướng dẫn vi&ecirc;n tại c&aacute;c tỉnh, th&agrave;nh phố. Tỷ lệ chi cho chi hội từng tỉnh, th&agrave;nh phố do Ban Chấp h&agrave;nh quy định.</li>\r\n<li>Chi trả lương cho nh&acirc;n vi&ecirc;n, bồi dưỡng cộng t&aacute;c vi&ecirc;n.</li>\r\n<li>Chi cho cơ sở vật chất kỹ thuật, giao tiếp từ thiện.</li>\r\n<li>C&aacute;c khoản chi hợp l&yacute; kh&aacute;c do Ban Chấp h&agrave;nh Hội quyết định.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 26. Quản l&yacute;, sử dụng t&agrave;i ch&iacute;nh v&agrave; t&agrave;i sản của Hội</h2>\r\n<ol style=\"list-style-type: decimal;\">\r\n<li>Hiệp hội Du lịch Việt Nam thống nhất quản l&yacute; t&agrave;i ch&iacute;nh, ban h&agrave;nh quy định sử dụng ng&acirc;n s&aacute;ch của Hội</li>\r\n<li>Ban Chấp h&agrave;nh Hội quy định việc sử dụng t&agrave;i ch&iacute;nh v&agrave; t&agrave;i sản của Hội ph&ugrave; hợp với quy định của nh&agrave; nước, của Hiệp hội Du lịch Việt Nam.</li>\r\n<li>Ban Kiểm tra c&oacute; tr&aacute;ch nhiệm kiểm tra v&agrave; b&aacute;o c&aacute;o t&agrave;i ch&iacute;nh, t&agrave;i sản c&ocirc;ng khai h&agrave;ng năm.</li>\r\n<li>Khi Hội giải thể, t&agrave;i sản, t&agrave;i ch&iacute;nh của Hội phải được kiểm tra v&agrave; xử l&yacute; theo quy định của ph&aacute;p luật.</li>\r\n</ol>\r\n</div>\r\n</div>\r\n<div class=\"chuong\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;\">Chương VI</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">KHEN THƯỞNG, KỶ LUẬT</h2>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 27. Khen thưởng</h2>\r\n<p>Hội vi&ecirc;n, Ủy vi&ecirc;n Ban Chấp h&agrave;nh Hội, Ban Kiểm tra, c&aacute;n bộ, nh&acirc;n vi&ecirc;n của Hội c&oacute; nhiều th&agrave;nh t&iacute;ch, đ&oacute;ng g&oacute;p t&iacute;ch cực v&agrave;o sự ph&aacute;t triển của Hội v&agrave; sự nghiệp ph&aacute;t triển ng&agrave;nh Du lịch, được Hội khen thưởng hoặc đề nghị Hiệp hội Du lịch Việt Nam v&agrave; c&aacute;c cơ quan c&oacute; thẩm quyền khen thưởng.</p>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 28. Kỷ luật.</h2>\r\n<p>Hội vi&ecirc;n, Ủy ban Ban Chấp h&agrave;nh Hội, Ban Kiểm tra, c&aacute;n bộ, nh&acirc;n vi&ecirc;n của Hội hoạt động tr&aacute;i với Quy chế, Nghị quyết của Hội, l&agrave;m tổn hại đến uy t&iacute;n, danh dự của Hội, bỏ sinh hoạt thường kỳ nhiều lần kh&ocirc;ng c&oacute; l&yacute; do ch&iacute;nh đ&aacute;ng, kh&ocirc;ng đ&oacute;ng hội ph&iacute; 1 năm, t&ugrave;y theo mức độ sẽ bị ph&ecirc; b&igrave;nh, khiển tr&aacute;ch, cảnh c&aacute;o, x&oacute;a t&ecirc;n trong danh s&aacute;ch hội vi&ecirc;n hoặc đề nghị c&aacute;c cơ quan c&oacute; thẩm quyền xử l&yacute; vi phạm theo quy định của ph&aacute;p luật.</p>\r\n</div>\r\n</div>\r\n<div class=\"chuong\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;\">Chương VII</h2>\r\n<h2 style=\"font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;\">ĐIỀU KHOẢN THI H&Agrave;NH</h2>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 29: Điều khoản chuyển đổii</h2>\r\n<p>C&aacute;c Chi hội, c&aacute;c C&acirc;u lạc bộ hướng dẫn vi&ecirc;n do Hiệp hội Du lịch c&aacute;c tỉnh, th&agrave;nh phố trực thuộc Trung ương th&agrave;nh lập trước ng&agrave;y 10/10/2017 vẫn tiếp tục hoạt động. Trong thời gian 6 th&aacute;ng chuyển sang cơ chế hoạt động mới quy định trong Quy chế n&agrave;y.</p>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 30. Sửa đổi, bổ sung Quy chế</h2>\r\n<p>CViệc sửa đổi, bổ sung Quy chế phải được Đại hội th&ocirc;ng qua hoặc c&oacute; &iacute;t nhất tr&ecirc;n &frac12; số hội vi&ecirc;n ch&iacute;nh thức nhất tr&iacute; kiến nghị v&agrave; được Hiệp hội Du lịch Việt Nam ph&ecirc; duyệt mới c&oacute; gi&aacute; trị thực hiện.</p>\r\n</div>\r\n<div style=\"text-align: justify;\">\r\n<h2 style=\"font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;\">Điều 31. Hiệu lực thi h&agrave;nh.</h2>\r\n<p>Quy chế n&agrave;y gồm 7 chương, 31 điều, c&oacute; hiệu lực thi h&agrave;nh kể từ ng&agrave;y được Chủ tịch Hiệp hội Du lịch Việt Nam k&yacute; quyết định ban h&agrave;nh./.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</body>\r\n</html>', '1', '2018-04-04 00:00:00', null, '4', null, '2018-04-04 20:57:58', '2018-04-04 20:57:58', null);
INSERT INTO `news` VALUES ('5', 'Tầm nh&igrave;n v&agrave; sứ mệnh', null, null, null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<div id=\"tamnhinsumenh\" class=\"table-content active\">\r\n<h4 class=\"heading\"><span class=\"text-heading\">Sứ mệnh</span></h4>\r\n<div class=\"main-content\">\r\n<p>- Tập hợp c&aacute;c hướng dẫn vi&ecirc;n giỏi tr&ecirc;n to&agrave;n quốc</p>\r\n<p>- X&acirc;y dựng hệ thống h&agrave;nh ch&iacute;nh, tạo điều kiện thuận lợi cho việc hoạt động v&agrave; sự ph&aacute;t triển của hướng dẫn vi&ecirc;n</p>\r\n<p>- Xậy dựng hệ thống th&ocirc;ng tin, hệ thống đ&agrave;o tạo to&agrave;n diện, hiện đại</p>\r\n<p>- Tạo s&acirc;n chơi l&agrave;nh mạnh, th&acirc;n thiện cho cộng đồng hướng dẫn vi&ecirc;n du lịch Việt Nam</p>\r\n<p>- X&acirc;y dựng hệ thống Review, phản hồi của kh&aacute;ch h&agrave;ng của c&aacute;c C&ocirc;ng ty lữ h&agrave;nh</p>\r\n</div>\r\n<h4 class=\"heading\">&nbsp;</h4>\r\n<h4 class=\"heading\"><span class=\"text-heading\">Tầm nh&igrave;n</span></h4>\r\n<div class=\"main-content\">\r\n<p>- Thu h&uacute;t v&agrave; đ&agrave;o tạo nh&acirc;n t&agrave;i</p>\r\n<p>- Ph&aacute;t triển khung năng lực th&agrave;nh vi&ecirc;n</p>\r\n<p>- Đưa hướng dẫn vi&ecirc;n Du lịch Việt Nam vươn tầm quốc tế</p>\r\n</div>\r\n</div>\r\n</body>\r\n</html>', '1', '2018-04-04 00:00:00', null, '2', null, '2018-04-04 20:58:56', '2018-04-04 21:09:13', null);
INSERT INTO `news` VALUES ('6', 'Grab nói gì về sự cố \"sập\" hệ thống, ứng dụng tê liệt?', null, null, 'Sau nhiều giờ tê liệt, đến nay ứng dụng của Grab đã hoạt động bình thường trở lại.', '<p style=\"text-align: justify;\">Từ chiều tối 3/4, nhiều hành khách đặt xe qua <a href=\"http://www.baogiaothong.vn/grab-sap-he-thong-hang-ngan-tai-xe-nguy-co-mat-tien-oan-d250496.html\">Grab</a>&nbsp;cho biết không thể truy cập vào ứng dụng được, họ buộc phải chuyển qua sử dụng Uber và taxi truyền thống thay thế. Cả khách hàng và lái xe Grab đều không truy cập được vào ứng dụng trong nhiều giờ.</p>\r\n<p style=\"text-align: justify;\">Không chỉ hành khách, nhiều đối tác là lái xe của Grab cũng không hiểu chuyện gì đang xảy ra. Lái xe GrabCar Nguyễn Văn Trọng (Thanh Xuân, Hà Nội) cho biết, sáng 3/4 anh nhận được thông báo của Grab qua ứng dụng nhưng không hiểu gì vì nội dung hiển thị toàn chữ nước ngoài. Đến chiều tối không thể truy cập được nữa.</p>\r\n<p style=\"text-align: justify;\">Lỗi không truy cập được vào ứng dụng Grab xảy ra trên diện rộng, nhiều lái xe Grab không hiểu chuyện gì đang xảy ra. Có ý kiến cho rằng do bảo trì, nâng cấp hệ thống nhưng không ít lái xe tỏ thái độ bức xúc vì họ không được Grab thông báo.</p>\r\n<p style=\"text-align: justify;\">Trao đổi với Báo Giao thông, đại diện truyền thông của Grab cho biết,&nbsp;chiều tối 3/4, Ứng dụng Grab &nbsp;có một vấn đề kỹ thuật nhỏ trong một thời gian ngắn, do đó một vài khách hàng và đối tác tài xế có thể gặp khó khăn khi sử dụng ứng dụng. “Grab thành thật xin lỗi vì sự bất tiện này. Chúng tôi đã tích cực khắc phục ngay trong ngày và ứng dụng hiện đã hoạt động lại bình thường”, đại diện truyền thông Grab nói.</p>\r\n<p style=\"text-align: justify;\">Tuy nhiên, ngày 4/4, chia sẻ với PV, một số lái xe Grab cho biết, truy cập vào lại hệ thống vẫn được báo “Điện thoại này chưa được đăng ký. Vui lòng nhấp vào Trung tâm trợ giúp để biết thêm thông tin”. Anh Trọng cho biết, khi gặp lỗi này phải xoá ứng dụng trên điện thoại sau đó tải về lại và đăng nhập bình thường. Thực hiện các thao tác này, ngay sáng hôm đó, ứng dụng đã hoạt động bình thường.</p>\r\n<p style=\"text-align: justify;\">“Vì không truy cập được ứng dụng nên chúng tôi không thể bắt khách, nhiều anh em tài xế tưởng bị Grab khoá tài khoản. Từ hôm qua, những lái xe bị “treo máy” đã phải cởi bỏ áo Grab để hoạt động và trả giá với hành khách không khác gì xe ôm truyền thống”, anh Nguyễn Văn Tú (Hà Nội), một tài xế GrabBike nói.</p>', '1', null, null, 'GT001', null, '2018-04-05 21:28:01', '2018-04-05 21:28:04', null);
INSERT INTO `news` VALUES ('7', 'Tài xế đánh lái cứu 2 nữ sinh phải bồi thường bao nhiêu?', null, null, 'Liên quan đến vụ TNGT liên hoàn giữa xe tải với 2 xe ô tô con, trao đổi với PV Báo Giao thông, anh Đỗ Văn Tiến - tài xế đã đánh lái cứu mạng 2 nữ sinh cho biết, anh đang chờ kết quả giám định từ cơ quan công an để giải quyết việc bồi thường thiệt hại sau vụ tai nạn.', '<p style=\"text-align:justify\">Anh Tiến cho biết, chiều 4/4, anh đ&atilde; c&ugrave;ng chủ xe tải l&agrave;m việc với chủ 2 chiếc xe &ocirc; t&ocirc; do anh đ&acirc;m phải sau c&uacute; đ&aacute;nh l&aacute;i cứu mạng 2 nữ sinh, nhưng hiện tại b&ecirc;n chủ 2 chiếc xe kia đang chờ cơ quan c&ocirc;ng an gi&aacute;m định thiệt hại mới thống nhất việc đền b&ugrave;.</p>\r\n\r\n<p style=\"text-align:justify\"><br />\r\nAnh Tiến cũng cho biết th&ecirc;m, xe tải do anh điều khiển hiện đang bị tạm giữ tại cơ quan c&ocirc;ng an. Chiếc xe Kia Morning sau khi cơ quan chức năng gi&aacute;m định xong đ&atilde; ho&agrave;n tất việc sửa chữa, chỉ c&ograve;n chiếc Toyota trị gi&aacute; gần 2 tỷ đồng vẫn chưa c&oacute; kết quả gi&aacute;m định.&ldquo;T&ocirc;i đ&atilde; li&ecirc;n hệ với b&ecirc;n chủ 2 chiếc xe &ocirc; t&ocirc; v&agrave; họ cho biết đ&atilde; ủy quyền cho b&ecirc;n CSGT gi&aacute;m định, đ&aacute;nh gi&aacute; thiệt hại. Họ muốn t&ocirc;i sửa chữa, thay mới những chỗ hư hỏng tr&ecirc;n 2 chiếc xe &ocirc; t&ocirc; như hiện trạng ban đầu&rdquo;, anh Tiến chia sẻ.</p>\r\n\r\n<table class=\"figure\" style=\"background-attachment:initial; background-clip:initial; background-image:none; background-origin:initial; background-position:initial; background-repeat:initial; background-size:initial; border:none !important; color:rgb(0, 0, 0); font-family:arial,helvetica,sans-serif; height:inherit !important; line-height:18px; list-style:none; margin:5px auto !important; outline:none; padding:0px !important; text-align:justify; width:1px !important\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"IMAG3699[1]\" src=\"http://cdn.baogiaothong.vn/files/hoa.pham/2018/03/29/183829-imag36991.jpg\" style=\"background:none; border:none !important; line-height:18px; list-style:none; max-width:492px !important; outline:none\" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<h2 style=\"font-style:italic; text-align:center\"><span style=\"font-size:small\"><em><strong>Xe tải do anh Tiến điều khiển cũng bị hư hỏng nặng sau c&uacute; đ&aacute;nh l&aacute;i cứu mạng 2 nữ sinh&nbsp;</strong></em></span></h2>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"text-align:justify\">Ngay sau vụ tai nạn, khi xem x&eacute;t ph&iacute;a bảo hiểm đ&atilde; quyết định chi trả 100 triệu đồng cho 3 chiếc xe gồm xe tải do anh Tiến điều khiển v&agrave; 2 chiếc xe &ocirc; t&ocirc; v&igrave; c&aacute;c xe chỉ mua bảo hiểm bắt buộc. V&agrave; dự t&iacute;nh số tiền anh Tiến phải bồi thường l&agrave; 240 triệu đồng.</p>\r\n\r\n<p style=\"text-align:justify\">Trước đ&oacute;, th&ocirc;ng tin anh Tiến đứng trước nguy cơ phải đền b&ugrave; một số tiền kh&aacute; lớn trong khi ho&agrave;n cảnh gia đ&igrave;nh rất kh&oacute; khăn đ&atilde; được chia sẻ rộng r&atilde;i tr&ecirc;n c&aacute;c diễn đ&agrave;n, c&aacute;c trang mạng x&atilde; hội; nhiều tổ chức, c&aacute; nh&acirc;n đ&atilde; k&ecirc;u gọi v&agrave; ủng hộ với gia đ&igrave;nh anh. Đến h&ocirc;m nay, anh Tiến đ&atilde; nhận được số tiền gần 305 triệu đồng ủng hộ, trong đ&oacute; gồm: 240 triệu đồng của doanh nh&acirc;n Nguyễn Ho&agrave;i Nam, khoảng 60 triệu đồng chuyển đến t&agrave;i khoản ng&acirc;n h&agrave;ng của vợ anh v&agrave; 5 triệu tiền mặt từ một c&ocirc;ng ty bảo hiểm ở Hải Ph&ograve;ng.</p>\r\n\r\n<p style=\"text-align:justify\">Hiện, anh Tiến đang chờ cơ quan c&ocirc;ng an th&ocirc;ng b&aacute;o kết quả gi&aacute;m định để ho&agrave;n thiện hồ sơ giải quyết nhanh ch&oacute;ng việc bồi thường vụ tai nạn.</p>\r\n', '1', null, null, 'GT001', null, '2018-04-05 21:30:51', '2018-04-05 21:30:54', null);
INSERT INTO `news` VALUES ('8', 'Đề xuất nâng cấp sân bay Điện Biên đón tàu bay cỡ lớn', null, null, 'Tại buổi làm việc với Bộ GTVT vào chiều nay (5/4), ông Mùa A Sơn, Chủ tịch UBND tỉnh Điện Biên cho biết, Cảng hàng không (CHK) Điện Biên được xây dựng từ thời kỳ Pháp thuộc. Trải qua nhiều lần cải tạo, nâng cấp, hiện tại, cảng chỉ có một đường cất, hạ cánh và khai thác được duy nhất loại máy bay ATR72 và tương đương trở xuống.', '<p style=\"text-align:justify\">&ldquo;Cảng chỉ khai th&aacute;c được v&agrave;o ban ng&agrave;y v&agrave; phụ thuộc rất nhiều v&agrave;o điều kiện thời tiết, kh&iacute; tượng. Đồng thời, c&aacute;c trang thiết bị dẫn đường hiện đại chưa được đầu tư n&ecirc;n hoạt động rất kh&oacute; khăn&rdquo;, &ocirc;ng Sơn n&oacute;i.</p>\r\n\r\n<p style=\"text-align:justify\">Cũng theo &ocirc;ng Sơn, tại Quyết định 2501 ng&agrave;y 28/8/2017 của Bộ GTVT về việc ph&ecirc; duyệt điều chỉnh quy hoạch chi tiết CHK Điện Bi&ecirc;n n&ecirc;u r&otilde;, trong giai đoạn đến năm 2020 sẽ n&acirc;ng cấp theo ti&ecirc;u chuẩn s&acirc;n bay 3C v&agrave; s&acirc;n bay qu&acirc;n sự cấp II, c&ocirc;ng suất 300.000 h&agrave;nh kh&aacute;ch/năm v&agrave; 500 tấn h&agrave;ng h&oacute;a/năm, loại t&agrave;u bay khai th&aacute;c A320, A321 v&agrave; tương đương,&hellip;</p>\r\n\r\n<p style=\"text-align:justify\">Tr&ecirc;n cơ sở đ&oacute;, Chủ tịch UBND tỉnh Điện Bi&ecirc;n kiến nghị Bộ GTVT đề xuất Ch&iacute;nh phủ v&agrave; c&aacute;c bộ, ng&agrave;nh ưu ti&ecirc;n bổ sung danh mục dự &aacute;n cải tạo, n&acirc;ng cấp CHK Điện Bi&ecirc;n với tổng vốn 2.146 tỷ đồng từ nguồn vốn ng&acirc;n s&aacute;ch Trung ương. Trước mắt, ưu ti&ecirc;n đầu tư x&acirc;y dựng hệ thống đường cất, hạ c&aacute;nh, đường lăn, s&acirc;n đỗ để đảm bảo giai đoạn đến năm 2020 khai th&aacute;c được t&agrave;u bay A320/321 v&agrave; tương đương trở l&ecirc;n.</p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Phương &aacute;n kh&aacute;c, Bộ GTVT b&aacute;o c&aacute;o Ch&iacute;nh phủ c&oacute; cơ chế thu h&uacute;t đầu tư từ c&aacute;c th&agrave;nh phần kinh tế để đầu tư dự &aacute;n theo h&igrave;nh thức đối t&aacute;c c&ocirc;ng tư&rdquo;, &ocirc;ng Sơn n&oacute;i v&agrave; cho biết, tỉnh Điện Bi&ecirc;n cam kết sẽ thực hiện tốt c&ocirc;ng t&aacute;c GPMB để thi c&ocirc;ng dự &aacute;n, chủ động thu h&uacute;t nguồn lực từ c&aacute;c tổ chức, c&aacute; nh&acirc;n, c&aacute;c th&agrave;nh phần kinh tế, doanh nghiệp tham gia đầu tư theo h&igrave;nh thức x&atilde; hội h&oacute;a.</p>\r\n\r\n<table class=\"figure\" style=\"background-attachment:initial; background-clip:initial; background-image:none; background-origin:initial; background-position:initial; background-repeat:initial; background-size:initial; border:none !important; color:rgb(0, 0, 0); font-family:arial,helvetica,sans-serif; height:inherit !important; line-height:18px; list-style:none; margin:5px auto !important; outline:none; padding:0px !important; text-align:justify; width:1px !important\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"dien bien\" src=\"http://cdn.baogiaothong.vn/files/quang.nguyen/2018/04/05/201852-dien-bien.jpg\" style=\"background:none; border:none !important; line-height:18px; list-style:none; max-width:492px !important; outline:none\" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<h2 style=\"font-style:italic\"><em>Đo&agrave;n c&ocirc;ng t&aacute;c của tỉnh Điện Bi&ecirc;n do &ocirc;ng Trần Văn Sơn, B&iacute; thư Tỉnh ủy l&agrave;m Trưởng đo&agrave;n l&agrave;m việc với Bộ GTVT v&agrave;o chiều nay 5/4</em></h2>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p style=\"text-align:justify\">&Ocirc;ng Đinh Việt Thắng, Cục trưởng Cục H&agrave;ng kh&ocirc;ng VN cho biết, năm 2017, CHK Điện Bi&ecirc;n khai th&aacute;c khoảng 1.200 chuyến bay, vận chuyển khoảng 70.000 h&agrave;nh kh&aacute;ch. Tuy nhi&ecirc;n, từ đầu năm 2018 đến nay, số lượng khai th&aacute;c chuyến bay v&agrave; khối lượng vận chuyển h&agrave;nh kh&aacute;ch của cảng đang giảm s&uacute;t.</p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Nguy&ecirc;n nh&acirc;n giảm c&ocirc;ng suất khai th&aacute;c do điều kiện thời tiết phức tạp, trang thiết bị kh&ocirc;ng đảm bảo điều kiện khai th&aacute;c khi thời tiết xấu&rdquo;, &ocirc;ng Thắng n&oacute;i v&agrave; cho biết, hiện nay, mỗi ng&agrave;y s&acirc;n bay Điện Bi&ecirc;n chỉ khai th&aacute;c được 2 chuyến bay. Kh&ocirc;ng đảm bảo hiệu quả khai th&aacute;c n&ecirc;n Vietnam Airlines đang mong muốn bỏ đường bay đến Điện Bi&ecirc;n.</p>\r\n\r\n<p style=\"text-align:justify\">Theo &ocirc;ng Thắng, việc đầu tư cải tảo, n&acirc;ng cấp CHK Điện Bi&ecirc;n hiện rất cấp thiết, khi ho&agrave;n th&agrave;nh đầu tư, s&acirc;n bay sẽ khai th&aacute;c được loại t&agrave;u bay cỡ lớn như A320/321, với lưu lượng vận chuyển h&agrave;nh kh&aacute;ch dự b&aacute;o khoảng 200.000 h&agrave;nh kh&aacute;ch/năm. &ldquo;Cục H&agrave;ng kh&ocirc;ng VN kiến nghị Bộ GTVT c&oacute; văn bản b&aacute;o c&aacute;o Thủ tướng Ch&iacute;nh phủ giao cho UBND tỉnh Điện Bi&ecirc;n l&agrave; cơ quan nh&agrave; nước c&oacute; thẩm quyền để triển khai dự &aacute;n theo h&igrave;nh thức x&atilde; hội h&oacute;a&rdquo;, &ocirc;ng Thắng n&oacute;i.</p>\r\n\r\n<p style=\"text-align:justify\">Ph&aacute;t biểu tại buổi l&agrave;m việc, Bộ trưởng Bộ GTVT Nguyễn Văn Thể cho biết, việc n&acirc;ng cấp s&acirc;n bay Điện Bi&ecirc;n c&oacute; &yacute; nghĩa rất quan trọng kh&ocirc;ng chỉ với ph&aacute;t triển KT-XH của Điện Bi&ecirc;n, m&agrave; cả khu vực T&acirc;y Bắc. &ldquo;Bộ GTVT sẽ phối hợp chặt chẽ với UBND tỉnh Điện Bi&ecirc;n để b&aacute;o c&aacute;o Ch&iacute;nh phủ về việc đầu tư n&acirc;ng cấp, cải tạo s&acirc;n bay Điện Bi&ecirc;n&rdquo;, Bộ trưởng n&oacute;i.</p>\r\n\r\n<p style=\"text-align:justify\">Về c&aacute;ch thức triển khai, Bộ trưởng Nguyễn Văn Thể khẳng định, Bộ GTVT thống nhất với Điện Bi&ecirc;n về việc x&atilde; hội h&oacute;a đầu tư đầu tư dự &aacute;n. &ldquo;Điện Bi&ecirc;n cần sớm l&agrave;m việc với nh&agrave; đầu tư, sau đ&oacute; c&oacute; văn bản b&aacute;o c&aacute;o cụ thể, chi tiết với Ch&iacute;nh phủ v&agrave; Bộ GTVT. Nh&agrave; đầu tư phải t&iacute;nh to&aacute;n kỹ lưỡng để đầu tư dự &aacute;n đảm bảo quy hoạch được duyệt, đường băng, s&acirc;n đỗ phải theo quy hoạch vị tr&iacute;, nh&agrave; ga phải đảm bảo t&iacute;nh ổn định l&acirc;u d&agrave;i, kinh ph&iacute; đầu tư sẽ được c&aacute;c cơ quan chuy&ecirc;n m&ocirc;n thẩm định, thẩm tra&rdquo;, Bộ trưởng n&oacute;i v&agrave; cho biết, về nguồn vốn, ưu ti&ecirc;n số một l&agrave; vốn tư nh&acirc;n đầu tư 100%, phương &aacute;n thứ hai l&agrave; đầu tư theo h&igrave;nh thức PPP, Nh&agrave; nước v&agrave; tư nh&acirc;n c&ugrave;ng l&agrave;m, cuối c&ugrave;ng mới t&iacute;nh đến việc đầu tư dự &aacute;n bằng nguồn ng&acirc;n s&aacute;ch Nh&agrave; nước.</p>\r\n\r\n<p style=\"text-align:justify\">Tại cuộc họp, Bộ trưởng Nguyễn Văn Thể cũng giao Vụ Kế hoạch &ndash; Đầu tư v&agrave; Tổng cục Đường bộ VN r&agrave; so&aacute;t, nghi&ecirc;n cứu để ưu ti&ecirc;n bố tr&iacute; nguồn vốn đầu tư cho dự &aacute;n cải tạo, n&acirc;ng cấp QL279B; dự &aacute;n QL279 từ Điện Bi&ecirc;n đến cửa khẩu T&acirc;y Trang, dự &aacute;n thảm tăng cường lắp b&ecirc; t&ocirc;ng nhựa mặt đường QL12,&hellip;</p>\r\n', '1', null, null, 'GT001', null, '2018-04-05 21:33:00', '2018-04-05 21:33:02', null);
INSERT INTO `news` VALUES ('9', 'Vụ tàu thép 67: Ngậm ngùi nhận đền bù \"bèo bọt\"!', null, null, 'Sau 27 cuộc họp, người dân đã tỏ ra quá mệt mỏi, còn phía doanh nghiệp cứ dây dưa các khoản đền bù. Đến nay, một số chủ tàu vỏ thép đành ngậm ngùi đồng ý nhận những khoản hỗ trợ \'bèo bọt\".', '<p style=\"text-align:justify\">Sau cuộc họp cuối c&ugrave;ng ở Sở NN-PTNT tỉnh B&igrave;nh Định v&agrave;o ng&agrave;y 2/4, hai đơn vị đ&oacute;ng t&agrave;u l&agrave; C&ocirc;ng ty TNHH Đại Nguy&ecirc;n Dương v&agrave; C&ocirc;ng ty TNHH MTV Nam Triệu đ&atilde; c&oacute; buổi l&agrave;m việc trực tiếp với ngư d&acirc;n ở c&aacute;c địa phương c&oacute; t&agrave;u vỏ th&eacute;p đ&oacute;ng mới theo Nghị định 67/CP bị hư hỏng.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">Ng&agrave;y 5/4, &ocirc;ng Nguyễn C&ocirc;ng B&igrave;nh, Ph&oacute; chi cục trưởng Chi cục Thủy sản tỉnh B&igrave;nh Định cho biết, 5 chủ&nbsp;t&agrave;u vỏ th&eacute;p&nbsp;hư hỏng ở tỉnh n&agrave;y đ&atilde; cơ bản thỏa thuận xong với C&ocirc;ng ty TNHH Đại Nguy&ecirc;n Dương (Nam Định) về mức hỗ trợ thiệt hại do t&agrave;u hư hỏng g&acirc;y ra.&nbsp;Tại c&aacute;c buổi l&agrave;m việc v&agrave;o ng&agrave;y 3 - 4/4 tại UBND hai huyện Ph&ugrave; Mỹ v&agrave; Ph&ugrave; C&aacute;t với sự chủ tr&igrave; của l&atilde;nh đạo UBND c&aacute;c huyện c&ugrave;ng c&aacute;c cơ quan, ban ng&agrave;nh li&ecirc;n quan, c&aacute;c ngư d&acirc;n đ&atilde; đồng &yacute; với một số khoản hỗ trợ. Theo đ&oacute;, C&ocirc;ng ty TNHH Đại Nguy&ecirc;n Dương&nbsp;thống nhất hỗ trợ tổng số tiền 881 triệu đồng cho cả 5 chủ t&agrave;u so với mức y&ecirc;u cầu l&agrave; 5,3 tỷ từ ph&iacute;a ngư d&acirc;n.</p>\r\n\r\n<p style=\"text-align:justify\">Ngư d&acirc;n Mai Văn Chương ở x&atilde; C&aacute;t Hải (huyện Ph&ugrave; C&aacute;t, B&igrave;nh Định) ng&aacute;n ngẩm: &ldquo;Qua thời gian d&agrave;i t&agrave;u nằm bờ t&ocirc;i thiệt hại đến hơn 1 tỷ đồng. Thế nhưng doanh nghiệp kh&ocirc;ng đồng &yacute; đền b&ugrave;, chỉ hỗ trợ 176 triệu đồng, l&yacute; do họ đưa ra l&agrave; v&igrave;&hellip; c&ocirc;ng ty thua lỗ, hết vốn, sắp ph&aacute; sản! Giờ như của đổ đi hốt lại, đ&agrave;nh nhận được đồng n&agrave;o th&igrave; được, chứ t&ocirc;i qu&aacute; mệt mỏi chuyện họp h&agrave;nh, đ&ocirc;i co, giờ m&agrave; khởi kiện th&igrave; tụi t&ocirc;i kh&ocirc;ng c&oacute; thời gian hầu t&ograve;a, c&ograve;n phải đi biển kiếm tiền b&ugrave; lỗ nữa!&rdquo;.</p>\r\n\r\n<p style=\"text-align:justify\">Ngo&agrave;i &ocirc;ng Chương, C&ocirc;ng ty Đại Nguy&ecirc;n Dương hỗ trợ cho 2 chủ t&agrave;u Nguyễn Văn Mạnh v&agrave; Nguyễn Văn L&yacute; mỗi người 136 triệu đồng, Trần Minh Vương 208 triệu đồng, V&otilde; Tu&acirc;n 225 triệu đồng.&nbsp;&quot;Họ hẹn trong 1 th&aacute;ng sẽ trả cho ch&uacute;ng t&ocirc;i bằng tiền mặt, nếu kh&ocirc;ng chủ t&agrave;u sẽ khởi kiện ra t&ograve;a&quot;, &ocirc;ng Trần Minh Vương cho hay.</p>\r\n\r\n<p style=\"text-align:justify\">Đến giờ n&agrave;y, vướng mắc c&ograve;n chưa giải quyết được giữa ngư d&acirc;n v&agrave; doanh nghiệp đ&oacute;ng t&agrave;u l&agrave; khoản l&atilde;i suất ng&acirc;n h&agrave;ng trong thời gian t&agrave;u nằm bờ sửa chữa. &ldquo;Vẫn c&ograve;n 1 điểm chưa thể đi đến thống nhất giữa c&aacute;c chủ t&agrave;u v&agrave; doanh nghiệp đ&oacute;ng t&agrave;u l&agrave; ph&acirc;n chia mức trả l&atilde;i ng&acirc;n h&agrave;ng trong thời gian sửa chữa t&agrave;u. Ch&uacute;ng t&ocirc;i tiếp tục lập bi&ecirc;n bản để b&aacute;o c&aacute;o UBND tỉnh chỉ đạo xử l&yacute;&rdquo;, &ocirc;ng L&ecirc; C&ocirc;ng B&igrave;nh, Chi cục ph&oacute; Chi cục Thủy sản B&igrave;nh Định cho biết.</p>\r\n\r\n<p style=\"text-align:justify\">Ngo&agrave;i C&ocirc;ng ty TNHH Đại Nguy&ecirc;n Dương, hiện C&ocirc;ng ty Nam Triệu (Bộ C&ocirc;ng an) cũng đang thỏa thuận bồi thường, hỗ trợ với 14 chủ t&agrave;u vỏ th&eacute;p hỏng do c&ocirc;ng ty n&agrave;y đ&oacute;ng.&nbsp;14 chủ t&agrave;u vỏ th&eacute;p n&agrave;y thống k&ecirc; thiệt hại của họ l&agrave; 27,8 tỉ đồng.&nbsp;Theo lịch, C&ocirc;ng ty TNHH một th&agrave;nh vi&ecirc;n Nam Triệu sẽ kết th&uacute;c việc thỏa thuận v&agrave;o ng&agrave;y 6/4 tới.</p>\r\n', '1', null, null, 'GT001', null, '2018-04-05 21:34:21', '2018-04-05 21:34:24', null);
INSERT INTO `news` VALUES ('10', 'Cần Thơ còn nhiều bến xe khách sai quy định của Bộ GTVT', null, null, 'Cần Thơ có 2 bến xe khách chưa công bố lại theo quy định của Bộ GTVT, nhiều bến xin gia hạn hoạt động.', 'Chiều 5/4, Sở GTVT TP Cần Thơ đã tổ chức họp lấy ý kiến về công tác triển khai thực hiện thu hút xã hội hóa đầu tư, khai thác bến xe khách và việc hoạt động của các bến xe khách chưa thực hiện xong việc công bố theo đúng quy định của Bộ GTVT. Theo báo cáo, trên địa bàn TP Cần Thơ hiện có 6 bến xe khách. Trong đó, có 2 bến đã được công bố theo quy định gồm: bến xe khách Trung tâm TP (Khu đô thị Nam Cần Thơ, QL1, phường Hưng Thạnh, quận Cái Răng, TP Cần Thơ) và bến xe khách TP (số 36 đường Nguyễn Văn Linh, quận Ninh Kiều, TP Cần Thơ). Có 2 bến chưa công bố lại theo quy định gồm: bến xe khách Ô Môn, bến xe khách huyện Cờ Đỏ, 2 bến xe tạm của quận Thốt Nốt và huyện Phong Điền. Ông Lê Tiến Dũng, Phó Giám đốc Sở GTVT TP cho biết, theo Thông tư 49/2012/TT-BGTVT “kể từ ngày 01/01/2015 các bến xe không được công bố lại do chưa đáp ứng được đầy đủ các quy định tại Quy chuẩn QCVN45:2012/BGTVT sẽ không được phép hoạt động”. Đến nay, mặc dù Sở GTVT TP thường xuyên chỉ đạo đến 2 bến xe khách nêu trên thực hiện công bố lại theo đúng quy định của Bộ GTVT nhưng vấn đề này vẫn chưa thực hiện được. Nguyên nhân là do bến xe Ô Môn với vị trí hiện tại đã không còn phù hợp, phải được di dời. Bên cạnh đó, bến này hoạt động chưa đúng theo quy hoạch chi tiết vị trí bến, bãi đỗ xe công cộng trên địa bàn TP đến năm 2030. Đối với bến xe huyện Cờ Đỏ là do Hợp tác xã Hiệp Đoàn hiện tại đã bị thu hồi 2/3 diện tích để quy hoạch dự án chỉnh trang đô thị và cải thiện môi trường sống tại thị trấn. Vì vậy, Hợp tác xã không đảm bảo diện tích bến xe loại 5. Thêm nữa, bến này cũng đang hoạt động chưa đúng quy hoạch. Riêng hai bến xe tạm là do đang trong thời gian chờ xây dựng bến mới theo vị trí đã được quy hoạch. Trình bày tại cuộc họp, đại diện các quận, huyện có bến xe nằm trong quy hoạch đều đồng loạt xin gia hạn cho các bến tiếp tục được hoạt động. Trong đó, đại diện quận Ô Môn, Thốt Nốt, Phong Điền xin được xem xét điều chỉnh quy hoạch bến xe ở vị trí khác.', '1', null, null, 'GT001', null, '2018-04-05 21:36:45', '2018-04-05 21:36:49', null);
INSERT INTO `news` VALUES ('11', 'Nghệ An: Tạm giữ lái xe buýt Đông Bắc khiến 2 người tử vong', null, null, 'Cơ quan CSĐT Công an huyện Nam Đàn (Nghệ An) đang tạm giữ hình sự tài xế Dũng người điều khiển xe buýt làm 5 người thương vong vào chiều 4/4.', '<p style=\"text-align:justify\">Trưa 5/4, trao đổi với PV B&aacute;o Giao th&ocirc;ng, Đại t&aacute; Cao Tiến Mai, Trưởng C&ocirc;ng an huyện Nam Đ&agrave;n (Nghệ An) cho biết: Cơ quan CSĐT đang tạm giữ h&igrave;nh sự t&agrave;i xế Nguyễn Hữu Dũng (SN 1985, tr&uacute; x&atilde; Diễn Hồng, huyện Diễn Ch&acirc;u, Nghệ An) để điều tra về h&agrave;nh vi&nbsp;&quot;Vi phạm quy định tham gia giao th&ocirc;ng đường bộ g&acirc;y hậu quả nghi&ecirc;m trọng&quot;.</p>\r\n\r\n<p style=\"text-align:justify\">T&agrave;i xế Dũng được x&aacute;c định l&agrave; người điều khiển xe bu&yacute;t Đ&ocirc;ng Bắc mang BKS: 37B -00095 l&agrave;m 5 người thương vong v&agrave;o trưa 4/4.</p>\r\n\r\n<p style=\"text-align:justify\">Theo Đại t&aacute; Mai, nguy&ecirc;n nh&acirc;n ban đầu được x&aacute;c định l&agrave; do xe bu&yacute;t đi lấn l&agrave;n đường, phần đường. Tuy nhi&ecirc;n, nguy&ecirc;n nh&acirc;n vụ tai nạn phải chờ kết quả cuối c&ugrave;ng từ cơ quan CSĐT.</p>\r\n\r\n<p style=\"text-align:justify\">Trước đ&oacute;, như B&aacute;o Giao th&ocirc;ng đ&atilde; đưa tin, khoảng 12h15 ng&agrave;y 4/4, chiếc xe bu&yacute;t Đ&ocirc;ng Bắc mang BKS: 37B -00095 do t&agrave;i xế Nguyễn Hữu Dũng (SN 1985, tr&uacute; x&atilde; Diễn Hồng, huyện Diễn Ch&acirc;u, Nghệ An) điều khiển theo hướng TP.Vinh - Thanh Chương. Khi đi đến địa phận x&atilde; Nam Th&aacute;i (huyện Nam Đ&agrave;n) bất ngờ va chạm với 2 chiếc xe m&aacute;y mang BKS: 37E1 - 382.39 v&agrave; 37H7 - 8466 do 2 nam thanh ni&ecirc;n điều khiển chở theo một nam thanh ni&ecirc;n kh&aacute;c chạy theo hướng ngược chiều. C&uacute; va chạm mạnh khiến 2 người điều khiển xe m&aacute;y tử vong tại chỗ, 3 người ngồi ph&iacute;a sau bị thương nặng được người d&acirc;n đưa đi cấp cứu trong t&igrave;nh trạng nguy kịch.</p>\r\n', '1', null, null, 'GT001', null, '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('12', 'Bế mạc Liên hoan ẩm thực Quốc tế Hội An 2018', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Sau 7 ngày diễn ra sôi nổi, Liên hoan ẩm thực Quốc tế Hội An lần thứ 3 - 2018 đã kết thúc chiều 17/3 mang đến nhiều tiếc cảm xúc trong lòng người dân và du khách.', 'Sau 7 ngày diễn ra sôi nổi, Liên hoan ẩm thực Quốc tế Hội An lần thứ 3 - 2018 đã kết thúc chiều 17/3 mang đến nhiều tiếc cảm xúc trong lòng người dân và du khách.', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('13', 'Trưng bày 50 cây và hàng vạn cành hoa anh đào Nhật Bản bên Hồ Gươm', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Sáng 19/3, tại Hà Nội, Sở Văn hóa và Thể thao Hà Nội đã tổ chức họp báo thông tin về các hoạt động Lễ hội giao lưu văn hóa Nhật Bản - thường quen thuộc với tên gọi lễ hội hoa Anh đào - nhân dịp kỷ niệm 45 năm thiết lập quan hệ ngoại giao Việt Nam - Nhật Bản. .', 'Sáng 19/3, tại Hà Nội, Sở Văn hóa và Thể thao Hà Nội đã tổ chức họp báo thông tin về các hoạt động Lễ hội giao lưu văn hóa Nhật Bản - thường quen thuộc với tên gọi lễ hội hoa Anh đào - nhân dịp kỷ niệm 45 năm thiết lập quan hệ ngoại giao Việt Nam - Nhật Bản. .', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('14', 'Ra mắt chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam “Ký ức Hội An” ra mắt công chúng tối ngày 18/3 tại Công viên Văn hóa chủ đề Ấn tượng Hội An bên bờ sông Hoài, Tp. Hội An, Quảng Nam. Chương trình là sự kiện đặc biệt ý nghĩa, góp phần đáng kể vào công tác bảo tồn văn hóa và phát triển…', 'Chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam “Ký ức Hội An” ra mắt công chúng tối ngày 18/3 tại Công viên Văn hóa chủ đề Ấn tượng Hội An bên bờ sông Hoài, Tp. Hội An, Quảng Nam. Chương trình là sự kiện đặc biệt ý nghĩa, góp phần đáng kể vào công tác bảo tồn văn hóa và phát triển…', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('15', 'Bế mạc Liên hoan ẩm thực Quốc tế Hội An 2018', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Sau 7 ngày diễn ra sôi nổi, Liên hoan ẩm thực Quốc tế Hội An lần thứ 3 - 2018 đã kết thúc chiều 17/3 mang đến nhiều tiếc cảm xúc trong lòng người dân và du khách.', 'Sau 7 ngày diễn ra sôi nổi, Liên hoan ẩm thực Quốc tế Hội An lần thứ 3 - 2018 đã kết thúc chiều 17/3 mang đến nhiều tiếc cảm xúc trong lòng người dân và du khách.', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('16', 'Trưng bày 50 cây và hàng vạn cành hoa anh đào Nhật Bản bên Hồ Gươm', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Sáng 19/3, tại Hà Nội, Sở Văn hóa và Thể thao Hà Nội đã tổ chức họp báo thông tin về các hoạt động Lễ hội giao lưu văn hóa Nhật Bản - thường quen thuộc với tên gọi lễ hội hoa Anh đào - nhân dịp kỷ niệm 45 năm thiết lập quan hệ ngoại giao Việt Nam - Nhật Bản. .', 'Sáng 19/3, tại Hà Nội, Sở Văn hóa và Thể thao Hà Nội đã tổ chức họp báo thông tin về các hoạt động Lễ hội giao lưu văn hóa Nhật Bản - thường quen thuộc với tên gọi lễ hội hoa Anh đào - nhân dịp kỷ niệm 45 năm thiết lập quan hệ ngoại giao Việt Nam - Nhật Bản. .', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('17', 'Ra mắt chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam “Ký ức Hội An” ra mắt công chúng tối ngày 18/3 tại Công viên Văn hóa chủ đề Ấn tượng Hội An bên bờ sông Hoài, Tp. Hội An, Quảng Nam. Chương trình là sự kiện đặc biệt ý nghĩa, góp phần đáng kể vào công tác bảo tồn văn hóa và phát triển…', 'Chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam “Ký ức Hội An” ra mắt công chúng tối ngày 18/3 tại Công viên Văn hóa chủ đề Ấn tượng Hội An bên bờ sông Hoài, Tp. Hội An, Quảng Nam. Chương trình là sự kiện đặc biệt ý nghĩa, góp phần đáng kể vào công tác bảo tồn văn hóa và phát triển…', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('18', 'Bế mạc Liên hoan ẩm thực Quốc tế Hội An 2018', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Sau 7 ngày diễn ra sôi nổi, Liên hoan ẩm thực Quốc tế Hội An lần thứ 3 - 2018 đã kết thúc chiều 17/3 mang đến nhiều tiếc cảm xúc trong lòng người dân và du khách.', 'Sau 7 ngày diễn ra sôi nổi, Liên hoan ẩm thực Quốc tế Hội An lần thứ 3 - 2018 đã kết thúc chiều 17/3 mang đến nhiều tiếc cảm xúc trong lòng người dân và du khách.', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('19', 'Trưng bày 50 cây và hàng vạn cành hoa anh đào Nhật Bản bên Hồ Gươm', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Sáng 19/3, tại Hà Nội, Sở Văn hóa và Thể thao Hà Nội đã tổ chức họp báo thông tin về các hoạt động Lễ hội giao lưu văn hóa Nhật Bản - thường quen thuộc với tên gọi lễ hội hoa Anh đào - nhân dịp kỷ niệm 45 năm thiết lập quan hệ ngoại giao Việt Nam - Nhật Bản. .', 'Sáng 19/3, tại Hà Nội, Sở Văn hóa và Thể thao Hà Nội đã tổ chức họp báo thông tin về các hoạt động Lễ hội giao lưu văn hóa Nhật Bản - thường quen thuộc với tên gọi lễ hội hoa Anh đào - nhân dịp kỷ niệm 45 năm thiết lập quan hệ ngoại giao Việt Nam - Nhật Bản. .', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);
INSERT INTO `news` VALUES ('20', 'Ra mắt chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam', 'http://vita2.local/images/news/img-1.jpg', 'http://vita2.local/images/news/img-1.jpg', 'Chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam “Ký ức Hội An” ra mắt công chúng tối ngày 18/3 tại Công viên Văn hóa chủ đề Ấn tượng Hội An bên bờ sông Hoài, Tp. Hội An, Quảng Nam. Chương trình là sự kiện đặc biệt ý nghĩa, góp phần đáng kể vào công tác bảo tồn văn hóa và phát triển…', 'Chương trình nghệ thuật thực cảnh quy mô nhất Việt Nam “Ký ức Hội An” ra mắt công chúng tối ngày 18/3 tại Công viên Văn hóa chủ đề Ấn tượng Hội An bên bờ sông Hoài, Tp. Hội An, Quảng Nam. Chương trình là sự kiện đặc biệt ý nghĩa, góp phần đáng kể vào công tác bảo tồn văn hóa và phát triển…', '1', null, null, null, '1', '2018-04-05 21:38:21', '2018-04-05 21:38:24', null);

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `function_type` int(11) DEFAULT NULL COMMENT '1: thẩm định 2: phê duyệt',
  `action_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notes
-- ----------------------------
INSERT INTO `notes` VALUES ('1', '1', '43', 'Từ chối thẩm định lần 1', '2018-03-13 10:58:13', '2018-03-13 10:58:13', '1', '0');
INSERT INTO `notes` VALUES ('2', '1', '43', 'Yêu cầu bổ sung lần 1', '2018-03-13 11:27:33', '2018-03-13 11:27:33', '1', '0');
INSERT INTO `notes` VALUES ('3', '1', '43', 'từ chối lần 2', '2018-03-13 11:29:14', '2018-03-13 11:29:14', '1', '0');
INSERT INTO `notes` VALUES ('4', '2', '43', 'Yêu cầu bổ sung 1', '2018-03-13 11:36:33', '2018-03-13 11:36:33', '1', '0');
INSERT INTO `notes` VALUES ('5', '5', '43', 'Từ chối lần 1', '2018-03-13 11:36:47', '2018-03-13 11:36:47', '1', '0');
INSERT INTO `notes` VALUES ('6', '7', '43', 'Từ chối lần 1', '2018-03-13 11:37:15', '2018-03-13 11:37:15', '1', '0');
INSERT INTO `notes` VALUES ('7', '2', '43', 'từ chối lần 1', '2018-03-13 11:42:23', '2018-03-13 11:42:23', '1', '0');
INSERT INTO `notes` VALUES ('8', '1', '44', 'Từ chối lần 1', '2018-03-13 13:51:04', '2018-03-13 13:51:04', '2', '0');
INSERT INTO `notes` VALUES ('9', '1', '44', 'Yêu cầu bổ sung phê duyệt 1', '2018-03-13 13:53:00', '2018-03-13 13:53:00', '2', '0');
INSERT INTO `notes` VALUES ('10', '1', '44', 'Từ chối lần 2', '2018-03-13 13:57:28', '2018-03-13 13:57:28', '2', '0');
INSERT INTO `notes` VALUES ('11', '1', '44', 'Yêu cầu bổ sung lần 3', '2018-03-13 13:57:45', '2018-03-13 13:57:45', '2', '0');
INSERT INTO `notes` VALUES ('12', '11', '43', 'yêu cầu bổ sung 1', '2018-03-13 14:02:09', '2018-03-13 14:02:09', '1', '0');
INSERT INTO `notes` VALUES ('13', '11', '43', 'Từ chối lần 1', '2018-03-13 16:10:32', '2018-03-13 16:10:32', '1', '0');
INSERT INTO `notes` VALUES ('14', '11', '43', 'Yêu cầu bổ sung lần 2', '2018-03-13 16:10:53', '2018-03-13 16:10:53', '1', '0');
INSERT INTO `notes` VALUES ('15', '11', '43', 'Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2TừEnd', '2018-03-13 16:13:43', '2018-03-13 16:13:43', '1', '0');
INSERT INTO `notes` VALUES ('16', '11', '43', 'Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ su', '2018-03-13 16:14:00', '2018-03-13 16:14:00', '1', '0');
INSERT INTO `notes` VALUES ('17', '11', '43', 'Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổEnđ', '2018-03-13 16:14:27', '2018-03-13 16:14:27', '1', '0');
INSERT INTO `notes` VALUES ('18', '11', '43', 'Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổEnđ', '2018-03-13 16:37:37', '2018-03-13 16:37:37', '1', '0');
INSERT INTO `notes` VALUES ('19', '11', '43', 'Hồ sơ đã được thẩm định', '2018-03-13 16:37:43', '2018-03-13 16:37:43', '1', '0');
INSERT INTO `notes` VALUES ('20', '16', '17', 'alaklfkljfkalf', '2018-03-13 22:31:11', '2018-03-13 22:31:11', '1', '2');
INSERT INTO `notes` VALUES ('21', '17', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 09:54:47', '2018-03-14 09:54:47', '1', '3');
INSERT INTO `notes` VALUES ('22', '17', '44', 'thiếu hồ sơ', '2018-03-14 10:02:21', '2018-03-14 10:02:21', '2', '8');
INSERT INTO `notes` VALUES ('23', '17', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 10:04:01', '2018-03-14 10:04:01', '2', '4');
INSERT INTO `notes` VALUES ('24', '22', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 10:51:56', '2018-03-14 10:51:56', '1', '3');
INSERT INTO `notes` VALUES ('25', '22', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 10:55:27', '2018-03-14 10:55:27', '2', '4');
INSERT INTO `notes` VALUES ('26', '34', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 11:29:26', '2018-03-14 11:29:26', '1', '3');
INSERT INTO `notes` VALUES ('27', '35', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 11:29:38', '2018-03-14 11:29:38', '1', '3');
INSERT INTO `notes` VALUES ('28', '36', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 11:29:45', '2018-03-14 11:29:45', '1', '3');
INSERT INTO `notes` VALUES ('29', '34', '44', 'd', '2018-03-14 11:30:26', '2018-03-14 11:30:26', '2', '8');
INSERT INTO `notes` VALUES ('30', '35', '44', 'c', '2018-03-14 11:30:34', '2018-03-14 11:30:34', '2', '8');
INSERT INTO `notes` VALUES ('31', '36', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 11:31:04', '2018-03-14 11:31:04', '2', '4');
INSERT INTO `notes` VALUES ('32', '34', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 11:31:24', '2018-03-14 11:31:24', '2', '4');
INSERT INTO `notes` VALUES ('33', '35', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 11:31:40', '2018-03-14 11:31:40', '2', '4');
INSERT INTO `notes` VALUES ('34', '31', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:32:26', '2018-03-14 11:32:26', '1', '3');
INSERT INTO `notes` VALUES ('35', '29', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:33:07', '2018-03-14 11:33:07', '1', '3');
INSERT INTO `notes` VALUES ('36', '28', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:33:45', '2018-03-14 11:33:45', '1', '3');
INSERT INTO `notes` VALUES ('37', '27', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:34:16', '2018-03-14 11:34:16', '1', '3');
INSERT INTO `notes` VALUES ('38', '25', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:34:57', '2018-03-14 11:34:57', '1', '3');
INSERT INTO `notes` VALUES ('39', '24', '59', 'dfsdfsdfsdf', '2018-03-14 11:43:24', '2018-03-14 11:43:24', '1', '7');
INSERT INTO `notes` VALUES ('40', '33', '1', 'Từ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thEnd', '2018-03-14 14:25:30', '2018-03-14 14:25:30', '1', '7');
INSERT INTO `notes` VALUES ('41', '33', '43', 'Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cEnđ', '2018-03-11 13:40:26', '2018-03-14 14:21:04', '1', '2');
INSERT INTO `notes` VALUES ('42', '32', '43', 'Từ chối thẩm định', '2018-03-14 13:41:42', '2018-03-14 13:41:42', '1', '7');
INSERT INTO `notes` VALUES ('43', '32', '43', 'Yêu cầu bổ sung lần 2', '2018-03-14 13:43:34', '2018-03-14 13:43:34', '1', '2');
INSERT INTO `notes` VALUES ('44', '30', '43', 'Từ chối thẩm định lần 1', '2018-03-14 13:44:31', '2018-03-14 13:44:31', '1', '7');
INSERT INTO `notes` VALUES ('45', '23', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 13:59:11', '2018-03-14 13:59:11', '1', '3');
INSERT INTO `notes` VALUES ('46', '18', '59', 'dfdsfsdfsdfsd', '2018-03-14 14:00:03', '2018-03-14 14:00:03', '1', '2');
INSERT INTO `notes` VALUES ('47', '33', '43', 'Từ chối lần 2', '2018-03-09 14:06:44', '2018-03-14 14:22:10', '1', '7');
INSERT INTO `notes` VALUES ('48', '32', '43', 'Từ chối lần 2', '2018-03-14 14:06:59', '2018-03-14 14:06:59', '1', '7');
INSERT INTO `notes` VALUES ('49', '30', '43', 'từ chối lần 2', '2018-03-14 14:07:16', '2018-03-14 14:07:16', '1', '7');
INSERT INTO `notes` VALUES ('50', '26', '43', 'Từ chối lần 2', '2018-03-14 14:07:28', '2018-03-14 14:07:28', '1', '7');
INSERT INTO `notes` VALUES ('51', '19', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 14:20:39', '2018-03-14 14:20:39', '1', '3');
INSERT INTO `notes` VALUES ('52', '33', '43', 'Yêu cầu bổ sung 2', '2018-03-12 14:22:44', '2018-03-14 14:23:14', '1', '2');
INSERT INTO `notes` VALUES ('53', '33', '43', 'Hồ sơ đã được thẩm định', '2018-03-13 14:23:46', '2018-03-14 14:34:26', '1', '3');
INSERT INTO `notes` VALUES ('54', '32', '43', 'Yêu cầu bổ sung 3', '2018-03-14 14:34:11', '2018-03-14 14:34:11', '1', '2');
INSERT INTO `notes` VALUES ('55', '24', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 14:35:14', '2018-03-14 14:35:14', '1', '3');
INSERT INTO `notes` VALUES ('56', '18', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 14:35:31', '2018-03-14 14:35:31', '1', '3');
INSERT INTO `notes` VALUES ('57', '33', '44', 'Từ chối phê duyệt lần 1', '2018-03-07 14:38:32', '2018-03-14 14:39:49', '2', '8');
INSERT INTO `notes` VALUES ('58', '33', '44', 'Yêu cầu bổ sung phê duyệt lần 1', '2018-03-08 14:44:48', '2018-03-14 14:45:28', '2', '2');
INSERT INTO `notes` VALUES ('59', '33', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 14:45:44', '2018-03-14 14:45:44', '2', '4');
INSERT INTO `notes` VALUES ('60', '19', '60', 'dsfsdgfsdgsdfsdfsdfsdfsdf', '2018-03-14 15:27:18', '2018-03-14 15:27:18', '2', '2');
INSERT INTO `notes` VALUES ('61', '19', '60', 'lần 2', '2018-03-14 15:27:50', '2018-03-14 15:27:50', '2', '2');
INSERT INTO `notes` VALUES ('62', '19', '60', 'lần 3', '2018-03-14 15:28:38', '2018-03-14 15:28:38', '2', '8');
INSERT INTO `notes` VALUES ('63', '19', '60', 'lần 4', '2018-03-14 15:28:52', '2018-03-14 15:28:52', '2', '2');
INSERT INTO `notes` VALUES ('64', '19', '60', 'Hồ sơ đã được phê duyệt', '2018-03-14 15:29:07', '2018-03-14 15:29:07', '2', '4');
INSERT INTO `notes` VALUES ('65', '11', '44', 'Đồng ý phê duyệt', '2018-03-14 15:47:01', '2018-03-14 15:47:01', '2', '4');
INSERT INTO `notes` VALUES ('66', '31', '44', 'Đồng ý phê duyệt', '2018-03-14 15:47:14', '2018-03-14 15:47:14', '2', '4');
INSERT INTO `notes` VALUES ('67', '29', '44', 'Đồng ý phê duyệt', '2018-03-14 15:48:05', '2018-03-14 15:48:05', '2', '4');
INSERT INTO `notes` VALUES ('68', '28', '44', 'Đồng ý phê duyệt', '2018-03-14 15:48:18', '2018-03-14 15:48:18', '2', '4');
INSERT INTO `notes` VALUES ('69', '27', '44', 'Đồng ý phê duyệt', '2018-03-14 15:48:29', '2018-03-14 15:48:29', '2', '4');
INSERT INTO `notes` VALUES ('70', '25', '44', 'Đồng ý phê duyệt', '2018-03-14 15:48:41', '2018-03-14 15:48:41', '2', '4');
INSERT INTO `notes` VALUES ('71', '24', '44', 'Đồng ý phê duyệt', '2018-03-14 15:49:05', '2018-03-14 15:49:05', '2', '4');
INSERT INTO `notes` VALUES ('72', '23', '44', 'Đồng ý phê duyệt', '2018-03-14 15:49:18', '2018-03-14 15:49:18', '2', '4');
INSERT INTO `notes` VALUES ('73', '18', '44', 'Đồng ý phê duyệt', '2018-03-14 15:49:28', '2018-03-14 15:49:28', '2', '4');
INSERT INTO `notes` VALUES ('74', '1', '17', 'Đồng ý phê duyệt', '2018-03-14 17:26:59', '2018-03-14 17:26:59', '2', '4');
INSERT INTO `notes` VALUES ('75', '37', '43', 'Đồng ý thẩm định', '2018-03-14 17:30:52', '2018-03-14 17:30:52', '1', '3');
INSERT INTO `notes` VALUES ('76', '38', '59', 'Đồng ý thẩm định', '2018-03-14 17:32:08', '2018-03-14 17:32:08', '1', '3');
INSERT INTO `notes` VALUES ('77', '37', '44', 'Đồng ý phê duyệt', '2018-03-14 17:32:12', '2018-03-14 17:32:12', '2', '4');
INSERT INTO `notes` VALUES ('78', '21', '59', 'Đồng ý thẩm định', '2018-03-14 17:32:18', '2018-03-14 17:32:18', '1', '3');
INSERT INTO `notes` VALUES ('79', '21', '44', 'sdfasdfa', '2018-03-15 10:00:18', '2018-03-15 10:00:18', '2', '2');
INSERT INTO `notes` VALUES ('80', '21', '44', 'sdfasdfasf', '2018-03-15 10:00:31', '2018-03-15 10:00:31', '2', '8');
INSERT INTO `notes` VALUES ('81', '21', '44', 'sdfasdfasf\r\nsdfasf', '2018-03-15 10:00:39', '2018-03-15 10:00:39', '2', '2');
INSERT INTO `notes` VALUES ('82', '21', '44', 'ssd', '2018-03-15 10:00:45', '2018-03-15 10:00:45', '2', '8');
INSERT INTO `notes` VALUES ('83', '21', '44', 'yeu cau bo sung', '2018-03-15 10:35:48', '2018-03-15 10:35:48', '2', '2');
INSERT INTO `notes` VALUES ('84', '21', '44', 'yeu cau bo sung', '2018-03-15 10:37:09', '2018-03-15 10:37:09', '2', '8');
INSERT INTO `notes` VALUES ('85', '40', '43', 'yêu cầu bổ sung hồ sơ', '2018-03-15 11:42:56', '2018-03-15 11:42:56', '1', '2');
INSERT INTO `notes` VALUES ('86', '49', '17', 'jajaja', '2018-03-08 13:54:40', '2018-03-15 14:32:17', '1', '2');
INSERT INTO `notes` VALUES ('87', '49', '43', 'Từ chối 1', '2018-03-09 13:58:49', '2018-03-15 14:32:25', '1', '7');
INSERT INTO `notes` VALUES ('88', '43', '59', 'bổ sung 1', '2018-03-15 14:07:21', '2018-03-15 14:07:21', '1', '2');
INSERT INTO `notes` VALUES ('89', '41', '10', 'yeu cau bo sung lan 1', '2018-03-15 14:22:50', '2018-03-15 14:22:50', '1', '2');
INSERT INTO `notes` VALUES ('90', '41', '10', 'tu choi lan 1', '2018-03-15 14:25:55', '2018-03-15 14:25:55', '1', '7');
INSERT INTO `notes` VALUES ('91', '49', '43', 'Yêu cầu bổ sung 2', '2018-03-10 14:29:19', '2018-03-15 14:32:32', '1', '2');
INSERT INTO `notes` VALUES ('92', '43', '59', 'Đồng ý thẩm định', '2018-03-15 14:31:15', '2018-03-15 14:31:15', '1', '3');
INSERT INTO `notes` VALUES ('93', '44', '59', 'Đồng ý thẩm định', '2018-03-15 14:31:34', '2018-03-15 14:31:34', '1', '3');
INSERT INTO `notes` VALUES ('94', '46', '59', 'Đồng ý thẩm định', '2018-03-15 14:31:44', '2018-03-15 14:31:44', '1', '3');
INSERT INTO `notes` VALUES ('95', '53', '17', 'aaaaaaa', '2018-03-15 14:34:57', '2018-03-15 14:34:57', '1', '2');
INSERT INTO `notes` VALUES ('96', '49', '43', 'Từ chối 2', '2018-03-11 14:35:44', '2018-03-15 14:38:33', '1', '7');
INSERT INTO `notes` VALUES ('97', '54', '17', 'ssssss', '2018-03-15 14:38:49', '2018-03-15 14:38:49', '1', '7');
INSERT INTO `notes` VALUES ('98', '52', '43', 'Đồng ý thẩm định', '2018-03-15 14:42:04', '2018-03-15 14:42:04', '1', '3');
INSERT INTO `notes` VALUES ('99', '41', '43', 'Đồng ý thẩm định', '2018-03-15 14:42:44', '2018-03-15 14:42:44', '1', '3');
INSERT INTO `notes` VALUES ('100', '48', '43', 'Từ chối lần 1', '2018-03-15 15:06:35', '2018-03-15 15:06:35', '1', '7');
INSERT INTO `notes` VALUES ('101', '48', '43', 'Yêu cầu bổ xung lần 1', '2018-03-15 15:55:10', '2018-03-15 15:55:10', '1', '2');
INSERT INTO `notes` VALUES ('102', '45', '43', 'từ chối lần 1', '2018-03-15 15:57:02', '2018-03-15 15:57:02', '1', '7');
INSERT INTO `notes` VALUES ('103', '52', '17', 'Yêu cầu bổ sung hồ sơ từ Hà', '2018-03-15 16:14:40', '2018-03-15 16:14:40', '2', '2');
INSERT INTO `notes` VALUES ('104', '42', '43', 'từ chối thẩm định lần 1', '2018-03-15 16:20:50', '2018-03-15 16:20:50', '1', '7');
INSERT INTO `notes` VALUES ('105', '49', '43', 'Đồng ý thẩm định', '2018-03-02 16:22:33', '2018-03-15 16:58:02', '1', '3');
INSERT INTO `notes` VALUES ('106', '48', '43', 'Đồng ý thẩm định', '2018-03-15 16:27:01', '2018-03-15 16:27:01', '1', '3');
INSERT INTO `notes` VALUES ('107', '45', '43', 'Đồng ý thẩm định', '2018-03-15 16:27:18', '2018-03-15 16:27:18', '1', '3');
INSERT INTO `notes` VALUES ('108', '42', '43', 'Đồng ý thẩm định', '2018-03-15 16:27:31', '2018-03-15 16:27:31', '1', '3');
INSERT INTO `notes` VALUES ('109', '32', '59', 'Đồng ý thẩm định', '2018-03-15 16:31:01', '2018-03-15 16:31:01', '1', '3');
INSERT INTO `notes` VALUES ('110', '30', '59', 'từ chối', '2018-03-15 16:32:44', '2018-03-15 16:32:44', '1', '7');
INSERT INTO `notes` VALUES ('111', '30', '43', 'Đồng ý thẩm định', '2018-03-15 16:32:45', '2018-03-15 16:32:45', '1', '3');
INSERT INTO `notes` VALUES ('112', '26', '59', 'Đồng ý thẩm định', '2018-03-15 16:33:43', '2018-03-15 16:33:43', '1', '3');
INSERT INTO `notes` VALUES ('113', '26', '43', 'Từ chối lần 2', '2018-03-15 16:33:43', '2018-03-15 16:33:43', '1', '2');
INSERT INTO `notes` VALUES ('114', '26', '43', 'Từ chối lần 2', '2018-03-15 16:36:07', '2018-03-15 16:36:07', '1', '2');
INSERT INTO `notes` VALUES ('115', '26', '43', 'bổ sung lần 3', '2018-03-15 16:36:47', '2018-03-15 16:36:47', '1', '2');
INSERT INTO `notes` VALUES ('116', '57', '59', 'Đồng ý thẩm định', '2018-03-15 16:37:57', '2018-03-15 16:37:57', '1', '3');
INSERT INTO `notes` VALUES ('117', '55', '59', 'Đồng ý thẩm định', '2018-03-15 16:40:59', '2018-03-15 16:40:59', '1', '3');
INSERT INTO `notes` VALUES ('118', '55', '60', 'dgrtfrtfr', '2018-03-15 16:44:08', '2018-03-15 16:44:08', '2', '8');
INSERT INTO `notes` VALUES ('119', '55', '60', '87897987', '2018-03-15 16:44:29', '2018-03-15 16:44:29', '2', '2');
INSERT INTO `notes` VALUES ('120', '57', '60', 'Đồng ý phê duyệt', '2018-03-15 16:45:45', '2018-03-15 16:45:45', '2', '4');
INSERT INTO `notes` VALUES ('121', '49', '44', 'Từ chối thẩm định 1', '2018-03-01 17:07:32', '2018-03-15 17:09:25', '2', '8');
INSERT INTO `notes` VALUES ('122', '49', '44', 'yêu cầu bổ sung lần 1', '2018-03-16 09:28:25', '2018-03-16 09:28:25', '2', '22');
INSERT INTO `notes` VALUES ('123', '59', '43', '', '2018-03-16 09:31:12', '2018-03-16 09:31:12', '1', '3');
INSERT INTO `notes` VALUES ('124', '49', '44', 'từ chối lần 2', '2018-03-16 09:31:20', '2018-03-16 09:31:20', '2', '8');
INSERT INTO `notes` VALUES ('125', '49', '44', 'yêu cầu bổ sung lần 2', '2018-03-16 09:31:46', '2018-03-16 09:31:46', '2', '22');
INSERT INTO `notes` VALUES ('126', '49', '44', 'từ chối lần 3', '2018-03-16 09:32:10', '2018-03-16 09:32:10', '2', '8');
INSERT INTO `notes` VALUES ('127', '49', '44', 'bổ sung thông tin lần 3', '2018-03-16 09:34:03', '2018-03-16 09:34:03', '2', '22');
INSERT INTO `notes` VALUES ('128', '42', '44', 'yêu cầu bổ sung', '2018-03-16 09:35:08', '2018-03-16 09:35:08', '2', '22');
INSERT INTO `notes` VALUES ('129', '59', '44', '', '2018-03-16 09:35:54', '2018-03-16 09:35:54', '2', '4');
INSERT INTO `notes` VALUES ('130', '49', '44', '', '2018-03-16 09:39:13', '2018-03-16 09:39:13', '2', '4');
INSERT INTO `notes` VALUES ('131', '42', '44', '', '2018-03-16 09:39:46', '2018-03-16 09:39:46', '2', '4');
INSERT INTO `notes` VALUES ('132', '48', '44', '', '2018-03-16 09:41:41', '2018-03-16 09:41:41', '2', '4');
INSERT INTO `notes` VALUES ('133', '32', '44', '', '2018-03-16 09:41:55', '2018-03-16 09:41:55', '2', '4');
INSERT INTO `notes` VALUES ('134', '45', '44', '', '2018-03-16 09:43:42', '2018-03-16 09:43:42', '2', '4');
INSERT INTO `notes` VALUES ('135', '60', '59', 'bổ sung lần 1', '2018-03-16 09:44:35', '2018-03-16 09:44:35', '1', '2');
INSERT INTO `notes` VALUES ('136', '30', '44', 'vsdfs', '2018-03-16 10:10:16', '2018-03-16 10:10:16', '2', '22');
INSERT INTO `notes` VALUES ('137', '51', '43', 'xcvzxv', '2018-03-16 10:19:13', '2018-03-16 10:19:13', '1', '7');
INSERT INTO `notes` VALUES ('138', '54', '43', 'dfasdfsd', '2018-03-16 10:19:37', '2018-03-16 10:19:37', '1', '7');
INSERT INTO `notes` VALUES ('139', '54', '43', 'dfasdfsd\r\nsdfsdf', '2018-03-16 10:20:11', '2018-03-16 10:20:11', '1', '2');
INSERT INTO `notes` VALUES ('140', '54', '43', '', '2018-03-16 10:20:32', '2018-03-16 10:20:32', '1', '3');
INSERT INTO `notes` VALUES ('141', '46', '60', '', '2018-03-16 10:47:36', '2018-03-16 10:47:36', '2', '4');
INSERT INTO `notes` VALUES ('142', '44', '60', '', '2018-03-16 10:47:55', '2018-03-16 10:47:55', '2', '4');
INSERT INTO `notes` VALUES ('143', '43', '60', '', '2018-03-16 10:48:31', '2018-03-16 10:48:31', '2', '4');
INSERT INTO `notes` VALUES ('144', '54', '60', '', '2018-03-16 10:52:49', '2018-03-16 10:52:49', '2', '4');
INSERT INTO `notes` VALUES ('145', '38', '60', '', '2018-03-16 10:53:14', '2018-03-16 10:53:14', '2', '4');
INSERT INTO `notes` VALUES ('146', '61', '43', '!@@$$^*(*)()\r\nboor sung', '2018-03-16 11:03:18', '2018-03-16 11:03:18', '1', '2');
INSERT INTO `notes` VALUES ('147', '62', '17', 'Yeeu caau bo sung abce x', '2018-03-16 11:16:55', '2018-03-16 11:16:55', '1', '2');
INSERT INTO `notes` VALUES ('148', '62', '17', 'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer to', '2018-03-16 11:19:07', '2018-03-16 11:19:07', '1', '7');
INSERT INTO `notes` VALUES ('149', '53', '17', 'Yêu cầu bổ sung, check mail', '2018-03-16 11:29:57', '2018-03-16 11:29:57', '1', '2');
INSERT INTO `notes` VALUES ('150', '71', '43', 'ádaas', '2018-03-16 14:00:18', '2018-03-16 14:00:18', '1', '7');
INSERT INTO `notes` VALUES ('151', '65', '43', '', '2018-03-16 14:21:07', '2018-03-16 14:21:07', '1', '3');
INSERT INTO `notes` VALUES ('152', '65', '44', '', '2018-03-16 14:21:27', '2018-03-16 14:21:27', '2', '4');
INSERT INTO `notes` VALUES ('153', '71', '43', '', '2018-03-16 15:00:09', '2018-03-16 15:00:09', '1', '3');
INSERT INTO `notes` VALUES ('154', '71', '44', '', '2018-03-16 15:00:40', '2018-03-16 15:00:40', '2', '4');
INSERT INTO `notes` VALUES ('155', '77', '43', 'từ chối', '2018-03-16 15:20:05', '2018-03-16 15:20:05', '1', '7');
INSERT INTO `notes` VALUES ('156', '70', '43', 'từ chối', '2018-03-16 15:30:01', '2018-03-16 15:30:01', '1', '7');
INSERT INTO `notes` VALUES ('157', '67', '43', 'từ chối', '2018-03-16 15:30:20', '2018-03-16 15:30:20', '1', '7');
INSERT INTO `notes` VALUES ('158', '66', '43', 'từ', '2018-03-16 15:30:36', '2018-03-16 15:30:36', '1', '7');
INSERT INTO `notes` VALUES ('159', '70', '43', 'yêu cầu bổ sung', '2018-03-16 15:33:08', '2018-03-16 15:33:08', '1', '2');
INSERT INTO `notes` VALUES ('160', '67', '43', 'yêu cầu bổ sung', '2018-03-16 15:33:27', '2018-03-16 15:33:27', '1', '2');
INSERT INTO `notes` VALUES ('161', '66', '43', 'yêu cầu bổ sung', '2018-03-16 15:33:42', '2018-03-16 15:33:42', '1', '2');
INSERT INTO `notes` VALUES ('162', '16', '24', 'asssssssssssss', '2018-03-16 15:38:19', '2018-03-16 15:38:19', '1', '2');
INSERT INTO `notes` VALUES ('163', '79', '59', 'từ chối lần 1', '2018-03-16 16:20:45', '2018-03-16 16:20:45', '1', '7');
INSERT INTO `notes` VALUES ('164', '79', '59', 'bổ sung lần 1', '2018-03-16 16:22:09', '2018-03-16 16:22:09', '1', '2');
INSERT INTO `notes` VALUES ('165', '79', '59', 'bổ sung lần 2', '2018-03-16 16:27:13', '2018-03-16 16:27:13', '1', '2');
INSERT INTO `notes` VALUES ('166', '79', '59', 'bổ sung lần 3', '2018-03-16 16:49:08', '2018-03-16 16:49:08', '1', '2');
INSERT INTO `notes` VALUES ('167', '79', '59', 'bổ sung lần 4', '2018-03-16 16:58:12', '2018-03-16 16:58:12', '1', '2');
INSERT INTO `notes` VALUES ('168', '79', '59', 'bổ sung lần 5', '2018-03-16 17:06:48', '2018-03-16 17:06:48', '1', '2');
INSERT INTO `notes` VALUES ('169', '80', '59', '', '2018-03-16 17:38:24', '2018-03-16 17:38:24', '1', '3');
INSERT INTO `notes` VALUES ('170', '80', '60', 'bổ sung phê duyệt 1', '2018-03-16 17:38:54', '2018-03-16 17:38:54', '2', '22');
INSERT INTO `notes` VALUES ('171', '82', '43', 'bs', '2018-03-19 14:59:55', '2018-03-19 14:59:55', '1', '2');
INSERT INTO `notes` VALUES ('172', '79', '43', 'gfdgd', '2018-03-19 15:00:16', '2018-03-19 15:00:16', '1', '7');
INSERT INTO `notes` VALUES ('173', '83', '43', 'thiếu thông tin cá nhân', '2018-03-19 15:44:25', '2018-03-19 15:44:25', '1', '2');
INSERT INTO `notes` VALUES ('174', '83', '43', 'thiếu thông tin cá nhân 3', '2018-03-19 15:58:44', '2018-03-19 15:58:44', '1', '2');
INSERT INTO `notes` VALUES ('175', '83', '43', '', '2018-03-19 16:00:32', '2018-03-19 16:00:32', '1', '3');
INSERT INTO `notes` VALUES ('176', '83', '44', 'bổ sung thêm ghi chú', '2018-03-19 16:09:25', '2018-03-19 16:09:25', '2', '22');
INSERT INTO `notes` VALUES ('177', '83', '44', '', '2018-03-19 16:27:28', '2018-03-19 16:27:28', '2', '4');

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES ('1', 'club', 'Câu lạc bộ tiếng nhật', null, '2018-03-23 11:43:14', '2018-03-23 11:43:17', 'CLB01');
INSERT INTO `options` VALUES ('2', 'club', 'Câu lạc bộ tiếng anh', null, '2018-03-23 11:43:34', '2018-03-23 11:43:38', 'CLB02');
INSERT INTO `options` VALUES ('4', 'position', 'Giám đốc', null, '2018-03-27 22:13:57', '2018-03-27 22:13:59', 'cdddd');
INSERT INTO `options` VALUES ('5', 'position', 'Ủy viên', null, '2018-03-27 22:14:12', '2018-03-27 22:14:15', 'ddg33');
INSERT INTO `options` VALUES ('6', 'branches', 'Chi Hội', null, '2018-04-01 21:41:33', '2018-04-01 21:41:33', 'BRANCHES01');
INSERT INTO `options` VALUES ('7', 'branches', 'CLB Thuộc Chi Hội', null, '2018-04-01 21:41:33', '2018-04-01 21:41:33', 'BRANCHES02');
INSERT INTO `options` VALUES ('8', 'branches', 'CLB Thuộc Hội', null, '2018-04-01 21:41:33', '2018-04-01 21:41:33', 'BRANCHES03');
INSERT INTO `options` VALUES ('9', 'club', 'Câu lạc bộ tiếng hàn', '0000-00-00 00:00:00', '2018-03-23 11:43:14', '2018-03-23 11:43:17', 'CLB03');
INSERT INTO `options` VALUES ('10', 'club', 'Câu lạc bộ tiếng mỹ', '0000-00-00 00:00:00', '2018-03-23 11:43:34', '2018-03-23 11:43:38', 'CLB04');
INSERT INTO `options` VALUES ('11', 'club', 'Câu lạc bộ tiếng nga', '0000-00-00 00:00:00', '2018-03-23 11:43:14', '2018-03-23 11:43:17', 'CLB05');
INSERT INTO `options` VALUES ('12', 'club', 'Câu lạc bộ tiếng pháp', '0000-00-00 00:00:00', '2018-03-23 11:43:34', '2018-03-23 11:43:38', 'CLB06');
INSERT INTO `options` VALUES ('13', 'infomation', 'Giao thông', null, '2018-04-04 22:53:53', '2018-04-04 22:53:56', 'GT001');
INSERT INTO `options` VALUES ('14', 'infomation', 'Phong tục tập quán', null, '2018-04-04 22:54:31', '2018-04-04 22:54:35', 'PT001');
INSERT INTO `options` VALUES ('15', 'infomation', 'Lễ hội', '0000-00-00 00:00:00', '2018-04-04 22:54:31', '2018-04-04 22:54:35', 'LH001');
INSERT INTO `options` VALUES ('16', 'infomation', 'Điểm đến', '0000-00-00 00:00:00', '2018-04-04 22:54:31', '2018-04-04 22:54:35', 'DD001');
INSERT INTO `options` VALUES ('17', 'infomation', 'Trang phục', '0000-00-00 00:00:00', '2018-04-04 22:54:31', '2018-04-04 22:54:35', 'TP001');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for typeguides
-- ----------------------------
DROP TABLE IF EXISTS `typeguides`;
CREATE TABLE `typeguides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of typeguides
-- ----------------------------
INSERT INTO `typeguides` VALUES ('1', 'Du lịch sinh thái mai sửa', '2017-12-01 00:08:38', '2017-12-01 00:08:38', '0');
INSERT INTO `typeguides` VALUES ('2', 'Du lịch văn hóa- lịch sử', '2017-12-01 00:08:38', '2017-12-01 00:08:38', '1');
INSERT INTO `typeguides` VALUES ('3', 'Du lịch tâm linh', '2017-12-01 21:55:54', '2017-12-01 22:54:56', '1');
INSERT INTO `typeguides` VALUES ('4', 'Du lịch thể thao-mạo hiểm', '2017-12-01 21:53:53', '2017-12-01 21:53:53', '1');
INSERT INTO `typeguides` VALUES ('5', 'Du lịch MICE, sự kiện', '2017-12-01 21:55:54', '2017-12-01 22:54:56', '1');
INSERT INTO `typeguides` VALUES ('6', 'Du lịch nghiên cứu-học tập', '2017-12-01 21:53:53', '2017-12-01 21:53:53', '1');
INSERT INTO `typeguides` VALUES ('7', 'Du lịch nghỉ dưỡng-chữa bệnh', '2017-12-01 21:55:54', '2017-12-01 22:54:56', '1');
INSERT INTO `typeguides` VALUES ('8', 'Loại hình khác', '2017-12-01 21:53:53', '2017-12-01 21:53:53', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `province_type` tinyint(4) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `memberId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Duong Dai Dao', 'daiduong47', '3', '1', 'daiduong47@gmail.com', '$2y$10$/CJzjXRB.sEQojyVfi2AOej1eqtGNzOes4NipVqBW24dKCGGIJYLe', 'PBwKaFT7MttvUoINSPoTyxsHQNnWVVunJu9OVxrZ9iDPhNb9d5UI6LoJTrDd', '2017-12-01 02:22:22', '2017-12-01 02:22:22', '1', null, '1');
INSERT INTO `users` VALUES ('2', 'hattt', 'hattt', '2', '1', 'hattt@eprtech.com', '$2y$10$Oe0H487tJeD1qieoDT2GOuelC.qeKhSkgGar9Gr8124GOOxE8Fgby', 'HKav0XhzFvC8YbC5OCpkDt1pEhC8YkPjfEcUzkYu1xKhNPGNO92pNLWfhPLh', '2017-12-01 08:53:00', '2017-12-01 08:53:00', '1', null, '1');
INSERT INTO `users` VALUES ('3', 'Ha Tran Thi Thanh', 'hattt123', '1', '1', 'hattt1@eprtech.com', '$2y$10$Lj9VIoxwvEyfq/fhkoA.zufAx97lcbzflYSLBve9Gck8fRlDB8BHW', '8cjQIYFR2BqImR0uBmqtvDdrPKda6uqaOveg6mZHq3xvwJvP1ecpQLgQYWbI', '2017-12-12 16:37:46', '2017-12-12 16:37:46', '3', null, '3');
INSERT INTO `users` VALUES ('4', 'admin', 'administrator', '3', '1', 'admin@gmail.com', '$2y$10$f4mlE8POn6gP31kJN3B1rexIE/8jDHtj0L.XNASYisvS.8/2l12fy', '7XJNB7hlBRXFzo5M2RneiKiBu8ls5Q55MWbeYOpPyO5IxkYMPCBboHldoAyB', '2017-12-12 16:39:40', '2017-12-12 16:39:40', '1', null, null);
INSERT INTO `users` VALUES ('5', 'Chuyên viên', 'daiduong472', '6', '1', 'daiduong472@gmail.com', '$2y$10$K3Ao1Gk9Kyj7igIIgP94I.YaPDEXAkePe6fSAoO6USr3f5jmSGfSC', 'MzIvyAdbDzMBYf9PDdRgpnQwZdUkThON3c108yqhoDDPIuLgF874QpcBOMzv', '2017-12-12 23:51:37', '2017-12-12 23:51:37', '1', null, null);
INSERT INTO `users` VALUES ('6', 'Dương Văn Phượng', 'phuongdv', '6', '1', 'duongphuongit@gmail.com', '$2y$10$FkLr.d382OSDEuuCAmw16e5RcB99q0PGhB0BnrsNUoO4qoSwFK4Iq', 'MZEliNFD2LiMZPtSDBqJS91UNiaKjCtpE8XEY6AOJt2QnX8GyHdGPxrMMjtE', '2017-12-13 01:34:58', '2017-12-13 01:34:58', '1', null, null);
INSERT INTO `users` VALUES ('7', 'tran thi thanh ha', 'hattt2', '73', '1', 'hattt+1@eprtech.com', '$2y$10$i7vbLHQ0kJzq4cwyThULPueRUjpXcN4tV0IFi9lTWEPL7ApulaqqS', 'EDAqjlxlMtOwCoj9nOuAlTI0YTH8NZX8IhWF1d9swbPJiMIOpcEQLEoxZJm9', '2017-12-15 03:05:51', '2017-12-15 03:05:51', '1', null, null);
INSERT INTO `users` VALUES ('8', 'bui van dung', 'trusohanoi', '7', '1', 'buivandungvn@gmail.com', '$2y$10$M4Q0e1I9r2qRe3XugiZn/epHBbNvufMJGznFqBu0bYetmiDgltwTu', null, '2017-12-15 06:33:35', '2017-12-15 06:33:35', '1', null, null);
INSERT INTO `users` VALUES ('9', 'nguyễn thị thanh thủy', 'thanhthuy123', '6', '1', 'nguyenthanhthuy21490@gmail.com', '$2y$10$1o6f77KMpbhZit6Gxpmw9OQd70UP9KyI6ngyjvnGMmoebcGEhXZi.', null, '2017-12-15 09:50:47', '2017-12-15 09:50:47', '1', null, null);
INSERT INTO `users` VALUES ('10', 'Chuyên viên Hội', 'cvh', '61', '1', 'chuyenvienhoi@hhdulichvn.vn', '$2y$10$Gf7MLSNEaFhHH62pcMB5su3cAiDT/7UtoNGJwbx.Z5ls/zq6yUdSy', 'YFrgGryX82BMzF6J4nD3H8QOXtd1CIPlnbPwhT7ra5svmeSgm4i5MWepsoZX', '2017-12-20 15:06:48', '2017-12-20 15:06:48', '1', null, null);
INSERT INTO `users` VALUES ('11', 'Lãnh đạo Hội', 'ldh', '8', '1', 'lanhdaohoi@hhdulichvn.vn', '$2y$10$Oe0H487tJeD1qieoDT2GOuelC.qeKhSkgGar9Gr8124GOOxE8Fgby', 'qimkEZWkf48tUeI2MwQuZLrurZK7RfTR0mLUHS8J7jg2ZgrdZ1ulBC8t0F8n', '2017-12-20 15:28:49', '2017-12-20 15:28:49', '1', null, null);
INSERT INTO `users` VALUES ('12', 'Chuyên Viên Đà Nẵng', 'chuyenviendn', '62', '1', 'chuyenviendn@hhdulich.vn', '$2y$10$3Zc5GeJwQII6Kmzi3CyMH.ygEeERP1K4VDaM/A4vntdZhFSwid0l.', 'hXplMiCrdguYdQN9rddimZZUPC1H4QDXjlMWLznuFdskgKndN0o2pzsAp6tt', '2017-12-21 02:35:08', '2017-12-21 02:35:08', '2', null, null);
INSERT INTO `users` VALUES ('13', 'Lãnh đạo VPĐD Đà Nẵng', 'lanhdaodn', '72', '1', 'lanhdaodn@hhdulich.vn', '$2y$10$voMAlwUJK.YSblHkQza2F.NhP42Y43DtgrrWVydjPz8UZMWwHqIOS', 'T1nHnI8ez7BcjelrIfjCBcc41Ik9Euw11xpbRUZJaPQ7KBnENw7hjV2E00sh', '2017-12-21 02:38:58', '2017-12-21 02:38:58', '2', null, null);
INSERT INTO `users` VALUES ('14', 'Chuyên viên Hồ Chí Minh', 'chuyenvienhcm', '63', '1', 'chuyenvienhcm@hhdulich.vn', '$2y$10$6Sr52wNLwfpDu.jc6XlVDuGcX2rFn2Zbz2S/UH.ehAafgDS7Wt0Nm', 'BI5zjOKqcRp8F4u8LdavkZ6ML1ynMqyjigyvOcAsIVY8x0l0xjgrFPZz4T1n', '2017-12-21 02:40:47', '2017-12-21 02:40:47', '3', null, null);
INSERT INTO `users` VALUES ('15', 'Lãnh đạo VPĐD Hồ Chí Minh', 'lanhdaohcm', '73', '1', 'lanhdaohcm@hhdulich.vn', '$2y$10$yrKS1PKZD4zP6fGdO77WL.ussx2J2MG.Z.uhQN6JyTB4GHPjqm8bS', 'IHk9zpU5mpiaVZfTGeVRZWGQamtGHzuechZIQjRSzGUxUTwjZYqnY55LotvW', '2017-12-21 02:41:37', '2017-12-21 02:41:37', '3', null, null);
INSERT INTO `users` VALUES ('16', 'Chuyên viên hội đà nẵng 2', 'chuyenviendn2', '9', '1', 'chuyenviendn2@gmail.com', '$2y$10$61BfafM7HyrrtCA6i1LWyeG5Grr/YlfeTYPX7ePdOADHJ1BLUsJpG', 'FGThjmQXrSkyn40q9toakhAq1HbChjKzjXZD8eWm7JPJql3eGyeg4X8nLIlV', '2017-12-21 02:59:26', '2017-12-21 02:59:26', '2', null, null);
INSERT INTO `users` VALUES ('17', 'Kế toán hội', 'ktoanhoi', '8', '1', 'kethoanhoi@hhdulich.vn', '$2y$10$eDh1dAzfeMg/rRO1YcSZJenMyURsBvQPkphEHsj22Uz3GF5CMjXs6', 'zi5QXqT9cr8auaeY1dQhnjKnVUXWsKmeYaBdBeKVnVxqtr9kzbH0CQpbtvrb', '2017-12-23 14:23:05', '2018-01-17 16:14:10', '1', null, null);
INSERT INTO `users` VALUES ('18', 'Nguyễn Xuân Bình', 'binhnx2000', '1', '1', 'binhnx2000@yahoo.com', '$2y$10$u.pnTRVbn3Gkh/25c0qvuOw2mmCWjfWNSzSykCU7JNjUi/R9lKZ3a', null, '2018-01-12 02:32:35', '2018-01-12 02:32:35', '1', null, null);
INSERT INTO `users` VALUES ('19', 'Đỗ Tuấn Việt', 'amiviet2012', '1', '1', 'amiviet2012@gmail.com', '$2y$10$442T12bSoDHn/j7PwlOr7eXzZKih0aPcy3ExHgBzSXP49MuopbDb2', null, '2018-01-12 02:33:06', '2018-01-12 02:33:06', '1', null, null);
INSERT INTO `users` VALUES ('20', 'Chuyên viên Hà Nội', 'cvhn', '61', '1', 'cvhn@gmail.com', '$2y$10$4cekl0IFLGWFGYjlVQI.nu06bSSmOQgaK.assOtiFTXyQqtm3wd1G', 'qimkEZWkf48tUeI2MwQuZLrurZK7RfTR0mLUHS8J7jg2ZgrdZ1ulBC8t0F8n', '2018-01-17 16:13:05', '2018-01-17 16:13:05', '1', null, null);
INSERT INTO `users` VALUES ('21', 'Lãnh đạo hà nội', 'ldhn', '71', '1', 'ldhn@gmail.com', '$2y$10$BwJL3cvO56fMHcd0MGkD0OIEagY56m89QzFletzQV1k1tVyAu3Wia', 'KDfutHTJNgOU3SBso1dJFZ7HHUtXps2kTstK9JOOJXOvJEJ1LXMloGrA2KGk', '2018-01-17 16:13:36', '2018-01-17 16:13:36', '1', null, null);
INSERT INTO `users` VALUES ('22', 'Chuyên viên Hà Nội', 'HoangLong', '9', '1', 'hoanglonghna@gmail.com', '$2y$10$nwef2nMGZKgil6AVkKC7H.s/nGZLFFfwJFmfhpDD4aBi44Eo0dxMC', 'Ftuh3iWwnGs605hCGh3yLMZRbzBt8izFcZr09LCFkpBzOZrazmJaJ7vcEi9j', '2018-01-17 16:13:05', '2018-01-17 16:13:05', '1', null, null);
INSERT INTO `users` VALUES ('23', 'Nguyễn Xuân Bình', 'binhnx2000', '1', '1', '1binhnx2000@yahoo.com', '$2y$10$CKgLekARuwwi713ivAjE3.pcP7f9dv8kiWbP5aW2pumCvz/gQkCk2', null, '2018-02-03 14:54:15', '2018-02-03 14:54:15', '1', null, null);
INSERT INTO `users` VALUES ('24', 'Administrator', 'admin', '1', '1', 'admin@hhdulich.vn', '$2y$10$Oe0H487tJeD1qieoDT2GOuelC.qeKhSkgGar9Gr8124GOOxE8Fgby', 'kfYrVCK86pmAgB1dFAaT5gmuQrzLlIiKp8pFYiV5qPPZYohKI7HmhPe06ued', '2017-12-22 17:23:05', '2017-12-22 17:23:05', '1', null, '1');
INSERT INTO `users` VALUES ('25', 'Họ tên cvtd HN', 'Tên đăng nhập cvtd HN', '61', '1', 'cvtdhn@gmail.com.vn', '$2y$10$IIFOcmbc2VWmLfuS6dyNnuOIcK.PVSOpuhG4DIz2APR2Pe2jtrgYK', 'AjJuOSsiOVBLjxVqEvKX20oUa9OAOUijzJ6ICKSFIWmn7ljudEt2UHFaxN7M', '2018-02-09 07:45:45', '2018-02-09 07:45:45', '1', null, null);
INSERT INTO `users` VALUES ('26', 'Họ tên cvtd HCM', 'Tên đăng nhập cvtd HCM', '63', '1', 'cvtdhcm@gmail.com', '$2y$10$wYwR5RA/rieqRRgQnv684Oxynb7/Ym6y.pGmM2slWSY73T3Zi77c2', 'ziMxWFv4dCvjcjF8KMML9lHF4JJouXTeJblblsNBqvrq33JKaoBiyLsLXVdS', '2018-02-09 08:08:12', '2018-02-09 08:08:12', '3', null, null);
INSERT INTO `users` VALUES ('27', 'họ tên ld hcm', 'tên đăng nhập ldhcm', '73', '1', 'ldhcm@gmail.com', '$2y$10$bDo1srWkIIfUWQuscMVUGeJK4GLzkHBMaq/VCQEjIikM5eEn1aTe.', 'xA3xuuKk0oguaXDMmdzt6bFUZa3UD1hRHbPmEHvjlqZ9DL77qtmlI2elDs1F', '2018-02-09 09:34:47', '2018-02-09 10:11:25', '3', null, null);
INSERT INTO `users` VALUES ('28', 'nguyễn thị an', 'annthn@hhdulich.vn', '1', '1', 'annt@hhdulich.vn', '$2y$10$VQ2RtjT1xwrT8AvvgKxKJeknpLVD0beGjwQVx4xkRCoGSnjyn/KZ6', null, '2018-02-09 09:37:48', '2018-02-09 09:37:48', '1', null, null);
INSERT INTO `users` VALUES ('29', 'họ tên kế toán', 'tên đăng nhập kế toán', '2', '1', 'kt@gmail.com', '$2y$10$AIzzZfTOhWlnQYOZgIfgm.cchV3Pzo.UpTY9j008KE3iOrDA2AiCy', 'yZDPrYTu6bqEvizuO5DecynXBmvg4GXOlishH1rGIHQUolE9NNjXz65golp3', '2018-02-09 09:38:19', '2018-02-09 09:38:19', '1', null, null);
INSERT INTO `users` VALUES ('30', 'Ngoan Cố', 'tranha08430', '1', '1', '65_tranha084@gmail.com', '$2y$10$XLgBGzRHq0hBd4OoWhvVkOQ.hfgyf/cLLISRCVcRVQNUi01O764jq', null, '2018-02-10 21:21:50', '2018-02-10 21:21:50', '2', null, null);
INSERT INTO `users` VALUES ('31', 'NGHIÊM TRUNG VIỆT', 'nghiemtrung.viet31', '1', '1', '2_nghiemtrung.viet@gmail.com', '$2y$10$.3o1ir7/ficittpRnrW4bu7cDL3DPlv0uUHJFccR5B56yboLvaFfO', null, '2018-02-10 21:40:56', '2018-02-10 21:40:56', '1', null, '11');
INSERT INTO `users` VALUES ('32', 'Ngoan Cố', 'tranha08418', '1', '1', '98_tranha084@gmail.com', '$2y$10$R5cVGQKQONTwC3TydjSThOBPvhpZH29QEOxxnxzxMtRtrVRtSO30a', null, '2018-02-10 22:00:29', '2018-02-10 22:00:29', '2', null, '114');
INSERT INTO `users` VALUES ('33', 'Nguyễn Quang Trung', 'quangtrung.dl081214', '1', '1', '13_quangtrung.dl0812@gmail.com', '$2y$10$yAmwZ2J1r3Yt6ADsYb/oGONwqcPbreoY.KGGVhNN0mKFVQvKUasyO', null, '2018-02-11 01:52:23', '2018-02-11 01:52:23', '1', null, '1');
INSERT INTO `users` VALUES ('34', 'Nguyễn Quang Trung', 'quangtrung.dl081278', '1', '1', '63_quangtrung.dl0812@gmail.com', '$2y$10$7fEfNf9mAVQr.E0y6w8aQO08F2Wq5.IypvElhRgAtOYmJ36go8FAe', null, '2018-02-11 02:27:34', '2018-02-11 02:27:34', '1', null, '1');
INSERT INTO `users` VALUES ('35', 'Tran Xuan Toi', 'xuantoi9099', '1', '1', '42_xuantoi90@gmail.com', '$2y$10$7tlCjhlW5.R6XTEEl7jFxeGYCRt4im4SjAZ6WA3wMDNG5N7CVQT7S', null, '2018-02-11 03:23:15', '2018-02-11 03:23:15', '1', null, '115');
INSERT INTO `users` VALUES ('36', 'Nguyễn Quang Trung', 'quangtrung.dl081296', '1', '1', '30_quangtrung.dl0812@gmail.com', '$2y$10$UWg3BXFTEHcEcrGsQmIoHeHR6ZG/DDXTs5LVtB4umDMWZYrZYNYhe', null, '2018-02-11 04:40:31', '2018-02-11 04:40:31', '1', null, '1');
INSERT INTO `users` VALUES ('37', 'Nguyễn Quang Trung', 'quangtrung.dl081240', '1', '1', '21_quangtrung.dl0812@gmail.com', '$2y$10$BuoEsfkRNIIPjN.iMb7cXuxe0LDfClER/IkA9dh0y3u0hZaEN4S9S', null, '2018-02-11 04:42:42', '2018-02-11 04:42:42', '1', null, '1');
INSERT INTO `users` VALUES ('38', 'Nguyễn Quang Trung', 'quangtrung.dl08121', '1', '1', '60_quangtrung.dl0812@gmail.com', '$2y$10$etVqW4GmXwkPuMhxBkwaJO08g1UIZyaCk3BHVeQuq6zsEzLXsrHHK', null, '2018-02-11 04:43:36', '2018-02-11 04:43:36', '1', null, '1');
INSERT INTO `users` VALUES ('39', 'Nguyễn Đức Hệ', 'dexi19752', '1', '1', '40_dexi1975@yahoo.com.vn', '$2y$10$XfImDwc77/hlhWZSBATIyelCllGcnYryLM4dadyznAzt1ZiTXlwu.', null, '2018-02-11 04:52:07', '2018-02-11 04:52:07', '1', null, '3');
INSERT INTO `users` VALUES ('40', 'Đỗ Tuấn Việt', 'amiviet201254', '1', '1', '33_amiviet2012@gmail.com', '$2y$10$L0TiaJkQT6rB9lfEuZlj0u7kiKkEXM9tPMDVZbYEZ6WU.AlxA1RPe', null, '2018-02-11 08:22:48', '2018-02-11 08:22:48', '1', null, '10');
INSERT INTO `users` VALUES ('41', 'Nguyễn Quang Trung', 'quangtrung.dl081292', '1', '1', '26_quangtrung.dl0812@gmail.com', '$2y$10$K3iaWb4Rsia5ZKuQF.RDbOFHdKzbAHPEnFeapZzsh4MH5pEnLdL4.', null, '2018-02-11 10:04:45', '2018-02-11 10:04:45', '1', null, '1');
INSERT INTO `users` VALUES ('42', 'Tran Xuan Toi', 'xuantoi9062', '1', '1', '58_xuantoi90@gmail.com', '$2y$10$6fZWkr5edut51NHCEtld7unYLQWyuHKZmv82kB2MzOFcz6i4i50L2', null, '2018-02-12 17:54:07', '2018-02-12 17:54:07', '1', null, '115');
INSERT INTO `users` VALUES ('43', 'Chuyên viên thẩm định hà nội', 'cvtdhn', '61', '1', 'cvtdhn@viettel.com.vn', '$2y$10$nAbCJky.ePJh9WncGXypaOVG3/3yUgErQu/z2O3iLBf2eBYrB/9Vq', 'R0xvhFwxxyJfp59k4ndwE0nfC1hGgmRqzuX6cD2WmRvBBkwXnCqFR62rckMK', '2018-02-22 07:14:04', '2018-02-22 07:14:04', '1', null, null);
INSERT INTO `users` VALUES ('44', 'Lãnh đạo thẩm định hà nội', 'ldtdhn', '71', '1', 'ldtdhn@viettel.com.vn', '$2y$10$WGKYR/vsnZx4ahku3xl8y.a862ngkTh3ehRD/GVSzQ4Iu8Xo.RJp2', '1XfVjVGyXOi9pYr6djUYPF8D3NnAWWSRQ1UxMqv00i7M05qdSAjhLKASOiwS', '2018-02-22 07:43:09', '2018-02-22 07:43:09', '1', null, null);
INSERT INTO `users` VALUES ('45', 'Nguyễn Đức Hệ', 'dexi19758', '1', '1', '80_dexi1975@yahoo.com.vn', '$2y$10$lnEThrSbR.4e1EF/vPTrTumaGiioAtKEsEJrIaRCFL0zC8p7NTQce', null, '2018-02-22 20:20:52', '2018-02-22 20:20:52', '1', null, '3');
INSERT INTO `users` VALUES ('46', 'Cấp thẻ', 'ct', '9', '1', 'ct@viettel.com.vn', '$2y$10$9MZIrqVF0FRhkRQ86YzX7uMgeqBGxH7QDfygIARQBCM.GyAttqa/u', 'PBJHgCwCPyZ8PxYsiu4WnkM63r6zngIPZDw2tnAbgsZxOzgncxtia6UWoc0Y', '2018-02-23 02:08:07', '2018-02-23 02:08:07', '1', null, null);
INSERT INTO `users` VALUES ('47', 'Lãnh đạo hội', 'ldhhn', '8', '1', 'ldhoi@viettel.com.vn', '$2y$10$AKIfbqsDFUC62hY5P/d3fOo/bmapYTMLZ9Z9O87aXHfCeYeqDV3aG', 'sjy2IRprNulZwBgPEsDuMhU23YUEze2e4EBXjgTBgK2iKT2wuDYT2KFArVrd', '2018-02-23 02:46:11', '2018-02-23 02:46:37', '1', null, null);
INSERT INTO `users` VALUES ('48', 'kế toán', 'kt', '2', '1', 'kt@viettel.com.vn', '$2y$10$7MKt8lAAtcWlCn2JQeixp.R8ZZrihXyJKHCOzKk11hChuucpSg1AO', '070vRvtLJZ5wLsCMcGdwv0qc2YN46xaCvQah0o9U5DppfeMtH1loWnzgb88m', '2018-02-23 08:35:30', '2018-02-23 08:35:30', '1', null, null);
INSERT INTO `users` VALUES ('49', 'Tran Xuan Toi', 'xuantoi9067', '1', '1', '50_xuantoi90@gmail.com', '$2y$10$OwgMGtE4XiK9lErqCwfvserU66DEt24437ifILUukMb4QBvbvguQi', null, '2018-02-23 09:54:50', '2018-02-23 09:54:50', '1', null, '115');
INSERT INTO `users` VALUES ('50', 'Tran Xuan Toi', 'xuantoi9083', '1', '1', '46_xuantoi90@gmail.com', '$2y$10$I14.b33/LyP577EyQ4o4VOUXNC78VSFQ2Ehrge6qqqCPXqJNDpfg2', null, '2018-02-23 09:58:05', '2018-02-23 09:58:05', '1', null, '115');
INSERT INTO `users` VALUES ('51', 'Tran Xuan Toi', 'tranha08439', '1', '1', '5_tranha084@gmail.com', '$2y$10$Ip0kbLT7wJF3vpU1L2VsteED7JNdU21zDUzbsxEIhQ6PHjTaCRQSa', null, '2018-02-23 09:59:23', '2018-02-23 09:59:23', '1', null, '115');
INSERT INTO `users` VALUES ('52', 'Chuyên viên thẩm định hồ chí minh', 'cvtdhcm', '63', '1', 'cvtdhcm@viettel.com.vn', '$2y$10$6/bEOe/ZivKzbFdSjZJfCerf1XlcTfHd7UOmKdprMqNsHbMCK98Vy', 'zoI0hAWky2lz84BEphvEPnLctZUeYHyxOFdawQ3j63utzSWpNk5l1gQ3wOYi', '2018-02-23 10:08:04', '2018-02-23 10:08:04', '3', null, null);
INSERT INTO `users` VALUES ('53', 'Lãnh đạo thẩm định hcm', 'ldtdhcm', '73', '1', 'ldtdhcm@viettel.com.vn', '$2y$10$1QRu1fYyywGEyqz2TinZueYl025/Ygg3LSY4FrJw8yOLGvLAVJBuW', 'FsvFV1gbrTmiIvq9ALm6JKpviwxkKLkXwNbg9CdwqmZjxcm7sskFLNulKnup', '2018-02-23 10:09:12', '2018-02-23 10:09:12', '3', null, null);
INSERT INTO `users` VALUES ('54', 'Chuyên viên thẩm định đà nẵng', 'cvtddn', '62', '1', 'cvtddn@viettel.com.vn', '$2y$10$Hx8c8YsjbCbcErQGB2K7t.Cba2O4DwZxV.kW1gHGl0HvwTBaatRWa', 'n4MJAR6sxMfYrHkxLWY8lzj0ftkZQpVhhVUXsBNPdeTfujhWz8muRHbIPyoh', '2018-02-23 10:10:21', '2018-02-23 10:12:02', '2', null, null);
INSERT INTO `users` VALUES ('55', 'Lãnh đạo thẩm định đà nẵng', 'ldtddn', '72', '1', 'ldtddn@viettel.com.vn', '$2y$10$GTyc/UWJeHcgUm6a5jTRte.Ta4Sz7fFMYFNm.3l4D4BwdJ4GeSWGe', 'jPyx8Ex3fB7Y7VS7uk6yXw6I73fqQ135kGel6cwjB0G4oKhbXx9JeglNX6zF', '2018-02-23 10:11:28', '2018-02-23 10:11:28', '2', null, null);
INSERT INTO `users` VALUES ('56', 'Tran Xuan Toi', 'tranha08415', '1', '1', '73_tranha084@gmail.com', '$2y$10$TPXLK4dSxtLwfik2i5drAOgDK9IoLxAGnukGSOpjTxMlPoFx.bIdu', null, '2018-02-23 10:52:45', '2018-02-23 10:52:45', '1', null, '115');
INSERT INTO `users` VALUES ('57', 'ádfasdfa', 'maint18100', '1', '1', '2_maint18@viettel.com.vn', '$2y$10$GOI0fK/Xct/xWRA2vJvWKO5c/7qpMhVa6i.neA7b7WK6UJ/SQkT02', null, '2018-02-25 23:00:28', '2018-02-25 23:00:28', '1', null, '10');
INSERT INTO `users` VALUES ('58', 'dsfasdfa', 'maint1818', '1', '1', '74_maint18@viettel.com.vn', '$2y$10$H3sVcIGAg/gk3MpUGU2WrOBEdzXraBxZiLpcflfp0rlDBNgMmyq2m', null, '2018-02-26 15:12:54', '2018-02-26 15:12:54', '1', null, '7');
INSERT INTO `users` VALUES ('59', 'Thầm định hồ sơ', 'thamdinhhn', '61', '1', 'abc@gmail.com', '$2y$10$Q4exwiERs9Brf6YMDraO9u3e3MiAVoXbvWVSKFGxsFU.2hpvnRDQW', 'H23cXbqRmybihWSNMgiKYY1KqMsP4v1PVkSppWwO80pQa12IjfVhbmbPRqK2', '2018-02-26 15:36:58', '2018-02-26 15:36:58', '1', null, null);
INSERT INTO `users` VALUES ('60', 'Phê duyệt hồ sơ', 'pheduyethn', '71', '1', 'pheduyet@gmail.com', '$2y$10$NpTW738lE19sXmej0KN9S.UeOBfkz9h//2unXgSKiFq84WU8jaCFi', 'OhHGem2Y1mb5GrzECKQgkN7Ds1sMtn1tmLsEUBn7mONabQ4s8GPVsjORAE1a', '2018-02-26 15:41:06', '2018-02-26 15:41:06', '1', null, null);
INSERT INTO `users` VALUES ('61', 'Kế toán', 'ketoanhn', '2', '1', 'ketoan@gmail.com', '$2y$10$SULSmiejljDb24WbLhAMB.xyYVGliUi5mZTsVJnwJ4G8M5cCidmnm', '08ifDyfhI3DxhuRPGQbFeSvyS9QTZB5t57cc91gsI8BJSK54Ijhw5o7CRzbF', '2018-02-26 15:42:36', '2018-02-26 15:42:36', '1', null, null);
INSERT INTO `users` VALUES ('62', 'Chuyên viên cấp thẻ hội viên', 'capthehn', '9', '1', 'capthe@gmail.com', '$2y$10$vqPaupnYWPhZEzVV/vRiD.1OxkRfxoR4uRVEnpS0Sig49I8BGKSki', 'cPB5UHINzXjYikzalyFpILnxg4c4mzMM9HWT3GyHoMFg5z3Cy6eNPrNALzXd', '2018-02-26 15:43:38', '2018-02-26 15:43:38', '1', null, null);
INSERT INTO `users` VALUES ('63', 'ha3', 'tranha08440', '1', '1', '90_tranha084@gmail.com', '$2y$10$wqUyu5KopB55z2/7jNl0jO4kByesqn8UM1enckKPUsN8jxE0cbMYK', null, '2018-02-26 15:53:51', '2018-02-26 15:53:51', '1', null, '1');
INSERT INTO `users` VALUES ('64', 'ha3', 'tranha08498', '1', '1', '81_tranha084@gmail.com', '$2y$10$CFiaLzWaDsY6MZW8Lmtun.6RbOxOMXhIxKzzWvqijA3lZkJui9Wz2', null, '2018-02-26 15:55:38', '2018-02-26 15:55:38', '1', null, '1');
INSERT INTO `users` VALUES ('65', 'ádfasf', 'maint1874', '1', '1', '5_maint18@viettel.com.vn', '$2y$10$sn6twuxeLYfo1RQLCmXxXejqlemJ6g0xBobLDrIn3Vao7HPOFkb4m', null, '2018-02-26 16:00:25', '2018-02-26 16:00:25', '1', null, '11');
INSERT INTO `users` VALUES ('66', 'ha4', 'tranha08445', '1', '1', '28_tranha084@gmail.com', '$2y$10$AqtwkceO8mA6lZL/lQCy..lz3Y4OQypfVP7FhrHwW.NJCHwTFYCd.', null, '2018-02-26 16:16:14', '2018-02-26 16:16:14', '1', null, '4');
INSERT INTO `users` VALUES ('68', 'ha1', 'tranha084116', '1', '1', '27_tranha08411@gmail.com', '$2y$10$3nWnRmR6IL4BnUmyzXXFHez3a3QDKbMOSDa8MJV3K3cZK3x6K15Ky', null, '2018-02-26 16:50:00', '2018-02-26 16:50:00', '1', null, '3');
INSERT INTO `users` VALUES ('69', 'ha1', 'tranha0841121', '1', '1', '93_tranha08411@gmail.com', '$2y$10$md14hSlaDSIXkWX2tXVQwu4WUfod5OVSRqR3X5k8iPrJEQYQrzm.i', null, '2018-02-26 17:00:28', '2018-02-26 17:00:28', '1', null, '3');
INSERT INTO `users` VALUES ('70', 'test02_2602', 'abc79', '1', '1', '96_abc@gmail.com', '$2y$10$v9hVGf8gGU54LAzlxBVb0eLC.vTWQYDtjoXuWDr8RghSIY5jI7IHu', null, '2018-02-26 17:08:19', '2018-02-26 17:08:19', '1', null, '18');
INSERT INTO `users` VALUES ('71', 'sdfasf', 'maint1856', '1', '1', '13_maint18@viettel.com.vn', '$2y$10$75ZouqbFLBOdrMC3ZpTANe2opq2deVE2NojSqtiPwnzIw9208XVcG', null, '2018-02-27 17:50:31', '2018-02-27 17:50:31', '1', null, '9');
INSERT INTO `users` VALUES ('72', 'test04_2701', 'abc19', '1', '1', '11_abc@gmail.com', '$2y$10$PZdOsDUrU/px0Buh3rP9OePiEjn7eVutGVYRwW1iqPvsi4oUY3WIu', null, '2018-03-01 10:06:07', '2018-03-01 10:06:07', '1', null, '35');
INSERT INTO `users` VALUES ('73', 'test01_2802', 'abc40', '1', '1', '89_abc@gmail.com', '$2y$10$XouVt.a8cIRe9Cdgw/X7UORFiRMBomeDMBzuCIAJXPXenmdmxxCg2', null, '2018-03-01 10:07:03', '2018-03-01 10:07:03', '1', null, '13');
INSERT INTO `users` VALUES ('74', 'ha2', 'ngocpbw110283', '1', '1', '29_ngocpbw1102@gmail.com', '$2y$10$oIVJh1sIfxs8JjvkAocfYOBwA6PCJQjbAo1hifF/FMW5HFtGhfU5O', null, '2018-03-01 11:03:12', '2018-03-01 11:03:12', '1', null, '2');
INSERT INTO `users` VALUES ('75', 'test03_2702', 'abc87', '1', '1', '21_abc@gmail.com', '$2y$10$REijmsj/26qXyJPoaYUrDuLHpmoAftidLZZXK3IZEzIFja/FPHZqW', null, '2018-03-01 11:28:25', '2018-03-01 11:28:25', '1', null, '34');
INSERT INTO `users` VALUES ('76', 'test02_2702', 'abc14', '1', '1', '59_abc@gmail.com', '$2y$10$Ud/Wqcp/ww3rSm0WpwX7COKCgHI8Y9rInct.bedRA3KSLuLFMTzji', null, '2018-03-01 11:31:12', '2018-03-01 11:31:12', '1', null, '22');
INSERT INTO `users` VALUES ('77', 'test03_2702', 'abc45', '1', '1', '78_abc@gmail.com', '$2y$10$JqXecDWXA3rFSecWH2MKI.Og2rl/jUdZRFoMAuK2MkB65bgRf3Rru', null, '2018-03-01 14:59:30', '2018-03-01 14:59:30', '1', null, '23');
INSERT INTO `users` VALUES ('78', 'test02_0103', 'thuyenht90', '1', '1', '41_thuyenht@viettel.com.vn', '$2y$10$o9rfBwLSpF.x7MT75ASXaemkK.A7iAs4./F8D8D1epbxEmh3UKs06', 'J6N3XWD3WTeaHWftefHoyXJ6e5UHDUy34waExcFJe0KcHFg55FRSiSn5tNd2', '2018-03-01 14:59:41', '2018-03-01 14:59:41', '1', null, '49');
INSERT INTO `users` VALUES ('79', 'Tes02_0103', 'thuyenht74', '1', '1', '2_thuyenht@viettel.com.vn', '$2y$10$9ecX72bQv8OvhgVg7Rgll.I3EErjSQiNIS8YyACSH8TT9vMrSltJO', 'GmWsbSVM9WFGS0eb8H2ywLXFC5JMnVLeMesoWOyWICD3hYOqzZI5UNNP2OyM', '2018-03-01 14:59:44', '2018-03-01 14:59:44', '1', null, '50');
INSERT INTO `users` VALUES ('80', 'Lãnh đạo hội', 'lanhdaohoi', '8', '1', 'lanhdaohoi@gmail.com', '$2y$10$b4d5FbxqYMTIsKTZF0VXCer9YepyjSNQpZy8J0cJ.XIMFKaCuLpGW', 'c1V3SBwRFGT5tAf9owhdWYzKBg0t1MkWXx4QLffIzPA1JbN6HkjR2zZKH4Eh', '2018-03-02 10:35:17', '2018-03-23 09:59:11', '1', '2018-03-23 09:59:11', null);
INSERT INTO `users` VALUES ('81', 'test bổ sung thông tin hội viên chính thức', 'thuyenht11', '1', '1', '53_thuyenht@viettel.com.vn', '$2y$10$i.euqsjooktdn8pngOQAN.b/SrDJqYykFwf6BUSYpMJ5nml9kv4NK', 'L83i9Kz2rZ7bdQr8bjF0NpA51w9gaDhEfimcRAdJZ32ANp9IyPXQhO40tyA0', '2018-03-02 10:56:43', '2018-03-02 10:56:43', '1', null, '52');
INSERT INTO `users` VALUES ('82', 'Nguyễn Thị Mai dữ liệu 24022018_2', 'maint1824', '1', '1', '15_maint18@viettel.com.vn', '$2y$10$dzL39agD3W6QZKSjbOH1xuaYMeJzy11gOTCBOlHeDtUE5iR.xbATW', '0i6sI7Nt4TKch86cliqfBmvSOxfXBQ8K5CoGucQHFRKNJt2jp0x9LgWAX7DB', '2018-03-02 11:42:08', '2018-03-10 09:06:51', '3', null, '6');
INSERT INTO `users` VALUES ('83', 'Nguyễn Thị Mai dữ liệu 27022018_1', 'maint1882', '1', '1', '46_maint18@viettel.com.vn', '$2y$10$vBUl4EZpRZGkMZxNJUYuIe/T.4v7jiouckcrR0XXtkj9lQRBKC7hq', null, '2018-03-02 14:32:44', '2018-03-02 14:32:44', '1', null, '25');
INSERT INTO `users` VALUES ('84', 'Dương Văn Phượng', 'phuongdv16', '1', '1', '12_phuongdv@viettel.com.vn', '$2y$10$G88Dax2JmFVT3tfxYKyFgu2x.vefiU4GY9L2O1.TzMYJc7Tkdak4e', null, '2018-03-02 14:40:58', '2018-03-02 14:40:58', '1', null, '46');
INSERT INTO `users` VALUES ('85', 'test02_0103', 'thuyenht65', '1', '1', '22_thuyenht@viettel.com.vn', '$2y$10$rNTrazGyBkNQXq0ohWaIdeDu0rcEYLs1oGMivO5WvCq/KaKD9w7Ry', 'xo04FaCPBPIOrk5P6ui8gI9L44kvvXlqNhjki2Sw3cnoz3EFf4GDaO6Ik44k', '2018-03-02 14:42:32', '2018-03-02 14:42:32', '1', null, '49');
INSERT INTO `users` VALUES ('86', 'test03_0103', 'thuyenht86', '1', '1', '16_thuyenht@viettel.com.vn', '$2y$10$.0UB3B5BFPMR8XL7eMXGIO39BtRucaG1Q7lLWyXZz78648JcDjIH.', 'B7MGCwQj0WAHMxmnvxXfCFP9Q15nZFwqd0TKANcm3Bb8j7CegVf8qGBKAja2', '2018-03-02 14:44:44', '2018-03-02 14:44:44', '1', null, '51');
INSERT INTO `users` VALUES ('87', 'test bổ sung 01', 'thuyenht27', '1', '1', '70_thuyenht@viettel.com.vn', '$2y$10$Jj4oMqTuoCrt3u397nX1XOi0tna6iOQin1YdPiHFdgT9wCpHlOpQO', 'sTh5TQXURDiJJdPC63BUlSwGdNULSJ4QMPX3COEYFbSqq13RksiyClgmIzlt', '2018-03-02 14:47:53', '2018-03-02 14:47:53', '1', null, '53');
INSERT INTO `users` VALUES ('88', 'test bổ sung 02', 'thuyenht24', '1', '1', '8_thuyenht@viettel.com.vn', '$2y$10$ymwMm0KS3Ksr0IOpvBTNz.ka3fsDSRdSq4jtT62dKybEO6eVFtGk2', null, '2018-03-02 16:14:09', '2018-03-02 16:14:09', '1', null, '54');
INSERT INTO `users` VALUES ('89', 'testcapma01', 'thuyenht58', '1', '1', '68_thuyenht@viettel.com.vn', '$2y$10$UXFWbqV.SFrAfZfE8sTnde4rCCPVjysCdjR.sLbVDP/FCmMUWX/qC', null, '2018-03-02 16:36:37', '2018-03-02 16:36:37', '1', null, '56');
INSERT INTO `users` VALUES ('90', 'testcapma02', 'thuyenht71', '1', '1', '65_thuyenht@viettel.com.vn', '$2y$10$xWd4ehczrKrHzDJsoTDXJev9ZbyHH9gy7vNgTNfHjRy5fncERRDOy', null, '2018-03-02 16:45:56', '2018-03-02 16:45:56', '1', null, '57');
INSERT INTO `users` VALUES ('91', 'ldhdl', 'ldhdl', '8', '1', 'ldhdl@gmail.com', '$2y$10$e/tZ8yT.o5cNVV/g0um9ReHnV6.UBL/3XRHmlON/pU.QfgkvUiNwC', null, '2018-03-06 09:07:45', '2018-03-22 18:24:59', '1', '2018-03-22 18:24:59', null);
INSERT INTO `users` VALUES ('92', 'Lãnh đạo hội', 'ldhoi', '8', '1', 'lanhdaohoi@viettel.com.vn', '$2y$10$zYtqwGvZlCnwtKWXm8Ip0.NOJFsYwbB.sGkjpGDThFdUdpLmYRoVW', 'm0yXASTWymQZH7aijCfQAOuep6oLaV9z7usd3nE7gxS1JEfzPKPAWz0mdLBL', '2018-03-06 09:33:19', '2018-03-22 18:24:47', '1', '2018-03-22 18:24:47', null);
INSERT INTO `users` VALUES ('93', 'Nguyễn Thị Hương dữ liệu 27022018_1', 'ngocpbw110276', '1', '1', '89_ngocpbw1102@gmail.com', '$2y$10$9vx1Cr2V2wbNykT45mesu.4N6JuHNJP7/wysA8cXdQmejb091HW/m', null, '2018-03-06 11:29:23', '2018-03-06 11:29:23', '1', null, '31');
INSERT INTO `users` VALUES ('94', 'testbosungtt_01', 'thuyenht80', '1', '1', '65_thuyenht@viettel.com.vn', '$2y$10$NavI4PjbcJUDZXwRZpm59umNGe3t0MP4uxO5vM/ic8FodmGyVataG', 'NpXm7L7fwFPEIWb3jzmWyeXDK1TS29bCPSQNIUnRa9WQ7s7Pv4bjoDsZuQFs', '2018-03-06 11:40:14', '2018-03-06 11:40:14', '3', null, '58');
INSERT INTO `users` VALUES ('95', 'bổ sung 07', 'thuyenht82', '1', '1', '62_thuyenht@viettel.com.vn', '$2y$10$NrZjP5MNTBsvKv4mUr5GiuRSxa56X8U00xXrO/s742pJaNi6DyiyC', 'YnVkjh5daIlGzZlPN2XDw3V9uvtXUxZcH9zmH4tt1t5zZkamgDYjkmJE6y4o', '2018-03-06 11:41:19', '2018-03-06 11:41:19', '1', null, '64');
INSERT INTO `users` VALUES ('96', 'Nguyễn Thị Mai 9/3/2018', 'maint1882', '1', '1', '74_maint18@viettel.com.vn', '$2y$10$rcuNeDrSIcqTJ0zYEC3Ar.FEWGAkkHPs7vKLc8rWfc11lnNabW4Ym', null, '2018-03-10 08:32:39', '2018-03-10 09:05:44', '2', null, '66');
INSERT INTO `users` VALUES ('97', 'Tập Cận Bình', 'demo69', '1', '1', '79_demo@gmail.com', '$2y$10$qXhM4ln90PH6Cfeo2ZOfEeNUskJv1PMvxtX8i5x0qHpTvbC0mYdBu', null, '2018-03-12 14:37:10', '2018-03-12 14:37:10', '2', null, '29');
INSERT INTO `users` VALUES ('98', 'Cao Thi Thao', 'demo15', '1', '1', '1_demo@gmail.com', '$2y$10$FCj32H2CxdEK08H1fmDkq.5ufRamHbt5XodGZWyv4LmQrjZddThY2', null, '2018-03-12 14:37:19', '2018-03-12 14:37:19', '1', null, '33');
INSERT INTO `users` VALUES ('99', 'test bố sung 02', 'demo17', '1', '1', '96_demo@gmail.com', '$2y$10$nqWC5IQnMj50AzQ4fOMZKOtMi4.hEAZT5ZqvhIaCX.uFcSbO6HkuW', null, '2018-03-12 14:44:26', '2018-03-12 14:44:26', '1', null, '59');
INSERT INTO `users` VALUES ('100', 'test bố sung 02', 'demo46', '1', '1', '69_demo@gmail.com', '$2y$10$HXpjowyovt95VYO0SQfb9e0X0RR9/BBANM2lYDFzJar51nsmPwPxi', null, '2018-03-12 14:44:26', '2018-03-12 14:44:26', '1', null, '59');
INSERT INTO `users` VALUES ('101', 'Nguyễn Thị Mai 12032018_outbound', 'maint1899', '1', '1', '100_maint18@viettel.com', '$2y$10$SegU/GnSZWcJC90pNA.GXu/DiAuYAQSe1keQ5ejwj5NZqWASCatgG', null, '2018-03-12 16:02:23', '2018-03-12 16:02:23', '1', null, '79');
INSERT INTO `users` VALUES ('102', 'Nguyễn Thị Mai 12032018_nội địa', 'maint1810', '1', '1', '81_maint18@viettel.com', '$2y$10$byFZyXvqJbUc3UP7nFn0we8L/Ft6oOf6xWqmSX3uiVyFtjP3uu9jC', null, '2018-03-12 16:02:28', '2018-03-12 16:02:28', '1', null, '80');
INSERT INTO `users` VALUES ('103', 'Nguyễn Thị Mai 13032018_tai diem', 'maint1858', '1', '1', '13_maint18@viettel.com.vn', '$2y$10$QlQnFhN4EzWssq39jdLDd.zWulQqVQrYuNMZE9sOxoxGbmwnNCmzG', 'tNAtOmjJ7gweVfks69OfsPl5cimWlDnq7QM80RgWg2tpLrO4EJbH0b4BqQa6', '2018-03-13 14:37:31', '2018-03-13 14:37:31', '1', null, '7');
INSERT INTO `users` VALUES ('104', 'Nguyễn Thị Mai 13032018_ inbound', 'maint1861', '1', '1', '8_maint18@viettel.com.vn', '$2y$10$FH1NIk5yKSX1aLV.n4CdJulEwbYUFLwoiSYRG.yNOlvPchtiPERn.', null, '2018-03-13 15:06:16', '2018-03-13 15:06:16', '1', null, '1');
INSERT INTO `users` VALUES ('105', 'abc2', 'thuyenht42', '1', '1', '17_thuyenht@viettel.com.vn', '$2y$10$VxYXx7qb1V3ySX0fzAUAjO0HiOZH8GuvCdwyWZ1vR5WW9EDd/WeyW', null, '2018-03-13 15:06:47', '2018-03-13 15:06:47', '3', null, '4');
INSERT INTO `users` VALUES ('106', 'Nguyễn Thị Mai 12032018_nội địa', 'maint1838', '1', '1', '27_maint18@viettel.com.vn', '$2y$10$1/PhWQIvJMvt1QAnQ/732.jzGn2oiAVKDkYlt0jFo4Pbvzpcvu7.q', null, '2018-03-13 15:06:51', '2018-03-13 15:06:51', '1', null, '5');
INSERT INTO `users` VALUES ('107', 'abc3', 'thuyenht62', '1', '1', '67_thuyenht@viettel.com.vn', '$2y$10$5e2XZLGH2Sse3DGs.B96DuY207L6hc8iCDpo6AaJXvdlq9IyOXNLa', null, '2018-03-13 15:06:59', '2018-03-13 15:06:59', '3', null, '6');
INSERT INTO `users` VALUES ('108', 'Nguyễn Thị mai 13032018_test ho so', 'maint1869', '1', '1', '57_maint18@viettel.com.vn', '$2y$10$LX9eoelbZ4VnvMyzAejudOJOGaY0ek7pPUW7d1vxBWF/mwQLetmR2', null, '2018-03-13 15:07:05', '2018-03-13 15:07:05', '1', null, '8');
INSERT INTO `users` VALUES ('109', 'abc4', 'thuyenht91', '1', '1', '85_thuyenht@viettel.com.vn', '$2y$10$YUpBkiDJjDiMUQWX7Yllp..9rgrqjlViYWBHO6si6naBQC2qeBRYe', null, '2018-03-13 15:07:08', '2018-03-13 15:07:08', '1', null, '9');
INSERT INTO `users` VALUES ('110', 'abc5', 'thuyenht52', '1', '1', '81_thuyenht@viettel.com.vn', '$2y$10$8biLn8la8yQmmYsNSUYPPeGowBXFIXly7G07idcvuFLaq5FMPbxVy', null, '2018-03-13 15:07:12', '2018-03-13 15:07:12', '1', null, '10');
INSERT INTO `users` VALUES ('111', 'abc6', 'thuyenht1', '1', '1', '7_thuyenht@viettel.com.vn', '$2y$10$anJekrMadTf/YvLbvCPn2.jGB3N49DKD0trf2HqANqilZD5hVgq7O', null, '2018-03-13 15:07:15', '2018-03-13 15:07:15', '1', null, '12');
INSERT INTO `users` VALUES ('112', 'abc7', 'thuyenht3', '1', '1', '26_thuyenht@viettel.com.vn', '$2y$10$ShbUwuCjcZtmLdDi4RLdN.gQau.k5d1mQ9w59llGTzcZ3zLNp1Q.u', 'YvEWSrFsNPRHQcfF4VtAvazJjs3SSyEKrVQnOIE2j53uTWvjZiZzAA6FfPlt', '2018-03-13 15:07:19', '2018-03-13 15:07:19', '1', null, '13');
INSERT INTO `users` VALUES ('113', 'abc8', 'thuyenht88', '1', '1', '60_thuyenht@viettel.com.vn', '$2y$10$9Lddcs.VqfxuJ1HoxLLCQOlDLCyJ1cXlZ069saxgCaPfb0GSeEfmW', null, '2018-03-13 22:16:18', '2018-03-13 22:16:18', '1', null, '14');
INSERT INTO `users` VALUES ('114', 'Viettel Test', 'ngandt589', '1', '1', '5_ngandt5@viettel.com.vn', '$2y$10$Aq3w8AAwV33/dJJ0q3vVx.T4FRphPt7hbZMWMAVhvef/7WXkCec..', null, '2018-03-14 10:39:06', '2018-03-14 10:39:06', '1', null, '17');
INSERT INTO `users` VALUES ('115', 'Viettel 1', 'ngandt532', '1', '1', '21_ngandt5@viettel.com.vn', '$2y$10$ZflaMzfSV3HrESaq/HqUz.1XP81yT33BEv.kS4Qrp7RLuZ0Oh9xvS', null, '2018-03-14 15:43:41', '2018-03-14 15:43:41', '1', null, '34');
INSERT INTO `users` VALUES ('116', 'Viettel 2', '123456781', '1', '1', '74_ngandt5@viettel.com.vn', '$2y$10$bbFRXGncF6WAihYVvAhGquXo0JUiizbsUMYryH0JhdqRLSajOZS2G', null, '2018-03-15 15:09:19', '2018-03-15 15:09:19', '1', null, '35');
INSERT INTO `users` VALUES ('117', 'thuyên_test1', '888888888', '1', '1', '28_thuyenht@viettel.com.vn', '$2y$10$U3ZaGdecdDVl8s/AYXg9QOSllmMp54VzAwVGILLWn0ifnbQsz8/ge', null, '2018-03-15 16:47:17', '2018-03-15 16:47:17', '1', null, '57');
INSERT INTO `users` VALUES ('118', 'Viettel 2', '123456782', '1', '1', '28_ngandt5@viettel.com.vn', '$2y$10$XCABn56j43OTurP5xgbc4.huFF9B7DQU5qQfZBD9t2tp8RtgcC/g.', null, '2018-03-15 17:13:18', '2018-03-15 17:13:18', '1', null, '36');
INSERT INTO `users` VALUES ('119', 'Nguyễn Thị Mai 14032018_tại điểm', '154154154', '1', '1', '46_maint18@viettel.com.vn', '$2y$10$M6UqDwCNNkDEn0FseOOCvOYLodNIR8iPBuFSQEbkC1Sw00dQILO5y', null, '2018-03-15 17:13:21', '2018-03-15 17:13:21', '1', null, '33');
INSERT INTO `users` VALUES ('120', 'test5', '444444445', '1', '1', '82_thuyenht@viettel.com.vn', '$2y$10$t3hqqYJ8Mlyh3ZNYAKC2CeEl.KW0Hg1o3RleRuFigmpYqxLpP15/e', null, '2018-03-15 17:13:24', '2018-03-15 17:13:24', '1', null, '28');
INSERT INTO `users` VALUES ('121', 'test4', '444444446', '1', '1', '75_thuyenht@viettel.com.vn', '$2y$10$mLr277uWXo5CmQWlHL/tdeL8sCNkONKlZLhIwtwCAEDfzEBVtAipe', null, '2018-03-15 17:13:27', '2018-03-15 17:13:27', '1', null, '27');
INSERT INTO `users` VALUES ('122', 'test2', '444444448', '1', '1', '74_thuyenht@viettel.com.vn', '$2y$10$Fu2O6xbbX.hswHYJ.RGV/OOXKRWmo9eGsm5D/YUXmfaEp2DHtW7IC', null, '2018-03-15 17:13:36', '2018-03-15 17:13:36', '1', null, '24');
INSERT INTO `users` VALUES ('123', 'test1', '444444449', '1', '1', '88_thuyenht@viettel.com.vn', '$2y$10$VCtBPTu97sg9HyVGzJvHP.EcgbNqo664QEgKF10wbL0C2jDGbKdnm', null, '2018-03-15 17:13:39', '2018-03-15 17:13:39', '1', null, '23');
INSERT INTO `users` VALUES ('124', 'Nguyễn Thị Mai 13032018_outbound', '136136136', '1', '1', '78_maint18@viettel.com.vn', '$2y$10$jrZR6G3.ppcLSJdfYi6Bye3nSG2CiAZSTQfaRxqFm5Uc4ZZs4QT6W', null, '2018-03-15 17:13:42', '2018-03-15 17:13:42', '1', null, '22');
INSERT INTO `users` VALUES ('125', '123', '333333339', '1', '1', '82_thuyenht@viettel.com.vn', '$2y$10$nO13ohMspEO38z8vbqx4ceRsTSVOW2J8ee8wvHuBsrt2y5WEERMo6', null, '2018-03-15 17:13:45', '2018-03-15 17:13:45', '1', null, '18');
INSERT INTO `users` VALUES ('126', 'abcd', '101111111', '1', '1', '81_thuyenht@viettel.com.vn', '$2y$10$nrQDdbm0rxfqVo3gnFOIb.OnAarTrcNrIb/v.2nJf6inYQav12v7u', 'C9K5d8BAshtvz2cI4hfsTaM2KMIX62kD51I3f40bKieeB1rEB1nGtV31Fukp', '2018-03-15 17:13:50', '2018-03-15 17:13:50', '1', null, '15');
INSERT INTO `users` VALUES ('127', 'Nguyễn Thị Mai 13032018_ inbound', '156156156', '1', '1', '85_maint18@viettel.com.vn', '$2y$10$02rgHba.kj5KEDJY2WBgye5K6H5hiqKNRjlTAnjLkkxdRbsyBhDXS', null, '2018-03-15 17:13:55', '2018-03-15 17:13:55', '1', null, '1');
INSERT INTO `users` VALUES ('128', 'Viettel 9', '232332243', '1', '1', '63_ngandt5@viettel.com.vn', '$2y$10$Ze29EHWlXCWVF2xxbzyVD.dkxiqZwv.xBElvaiQOPDZ34ry1.HUQe', 'vfVLzPeMk7NWu0KU6NW3T8wl8ehzZXnrWoVnhBHR6zhquItSJ3mZdn6IkNAo', '2018-03-16 10:13:36', '2018-03-16 10:13:36', '1', null, '59');
INSERT INTO `users` VALUES ('129', 'test7', '444444443', '1', '1', '97_thuyenht@viettel.com.vn', '$2y$10$d9PSm.r5n8ygg.ZBLM80iuDM7A7cCBDMEK2I4GD/1UBj114V088gO', 'le61JdK0zLp1tiJcBUdVKeXvXmOdh5FfbaOBfaP6S1F6bBw7TEe1V7CRrw30', '2018-03-16 15:04:54', '2018-03-16 15:04:54', '1', null, '31');
INSERT INTO `users` VALUES ('130', 'duyduc', 'admintest', '9', '1', 'daiduong47@gmail.com', '$2y$10$/CJzjXRB.sEQojyVfi2AOej1eqtGNzOes4NipVqBW24dKCGGIJYLe', '1268ZarE5VXHJZs8sXiOrWmxLfFA7qZz9zikLRconXffN4BYsL7IvVD3xxAi', '2017-12-01 02:22:22', '2017-12-01 02:22:22', '1', '0000-00-00 00:00:00', null);
INSERT INTO `users` VALUES ('131', 'aaaaaaaaaaa', '555555559', '1', '1', '48_thuyenht@viettel.com.vn', '$2y$10$FirmF5Z7s2OSv5SE6K0ga.lP29FcqQ0Lmtqrn4UwnYbOjTnOUDJfG', null, '2018-03-22 16:09:46', '2018-03-22 16:09:46', '1', null, '38');
INSERT INTO `users` VALUES ('132', 'Tran Thi Thanh Ha', '123567895', '1', '1', '35_tranha084@gmail.com', '$2y$10$14RktkDsuxQqAN9yEdyMie4CiE2VItoNwzHknEETICNTAUOsQBcVK', null, '2018-03-26 14:37:42', '2018-03-26 14:37:42', '3', null, '39');
INSERT INTO `users` VALUES ('133', 'Nguyễn Thị Mai 13032018_tai diem', '159159159', '1', '1', '53_maint18@viettel.com.vn', '$2y$10$GU2E7nrpzv6nTTGLbL9JbuT5SITQKP.A4UFjNCoCr8JUbXmvGgBQu', null, '2018-03-26 14:55:31', '2018-03-26 14:55:31', '1', null, '7');

-- ----------------------------
-- Table structure for vita
-- ----------------------------
DROP TABLE IF EXISTS `vita`;
CREATE TABLE `vita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `deptec` tinyint(1) DEFAULT NULL COMMENT '1: Ban Chấp hành',
  `deptsc` tinyint(1) DEFAULT NULL COMMENT 'Ban Thường trực',
  `deptad` tinyint(1) DEFAULT NULL COMMENT 'Ban Cố vấn',
  `deptex` tinyint(1) DEFAULT NULL COMMENT 'Ban Chuyên môn: 1 - Ban Hội viên, 2 - Ban Đào tạo , 3 -	Ban Truyền thông và sự kiện ',
  `email` varchar(255) DEFAULT NULL,
  `phone` int(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vita
-- ----------------------------
INSERT INTO `vita` VALUES ('1', '1', '1', 'Trần Trọng Kiên', 'CT', 'Chủ tịch HĐQT \r\nCông ty CP Du lịch Thiên Minh', 'Tầng 9, số 70-72 Bà Triệu, Q. Hoàn Kiếm, Hà Nội', '1', '1', null, null, null, null);
INSERT INTO `vita` VALUES ('2', '1', '1', 'Phạm Lê Thảo', 'PCT', 'Phó Vụ trưởng \nVụ Lữ hành Tổng cục Du lịch ', '80 Quán Sứ, Q. Hoàn Kiếm, HN', '1', '1', null, null, null, null);
INSERT INTO `vita` VALUES ('3', '1', '1', 'Bùi Văn Dũng', 'PCT', 'Trưởng ban Hội viên và Đào tạo HHDLVN', 'tầng 7, số 58 Kim Mã, Q. Ba Đình, HN', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('4', '1', '1', 'Hoàng Hữu Lộc', 'PCT', 'Chủ tịch HĐTV Cty TNHH MTV Dịch vụ Lữ hành Saigontourist', '45 Lê Thánh Tôn, Q1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('5', '1', '1', 'Trần Văn Long', 'PCT', 'Tổng Giám đốc Công ty CP Truyền thông Du lịch Việt', '175 Nguyễn Thái Bình, Q.1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('6', '1', '1', 'Cao Trí Dũng', 'PCT', 'Chủ tịch Chi hội Lữ hành Đà Nẵng', '68 Nguyễn Thị Minh Khai, Q. hải Châu, Đà Nẵng', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('7', '1', '1', 'Huỳnh Chí Công', 'UV', 'Phó Giám đốc Phòng hướng dẫn Công ty CP Dịch vụ Du lịch Bến Thành', '82 Calmetle, Q1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('8', '1', '1', 'Nguyễn Đức Cường', 'UV', 'Hiệp hội Du lịch TP. HCM', '18 Trương Định, Q3, TP. HCM', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('9', '1', '1', 'Lâm Hải Đào', 'UV', 'Giám đốc Phòng hướng dẫn Công ty Cổ phần FIDITOUR', '127 Nguyễn Huệ, Q1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('10', '1', '1', 'Huỳnh Công Hiếu', 'UV', 'Phó phòng Hướng dẫn viên quốc tế Công ty TNHH MTV Dịch vụ lữ hành Saigontourist', '45 Lê Thánh Tôn, Q1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('11', '1', '1', 'Trần Thị Việt Hương', 'UV', 'Giám đốc ban Hướng dẫn viên Cty CP Du lịch và Tiếp thị GTVT Việt Nam', '160 Pasteur, P6,Q3, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('12', '1', '1', 'Phan Bửu Toàn', 'UV', 'Phó Hiệu trưởng thường trực Trường Cao Đẳng nghề Du lịch Sài Gòn', '347A Nguyễn Thượng Hiền,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('13', '1', '1', 'Nguyễn Hữu Bình', 'UV', 'Phó Giám đốc Chi nhánh Công ty TNHH JTB-TNT tại Hà Nội', 'Phòng số 6, tầng M Khách sạn  Pan Pacific- Số 1 Đường Thanh Niên, Q. Ba Đình, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('14', '1', '1', 'Vũ Thái Chính', 'UV', 'Chủ tịch CLB Hướng dẫn viên tiếng Nhật Hà Nội', 'Tầng 7, 58 Kim Mã, Ba Đình, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('15', '1', '1', 'Lại Quốc Cường', 'UV', 'Giám đốc Công ty CP Du lịch Vẻ đẹp Việt', '41 Lô 3, Tổ 37A, Thanh Lương,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('16', '1', '1', 'Nguyễn Hồng Đài', 'UV', 'Giám đốc Công tyTNHH Du lịch Quốc tế Thái Bình Dương', 'Số 5 Hàng Chiếu, Hoàn Kiếm, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('17', '1', '1', 'Nguyễn Tiến Đạt', 'UV', 'Phó Giám đốc Công ty Du lịch Trần Việt', 'Tầng 4 Tòa nhà Bắc Á', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('18', '1', '1', 'Nguyễn Thị Huyền', 'UV', 'Giám đốc điều hành Công TY TNHH VIETRANTOUR', 'Tầng 1, Tòa nhà Coalimex', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('19', '1', '1', 'Nguyễn Thị Thu Mai', 'UV', 'Phó trưởng khoa Du lịch Viện Đại học Mở', 'Nhà B101, Nguyễn Hiền,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('20', '1', '1', 'Nguyễn Hồng Nguyên', '', 'Phó phòng Hướng dẫn Cty Lữ hành Hanoitourist', '18 Lý Thường Kiệt, Q. Hoàn Kiếm, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('21', '1', '1', 'Nguyễn Minh Thắng', 'UV', 'Trợ lý Trưởng phòng Điều hành Công ty TNHH Du lịch Trâu Việt Nam', 'Tầng 9, số 70-72 Bà Triệu, Q. Hoàn Kiếm, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('22', '1', '1', 'Nguyễn Đình Thành', 'UV', 'Hiệp hội Du lịch Đà Nẵng ', '14 Nguyễn Chí Thanh, Q. Hải Châu, TP. Đà Nẵng', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('23', '1', '1', 'Lê Tấn Thanh Tùng', 'UV', 'Phó Tổng Giám đốc Công ty Cổ phần Du lịch Việt Nam Vitour', '83 Nguyễn Thị minh Khai, Q. Hải Châu, TP. Đà Nẵng', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('24', '1', '1', 'Trần Trà ', 'UV', 'Chủ tịch Chi hội Hướng dẫn viên Du lịch Đà Nẵng', 'J226/1 Trương Nữ Vương,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('25', '1', '1', 'Nguyễn Thế Huệ', 'UV', 'Phó Chủ tịch Hiệp hội Du lịch Quảng Ninh', 'Trụ sở tại Cty CP Du lịch Hạ Long. Đường Hậu Cần, TP. Hạ Long, Quảng Ninh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('26', '1', '1', 'Trần Nhuận Vinh', 'UV', 'Chủ tịch CLB Hướng dẫn viên du lịch Quảng Ninh', 'Số 1 Cảng Mới, Bạch Đằng, TP.', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('27', '1', '1', 'Hoàng Tuấn Anh', 'UV', 'Giám đốc Công ty CP Quốc tế nguồn nhân lực', '85/261 Trần Nguyên Hãn, Q.', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('28', '1', '1', 'Nguyễn Hữu Bắc', 'UV', 'Chủ tịch Công ty TNHH MTV Đầu tư Du lịch Phuc Group', 'P.11 17 CT1, Tòa nhà Kim Thi,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('29', '1', '1', 'Nguyễn Phương Đông', 'UV', 'Trưởng văn phòng hướng dẫn thăm quan Đô thị cổ Hội An', '01 Cao Lãnh, TP. Hội An, Quảng Nam', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('30', '1', '1', 'Nguyễn Tiến Giáp', 'UV', 'Chủ nhiệm CLB Hướng dẫn viên du lịch Thanh Hóa', 'Số 411 Trần Hưng Đạo, TP. Thanh Hóa', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('31', '1', '1', 'Lê Đình Huy', 'UV', 'Chủ nhiệm CLB Hướng dẫn viên du lịch Huế', '1/63 La Sơn Phu Tử, Tp. Huế', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('32', '1', '1', 'Nguyễn Vũ Khắc Huy', 'UV', 'Giám đốc Công ty TNHH MTV Du lịch VINA Phú Quốc', 'Tổ 14, Đường 30/4 Khu phố 1', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('33', '1', '1', 'Trần Mạnh Khang', 'UV', 'Tổ trưởng Hướng dẫn viên Vietravel Cần Thơ', '05-07 Trần Văn Khéo, P. Cái Khế,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('34', '1', '1', 'Nguyễn Thị Xuân Lan', 'UV', 'Giám đốc Công ty TNHH Golden Life', '43A Lê Thánh Tôn, TP. Quy Nhơn, Bình Định', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('35', '1', '1', 'Phan Long', 'UV', 'Giám đốc Vietravel Quảng Ngãi', '516 Quang Trung, TP. Quảng Ngãi', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('36', '1', '1', 'Hoàng Bình Minh', 'UV', 'Giám đốc Công ty CP Du lịch Ninh Bình', '50 Lý Tự Trọng, TP. Ninh Bình', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('37', '1', '1', 'Võ Thanh Mỹ', 'UV', 'Giám đốc Công ty Du lịch Vietravel Vũng Tầu', '150 Trương Công Định,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('38', '1', '1', 'Phạm Chí Ta', 'UV', 'Giám đốc Công ty TNHH Lữ hành Cao nguyên Việt Nam', '24 Lý Thường Kiệt, TP.', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('39', '1', '1', 'Bùi Minh Thắng', 'UV', 'Giám đốc Công ty TNHH DVDL Thương mại Phương Thắng', '31 Nguyễn Cảnh Đức, TP. Nha Trang, Khánh Hòa', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('40', '1', '1', 'Đinh Văn Thép', 'UV', 'Chủ tịch HĐQT Công ty TNHH MTV Du lịch Thái Hân', '68 Nguyễn Thái Học,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('41', '1', '1', 'Võ Đức Trung', 'UV', 'Giám đốc Chi nhánh Công ty CP Mạo Hiểm Việt tại Đà Lạt', '109 Nguyễn Văn Trỗi, TP.Đà Lạt, Lâm Đồng', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('42', '1', '1', 'Nguyễn Cường Hiền', 'Ủy viên', null, null, null, '1', null, null, null, null);
INSERT INTO `vita` VALUES ('43', '1', '1', 'Nguyễn Hữu Trà', 'Ủy viên', null, null, null, '1', null, null, null, null);
INSERT INTO `vita` VALUES ('44', '1', '1', 'Chu Đức Ảnh', 'Ủy viên', null, null, null, '1', null, null, null, null);
INSERT INTO `vita` VALUES ('45', '1', '1', 'Phạm Mạnh Cương', 'Trưởng ban', null, null, null, null, null, '1', 'cuongpm84@gmail.com', '986989661');
INSERT INTO `vita` VALUES ('46', '1', '1', 'Nguyễn Quang Trung', 'Trưởng ban', null, null, null, null, null, '2', 'quangtrung.dl0812@gmail.com', '973581219');

-- ----------------------------
-- Table structure for workhistory
-- ----------------------------
DROP TABLE IF EXISTS `workhistory`;
CREATE TABLE `workhistory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `elementId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `workhistory_memberid_foreign` (`memberId`),
  KEY `workhistory_elementid_foreign` (`elementId`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of workhistory
-- ----------------------------
INSERT INTO `workhistory` VALUES ('3', '2', '3', null, null);
INSERT INTO `workhistory` VALUES ('4', '2', '4', null, null);
INSERT INTO `workhistory` VALUES ('5', '3', '5', null, null);
INSERT INTO `workhistory` VALUES ('6', '4', '6', null, null);
INSERT INTO `workhistory` VALUES ('7', '4', '7', null, null);
INSERT INTO `workhistory` VALUES ('8', '5', '8', null, null);
INSERT INTO `workhistory` VALUES ('9', '5', '9', null, null);
INSERT INTO `workhistory` VALUES ('10', '6', '10', null, null);
INSERT INTO `workhistory` VALUES ('11', '7', '3', null, null);
INSERT INTO `workhistory` VALUES ('12', '7', '11', null, null);
INSERT INTO `workhistory` VALUES ('13', '8', '12', null, null);
INSERT INTO `workhistory` VALUES ('14', '8', '13', null, null);
INSERT INTO `workhistory` VALUES ('15', '9', '14', null, null);
INSERT INTO `workhistory` VALUES ('16', '10', '15', null, null);
INSERT INTO `workhistory` VALUES ('17', '11', '3', null, null);
INSERT INTO `workhistory` VALUES ('18', '11', '16', null, null);
INSERT INTO `workhistory` VALUES ('19', '12', '17', null, null);
INSERT INTO `workhistory` VALUES ('20', '13', '18', null, null);
INSERT INTO `workhistory` VALUES ('21', '14', '19', null, null);
INSERT INTO `workhistory` VALUES ('22', '15', '20', null, null);
INSERT INTO `workhistory` VALUES ('25', '18', '23', null, null);
INSERT INTO `workhistory` VALUES ('26', '19', '24', null, null);
INSERT INTO `workhistory` VALUES ('27', '20', '25', null, null);
INSERT INTO `workhistory` VALUES ('28', '21', '26', null, null);
INSERT INTO `workhistory` VALUES ('29', '21', '27', null, null);
INSERT INTO `workhistory` VALUES ('30', '22', '28', null, null);
INSERT INTO `workhistory` VALUES ('31', '22', '29', null, null);
INSERT INTO `workhistory` VALUES ('32', '23', '30', null, null);
INSERT INTO `workhistory` VALUES ('33', '24', '31', null, null);
INSERT INTO `workhistory` VALUES ('34', '25', '32', null, null);
INSERT INTO `workhistory` VALUES ('35', '26', '3', null, null);
INSERT INTO `workhistory` VALUES ('36', '26', '33', null, null);
INSERT INTO `workhistory` VALUES ('37', '27', '34', null, null);
INSERT INTO `workhistory` VALUES ('38', '28', '35', null, null);
INSERT INTO `workhistory` VALUES ('39', '28', '36', null, null);
INSERT INTO `workhistory` VALUES ('40', '29', '37', null, null);
INSERT INTO `workhistory` VALUES ('41', '30', '3', null, null);
INSERT INTO `workhistory` VALUES ('42', '30', '38', null, null);
INSERT INTO `workhistory` VALUES ('44', '32', '40', null, null);
INSERT INTO `workhistory` VALUES ('45', '32', '11', null, null);
INSERT INTO `workhistory` VALUES ('46', '32', '41', null, null);
INSERT INTO `workhistory` VALUES ('47', '33', '40', null, null);
INSERT INTO `workhistory` VALUES ('48', '34', '42', null, null);
INSERT INTO `workhistory` VALUES ('49', '35', '43', null, null);
INSERT INTO `workhistory` VALUES ('50', '36', '43', null, null);
INSERT INTO `workhistory` VALUES ('51', '37', '44', null, null);
INSERT INTO `workhistory` VALUES ('53', '38', '46', null, null);
INSERT INTO `workhistory` VALUES ('54', '39', '47', null, null);
INSERT INTO `workhistory` VALUES ('55', '40', '43', null, null);
INSERT INTO `workhistory` VALUES ('56', '41', '48', null, null);
INSERT INTO `workhistory` VALUES ('57', '41', '49', null, null);
INSERT INTO `workhistory` VALUES ('58', '42', '40', null, null);
INSERT INTO `workhistory` VALUES ('59', '42', '50', null, null);
INSERT INTO `workhistory` VALUES ('60', '43', '51', null, null);
INSERT INTO `workhistory` VALUES ('61', '44', '52', null, null);
INSERT INTO `workhistory` VALUES ('62', '44', '53', null, null);
INSERT INTO `workhistory` VALUES ('63', '45', '54', null, null);
INSERT INTO `workhistory` VALUES ('64', '46', '55', null, null);
INSERT INTO `workhistory` VALUES ('65', '46', '56', null, null);
INSERT INTO `workhistory` VALUES ('66', '46', '57', null, null);
INSERT INTO `workhistory` VALUES ('67', '47', '58', null, null);
INSERT INTO `workhistory` VALUES ('68', '48', '59', null, null);
INSERT INTO `workhistory` VALUES ('69', '49', '60', null, null);
INSERT INTO `workhistory` VALUES ('70', '49', '16', null, null);
INSERT INTO `workhistory` VALUES ('71', '50', '61', null, null);
INSERT INTO `workhistory` VALUES ('72', '51', '62', null, null);
INSERT INTO `workhistory` VALUES ('73', '51', '63', null, null);
INSERT INTO `workhistory` VALUES ('74', '51', '64', null, null);
INSERT INTO `workhistory` VALUES ('75', '52', '65', null, null);
INSERT INTO `workhistory` VALUES ('76', '52', '66', null, null);
INSERT INTO `workhistory` VALUES ('78', '54', '68', null, null);
INSERT INTO `workhistory` VALUES ('79', '55', '69', null, null);
INSERT INTO `workhistory` VALUES ('80', '55', '70', null, null);
INSERT INTO `workhistory` VALUES ('81', '56', '71', null, null);
INSERT INTO `workhistory` VALUES ('82', '57', '72', null, null);
INSERT INTO `workhistory` VALUES ('83', '57', '73', null, null);
INSERT INTO `workhistory` VALUES ('85', '58', '74', null, null);
INSERT INTO `workhistory` VALUES ('86', '58', '75', null, null);
INSERT INTO `workhistory` VALUES ('87', '59', '76', null, null);
INSERT INTO `workhistory` VALUES ('88', '60', '77', null, null);
INSERT INTO `workhistory` VALUES ('89', '60', '78', null, null);
INSERT INTO `workhistory` VALUES ('90', '60', '79', null, null);
INSERT INTO `workhistory` VALUES ('91', '60', '80', null, null);
INSERT INTO `workhistory` VALUES ('92', '61', '76', null, null);
INSERT INTO `workhistory` VALUES ('93', '62', '76', null, null);
INSERT INTO `workhistory` VALUES ('94', '53', '67', '2018-03-16 11:31:30', '2018-03-16 11:31:30');
INSERT INTO `workhistory` VALUES ('95', '63', '76', null, null);
INSERT INTO `workhistory` VALUES ('96', '64', '81', null, null);
INSERT INTO `workhistory` VALUES ('97', '64', '82', null, null);
INSERT INTO `workhistory` VALUES ('98', '64', '83', null, null);
INSERT INTO `workhistory` VALUES ('99', '64', '84', null, null);
INSERT INTO `workhistory` VALUES ('100', '64', '85', null, null);
INSERT INTO `workhistory` VALUES ('101', '64', '86', null, null);
INSERT INTO `workhistory` VALUES ('102', '64', '87', null, null);
INSERT INTO `workhistory` VALUES ('103', '64', '88', null, null);
INSERT INTO `workhistory` VALUES ('104', '64', '89', null, null);
INSERT INTO `workhistory` VALUES ('105', '64', '90', null, null);
INSERT INTO `workhistory` VALUES ('106', '64', '91', null, null);
INSERT INTO `workhistory` VALUES ('107', '64', '92', null, null);
INSERT INTO `workhistory` VALUES ('108', '64', '93', null, null);
INSERT INTO `workhistory` VALUES ('109', '64', '36', null, null);
INSERT INTO `workhistory` VALUES ('110', '65', '94', null, null);
INSERT INTO `workhistory` VALUES ('111', '65', '95', null, null);
INSERT INTO `workhistory` VALUES ('112', '66', '94', null, null);
INSERT INTO `workhistory` VALUES ('113', '66', '95', null, null);
INSERT INTO `workhistory` VALUES ('114', '67', '94', null, null);
INSERT INTO `workhistory` VALUES ('115', '67', '95', null, null);
INSERT INTO `workhistory` VALUES ('116', '68', '94', null, null);
INSERT INTO `workhistory` VALUES ('117', '68', '95', null, null);
INSERT INTO `workhistory` VALUES ('118', '69', '96', null, null);
INSERT INTO `workhistory` VALUES ('121', '71', '97', null, null);
INSERT INTO `workhistory` VALUES ('122', '71', '90', null, null);
INSERT INTO `workhistory` VALUES ('123', '71', '98', null, null);
INSERT INTO `workhistory` VALUES ('124', '71', '36', null, null);
INSERT INTO `workhistory` VALUES ('125', '72', '94', null, null);
INSERT INTO `workhistory` VALUES ('126', '72', '95', null, null);
INSERT INTO `workhistory` VALUES ('127', '73', '99', null, null);
INSERT INTO `workhistory` VALUES ('130', '74', '94', null, null);
INSERT INTO `workhistory` VALUES ('131', '74', '95', null, null);
INSERT INTO `workhistory` VALUES ('132', '75', '94', null, null);
INSERT INTO `workhistory` VALUES ('133', '75', '95', null, null);
INSERT INTO `workhistory` VALUES ('134', '76', '94', null, null);
INSERT INTO `workhistory` VALUES ('135', '76', '95', null, null);
INSERT INTO `workhistory` VALUES ('136', '77', '94', null, null);
INSERT INTO `workhistory` VALUES ('137', '77', '95', null, null);
INSERT INTO `workhistory` VALUES ('138', '78', '94', null, null);
INSERT INTO `workhistory` VALUES ('139', '78', '95', null, null);
INSERT INTO `workhistory` VALUES ('142', '70', '94', '2018-03-16 15:37:33', '2018-03-16 15:37:33');
INSERT INTO `workhistory` VALUES ('143', '70', '95', '2018-03-16 15:37:33', '2018-03-16 15:37:33');
INSERT INTO `workhistory` VALUES ('144', '16', '21', '2018-03-16 15:39:20', '2018-03-16 15:39:20');
INSERT INTO `workhistory` VALUES ('154', '31', '112', null, null);
INSERT INTO `workhistory` VALUES ('155', '31', '27', null, null);
INSERT INTO `workhistory` VALUES ('166', '79', '116', '2018-03-16 17:06:28', '2018-03-16 17:06:28');
INSERT INTO `workhistory` VALUES ('167', '79', '116', '2018-03-16 17:06:28', '2018-03-16 17:06:28');
INSERT INTO `workhistory` VALUES ('168', '79', '117', '2018-03-16 17:06:28', '2018-03-16 17:06:28');
INSERT INTO `workhistory` VALUES ('169', '79', '118', '2018-03-16 17:06:28', '2018-03-16 17:06:28');
INSERT INTO `workhistory` VALUES ('170', '1', '121', null, null);
INSERT INTO `workhistory` VALUES ('171', '1', '122', null, null);
INSERT INTO `workhistory` VALUES ('172', '80', '123', null, null);
INSERT INTO `workhistory` VALUES ('173', '17', '124', null, null);
INSERT INTO `workhistory` VALUES ('174', '81', '125', null, null);
INSERT INTO `workhistory` VALUES ('175', '81', '126', null, null);
INSERT INTO `workhistory` VALUES ('176', '82', '125', null, null);
INSERT INTO `workhistory` VALUES ('177', '82', '126', null, null);
INSERT INTO `workhistory` VALUES ('183', '83', '127', '2018-03-19 15:59:51', '2018-03-19 15:59:51');
INSERT INTO `workhistory` VALUES ('184', '83', '128', '2018-03-19 15:59:51', '2018-03-19 15:59:51');
INSERT INTO `workhistory` VALUES ('185', '83', '129', '2018-03-19 15:59:51', '2018-03-19 15:59:51');

-- ----------------------------
-- Table structure for works
-- ----------------------------
DROP TABLE IF EXISTS `works`;
CREATE TABLE `works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `elementName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeOfContract` tinyint(4) DEFAULT NULL,
  `fromDate` datetime DEFAULT NULL,
  `toDate` datetime DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of works
-- ----------------------------
INSERT INTO `works` VALUES ('3', 'Tên doanh nghiệp 1', '1', '2014-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-13 09:59:56', '2018-03-13 09:59:56', null);
INSERT INTO `works` VALUES ('4', 'Tên doanh nghiệp 2', '2', '2015-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-13 09:59:56', '2018-03-13 09:59:56', null);
INSERT INTO `works` VALUES ('5', 'abc1', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 10:05:04', '2018-03-13 10:05:04', null);
INSERT INTO `works` VALUES ('6', 'abc2', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 10:10:03', '2018-03-13 10:10:03', null);
INSERT INTO `works` VALUES ('7', 'abc2', '2', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 10:10:03', '2018-03-13 10:10:03', null);
INSERT INTO `works` VALUES ('8', 'Tên doanh nghiệp 1', '1', '2015-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-13 10:11:17', '2018-03-13 10:11:17', null);
INSERT INTO `works` VALUES ('9', 'Tên doanh nghieepjj 2', '2', '2014-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-13 10:11:17', '2018-03-13 10:11:17', null);
INSERT INTO `works` VALUES ('10', 'abc3', '1', '2013-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-13 10:11:40', '2018-03-13 10:11:40', null);
INSERT INTO `works` VALUES ('11', 'Tên doanh nghiệp 2', '2', '2016-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-13 10:14:55', '2018-03-13 10:14:55', null);
INSERT INTO `works` VALUES ('12', 'sdfasf', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 10:35:52', '2018-03-13 10:35:52', null);
INSERT INTO `works` VALUES ('13', 'sdfasdfa', '2', null, null, '2018-03-13 10:35:52', '2018-03-13 10:35:52', null);
INSERT INTO `works` VALUES ('14', 'abc4', '2', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 11:42:24', '2018-03-13 11:42:24', null);
INSERT INTO `works` VALUES ('15', 'abc5', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-13 13:57:24', '2018-03-13 13:57:24', null);
INSERT INTO `works` VALUES ('16', 'Tên doanh nghiệp 2', '2', '2015-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-13 14:00:48', '2018-03-13 14:00:48', null);
INSERT INTO `works` VALUES ('17', 'abc', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-13 14:13:06', '2018-03-13 14:13:06', null);
INSERT INTO `works` VALUES ('18', 'abc7', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-13 14:16:53', '2018-03-13 14:16:53', null);
INSERT INTO `works` VALUES ('19', 'abc', '2', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 14:20:27', '2018-03-13 14:20:27', null);
INSERT INTO `works` VALUES ('20', 'abc', '2', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 14:22:15', '2018-03-13 14:22:15', null);
INSERT INTO `works` VALUES ('21', '111111421', '1', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-13 22:29:33', '2018-03-13 22:29:33', null);
INSERT INTO `works` VALUES ('23', 'abc', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-14 09:42:54', '2018-03-14 09:42:54', null);
INSERT INTO `works` VALUES ('24', 'abc', '1', '2017-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 10:31:48', '2018-03-14 10:31:48', null);
INSERT INTO `works` VALUES ('25', 'absdfsdfdsf', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 10:34:11', '2018-03-14 10:34:11', null);
INSERT INTO `works` VALUES ('26', 'ádfasdf', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 10:37:23', '2018-03-14 10:37:23', null);
INSERT INTO `works` VALUES ('27', 'sdfasdf', '2', null, null, '2018-03-14 10:37:23', '2018-03-14 10:37:23', null);
INSERT INTO `works` VALUES ('28', 'sdfasdfa', '1', '2014-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-14 10:40:40', '2018-03-14 10:40:40', null);
INSERT INTO `works` VALUES ('29', 'ádfasdf', '2', null, null, '2018-03-14 10:40:40', '2018-03-14 10:40:40', null);
INSERT INTO `works` VALUES ('30', 'FSDFSDFDS', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-14 10:58:44', '2018-03-14 10:58:44', null);
INSERT INTO `works` VALUES ('31', 'àdasd', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 11:01:34', '2018-03-14 11:01:34', null);
INSERT INTO `works` VALUES ('32', 'sdfsdf', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-14 11:03:19', '2018-03-14 11:03:19', null);
INSERT INTO `works` VALUES ('33', 'Tên doanh nghiệp 2', '2', '2017-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-14 11:04:03', '2018-03-14 11:04:03', null);
INSERT INTO `works` VALUES ('34', 'ádfsd', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-14 11:05:08', '2018-03-14 11:05:08', null);
INSERT INTO `works` VALUES ('35', 'sadfsdfdsf', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 11:08:33', '2018-03-14 11:08:33', null);
INSERT INTO `works` VALUES ('36', null, null, null, null, '2018-03-14 11:08:33', '2018-03-14 11:08:33', null);
INSERT INTO `works` VALUES ('37', 'sdf', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-14 11:10:14', '2018-03-14 11:10:14', null);
INSERT INTO `works` VALUES ('38', 'Tên doanh nghiệp 2', '2', '2016-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-14 11:11:30', '2018-03-14 11:11:30', null);
INSERT INTO `works` VALUES ('40', 'Tên doanh nghiệp 1', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 11:15:27', '2018-03-14 11:15:27', null);
INSERT INTO `works` VALUES ('41', 'Tên doanh nghiệp 3', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 11:15:27', '2018-03-14 11:15:27', null);
INSERT INTO `works` VALUES ('42', 'rrr', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-14 11:20:04', '2018-03-14 11:20:04', null);
INSERT INTO `works` VALUES ('43', 'Viettrantour', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-14 11:22:42', '2018-03-14 11:22:42', null);
INSERT INTO `works` VALUES ('44', 'fdf', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-14 17:29:23', '2018-03-14 17:29:23', null);
INSERT INTO `works` VALUES ('45', 'sdfsd', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-14 17:30:16', '2018-03-14 17:30:16', null);
INSERT INTO `works` VALUES ('46', 'sdfsd', '1', '2015-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 17:31:14', '2018-03-14 17:31:14', null);
INSERT INTO `works` VALUES ('47', '123567895123567895123567895', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 10:53:49', '2018-03-15 10:53:49', null);
INSERT INTO `works` VALUES ('48', 'Viettrantour', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-15 11:29:04', '2018-03-15 11:29:04', null);
INSERT INTO `works` VALUES ('49', 'qưerrt', '2', '2016-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:29:04', '2018-03-15 11:29:04', null);
INSERT INTO `works` VALUES ('50', 'Tên doanh nghiệp 2', '2', '2016-01-01 00:00:00', '2020-01-01 00:00:00', '2018-03-15 11:29:59', '2018-03-15 11:29:59', null);
INSERT INTO `works` VALUES ('51', 'sdfdsfdsfsd', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-15 11:31:40', '2018-03-15 11:31:40', null);
INSERT INTO `works` VALUES ('52', 'sdfsdf', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:36:10', '2018-03-15 11:36:10', null);
INSERT INTO `works` VALUES ('53', 'adfsdfdsfdsf', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:36:10', '2018-03-15 11:36:10', null);
INSERT INTO `works` VALUES ('54', 'Tên doanh nghiệp 1', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:38:59', '2018-03-15 11:38:59', null);
INSERT INTO `works` VALUES ('55', 'sadasdasd', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:39:11', '2018-03-15 11:39:11', null);
INSERT INTO `works` VALUES ('56', 'ádasdas', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-15 11:39:11', '2018-03-15 11:39:11', null);
INSERT INTO `works` VALUES ('57', 'ádsadas', '1', '2018-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-15 11:39:11', '2018-03-15 11:39:11', null);
INSERT INTO `works` VALUES ('58', '535353211', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:52:59', '2018-03-15 11:52:59', null);
INSERT INTO `works` VALUES ('59', 'Tên doanh nghiệp 1', '2', '2015-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-15 11:52:59', '2018-03-15 11:52:59', null);
INSERT INTO `works` VALUES ('60', 'Tên doanh nghiệp1', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-15 13:38:43', '2018-03-15 13:38:43', null);
INSERT INTO `works` VALUES ('61', '678978998678978998', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 13:44:37', '2018-03-15 13:44:37', null);
INSERT INTO `works` VALUES ('62', 'DN1', '1', '2018-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-15 13:53:07', '2018-03-15 13:53:07', null);
INSERT INTO `works` VALUES ('63', 'DN22222222222222222222222222222333333333333333333333333334444444444444444444444444444444444444444444', '2', '2015-01-01 00:00:00', '2019-01-01 00:00:00', '2018-03-15 13:53:07', '2018-03-15 13:53:07', null);
INSERT INTO `works` VALUES ('64', 'DN3', null, null, null, '2018-03-15 13:53:07', '2018-03-15 13:53:07', null);
INSERT INTO `works` VALUES ('65', 'ewewew', '1', '2018-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-15 14:30:20', '2018-03-15 14:30:20', null);
INSERT INTO `works` VALUES ('66', 'ew', '1', '2018-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-15 14:30:20', '2018-03-15 14:30:20', null);
INSERT INTO `works` VALUES ('67', '632222222', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 14:33:57', '2018-03-15 14:33:57', null);
INSERT INTO `works` VALUES ('68', '323232323', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 14:38:02', '2018-03-15 14:38:02', null);
INSERT INTO `works` VALUES ('69', 'vn', '1', '2018-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-15 16:18:01', '2018-03-15 16:18:01', null);
INSERT INTO `works` VALUES ('70', 'fdf', null, null, null, '2018-03-15 16:18:01', '2018-03-15 16:18:01', null);
INSERT INTO `works` VALUES ('71', 'ểwr', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-15 16:27:16', '2018-03-15 16:27:16', null);
INSERT INTO `works` VALUES ('72', 'abc', '1', '2018-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-15 16:27:39', '2018-03-15 16:27:39', null);
INSERT INTO `works` VALUES ('73', 'sdfsdfsdfsddssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2018-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-15 16:27:39', '2018-03-15 16:27:39', null);
INSERT INTO `works` VALUES ('74', 'ểwr', '1', '2018-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-15 16:31:06', '2018-03-15 16:31:06', null);
INSERT INTO `works` VALUES ('75', 'dsads', null, null, null, '2018-03-15 16:31:29', '2018-03-15 16:31:29', null);
INSERT INTO `works` VALUES ('76', 'vn', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 09:29:02', '2018-03-16 09:29:02', null);
INSERT INTO `works` VALUES ('77', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt qua được nỗi ám ảnh của cuộc thảm sát Mỹ Lai. Mike', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 09:36:06', '2018-03-16 09:36:06', null);
INSERT INTO `works` VALUES ('78', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt qua được nỗi ám ảnh của cuộc thảm sát Mỹ Lai. Mike', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 09:36:06', '2018-03-16 09:36:06', null);
INSERT INTO `works` VALUES ('79', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt qua được nỗi ám ảnh của cuộc thảm sát Mỹ Lai. Mike', '1', '2015-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 09:36:06', '2018-03-16 09:36:06', null);
INSERT INTO `works` VALUES ('80', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt qua được nỗi ám ảnh của cuộc thảm sát Mỹ Lai. Mike', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 09:36:06', '2018-03-16 09:36:06', null);
INSERT INTO `works` VALUES ('81', 'Tên doanh nghiệp 1Tên doanh nghiệp 1Tên doanh nghiệp 1Tên doanh nghiệp 1Tên doanh nghiệp 1Tên doaEnd', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('82', 'Tên doanh nghiệp 2Tên doanh nghiệp 2Tên doanh nghiệp 2Tên doanh nghiệp 2Tên doanh nghiệp 2Tên doaEnđ', '2', '2015-01-01 00:00:00', '2023-01-01 00:00:00', '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('83', 'sdfadf', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('84', 'ádfadf', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('85', 'ádfaf', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('86', 'ádfasdf', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('87', 'sdfasdf', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('88', 'ádfasdfa', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('89', 'sdfasdfas', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('90', null, '2', null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('91', null, null, '2019-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('92', null, null, '2018-01-01 00:00:00', null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('93', null, null, null, '2015-01-01 00:00:00', '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('94', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 13:32:45', '2018-03-16 13:32:45', null);
INSERT INTO `works` VALUES ('95', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '2', '2015-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 13:32:45', '2018-03-16 13:32:45', null);
INSERT INTO `works` VALUES ('96', 'sdfsdfdsfsdfsdfdsfdsfdsfdsffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffgggggggggg', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 13:39:48', '2018-03-16 13:39:48', null);
INSERT INTO `works` VALUES ('97', 'Tên doanh nghiệp 1Tên doanh nghiệp 1Tên doanh nghiệp 1Tên doanh nghiệp 1Tên doanh nghiệp 1Tên doaEnđ', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 13:44:56', '2018-03-16 13:44:56', null);
INSERT INTO `works` VALUES ('98', null, null, '2015-01-01 00:00:00', null, '2018-03-16 13:44:56', '2018-03-16 13:44:56', null);
INSERT INTO `works` VALUES ('99', 'sdfasdf', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 14:25:47', '2018-03-16 14:25:47', null);
INSERT INTO `works` VALUES ('104', 'sdfsdfsd', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 16:09:19', '2018-03-16 16:09:19', null);
INSERT INTO `works` VALUES ('107', 'dsfsdfds sửa', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 16:25:25', '2018-03-16 16:25:25', null);
INSERT INTO `works` VALUES ('108', 'dsfsdfds sửa', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:25:25', '2018-03-16 16:25:25', null);
INSERT INTO `works` VALUES ('109', 'dsfsdfds sửa', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:25:25', '2018-03-16 16:25:25', null);
INSERT INTO `works` VALUES ('112', 'dsfsdfds', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-16 16:28:16', '2018-03-16 16:28:16', null);
INSERT INTO `works` VALUES ('113', '11111111111sua2', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 16:37:41', '2018-03-16 16:37:41', null);
INSERT INTO `works` VALUES ('114', '11111111111sua2', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:37:41', '2018-03-16 16:37:41', null);
INSERT INTO `works` VALUES ('115', '11111111111sua2', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:37:41', '2018-03-16 16:37:41', null);
INSERT INTO `works` VALUES ('116', 'bổ sung thẩm định lần 3', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 16:55:06', '2018-03-16 16:55:06', null);
INSERT INTO `works` VALUES ('117', 'bổ sung thẩm định lần 3', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:55:06', '2018-03-16 16:55:06', null);
INSERT INTO `works` VALUES ('118', 'bổ sung thẩm định lần 3', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:55:06', '2018-03-16 16:55:06', null);
INSERT INTO `works` VALUES ('121', 'Tên doanh nghiệp 123355', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-16 17:10:07', '2018-03-16 17:10:07', null);
INSERT INTO `works` VALUES ('122', 'Tên doanh nghiệp 223355', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 17:10:07', '2018-03-16 17:10:07', null);
INSERT INTO `works` VALUES ('123', 'dsfsfdsdf', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 17:37:11', '2018-03-16 17:37:11', null);
INSERT INTO `works` VALUES ('124', 'Viettrantour', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-19 08:50:33', '2018-03-19 08:50:33', null);
INSERT INTO `works` VALUES ('125', 'Tên doanh nghiệp đã và đang công tác Tên doanh nghiệp đã và đang công tác', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-19 11:37:54', '2018-03-19 11:37:54', null);
INSERT INTO `works` VALUES ('126', 'Tên doanh nghiệp đã và đang công tác 11111', '2', '2016-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-19 11:37:54', '2018-03-19 11:37:54', null);
INSERT INTO `works` VALUES ('127', 'Tên doanh nghiệp đã và đang công tác Tên doanh nghiệp đã và đang công tác', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-19 15:42:18', '2018-03-19 15:42:18', null);
INSERT INTO `works` VALUES ('128', 'Tên doanh nghiệp đã và đang công tác 11111', '2', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-19 15:42:18', '2018-03-19 15:42:18', null);
INSERT INTO `works` VALUES ('129', '222', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-19 15:47:46', '2018-03-19 15:47:46', null);
