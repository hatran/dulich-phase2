/*
Navicat MySQL Data Transfer

Source Server         : LOCALHOST
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : dulich

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-04-23 01:00:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for achievementfiles
-- ----------------------------
DROP TABLE IF EXISTS `achievementfiles`;
CREATE TABLE `achievementfiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `achievementfiles_memberid_foreign` (`memberId`),
  KEY `achievementfiles_fileid_foreign` (`fileId`),
  CONSTRAINT `achievementfiles_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `achievementfiles_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of achievementfiles
-- ----------------------------

-- ----------------------------
-- Table structure for branches
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_info` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `parent_id` int(11) DEFAULT NULL,
  `option_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of branches
-- ----------------------------
INSERT INTO `branches` VALUES ('1', 'ma008', 'Văn phòng HN', 'HN', '08765432', 'a@gmai.com', null, null, null, null, '/offices_201804130314149.jpg', '1', '2018-04-12 03:10:37', '2018-04-13 03:14:14', null);
INSERT INTO `branches` VALUES ('2', 'ma008', 'Trần ban', null, '098765432', 'a@gmai.com', null, null, '1', 'BRANCHES01', null, '1', '2018-04-12 03:11:09', '2018-04-12 03:11:09', null);
INSERT INTO `branches` VALUES ('4', 'MA002T', 'Lê Lan', 'HN', '09876543', 'Lan@gmail.com', null, null, '3', 'BRANCHES01', null, '1', '2018-04-12 03:31:25', '2018-04-12 03:31:25', null);
INSERT INTO `branches` VALUES ('5', 'CLB01', 'Câu lạc bộ tiếng nhật', 'HN', '098765432', 'a@gmai.com', null, null, null, 'BRANCHES03', null, '1', '2018-04-12 03:53:17', '2018-04-13 04:54:24', null);
INSERT INTO `branches` VALUES ('6', 'qvmPz0UJ', 'Câu lạc bộ tiếng nhật', 'HN', '0987654321', 'a@gmai.com', null, null, '4', 'BRANCHES02', null, '1', '2018-04-12 03:53:48', '2018-04-12 06:49:17', null);
INSERT INTO `branches` VALUES ('8', 'JkHY37rz', 'CLB Thuộc Hội', 'Hà Nội', '098765432234', 'tienganh@gmail.com', null, null, '1', 'BRANCHES03', null, '1', '2018-04-12 05:31:02', '2018-04-13 04:54:01', null);
INSERT INTO `branches` VALUES ('9', 'ma001', 'A.Tú', null, '09876543456', 'a@gmai.com', null, null, '3', 'BRANCHES01', null, '1', '2018-04-12 06:44:53', '2018-04-12 06:47:14', null);
INSERT INTO `branches` VALUES ('10', 'CLB01', 'Câu lạc bộ tiếng nhật', 'Đền bao gồm tổ hợp ba công trình kiến trúc: đền Th', '098652345', 'anh@gmail.com', null, null, '9', 'BRANCHES02', null, '1', '2018-04-12 06:49:02', '2018-04-13 04:35:54', null);
INSERT INTO `branches` VALUES ('11', '6lvdqubL', 'Câu lạc bộ tiếng hàn', 'TB', '0987654321', 'tunguyenanh900@gmail.com', null, null, '9', 'BRANCHES02', null, '1', '2018-04-12 07:02:24', '2018-04-12 07:08:08', null);
INSERT INTO `branches` VALUES ('12', 'ma002', 'Lan Anh', null, '09876543', 'lananh@gmail.com', null, null, '3', 'BRANCHES01', null, '0', '2018-04-12 07:14:31', '2018-04-12 07:14:31', null);
INSERT INTO `branches` VALUES ('13', '5Qzf53Iz', 'Câu lạc bộ tiếng anh', 'Hà nội', '08765432', 'anh@gmail.com', null, 'ghi chú 1', '9', 'BRANCHES02', null, '1', '2018-04-12 08:10:01', '2018-04-12 08:10:01', null);
INSERT INTO `branches` VALUES ('14', 'ekjxviPq', 'Câu lạc bộ tiếng hàn', null, '098765433456', 'anh@gmail.com', null, null, '9', 'BRANCHES02', null, '1', '2018-04-12 10:39:31', '2018-04-13 04:36:25', '2018-04-13 04:36:25');
INSERT INTO `branches` VALUES ('16', 'ma100', 'Văn phòng HN', 'hn', '098765432', '-#$abc@xyz.xxx', null, null, null, null, '/offices_20180413035916', '1', '2018-04-13 03:58:36', '2018-04-13 03:59:16', null);
INSERT INTO `branches` VALUES ('17', 'maDN', 'Văn phòng HN', 'hn', '123456789', 'a@gmai.com', null, null, null, null, '/offices_20180413041401', '1', '2018-04-13 04:14:01', '2018-04-13 04:14:01', null);
INSERT INTO `branches` VALUES ('19', 'maDN', 'Minh', 'hn', '087654', 'anh@gmail.com', null, null, null, null, '/offices_20180413043402', '1', '2018-04-13 04:34:02', '2018-04-13 04:34:02', null);
INSERT INTO `branches` VALUES ('20', 'CLB02', 'Câu lạc bộ tiếng anh', 'hn', '08765432', 'anh@gmail.com', null, null, '2', 'BRANCHES02', null, '1', '2018-04-13 04:48:15', '2018-04-13 04:48:15', null);
INSERT INTO `branches` VALUES ('21', 'CLB02', 'Câu lạc bộ tiếng anh', 'hn', '08765432', 'anh@gmail.com', null, null, '2', 'BRANCHES02', null, '1', '2018-04-13 04:48:40', '2018-04-13 04:48:40', null);
INSERT INTO `branches` VALUES ('22', 'CLB02', 'Câu lạc bộ tiếng anh', 'hn', '08765432', 'anh@gmail.com', null, null, '2', 'BRANCHES02', null, '1', '2018-04-13 04:48:40', '2018-04-13 04:48:40', null);
INSERT INTO `branches` VALUES ('23', 'ma011', 'Minh', null, '08765432', 'a@gmai.com', null, null, '16', 'BRANCHES01', null, '1', '2018-04-13 05:13:19', '2018-04-13 05:13:19', null);
INSERT INTO `branches` VALUES ('24', '002', 'Minh khang', null, '0987654321', 'anh@gmail.com', null, null, '17', 'BRANCHES01', null, '1', '2018-04-13 05:13:45', '2018-04-13 05:13:45', null);
INSERT INTO `branches` VALUES ('25', '003', 'lan anh', 'hn', '08765432', 'a@gmai.com', null, null, '17', 'BRANCHES01', null, '1', '2018-04-13 05:14:17', '2018-04-13 05:14:17', null);
INSERT INTO `branches` VALUES ('26', '004', 'ánh', null, '012345676543', 'a@gmail.com', null, null, '19', 'BRANCHES01', null, '1', '2018-04-13 05:14:39', '2018-04-13 05:14:39', null);
INSERT INTO `branches` VALUES ('27', '005', 'Nguyễn An', 'hn', '012345676543', 'a@gmai.com', null, null, '1', 'BRANCHES01', null, '1', '2018-04-13 05:15:01', '2018-04-13 05:15:01', null);
INSERT INTO `branches` VALUES ('28', '006', 'An', null, '098523456', 'a@gmai.com', null, null, '16', 'BRANCHES01', null, '1', '2018-04-13 05:42:37', '2018-04-13 05:42:37', null);
INSERT INTO `branches` VALUES ('29', '007', 'duy', null, '012345676543', 'a@gmai.com', null, null, '19', 'BRANCHES01', null, '1', '2018-04-13 05:43:00', '2018-04-13 05:43:00', '2018-04-15 22:42:45');

-- ----------------------------
-- Table structure for code
-- ----------------------------
DROP TABLE IF EXISTS `code`;
CREATE TABLE `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` char(10) DEFAULT NULL,
  `current_code` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of code
-- ----------------------------
INSERT INTO `code` VALUES ('1', 'C', '82', '2018-03-13 03:29:20', '2018-04-09 04:53:03');
INSERT INTO `code` VALUES ('2', '3', '19', '2018-03-13 07:37:31', '2018-04-02 16:31:50');
INSERT INTO `code` VALUES ('3', '1', '19', '2018-03-13 08:06:16', '2018-04-02 16:29:10');
INSERT INTO `code` VALUES ('4', '2', '41', '2018-03-13 08:06:47', '2018-04-04 01:35:28');

-- ----------------------------
-- Table structure for educationbranches
-- ----------------------------
DROP TABLE IF EXISTS `educationbranches`;
CREATE TABLE `educationbranches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branchName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of educationbranches
-- ----------------------------
INSERT INTO `educationbranches` VALUES ('1', 'Du lịch', '2017-12-01 00:08:35', '2017-12-01 00:08:35', '1');
INSERT INTO `educationbranches` VALUES ('2', 'Kinh tế/Khoa học xã hội', '2017-12-01 00:08:35', '2017-12-01 00:08:35', '1');
INSERT INTO `educationbranches` VALUES ('3', 'Kỹ thuật/Khoa học tự nhiên', '2017-12-30 00:00:00', '2017-12-30 00:00:00', '1');

-- ----------------------------
-- Table structure for educationdegrees
-- ----------------------------
DROP TABLE IF EXISTS `educationdegrees`;
CREATE TABLE `educationdegrees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `degree` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of educationdegrees
-- ----------------------------
INSERT INTO `educationdegrees` VALUES ('1', 'Trung cấp sửa', '2017-12-01 00:08:35', '2017-12-01 00:08:35', '0');
INSERT INTO `educationdegrees` VALUES ('2', 'Cao đẳng', '2017-12-01 00:08:35', '2017-12-01 00:08:35', '1');
INSERT INTO `educationdegrees` VALUES ('3', 'Đại học', '2017-12-01 09:26:24', '2017-12-30 10:27:30', '1');
INSERT INTO `educationdegrees` VALUES ('4', 'Trên Đại học', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for educations
-- ----------------------------
DROP TABLE IF EXISTS `educations`;
CREATE TABLE `educations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `branchId` int(10) unsigned NOT NULL DEFAULT '1',
  `degreeId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `educations_memberid_foreign` (`memberId`),
  KEY `educations_branchid_foreign` (`branchId`),
  KEY `educations_degreeid_foreign` (`degreeId`),
  KEY `educations_fileid_foreign` (`fileId`),
  CONSTRAINT `educations_branchid_foreign` FOREIGN KEY (`branchId`) REFERENCES `educationbranches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `educations_degreeid_foreign` FOREIGN KEY (`degreeId`) REFERENCES `educationdegrees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `educations_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `educations_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of educations
-- ----------------------------
INSERT INTO `educations` VALUES ('1', '1', '3', '4', null, '2018-03-13 09:53:09', '2018-03-26 23:38:11');
INSERT INTO `educations` VALUES ('2', '2', '3', '4', null, '2018-03-13 09:59:56', '2018-03-22 11:56:20');
INSERT INTO `educations` VALUES ('3', '3', '1', '4', null, '2018-03-13 10:05:04', '2018-03-13 10:05:04');
INSERT INTO `educations` VALUES ('4', '4', '3', '3', null, '2018-03-13 10:10:03', '2018-03-13 10:10:03');
INSERT INTO `educations` VALUES ('5', '5', '2', '4', null, '2018-03-13 10:11:17', '2018-03-13 10:11:17');
INSERT INTO `educations` VALUES ('6', '6', '2', '4', null, '2018-03-13 10:11:40', '2018-03-13 10:11:40');
INSERT INTO `educations` VALUES ('7', '7', '3', '4', null, '2018-03-13 10:14:55', '2018-03-13 10:14:55');
INSERT INTO `educations` VALUES ('8', '8', '1', '2', null, '2018-03-13 10:35:52', '2018-03-13 10:35:52');
INSERT INTO `educations` VALUES ('9', '9', '2', '4', null, '2018-03-13 11:42:24', '2018-03-13 11:42:24');
INSERT INTO `educations` VALUES ('10', '10', '2', '4', null, '2018-03-13 13:57:24', '2018-03-13 13:57:24');
INSERT INTO `educations` VALUES ('11', '11', '2', '2', null, '2018-03-13 14:00:48', '2018-03-13 14:00:48');
INSERT INTO `educations` VALUES ('12', '12', '2', '2', null, '2018-03-13 14:13:06', '2018-03-13 14:13:06');
INSERT INTO `educations` VALUES ('13', '13', '2', '4', null, '2018-03-13 14:16:53', '2018-03-13 14:16:53');
INSERT INTO `educations` VALUES ('14', '14', '2', '3', null, '2018-03-13 14:20:27', '2018-03-13 14:20:27');
INSERT INTO `educations` VALUES ('15', '15', '2', '3', null, '2018-03-13 14:22:15', '2018-03-13 14:22:15');
INSERT INTO `educations` VALUES ('16', '16', '2', '2', null, '2018-03-13 22:29:33', '2018-03-16 15:39:20');
INSERT INTO `educations` VALUES ('17', '17', '1', '3', null, '2018-03-14 09:37:17', '2018-03-19 08:50:33');
INSERT INTO `educations` VALUES ('18', '18', '2', '2', null, '2018-03-14 09:42:54', '2018-03-14 09:42:54');
INSERT INTO `educations` VALUES ('19', '19', '2', '2', null, '2018-03-14 10:31:48', '2018-03-14 10:31:48');
INSERT INTO `educations` VALUES ('20', '20', '2', '2', null, '2018-03-14 10:34:11', '2018-03-14 10:34:11');
INSERT INTO `educations` VALUES ('21', '21', '2', '3', null, '2018-03-14 10:37:23', '2018-03-14 10:37:23');
INSERT INTO `educations` VALUES ('22', '22', '2', '3', null, '2018-03-14 10:40:40', '2018-03-14 10:40:40');
INSERT INTO `educations` VALUES ('23', '23', '2', '2', null, '2018-03-14 10:58:44', '2018-03-14 10:58:44');
INSERT INTO `educations` VALUES ('24', '24', '2', '3', null, '2018-03-14 11:01:34', '2018-03-14 11:01:34');
INSERT INTO `educations` VALUES ('25', '25', '2', '2', null, '2018-03-14 11:03:19', '2018-03-14 11:03:19');
INSERT INTO `educations` VALUES ('26', '26', '2', '3', null, '2018-03-14 11:04:03', '2018-03-22 10:54:29');
INSERT INTO `educations` VALUES ('27', '27', '2', '2', null, '2018-03-14 11:05:08', '2018-03-14 11:05:08');
INSERT INTO `educations` VALUES ('28', '28', '2', '2', null, '2018-03-14 11:08:33', '2018-03-14 11:08:33');
INSERT INTO `educations` VALUES ('29', '29', '2', '3', null, '2018-03-14 11:10:14', '2018-03-14 11:10:14');
INSERT INTO `educations` VALUES ('30', '30', '3', '3', null, '2018-03-14 11:11:30', '2018-03-14 11:11:30');
INSERT INTO `educations` VALUES ('31', '31', '2', '4', null, '2018-03-14 11:12:00', '2018-03-16 16:28:16');
INSERT INTO `educations` VALUES ('32', '32', '2', '3', null, '2018-03-14 11:15:27', '2018-03-14 11:15:27');
INSERT INTO `educations` VALUES ('33', '33', '3', '3', null, '2018-03-14 11:17:53', '2018-03-14 11:17:53');
INSERT INTO `educations` VALUES ('34', '34', '2', '2', null, '2018-03-14 11:20:04', '2018-03-14 11:20:04');
INSERT INTO `educations` VALUES ('35', '35', '1', '3', null, '2018-03-14 11:22:42', '2018-03-14 11:22:42');
INSERT INTO `educations` VALUES ('36', '36', '1', '2', null, '2018-03-14 11:25:18', '2018-03-14 11:25:18');
INSERT INTO `educations` VALUES ('37', '37', '1', '2', null, '2018-03-14 17:29:23', '2018-03-14 17:29:23');
INSERT INTO `educations` VALUES ('38', '38', '2', '2', null, '2018-03-14 17:30:16', '2018-03-14 17:30:16');
INSERT INTO `educations` VALUES ('39', '38', '2', '2', null, '2018-03-14 17:31:14', '2018-03-14 17:31:14');
INSERT INTO `educations` VALUES ('40', '39', '3', '4', null, '2018-03-15 10:53:49', '2018-03-15 10:53:49');
INSERT INTO `educations` VALUES ('41', '40', '2', '2', null, '2018-03-15 11:00:38', '2018-03-15 11:00:38');
INSERT INTO `educations` VALUES ('42', '40', '2', '2', null, '2018-03-15 11:01:06', '2018-03-15 11:01:06');
INSERT INTO `educations` VALUES ('43', '41', '1', '2', null, '2018-03-15 11:29:04', '2018-03-15 11:29:04');
INSERT INTO `educations` VALUES ('44', '42', '1', '2', null, '2018-03-15 11:29:59', '2018-03-15 11:29:59');
INSERT INTO `educations` VALUES ('45', '43', '2', '2', null, '2018-03-15 11:31:40', '2018-03-15 11:31:40');
INSERT INTO `educations` VALUES ('46', '41', '1', '2', null, '2018-03-15 11:32:31', '2018-03-15 11:32:31');
INSERT INTO `educations` VALUES ('47', '44', '2', '2', null, '2018-03-15 11:36:10', '2018-03-15 11:36:10');
INSERT INTO `educations` VALUES ('48', '45', '2', '4', null, '2018-03-15 11:38:59', '2018-03-15 11:38:59');
INSERT INTO `educations` VALUES ('49', '46', '2', '3', null, '2018-03-15 11:39:11', '2018-03-15 11:39:11');
INSERT INTO `educations` VALUES ('50', '47', '2', '4', null, '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `educations` VALUES ('51', '48', '3', '3', null, '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `educations` VALUES ('52', '49', '1', '2', null, '2018-03-15 13:38:43', '2018-03-15 13:38:43');
INSERT INTO `educations` VALUES ('53', '50', '2', '3', null, '2018-03-15 13:44:37', '2018-03-15 13:44:37');
INSERT INTO `educations` VALUES ('54', '51', '1', '4', null, '2018-03-15 13:53:07', '2018-03-15 13:53:07');
INSERT INTO `educations` VALUES ('55', '51', '1', '4', null, '2018-03-15 13:54:19', '2018-03-15 13:54:19');
INSERT INTO `educations` VALUES ('56', '52', '1', '2', null, '2018-03-15 14:30:20', '2018-03-15 14:30:20');
INSERT INTO `educations` VALUES ('57', '53', '2', '4', null, '2018-03-15 14:33:57', '2018-03-16 11:31:30');
INSERT INTO `educations` VALUES ('58', '54', '3', '4', null, '2018-03-15 14:38:02', '2018-03-15 14:38:02');
INSERT INTO `educations` VALUES ('59', '55', '1', '3', null, '2018-03-15 16:18:01', '2018-03-15 16:18:01');
INSERT INTO `educations` VALUES ('60', '55', '1', '3', null, '2018-03-15 16:22:22', '2018-03-15 16:22:22');
INSERT INTO `educations` VALUES ('61', '56', '1', '2', null, '2018-03-15 16:27:16', '2018-03-15 16:27:16');
INSERT INTO `educations` VALUES ('62', '57', '2', '2', null, '2018-03-15 16:27:39', '2018-03-15 16:27:39');
INSERT INTO `educations` VALUES ('63', '58', '1', '2', null, '2018-03-15 16:28:35', '2018-03-15 16:28:35');
INSERT INTO `educations` VALUES ('64', '58', '1', '2', null, '2018-03-15 16:30:48', '2018-03-15 16:30:48');
INSERT INTO `educations` VALUES ('65', '58', '1', '2', null, '2018-03-15 16:31:06', '2018-03-15 16:31:06');
INSERT INTO `educations` VALUES ('66', '58', '1', '2', null, '2018-03-15 16:31:29', '2018-03-15 16:31:29');
INSERT INTO `educations` VALUES ('67', '59', '1', '2', null, '2018-03-16 09:29:02', '2018-03-30 16:24:05');
INSERT INTO `educations` VALUES ('68', '60', '2', '3', null, '2018-03-16 09:36:06', '2018-03-16 09:36:06');
INSERT INTO `educations` VALUES ('69', '61', '1', '2', null, '2018-03-16 11:00:54', '2018-03-16 11:00:54');
INSERT INTO `educations` VALUES ('70', '62', '1', '2', null, '2018-03-16 11:09:11', '2018-03-16 11:09:11');
INSERT INTO `educations` VALUES ('71', '63', '1', '2', null, '2018-03-16 11:34:15', '2018-03-16 11:34:15');
INSERT INTO `educations` VALUES ('72', '64', '3', '4', null, '2018-03-16 11:34:40', '2018-03-16 11:34:40');
INSERT INTO `educations` VALUES ('73', '65', '2', '2', null, '2018-03-16 13:32:45', '2018-03-16 13:32:45');
INSERT INTO `educations` VALUES ('74', '66', '2', '2', null, '2018-03-16 13:33:40', '2018-03-16 13:33:40');
INSERT INTO `educations` VALUES ('75', '67', '2', '2', null, '2018-03-16 13:34:19', '2018-03-16 13:34:19');
INSERT INTO `educations` VALUES ('76', '68', '2', '2', null, '2018-03-16 13:35:12', '2018-03-16 13:35:12');
INSERT INTO `educations` VALUES ('77', '69', '2', '2', null, '2018-03-16 13:39:48', '2018-03-16 13:39:48');
INSERT INTO `educations` VALUES ('78', '70', '2', '2', null, '2018-03-16 13:39:49', '2018-03-16 15:37:33');
INSERT INTO `educations` VALUES ('79', '71', '2', '2', null, '2018-03-16 13:44:56', '2018-03-16 13:44:56');
INSERT INTO `educations` VALUES ('80', '71', '2', '2', null, '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `educations` VALUES ('81', '72', '2', '2', null, '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `educations` VALUES ('82', '71', '2', '2', null, '2018-03-16 13:46:39', '2018-03-16 13:46:39');
INSERT INTO `educations` VALUES ('83', '71', '2', '2', null, '2018-03-16 13:47:45', '2018-03-16 13:47:45');
INSERT INTO `educations` VALUES ('84', '71', '2', '2', null, '2018-03-16 13:48:09', '2018-03-16 13:48:09');
INSERT INTO `educations` VALUES ('85', '73', '2', '2', null, '2018-03-16 14:25:47', '2018-03-16 14:25:47');
INSERT INTO `educations` VALUES ('86', '73', '2', '2', null, '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `educations` VALUES ('87', '73', '2', '2', null, '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `educations` VALUES ('88', '73', '2', '2', null, '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `educations` VALUES ('89', '73', '2', '2', null, '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `educations` VALUES ('90', '73', '2', '2', null, '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `educations` VALUES ('91', '73', '2', '2', null, '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `educations` VALUES ('92', '73', '2', '2', null, '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `educations` VALUES ('93', '73', '2', '2', null, '2018-03-16 14:26:41', '2018-03-16 14:26:41');
INSERT INTO `educations` VALUES ('94', '73', '2', '2', null, '2018-03-16 14:27:04', '2018-03-16 14:27:04');
INSERT INTO `educations` VALUES ('95', '72', '2', '2', null, '2018-03-16 14:28:15', '2018-03-16 14:28:15');
INSERT INTO `educations` VALUES ('96', '74', '2', '2', null, '2018-03-16 14:56:22', '2018-03-16 14:56:22');
INSERT INTO `educations` VALUES ('97', '75', '2', '2', null, '2018-03-16 15:01:23', '2018-03-16 15:01:23');
INSERT INTO `educations` VALUES ('98', '75', '2', '2', null, '2018-03-16 15:01:48', '2018-03-16 15:01:48');
INSERT INTO `educations` VALUES ('99', '76', '2', '2', null, '2018-03-16 15:18:48', '2018-03-16 15:18:48');
INSERT INTO `educations` VALUES ('100', '77', '2', '2', null, '2018-03-16 15:19:23', '2018-03-16 15:19:23');
INSERT INTO `educations` VALUES ('101', '78', '2', '2', null, '2018-03-16 15:24:01', '2018-03-16 15:24:01');
INSERT INTO `educations` VALUES ('102', '79', '1', '2', null, '2018-03-16 16:09:19', '2018-03-16 17:06:28');
INSERT INTO `educations` VALUES ('103', '80', '2', '2', null, '2018-03-16 17:37:11', '2018-03-16 17:37:11');
INSERT INTO `educations` VALUES ('104', '81', '3', '4', null, '2018-03-19 11:37:54', '2018-03-19 11:37:54');
INSERT INTO `educations` VALUES ('105', '81', '3', '4', null, '2018-03-19 11:38:39', '2018-03-19 11:38:39');
INSERT INTO `educations` VALUES ('106', '82', '3', '4', null, '2018-03-19 11:44:32', '2018-03-19 11:44:32');
INSERT INTO `educations` VALUES ('107', '83', '1', '4', null, '2018-03-19 15:42:18', '2018-03-29 08:18:39');
INSERT INTO `educations` VALUES ('108', '84', '2', '3', null, '2018-03-20 08:56:42', '2018-03-20 08:56:42');
INSERT INTO `educations` VALUES ('109', '85', '1', '3', null, '2018-03-20 08:58:48', '2018-03-26 18:55:12');
INSERT INTO `educations` VALUES ('110', '86', '2', '3', null, '2018-03-20 10:22:59', '2018-03-20 10:24:38');
INSERT INTO `educations` VALUES ('111', '87', '2', '2', null, '2018-03-20 11:08:11', '2018-03-22 14:06:52');
INSERT INTO `educations` VALUES ('112', '87', '2', '2', null, '2018-03-20 11:08:36', '2018-03-22 14:06:52');
INSERT INTO `educations` VALUES ('113', '88', '2', '2', null, '2018-03-20 11:39:12', '2018-03-20 11:39:12');
INSERT INTO `educations` VALUES ('114', '89', '1', '2', null, '2018-03-21 10:12:52', '2018-03-21 10:12:52');
INSERT INTO `educations` VALUES ('115', '89', '1', '2', null, '2018-03-21 10:17:18', '2018-03-21 10:17:18');
INSERT INTO `educations` VALUES ('116', '90', '2', '4', null, '2018-03-23 09:45:30', '2018-03-23 09:45:30');
INSERT INTO `educations` VALUES ('117', '91', '2', '2', null, '2018-03-23 09:53:29', '2018-03-23 09:53:29');
INSERT INTO `educations` VALUES ('118', '92', '3', '4', null, '2018-03-23 09:58:34', '2018-03-23 09:58:34');
INSERT INTO `educations` VALUES ('119', '19', '1', '3', null, '2018-03-23 10:06:04', '2018-03-23 10:06:04');
INSERT INTO `educations` VALUES ('120', '93', '2', '2', null, '2018-03-23 10:56:58', '2018-03-23 11:00:39');
INSERT INTO `educations` VALUES ('121', '94', '1', '2', null, '2018-03-23 15:12:15', '2018-03-23 15:12:15');
INSERT INTO `educations` VALUES ('122', '76', '1', '2', null, '2018-03-23 16:11:57', '2018-03-23 16:11:57');
INSERT INTO `educations` VALUES ('123', '21', '1', '2', null, '2018-03-23 16:21:51', '2018-03-23 16:21:51');
INSERT INTO `educations` VALUES ('124', '95', '1', '2', null, '2018-03-26 08:38:00', '2018-03-26 08:38:00');
INSERT INTO `educations` VALUES ('125', '96', '1', '2', null, '2018-03-26 08:43:13', '2018-03-26 08:43:13');
INSERT INTO `educations` VALUES ('126', '97', '1', '2', null, '2018-03-26 09:17:19', '2018-03-26 09:17:19');
INSERT INTO `educations` VALUES ('127', '98', '2', '4', null, '2018-03-26 09:20:07', '2018-03-26 09:20:07');
INSERT INTO `educations` VALUES ('128', '99', '1', '2', null, '2018-03-26 11:52:57', '2018-03-26 11:52:57');
INSERT INTO `educations` VALUES ('129', '100', '1', '2', null, '2018-03-26 16:14:06', '2018-03-26 16:14:06');
INSERT INTO `educations` VALUES ('130', '101', '1', '2', null, '2018-03-26 16:16:00', '2018-03-26 16:16:00');
INSERT INTO `educations` VALUES ('131', '102', '1', '2', null, '2018-03-26 16:16:53', '2018-03-26 16:16:53');
INSERT INTO `educations` VALUES ('132', '103', '1', '2', null, '2018-03-26 16:18:00', '2018-03-26 16:18:00');
INSERT INTO `educations` VALUES ('133', '104', '1', '2', null, '2018-03-26 17:03:03', '2018-03-26 17:03:03');
INSERT INTO `educations` VALUES ('134', '105', '1', '2', null, '2018-03-26 17:13:08', '2018-03-26 17:13:08');
INSERT INTO `educations` VALUES ('135', '106', '2', '2', null, '2018-03-26 17:19:54', '2018-03-26 17:19:54');
INSERT INTO `educations` VALUES ('136', '107', '1', '2', null, '2018-03-26 17:20:39', '2018-03-26 17:20:39');
INSERT INTO `educations` VALUES ('137', '108', '2', '2', null, '2018-03-26 17:20:58', '2018-03-26 17:20:58');
INSERT INTO `educations` VALUES ('138', '109', '1', '2', null, '2018-03-26 17:21:02', '2018-03-26 17:21:02');
INSERT INTO `educations` VALUES ('139', '110', '3', '4', null, '2018-03-26 17:22:31', '2018-03-26 17:22:31');
INSERT INTO `educations` VALUES ('140', '111', '2', '4', null, '2018-03-26 17:25:28', '2018-03-26 17:25:28');
INSERT INTO `educations` VALUES ('141', '112', '2', '2', null, '2018-03-26 17:28:20', '2018-03-26 17:28:20');
INSERT INTO `educations` VALUES ('142', '113', '1', '2', null, '2018-03-27 08:20:51', '2018-03-27 08:20:51');
INSERT INTO `educations` VALUES ('143', '114', '1', '2', null, '2018-03-27 10:05:59', '2018-03-27 10:05:59');
INSERT INTO `educations` VALUES ('144', '115', '1', '2', null, '2018-03-27 10:15:49', '2018-03-27 10:15:49');
INSERT INTO `educations` VALUES ('145', '116', '1', '2', null, '2018-03-27 14:00:43', '2018-03-27 14:00:43');
INSERT INTO `educations` VALUES ('146', '117', '1', '2', null, '2018-03-27 14:28:04', '2018-03-27 14:42:50');
INSERT INTO `educations` VALUES ('147', '118', '1', '2', null, '2018-03-27 15:04:04', '2018-03-27 15:19:17');
INSERT INTO `educations` VALUES ('148', '119', '1', '2', null, '2018-03-27 15:36:31', '2018-03-27 15:36:31');
INSERT INTO `educations` VALUES ('149', '120', '1', '2', null, '2018-03-27 15:58:31', '2018-03-27 15:58:31');
INSERT INTO `educations` VALUES ('150', '121', '1', '2', null, '2018-03-27 16:00:55', '2018-03-27 16:00:55');
INSERT INTO `educations` VALUES ('151', '122', '1', '2', null, '2018-03-27 16:26:24', '2018-03-27 16:26:24');
INSERT INTO `educations` VALUES ('152', '123', '1', '2', null, '2018-03-27 16:56:59', '2018-03-28 08:33:33');
INSERT INTO `educations` VALUES ('153', '124', '3', '4', null, '2018-03-28 00:02:53', '2018-03-28 00:02:53');
INSERT INTO `educations` VALUES ('154', '125', '3', '3', null, '2018-03-28 00:06:48', '2018-03-28 00:06:48');
INSERT INTO `educations` VALUES ('155', '126', '2', '2', null, '2018-03-28 09:37:08', '2018-03-28 09:37:08');
INSERT INTO `educations` VALUES ('156', '127', '2', '2', null, '2018-03-28 09:38:41', '2018-03-28 09:38:41');
INSERT INTO `educations` VALUES ('157', '128', '2', '2', null, '2018-03-28 09:42:45', '2018-03-28 09:42:45');
INSERT INTO `educations` VALUES ('158', '129', '3', '2', null, '2018-03-28 10:17:52', '2018-03-28 10:22:15');
INSERT INTO `educations` VALUES ('159', '130', '1', '3', null, '2018-03-28 10:29:01', '2018-03-28 10:43:25');
INSERT INTO `educations` VALUES ('160', '131', '2', '2', null, '2018-03-28 15:12:11', '2018-03-28 15:12:11');
INSERT INTO `educations` VALUES ('161', '132', '1', '2', null, '2018-03-28 16:02:10', '2018-03-28 16:09:46');
INSERT INTO `educations` VALUES ('162', '133', '1', '2', null, '2018-03-28 16:13:45', '2018-03-28 16:13:45');
INSERT INTO `educations` VALUES ('163', '134', '1', '2', null, '2018-03-28 16:28:12', '2018-03-28 17:52:58');
INSERT INTO `educations` VALUES ('164', '135', '3', '3', null, '2018-03-28 17:28:50', '2018-03-28 17:28:50');
INSERT INTO `educations` VALUES ('165', '136', '1', '2', null, '2018-03-28 17:33:04', '2018-03-28 17:33:04');
INSERT INTO `educations` VALUES ('166', '137', '2', '4', null, '2018-03-28 17:34:48', '2018-03-28 17:34:48');
INSERT INTO `educations` VALUES ('167', '138', '2', '3', null, '2018-03-29 09:53:54', '2018-03-29 09:53:54');
INSERT INTO `educations` VALUES ('168', '139', '1', '2', null, '2018-03-29 10:10:07', '2018-03-29 10:10:07');
INSERT INTO `educations` VALUES ('169', '140', '1', '3', null, '2018-03-29 10:13:19', '2018-03-29 10:13:19');
INSERT INTO `educations` VALUES ('170', '141', '1', '2', null, '2018-03-30 08:39:58', '2018-03-30 08:39:58');
INSERT INTO `educations` VALUES ('171', '142', '1', '2', null, '2018-03-30 10:56:43', '2018-03-30 10:56:43');
INSERT INTO `educations` VALUES ('172', '143', '1', '4', null, '2018-03-30 10:59:39', '2018-03-30 10:59:39');
INSERT INTO `educations` VALUES ('173', '144', '1', '3', null, '2018-03-30 14:12:02', '2018-03-30 14:12:02');
INSERT INTO `educations` VALUES ('174', '145', '1', '2', null, '2018-03-30 15:50:44', '2018-03-30 15:50:44');
INSERT INTO `educations` VALUES ('175', '146', '1', '2', null, '2018-03-30 16:47:34', '2018-03-30 16:47:34');
INSERT INTO `educations` VALUES ('176', '147', '1', '2', null, '2018-03-30 23:38:33', '2018-03-30 23:38:33');
INSERT INTO `educations` VALUES ('177', '148', '1', '2', null, '2018-03-30 23:39:40', '2018-03-30 23:39:40');
INSERT INTO `educations` VALUES ('178', '149', '1', '2', null, '2018-03-30 23:52:48', '2018-03-30 23:52:48');
INSERT INTO `educations` VALUES ('179', '150', '1', '2', null, '2018-04-02 08:58:00', '2018-04-02 08:58:00');
INSERT INTO `educations` VALUES ('180', '151', '1', '2', null, '2018-04-02 09:59:33', '2018-04-02 09:59:33');
INSERT INTO `educations` VALUES ('181', '152', '1', '2', null, '2018-04-02 14:13:53', '2018-04-02 14:13:53');
INSERT INTO `educations` VALUES ('182', '153', '1', '2', null, '2018-04-02 14:15:20', '2018-04-02 14:15:20');
INSERT INTO `educations` VALUES ('183', '154', '1', '2', null, '2018-04-02 14:26:32', '2018-04-02 14:26:32');
INSERT INTO `educations` VALUES ('184', '155', '1', '2', null, '2018-04-02 15:03:40', '2018-04-02 15:03:40');
INSERT INTO `educations` VALUES ('185', '156', '1', '2', null, '2018-04-02 15:04:58', '2018-04-02 15:04:58');
INSERT INTO `educations` VALUES ('186', '157', '1', '3', null, '2018-04-02 15:22:09', '2018-04-02 15:22:09');
INSERT INTO `educations` VALUES ('187', '158', '1', '2', null, '2018-04-02 16:17:49', '2018-04-02 16:17:49');
INSERT INTO `educations` VALUES ('188', '159', '1', '2', null, '2018-04-02 16:18:05', '2018-04-02 16:18:05');
INSERT INTO `educations` VALUES ('189', '160', '1', '2', null, '2018-04-04 01:23:17', '2018-04-04 01:23:17');
INSERT INTO `educations` VALUES ('190', '161', '2', '3', null, '2018-04-05 02:24:04', '2018-04-05 02:24:04');
INSERT INTO `educations` VALUES ('191', '162', '1', '4', null, '2018-04-07 07:40:07', '2018-04-07 07:40:07');

-- ----------------------------
-- Table structure for elements
-- ----------------------------
DROP TABLE IF EXISTS `elements`;
CREATE TABLE `elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `typeGuideId` int(10) unsigned NOT NULL,
  `groupSizeId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `elements_memberid_foreign` (`memberId`),
  KEY `elements_typeguideid_foreign` (`typeGuideId`),
  KEY `elements_groupsizeid_foreign` (`groupSizeId`),
  CONSTRAINT `elements_groupsizeid_foreign` FOREIGN KEY (`groupSizeId`) REFERENCES `groupsizes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `elements_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `elements_typeguideid_foreign` FOREIGN KEY (`typeGuideId`) REFERENCES `typeguides` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of elements
-- ----------------------------
INSERT INTO `elements` VALUES ('1', '1', '7', '5', '2018-03-13 09:53:09', '2018-03-26 23:38:11');
INSERT INTO `elements` VALUES ('2', '2', '7', '5', '2018-03-13 09:59:56', '2018-03-22 11:56:20');
INSERT INTO `elements` VALUES ('3', '3', '3', '5', '2018-03-13 10:05:04', '2018-03-13 10:05:04');
INSERT INTO `elements` VALUES ('4', '4', '5', '4', '2018-03-13 10:10:03', '2018-03-13 10:10:03');
INSERT INTO `elements` VALUES ('5', '5', '7', '5', '2018-03-13 10:11:17', '2018-03-13 10:11:17');
INSERT INTO `elements` VALUES ('6', '6', '6', '2', '2018-03-13 10:11:40', '2018-03-13 10:11:40');
INSERT INTO `elements` VALUES ('7', '7', '7', '4', '2018-03-13 10:14:55', '2018-03-13 10:14:55');
INSERT INTO `elements` VALUES ('8', '8', '8', '2', '2018-03-13 10:35:52', '2018-03-13 10:35:52');
INSERT INTO `elements` VALUES ('9', '9', '7', '3', '2018-03-13 11:42:24', '2018-03-13 11:42:24');
INSERT INTO `elements` VALUES ('10', '10', '7', '4', '2018-03-13 13:57:24', '2018-03-13 13:57:24');
INSERT INTO `elements` VALUES ('11', '11', '4', '2', '2018-03-13 14:00:48', '2018-03-13 14:00:48');
INSERT INTO `elements` VALUES ('12', '12', '6', '3', '2018-03-13 14:13:06', '2018-03-13 14:13:06');
INSERT INTO `elements` VALUES ('13', '13', '7', '4', '2018-03-13 14:16:53', '2018-03-13 14:16:53');
INSERT INTO `elements` VALUES ('14', '14', '6', '3', '2018-03-13 14:20:27', '2018-03-13 14:20:27');
INSERT INTO `elements` VALUES ('15', '15', '6', '4', '2018-03-13 14:22:15', '2018-03-13 14:22:15');
INSERT INTO `elements` VALUES ('16', '16', '3', '4', '2018-03-13 22:29:33', '2018-03-16 15:39:20');
INSERT INTO `elements` VALUES ('17', '17', '2', '4', '2018-03-14 09:37:17', '2018-03-19 08:50:33');
INSERT INTO `elements` VALUES ('18', '18', '7', '2', '2018-03-14 09:42:54', '2018-03-14 09:42:54');
INSERT INTO `elements` VALUES ('19', '19', '3', '2', '2018-03-14 10:31:48', '2018-03-14 10:31:48');
INSERT INTO `elements` VALUES ('20', '20', '7', '2', '2018-03-14 10:34:11', '2018-03-14 10:34:11');
INSERT INTO `elements` VALUES ('21', '21', '7', '2', '2018-03-14 10:37:23', '2018-03-14 10:37:23');
INSERT INTO `elements` VALUES ('22', '22', '5', '4', '2018-03-14 10:40:40', '2018-03-14 10:40:40');
INSERT INTO `elements` VALUES ('23', '23', '2', '2', '2018-03-14 10:58:44', '2018-03-14 10:58:44');
INSERT INTO `elements` VALUES ('24', '24', '2', '4', '2018-03-14 11:01:34', '2018-03-14 11:01:34');
INSERT INTO `elements` VALUES ('25', '25', '7', '2', '2018-03-14 11:03:19', '2018-03-14 11:03:19');
INSERT INTO `elements` VALUES ('26', '26', '6', '5', '2018-03-14 11:04:03', '2018-03-22 10:54:29');
INSERT INTO `elements` VALUES ('27', '27', '7', '2', '2018-03-14 11:05:08', '2018-03-14 11:05:08');
INSERT INTO `elements` VALUES ('28', '28', '3', '3', '2018-03-14 11:08:33', '2018-03-14 11:08:33');
INSERT INTO `elements` VALUES ('29', '29', '2', '2', '2018-03-14 11:10:14', '2018-03-14 11:10:14');
INSERT INTO `elements` VALUES ('30', '30', '6', '5', '2018-03-14 11:11:30', '2018-03-14 11:11:30');
INSERT INTO `elements` VALUES ('31', '31', '8', '5', '2018-03-14 11:12:00', '2018-03-16 16:28:16');
INSERT INTO `elements` VALUES ('32', '32', '6', '5', '2018-03-14 11:15:27', '2018-03-14 11:15:27');
INSERT INTO `elements` VALUES ('33', '33', '6', '5', '2018-03-14 11:17:53', '2018-03-14 11:17:53');
INSERT INTO `elements` VALUES ('34', '34', '5', '2', '2018-03-14 11:20:04', '2018-03-14 11:20:04');
INSERT INTO `elements` VALUES ('35', '35', '5', '2', '2018-03-14 11:22:42', '2018-03-14 11:22:42');
INSERT INTO `elements` VALUES ('36', '36', '5', '4', '2018-03-14 11:25:18', '2018-03-14 11:25:18');
INSERT INTO `elements` VALUES ('37', '37', '5', '2', '2018-03-14 17:29:23', '2018-03-14 17:29:23');
INSERT INTO `elements` VALUES ('38', '38', '2', '3', '2018-03-14 17:30:16', '2018-03-14 17:30:16');
INSERT INTO `elements` VALUES ('39', '38', '2', '3', '2018-03-14 17:31:14', '2018-03-14 17:31:14');
INSERT INTO `elements` VALUES ('40', '39', '4', '4', '2018-03-15 10:53:49', '2018-03-15 10:53:49');
INSERT INTO `elements` VALUES ('41', '40', '5', '3', '2018-03-15 11:00:38', '2018-03-15 11:00:38');
INSERT INTO `elements` VALUES ('42', '40', '5', '3', '2018-03-15 11:01:06', '2018-03-15 11:01:06');
INSERT INTO `elements` VALUES ('43', '41', '7', '4', '2018-03-15 11:29:04', '2018-03-15 11:29:04');
INSERT INTO `elements` VALUES ('44', '42', '8', '5', '2018-03-15 11:29:59', '2018-03-15 11:29:59');
INSERT INTO `elements` VALUES ('45', '43', '2', '3', '2018-03-15 11:31:40', '2018-03-15 11:31:40');
INSERT INTO `elements` VALUES ('46', '41', '7', '4', '2018-03-15 11:32:31', '2018-03-15 11:32:31');
INSERT INTO `elements` VALUES ('47', '44', '7', '3', '2018-03-15 11:36:10', '2018-03-15 11:36:10');
INSERT INTO `elements` VALUES ('48', '45', '2', '2', '2018-03-15 11:38:59', '2018-03-15 11:38:59');
INSERT INTO `elements` VALUES ('49', '46', '2', '2', '2018-03-15 11:39:11', '2018-03-15 11:39:11');
INSERT INTO `elements` VALUES ('50', '47', '3', '4', '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `elements` VALUES ('51', '48', '5', '2', '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `elements` VALUES ('52', '49', '6', '3', '2018-03-15 13:38:43', '2018-03-15 13:38:43');
INSERT INTO `elements` VALUES ('53', '50', '3', '5', '2018-03-15 13:44:37', '2018-03-15 13:44:37');
INSERT INTO `elements` VALUES ('54', '51', '7', '5', '2018-03-15 13:53:07', '2018-03-15 13:53:07');
INSERT INTO `elements` VALUES ('55', '51', '7', '5', '2018-03-15 13:54:19', '2018-03-15 13:54:19');
INSERT INTO `elements` VALUES ('56', '52', '7', '3', '2018-03-15 14:30:20', '2018-03-15 14:30:20');
INSERT INTO `elements` VALUES ('57', '53', '2', '5', '2018-03-15 14:33:57', '2018-03-16 11:31:30');
INSERT INTO `elements` VALUES ('58', '54', '8', '3', '2018-03-15 14:38:02', '2018-03-15 14:38:02');
INSERT INTO `elements` VALUES ('59', '55', '5', '4', '2018-03-15 16:18:01', '2018-03-15 16:18:01');
INSERT INTO `elements` VALUES ('60', '55', '5', '4', '2018-03-15 16:22:22', '2018-03-15 16:22:22');
INSERT INTO `elements` VALUES ('61', '56', '5', '3', '2018-03-15 16:27:16', '2018-03-15 16:27:16');
INSERT INTO `elements` VALUES ('62', '57', '2', '2', '2018-03-15 16:27:39', '2018-03-15 16:27:39');
INSERT INTO `elements` VALUES ('63', '58', '5', '3', '2018-03-15 16:28:35', '2018-03-15 16:28:35');
INSERT INTO `elements` VALUES ('64', '58', '5', '3', '2018-03-15 16:30:48', '2018-03-15 16:30:48');
INSERT INTO `elements` VALUES ('65', '58', '5', '3', '2018-03-15 16:31:06', '2018-03-15 16:31:06');
INSERT INTO `elements` VALUES ('66', '58', '5', '3', '2018-03-15 16:31:29', '2018-03-15 16:31:29');
INSERT INTO `elements` VALUES ('67', '59', '7', '4', '2018-03-16 09:29:02', '2018-03-30 16:24:05');
INSERT INTO `elements` VALUES ('68', '60', '2', '3', '2018-03-16 09:36:06', '2018-03-16 09:36:06');
INSERT INTO `elements` VALUES ('69', '61', '7', '4', '2018-03-16 11:00:54', '2018-03-16 11:00:54');
INSERT INTO `elements` VALUES ('70', '62', '7', '4', '2018-03-16 11:09:11', '2018-03-16 11:09:11');
INSERT INTO `elements` VALUES ('71', '63', '7', '4', '2018-03-16 11:34:15', '2018-03-16 11:34:15');
INSERT INTO `elements` VALUES ('72', '64', '5', '4', '2018-03-16 11:34:40', '2018-03-16 11:34:40');
INSERT INTO `elements` VALUES ('73', '65', '6', '2', '2018-03-16 13:32:45', '2018-03-16 13:32:45');
INSERT INTO `elements` VALUES ('74', '66', '6', '2', '2018-03-16 13:33:40', '2018-03-16 13:33:40');
INSERT INTO `elements` VALUES ('75', '67', '6', '2', '2018-03-16 13:34:19', '2018-03-16 13:34:19');
INSERT INTO `elements` VALUES ('76', '68', '6', '2', '2018-03-16 13:35:12', '2018-03-16 13:35:12');
INSERT INTO `elements` VALUES ('77', '69', '2', '2', '2018-03-16 13:39:48', '2018-03-16 13:39:48');
INSERT INTO `elements` VALUES ('78', '70', '6', '2', '2018-03-16 13:39:49', '2018-03-16 15:37:33');
INSERT INTO `elements` VALUES ('79', '71', '4', '5', '2018-03-16 13:44:56', '2018-03-16 13:44:56');
INSERT INTO `elements` VALUES ('80', '71', '4', '5', '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `elements` VALUES ('81', '72', '6', '2', '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `elements` VALUES ('82', '71', '4', '5', '2018-03-16 13:46:39', '2018-03-16 13:46:39');
INSERT INTO `elements` VALUES ('83', '71', '4', '5', '2018-03-16 13:47:45', '2018-03-16 13:47:45');
INSERT INTO `elements` VALUES ('84', '71', '4', '5', '2018-03-16 13:48:09', '2018-03-16 13:48:09');
INSERT INTO `elements` VALUES ('85', '73', '6', '5', '2018-03-16 14:25:47', '2018-03-16 14:25:47');
INSERT INTO `elements` VALUES ('86', '73', '6', '5', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `elements` VALUES ('87', '73', '6', '5', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `elements` VALUES ('88', '73', '6', '5', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `elements` VALUES ('89', '73', '6', '5', '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `elements` VALUES ('90', '73', '6', '5', '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `elements` VALUES ('91', '73', '6', '5', '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `elements` VALUES ('92', '73', '6', '5', '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `elements` VALUES ('93', '73', '6', '5', '2018-03-16 14:26:41', '2018-03-16 14:26:41');
INSERT INTO `elements` VALUES ('94', '73', '6', '5', '2018-03-16 14:27:04', '2018-03-16 14:27:04');
INSERT INTO `elements` VALUES ('95', '72', '6', '2', '2018-03-16 14:28:15', '2018-03-16 14:28:15');
INSERT INTO `elements` VALUES ('96', '74', '6', '2', '2018-03-16 14:56:22', '2018-03-16 14:56:22');
INSERT INTO `elements` VALUES ('97', '75', '6', '2', '2018-03-16 15:01:23', '2018-03-16 15:01:23');
INSERT INTO `elements` VALUES ('98', '75', '6', '2', '2018-03-16 15:01:48', '2018-03-16 15:01:48');
INSERT INTO `elements` VALUES ('99', '76', '6', '2', '2018-03-16 15:18:48', '2018-03-16 15:18:48');
INSERT INTO `elements` VALUES ('100', '77', '6', '2', '2018-03-16 15:19:24', '2018-03-16 15:19:24');
INSERT INTO `elements` VALUES ('101', '78', '6', '2', '2018-03-16 15:24:01', '2018-03-16 15:24:01');
INSERT INTO `elements` VALUES ('102', '79', '8', '5', '2018-03-16 16:09:19', '2018-03-16 17:06:28');
INSERT INTO `elements` VALUES ('103', '80', '2', '2', '2018-03-16 17:37:11', '2018-03-16 17:37:11');
INSERT INTO `elements` VALUES ('104', '81', '7', '5', '2018-03-19 11:37:54', '2018-03-19 11:37:54');
INSERT INTO `elements` VALUES ('105', '81', '7', '5', '2018-03-19 11:38:39', '2018-03-19 11:38:39');
INSERT INTO `elements` VALUES ('106', '82', '7', '5', '2018-03-19 11:44:32', '2018-03-19 11:44:32');
INSERT INTO `elements` VALUES ('107', '83', '3', '5', '2018-03-19 15:42:18', '2018-03-29 08:18:39');
INSERT INTO `elements` VALUES ('108', '84', '3', '3', '2018-03-20 08:56:42', '2018-03-20 08:56:42');
INSERT INTO `elements` VALUES ('109', '85', '7', '3', '2018-03-20 08:58:48', '2018-03-26 18:55:12');
INSERT INTO `elements` VALUES ('110', '86', '6', '2', '2018-03-20 10:22:59', '2018-03-20 10:24:38');
INSERT INTO `elements` VALUES ('111', '87', '5', '2', '2018-03-20 11:08:11', '2018-03-22 14:06:52');
INSERT INTO `elements` VALUES ('112', '87', '5', '2', '2018-03-20 11:08:36', '2018-03-22 14:06:52');
INSERT INTO `elements` VALUES ('113', '88', '5', '2', '2018-03-20 11:39:12', '2018-03-20 11:39:12');
INSERT INTO `elements` VALUES ('114', '89', '5', '5', '2018-03-21 10:12:52', '2018-03-21 10:12:52');
INSERT INTO `elements` VALUES ('115', '89', '5', '5', '2018-03-21 10:17:18', '2018-03-21 10:17:18');
INSERT INTO `elements` VALUES ('116', '90', '6', '3', '2018-03-23 09:45:30', '2018-03-23 09:45:30');
INSERT INTO `elements` VALUES ('117', '91', '5', '5', '2018-03-23 09:53:29', '2018-03-23 09:53:29');
INSERT INTO `elements` VALUES ('118', '92', '2', '5', '2018-03-23 09:58:34', '2018-03-23 09:58:34');
INSERT INTO `elements` VALUES ('119', '19', '6', '5', '2018-03-23 10:06:04', '2018-03-23 10:06:04');
INSERT INTO `elements` VALUES ('120', '93', '6', '5', '2018-03-23 10:56:58', '2018-03-23 11:00:39');
INSERT INTO `elements` VALUES ('121', '94', '2', '5', '2018-03-23 15:12:15', '2018-03-23 15:12:15');
INSERT INTO `elements` VALUES ('122', '76', '3', '5', '2018-03-23 16:11:57', '2018-03-23 16:11:57');
INSERT INTO `elements` VALUES ('123', '21', '3', '5', '2018-03-23 16:21:51', '2018-03-23 16:21:51');
INSERT INTO `elements` VALUES ('124', '95', '7', '3', '2018-03-26 08:38:00', '2018-03-26 08:38:00');
INSERT INTO `elements` VALUES ('125', '96', '7', '3', '2018-03-26 08:43:13', '2018-03-26 08:43:13');
INSERT INTO `elements` VALUES ('126', '97', '5', '3', '2018-03-26 09:17:19', '2018-03-26 09:17:19');
INSERT INTO `elements` VALUES ('127', '98', '7', '3', '2018-03-26 09:20:07', '2018-03-26 09:20:07');
INSERT INTO `elements` VALUES ('128', '99', '7', '2', '2018-03-26 11:52:57', '2018-03-26 11:52:57');
INSERT INTO `elements` VALUES ('129', '100', '8', '3', '2018-03-26 16:14:06', '2018-03-26 16:14:06');
INSERT INTO `elements` VALUES ('130', '101', '8', '3', '2018-03-26 16:16:00', '2018-03-26 16:16:00');
INSERT INTO `elements` VALUES ('131', '102', '8', '3', '2018-03-26 16:16:53', '2018-03-26 16:16:53');
INSERT INTO `elements` VALUES ('132', '103', '8', '3', '2018-03-26 16:18:00', '2018-03-26 16:18:00');
INSERT INTO `elements` VALUES ('133', '104', '4', '4', '2018-03-26 17:03:03', '2018-03-26 17:03:03');
INSERT INTO `elements` VALUES ('134', '105', '4', '4', '2018-03-26 17:13:08', '2018-03-26 17:13:08');
INSERT INTO `elements` VALUES ('135', '106', '5', '3', '2018-03-26 17:19:54', '2018-03-26 17:19:54');
INSERT INTO `elements` VALUES ('136', '107', '5', '3', '2018-03-26 17:20:39', '2018-03-26 17:20:39');
INSERT INTO `elements` VALUES ('137', '108', '5', '3', '2018-03-26 17:20:58', '2018-03-26 17:20:58');
INSERT INTO `elements` VALUES ('138', '109', '5', '3', '2018-03-26 17:21:02', '2018-03-26 17:21:02');
INSERT INTO `elements` VALUES ('139', '110', '8', '5', '2018-03-26 17:22:31', '2018-03-26 17:22:31');
INSERT INTO `elements` VALUES ('140', '111', '4', '4', '2018-03-26 17:25:28', '2018-03-26 17:25:28');
INSERT INTO `elements` VALUES ('141', '112', '5', '2', '2018-03-26 17:28:20', '2018-03-26 17:28:20');
INSERT INTO `elements` VALUES ('142', '113', '5', '4', '2018-03-27 08:20:51', '2018-03-27 08:20:51');
INSERT INTO `elements` VALUES ('143', '114', '6', '2', '2018-03-27 10:05:59', '2018-03-27 10:05:59');
INSERT INTO `elements` VALUES ('144', '115', '3', '2', '2018-03-27 10:15:49', '2018-03-27 10:15:49');
INSERT INTO `elements` VALUES ('145', '116', '5', '3', '2018-03-27 14:00:43', '2018-03-27 14:00:43');
INSERT INTO `elements` VALUES ('146', '117', '5', '2', '2018-03-27 14:28:04', '2018-03-27 14:42:50');
INSERT INTO `elements` VALUES ('147', '118', '5', '2', '2018-03-27 15:04:04', '2018-03-27 15:19:18');
INSERT INTO `elements` VALUES ('148', '119', '5', '2', '2018-03-27 15:36:31', '2018-03-27 15:36:31');
INSERT INTO `elements` VALUES ('149', '120', '5', '2', '2018-03-27 15:58:31', '2018-03-27 15:58:31');
INSERT INTO `elements` VALUES ('150', '121', '5', '2', '2018-03-27 16:00:55', '2018-03-27 16:00:55');
INSERT INTO `elements` VALUES ('151', '122', '5', '2', '2018-03-27 16:26:24', '2018-03-27 16:26:24');
INSERT INTO `elements` VALUES ('152', '123', '5', '2', '2018-03-27 16:56:59', '2018-03-28 08:33:33');
INSERT INTO `elements` VALUES ('153', '124', '4', '3', '2018-03-28 00:02:53', '2018-03-28 00:02:53');
INSERT INTO `elements` VALUES ('154', '125', '2', '3', '2018-03-28 00:06:48', '2018-03-28 00:06:48');
INSERT INTO `elements` VALUES ('155', '126', '5', '2', '2018-03-28 09:37:08', '2018-03-28 09:37:08');
INSERT INTO `elements` VALUES ('156', '127', '5', '2', '2018-03-28 09:38:41', '2018-03-28 09:38:41');
INSERT INTO `elements` VALUES ('157', '128', '4', '2', '2018-03-28 09:42:45', '2018-03-28 09:42:45');
INSERT INTO `elements` VALUES ('158', '129', '5', '2', '2018-03-28 10:17:52', '2018-03-28 10:22:15');
INSERT INTO `elements` VALUES ('159', '130', '7', '3', '2018-03-28 10:29:01', '2018-03-28 10:43:25');
INSERT INTO `elements` VALUES ('160', '131', '5', '2', '2018-03-28 15:12:11', '2018-03-28 15:12:11');
INSERT INTO `elements` VALUES ('161', '132', '5', '2', '2018-03-28 16:02:10', '2018-03-28 16:09:46');
INSERT INTO `elements` VALUES ('162', '133', '5', '2', '2018-03-28 16:13:45', '2018-03-28 16:13:45');
INSERT INTO `elements` VALUES ('163', '134', '5', '5', '2018-03-28 16:28:12', '2018-03-28 17:52:58');
INSERT INTO `elements` VALUES ('164', '135', '6', '2', '2018-03-28 17:28:50', '2018-03-28 17:28:50');
INSERT INTO `elements` VALUES ('165', '136', '4', '2', '2018-03-28 17:33:04', '2018-03-28 17:33:04');
INSERT INTO `elements` VALUES ('166', '137', '2', '3', '2018-03-28 17:34:48', '2018-03-28 17:34:48');
INSERT INTO `elements` VALUES ('167', '138', '5', '2', '2018-03-29 09:53:54', '2018-03-29 09:53:54');
INSERT INTO `elements` VALUES ('168', '139', '5', '2', '2018-03-29 10:10:07', '2018-03-29 10:10:07');
INSERT INTO `elements` VALUES ('169', '140', '5', '2', '2018-03-29 10:13:19', '2018-03-29 10:13:19');
INSERT INTO `elements` VALUES ('170', '141', '7', '2', '2018-03-30 08:39:58', '2018-03-30 08:39:58');
INSERT INTO `elements` VALUES ('171', '142', '4', '4', '2018-03-30 10:56:43', '2018-03-30 10:56:43');
INSERT INTO `elements` VALUES ('172', '143', '5', '2', '2018-03-30 10:59:39', '2018-03-30 10:59:39');
INSERT INTO `elements` VALUES ('173', '144', '6', '3', '2018-03-30 14:12:02', '2018-03-30 14:12:02');
INSERT INTO `elements` VALUES ('174', '145', '2', '2', '2018-03-30 15:50:44', '2018-03-30 15:50:44');
INSERT INTO `elements` VALUES ('175', '146', '6', '2', '2018-03-30 16:47:34', '2018-03-30 16:47:34');
INSERT INTO `elements` VALUES ('176', '147', '5', '2', '2018-03-30 23:38:33', '2018-03-30 23:38:33');
INSERT INTO `elements` VALUES ('177', '148', '5', '2', '2018-03-30 23:39:40', '2018-03-30 23:39:40');
INSERT INTO `elements` VALUES ('178', '149', '5', '2', '2018-03-30 23:52:48', '2018-03-30 23:52:48');
INSERT INTO `elements` VALUES ('179', '150', '2', '5', '2018-04-02 08:58:00', '2018-04-02 08:58:00');
INSERT INTO `elements` VALUES ('180', '151', '7', '2', '2018-04-02 09:59:33', '2018-04-02 09:59:33');
INSERT INTO `elements` VALUES ('181', '152', '5', '2', '2018-04-02 14:13:53', '2018-04-02 14:13:53');
INSERT INTO `elements` VALUES ('182', '153', '5', '2', '2018-04-02 14:15:20', '2018-04-02 14:15:20');
INSERT INTO `elements` VALUES ('183', '154', '5', '2', '2018-04-02 14:26:32', '2018-04-02 14:26:32');
INSERT INTO `elements` VALUES ('184', '155', '5', '2', '2018-04-02 15:03:40', '2018-04-02 15:03:40');
INSERT INTO `elements` VALUES ('185', '156', '5', '2', '2018-04-02 15:04:58', '2018-04-02 15:04:58');
INSERT INTO `elements` VALUES ('186', '157', '7', '2', '2018-04-02 15:22:09', '2018-04-02 15:22:09');
INSERT INTO `elements` VALUES ('187', '158', '5', '2', '2018-04-02 16:17:49', '2018-04-02 16:17:49');
INSERT INTO `elements` VALUES ('188', '159', '5', '2', '2018-04-02 16:18:05', '2018-04-02 16:18:05');
INSERT INTO `elements` VALUES ('189', '160', '7', '3', '2018-04-04 01:23:17', '2018-04-04 01:23:17');
INSERT INTO `elements` VALUES ('190', '161', '4', '4', '2018-04-05 02:24:04', '2018-04-05 02:24:04');
INSERT INTO `elements` VALUES ('191', '162', '5', '5', '2018-04-07 07:40:07', '2018-04-07 07:40:07');

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` text COLLATE utf8mb4_unicode_ci,
  `birthday` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `option_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of employees
-- ----------------------------
INSERT INTO `employees` VALUES ('1', 'Nguyễn An', null, '1998-04-12 10:29:43', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:43', '2018-04-12 10:29:43', null);
INSERT INTO `employees` VALUES ('2', 'Nguyễn An', null, '1998-04-12 10:29:44', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:44', '2018-04-12 10:29:44', null);
INSERT INTO `employees` VALUES ('3', 'Nguyễn An', null, '1998-04-12 10:29:45', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:45', '2018-04-12 10:29:45', null);
INSERT INTO `employees` VALUES ('4', 'Nguyễn An', null, '1998-04-12 10:29:45', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:45', '2018-04-12 10:29:45', null);
INSERT INTO `employees` VALUES ('5', 'Nguyễn An', null, '1998-04-12 10:29:45', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:45', '2018-04-12 10:29:45', null);
INSERT INTO `employees` VALUES ('6', 'Nguyễn An', null, '1998-04-12 10:29:45', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:45', '2018-04-12 10:29:45', null);
INSERT INTO `employees` VALUES ('7', 'Nguyễn An', null, '1998-04-12 10:29:45', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:45', '2018-04-12 10:29:45', null);
INSERT INTO `employees` VALUES ('8', 'Nguyễn An', null, '1998-04-12 10:29:45', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:45', '2018-04-12 10:29:45', null);
INSERT INTO `employees` VALUES ('9', 'Nguyễn An', null, '1998-04-12 10:29:46', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:46', '2018-04-12 10:29:46', null);
INSERT INTO `employees` VALUES ('10', 'Nguyễn An', null, '1998-04-12 10:29:57', null, 'cdddd', 'a@gmail.com', '9876543', null, null, '2018-04-12 10:29:57', '2018-04-12 10:29:57', null);
INSERT INTO `employees` VALUES ('11', 'Nguyễn An', null, '1998-04-12 00:00:00', null, 'cdddd', 'a@gmail.com', '9876543', null, '2018-04-12 10:32:25', '2018-04-12 10:31:07', '2018-04-12 10:32:25', null);
INSERT INTO `employees` VALUES ('12', 'Nguyễn Bình', null, '1997-04-12 10:32:48', null, 'cdddd', 'a@gmail.com', '98765432', null, null, '2018-04-12 10:32:48', '2018-04-12 10:32:48', null);
INSERT INTO `employees` VALUES ('13', 'Nguyễn An', '/employees_20180412160309Chrysanthemum.jpg', '1998-04-12', '1', null, 'a@gmail.com', '9876543', 'aaaâ', null, '2018-04-12 16:03:09', '2018-04-12 16:03:09', '4444444');
INSERT INTO `employees` VALUES ('14', 'Nguyễn An', '/employees_20180412160502Penguins.jpg', '1998-04-12', '1', null, 'a@gmail.com', '9876543', '1111', null, '2018-04-12 16:05:02', '2018-04-12 16:05:02', '11111');
INSERT INTO `employees` VALUES ('15', '11111', '/employees_20180412160538Tulips.jpg', '111111-11-01', '0', null, 'tranha084@gmail.com', '1111', '1111', null, '2018-04-12 16:05:38', '2018-04-12 16:05:38', '111');
INSERT INTO `employees` VALUES ('16', 'Nguyễn An', '/employees_20180412162436Penguins.jpg', '1998-04-12', '1', null, 'a@gmail.com', '9876543', '111', null, '2018-04-12 16:24:36', '2018-04-12 16:24:36', '1111');
INSERT INTO `employees` VALUES ('17', 'Nguyễn An', '/employees_20180412162657Penguins.jpg', '1998-04-12', '1', null, 'a@gmail.com', '9876543', '111', null, '2018-04-12 16:26:57', '2018-04-12 16:26:57', '1111');
INSERT INTO `employees` VALUES ('18', 'Nguyễn An', '/employees_20180412162714Penguins.jpg', '1998-04-12', '1', null, 'a@gmail.com', '9876543', '22222222', null, '2018-04-12 16:27:14', '2018-04-12 16:27:14', '2222222222222222222222222222222');
INSERT INTO `employees` VALUES ('19', 'Nguyễn An 11111', '/employees_20180412162744', '1998-04-12', '1', null, 'a@gmail.com', '9876543', 'aaaâ', null, '2018-04-12 16:27:44', '2018-04-12 16:27:44', '4444444');
INSERT INTO `employees` VALUES ('20', 'Nguyễn An', '/employees_20180413034627', '1998-04-12', '1', null, 'a@gmail.com', '9876543', 'aaaâ', null, '2018-04-13 03:46:27', '2018-04-13 03:46:27', '4444444');
INSERT INTO `employees` VALUES ('21', 'Nguyễn An', '/employees_20180413034643', '1998-04-12', '1', null, 'a@gmail.com', '9876543', 'hn', null, '2018-04-13 03:46:43', '2018-04-13 03:46:43', 'hn');
INSERT INTO `employees` VALUES ('22', 'aaaa', '/employees_20180413040026', '2018-04-10', '1', null, 'sasha.vovk@aol.com', '123123123', 'aaaa aaa', null, '2018-04-13 04:00:26', '2018-04-13 04:00:26', 'aaa aaaa');
INSERT INTO `employees` VALUES ('23', 'b', null, '1888-04-13 04:58:02', null, 'cdddd', 'b@gmail.com', '98765', null, null, '2018-04-13 04:58:02', '2018-04-13 04:58:02', null);
INSERT INTO `employees` VALUES ('24', 'bv', null, '1970-01-01 00:00:00', null, 'cdddd', null, '98765', null, null, '2018-04-13 05:10:35', '2018-04-13 05:10:35', null);
INSERT INTO `employees` VALUES ('25', 'Nguyễn An', '/employees_20180413061149', '1998-04-12', '1', null, 'a@gmail.com', '9876543', 'hn', null, '2018-04-13 06:11:49', '2018-04-13 06:11:49', 'hnh');
INSERT INTO `employees` VALUES ('26', 'Nguyễn An', '/employees_20180413061201', '1998-04-12', '1', null, 'a@gmail.com', '9876543', 'hn', null, '2018-04-13 06:12:01', '2018-04-13 06:12:01', 'hnh');
INSERT INTO `employees` VALUES ('27', '1233', null, null, null, 'cdddd', null, null, null, null, '2018-04-15 14:58:02', '2018-04-15 14:58:02', null);
INSERT INTO `employees` VALUES ('28', '1234', null, null, null, 'cdddd', null, null, null, null, '2018-04-15 15:00:13', '2018-04-15 15:00:13', null);
INSERT INTO `employees` VALUES ('29', '1234', null, null, null, 'cdddd', null, null, null, null, '2018-04-15 15:00:14', '2018-04-15 15:00:14', null);
INSERT INTO `employees` VALUES ('30', '4321', null, null, null, 'cdddd', null, null, null, null, '2018-04-15 15:03:10', '2018-04-15 15:03:10', null);
INSERT INTO `employees` VALUES ('31', '5444', null, null, null, 'cdddd', null, null, null, null, '2018-04-15 15:03:15', '2018-04-15 15:03:15', null);
INSERT INTO `employees` VALUES ('32', '55555555', null, null, null, 'cdddd', null, null, null, null, '2018-04-15 15:03:20', '2018-04-15 15:03:20', null);
INSERT INTO `employees` VALUES ('33', '11111111111111111111112222222222222 22222222222222', null, null, null, 'cdddd', null, '999999999999999999', null, null, '2018-04-15 15:03:26', '2018-04-15 15:03:26', null);
INSERT INTO `employees` VALUES ('34', '4444444444444444444444444444444444', null, null, null, 'cdddd', null, '9223372036854775807', null, null, '2018-04-15 15:13:15', '2018-04-15 15:13:15', null);
INSERT INTO `employees` VALUES ('35', '2222222222222222222222222222222222222222', null, null, null, 'cdddd', null, '111111111111111111', null, null, '2018-04-15 15:26:24', '2018-04-15 15:26:24', null);
INSERT INTO `employees` VALUES ('36', 'uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu', null, null, null, 'cdddd', null, '999999999999999999', null, null, '2018-04-15 15:26:32', '2018-04-15 15:26:32', null);
INSERT INTO `employees` VALUES ('37', 'lananh', null, '1970-01-01 00:00:00', null, 'cdddd', 'lananh@gmail.com', '983335555', null, null, '2018-04-15 15:48:55', '2018-04-15 15:48:55', null);
INSERT INTO `employees` VALUES ('38', 'lananh', null, '26', null, 'cdddd', null, '95475455555', null, null, '2018-04-15 15:53:03', '2018-04-15 15:53:03', null);
INSERT INTO `employees` VALUES ('39', '12222', null, '333', null, 'cdddd', null, '3333', null, null, '2018-04-15 16:04:43', '2018-04-15 16:04:43', null);
INSERT INTO `employees` VALUES ('40', '22222', null, '26/10/1990', null, 'cdddd', null, null, null, null, '2018-04-15 16:04:57', '2018-04-16 17:47:14', null);
INSERT INTO `employees` VALUES ('41', '11222', null, '3333', null, 'cdddd', null, '44444', null, null, '2018-04-16 15:31:07', '2018-04-16 15:31:07', null);
INSERT INTO `employees` VALUES ('42', '33333', null, '444', null, 'cdddd', null, '5555', null, null, '2018-04-16 15:31:18', '2018-04-16 15:31:18', null);
INSERT INTO `employees` VALUES ('43', '455', null, '666', null, 'ddg33', null, '666', null, null, '2018-04-16 15:31:23', '2018-04-16 15:31:23', null);
INSERT INTO `employees` VALUES ('44', '33333', null, '5555', null, 'cdddd', null, '44444', null, null, '2018-04-16 16:16:30', '2018-04-16 16:16:30', null);
INSERT INTO `employees` VALUES ('45', 'Mai anh', null, '26/10/1990', null, 'cdddd', 'laven9696@gmail.com', '947478686', null, null, '2018-04-16 16:17:15', '2018-04-16 16:17:15', null);

-- ----------------------------
-- Table structure for employee_position
-- ----------------------------
DROP TABLE IF EXISTS `employee_position`;
CREATE TABLE `employee_position` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `branch` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `option_code` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of employee_position
-- ----------------------------
INSERT INTO `employee_position` VALUES ('1', '12', null, '11', 'cdddd', '2018-04-12 10:32:25', '2018-04-12 10:31:07', '2018-04-12 10:32:25');
INSERT INTO `employee_position` VALUES ('2', '12', null, '12', 'cdddd', null, '2018-04-12 10:32:48', '2018-04-12 10:32:48');
INSERT INTO `employee_position` VALUES ('3', null, 'DEPARTMENT01', '17', 'ddg33', null, '2018-04-12 16:26:57', '2018-04-12 16:26:57');
INSERT INTO `employee_position` VALUES ('4', null, 'DEPARTMENT02', '18', 'ddg33', null, '2018-04-12 16:27:14', '2018-04-12 16:27:14');
INSERT INTO `employee_position` VALUES ('5', null, 'DEPARTMENT03', '19', 'cdddd', null, '2018-04-12 16:27:44', '2018-04-12 16:27:44');
INSERT INTO `employee_position` VALUES ('6', '0', 'DEPARTMENT01', '20', 'ddg33', null, '2018-04-13 03:46:27', '2018-04-13 03:46:27');
INSERT INTO `employee_position` VALUES ('7', '0', 'DEPARTMENT02', '21', 'ddg33', null, '2018-04-13 03:46:43', '2018-04-13 03:46:43');
INSERT INTO `employee_position` VALUES ('8', '12', null, '23', 'cdddd', null, '2018-04-13 04:58:02', '2018-04-13 04:58:02');
INSERT INTO `employee_position` VALUES ('9', '12', null, '24', 'cdddd', null, '2018-04-13 05:10:35', '2018-04-13 05:10:35');
INSERT INTO `employee_position` VALUES ('10', '0', 'DEPARTMENT01', '26', 'cdddd', null, '2018-04-13 06:12:01', '2018-04-13 06:12:01');
INSERT INTO `employee_position` VALUES ('11', '29', null, '27', 'cdddd', null, '2018-04-15 14:58:02', '2018-04-15 14:58:02');
INSERT INTO `employee_position` VALUES ('12', '29', null, '28', 'cdddd', null, '2018-04-15 15:00:13', '2018-04-15 15:00:13');
INSERT INTO `employee_position` VALUES ('13', '29', null, '29', 'cdddd', null, '2018-04-15 15:00:14', '2018-04-15 15:00:14');
INSERT INTO `employee_position` VALUES ('14', '29', null, '30', 'cdddd', null, '2018-04-15 15:03:10', '2018-04-15 15:03:10');
INSERT INTO `employee_position` VALUES ('15', '29', null, '31', 'cdddd', null, '2018-04-15 15:03:15', '2018-04-15 15:03:15');
INSERT INTO `employee_position` VALUES ('16', '29', null, '32', 'cdddd', null, '2018-04-15 15:03:20', '2018-04-15 15:03:20');
INSERT INTO `employee_position` VALUES ('17', '29', null, '33', 'cdddd', null, '2018-04-15 15:03:26', '2018-04-15 15:03:26');
INSERT INTO `employee_position` VALUES ('18', '29', null, '35', 'cdddd', null, '2018-04-15 15:26:24', '2018-04-15 15:26:24');
INSERT INTO `employee_position` VALUES ('19', '29', null, '36', 'cdddd', null, '2018-04-15 15:26:32', '2018-04-15 15:26:32');
INSERT INTO `employee_position` VALUES ('20', '1', null, '37', 'cdddd', null, '2018-04-15 15:48:55', '2018-04-15 15:48:55');
INSERT INTO `employee_position` VALUES ('21', '1', null, '38', 'cdddd', null, '2018-04-15 15:53:03', '2018-04-15 15:53:03');
INSERT INTO `employee_position` VALUES ('23', '22', null, '40', 'cdddd', null, '2018-04-15 16:04:57', '2018-04-16 17:47:14');
INSERT INTO `employee_position` VALUES ('24', '28', null, '41', 'cdddd', null, '2018-04-16 15:31:07', '2018-04-16 15:31:07');
INSERT INTO `employee_position` VALUES ('25', '28', null, '42', 'cdddd', null, '2018-04-16 15:31:18', '2018-04-16 15:31:18');
INSERT INTO `employee_position` VALUES ('26', '28', null, '43', 'ddg33', null, '2018-04-16 15:31:23', '2018-04-16 15:31:23');
INSERT INTO `employee_position` VALUES ('27', '1', null, '44', 'cdddd', null, '2018-04-16 16:16:30', '2018-04-16 16:16:30');
INSERT INTO `employee_position` VALUES ('28', '8', null, '45', 'cdddd', null, '2018-04-16 16:17:15', '2018-04-16 16:17:15');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `originalName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `absPath` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relativeUrl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=589 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of files
-- ----------------------------
INSERT INTO `files` VALUES ('1', 'NGUYEN DUC HANH.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451167_5a90fc1fcf16c3.79462674.jpeg', 'collection/2018/02/24/img_1519451167_5a90fc1fcf16c3.79462674.jpeg', 'image/jpeg', '243185', '2018-02-24 12:46:07', '2018-02-24 12:46:07', '1');
INSERT INTO `files` VALUES ('2', 'TranVanTrinh.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451203_5a90fc431d1248.79172703.jpeg', 'collection/2018/02/24/img_1519451203_5a90fc431d1248.79172703.jpeg', 'image/jpeg', '34932', '2018-02-24 12:46:43', '2018-02-24 12:46:43', '1');
INSERT INTO `files` VALUES ('3', 'DAO DUY KHANH TUNG.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451321_5a90fcb93a5dd7.69756380.jpeg', 'collection/2018/02/24/img_1519451321_5a90fcb93a5dd7.69756380.jpeg', 'image/jpeg', '100723', '2018-02-24 12:48:41', '2018-02-24 12:48:41', '1');
INSERT INTO `files` VALUES ('4', 'NGUYEN DUC HANH.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451412_5a90fd1467a600.08515929.jpeg', 'collection/2018/02/24/img_1519451412_5a90fd1467a600.08515929.jpeg', 'image/jpeg', '243185', '2018-02-24 12:50:12', '2018-02-24 12:50:12', '1');
INSERT INTO `files` VALUES ('5', 'NGUYEN DUC HANH.jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519451419_5a90fd1b3e49c1.66191795.jpeg', 'collection/2018/02/24/img_1519451419_5a90fd1b3e49c1.66191795.jpeg', 'image/jpeg', '243185', '2018-02-24 12:50:19', '2018-02-24 12:50:19', '1');
INSERT INTO `files` VALUES ('6', 'tải xuống (1).jpg', '/var/www/html/public/files/collection/2018/02/24/img_1519454500_5a910924ecf468.76533821.jpeg', 'collection/2018/02/24/img_1519454500_5a910924ecf468.76533821.jpeg', 'image/jpeg', '8797', '2018-02-24 13:41:40', '2018-02-24 13:41:40', '1');
INSERT INTO `files` VALUES ('7', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519456638_5a91117eaae866.31338780.png', 'collection/2018/02/24/img_1519456638_5a91117eaae866.31338780.png', 'image/png', '23489', '2018-02-24 14:17:18', '2018-02-24 14:17:18', '1');
INSERT INTO `files` VALUES ('8', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519457700_5a9115a402da64.77237649.png', 'collection/2018/02/24/img_1519457700_5a9115a402da64.77237649.png', 'image/png', '23489', '2018-02-24 14:35:00', '2018-02-24 14:35:00', '1');
INSERT INTO `files` VALUES ('9', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519457805_5a91160d546610.77412253.png', 'collection/2018/02/24/img_1519457805_5a91160d546610.77412253.png', 'image/png', '23489', '2018-02-24 14:36:45', '2018-02-24 14:36:45', '1');
INSERT INTO `files` VALUES ('10', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519457897_5a9116690a10b2.79534223.png', 'collection/2018/02/24/img_1519457897_5a9116690a10b2.79534223.png', 'image/png', '23489', '2018-02-24 14:38:17', '2018-02-24 14:38:17', '1');
INSERT INTO `files` VALUES ('11', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519457998_5a9116ce0174b1.44027310.png', 'collection/2018/02/24/img_1519457998_5a9116ce0174b1.44027310.png', 'image/png', '23489', '2018-02-24 14:39:58', '2018-02-24 14:39:58', '1');
INSERT INTO `files` VALUES ('12', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519458202_5a91179a078ce9.90395819.png', 'collection/2018/02/24/img_1519458202_5a91179a078ce9.90395819.png', 'image/png', '23489', '2018-02-24 14:43:22', '2018-02-24 14:43:22', '1');
INSERT INTO `files` VALUES ('13', 'ip.png', '/var/www/html/public/files/collection/2018/02/24/img_1519458335_5a91181f296032.75680834.png', 'collection/2018/02/24/img_1519458335_5a91181f296032.75680834.png', 'image/png', '23489', '2018-02-24 14:45:35', '2018-02-24 14:45:35', '1');
INSERT INTO `files` VALUES ('14', '5a92dd894598e_1519574409_20180225.jpg', 'files/decision/2018/02/5a92dd894598e_1519574409_20180225.jpg', 'files/decision/2018/02/5a92dd894598e_1519574409_20180225.jpg', 'image/jpeg', '11122', '2018-02-25 23:00:09', '2018-02-25 23:00:09', '1');
INSERT INTO `files` VALUES ('15', '5a92eaba9dfa8_1519577786_20180225.jpg', 'files/decision/2018/02/5a92eaba9dfa8_1519577786_20180225.jpg', 'files/decision/2018/02/5a92eaba9dfa8_1519577786_20180225.jpg', 'image/jpeg', '1934362', '2018-02-25 23:56:26', '2018-02-25 23:56:26', '1');
INSERT INTO `files` VALUES ('16', '15-thumb.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519611441_5a936e31e6e684.04251287.jpeg', 'collection/2018/02/26/img_1519611441_5a936e31e6e684.04251287.jpeg', 'image/jpeg', '126194', '2018-02-26 09:17:21', '2018-02-26 09:17:21', '1');
INSERT INTO `files` VALUES ('17', '13.png', '/var/www/html/public/files/collection/2018/02/26/img_1519612808_5a9373889c4be5.03437785.png', 'collection/2018/02/26/img_1519612808_5a9373889c4be5.03437785.png', 'image/png', '596463', '2018-02-26 09:40:08', '2018-02-26 09:40:08', '1');
INSERT INTO `files` VALUES ('18', '13.png', '/var/www/html/public/files/collection/2018/02/26/img_1519612975_5a93742f2634e5.81380154.png', 'collection/2018/02/26/img_1519612975_5a93742f2634e5.81380154.png', 'image/png', '596463', '2018-02-26 09:42:55', '2018-02-26 09:42:55', '1');
INSERT INTO `files` VALUES ('19', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621769_5a939689d6e5a8.05789966.png', 'collection/2018/02/26/img_1519621769_5a939689d6e5a8.05789966.png', 'image/png', '188588', '2018-02-26 12:09:29', '2018-02-26 12:09:29', '1');
INSERT INTO `files` VALUES ('20', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621778_5a939692bc4006.44694624.png', 'collection/2018/02/26/img_1519621778_5a939692bc4006.44694624.png', 'image/png', '188588', '2018-02-26 12:09:38', '2018-02-26 12:09:38', '1');
INSERT INTO `files` VALUES ('21', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621826_5a9396c2891a51.61705432.png', 'collection/2018/02/26/img_1519621826_5a9396c2891a51.61705432.png', 'image/png', '188588', '2018-02-26 12:10:26', '2018-02-26 12:10:26', '1');
INSERT INTO `files` VALUES ('22', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621857_5a9396e18ce861.51794698.png', 'collection/2018/02/26/img_1519621857_5a9396e18ce861.51794698.png', 'image/png', '188588', '2018-02-26 12:10:57', '2018-02-26 12:10:57', '1');
INSERT INTO `files` VALUES ('23', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519621863_5a9396e7f279c7.11585345.png', 'collection/2018/02/26/img_1519621863_5a9396e7f279c7.11585345.png', 'image/png', '188588', '2018-02-26 12:11:04', '2018-02-26 12:11:04', '1');
INSERT INTO `files` VALUES ('24', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519622879_5a939adfba43e7.76143131.png', 'collection/2018/02/26/img_1519622879_5a939adfba43e7.76143131.png', 'image/png', '188588', '2018-02-26 12:27:59', '2018-02-26 12:27:59', '1');
INSERT INTO `files` VALUES ('25', 'cr.png', '/var/www/html/public/files/collection/2018/02/26/img_1519622901_5a939af55eae35.29623507.png', 'collection/2018/02/26/img_1519622901_5a939af55eae35.29623507.png', 'image/png', '188588', '2018-02-26 12:28:21', '2018-02-26 12:28:21', '1');
INSERT INTO `files` VALUES ('26', '5a93c167bb189_1519632743_20180226.png', 'files/decision/2018/02/5a93c167bb189_1519632743_20180226.png', 'files/decision/2018/02/5a93c167bb189_1519632743_20180226.png', 'image/png', '90744', '2018-02-26 15:12:23', '2018-02-26 15:12:23', '1');
INSERT INTO `files` VALUES ('27', '5a93cb152a704_1519635221_20180226.JPG', 'files/decision/2018/02/5a93cb152a704_1519635221_20180226.JPG', 'files/decision/2018/02/5a93cb152a704_1519635221_20180226.JPG', 'image/jpeg', '1726372', '2018-02-26 15:53:41', '2018-02-26 15:53:41', '1');
INSERT INTO `files` VALUES ('28', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519635918_5a93cdce6a29a8.70429118.jpeg', 'collection/2018/02/26/img_1519635918_5a93cdce6a29a8.70429118.jpeg', 'image/jpeg', '879394', '2018-02-26 16:05:18', '2018-02-26 16:05:18', '1');
INSERT INTO `files` VALUES ('29', '5a93d055b90ea_1519636565_20180226.PNG', 'files/decision/2018/02/5a93d055b90ea_1519636565_20180226.PNG', 'files/decision/2018/02/5a93d055b90ea_1519636565_20180226.PNG', 'image/png', '30222', '2018-02-26 16:16:05', '2018-02-26 16:16:05', '1');
INSERT INTO `files` VALUES ('30', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519637964_5a93d5cc4ebe85.50448720.jpeg', 'collection/2018/02/26/img_1519637964_5a93d5cc4ebe85.50448720.jpeg', 'image/jpeg', '879394', '2018-02-26 16:39:24', '2018-02-26 16:39:24', '1');
INSERT INTO `files` VALUES ('31', '5a93d78a3a07e_1519638410_20180226.PNG', 'files/decision/2018/02/5a93d78a3a07e_1519638410_20180226.PNG', 'files/decision/2018/02/5a93d78a3a07e_1519638410_20180226.PNG', 'image/png', '30222', '2018-02-26 16:46:50', '2018-02-26 16:46:50', '1');
INSERT INTO `files` VALUES ('32', 'Desert.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519638578_5a93d832b2d622.92349533.jpeg', 'collection/2018/02/26/img_1519638578_5a93d832b2d622.92349533.jpeg', 'image/jpeg', '845941', '2018-02-26 16:49:38', '2018-02-26 16:49:38', '1');
INSERT INTO `files` VALUES ('33', '5a93dc658ff7f_1519639653_20180226.png', 'files/decision/2018/02/5a93dc658ff7f_1519639653_20180226.png', 'files/decision/2018/02/5a93dc658ff7f_1519639653_20180226.png', 'image/png', '90744', '2018-02-26 17:07:33', '2018-02-26 17:07:33', '1');
INSERT INTO `files` VALUES ('34', 'hinh-nen-phim-angry-bird-dep-nhat-250x320.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519662393_5a94353995ab98.32311091.jpeg', 'collection/2018/02/26/img_1519662393_5a94353995ab98.32311091.jpeg', 'image/jpeg', '16340', '2018-02-26 23:26:33', '2018-02-26 23:26:33', '1');
INSERT INTO `files` VALUES ('35', 'hinh-nen-phim-angry-bird-dep-nhat-250x320.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519662527_5a9435bf0cc753.06918980.jpeg', 'collection/2018/02/26/img_1519662527_5a9435bf0cc753.06918980.jpeg', 'image/jpeg', '16340', '2018-02-26 23:28:47', '2018-02-26 23:28:47', '1');
INSERT INTO `files` VALUES ('36', 'hinh-nen-phim-angry-bird-dep-nhat-250x320.jpg', '/var/www/html/public/files/collection/2018/02/26/img_1519662653_5a94363db4a4f6.38715833.jpeg', 'collection/2018/02/26/img_1519662653_5a94363db4a4f6.38715833.jpeg', 'image/jpeg', '16340', '2018-02-26 23:30:53', '2018-02-26 23:30:53', '1');
INSERT INTO `files` VALUES ('37', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519702580_5a94d2341625e4.46964606.jpeg', 'collection/2018/02/27/img_1519702580_5a94d2341625e4.46964606.jpeg', 'image/jpeg', '879394', '2018-02-27 10:36:20', '2018-02-27 10:36:20', '1');
INSERT INTO `files` VALUES ('38', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519702751_5a94d2df1d22e8.75918553.jpeg', 'collection/2018/02/27/img_1519702751_5a94d2df1d22e8.75918553.jpeg', 'image/jpeg', '879394', '2018-02-27 10:39:11', '2018-02-27 10:39:11', '1');
INSERT INTO `files` VALUES ('39', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519703063_5a94d4176b1e72.42922819.jpeg', 'collection/2018/02/27/img_1519703063_5a94d4176b1e72.42922819.jpeg', 'image/jpeg', '879394', '2018-02-27 10:44:23', '2018-02-27 10:44:23', '1');
INSERT INTO `files` VALUES ('40', '109.png', '/var/www/html/public/files/collection/2018/02/27/img_1519703299_5a94d503e87d72.99676817.png', 'collection/2018/02/27/img_1519703299_5a94d503e87d72.99676817.png', 'image/png', '545911', '2018-02-27 10:48:19', '2018-02-27 10:48:19', '1');
INSERT INTO `files` VALUES ('41', 'computername.png', '/var/www/html/public/files/collection/2018/02/27/img_1519703841_5a94d721c317e6.56836703.png', 'collection/2018/02/27/img_1519703841_5a94d721c317e6.56836703.png', 'image/png', '37785', '2018-02-27 10:57:21', '2018-02-27 10:57:21', '1');
INSERT INTO `files` VALUES ('42', '300[1].jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519704996_5a94dba480c453.62476810.jpeg', 'collection/2018/02/27/img_1519704996_5a94dba480c453.62476810.jpeg', 'image/jpeg', '11479', '2018-02-27 11:16:36', '2018-02-27 11:16:36', '1');
INSERT INTO `files` VALUES ('43', '300[1].jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519705532_5a94ddbc840759.43046652.jpeg', 'collection/2018/02/27/img_1519705532_5a94ddbc840759.43046652.jpeg', 'image/jpeg', '11479', '2018-02-27 11:25:32', '2018-02-27 11:25:32', '1');
INSERT INTO `files` VALUES ('44', 'IMG-0350 (1).jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519705848_5a94def8cf33e4.52246967.jpeg', 'collection/2018/02/27/img_1519705848_5a94def8cf33e4.52246967.jpeg', 'image/jpeg', '506056', '2018-02-27 11:30:49', '2018-02-27 11:30:49', '1');
INSERT INTO `files` VALUES ('45', 'TranVanTrinh.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519706052_5a94dfc4a389c9.40438360.jpeg', 'collection/2018/02/27/img_1519706052_5a94dfc4a389c9.40438360.jpeg', 'image/jpeg', '34932', '2018-02-27 11:34:12', '2018-02-27 11:34:12', '1');
INSERT INTO `files` VALUES ('46', 'IMG-0350.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519706197_5a94e0550265f1.25549632.jpeg', 'collection/2018/02/27/img_1519706197_5a94e0550265f1.25549632.jpeg', 'image/jpeg', '506056', '2018-02-27 11:36:37', '2018-02-27 11:36:37', '1');
INSERT INTO `files` VALUES ('47', 'TranVanTrinh.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519706346_5a94e0ea076d65.00773363.jpeg', 'collection/2018/02/27/img_1519706346_5a94e0ea076d65.00773363.jpeg', 'image/jpeg', '34932', '2018-02-27 11:39:06', '2018-02-27 11:39:06', '1');
INSERT INTO `files` VALUES ('48', '5a9512b4d99ee_1519719092_20180227.jpg', 'files/decision/2018/02/5a9512b4d99ee_1519719092_20180227.jpg', 'files/decision/2018/02/5a9512b4d99ee_1519719092_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:11:32', '2018-02-27 15:11:32', '1');
INSERT INTO `files` VALUES ('49', '5a951337ba1c4_1519719223_20180227.jpg', 'files/decision/2018/02/5a951337ba1c4_1519719223_20180227.jpg', 'files/decision/2018/02/5a951337ba1c4_1519719223_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:13:43', '2018-02-27 15:13:43', '1');
INSERT INTO `files` VALUES ('50', '5a95134a8e271_1519719242_20180227.jpg', 'files/decision/2018/02/5a95134a8e271_1519719242_20180227.jpg', 'files/decision/2018/02/5a95134a8e271_1519719242_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:14:02', '2018-02-27 15:14:02', '1');
INSERT INTO `files` VALUES ('51', '5a95139bd06d6_1519719323_20180227.jpg', 'files/decision/2018/02/5a95139bd06d6_1519719323_20180227.jpg', 'files/decision/2018/02/5a95139bd06d6_1519719323_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:15:23', '2018-02-27 15:15:23', '1');
INSERT INTO `files` VALUES ('52', '5a95140ef2a15_1519719438_20180227.jpg', 'files/decision/2018/02/5a95140ef2a15_1519719438_20180227.jpg', 'files/decision/2018/02/5a95140ef2a15_1519719438_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:17:18', '2018-02-27 15:17:18', '1');
INSERT INTO `files` VALUES ('53', '5a95142cb7476_1519719468_20180227.jpg', 'files/decision/2018/02/5a95142cb7476_1519719468_20180227.jpg', 'files/decision/2018/02/5a95142cb7476_1519719468_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:17:48', '2018-02-27 15:17:48', '1');
INSERT INTO `files` VALUES ('54', '5a95143963828_1519719481_20180227.jpg', 'files/decision/2018/02/5a95143963828_1519719481_20180227.jpg', 'files/decision/2018/02/5a95143963828_1519719481_20180227.jpg', 'image/jpeg', '879394', '2018-02-27 15:18:01', '2018-02-27 15:18:01', '1');
INSERT INTO `files` VALUES ('55', '5a95144fba0fd_1519719503_20180227.jpg', 'files/decision/2018/02/5a95144fba0fd_1519719503_20180227.jpg', 'files/decision/2018/02/5a95144fba0fd_1519719503_20180227.jpg', 'image/jpeg', '780831', '2018-02-27 15:18:23', '2018-02-27 15:18:23', '1');
INSERT INTO `files` VALUES ('56', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519721016_5a951a3864be80.41599715.jpeg', 'collection/2018/02/27/img_1519721016_5a951a3864be80.41599715.jpeg', 'image/jpeg', '879394', '2018-02-27 15:43:36', '2018-02-27 15:43:36', '1');
INSERT INTO `files` VALUES ('57', 'Desert.jpg', '/var/www/html/public/files/collection/2018/02/27/img_1519721229_5a951b0d0db026.89933087.jpeg', 'collection/2018/02/27/img_1519721229_5a951b0d0db026.89933087.jpeg', 'image/jpeg', '845941', '2018-02-27 15:47:09', '2018-02-27 15:47:09', '1');
INSERT INTO `files` VALUES ('58', '5a9530c5cc500_1519726789_20180227.jpg', 'files/decision/2018/02/5a9530c5cc500_1519726789_20180227.jpg', 'files/decision/2018/02/5a9530c5cc500_1519726789_20180227.jpg', 'image/jpeg', '222158', '2018-02-27 17:19:49', '2018-02-27 17:19:49', '1');
INSERT INTO `files` VALUES ('59', '5a9536d63d9dd_1519728342_20180227.png', 'files/decision/2018/02/5a9536d63d9dd_1519728342_20180227.png', 'files/decision/2018/02/5a9536d63d9dd_1519728342_20180227.png', 'image/png', '48978', '2018-02-27 17:45:42', '2018-02-27 17:45:42', '1');
INSERT INTO `files` VALUES ('60', '5a9536ee7ca3e_1519728366_20180227.png', 'files/decision/2018/02/5a9536ee7ca3e_1519728366_20180227.png', 'files/decision/2018/02/5a9536ee7ca3e_1519728366_20180227.png', 'image/png', '206138', '2018-02-27 17:46:06', '2018-02-27 17:46:06', '1');
INSERT INTO `files` VALUES ('61', '5a95370e3187a_1519728398_20180227.png', 'files/decision/2018/02/5a95370e3187a_1519728398_20180227.png', 'files/decision/2018/02/5a95370e3187a_1519728398_20180227.png', 'image/png', '206138', '2018-02-27 17:46:38', '2018-02-27 17:46:38', '1');
INSERT INTO `files` VALUES ('62', '5a953718dc0ab_1519728408_20180227.png', 'files/decision/2018/02/5a953718dc0ab_1519728408_20180227.png', 'files/decision/2018/02/5a953718dc0ab_1519728408_20180227.png', 'image/png', '206138', '2018-02-27 17:46:48', '2018-02-27 17:46:48', '1');
INSERT INTO `files` VALUES ('63', '5a954afd5d6cc_1519733501_20180227.png', 'files/decision/2018/02/5a954afd5d6cc_1519733501_20180227.png', 'files/decision/2018/02/5a954afd5d6cc_1519733501_20180227.png', 'image/png', '206138', '2018-02-27 19:11:41', '2018-02-27 19:11:41', '1');
INSERT INTO `files` VALUES ('64', '5a954b14a1e90_1519733524_20180227.png', 'files/decision/2018/02/5a954b14a1e90_1519733524_20180227.png', 'files/decision/2018/02/5a954b14a1e90_1519733524_20180227.png', 'image/png', '57589', '2018-02-27 19:12:04', '2018-02-27 19:12:04', '1');
INSERT INTO `files` VALUES ('65', '5a954b57e2084_1519733591_20180227.png', 'files/decision/2018/02/5a954b57e2084_1519733591_20180227.png', 'files/decision/2018/02/5a954b57e2084_1519733591_20180227.png', 'image/png', '206138', '2018-02-27 19:13:11', '2018-02-27 19:13:11', '1');
INSERT INTO `files` VALUES ('66', '5a954b86ebc85_1519733638_20180227.png', 'files/decision/2018/02/5a954b86ebc85_1519733638_20180227.png', 'files/decision/2018/02/5a954b86ebc85_1519733638_20180227.png', 'image/png', '581436', '2018-02-27 19:13:58', '2018-02-27 19:13:58', '1');
INSERT INTO `files` VALUES ('67', '5a961e2481702_1519787556_20180228.jpg', 'files/decision/2018/02/5a961e2481702_1519787556_20180228.jpg', 'files/decision/2018/02/5a961e2481702_1519787556_20180228.jpg', 'image/jpeg', '162122', '2018-02-28 10:12:36', '2018-02-28 10:12:36', '1');
INSERT INTO `files` VALUES ('68', '5a962068c6efa_1519788136_20180228.jpg', 'files/decision/2018/02/5a962068c6efa_1519788136_20180228.jpg', 'files/decision/2018/02/5a962068c6efa_1519788136_20180228.jpg', 'image/jpeg', '879394', '2018-02-28 10:22:16', '2018-02-28 10:22:16', '1');
INSERT INTO `files` VALUES ('69', '5a96210d59811_1519788301_20180228.jpg', 'files/decision/2018/02/5a96210d59811_1519788301_20180228.jpg', 'files/decision/2018/02/5a96210d59811_1519788301_20180228.jpg', 'image/jpeg', '595284', '2018-02-28 10:25:01', '2018-02-28 10:25:01', '1');
INSERT INTO `files` VALUES ('70', '5a96211e40765_1519788318_20180228.jpg', 'files/decision/2018/02/5a96211e40765_1519788318_20180228.jpg', 'files/decision/2018/02/5a96211e40765_1519788318_20180228.jpg', 'image/jpeg', '595284', '2018-02-28 10:25:18', '2018-02-28 10:25:18', '1');
INSERT INTO `files` VALUES ('71', '5a975c7840310_1519869048_20180301.mp4', 'files/decision/2018/03/5a975c7840310_1519869048_20180301.mp4', 'files/decision/2018/03/5a975c7840310_1519869048_20180301.mp4', 'video/mp4', '1892700', '2018-02-28 10:25:34', '2018-03-01 08:50:48', '1');
INSERT INTO `files` VALUES ('72', 'thamdinh_2acc.jpg', '/var/www/html/public/files/collection/2018/02/28/img_1519790073_5a9627f99d18e3.65271982.jpeg', 'collection/2018/02/28/img_1519790073_5a9627f99d18e3.65271982.jpeg', 'image/jpeg', '222158', '2018-02-28 10:54:33', '2018-02-28 10:54:33', '1');
INSERT INTO `files` VALUES ('73', 'computername.png', '/var/www/html/public/files/collection/2018/02/28/img_1519803974_5a965e46111852.39977560.png', 'collection/2018/02/28/img_1519803974_5a965e46111852.39977560.png', 'image/png', '37785', '2018-02-28 14:46:14', '2018-02-28 14:46:14', '1');
INSERT INTO `files` VALUES ('74', 'IMG_8091.JPG', '/var/www/html/public/files/collection/2018/02/28/img_1519804473_5a9660394e9774.52846602.jpeg', 'collection/2018/02/28/img_1519804473_5a9660394e9774.52846602.jpeg', 'image/jpeg', '1729938', '2018-02-28 14:54:33', '2018-02-28 14:54:33', '1');
INSERT INTO `files` VALUES ('75', 'baoloithieuanhchandungmacduco.png', '/var/www/html/public/files/collection/2018/02/28/img_1519813781_5a9684953e7a18.76667414.png', 'collection/2018/02/28/img_1519813781_5a9684953e7a18.76667414.png', 'image/png', '90744', '2018-02-28 17:29:41', '2018-02-28 17:29:41', '1');
INSERT INTO `files` VALUES ('76', '5a96856f540d9_1519813999_20180228.mp4', 'files/decision/2018/02/5a96856f540d9_1519813999_20180228.mp4', 'files/decision/2018/02/5a96856f540d9_1519813999_20180228.mp4', 'video/mp4', '1892700', '2018-02-28 17:33:19', '2018-02-28 17:33:19', '1');
INSERT INTO `files` VALUES ('77', 'IMG_8091.JPG', '/var/www/html/public/files/collection/2018/02/28/img_1519814507_5a96876b9df6b9.29143005.jpeg', 'collection/2018/02/28/img_1519814507_5a96876b9df6b9.29143005.jpeg', 'image/jpeg', '1729938', '2018-02-28 17:41:47', '2018-02-28 17:41:47', '1');
INSERT INTO `files` VALUES ('78', '5a97629692d8b_1519870614_20180301.jpg', 'files/decision/2018/03/5a97629692d8b_1519870614_20180301.jpg', 'files/decision/2018/03/5a97629692d8b_1519870614_20180301.jpg', 'image/jpeg', '845941', '2018-03-01 09:16:54', '2018-03-01 09:16:54', '1');
INSERT INTO `files` VALUES ('79', '5a9762b500fdd_1519870645_20180301.jpg', 'files/decision/2018/03/5a9762b500fdd_1519870645_20180301.jpg', 'files/decision/2018/03/5a9762b500fdd_1519870645_20180301.jpg', 'image/jpeg', '879394', '2018-03-01 09:17:25', '2018-03-01 09:17:25', '1');
INSERT INTO `files` VALUES ('80', 'baoloithieuanhchandungmacduco.png', '/var/www/html/public/files/collection/2018/03/01/img_1519875635_5a977633c84bf9.55579465.png', 'collection/2018/03/01/img_1519875635_5a977633c84bf9.55579465.png', 'image/png', '90744', '2018-03-01 10:40:35', '2018-03-01 10:40:35', '1');
INSERT INTO `files` VALUES ('81', 'computername.png', '/var/www/html/public/files/collection/2018/03/01/img_1519875656_5a977648844045.42114736.png', 'collection/2018/03/01/img_1519875656_5a977648844045.42114736.png', 'image/png', '37785', '2018-03-01 10:40:56', '2018-03-01 10:40:56', '1');
INSERT INTO `files` VALUES ('82', '5a9781c03d16b_1519878592_20180301.jpg', 'files/decision/2018/03/5a9781c03d16b_1519878592_20180301.jpg', 'files/decision/2018/03/5a9781c03d16b_1519878592_20180301.jpg', 'image/jpeg', '879394', '2018-03-01 11:29:52', '2018-03-01 11:29:52', '1');
INSERT INTO `files` VALUES ('83', '5a9781f89e50b_1519878648_20180301.jpg', 'files/decision/2018/03/5a9781f89e50b_1519878648_20180301.jpg', 'files/decision/2018/03/5a9781f89e50b_1519878648_20180301.jpg', 'image/jpeg', '845941', '2018-03-01 11:30:48', '2018-03-01 11:30:48', '1');
INSERT INTO `files` VALUES ('84', 'IMG-0350.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519887729_5a97a5714277b6.59899769.jpeg', 'collection/2018/03/01/img_1519887729_5a97a5714277b6.59899769.jpeg', 'image/jpeg', '506056', '2018-03-01 14:02:09', '2018-03-01 14:02:09', '1');
INSERT INTO `files` VALUES ('85', 'DAO DUY KHANH TUNG.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519887849_5a97a5e9555bb6.64068158.jpeg', 'collection/2018/03/01/img_1519887849_5a97a5e9555bb6.64068158.jpeg', 'image/jpeg', '100723', '2018-03-01 14:04:09', '2018-03-01 14:04:09', '1');
INSERT INTO `files` VALUES ('86', 'NGUYEN DUC HANH.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519888022_5a97a6966870c5.30132912.jpeg', 'collection/2018/03/01/img_1519888022_5a97a6966870c5.30132912.jpeg', 'image/jpeg', '243185', '2018-03-01 14:07:02', '2018-03-01 14:07:02', '1');
INSERT INTO `files` VALUES ('87', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519888234_5a97a76a3b4628.73415917.jpeg', 'collection/2018/03/01/img_1519888234_5a97a76a3b4628.73415917.jpeg', 'image/jpeg', '777835', '2018-03-01 14:10:34', '2018-03-01 14:10:34', '1');
INSERT INTO `files` VALUES ('88', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519888824_5a97a9b89d2943.40766449.jpeg', 'collection/2018/03/01/img_1519888824_5a97a9b89d2943.40766449.jpeg', 'image/jpeg', '777835', '2018-03-01 14:20:24', '2018-03-01 14:20:24', '1');
INSERT INTO `files` VALUES ('89', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519888861_5a97a9dd207b75.98475229.jpeg', 'collection/2018/03/01/img_1519888861_5a97a9dd207b75.98475229.jpeg', 'image/jpeg', '780831', '2018-03-01 14:21:01', '2018-03-01 14:21:01', '1');
INSERT INTO `files` VALUES ('90', 'Baoloithieu anh chan dung.png', '/var/www/html/public/files/collection/2018/03/01/img_1519889778_5a97ad72d32397.88163598.png', 'collection/2018/03/01/img_1519889778_5a97ad72d32397.88163598.png', 'image/png', '84240', '2018-03-01 14:36:18', '2018-03-01 14:36:18', '1');
INSERT INTO `files` VALUES ('91', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519890382_5a97afce2a7c66.32480494.jpeg', 'collection/2018/03/01/img_1519890382_5a97afce2a7c66.32480494.jpeg', 'image/jpeg', '879394', '2018-03-01 14:46:22', '2018-03-01 14:46:22', '1');
INSERT INTO `files` VALUES ('92', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519890607_5a97b0afba1529.38901662.jpeg', 'collection/2018/03/01/img_1519890607_5a97b0afba1529.38901662.jpeg', 'image/jpeg', '845941', '2018-03-01 14:50:07', '2018-03-01 14:50:07', '1');
INSERT INTO `files` VALUES ('93', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519890845_5a97b19dd5cd36.40972633.jpeg', 'collection/2018/03/01/img_1519890845_5a97b19dd5cd36.40972633.jpeg', 'image/jpeg', '845941', '2018-03-01 14:54:05', '2018-03-01 14:54:05', '1');
INSERT INTO `files` VALUES ('94', '5a97b2c1e41c2_1519891137_20180301.jpg', 'files/decision/2018/03/5a97b2c1e41c2_1519891137_20180301.jpg', 'files/decision/2018/03/5a97b2c1e41c2_1519891137_20180301.jpg', 'image/jpeg', '879394', '2018-03-01 14:58:57', '2018-03-01 14:58:57', '1');
INSERT INTO `files` VALUES ('95', '5a97b2cec51a6_1519891150_20180301.jpg', 'files/decision/2018/03/5a97b2cec51a6_1519891150_20180301.jpg', 'files/decision/2018/03/5a97b2cec51a6_1519891150_20180301.jpg', 'image/jpeg', '845941', '2018-03-01 14:59:10', '2018-03-01 14:59:10', '1');
INSERT INTO `files` VALUES ('96', '5a97b2d99dc40_1519891161_20180301.jpg', 'files/decision/2018/03/5a97b2d99dc40_1519891161_20180301.jpg', 'files/decision/2018/03/5a97b2d99dc40_1519891161_20180301.jpg', 'image/jpeg', '775702', '2018-03-01 14:59:21', '2018-03-01 14:59:21', '1');
INSERT INTO `files` VALUES ('97', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/01/img_1519892904_5a97b9a8e800e4.01452158.jpeg', 'collection/2018/03/01/img_1519892904_5a97b9a8e800e4.01452158.jpeg', 'image/jpeg', '780831', '2018-03-01 15:28:24', '2018-03-01 15:28:24', '1');
INSERT INTO `files` VALUES ('98', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519962556_5a98c9bc9d8475.67006973.jpeg', 'collection/2018/03/02/img_1519962556_5a98c9bc9d8475.67006973.jpeg', 'image/jpeg', '879394', '2018-03-02 10:49:16', '2018-03-02 10:49:16', '1');
INSERT INTO `files` VALUES ('99', '5a98ca9e8bb6e_1519962782_20180302.png', 'files/decision/2018/03/5a98ca9e8bb6e_1519962782_20180302.png', 'files/decision/2018/03/5a98ca9e8bb6e_1519962782_20180302.png', 'image/png', '32898', '2018-03-02 10:53:02', '2018-03-02 10:53:02', '1');
INSERT INTO `files` VALUES ('100', '5a98cb72948c1_1519962994_20180302.jpg', 'files/decision/2018/03/5a98cb72948c1_1519962994_20180302.jpg', 'files/decision/2018/03/5a98cb72948c1_1519962994_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 10:56:34', '2018-03-02 10:56:34', '1');
INSERT INTO `files` VALUES ('101', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519964559_5a98d18f08f574.04424515.jpeg', 'collection/2018/03/02/img_1519964559_5a98d18f08f574.04424515.jpeg', 'image/jpeg', '879394', '2018-03-02 11:22:39', '2018-03-02 11:22:39', '1');
INSERT INTO `files` VALUES ('102', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519964678_5a98d206bd9f80.79874393.jpeg', 'collection/2018/03/02/img_1519964678_5a98d206bd9f80.79874393.jpeg', 'image/jpeg', '879394', '2018-03-02 11:24:38', '2018-03-02 11:24:38', '1');
INSERT INTO `files` VALUES ('103', '5a98d509f3b9b_1519965449_20180302.png', 'files/decision/2018/03/5a98d509f3b9b_1519965449_20180302.png', 'files/decision/2018/03/5a98d509f3b9b_1519965449_20180302.png', 'image/png', '32898', '2018-03-02 11:37:29', '2018-03-02 11:37:29', '1');
INSERT INTO `files` VALUES ('104', '5a98f4f2d10a0_1519973618_20180302.jpg', 'files/decision/2018/03/5a98f4f2d10a0_1519973618_20180302.jpg', 'files/decision/2018/03/5a98f4f2d10a0_1519973618_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 13:53:38', '2018-03-02 13:53:38', '1');
INSERT INTO `files` VALUES ('105', '5a98f95f8ba01_1519974751_20180302.jpg', 'files/decision/2018/03/5a98f95f8ba01_1519974751_20180302.jpg', 'files/decision/2018/03/5a98f95f8ba01_1519974751_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 14:12:31', '2018-03-02 14:12:31', '1');
INSERT INTO `files` VALUES ('106', '5a98fc98e9b06_1519975576_20180302.jpg', 'files/decision/2018/03/5a98fc98e9b06_1519975576_20180302.jpg', 'files/decision/2018/03/5a98fc98e9b06_1519975576_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 14:26:16', '2018-03-02 14:26:16', '1');
INSERT INTO `files` VALUES ('107', '5a98fe4d40d03_1519976013_20180302.jpg', 'files/decision/2018/03/5a98fe4d40d03_1519976013_20180302.jpg', 'files/decision/2018/03/5a98fe4d40d03_1519976013_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 14:33:33', '2018-03-02 14:33:33', '1');
INSERT INTO `files` VALUES ('108', '5a98fe5882cb1_1519976024_20180302.jpg', 'files/decision/2018/03/5a98fe5882cb1_1519976024_20180302.jpg', 'files/decision/2018/03/5a98fe5882cb1_1519976024_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 14:33:44', '2018-03-02 14:33:44', '1');
INSERT INTO `files` VALUES ('109', '5a98fe65689ac_1519976037_20180302.jpg', 'files/decision/2018/03/5a98fe65689ac_1519976037_20180302.jpg', 'files/decision/2018/03/5a98fe65689ac_1519976037_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 14:33:57', '2018-03-02 14:33:57', '1');
INSERT INTO `files` VALUES ('110', '5a98fe700dfdb_1519976048_20180302.jpg', 'files/decision/2018/03/5a98fe700dfdb_1519976048_20180302.jpg', 'files/decision/2018/03/5a98fe700dfdb_1519976048_20180302.jpg', 'image/jpeg', '775702', '2018-03-02 14:34:08', '2018-03-02 14:34:08', '1');
INSERT INTO `files` VALUES ('111', '5a990219c9643_1519976985_20180302.jpg', 'files/decision/2018/03/5a990219c9643_1519976985_20180302.jpg', 'files/decision/2018/03/5a990219c9643_1519976985_20180302.jpg', 'image/jpeg', '845941', '2018-03-02 14:49:45', '2018-03-02 14:49:45', '1');
INSERT INTO `files` VALUES ('112', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519978071_5a99065796ac33.92452609.jpeg', 'collection/2018/03/02/img_1519978071_5a99065796ac33.92452609.jpeg', 'image/jpeg', '620888', '2018-03-02 15:07:51', '2018-03-02 15:07:51', '1');
INSERT INTO `files` VALUES ('113', 'chu thanh 2 dong.png', '/var/www/html/public/files/collection/2018/03/02/img_1519978716_5a9908dc1e12c1.64640357.png', 'collection/2018/03/02/img_1519978716_5a9908dc1e12c1.64640357.png', 'image/png', '32898', '2018-03-02 15:18:36', '2018-03-02 15:18:36', '1');
INSERT INTO `files` VALUES ('114', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519978846_5a99095e945dd7.04743105.jpeg', 'collection/2018/03/02/img_1519978846_5a99095e945dd7.04743105.jpeg', 'image/jpeg', '879394', '2018-03-02 15:20:46', '2018-03-02 15:20:46', '1');
INSERT INTO `files` VALUES ('115', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519982195_5a99167343c113.40505644.jpeg', 'collection/2018/03/02/img_1519982195_5a99167343c113.40505644.jpeg', 'image/jpeg', '879394', '2018-03-02 16:16:35', '2018-03-02 16:16:35', '1');
INSERT INTO `files` VALUES ('116', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/02/img_1519982317_5a9916edbbf7b6.45274410.jpeg', 'collection/2018/03/02/img_1519982317_5a9916edbbf7b6.45274410.jpeg', 'image/jpeg', '879394', '2018-03-02 16:18:37', '2018-03-02 16:18:37', '1');
INSERT INTO `files` VALUES ('117', '5a99189cc5369_1519982748_20180302.jpg', 'files/decision/2018/03/5a99189cc5369_1519982748_20180302.jpg', 'files/decision/2018/03/5a99189cc5369_1519982748_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 16:25:48', '2018-03-02 16:25:48', '1');
INSERT INTO `files` VALUES ('118', '5a991a4993d0a_1519983177_20180302.jpg', 'files/decision/2018/03/5a991a4993d0a_1519983177_20180302.jpg', 'files/decision/2018/03/5a991a4993d0a_1519983177_20180302.jpg', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '128991', '2018-03-02 16:32:57', '2018-03-02 16:32:57', '1');
INSERT INTO `files` VALUES ('119', '5a991a7734daa_1519983223_20180302.jpg', 'files/decision/2018/03/5a991a7734daa_1519983223_20180302.jpg', 'files/decision/2018/03/5a991a7734daa_1519983223_20180302.jpg', 'image/jpeg', '879394', '2018-03-02 16:33:43', '2018-03-02 16:33:43', '1');
INSERT INTO `files` VALUES ('120', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520221215_5a9cbc1f4d24a9.49649968.jpeg', 'collection/2018/03/05/img_1520221215_5a9cbc1f4d24a9.49649968.jpeg', 'image/jpeg', '845941', '2018-03-05 10:40:15', '2018-03-05 10:40:15', '1');
INSERT INTO `files` VALUES ('121', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520222416_5a9cc0d0b827d3.51786539.jpeg', 'collection/2018/03/05/img_1520222416_5a9cc0d0b827d3.51786539.jpeg', 'image/jpeg', '845941', '2018-03-05 11:00:16', '2018-03-05 11:00:16', '1');
INSERT INTO `files` VALUES ('122', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520225153_5a9ccb8176ff14.25095687.jpeg', 'collection/2018/03/05/img_1520225153_5a9ccb8176ff14.25095687.jpeg', 'image/jpeg', '845941', '2018-03-05 11:45:53', '2018-03-05 11:45:53', '1');
INSERT INTO `files` VALUES ('123', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520233065_5a9cea69edb9e8.98169429.jpeg', 'collection/2018/03/05/img_1520233065_5a9cea69edb9e8.98169429.jpeg', 'image/jpeg', '595284', '2018-03-05 13:57:46', '2018-03-05 13:57:46', '1');
INSERT INTO `files` VALUES ('124', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520237365_5a9cfb350571c7.04321761.jpeg', 'collection/2018/03/05/img_1520237365_5a9cfb350571c7.04321761.jpeg', 'image/jpeg', '845941', '2018-03-05 15:09:25', '2018-03-05 15:09:25', '1');
INSERT INTO `files` VALUES ('125', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520237472_5a9cfba0d8b1f6.88339861.jpeg', 'collection/2018/03/05/img_1520237472_5a9cfba0d8b1f6.88339861.jpeg', 'image/jpeg', '845941', '2018-03-05 15:11:12', '2018-03-05 15:11:12', '1');
INSERT INTO `files` VALUES ('126', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/05/img_1520242374_5a9d0ec617bca7.04319948.jpeg', 'collection/2018/03/05/img_1520242374_5a9d0ec617bca7.04319948.jpeg', 'image/jpeg', '845941', '2018-03-05 16:32:54', '2018-03-05 16:32:54', '1');
INSERT INTO `files` VALUES ('127', '5a9e0a78205dd_1520306808_20180306.jpg', 'files/decision/2018/03/5a9e0a78205dd_1520306808_20180306.jpg', 'files/decision/2018/03/5a9e0a78205dd_1520306808_20180306.jpg', 'image/jpeg', '879394', '2018-03-06 10:26:48', '2018-03-06 10:26:48', '1');
INSERT INTO `files` VALUES ('128', '5a9e0bd7e02fc_1520307159_20180306.jpg', 'files/decision/2018/03/5a9e0bd7e02fc_1520307159_20180306.jpg', 'files/decision/2018/03/5a9e0bd7e02fc_1520307159_20180306.jpg', 'image/jpeg', '879394', '2018-03-06 10:32:39', '2018-03-06 10:32:39', '1');
INSERT INTO `files` VALUES ('129', '5a9e179607d4e_1520310166_20180306.jpg', 'files/decision/2018/03/5a9e179607d4e_1520310166_20180306.jpg', 'files/decision/2018/03/5a9e179607d4e_1520310166_20180306.jpg', 'image/jpeg', '845941', '2018-03-06 11:22:46', '2018-03-06 11:22:46', '1');
INSERT INTO `files` VALUES ('130', '5a9e1a8293b6d_1520310914_20180306.jpg', 'files/decision/2018/03/5a9e1a8293b6d_1520310914_20180306.jpg', 'files/decision/2018/03/5a9e1a8293b6d_1520310914_20180306.jpg', 'image/jpeg', '845941', '2018-03-06 11:35:14', '2018-03-06 11:35:14', '1');
INSERT INTO `files` VALUES ('131', '5a9e1ae3f00f2_1520311011_20180306.jpg', 'files/decision/2018/03/5a9e1ae3f00f2_1520311011_20180306.jpg', 'files/decision/2018/03/5a9e1ae3f00f2_1520311011_20180306.jpg', 'image/jpeg', '879394', '2018-03-06 11:36:51', '2018-03-06 11:36:51', '1');
INSERT INTO `files` VALUES ('132', 'chu thanh 2 dong.png', '/var/www/html/public/files/collection/2018/03/07/img_1520385353_5a9f3d49bee204.24124243.png', 'collection/2018/03/07/img_1520385353_5a9f3d49bee204.24124243.png', 'image/png', '32898', '2018-03-07 08:15:53', '2018-03-07 08:15:53', '1');
INSERT INTO `files` VALUES ('133', '5a9f9783e075e_1520408451_20180307.jpg', 'files/decision/2018/03/5a9f9783e075e_1520408451_20180307.jpg', 'files/decision/2018/03/5a9f9783e075e_1520408451_20180307.jpg', 'image/jpeg', '595284', '2018-03-07 14:40:51', '2018-03-07 14:40:51', '1');
INSERT INTO `files` VALUES ('134', '5a9fa202af223_1520411138_20180307.jpg', 'files/decision/2018/03/5a9fa202af223_1520411138_20180307.jpg', 'files/decision/2018/03/5a9fa202af223_1520411138_20180307.jpg', 'image/jpeg', '845941', '2018-03-07 15:25:38', '2018-03-07 15:25:38', '1');
INSERT INTO `files` VALUES ('135', '5a9fa20e16f24_1520411150_20180307.jpg', 'files/decision/2018/03/5a9fa20e16f24_1520411150_20180307.jpg', 'files/decision/2018/03/5a9fa20e16f24_1520411150_20180307.jpg', 'image/jpeg', '595284', '2018-03-07 15:25:50', '2018-03-07 15:25:50', '1');
INSERT INTO `files` VALUES ('136', '5a9fa52512c13_1520411941_20180307.gif', 'files/decision/2018/03/5a9fa52512c13_1520411941_20180307.gif', 'files/decision/2018/03/5a9fa52512c13_1520411941_20180307.gif', 'image/gif', '18061', '2018-03-07 15:39:01', '2018-03-07 15:39:01', '1');
INSERT INTO `files` VALUES ('137', '5a9fa5828fd19_1520412034_20180307.jpg', 'files/decision/2018/03/5a9fa5828fd19_1520412034_20180307.jpg', 'files/decision/2018/03/5a9fa5828fd19_1520412034_20180307.jpg', 'image/jpeg', '15784', '2018-03-07 15:40:34', '2018-03-07 15:40:34', '1');
INSERT INTO `files` VALUES ('138', 'nuocmam.jpg', '/var/www/html/public/files/collection/2018/03/07/img_1520416075_5a9fb54bc99425.40677675.jpeg', 'collection/2018/03/07/img_1520416075_5a9fb54bc99425.40677675.jpeg', 'image/jpeg', '9188', '2018-03-07 16:47:55', '2018-03-07 16:47:55', '1');
INSERT INTO `files` VALUES ('139', 'lavi.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520565145_5aa1fb99e43d28.94729610.jpeg', 'collection/2018/03/09/img_1520565145_5aa1fb99e43d28.94729610.jpeg', 'image/jpeg', '6221', '2018-03-09 10:12:25', '2018-03-09 10:12:25', '1');
INSERT INTO `files` VALUES ('140', 'cakhonhanhau.png', '/var/www/html/public/files/collection/2018/03/09/img_1520568031_5aa206dfd094f3.69540712.png', 'collection/2018/03/09/img_1520568031_5aa206dfd094f3.69540712.png', 'image/png', '5727', '2018-03-09 11:00:31', '2018-03-09 11:00:31', '1');
INSERT INTO `files` VALUES ('141', 'miendong.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520568468_5aa20894eabc77.22518487.jpeg', 'collection/2018/03/09/img_1520568468_5aa20894eabc77.22518487.jpeg', 'image/jpeg', '9704', '2018-03-09 11:07:48', '2018-03-09 11:07:48', '1');
INSERT INTO `files` VALUES ('142', '`~!@#$%^&()-=_+{};\',.lavi.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520569489_5aa20c9148c256.75396917.jpeg', 'collection/2018/03/09/img_1520569489_5aa20c9148c256.75396917.jpeg', 'image/jpeg', '6221', '2018-03-09 11:24:49', '2018-03-09 11:24:49', '1');
INSERT INTO `files` VALUES ('143', 'miendong.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520570294_5aa20fb67b0ea4.60357904.jpeg', 'collection/2018/03/09/img_1520570294_5aa20fb67b0ea4.60357904.jpeg', 'image/jpeg', '9704', '2018-03-09 11:38:14', '2018-03-09 11:38:14', '1');
INSERT INTO `files` VALUES ('144', '5aa22fea458b4_1520578538_20180309.jpg', 'files/decision/2018/03/5aa22fea458b4_1520578538_20180309.jpg', 'files/decision/2018/03/5aa22fea458b4_1520578538_20180309.jpg', 'image/jpeg', '845941', '2018-03-09 13:55:38', '2018-03-09 13:55:38', '1');
INSERT INTO `files` VALUES ('145', 'daiwoo.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520578941_5aa2317d3f5dc7.95437428.jpeg', 'collection/2018/03/09/img_1520578941_5aa2317d3f5dc7.95437428.jpeg', 'image/jpeg', '6699', '2018-03-09 14:02:21', '2018-03-09 14:02:21', '1');
INSERT INTO `files` VALUES ('146', 'miendong.jpg', '/var/www/html/public/files/collection/2018/03/09/img_1520579402_5aa2334a5c8508.72610169.jpeg', 'collection/2018/03/09/img_1520579402_5aa2334a5c8508.72610169.jpeg', 'image/jpeg', '9704', '2018-03-09 14:10:02', '2018-03-09 14:10:02', '1');
INSERT INTO `files` VALUES ('147', '5aa2368f69e0a_1520580239_20180309.jpg', 'files/decision/2018/03/5aa2368f69e0a_1520580239_20180309.jpg', 'files/decision/2018/03/5aa2368f69e0a_1520580239_20180309.jpg', 'image/jpeg', '595284', '2018-03-09 14:23:59', '2018-03-09 14:23:59', '1');
INSERT INTO `files` VALUES ('148', '5aa2379d5c714_1520580509_20180309.jpg', 'files/decision/2018/03/5aa2379d5c714_1520580509_20180309.jpg', 'files/decision/2018/03/5aa2379d5c714_1520580509_20180309.jpg', 'image/jpeg', '845941', '2018-03-09 14:28:29', '2018-03-09 14:28:29', '1');
INSERT INTO `files` VALUES ('149', '5aa237c693c9a_1520580550_20180309.jpg', 'files/decision/2018/03/5aa237c693c9a_1520580550_20180309.jpg', 'files/decision/2018/03/5aa237c693c9a_1520580550_20180309.jpg', 'image/jpeg', '845941', '2018-03-09 14:29:10', '2018-03-09 14:29:10', '1');
INSERT INTO `files` VALUES ('150', '5aa238a357a35_1520580771_20180309.jpg', 'files/decision/2018/03/5aa238a357a35_1520580771_20180309.jpg', 'files/decision/2018/03/5aa238a357a35_1520580771_20180309.jpg', 'image/jpeg', '845941', '2018-03-09 14:32:51', '2018-03-09 14:32:51', '1');
INSERT INTO `files` VALUES ('151', '5aa2395c9580d_1520580956_20180309.jpg', 'files/decision/2018/03/5aa2395c9580d_1520580956_20180309.jpg', 'files/decision/2018/03/5aa2395c9580d_1520580956_20180309.jpg', 'image/jpeg', '595284', '2018-03-09 14:35:56', '2018-03-09 14:35:56', '1');
INSERT INTO `files` VALUES ('152', '5aa23b35c76d2_1520581429_20180309.mp4', 'files/decision/2018/03/5aa23b35c76d2_1520581429_20180309.mp4', 'files/decision/2018/03/5aa23b35c76d2_1520581429_20180309.mp4', 'video/mp4', '1892700', '2018-03-09 14:43:49', '2018-03-09 14:43:49', '1');
INSERT INTO `files` VALUES ('153', '5aa23c2cb1a2e_1520581676_20180309.jpg', 'files/decision/2018/03/5aa23c2cb1a2e_1520581676_20180309.jpg', 'files/decision/2018/03/5aa23c2cb1a2e_1520581676_20180309.jpg', 'image/jpeg', '879394', '2018-03-09 14:47:56', '2018-03-09 14:47:56', '1');
INSERT INTO `files` VALUES ('154', '5aa23c4c6581e_1520581708_20180309.jpg', 'files/decision/2018/03/5aa23c4c6581e_1520581708_20180309.jpg', 'files/decision/2018/03/5aa23c4c6581e_1520581708_20180309.jpg', 'image/jpeg', '879394', '2018-03-09 14:48:28', '2018-03-09 14:48:28', '1');
INSERT INTO `files` VALUES ('155', '5aa23cadcd00f_1520581805_20180309.jpg', 'files/decision/2018/03/5aa23cadcd00f_1520581805_20180309.jpg', 'files/decision/2018/03/5aa23cadcd00f_1520581805_20180309.jpg', 'image/jpeg', '595284', '2018-03-09 14:50:05', '2018-03-09 14:50:05', '1');
INSERT INTO `files` VALUES ('156', '5aa335a9c9f04_1520645545_20180310.jpg', 'files/decision/2018/03/5aa335a9c9f04_1520645545_20180310.jpg', 'files/decision/2018/03/5aa335a9c9f04_1520645545_20180310.jpg', 'image/jpeg', '9188', '2018-03-10 08:32:25', '2018-03-10 08:32:25', '1');
INSERT INTO `files` VALUES ('157', 'nuocmam.jpg', '/var/www/html/public/files/collection/2018/03/10/img_1520647994_5aa33f3a725ab0.20033195.jpeg', 'collection/2018/03/10/img_1520647994_5aa33f3a725ab0.20033195.jpeg', 'image/jpeg', '9188', '2018-03-10 09:13:14', '2018-03-10 09:13:14', '1');
INSERT INTO `files` VALUES ('158', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/10/img_1520666850_5aa388e2407c94.18538097.jpeg', 'collection/2018/03/10/img_1520666850_5aa388e2407c94.18538097.jpeg', 'image/jpeg', '845941', '2018-03-10 14:27:30', '2018-03-10 14:27:30', '1');
INSERT INTO `files` VALUES ('159', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/10/img_1520667124_5aa389f4c405e1.87659274.jpeg', 'collection/2018/03/10/img_1520667124_5aa389f4c405e1.87659274.jpeg', 'image/jpeg', '845941', '2018-03-10 14:32:04', '2018-03-10 14:32:04', '1');
INSERT INTO `files` VALUES ('160', 'girl-bikini-4.jpg', '/var/www/html/public/files/collection/2018/03/11/img_1520786114_5aa55ac251b5c9.62013960.jpeg', 'collection/2018/03/11/img_1520786114_5aa55ac251b5c9.62013960.jpeg', 'image/jpeg', '37501', '2018-03-11 23:35:14', '2018-03-11 23:35:14', '1');
INSERT INTO `files` VALUES ('161', 'girl-bikini-4.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520820381_5aa5e09dd7f246.25060163.jpeg', 'collection/2018/03/12/img_1520820381_5aa5e09dd7f246.25060163.jpeg', 'image/jpeg', '37501', '2018-03-12 09:06:21', '2018-03-12 09:06:21', '1');
INSERT INTO `files` VALUES ('162', 'girl-bikini-4.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520820592_5aa5e170cbf3f5.94477133.jpeg', 'collection/2018/03/12/img_1520820592_5aa5e170cbf3f5.94477133.jpeg', 'image/jpeg', '37501', '2018-03-12 09:09:52', '2018-03-12 09:09:52', '1');
INSERT INTO `files` VALUES ('163', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/12/img_1520824009_5aa5eec9d528d2.10330981.png', 'collection/2018/03/12/img_1520824009_5aa5eec9d528d2.10330981.png', 'image/png', '98765', '2018-03-12 10:06:49', '2018-03-12 10:06:49', '1');
INSERT INTO `files` VALUES ('164', 'moi cau.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520824024_5aa5eed88f5bc8.16742004.jpeg', 'collection/2018/03/12/img_1520824024_5aa5eed88f5bc8.16742004.jpeg', 'image/jpeg', '452536', '2018-03-12 10:07:04', '2018-03-12 10:07:04', '1');
INSERT INTO `files` VALUES ('165', 'mat-bang-biet-thu-1024x683.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825011_5aa5f2b38129b8.09329099.jpeg', 'collection/2018/03/12/img_1520825011_5aa5f2b38129b8.09329099.jpeg', 'image/jpeg', '129581', '2018-03-12 10:23:31', '2018-03-12 10:23:31', '1');
INSERT INTO `files` VALUES ('166', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825338_5aa5f3fa1c05a8.34990960.jpeg', 'collection/2018/03/12/img_1520825338_5aa5f3fa1c05a8.34990960.jpeg', 'image/jpeg', '1806735', '2018-03-12 10:28:58', '2018-03-12 10:28:58', '1');
INSERT INTO `files` VALUES ('167', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825526_5aa5f4b6890063.11018833.jpeg', 'collection/2018/03/12/img_1520825526_5aa5f4b6890063.11018833.jpeg', 'image/jpeg', '1806735', '2018-03-12 10:32:06', '2018-03-12 10:32:06', '1');
INSERT INTO `files` VALUES ('168', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825550_5aa5f4ce8eb7f6.84416229.jpeg', 'collection/2018/03/12/img_1520825550_5aa5f4ce8eb7f6.84416229.jpeg', 'image/jpeg', '1806735', '2018-03-12 10:32:30', '2018-03-12 10:32:30', '1');
INSERT INTO `files` VALUES ('169', '20170421_155954.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520825740_5aa5f58c1700c6.32845773.jpeg', 'collection/2018/03/12/img_1520825740_5aa5f58c1700c6.32845773.jpeg', 'image/jpeg', '2801905', '2018-03-12 10:35:40', '2018-03-12 10:35:40', '1');
INSERT INTO `files` VALUES ('170', '5aa5f627041c7_1520825895_20180312.png', 'files/decision/2018/03/5aa5f627041c7_1520825895_20180312.png', 'files/decision/2018/03/5aa5f627041c7_1520825895_20180312.png', 'image/png', '362909', '2018-03-12 10:38:15', '2018-03-12 10:38:15', '1');
INSERT INTO `files` VALUES ('171', '5aa5f64e1132b_1520825934_20180312.png', 'files/decision/2018/03/5aa5f64e1132b_1520825934_20180312.png', 'files/decision/2018/03/5aa5f64e1132b_1520825934_20180312.png', 'image/png', '362909', '2018-03-12 10:38:54', '2018-03-12 10:38:54', '1');
INSERT INTO `files` VALUES ('172', '5aa5f884ccdaf_1520826500_20180312.png', 'files/decision/2018/03/5aa5f884ccdaf_1520826500_20180312.png', 'files/decision/2018/03/5aa5f884ccdaf_1520826500_20180312.png', 'image/png', '362909', '2018-03-12 10:48:20', '2018-03-12 10:48:20', '1');
INSERT INTO `files` VALUES ('173', '5aa5f941397f3_1520826689_20180312.mp4', 'files/decision/2018/03/5aa5f941397f3_1520826689_20180312.mp4', 'files/decision/2018/03/5aa5f941397f3_1520826689_20180312.mp4', 'video/mp4', '1385735', '2018-03-12 10:51:29', '2018-03-12 10:51:29', '1');
INSERT INTO `files` VALUES ('174', '5aa5fd8262464_1520827778_20180312.exe', 'files/decision/2018/03/5aa5fd8262464_1520827778_20180312.exe', 'files/decision/2018/03/5aa5fd8262464_1520827778_20180312.exe', 'application/x-dosexec', '54272', '2018-03-12 11:09:38', '2018-03-12 11:09:38', '1');
INSERT INTO `files` VALUES ('175', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/12/img_1520829929_5aa605e93aa0a6.29410782.png', 'collection/2018/03/12/img_1520829929_5aa605e93aa0a6.29410782.png', 'image/png', '98765', '2018-03-12 11:45:29', '2018-03-12 11:45:29', '1');
INSERT INTO `files` VALUES ('176', '20170421_155726.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520830278_5aa60746b679e9.13138608.jpeg', 'collection/2018/03/12/img_1520830278_5aa60746b679e9.13138608.jpeg', 'image/jpeg', '3938333', '2018-03-12 11:51:19', '2018-03-12 11:51:19', '1');
INSERT INTO `files` VALUES ('177', '5aa620c993899_1520836809_20180312.png', 'files/decision/2018/03/5aa620c993899_1520836809_20180312.png', 'files/decision/2018/03/5aa620c993899_1520836809_20180312.png', 'image/png', '185324', '2018-03-12 13:40:09', '2018-03-12 13:40:09', '1');
INSERT INTO `files` VALUES ('178', '20170421_155726.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520837202_5aa622524a4552.42640908.jpeg', 'collection/2018/03/12/img_1520837202_5aa622524a4552.42640908.jpeg', 'image/jpeg', '3938333', '2018-03-12 13:46:42', '2018-03-12 13:46:42', '1');
INSERT INTO `files` VALUES ('179', '5aa624014f034_1520837633_20180312.png', 'files/decision/2018/03/5aa624014f034_1520837633_20180312.png', 'files/decision/2018/03/5aa624014f034_1520837633_20180312.png', 'image/png', '362909', '2018-03-12 13:53:53', '2018-03-12 13:53:53', '1');
INSERT INTO `files` VALUES ('180', '20170421_155954.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520838247_5aa62668005810.80383000.jpeg', 'collection/2018/03/12/img_1520838247_5aa62668005810.80383000.jpeg', 'image/jpeg', '2801905', '2018-03-12 14:04:08', '2018-03-12 14:04:08', '1');
INSERT INTO `files` VALUES ('181', 'cover.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520839618_5aa62bc2a2f2e6.95922032.jpeg', 'collection/2018/03/12/img_1520839618_5aa62bc2a2f2e6.95922032.jpeg', 'image/jpeg', '3960829', '2018-03-12 14:26:59', '2018-03-12 14:26:59', '1');
INSERT INTO `files` VALUES ('182', 'IMG_6492.JPG', '/var/www/html/public/files/collection/2018/03/12/img_1520840435_5aa62ef3af07e7.37981969.jpeg', 'collection/2018/03/12/img_1520840435_5aa62ef3af07e7.37981969.jpeg', 'image/jpeg', '3017162', '2018-03-12 14:40:36', '2018-03-12 14:40:36', '1');
INSERT INTO `files` VALUES ('183', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/12/img_1520841181_5aa631dd871cb0.81085817.png', 'collection/2018/03/12/img_1520841181_5aa631dd871cb0.81085817.png', 'image/png', '362909', '2018-03-12 14:53:01', '2018-03-12 14:53:01', '1');
INSERT INTO `files` VALUES ('184', 'Benh vien nhiet doi.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520841252_5aa632245c55e3.08665471.jpeg', 'collection/2018/03/12/img_1520841252_5aa632245c55e3.08665471.jpeg', 'image/jpeg', '2539128', '2018-03-12 14:54:12', '2018-03-12 14:54:12', '1');
INSERT INTO `files` VALUES ('185', '20170421_155954.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520841452_5aa632ec5c75c1.33950061.jpeg', 'collection/2018/03/12/img_1520841452_5aa632ec5c75c1.33950061.jpeg', 'image/jpeg', '2801905', '2018-03-12 14:57:32', '2018-03-12 14:57:32', '1');
INSERT INTO `files` VALUES ('186', 'cover.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520841643_5aa633ab1ea1f5.39662048.jpeg', 'collection/2018/03/12/img_1520841643_5aa633ab1ea1f5.39662048.jpeg', 'image/jpeg', '3960829', '2018-03-12 15:00:43', '2018-03-12 15:00:43', '1');
INSERT INTO `files` VALUES ('187', 'Screenshot from 2018-02-11 01-16-37.png', '/var/www/html/public/files/collection/2018/03/12/img_1520841954_5aa634e233f7a8.11462606.png', 'collection/2018/03/12/img_1520841954_5aa634e233f7a8.11462606.png', 'image/png', '31429', '2018-03-12 15:05:54', '2018-03-12 15:05:54', '1');
INSERT INTO `files` VALUES ('188', 'Screenshot from 2018-01-11 22-32-15.png', '/var/www/html/public/files/collection/2018/03/12/img_1520842313_5aa63649307cc6.91399541.png', 'collection/2018/03/12/img_1520842313_5aa63649307cc6.91399541.png', 'image/png', '19066', '2018-03-12 15:11:53', '2018-03-12 15:11:53', '1');
INSERT INTO `files` VALUES ('189', '5aa638d67b668_1520842966_20180312.png', 'files/decision/2018/03/5aa638d67b668_1520842966_20180312.png', 'files/decision/2018/03/5aa638d67b668_1520842966_20180312.png', 'image/png', '362909', '2018-03-12 15:22:46', '2018-03-12 15:22:46', '1');
INSERT INTO `files` VALUES ('190', '5aa641f298a7d_1520845298_20180312.png', 'files/decision/2018/03/5aa641f298a7d_1520845298_20180312.png', 'files/decision/2018/03/5aa641f298a7d_1520845298_20180312.png', 'image/png', '362909', '2018-03-12 16:01:38', '2018-03-12 16:01:38', '1');
INSERT INTO `files` VALUES ('191', '5aa641fcae0cd_1520845308_20180312.png', 'files/decision/2018/03/5aa641fcae0cd_1520845308_20180312.png', 'files/decision/2018/03/5aa641fcae0cd_1520845308_20180312.png', 'image/png', '362909', '2018-03-12 16:01:48', '2018-03-12 16:01:48', '1');
INSERT INTO `files` VALUES ('192', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/12/img_1520846677_5aa647559d2656.87996344.png', 'collection/2018/03/12/img_1520846677_5aa647559d2656.87996344.png', 'image/png', '15927', '2018-03-12 16:24:37', '2018-03-12 16:24:37', '1');
INSERT INTO `files` VALUES ('193', '20180224_213716.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520846704_5aa64770659461.73705221.jpeg', 'collection/2018/03/12/img_1520846704_5aa64770659461.73705221.jpeg', 'image/jpeg', '2292385', '2018-03-12 16:25:04', '2018-03-12 16:25:04', '1');
INSERT INTO `files` VALUES ('194', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/12/img_1520846893_5aa6482daf5ba3.20341892.png', 'collection/2018/03/12/img_1520846893_5aa6482daf5ba3.20341892.png', 'image/png', '362909', '2018-03-12 16:28:13', '2018-03-12 16:28:13', '1');
INSERT INTO `files` VALUES ('195', '20180224_213716.jpg', '/var/www/html/public/files/collection/2018/03/12/img_1520846903_5aa648372ddc10.92822141.jpeg', 'collection/2018/03/12/img_1520846903_5aa648372ddc10.92822141.jpeg', 'image/jpeg', '2292385', '2018-03-12 16:28:23', '2018-03-12 16:28:23', '1');
INSERT INTO `files` VALUES ('196', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/12/img_1520847355_5aa649fb7a3939.83603813.png', 'collection/2018/03/12/img_1520847355_5aa649fb7a3939.83603813.png', 'image/png', '15927', '2018-03-12 16:35:55', '2018-03-12 16:35:55', '1');
INSERT INTO `files` VALUES ('197', '5aa655d86542b_1520850392_20180312.png', 'files/decision/2018/03/5aa655d86542b_1520850392_20180312.png', 'files/decision/2018/03/5aa655d86542b_1520850392_20180312.png', 'image/png', '362909', '2018-03-12 17:26:32', '2018-03-12 17:26:32', '1');
INSERT INTO `files` VALUES ('198', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520907817_5aa736299ea2a8.76562326.jpeg', 'collection/2018/03/13/img_1520907817_5aa736299ea2a8.76562326.jpeg', 'image/jpeg', '1806735', '2018-03-13 09:23:37', '2018-03-13 09:23:37', '1');
INSERT INTO `files` VALUES ('199', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520909827_5aa73e031a8872.78818463.jpeg', 'collection/2018/03/13/img_1520909827_5aa73e031a8872.78818463.jpeg', 'image/jpeg', '1806735', '2018-03-13 09:57:07', '2018-03-13 09:57:07', '1');
INSERT INTO `files` VALUES ('200', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520910071_5aa73ef73e9a37.58212534.jpeg', 'collection/2018/03/13/img_1520910071_5aa73ef73e9a37.58212534.jpeg', 'image/jpeg', '1806735', '2018-03-13 10:01:11', '2018-03-13 10:01:11', '1');
INSERT INTO `files` VALUES ('201', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520910196_5aa73f74c77975.74480818.png', 'collection/2018/03/13/img_1520910196_5aa73f74c77975.74480818.png', 'image/png', '362909', '2018-03-13 10:03:16', '2018-03-13 10:03:16', '1');
INSERT INTO `files` VALUES ('202', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520910601_5aa741091ede56.60651325.png', 'collection/2018/03/13/img_1520910601_5aa741091ede56.60651325.png', 'image/png', '362909', '2018-03-13 10:10:01', '2018-03-13 10:10:01', '1');
INSERT INTO `files` VALUES ('203', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520910634_5aa7412a0c2a93.69600324.png', 'collection/2018/03/13/img_1520910634_5aa7412a0c2a93.69600324.png', 'image/png', '362909', '2018-03-13 10:10:34', '2018-03-13 10:10:34', '1');
INSERT INTO `files` VALUES ('204', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520910754_5aa741a2063095.01134530.jpeg', 'collection/2018/03/13/img_1520910754_5aa741a2063095.01134530.jpeg', 'image/jpeg', '1806735', '2018-03-13 10:12:34', '2018-03-13 10:12:34', '1');
INSERT INTO `files` VALUES ('205', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520912102_5aa746e6aa7ad6.23260536.jpeg', 'collection/2018/03/13/img_1520912102_5aa746e6aa7ad6.23260536.jpeg', 'image/jpeg', '1806735', '2018-03-13 10:35:02', '2018-03-13 10:35:02', '1');
INSERT INTO `files` VALUES ('206', '5aa74f0cdb888_1520914188_20180313.pdf', 'files/decision/2018/03/5aa74f0cdb888_1520914188_20180313.pdf', 'files/decision/2018/03/5aa74f0cdb888_1520914188_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:09:48', '2018-03-13 11:09:48', '1');
INSERT INTO `files` VALUES ('207', '5aa7509d7ae9d_1520914589_20180313.pdf', 'files/decision/2018/03/5aa7509d7ae9d_1520914589_20180313.pdf', 'files/decision/2018/03/5aa7509d7ae9d_1520914589_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:16:29', '2018-03-13 11:16:29', '1');
INSERT INTO `files` VALUES ('208', '5aa750b0e4fb0_1520914608_20180313.png', 'files/decision/2018/03/5aa750b0e4fb0_1520914608_20180313.png', 'files/decision/2018/03/5aa750b0e4fb0_1520914608_20180313.png', 'image/png', '163127', '2018-03-13 11:16:48', '2018-03-13 11:16:48', '1');
INSERT INTO `files` VALUES ('209', '5aa750bd5ec74_1520914621_20180313.png', 'files/decision/2018/03/5aa750bd5ec74_1520914621_20180313.png', 'files/decision/2018/03/5aa750bd5ec74_1520914621_20180313.png', 'image/png', '102121', '2018-03-13 11:17:01', '2018-03-13 11:17:01', '1');
INSERT INTO `files` VALUES ('210', '5aa7521ae41c7_1520914970_20180313.jpg', 'files/decision/2018/03/5aa7521ae41c7_1520914970_20180313.jpg', 'files/decision/2018/03/5aa7521ae41c7_1520914970_20180313.jpg', 'video/mp4', '1385735', '2018-03-13 11:21:07', '2018-03-13 11:22:50', '1');
INSERT INTO `files` VALUES ('211', '5aa7549ef1dbf_1520915614_20180313.pdf', 'files/decision/2018/03/5aa7549ef1dbf_1520915614_20180313.pdf', 'files/decision/2018/03/5aa7549ef1dbf_1520915614_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:33:34', '2018-03-13 11:33:34', '1');
INSERT INTO `files` VALUES ('212', '5aa754b10691c_1520915633_20180313.pdf', 'files/decision/2018/03/5aa754b10691c_1520915633_20180313.pdf', 'files/decision/2018/03/5aa754b10691c_1520915633_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:33:53', '2018-03-13 11:33:53', '1');
INSERT INTO `files` VALUES ('213', '5aa754bf3f2bb_1520915647_20180313.pdf', 'files/decision/2018/03/5aa754bf3f2bb_1520915647_20180313.pdf', 'files/decision/2018/03/5aa754bf3f2bb_1520915647_20180313.pdf', 'application/pdf', '6510534', '2018-03-13 11:34:07', '2018-03-13 11:34:07', '1');
INSERT INTO `files` VALUES ('214', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520916141_5aa756ad53b774.87680943.png', 'collection/2018/03/13/img_1520916141_5aa756ad53b774.87680943.png', 'image/png', '362909', '2018-03-13 11:42:21', '2018-03-13 11:42:21', '1');
INSERT INTO `files` VALUES ('215', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520924242_5aa776520efc94.72533112.png', 'collection/2018/03/13/img_1520924242_5aa776520efc94.72533112.png', 'image/png', '362909', '2018-03-13 13:57:22', '2018-03-13 13:57:22', '1');
INSERT INTO `files` VALUES ('216', 'Benh vien nhiet doi.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520924322_5aa776a27461f1.96027366.jpeg', 'collection/2018/03/13/img_1520924322_5aa776a27461f1.96027366.jpeg', 'image/jpeg', '2539128', '2018-03-13 13:58:42', '2018-03-13 13:58:42', '1');
INSERT INTO `files` VALUES ('217', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520925126_5aa779c62515f7.62061228.png', 'collection/2018/03/13/img_1520925126_5aa779c62515f7.62061228.png', 'image/png', '362909', '2018-03-13 14:12:06', '2018-03-13 14:12:06', '1');
INSERT INTO `files` VALUES ('218', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520925411_5aa77ae338ecc5.08126084.png', 'collection/2018/03/13/img_1520925411_5aa77ae338ecc5.08126084.png', 'image/png', '362909', '2018-03-13 14:16:51', '2018-03-13 14:16:51', '1');
INSERT INTO `files` VALUES ('219', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520925624_5aa77bb8c88a67.25408616.png', 'collection/2018/03/13/img_1520925624_5aa77bb8c88a67.25408616.png', 'image/png', '362909', '2018-03-13 14:20:24', '2018-03-13 14:20:24', '1');
INSERT INTO `files` VALUES ('220', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/13/img_1520925662_5aa77bde6d8a10.88546578.png', 'collection/2018/03/13/img_1520925662_5aa77bde6d8a10.88546578.png', 'image/png', '362909', '2018-03-13 14:21:02', '2018-03-13 14:21:02', '1');
INSERT INTO `files` VALUES ('221', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520925796_5aa77c64b53198.22224956.jpeg', 'collection/2018/03/13/img_1520925796_5aa77c64b53198.22224956.jpeg', 'image/jpeg', '1806735', '2018-03-13 14:23:16', '2018-03-13 14:23:16', '1');
INSERT INTO `files` VALUES ('222', '5aa77f1047c79_1520926480_20180313.png', 'files/decision/2018/03/5aa77f1047c79_1520926480_20180313.png', 'files/decision/2018/03/5aa77f1047c79_1520926480_20180313.png', 'image/png', '362909', '2018-03-13 14:34:40', '2018-03-13 14:34:40', '1');
INSERT INTO `files` VALUES ('223', '5aa77f1b3b912_1520926491_20180313.png', 'files/decision/2018/03/5aa77f1b3b912_1520926491_20180313.png', 'files/decision/2018/03/5aa77f1b3b912_1520926491_20180313.png', 'image/png', '362909', '2018-03-13 14:34:51', '2018-03-13 14:34:51', '1');
INSERT INTO `files` VALUES ('224', '5aa77f4360c47_1520926531_20180313.png', 'files/decision/2018/03/5aa77f4360c47_1520926531_20180313.png', 'files/decision/2018/03/5aa77f4360c47_1520926531_20180313.png', 'image/png', '362909', '2018-03-13 14:35:31', '2018-03-13 14:35:31', '1');
INSERT INTO `files` VALUES ('225', '5aa77f4e4ed04_1520926542_20180313.png', 'files/decision/2018/03/5aa77f4e4ed04_1520926542_20180313.png', 'files/decision/2018/03/5aa77f4e4ed04_1520926542_20180313.png', 'image/png', '362909', '2018-03-13 14:35:42', '2018-03-13 14:35:42', '1');
INSERT INTO `files` VALUES ('226', '5aa77f59bae2a_1520926553_20180313.png', 'files/decision/2018/03/5aa77f59bae2a_1520926553_20180313.png', 'files/decision/2018/03/5aa77f59bae2a_1520926553_20180313.png', 'image/png', '362909', '2018-03-13 14:35:53', '2018-03-13 14:35:53', '1');
INSERT INTO `files` VALUES ('227', '5aa77f69c36cf_1520926569_20180313.png', 'files/decision/2018/03/5aa77f69c36cf_1520926569_20180313.png', 'files/decision/2018/03/5aa77f69c36cf_1520926569_20180313.png', 'image/png', '362909', '2018-03-13 14:36:09', '2018-03-13 14:36:09', '1');
INSERT INTO `files` VALUES ('228', '5aa77f763f0f6_1520926582_20180313.png', 'files/decision/2018/03/5aa77f763f0f6_1520926582_20180313.png', 'files/decision/2018/03/5aa77f763f0f6_1520926582_20180313.png', 'image/png', '362909', '2018-03-13 14:36:22', '2018-03-13 14:36:22', '1');
INSERT INTO `files` VALUES ('229', '5aa77f8da6c04_1520926605_20180313.png', 'files/decision/2018/03/5aa77f8da6c04_1520926605_20180313.png', 'files/decision/2018/03/5aa77f8da6c04_1520926605_20180313.png', 'image/png', '362909', '2018-03-13 14:36:45', '2018-03-13 14:36:45', '1');
INSERT INTO `files` VALUES ('230', '5aa782777deb7_1520927351_20180313.png', 'files/decision/2018/03/5aa782777deb7_1520927351_20180313.png', 'files/decision/2018/03/5aa782777deb7_1520927351_20180313.png', 'image/png', '362909', '2018-03-13 14:49:11', '2018-03-13 14:49:11', '1');
INSERT INTO `files` VALUES ('231', '5aa783448a9f3_1520927556_20180313.png', 'files/decision/2018/03/5aa783448a9f3_1520927556_20180313.png', 'files/decision/2018/03/5aa783448a9f3_1520927556_20180313.png', 'image/png', '362909', '2018-03-13 14:52:36', '2018-03-13 14:52:36', '1');
INSERT INTO `files` VALUES ('232', 'new-field-group.jpg', '/var/www/html/public/files/collection/2018/03/13/img_1520954904_5aa7ee18e17c42.56431147.jpeg', 'collection/2018/03/13/img_1520954904_5aa7ee18e17c42.56431147.jpeg', 'image/jpeg', '53097', '2018-03-13 22:28:24', '2018-03-13 22:28:24', '1');
INSERT INTO `files` VALUES ('233', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1520995033_5aa88ad9d7e3b4.77550918.png', 'collection/2018/03/14/img_1520995033_5aa88ad9d7e3b4.77550918.png', 'image/png', '98765', '2018-03-14 09:37:13', '2018-03-14 09:37:13', '1');
INSERT INTO `files` VALUES ('234', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1520995370_5aa88c2ad091d4.55577605.png', 'collection/2018/03/14/img_1520995370_5aa88c2ad091d4.55577605.png', 'image/png', '362909', '2018-03-14 09:42:50', '2018-03-14 09:42:50', '1');
INSERT INTO `files` VALUES ('235', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1520998304_5aa897a0b0f934.61956797.png', 'collection/2018/03/14/img_1520998304_5aa897a0b0f934.61956797.png', 'image/png', '362909', '2018-03-14 10:31:44', '2018-03-14 10:31:44', '1');
INSERT INTO `files` VALUES ('236', '5aa897a1c53ac_1520998305_20180314.pdf', 'files/decision/2018/03/5aa897a1c53ac_1520998305_20180314.pdf', 'files/decision/2018/03/5aa897a1c53ac_1520998305_20180314.pdf', 'application/pdf', '64955', '2018-03-14 10:31:45', '2018-03-14 10:31:45', '1');
INSERT INTO `files` VALUES ('237', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1520998448_5aa89830333874.71676028.png', 'collection/2018/03/14/img_1520998448_5aa89830333874.71676028.png', 'image/png', '362909', '2018-03-14 10:34:08', '2018-03-14 10:34:08', '1');
INSERT INTO `files` VALUES ('238', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1520998492_5aa8985d0022a8.21722310.jpeg', 'collection/2018/03/14/img_1520998492_5aa8985d0022a8.21722310.jpeg', 'image/jpeg', '1806735', '2018-03-14 10:34:53', '2018-03-14 10:34:53', '1');
INSERT INTO `files` VALUES ('239', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1520998764_5aa8996cd76048.57500578.jpeg', 'collection/2018/03/14/img_1520998764_5aa8996cd76048.57500578.jpeg', 'image/jpeg', '1806735', '2018-03-14 10:39:25', '2018-03-14 10:39:25', '1');
INSERT INTO `files` VALUES ('240', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1520999922_5aa89df264c614.53045359.png', 'collection/2018/03/14/img_1520999922_5aa89df264c614.53045359.png', 'image/png', '362909', '2018-03-14 10:58:42', '2018-03-14 10:58:42', '1');
INSERT INTO `files` VALUES ('241', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000074_5aa89e8ab8c867.46209850.jpeg', 'collection/2018/03/14/img_1521000074_5aa89e8ab8c867.46209850.jpeg', 'image/jpeg', '1806735', '2018-03-14 11:01:14', '2018-03-14 11:01:14', '1');
INSERT INTO `files` VALUES ('242', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000092_5aa89e9cab2b26.23505432.png', 'collection/2018/03/14/img_1521000092_5aa89e9cab2b26.23505432.png', 'image/png', '362909', '2018-03-14 11:01:32', '2018-03-14 11:01:32', '1');
INSERT INTO `files` VALUES ('243', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000157_5aa89edd3e2789.18421697.png', 'collection/2018/03/14/img_1521000157_5aa89edd3e2789.18421697.png', 'image/png', '362909', '2018-03-14 11:02:37', '2018-03-14 11:02:37', '1');
INSERT INTO `files` VALUES ('244', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000293_5aa89f6505ab40.45438842.jpeg', 'collection/2018/03/14/img_1521000293_5aa89f6505ab40.45438842.jpeg', 'image/jpeg', '1806735', '2018-03-14 11:04:53', '2018-03-14 11:04:53', '1');
INSERT INTO `files` VALUES ('245', 'Benh vien nhiet doi.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000303_5aa89f6f1dde48.51656426.jpeg', 'collection/2018/03/14/img_1521000303_5aa89f6f1dde48.51656426.jpeg', 'image/jpeg', '2539128', '2018-03-14 11:05:03', '2018-03-14 11:05:03', '1');
INSERT INTO `files` VALUES ('246', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000306_5aa89f721a09c5.11702751.png', 'collection/2018/03/14/img_1521000306_5aa89f721a09c5.11702751.png', 'image/png', '362909', '2018-03-14 11:05:06', '2018-03-14 11:05:06', '1');
INSERT INTO `files` VALUES ('247', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000499_5aa8a033538b97.16579189.png', 'collection/2018/03/14/img_1521000499_5aa8a033538b97.16579189.png', 'image/png', '362909', '2018-03-14 11:08:19', '2018-03-14 11:08:19', '1');
INSERT INTO `files` VALUES ('248', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000510_5aa8a03e918673.63219191.png', 'collection/2018/03/14/img_1521000510_5aa8a03e918673.63219191.png', 'image/png', '362909', '2018-03-14 11:08:30', '2018-03-14 11:08:30', '1');
INSERT INTO `files` VALUES ('249', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000611_5aa8a0a3be9418.75209168.png', 'collection/2018/03/14/img_1521000611_5aa8a0a3be9418.75209168.png', 'image/png', '362909', '2018-03-14 11:10:11', '2018-03-14 11:10:11', '1');
INSERT INTO `files` VALUES ('250', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521000718_5aa8a10e36ca45.30924792.png', 'collection/2018/03/14/img_1521000718_5aa8a10e36ca45.30924792.png', 'image/png', '362909', '2018-03-14 11:11:58', '2018-03-14 11:11:58', '1');
INSERT INTO `files` VALUES ('251', '20170421_155726.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000776_5aa8a148b02fa4.58657616.jpeg', 'collection/2018/03/14/img_1521000776_5aa8a148b02fa4.58657616.jpeg', 'image/jpeg', '3938333', '2018-03-14 11:12:56', '2018-03-14 11:12:56', '1');
INSERT INTO `files` VALUES ('252', '20170421_155954.jpg', '/var/www/html/public/files/collection/2018/03/14/img_1521000979_5aa8a213a90524.43442784.jpeg', 'collection/2018/03/14/img_1521000979_5aa8a213a90524.43442784.jpeg', 'image/jpeg', '2801905', '2018-03-14 11:16:19', '2018-03-14 11:16:19', '1');
INSERT INTO `files` VALUES ('253', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521001094_5aa8a286a6e802.64848611.png', 'collection/2018/03/14/img_1521001094_5aa8a286a6e802.64848611.png', 'image/png', '98765', '2018-03-14 11:18:14', '2018-03-14 11:18:14', '1');
INSERT INTO `files` VALUES ('254', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521001300_5aa8a354bf9383.26169011.png', 'collection/2018/03/14/img_1521001300_5aa8a354bf9383.26169011.png', 'image/png', '98765', '2018-03-14 11:21:40', '2018-03-14 11:21:40', '1');
INSERT INTO `files` VALUES ('255', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521001418_5aa8a3ca7cb000.08155502.png', 'collection/2018/03/14/img_1521001418_5aa8a3ca7cb000.08155502.png', 'image/png', '98765', '2018-03-14 11:23:38', '2018-03-14 11:23:38', '1');
INSERT INTO `files` VALUES ('256', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521001456_5aa8a3f0885c33.93377903.png', 'collection/2018/03/14/img_1521001456_5aa8a3f0885c33.93377903.png', 'image/png', '98765', '2018-03-14 11:24:16', '2018-03-14 11:24:16', '1');
INSERT INTO `files` VALUES ('257', '5aa8d0285e502_1521012776_20180314.png', 'files/decision/2018/03/5aa8d0285e502_1521012776_20180314.png', 'files/decision/2018/03/5aa8d0285e502_1521012776_20180314.png', 'image/png', '98765', '2018-03-14 14:32:56', '2018-03-14 14:32:56', '1');
INSERT INTO `files` VALUES ('258', '5aa8d32ac27e2_1521013546_20180314.png', 'files/decision/2018/03/5aa8d32ac27e2_1521013546_20180314.png', 'files/decision/2018/03/5aa8d32ac27e2_1521013546_20180314.png', 'image/png', '98765', '2018-03-14 14:45:46', '2018-03-14 14:45:46', '1');
INSERT INTO `files` VALUES ('259', '5aa8d536787de_1521014070_20180314.jpg', 'files/decision/2018/03/5aa8d536787de_1521014070_20180314.jpg', 'files/decision/2018/03/5aa8d536787de_1521014070_20180314.jpg', 'image/jpeg', '452536', '2018-03-14 14:54:30', '2018-03-14 14:54:30', '1');
INSERT INTO `files` VALUES ('260', '5aa8d61d0f5b1_1521014301_20180314.jpg', 'files/decision/2018/03/5aa8d61d0f5b1_1521014301_20180314.jpg', 'files/decision/2018/03/5aa8d61d0f5b1_1521014301_20180314.jpg', 'image/jpeg', '452536', '2018-03-14 14:58:21', '2018-03-14 14:58:21', '1');
INSERT INTO `files` VALUES ('261', '5aa8d89a75a77_1521014938_20180314.jpg', 'files/decision/2018/03/5aa8d89a75a77_1521014938_20180314.jpg', 'files/decision/2018/03/5aa8d89a75a77_1521014938_20180314.jpg', 'image/jpeg', '452536', '2018-03-14 15:05:45', '2018-03-14 15:08:58', '1');
INSERT INTO `files` VALUES ('262', '5aa8d9bc6a5cf_1521015228_20180314.png', 'files/decision/2018/03/5aa8d9bc6a5cf_1521015228_20180314.png', 'files/decision/2018/03/5aa8d9bc6a5cf_1521015228_20180314.png', 'image/png', '98765', '2018-03-14 15:13:48', '2018-03-14 15:13:48', '1');
INSERT INTO `files` VALUES ('263', 'APIN_Sonar.png', '/var/www/html/public/files/collection/2018/03/14/img_1521023353_5aa8f979cbdc89.65386207.png', 'collection/2018/03/14/img_1521023353_5aa8f979cbdc89.65386207.png', 'image/png', '98765', '2018-03-14 17:29:13', '2018-03-14 17:29:13', '1');
INSERT INTO `files` VALUES ('264', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521023364_5aa8f9846a29a2.21228407.png', 'collection/2018/03/14/img_1521023364_5aa8f9846a29a2.21228407.png', 'image/png', '362909', '2018-03-14 17:29:24', '2018-03-14 17:29:24', '1');
INSERT INTO `files` VALUES ('265', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/14/img_1521023457_5aa8f9e192b5a3.78287630.png', 'collection/2018/03/14/img_1521023457_5aa8f9e192b5a3.78287630.png', 'image/png', '362909', '2018-03-14 17:30:57', '2018-03-14 17:30:57', '1');
INSERT INTO `files` VALUES ('266', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521081015_5aa9dab7d6bed4.36771609.jpeg', 'collection/2018/03/15/img_1521081015_5aa9dab7d6bed4.36771609.jpeg', 'image/jpeg', '114501', '2018-03-15 09:30:15', '2018-03-15 09:30:15', '1');
INSERT INTO `files` VALUES ('267', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/15/img_1521085962_5aa9ee0ad475d4.46558244.png', 'collection/2018/03/15/img_1521085962_5aa9ee0ad475d4.46558244.png', 'image/png', '15927', '2018-03-15 10:52:42', '2018-03-15 10:52:42', '1');
INSERT INTO `files` VALUES ('268', '1.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521086361_5aa9ef99a931c1.10860793.jpeg', 'collection/2018/03/15/img_1521086361_5aa9ef99a931c1.10860793.jpeg', 'image/jpeg', '138409', '2018-03-15 10:59:21', '2018-03-15 10:59:21', '1');
INSERT INTO `files` VALUES ('269', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521087489_5aa9f401692d25.28679830.jpeg', 'collection/2018/03/15/img_1521087489_5aa9f401692d25.28679830.jpeg', 'image/jpeg', '1806735', '2018-03-15 11:18:09', '2018-03-15 11:18:09', '1');
INSERT INTO `files` VALUES ('270', '1.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521087979_5aa9f5eb5da5c8.99504258.jpeg', 'collection/2018/03/15/img_1521087979_5aa9f5eb5da5c8.99504258.jpeg', 'image/jpeg', '138409', '2018-03-15 11:26:19', '2018-03-15 11:26:19', '1');
INSERT INTO `files` VALUES ('271', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/15/img_1521088297_5aa9f7296fcaf7.17732179.png', 'collection/2018/03/15/img_1521088297_5aa9f7296fcaf7.17732179.png', 'image/png', '362909', '2018-03-15 11:31:37', '2018-03-15 11:31:37', '1');
INSERT INTO `files` VALUES ('272', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/15/img_1521088561_5aa9f8318dfa69.34264909.png', 'collection/2018/03/15/img_1521088561_5aa9f8318dfa69.34264909.png', 'image/png', '362909', '2018-03-15 11:36:01', '2018-03-15 11:36:01', '1');
INSERT INTO `files` VALUES ('273', 'Benh vien nhiet doi.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521088608_5aa9f860021707.21099739.jpeg', 'collection/2018/03/15/img_1521088608_5aa9f860021707.21099739.jpeg', 'image/jpeg', '2539128', '2018-03-15 11:36:48', '2018-03-15 11:36:48', '1');
INSERT INTO `files` VALUES ('274', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/15/img_1521088654_5aa9f88e426f73.60831979.png', 'collection/2018/03/15/img_1521088654_5aa9f88e426f73.60831979.png', 'image/png', '362909', '2018-03-15 11:37:34', '2018-03-15 11:37:34', '1');
INSERT INTO `files` VALUES ('275', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521089366_5aa9fb5672a100.58102938.jpeg', 'collection/2018/03/15/img_1521089366_5aa9fb5672a100.58102938.jpeg', 'image/jpeg', '1806735', '2018-03-15 11:49:26', '2018-03-15 11:49:26', '1');
INSERT INTO `files` VALUES ('276', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521089519_5aa9fbef34ee43.91112308.jpeg', 'collection/2018/03/15/img_1521089519_5aa9fbef34ee43.91112308.jpeg', 'image/jpeg', '1806735', '2018-03-15 11:51:59', '2018-03-15 11:51:59', '1');
INSERT INTO `files` VALUES ('277', 'image1.png', '/var/www/html/public/files/collection/2018/03/15/img_1521089533_5aa9fbfdab41b7.34213664.png', 'collection/2018/03/15/img_1521089533_5aa9fbfdab41b7.34213664.png', 'image/png', '77529', '2018-03-15 11:52:13', '2018-03-15 11:52:13', '1');
INSERT INTO `files` VALUES ('278', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521089540_5aa9fc04cd84a2.49142164.jpeg', 'collection/2018/03/15/img_1521089540_5aa9fc04cd84a2.49142164.jpeg', 'image/jpeg', '1806735', '2018-03-15 11:52:20', '2018-03-15 11:52:20', '1');
INSERT INTO `files` VALUES ('279', '15489771400_688bffd72f_o.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521095690_5aaa140a1fbde8.22984340.jpeg', 'collection/2018/03/15/img_1521095690_5aaa140a1fbde8.22984340.jpeg', 'image/jpeg', '168052', '2018-03-15 13:34:50', '2018-03-15 13:34:50', '1');
INSERT INTO `files` VALUES ('280', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521095722_5aaa142ac1a529.28993798.jpeg', 'collection/2018/03/15/img_1521095722_5aaa142ac1a529.28993798.jpeg', 'image/jpeg', '777835', '2018-03-15 13:35:22', '2018-03-15 13:35:22', '1');
INSERT INTO `files` VALUES ('281', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/15/img_1521096217_5aaa16197fe6a9.26303312.png', 'collection/2018/03/15/img_1521096217_5aaa16197fe6a9.26303312.png', 'image/png', '15927', '2018-03-15 13:43:37', '2018-03-15 13:43:37', '1');
INSERT INTO `files` VALUES ('282', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096303_5aaa166f5b89b7.06286982.jpeg', 'collection/2018/03/15/img_1521096303_5aaa166f5b89b7.06286982.jpeg', 'image/jpeg', '114501', '2018-03-15 13:45:03', '2018-03-15 13:45:03', '1');
INSERT INTO `files` VALUES ('283', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096533_5aaa1755d502f9.89203985.jpeg', 'collection/2018/03/15/img_1521096533_5aaa1755d502f9.89203985.jpeg', 'image/jpeg', '114501', '2018-03-15 13:48:53', '2018-03-15 13:48:53', '1');
INSERT INTO `files` VALUES ('284', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096555_5aaa176b28b3c5.99191496.jpeg', 'collection/2018/03/15/img_1521096555_5aaa176b28b3c5.99191496.jpeg', 'image/jpeg', '114501', '2018-03-15 13:49:15', '2018-03-15 13:49:15', '1');
INSERT INTO `files` VALUES ('285', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096669_5aaa17ddb87994.10141649.jpeg', 'collection/2018/03/15/img_1521096669_5aaa17ddb87994.10141649.jpeg', 'image/jpeg', '114501', '2018-03-15 13:51:09', '2018-03-15 13:51:09', '1');
INSERT INTO `files` VALUES ('286', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521096785_5aaa18516c5c95.82943798.jpeg', 'collection/2018/03/15/img_1521096785_5aaa18516c5c95.82943798.jpeg', 'image/jpeg', '114501', '2018-03-15 13:53:05', '2018-03-15 13:53:05', '1');
INSERT INTO `files` VALUES ('287', 'Dau hieu virus 1 contact.png', '/var/www/html/public/files/collection/2018/03/15/img_1521097127_5aaa19a7a8be14.49737443.png', 'collection/2018/03/15/img_1521097127_5aaa19a7a8be14.49737443.png', 'image/png', '63048', '2018-03-15 13:58:47', '2018-03-15 13:58:47', '1');
INSERT INTO `files` VALUES ('288', '2.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521098965_5aaa20d5570f70.46692602.jpeg', 'collection/2018/03/15/img_1521098965_5aaa20d5570f70.46692602.jpeg', 'image/jpeg', '37400', '2018-03-15 14:29:25', '2018-03-15 14:29:25', '1');
INSERT INTO `files` VALUES ('289', '2.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521098991_5aaa20efe7a8d8.49010293.jpeg', 'collection/2018/03/15/img_1521098991_5aaa20efe7a8d8.49010293.jpeg', 'image/jpeg', '37400', '2018-03-15 14:29:51', '2018-03-15 14:29:51', '1');
INSERT INTO `files` VALUES ('290', '2.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521099017_5aaa2109e09aa4.26538832.jpeg', 'collection/2018/03/15/img_1521099017_5aaa2109e09aa4.26538832.jpeg', 'image/jpeg', '37400', '2018-03-15 14:30:17', '2018-03-15 14:30:17', '1');
INSERT INTO `files` VALUES ('291', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/15/img_1521099182_5aaa21ae76da72.44122017.png', 'collection/2018/03/15/img_1521099182_5aaa21ae76da72.44122017.png', 'image/png', '15927', '2018-03-15 14:33:02', '2018-03-15 14:33:02', '1');
INSERT INTO `files` VALUES ('292', 'Logo VTGA.png', '/var/www/html/public/files/collection/2018/03/15/img_1521099437_5aaa22ade093d0.43878124.png', 'collection/2018/03/15/img_1521099437_5aaa22ade093d0.43878124.png', 'image/png', '15927', '2018-03-15 14:37:17', '2018-03-15 14:37:17', '1');
INSERT INTO `files` VALUES ('293', 'new-field-group.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521101693_5aaa2b7d29b8f8.00685716.jpeg', 'collection/2018/03/15/img_1521101693_5aaa2b7d29b8f8.00685716.jpeg', 'image/jpeg', '53097', '2018-03-15 15:14:53', '2018-03-15 15:14:53', '1');
INSERT INTO `files` VALUES ('294', '5aaa38c7b46a2_1521105095_20180315.pdf', 'files/decision/2018/03/5aaa38c7b46a2_1521105095_20180315.pdf', 'files/decision/2018/03/5aaa38c7b46a2_1521105095_20180315.pdf', 'application/pdf', '7945', '2018-03-15 16:11:35', '2018-03-15 16:11:35', '1');
INSERT INTO `files` VALUES ('295', '5aaa390fac4dd_1521105167_20180315.pdf', 'files/decision/2018/03/5aaa390fac4dd_1521105167_20180315.pdf', 'files/decision/2018/03/5aaa390fac4dd_1521105167_20180315.pdf', 'application/pdf', '7945', '2018-03-15 16:12:47', '2018-03-15 16:12:47', '1');
INSERT INTO `files` VALUES ('296', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521105233_5aaa3951ead131.12556780.jpeg', 'collection/2018/03/15/img_1521105233_5aaa3951ead131.12556780.jpeg', 'image/jpeg', '114501', '2018-03-15 16:13:53', '2018-03-15 16:13:53', '1');
INSERT INTO `files` VALUES ('297', '5aaa39529f8d2_1521105234_20180315.png', 'files/decision/2018/03/5aaa39529f8d2_1521105234_20180315.png', 'files/decision/2018/03/5aaa39529f8d2_1521105234_20180315.png', 'image/png', '362909', '2018-03-15 16:13:54', '2018-03-15 16:13:54', '1');
INSERT INTO `files` VALUES ('298', '5aaa397c33757_1521105276_20180315.png', 'files/decision/2018/03/5aaa397c33757_1521105276_20180315.png', 'files/decision/2018/03/5aaa397c33757_1521105276_20180315.png', 'image/png', '362909', '2018-03-15 16:14:36', '2018-03-15 16:14:36', '1');
INSERT INTO `files` VALUES ('299', '5aaa398f42a0a_1521105295_20180315.pdf', 'files/decision/2018/03/5aaa398f42a0a_1521105295_20180315.pdf', 'files/decision/2018/03/5aaa398f42a0a_1521105295_20180315.pdf', 'application/pdf', '7945', '2018-03-15 16:14:55', '2018-03-15 16:14:55', '1');
INSERT INTO `files` VALUES ('300', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521105634_5aaa3ae29770e2.95580527.jpeg', 'collection/2018/03/15/img_1521105634_5aaa3ae29770e2.95580527.jpeg', 'image/jpeg', '114501', '2018-03-15 16:20:34', '2018-03-15 16:20:34', '1');
INSERT INTO `files` VALUES ('301', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521105921_5aaa3c0160c8f4.25210043.jpeg', 'collection/2018/03/15/img_1521105921_5aaa3c0160c8f4.25210043.jpeg', 'image/jpeg', '114501', '2018-03-15 16:25:21', '2018-03-15 16:25:21', '1');
INSERT INTO `files` VALUES ('302', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/15/img_1521106057_5aaa3c89715ce3.22080173.png', 'collection/2018/03/15/img_1521106057_5aaa3c89715ce3.22080173.png', 'image/png', '362909', '2018-03-15 16:27:37', '2018-03-15 16:27:37', '1');
INSERT INTO `files` VALUES ('303', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521106656_5aaa3ee07148a0.82554474.jpeg', 'collection/2018/03/15/img_1521106656_5aaa3ee07148a0.82554474.jpeg', 'image/jpeg', '777835', '2018-03-15 16:37:36', '2018-03-15 16:37:36', '1');
INSERT INTO `files` VALUES ('304', '5aaa411992fa6_1521107225_20180315.png', 'files/decision/2018/03/5aaa411992fa6_1521107225_20180315.png', 'files/decision/2018/03/5aaa411992fa6_1521107225_20180315.png', 'image/png', '362909', '2018-03-15 16:47:05', '2018-03-15 16:47:05', '1');
INSERT INTO `files` VALUES ('305', '5aaa466472076_1521108580_20180315.pdf', 'files/decision/2018/03/5aaa466472076_1521108580_20180315.pdf', 'files/decision/2018/03/5aaa466472076_1521108580_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:09:40', '2018-03-15 17:09:40', '1');
INSERT INTO `files` VALUES ('306', '5aaa46cf88c11_1521108687_20180315.pdf', 'files/decision/2018/03/5aaa46cf88c11_1521108687_20180315.pdf', 'files/decision/2018/03/5aaa46cf88c11_1521108687_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:11:27', '2018-03-15 17:11:27', '1');
INSERT INTO `files` VALUES ('307', '5aaa46db5a225_1521108699_20180315.pdf', 'files/decision/2018/03/5aaa46db5a225_1521108699_20180315.pdf', 'files/decision/2018/03/5aaa46db5a225_1521108699_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:11:39', '2018-03-15 17:11:39', '1');
INSERT INTO `files` VALUES ('308', '5aaa46e7f38c4_1521108711_20180315.pdf', 'files/decision/2018/03/5aaa46e7f38c4_1521108711_20180315.pdf', 'files/decision/2018/03/5aaa46e7f38c4_1521108711_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:11:52', '2018-03-15 17:11:52', '1');
INSERT INTO `files` VALUES ('309', '5aaa470216e96_1521108738_20180315.pdf', 'files/decision/2018/03/5aaa470216e96_1521108738_20180315.pdf', 'files/decision/2018/03/5aaa470216e96_1521108738_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:12:18', '2018-03-15 17:12:18', '1');
INSERT INTO `files` VALUES ('310', '5aaa470d344ff_1521108749_20180315.pdf', 'files/decision/2018/03/5aaa470d344ff_1521108749_20180315.pdf', 'files/decision/2018/03/5aaa470d344ff_1521108749_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:12:29', '2018-03-15 17:12:29', '1');
INSERT INTO `files` VALUES ('311', '5aaa4718ca765_1521108760_20180315.pdf', 'files/decision/2018/03/5aaa4718ca765_1521108760_20180315.pdf', 'files/decision/2018/03/5aaa4718ca765_1521108760_20180315.pdf', 'application/pdf', '6510534', '2018-03-15 17:12:40', '2018-03-15 17:12:40', '1');
INSERT INTO `files` VALUES ('312', 'new-field-group.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521111158_5aaa50762de761.55702363.jpeg', 'collection/2018/03/15/img_1521111158_5aaa50762de761.55702363.jpeg', 'image/jpeg', '53097', '2018-03-15 17:52:38', '2018-03-15 17:52:38', '1');
INSERT INTO `files` VALUES ('313', 'new-field-group.jpg', '/var/www/html/public/files/collection/2018/03/15/img_1521111199_5aaa509f2df997.43021921.jpeg', 'collection/2018/03/15/img_1521111199_5aaa509f2df997.43021921.jpeg', 'image/jpeg', '53097', '2018-03-15 17:53:19', '2018-03-15 17:53:19', '1');
INSERT INTO `files` VALUES ('314', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521167144_5aab2b28cfc735.46161204.jpeg', 'collection/2018/03/16/img_1521167144_5aab2b28cfc735.46161204.jpeg', 'image/jpeg', '114501', '2018-03-16 09:25:44', '2018-03-16 09:25:44', '1');
INSERT INTO `files` VALUES ('315', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521167738_5aab2d7a1fd1e4.21099877.png', 'collection/2018/03/16/img_1521167738_5aab2d7a1fd1e4.21099877.png', 'image/png', '362909', '2018-03-16 09:35:38', '2018-03-16 09:35:38', '1');
INSERT INTO `files` VALUES ('316', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521167765_5aab2d955deb76.02266190.png', 'collection/2018/03/16/img_1521167765_5aab2d955deb76.02266190.png', 'image/png', '362909', '2018-03-16 09:36:05', '2018-03-16 09:36:05', '1');
INSERT INTO `files` VALUES ('317', '5aab3200c5e35_1521168896_20180316.jpg', 'files/decision/2018/03/5aab3200c5e35_1521168896_20180316.jpg', 'files/decision/2018/03/5aab3200c5e35_1521168896_20180316.jpg', 'image/jpeg', '138409', '2018-03-16 09:54:56', '2018-03-16 09:54:56', '1');
INSERT INTO `files` VALUES ('318', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521172808_5aab4148712340.65793374.jpeg', 'collection/2018/03/16/img_1521172808_5aab4148712340.65793374.jpeg', 'image/jpeg', '114501', '2018-03-16 11:00:08', '2018-03-16 11:00:08', '1');
INSERT INTO `files` VALUES ('319', 'Capture1.PNG', '/var/www/html/public/files/collection/2018/03/16/img_1521173341_5aab435db17c56.13080195.png', 'collection/2018/03/16/img_1521173341_5aab435db17c56.13080195.png', 'image/png', '59903', '2018-03-16 11:09:01', '2018-03-16 11:09:01', '1');
INSERT INTO `files` VALUES ('320', 'Chrysanthemum@#$%^.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174390_5aab47766ec0a0.61279155.jpeg', 'collection/2018/03/16/img_1521174390_5aab47766ec0a0.61279155.jpeg', 'image/jpeg', '879394', '2018-03-16 11:26:30', '2018-03-16 11:26:30', '1');
INSERT INTO `files` VALUES ('321', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174396_5aab477c1b7df5.24878867.jpeg', 'collection/2018/03/16/img_1521174396_5aab477c1b7df5.24878867.jpeg', 'image/jpeg', '780831', '2018-03-16 11:26:36', '2018-03-16 11:26:36', '1');
INSERT INTO `files` VALUES ('322', 'Picture66.png', '/var/www/html/public/files/collection/2018/03/16/img_1521174682_5aab489aa02626.78994025.png', 'collection/2018/03/16/img_1521174682_5aab489aa02626.78994025.png', 'image/png', '286371', '2018-03-16 11:31:22', '2018-03-16 11:31:22', '1');
INSERT INTO `files` VALUES ('323', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174784_5aab49008a4d23.33491515.jpeg', 'collection/2018/03/16/img_1521174784_5aab49008a4d23.33491515.jpeg', 'image/jpeg', '561276', '2018-03-16 11:33:04', '2018-03-16 11:33:04', '1');
INSERT INTO `files` VALUES ('324', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521174851_5aab4943854cd0.01036815.png', 'collection/2018/03/16/img_1521174851_5aab4943854cd0.01036815.png', 'image/png', '185324', '2018-03-16 11:34:11', '2018-03-16 11:34:11', '1');
INSERT INTO `files` VALUES ('325', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174851_5aab4943cc0cf8.42700808.jpeg', 'collection/2018/03/16/img_1521174851_5aab4943cc0cf8.42700808.jpeg', 'image/jpeg', '114501', '2018-03-16 11:34:11', '2018-03-16 11:34:11', '1');
INSERT INTO `files` VALUES ('326', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174852_5aab494418e6e8.66516951.jpeg', 'collection/2018/03/16/img_1521174852_5aab494418e6e8.66516951.jpeg', 'image/jpeg', '777835', '2018-03-16 11:34:12', '2018-03-16 11:34:12', '1');
INSERT INTO `files` VALUES ('327', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521174950_5aab49a6934246.21878407.png', 'collection/2018/03/16/img_1521174950_5aab49a6934246.21878407.png', 'image/png', '185324', '2018-03-16 11:35:50', '2018-03-16 11:35:50', '1');
INSERT INTO `files` VALUES ('328', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521174993_5aab49d14104c4.34316348.jpeg', 'collection/2018/03/16/img_1521174993_5aab49d14104c4.34316348.jpeg', 'image/jpeg', '620888', '2018-03-16 11:36:33', '2018-03-16 11:36:33', '1');
INSERT INTO `files` VALUES ('329', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521175470_5aab4bae6fbc81.99147213.jpeg', 'collection/2018/03/16/img_1521175470_5aab4bae6fbc81.99147213.jpeg', 'image/jpeg', '114501', '2018-03-16 11:44:30', '2018-03-16 11:44:30', '1');
INSERT INTO `files` VALUES ('330', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521175627_5aab4c4ba94ac0.32452118.jpeg', 'collection/2018/03/16/img_1521175627_5aab4c4ba94ac0.32452118.jpeg', 'image/jpeg', '114501', '2018-03-16 11:47:07', '2018-03-16 11:47:07', '1');
INSERT INTO `files` VALUES ('331', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521181719_5aab64179c2b26.25411238.jpeg', 'collection/2018/03/16/img_1521181719_5aab64179c2b26.25411238.jpeg', 'image/jpeg', '114501', '2018-03-16 13:28:39', '2018-03-16 13:28:39', '1');
INSERT INTO `files` VALUES ('332', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', 'collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', 'image/jpeg', '114501', '2018-03-16 13:32:39', '2018-03-16 13:32:39', '1');
INSERT INTO `files` VALUES ('333', 'Capture.PNG', '/var/www/html/public/files/collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', 'collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', 'image/png', '25040', '2018-03-16 13:35:06', '2018-03-16 13:35:06', '1');
INSERT INTO `files` VALUES ('334', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521182366_5aab669ec16966.41366917.png', 'collection/2018/03/16/img_1521182366_5aab669ec16966.41366917.png', 'image/png', '362909', '2018-03-16 13:39:26', '2018-03-16 13:39:26', '1');
INSERT INTO `files` VALUES ('335', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521182376_5aab66a82d3aa8.89669388.png', 'collection/2018/03/16/img_1521182376_5aab66a82d3aa8.89669388.png', 'image/png', '185324', '2018-03-16 13:39:36', '2018-03-16 13:39:36', '1');
INSERT INTO `files` VALUES ('336', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521182473_5aab67090e2454.09445286.jpeg', 'collection/2018/03/16/img_1521182473_5aab67090e2454.09445286.jpeg', 'image/jpeg', '777835', '2018-03-16 13:41:13', '2018-03-16 13:41:13', '1');
INSERT INTO `files` VALUES ('337', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521183696_5aab6bd0e2ed68.88136904.jpeg', 'collection/2018/03/16/img_1521183696_5aab6bd0e2ed68.88136904.jpeg', 'image/jpeg', '561276', '2018-03-16 14:01:36', '2018-03-16 14:01:36', '1');
INSERT INTO `files` VALUES ('338', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521183896_5aab6c980f06c1.70832777.jpeg', 'collection/2018/03/16/img_1521183896_5aab6c980f06c1.70832777.jpeg', 'image/jpeg', '777835', '2018-03-16 14:04:56', '2018-03-16 14:04:56', '1');
INSERT INTO `files` VALUES ('339', '20161208_152101.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521184448_5aab6ec0661652.77785329.jpeg', 'collection/2018/03/16/img_1521184448_5aab6ec0661652.77785329.jpeg', 'image/jpeg', '1806735', '2018-03-16 14:14:08', '2018-03-16 14:14:08', '1');
INSERT INTO `files` VALUES ('340', 'Chrysanthemum@#$%^.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521184477_5aab6edd38c1d1.61064554.jpeg', 'collection/2018/03/16/img_1521184477_5aab6edd38c1d1.61064554.jpeg', 'image/jpeg', '879394', '2018-03-16 14:14:37', '2018-03-16 14:14:37', '1');
INSERT INTO `files` VALUES ('341', 'nhietke.png', '/var/www/html/public/files/collection/2018/03/16/img_1521185072_5aab7130b485d3.12720080.png', 'collection/2018/03/16/img_1521185072_5aab7130b485d3.12720080.png', 'image/png', '112872', '2018-03-16 14:24:32', '2018-03-16 14:24:32', '1');
INSERT INTO `files` VALUES ('342', 'FullSizeRender.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521185137_5aab7171bfdb67.94907905.jpeg', 'collection/2018/03/16/img_1521185137_5aab7171bfdb67.94907905.jpeg', 'image/jpeg', '1097860', '2018-03-16 14:25:37', '2018-03-16 14:25:37', '1');
INSERT INTO `files` VALUES ('343', '5aab77d787b57_1521186775_20180316.jpg', 'files/decision/2018/03/5aab77d787b57_1521186775_20180316.jpg', 'files/decision/2018/03/5aab77d787b57_1521186775_20180316.jpg', 'video/mp4', '1385735', '2018-03-16 14:52:55', '2018-03-16 14:52:55', '1');
INSERT INTO `files` VALUES ('344', '5aab77f9301fd_1521186809_20180316.jpg', 'files/decision/2018/03/5aab77f9301fd_1521186809_20180316.jpg', 'files/decision/2018/03/5aab77f9301fd_1521186809_20180316.jpg', 'video/mp4', '1246482', '2018-03-16 14:53:29', '2018-03-16 14:53:29', '1');
INSERT INTO `files` VALUES ('345', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521186947_5aab7883575480.02318570.jpeg', 'collection/2018/03/16/img_1521186947_5aab7883575480.02318570.jpeg', 'image/jpeg', '114501', '2018-03-16 14:55:47', '2018-03-16 14:55:47', '1');
INSERT INTO `files` VALUES ('346', '5aab7885e8ebc_1521186949_20180316.jpg', 'files/decision/2018/03/5aab7885e8ebc_1521186949_20180316.jpg', 'files/decision/2018/03/5aab7885e8ebc_1521186949_20180316.jpg', 'video/mp4', '1246482', '2018-03-16 14:55:49', '2018-03-16 14:55:49', '1');
INSERT INTO `files` VALUES ('347', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521186969_5aab789974cb56.82377981.jpeg', 'collection/2018/03/16/img_1521186969_5aab789974cb56.82377981.jpeg', 'image/jpeg', '114501', '2018-03-16 14:56:09', '2018-03-16 14:56:09', '1');
INSERT INTO `files` VALUES ('348', '2.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', 'collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', 'image/jpeg', '37400', '2018-03-16 15:01:16', '2018-03-16 15:01:16', '1');
INSERT INTO `files` VALUES ('349', '5aab7a8e25366_1521187470_20180316.pdf', 'files/decision/2018/03/5aab7a8e25366_1521187470_20180316.pdf', 'files/decision/2018/03/5aab7a8e25366_1521187470_20180316.pdf', 'application/pdf', '6510534', '2018-03-16 15:04:30', '2018-03-16 15:04:30', '1');
INSERT INTO `files` VALUES ('350', '14628088677_d3f806abe4_c.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521188636_5aab7f1cb429a3.47387654.jpeg', 'collection/2018/03/16/img_1521188636_5aab7f1cb429a3.47387654.jpeg', 'image/jpeg', '114501', '2018-03-16 15:23:56', '2018-03-16 15:23:56', '1');
INSERT INTO `files` VALUES ('351', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521188702_5aab7f5ee36f92.84609281.jpeg', 'collection/2018/03/16/img_1521188702_5aab7f5ee36f92.84609281.jpeg', 'image/jpeg', '777835', '2018-03-16 15:25:02', '2018-03-16 15:25:02', '1');
INSERT INTO `files` VALUES ('352', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521191163_5aab88fbf27890.38064920.png', 'collection/2018/03/16/img_1521191163_5aab88fbf27890.38064920.png', 'image/png', '362909', '2018-03-16 16:06:04', '2018-03-16 16:06:04', '1');
INSERT INTO `files` VALUES ('353', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521191335_5aab89a77e0e21.51858982.png', 'collection/2018/03/16/img_1521191335_5aab89a77e0e21.51858982.png', 'image/png', '362909', '2018-03-16 16:08:55', '2018-03-16 16:08:55', '1');
INSERT INTO `files` VALUES ('354', 'images.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521191810_5aab8b82842903.69869790.jpeg', 'collection/2018/03/16/img_1521191810_5aab8b82842903.69869790.jpeg', 'image/jpeg', '6228', '2018-03-16 16:16:50', '2018-03-16 16:16:50', '1');
INSERT INTO `files` VALUES ('355', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521191874_5aab8bc2e75776.81906463.jpeg', 'collection/2018/03/16/img_1521191874_5aab8bc2e75776.81906463.jpeg', 'image/jpeg', '845941', '2018-03-16 16:17:54', '2018-03-16 16:17:54', '1');
INSERT INTO `files` VALUES ('356', 'images (1).jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521191949_5aab8c0d745545.49222775.jpeg', 'collection/2018/03/16/img_1521191949_5aab8c0d745545.49222775.jpeg', 'image/jpeg', '7035', '2018-03-16 16:19:09', '2018-03-16 16:19:09', '1');
INSERT INTO `files` VALUES ('357', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521192218_5aab8d1a872120.82776724.png', 'collection/2018/03/16/img_1521192218_5aab8d1a872120.82776724.png', 'image/png', '185324', '2018-03-16 16:23:38', '2018-03-16 16:23:38', '1');
INSERT INTO `files` VALUES ('358', 'bo-suu-tap-nhung-hinh-anh-hot-girl-mac-bikini-goi-cam-nhat-6.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521192314_5aab8d7aee0d45.45701283.jpeg', 'collection/2018/03/16/img_1521192314_5aab8d7aee0d45.45701283.jpeg', 'image/jpeg', '304531', '2018-03-16 16:25:15', '2018-03-16 16:25:15', '1');
INSERT INTO `files` VALUES ('359', 'goki-logomaker.png', '/var/www/html/public/files/collection/2018/03/16/img_1521192563_5aab8e734bf781.08818864.png', 'collection/2018/03/16/img_1521192563_5aab8e734bf781.08818864.png', 'image/png', '185324', '2018-03-16 16:29:23', '2018-03-16 16:29:23', '1');
INSERT INTO `files` VALUES ('360', 'images.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521192672_5aab8ee0bf9419.04846807.jpeg', 'collection/2018/03/16/img_1521192672_5aab8ee0bf9419.04846807.jpeg', 'image/jpeg', '6228', '2018-03-16 16:31:12', '2018-03-16 16:31:12', '1');
INSERT INTO `files` VALUES ('361', 'bo-suu-tap-nhung-hinh-anh-hot-girl-mac-bikini-goi-cam-nhat-6.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521194464_5aab95e0737ad9.13724145.jpeg', 'collection/2018/03/16/img_1521194464_5aab95e0737ad9.13724145.jpeg', 'image/jpeg', '304531', '2018-03-16 17:01:04', '2018-03-16 17:01:04', '1');
INSERT INTO `files` VALUES ('362', 'images (1).jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521194697_5aab96c9b86990.58245542.jpeg', 'collection/2018/03/16/img_1521194697_5aab96c9b86990.58245542.jpeg', 'image/jpeg', '7035', '2018-03-16 17:04:57', '2018-03-16 17:04:57', '1');
INSERT INTO `files` VALUES ('363', 'bo-suu-tap-nhung-hinh-anh-hot-girl-mac-bikini-goi-cam-nhat-6.jpg', '/var/www/html/public/files/collection/2018/03/16/img_1521194978_5aab97e2a61580.66076550.jpeg', 'collection/2018/03/16/img_1521194978_5aab97e2a61580.66076550.jpeg', 'image/jpeg', '304531', '2018-03-16 17:09:38', '2018-03-16 17:09:38', '1');
INSERT INTO `files` VALUES ('364', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521196544_5aab9e007646b7.97909790.png', 'collection/2018/03/16/img_1521196544_5aab9e007646b7.97909790.png', 'image/png', '362909', '2018-03-16 17:35:44', '2018-03-16 17:35:44', '1');
INSERT INTO `files` VALUES ('365', 'nap truc tiep.png', '/var/www/html/public/files/collection/2018/03/16/img_1521196610_5aab9e42252ea7.13632401.png', 'collection/2018/03/16/img_1521196610_5aab9e42252ea7.13632401.png', 'image/png', '362909', '2018-03-16 17:36:50', '2018-03-16 17:36:50', '1');
INSERT INTO `files` VALUES ('366', '2018-03-18_19-08-54.png', '/var/www/html/public/files/collection/2018/03/19/img_1521423836_5aaf15dc46dd99.83793664.png', 'collection/2018/03/19/img_1521423836_5aaf15dc46dd99.83793664.png', 'image/png', '39416', '2018-03-19 08:43:56', '2018-03-19 08:43:56', '1');
INSERT INTO `files` VALUES ('367', '2018-03-18_9-23-40.png', '/var/www/html/public/files/collection/2018/03/19/img_1521423977_5aaf166926f483.56938990.png', 'collection/2018/03/19/img_1521423977_5aaf166926f483.56938990.png', 'image/png', '7793', '2018-03-19 08:46:17', '2018-03-19 08:46:17', '1');
INSERT INTO `files` VALUES ('368', '2018-03-18_9-36-53.png', '/var/www/html/public/files/collection/2018/03/19/img_1521424051_5aaf16b3407510.12927624.png', 'collection/2018/03/19/img_1521424051_5aaf16b3407510.12927624.png', 'image/png', '16137', '2018-03-19 08:47:31', '2018-03-19 08:47:31', '1');
INSERT INTO `files` VALUES ('369', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521433815_5aaf3cd707fb17.31644168.jpeg', 'collection/2018/03/19/img_1521433815_5aaf3cd707fb17.31644168.jpeg', 'image/jpeg', '879394', '2018-03-19 11:30:15', '2018-03-19 11:30:15', '1');
INSERT INTO `files` VALUES ('370', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521434269_5aaf3e9dc877f9.89915900.jpeg', 'collection/2018/03/19/img_1521434269_5aaf3e9dc877f9.89915900.jpeg', 'image/jpeg', '879394', '2018-03-19 11:37:49', '2018-03-19 11:37:49', '1');
INSERT INTO `files` VALUES ('371', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521434638_5aaf400e6357f5.14271148.jpeg', 'collection/2018/03/19/img_1521434638_5aaf400e6357f5.14271148.jpeg', 'image/jpeg', '845941', '2018-03-19 11:43:58', '2018-03-19 11:43:58', '1');
INSERT INTO `files` VALUES ('372', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521434660_5aaf402439ffb1.99764789.jpeg', 'collection/2018/03/19/img_1521434660_5aaf402439ffb1.99764789.jpeg', 'image/jpeg', '595284', '2018-03-19 11:44:20', '2018-03-19 11:44:20', '1');
INSERT INTO `files` VALUES ('373', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521434670_5aaf402e31be81.55316577.jpeg', 'collection/2018/03/19/img_1521434670_5aaf402e31be81.55316577.jpeg', 'image/jpeg', '845941', '2018-03-19 11:44:30', '2018-03-19 11:44:30', '1');
INSERT INTO `files` VALUES ('374', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521448224_5aaf75206309e9.65263255.jpeg', 'collection/2018/03/19/img_1521448224_5aaf75206309e9.65263255.jpeg', 'image/jpeg', '595284', '2018-03-19 15:30:24', '2018-03-19 15:30:24', '1');
INSERT INTO `files` VALUES ('375', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/19/img_1521448853_5aaf77959221c5.35910421.jpeg', 'collection/2018/03/19/img_1521448853_5aaf77959221c5.35910421.jpeg', 'image/jpeg', '780831', '2018-03-19 15:40:53', '2018-03-19 15:40:53', '1');
INSERT INTO `files` VALUES ('376', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521510673_5ab06911f1d522.16753546.jpeg', 'collection/2018/03/20/img_1521510673_5ab06911f1d522.16753546.jpeg', 'image/jpeg', '879394', '2018-03-20 08:51:14', '2018-03-20 08:51:14', '1');
INSERT INTO `files` VALUES ('377', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521510738_5ab06952f2c9a8.86342948.jpeg', 'collection/2018/03/20/img_1521510738_5ab06952f2c9a8.86342948.jpeg', 'image/jpeg', '775702', '2018-03-20 08:52:19', '2018-03-20 08:52:19', '1');
INSERT INTO `files` VALUES ('378', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521510748_5ab0695cef8263.73301394.jpeg', 'collection/2018/03/20/img_1521510748_5ab0695cef8263.73301394.jpeg', 'image/jpeg', '879394', '2018-03-20 08:52:29', '2018-03-20 08:52:29', '1');
INSERT INTO `files` VALUES ('379', '1.PNG', '/var/www/html/public/files/collection/2018/03/20/img_1521510934_5ab06a16ac0551.58971158.png', 'collection/2018/03/20/img_1521510934_5ab06a16ac0551.58971158.png', 'image/png', '1073495', '2018-03-20 08:55:34', '2018-03-20 08:55:34', '1');
INSERT INTO `files` VALUES ('380', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521511055_5ab06a8fd1a1d4.79809179.jpeg', 'collection/2018/03/20/img_1521511055_5ab06a8fd1a1d4.79809179.jpeg', 'image/jpeg', '845941', '2018-03-20 08:57:35', '2018-03-20 08:57:35', '1');
INSERT INTO `files` VALUES ('381', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521511821_5ab06d8db544c4.44298844.jpeg', 'collection/2018/03/20/img_1521511821_5ab06d8db544c4.44298844.jpeg', 'image/jpeg', '595284', '2018-03-20 09:10:21', '2018-03-20 09:10:21', '1');
INSERT INTO `files` VALUES ('382', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521516106_5ab07e4a49efc9.94556175.jpeg', 'collection/2018/03/20/img_1521516106_5ab07e4a49efc9.94556175.jpeg', 'image/jpeg', '777835', '2018-03-20 10:21:46', '2018-03-20 10:21:46', '1');
INSERT INTO `files` VALUES ('383', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521518838_5ab088f605faf8.42492612.jpeg', 'collection/2018/03/20/img_1521518838_5ab088f605faf8.42492612.jpeg', 'image/jpeg', '620888', '2018-03-20 11:07:18', '2018-03-20 11:07:18', '1');
INSERT INTO `files` VALUES ('384', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521520750_5ab0906ed01de8.62584430.jpeg', 'collection/2018/03/20/img_1521520750_5ab0906ed01de8.62584430.jpeg', 'image/jpeg', '620888', '2018-03-20 11:39:10', '2018-03-20 11:39:10', '1');
INSERT INTO `files` VALUES ('385', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/20/img_1521535141_5ab0c8a510f8e9.03345318.jpeg', 'collection/2018/03/20/img_1521535141_5ab0c8a510f8e9.03345318.jpeg', 'image/jpeg', '595284', '2018-03-20 15:39:01', '2018-03-20 15:39:01', '1');
INSERT INTO `files` VALUES ('386', '5ab0ce3c9f745_1521536572_20180320.jpg', 'files/decision/2018/03/5ab0ce3c9f745_1521536572_20180320.jpg', 'files/decision/2018/03/5ab0ce3c9f745_1521536572_20180320.jpg', 'image/jpeg', '780831', '2018-03-20 16:02:52', '2018-03-20 16:02:52', '1');
INSERT INTO `files` VALUES ('387', '5ab1bcd8c2aeb_1521597656_20180321.jpg', 'files/decision/2018/03/5ab1bcd8c2aeb_1521597656_20180321.jpg', 'files/decision/2018/03/5ab1bcd8c2aeb_1521597656_20180321.jpg', 'image/jpeg', '879394', '2018-03-21 09:00:56', '2018-03-21 09:00:56', '1');
INSERT INTO `files` VALUES ('388', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/21/img_1521601847_5ab1cd373fbcc6.62105185.jpeg', 'collection/2018/03/21/img_1521601847_5ab1cd373fbcc6.62105185.jpeg', 'image/jpeg', '595284', '2018-03-21 10:10:47', '2018-03-21 10:10:47', '1');
INSERT INTO `files` VALUES ('389', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/21/img_1521601897_5ab1cd696f0992.28061792.jpeg', 'collection/2018/03/21/img_1521601897_5ab1cd696f0992.28061792.jpeg', 'image/jpeg', '595284', '2018-03-21 10:11:37', '2018-03-21 10:11:37', '1');
INSERT INTO `files` VALUES ('390', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/21/img_1521601920_5ab1cd8045c9a7.87272148.jpeg', 'collection/2018/03/21/img_1521601920_5ab1cd8045c9a7.87272148.jpeg', 'image/jpeg', '595284', '2018-03-21 10:12:00', '2018-03-21 10:12:00', '1');
INSERT INTO `files` VALUES ('391', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/21/img_1521601923_5ab1cd8344c537.77921336.jpeg', 'collection/2018/03/21/img_1521601923_5ab1cd8344c537.77921336.jpeg', 'image/jpeg', '775702', '2018-03-21 10:12:03', '2018-03-21 10:12:03', '1');
INSERT INTO `files` VALUES ('392', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/21/img_1521601970_5ab1cdb218cfb7.63607810.jpeg', 'collection/2018/03/21/img_1521601970_5ab1cdb218cfb7.63607810.jpeg', 'image/jpeg', '620888', '2018-03-21 10:12:50', '2018-03-21 10:12:50', '1');
INSERT INTO `files` VALUES ('393', '5ab1d67b42049_1521604219_20180321.jpg', 'files/decision/2018/03/5ab1d67b42049_1521604219_20180321.jpg', 'files/decision/2018/03/5ab1d67b42049_1521604219_20180321.jpg', 'image/jpeg', '780831', '2018-03-21 10:50:19', '2018-03-21 10:50:19', '1');
INSERT INTO `files` VALUES ('394', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/21/img_1521615051_5ab200cb366139.91016331.jpeg', 'collection/2018/03/21/img_1521615051_5ab200cb366139.91016331.jpeg', 'image/jpeg', '879394', '2018-03-21 13:50:51', '2018-03-21 13:50:51', '1');
INSERT INTO `files` VALUES ('395', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/21/img_1521615469_5ab2026d6f8d30.66073028.jpeg', 'collection/2018/03/21/img_1521615469_5ab2026d6f8d30.66073028.jpeg', 'image/jpeg', '879394', '2018-03-21 13:57:49', '2018-03-21 13:57:49', '1');
INSERT INTO `files` VALUES ('396', '5ab206524e3bb_1521616466_20180321.jpg', 'files/decision/2018/03/5ab206524e3bb_1521616466_20180321.jpg', 'files/decision/2018/03/5ab206524e3bb_1521616466_20180321.jpg', 'image/jpeg', '780831', '2018-03-21 14:14:26', '2018-03-21 14:14:26', '1');
INSERT INTO `files` VALUES ('397', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/22/img_1521702633_5ab356e92f96c0.77369710.jpeg', 'collection/2018/03/22/img_1521702633_5ab356e92f96c0.77369710.jpeg', 'image/jpeg', '595284', '2018-03-22 14:10:33', '2018-03-22 14:10:33', '1');
INSERT INTO `files` VALUES ('398', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/22/img_1521713489_5ab381518cdb84.53668732.jpeg', 'collection/2018/03/22/img_1521713489_5ab381518cdb84.53668732.jpeg', 'image/jpeg', '879394', '2018-03-22 17:11:29', '2018-03-22 17:11:29', '1');
INSERT INTO `files` VALUES ('399', 'anh4x6.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521773064_5ab46a08a61a33.87975399.jpeg', 'collection/2018/03/23/img_1521773064_5ab46a08a61a33.87975399.jpeg', 'image/jpeg', '3276', '2018-03-23 09:44:24', '2018-03-23 09:44:24', '1');
INSERT INTO `files` VALUES ('400', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521773155_5ab46a63e71f89.87046547.jpeg', 'collection/2018/03/23/img_1521773155_5ab46a63e71f89.87046547.jpeg', 'image/jpeg', '845941', '2018-03-23 09:45:55', '2018-03-23 09:45:55', '1');
INSERT INTO `files` VALUES ('401', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521773490_5ab46bb27ffdd3.33797960.jpeg', 'collection/2018/03/23/img_1521773490_5ab46bb27ffdd3.33797960.jpeg', 'image/jpeg', '595284', '2018-03-23 09:51:30', '2018-03-23 09:51:30', '1');
INSERT INTO `files` VALUES ('402', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521773602_5ab46c22064906.51526330.jpeg', 'collection/2018/03/23/img_1521773602_5ab46c22064906.51526330.jpeg', 'image/jpeg', '775702', '2018-03-23 09:53:22', '2018-03-23 09:53:22', '1');
INSERT INTO `files` VALUES ('403', 'anh4x6.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521773840_5ab46d10429491.73126883.jpeg', 'collection/2018/03/23/img_1521773840_5ab46d10429491.73126883.jpeg', 'image/jpeg', '3276', '2018-03-23 09:57:20', '2018-03-23 09:57:20', '1');
INSERT INTO `files` VALUES ('404', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521774304_5ab46ee06250c8.02506241.jpeg', 'collection/2018/03/23/img_1521774304_5ab46ee06250c8.02506241.jpeg', 'image/jpeg', '775702', '2018-03-23 10:05:04', '2018-03-23 10:05:04', '1');
INSERT INTO `files` VALUES ('405', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521777259_5ab47a6b82da77.36810206.jpeg', 'collection/2018/03/23/img_1521777259_5ab47a6b82da77.36810206.jpeg', 'image/jpeg', '879394', '2018-03-23 10:54:19', '2018-03-23 10:54:19', '1');
INSERT INTO `files` VALUES ('406', '5ab4b044d2527_1521791044_20180323.jpg', 'files/decision/2018/03/5ab4b044d2527_1521791044_20180323.jpg', 'files/decision/2018/03/5ab4b044d2527_1521791044_20180323.jpg', 'image/jpeg', '845941', '2018-03-23 14:44:04', '2018-03-23 14:44:04', '1');
INSERT INTO `files` VALUES ('407', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521792651_5ab4b68ba553b1.86924360.jpeg', 'collection/2018/03/23/img_1521792651_5ab4b68ba553b1.86924360.jpeg', 'image/jpeg', '620888', '2018-03-23 15:10:51', '2018-03-23 15:10:51', '1');
INSERT INTO `files` VALUES ('408', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/23/img_1521796242_5ab4c492d0db12.65300852.jpeg', 'collection/2018/03/23/img_1521796242_5ab4c492d0db12.65300852.jpeg', 'image/jpeg', '879394', '2018-03-23 16:10:42', '2018-03-23 16:10:42', '1');
INSERT INTO `files` VALUES ('409', '5ab67f7ddf374_1521909629_20180324.png', 'files/decision/2018/03/5ab67f7ddf374_1521909629_20180324.png', 'files/decision/2018/03/5ab67f7ddf374_1521909629_20180324.png', 'image/png', '18859', '2018-03-24 23:40:29', '2018-03-24 23:40:29', '1');
INSERT INTO `files` VALUES ('410', '5ab67fa4f0420_1521909668_20180324.png', 'files/decision/2018/03/5ab67fa4f0420_1521909668_20180324.png', 'files/decision/2018/03/5ab67fa4f0420_1521909668_20180324.png', 'image/png', '18859', '2018-03-24 23:41:08', '2018-03-24 23:41:08', '1');
INSERT INTO `files` VALUES ('411', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522028059_5ab84e1be9d855.30998359.jpeg', 'collection/2018/03/26/img_1522028059_5ab84e1be9d855.30998359.jpeg', 'image/jpeg', '845941', '2018-03-26 08:34:19', '2018-03-26 08:34:19', '1');
INSERT INTO `files` VALUES ('412', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522028212_5ab84eb4eb5699.68372883.jpeg', 'collection/2018/03/26/img_1522028212_5ab84eb4eb5699.68372883.jpeg', 'image/jpeg', '775702', '2018-03-26 08:36:52', '2018-03-26 08:36:52', '1');
INSERT INTO `files` VALUES ('413', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522028273_5ab84ef1cb4635.77182738.jpeg', 'collection/2018/03/26/img_1522028273_5ab84ef1cb4635.77182738.jpeg', 'image/jpeg', '775702', '2018-03-26 08:37:53', '2018-03-26 08:37:53', '1');
INSERT INTO `files` VALUES ('414', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522030476_5ab8578c13e0c5.59240225.jpeg', 'collection/2018/03/26/img_1522030476_5ab8578c13e0c5.59240225.jpeg', 'image/jpeg', '879394', '2018-03-26 09:14:36', '2018-03-26 09:14:36', '1');
INSERT INTO `files` VALUES ('415', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522030589_5ab857fdd941a2.99713462.jpeg', 'collection/2018/03/26/img_1522030589_5ab857fdd941a2.99713462.jpeg', 'image/jpeg', '879394', '2018-03-26 09:16:29', '2018-03-26 09:16:29', '1');
INSERT INTO `files` VALUES ('416', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522030674_5ab858524a7611.20019330.jpeg', 'collection/2018/03/26/img_1522030674_5ab858524a7611.20019330.jpeg', 'image/jpeg', '879394', '2018-03-26 09:17:54', '2018-03-26 09:17:54', '1');
INSERT INTO `files` VALUES ('417', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522030763_5ab858ab34d813.65221874.jpeg', 'collection/2018/03/26/img_1522030763_5ab858ab34d813.65221874.jpeg', 'image/jpeg', '845941', '2018-03-26 09:19:23', '2018-03-26 09:19:23', '1');
INSERT INTO `files` VALUES ('418', 'IMG_7205.JPG', '/var/www/html/public/files/collection/2018/03/26/img_1522033210_5ab8623a059250.24557980.jpeg', 'collection/2018/03/26/img_1522033210_5ab8623a059250.24557980.jpeg', 'image/jpeg', '2183151', '2018-03-26 10:00:10', '2018-03-26 10:00:10', '1');
INSERT INTO `files` VALUES ('419', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522033315_5ab862a35b99f3.96890394.jpeg', 'collection/2018/03/26/img_1522033315_5ab862a35b99f3.96890394.jpeg', 'image/jpeg', '845941', '2018-03-26 10:01:55', '2018-03-26 10:01:55', '1');
INSERT INTO `files` VALUES ('420', '5ab869691627b_1522035049_20180326.jpg', 'files/decision/2018/03/5ab869691627b_1522035049_20180326.jpg', 'files/decision/2018/03/5ab869691627b_1522035049_20180326.jpg', 'image/jpeg', '780831', '2018-03-26 10:30:49', '2018-03-26 10:30:49', '1');
INSERT INTO `files` VALUES ('421', '5ab86981dd9a9_1522035073_20180326.jpg', 'files/decision/2018/03/5ab86981dd9a9_1522035073_20180326.jpg', 'files/decision/2018/03/5ab86981dd9a9_1522035073_20180326.jpg', 'image/jpeg', '780831', '2018-03-26 10:31:13', '2018-03-26 10:31:13', '1');
INSERT INTO `files` VALUES ('422', '5ab869935f2fb_1522035091_20180326.jpg', 'files/decision/2018/03/5ab869935f2fb_1522035091_20180326.jpg', 'files/decision/2018/03/5ab869935f2fb_1522035091_20180326.jpg', 'image/jpeg', '775702', '2018-03-26 10:31:31', '2018-03-26 10:31:31', '1');
INSERT INTO `files` VALUES ('423', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522039909_5ab87c653d4ac3.35791307.jpeg', 'collection/2018/03/26/img_1522039909_5ab87c653d4ac3.35791307.jpeg', 'image/jpeg', '775702', '2018-03-26 11:51:49', '2018-03-26 11:51:49', '1');
INSERT INTO `files` VALUES ('424', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522039975_5ab87ca70dce97.89707261.jpeg', 'collection/2018/03/26/img_1522039975_5ab87ca70dce97.89707261.jpeg', 'image/jpeg', '775702', '2018-03-26 11:52:55', '2018-03-26 11:52:55', '1');
INSERT INTO `files` VALUES ('425', '5ab898553c0ed_1522047061_20180326.jpg', 'files/decision/2018/03/5ab898553c0ed_1522047061_20180326.jpg', 'files/decision/2018/03/5ab898553c0ed_1522047061_20180326.jpg', 'image/jpeg', '775702', '2018-03-26 13:51:01', '2018-03-26 13:51:01', '1');
INSERT INTO `files` VALUES ('426', '5ab89867ddbab_1522047079_20180326.jpg', 'files/decision/2018/03/5ab89867ddbab_1522047079_20180326.jpg', 'files/decision/2018/03/5ab89867ddbab_1522047079_20180326.jpg', 'image/jpeg', '595284', '2018-03-26 13:51:19', '2018-03-26 13:51:19', '1');
INSERT INTO `files` VALUES ('427', '5ab8987aa54b0_1522047098_20180326.jpg', 'files/decision/2018/03/5ab8987aa54b0_1522047098_20180326.jpg', 'files/decision/2018/03/5ab8987aa54b0_1522047098_20180326.jpg', 'image/jpeg', '595284', '2018-03-26 13:51:38', '2018-03-26 13:51:38', '1');
INSERT INTO `files` VALUES ('428', '5ab8989f06c7b_1522047135_20180326.jpg', 'files/decision/2018/03/5ab8989f06c7b_1522047135_20180326.jpg', 'files/decision/2018/03/5ab8989f06c7b_1522047135_20180326.jpg', 'image/jpeg', '775702', '2018-03-26 13:52:15', '2018-03-26 13:52:15', '1');
INSERT INTO `files` VALUES ('429', '5ab89a80f27ed_1522047616_20180326.jpg', 'files/decision/2018/03/5ab89a80f27ed_1522047616_20180326.jpg', 'files/decision/2018/03/5ab89a80f27ed_1522047616_20180326.jpg', 'image/jpeg', '561276', '2018-03-26 14:00:16', '2018-03-26 14:00:16', '1');
INSERT INTO `files` VALUES ('430', '5ab89a8f0bd2f_1522047631_20180326.jpg', 'files/decision/2018/03/5ab89a8f0bd2f_1522047631_20180326.jpg', 'files/decision/2018/03/5ab89a8f0bd2f_1522047631_20180326.jpg', 'image/jpeg', '595284', '2018-03-26 14:00:31', '2018-03-26 14:00:31', '1');
INSERT INTO `files` VALUES ('431', '5abb38b9ea1fc_1522219193_20180328.jpg', 'files/decision/2018/03/5abb38b9ea1fc_1522219193_20180328.jpg', 'files/decision/2018/03/5abb38b9ea1fc_1522219193_20180328.jpg', 'image/jpeg', '879394', '2018-03-26 14:00:45', '2018-03-28 13:39:53', '1');
INSERT INTO `files` VALUES ('432', '5ab89aad48ed1_1522047661_20180326.jpg', 'files/decision/2018/03/5ab89aad48ed1_1522047661_20180326.jpg', 'files/decision/2018/03/5ab89aad48ed1_1522047661_20180326.jpg', 'image/jpeg', '775702', '2018-03-26 14:01:01', '2018-03-26 14:01:01', '1');
INSERT INTO `files` VALUES ('433', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522055542_5ab8b976c8e649.28057464.jpeg', 'collection/2018/03/26/img_1522055542_5ab8b976c8e649.28057464.jpeg', 'image/jpeg', '775702', '2018-03-26 16:12:22', '2018-03-26 16:12:22', '1');
INSERT INTO `files` VALUES ('434', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522055680_5ab8ba0001c519.57662504.jpeg', 'collection/2018/03/26/img_1522055680_5ab8ba0001c519.57662504.jpeg', 'image/jpeg', '620888', '2018-03-26 16:14:40', '2018-03-26 16:14:40', '1');
INSERT INTO `files` VALUES ('435', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522055758_5ab8ba4ea7c3c9.03627970.jpeg', 'collection/2018/03/26/img_1522055758_5ab8ba4ea7c3c9.03627970.jpeg', 'image/jpeg', '775702', '2018-03-26 16:15:58', '2018-03-26 16:15:58', '1');
INSERT INTO `files` VALUES ('436', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522055800_5ab8ba7869c5c2.14002459.jpeg', 'collection/2018/03/26/img_1522055800_5ab8ba7869c5c2.14002459.jpeg', 'image/jpeg', '879394', '2018-03-26 16:16:40', '2018-03-26 16:16:40', '1');
INSERT INTO `files` VALUES ('437', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522055848_5ab8baa8efb975.84247809.jpeg', 'collection/2018/03/26/img_1522055848_5ab8baa8efb975.84247809.jpeg', 'image/jpeg', '777835', '2018-03-26 16:17:29', '2018-03-26 16:17:29', '1');
INSERT INTO `files` VALUES ('438', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522058529_5ab8c521712f62.63324620.jpeg', 'collection/2018/03/26/img_1522058529_5ab8c521712f62.63324620.jpeg', 'image/jpeg', '775702', '2018-03-26 17:02:09', '2018-03-26 17:02:09', '1');
INSERT INTO `files` VALUES ('439', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522058580_5ab8c554add3b8.59978484.jpeg', 'collection/2018/03/26/img_1522058580_5ab8c554add3b8.59978484.jpeg', 'image/jpeg', '561276', '2018-03-26 17:03:00', '2018-03-26 17:03:00', '1');
INSERT INTO `files` VALUES ('440', 'anh4x6.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522059447_5ab8c8b76f53e7.12672449.jpeg', 'collection/2018/03/26/img_1522059447_5ab8c8b76f53e7.12672449.jpeg', 'image/jpeg', '3276', '2018-03-26 17:17:27', '2018-03-26 17:17:27', '1');
INSERT INTO `files` VALUES ('441', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522059535_5ab8c90f678de5.67800777.jpeg', 'collection/2018/03/26/img_1522059535_5ab8c90f678de5.67800777.jpeg', 'image/jpeg', '780831', '2018-03-26 17:18:55', '2018-03-26 17:18:55', '1');
INSERT INTO `files` VALUES ('442', 'Untitled.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522059614_5ab8c95e168950.73502337.jpeg', 'collection/2018/03/26/img_1522059614_5ab8c95e168950.73502337.jpeg', 'image/jpeg', '4353', '2018-03-26 17:20:14', '2018-03-26 17:20:14', '1');
INSERT INTO `files` VALUES ('443', 'Untitled.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522059637_5ab8c975ad3493.98713843.jpeg', 'collection/2018/03/26/img_1522059637_5ab8c975ad3493.98713843.jpeg', 'image/jpeg', '4353', '2018-03-26 17:20:37', '2018-03-26 17:20:37', '1');
INSERT INTO `files` VALUES ('444', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522059791_5ab8ca0f1975e2.66933381.jpeg', 'collection/2018/03/26/img_1522059791_5ab8ca0f1975e2.66933381.jpeg', 'image/jpeg', '620888', '2018-03-26 17:23:11', '2018-03-26 17:23:11', '1');
INSERT INTO `files` VALUES ('445', 'addCLBchi hoi.png', '/var/www/html/public/files/collection/2018/03/26/img_1522059861_5ab8ca55dfeed4.88178022.png', 'collection/2018/03/26/img_1522059861_5ab8ca55dfeed4.88178022.png', 'image/png', '31499', '2018-03-26 17:24:21', '2018-03-26 17:24:21', '1');
INSERT INTO `files` VALUES ('446', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522060029_5ab8cafd38a396.73309787.jpeg', 'collection/2018/03/26/img_1522060029_5ab8cafd38a396.73309787.jpeg', 'image/jpeg', '780831', '2018-03-26 17:27:09', '2018-03-26 17:27:09', '1');
INSERT INTO `files` VALUES ('447', '2018-03-18_18-07-47.png', '/var/www/html/public/files/collection/2018/03/26/img_1522064881_5ab8ddf1306394.37303121.png', 'collection/2018/03/26/img_1522064881_5ab8ddf1306394.37303121.png', 'image/png', '15065', '2018-03-26 18:48:01', '2018-03-26 18:48:01', '1');
INSERT INTO `files` VALUES ('448', '1.PNG', '/var/www/html/public/files/collection/2018/03/26/img_1522065009_5ab8de712e3368.76485787.png', 'collection/2018/03/26/img_1522065009_5ab8de712e3368.76485787.png', 'image/png', '1073495', '2018-03-26 18:50:09', '2018-03-26 18:50:09', '1');
INSERT INTO `files` VALUES ('449', '1.PNG', '/var/www/html/public/files/collection/2018/03/26/img_1522065099_5ab8decbd10a21.63775041.png', 'collection/2018/03/26/img_1522065099_5ab8decbd10a21.63775041.png', 'image/png', '1073495', '2018-03-26 18:51:39', '2018-03-26 18:51:39', '1');
INSERT INTO `files` VALUES ('450', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522065180_5ab8df1ccdb287.49349005.jpeg', 'collection/2018/03/26/img_1522065180_5ab8df1ccdb287.49349005.jpeg', 'image/jpeg', '620888', '2018-03-26 18:53:00', '2018-03-26 18:53:00', '1');
INSERT INTO `files` VALUES ('451', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522077623_5ab90fb7252464.25533511.jpeg', 'collection/2018/03/26/img_1522077623_5ab90fb7252464.25533511.jpeg', 'image/jpeg', '620888', '2018-03-26 22:20:23', '2018-03-26 22:20:23', '1');
INSERT INTO `files` VALUES ('452', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522080318_5ab91a3eeabc29.89066629.jpeg', 'collection/2018/03/26/img_1522080318_5ab91a3eeabc29.89066629.jpeg', 'image/jpeg', '561276', '2018-03-26 23:05:18', '2018-03-26 23:05:18', '1');
INSERT INTO `files` VALUES ('453', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522080919_5ab91c9704ea85.72716688.jpeg', 'collection/2018/03/26/img_1522080919_5ab91c9704ea85.72716688.jpeg', 'image/jpeg', '777835', '2018-03-26 23:15:19', '2018-03-26 23:15:19', '1');
INSERT INTO `files` VALUES ('454', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522081237_5ab91dd54b7bc8.01184475.jpeg', 'collection/2018/03/26/img_1522081237_5ab91dd54b7bc8.01184475.jpeg', 'image/jpeg', '780831', '2018-03-26 23:20:37', '2018-03-26 23:20:37', '1');
INSERT INTO `files` VALUES ('455', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522081361_5ab91e51e0b7d7.45136992.jpeg', 'collection/2018/03/26/img_1522081361_5ab91e51e0b7d7.45136992.jpeg', 'image/jpeg', '620888', '2018-03-26 23:22:41', '2018-03-26 23:22:41', '1');
INSERT INTO `files` VALUES ('456', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522081479_5ab91ec77f8100.89010451.jpeg', 'collection/2018/03/26/img_1522081479_5ab91ec77f8100.89010451.jpeg', 'image/jpeg', '775702', '2018-03-26 23:24:39', '2018-03-26 23:24:39', '1');
INSERT INTO `files` VALUES ('457', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522082242_5ab921c26f4d01.91596632.jpeg', 'collection/2018/03/26/img_1522082242_5ab921c26f4d01.91596632.jpeg', 'image/jpeg', '620888', '2018-03-26 23:37:22', '2018-03-26 23:37:22', '1');
INSERT INTO `files` VALUES ('458', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/26/img_1522082421_5ab92275abea18.36436618.jpeg', 'collection/2018/03/26/img_1522082421_5ab92275abea18.36436618.jpeg', 'image/jpeg', '879394', '2018-03-26 23:40:21', '2018-03-26 23:40:21', '1');
INSERT INTO `files` VALUES ('459', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522113549_5ab99c0d10bd11.79695417.jpeg', 'collection/2018/03/27/img_1522113549_5ab99c0d10bd11.79695417.jpeg', 'image/jpeg', '595284', '2018-03-27 08:19:09', '2018-03-27 08:19:09', '1');
INSERT INTO `files` VALUES ('460', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522114440_5ab99f886e5b65.08173467.jpeg', 'collection/2018/03/27/img_1522114440_5ab99f886e5b65.08173467.jpeg', 'image/jpeg', '777835', '2018-03-27 08:34:00', '2018-03-27 08:34:00', '1');
INSERT INTO `files` VALUES ('461', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522119847_5ab9b4a7ac6573.55710639.jpeg', 'collection/2018/03/27/img_1522119847_5ab9b4a7ac6573.55710639.jpeg', 'image/jpeg', '879394', '2018-03-27 10:04:07', '2018-03-27 10:04:07', '1');
INSERT INTO `files` VALUES ('462', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522120419_5ab9b6e344e696.35478645.jpeg', 'collection/2018/03/27/img_1522120419_5ab9b6e344e696.35478645.jpeg', 'image/jpeg', '879394', '2018-03-27 10:13:39', '2018-03-27 10:13:39', '1');
INSERT INTO `files` VALUES ('463', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522124379_5ab9c65bb46471.22379471.jpeg', 'collection/2018/03/27/img_1522124379_5ab9c65bb46471.22379471.jpeg', 'image/jpeg', '879394', '2018-03-27 11:19:39', '2018-03-27 11:19:39', '1');
INSERT INTO `files` VALUES ('464', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522133203_5ab9e8d3edeb99.05959898.jpeg', 'collection/2018/03/27/img_1522133203_5ab9e8d3edeb99.05959898.jpeg', 'image/jpeg', '780831', '2018-03-27 13:46:44', '2018-03-27 13:46:44', '1');
INSERT INTO `files` VALUES ('465', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522133930_5ab9ebaa5788c5.25299161.jpeg', 'collection/2018/03/27/img_1522133930_5ab9ebaa5788c5.25299161.jpeg', 'image/jpeg', '777835', '2018-03-27 13:58:50', '2018-03-27 13:58:50', '1');
INSERT INTO `files` VALUES ('466', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522135604_5ab9f23494bac5.56776952.jpeg', 'collection/2018/03/27/img_1522135604_5ab9f23494bac5.56776952.jpeg', 'image/jpeg', '595284', '2018-03-27 14:26:44', '2018-03-27 14:26:44', '1');
INSERT INTO `files` VALUES ('467', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522136412_5ab9f55ccf2f70.45092992.jpeg', 'collection/2018/03/27/img_1522136412_5ab9f55ccf2f70.45092992.jpeg', 'image/jpeg', '845941', '2018-03-27 14:40:12', '2018-03-27 14:40:12', '1');
INSERT INTO `files` VALUES ('468', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522137759_5ab9fa9fcce257.45374061.jpeg', 'collection/2018/03/27/img_1522137759_5ab9fa9fcce257.45374061.jpeg', 'image/jpeg', '561276', '2018-03-27 15:02:39', '2018-03-27 15:02:39', '1');
INSERT INTO `files` VALUES ('469', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522139753_5aba0269127eb7.08666750.jpeg', 'collection/2018/03/27/img_1522139753_5aba0269127eb7.08666750.jpeg', 'image/jpeg', '595284', '2018-03-27 15:35:53', '2018-03-27 15:35:53', '1');
INSERT INTO `files` VALUES ('470', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522142782_5aba0e3e03f7f6.70033512.jpeg', 'collection/2018/03/27/img_1522142782_5aba0e3e03f7f6.70033512.jpeg', 'image/jpeg', '561276', '2018-03-27 16:26:22', '2018-03-27 16:26:22', '1');
INSERT INTO `files` VALUES ('471', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522144413_5aba149d259604.20521497.jpeg', 'collection/2018/03/27/img_1522144413_5aba149d259604.20521497.jpeg', 'image/jpeg', '775702', '2018-03-27 16:53:33', '2018-03-27 16:53:33', '1');
INSERT INTO `files` VALUES ('472', '5aba222bdba34_1522147883_20180327.jpg', 'files/decision/2018/03/5aba222bdba34_1522147883_20180327.jpg', 'files/decision/2018/03/5aba222bdba34_1522147883_20180327.jpg', 'image/jpeg', '879394', '2018-03-27 17:51:23', '2018-03-27 17:51:23', '1');
INSERT INTO `files` VALUES ('473', '5aba22945c5f5_1522147988_20180327.jpg', 'files/decision/2018/03/5aba22945c5f5_1522147988_20180327.jpg', 'files/decision/2018/03/5aba22945c5f5_1522147988_20180327.jpg', 'image/jpeg', '879394', '2018-03-27 17:53:08', '2018-03-27 17:53:08', '1');
INSERT INTO `files` VALUES ('474', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522170074_5aba78da1e5229.60135159.jpeg', 'collection/2018/03/27/img_1522170074_5aba78da1e5229.60135159.jpeg', 'image/jpeg', '879394', '2018-03-28 00:01:14', '2018-03-28 00:01:14', '1');
INSERT INTO `files` VALUES ('475', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/27/img_1522170308_5aba79c4733170.64970502.jpeg', 'collection/2018/03/27/img_1522170308_5aba79c4733170.64970502.jpeg', 'image/jpeg', '595284', '2018-03-28 00:05:08', '2018-03-28 00:05:08', '1');
INSERT INTO `files` VALUES ('476', '5abaf02b60cac_1522200619_20180328.jpg', 'files/decision/2018/03/5abaf02b60cac_1522200619_20180328.jpg', 'files/decision/2018/03/5abaf02b60cac_1522200619_20180328.jpg', 'image/jpeg', '780831', '2018-03-28 08:30:19', '2018-03-28 08:30:19', '1');
INSERT INTO `files` VALUES ('477', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522202700_5abaf84c1bfda7.05515239.jpeg', 'collection/2018/03/28/img_1522202700_5abaf84c1bfda7.05515239.jpeg', 'image/jpeg', '777835', '2018-03-28 09:05:00', '2018-03-28 09:05:00', '1');
INSERT INTO `files` VALUES ('478', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522204555_5abaff8b4768d5.14101522.jpeg', 'collection/2018/03/28/img_1522204555_5abaff8b4768d5.14101522.jpeg', 'image/jpeg', '845941', '2018-03-28 09:35:55', '2018-03-28 09:35:55', '1');
INSERT INTO `files` VALUES ('479', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522204716_5abb002c2e7801.33573484.jpeg', 'collection/2018/03/28/img_1522204716_5abb002c2e7801.33573484.jpeg', 'image/jpeg', '845941', '2018-03-28 09:38:36', '2018-03-28 09:38:36', '1');
INSERT INTO `files` VALUES ('480', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522204908_5abb00ec579376.97567911.jpeg', 'collection/2018/03/28/img_1522204908_5abb00ec579376.97567911.jpeg', 'image/jpeg', '879394', '2018-03-28 09:41:48', '2018-03-28 09:41:48', '1');
INSERT INTO `files` VALUES ('481', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522207001_5abb0919e16bb4.40660733.jpeg', 'collection/2018/03/28/img_1522207001_5abb0919e16bb4.40660733.jpeg', 'image/jpeg', '780831', '2018-03-28 10:16:41', '2018-03-28 10:16:41', '1');
INSERT INTO `files` VALUES ('482', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522207694_5abb0bce135fb8.05084874.jpeg', 'collection/2018/03/28/img_1522207694_5abb0bce135fb8.05084874.jpeg', 'image/jpeg', '775702', '2018-03-28 10:28:14', '2018-03-28 10:28:14', '1');
INSERT INTO `files` VALUES ('483', '5abb17b110417_1522210737_20180328.jpg', 'files/decision/2018/03/5abb17b110417_1522210737_20180328.jpg', 'files/decision/2018/03/5abb17b110417_1522210737_20180328.jpg', 'image/jpeg', '879394', '2018-03-28 11:18:57', '2018-03-28 11:18:57', '1');
INSERT INTO `files` VALUES ('484', '5abb1be2bb39e_1522211810_20180328.jpg', 'files/decision/2018/03/5abb1be2bb39e_1522211810_20180328.jpg', 'files/decision/2018/03/5abb1be2bb39e_1522211810_20180328.jpg', 'image/jpeg', '845941', '2018-03-28 11:36:50', '2018-03-28 11:36:50', '1');
INSERT INTO `files` VALUES ('485', '5abb1d9b419e4_1522212251_20180328.jpg', 'files/decision/2018/03/5abb1d9b419e4_1522212251_20180328.jpg', 'files/decision/2018/03/5abb1d9b419e4_1522212251_20180328.jpg', 'image/jpeg', '777835', '2018-03-28 11:44:11', '2018-03-28 11:44:11', '1');
INSERT INTO `files` VALUES ('486', '5abb3a3b72c74_1522219579_20180328.jpg', 'files/decision/2018/03/5abb3a3b72c74_1522219579_20180328.jpg', 'files/decision/2018/03/5abb3a3b72c74_1522219579_20180328.jpg', 'image/jpeg', '879394', '2018-03-28 13:46:19', '2018-03-28 13:46:19', '1');
INSERT INTO `files` VALUES ('487', '5abb403e71ac3_1522221118_20180328.jpg', 'files/decision/2018/03/5abb403e71ac3_1522221118_20180328.jpg', 'files/decision/2018/03/5abb403e71ac3_1522221118_20180328.jpg', 'image/jpeg', '780831', '2018-03-28 14:11:58', '2018-03-28 14:11:58', '1');
INSERT INTO `files` VALUES ('488', '5abb4054158df_1522221140_20180328.jpg', 'files/decision/2018/03/5abb4054158df_1522221140_20180328.jpg', 'files/decision/2018/03/5abb4054158df_1522221140_20180328.jpg', 'image/jpeg', '775702', '2018-03-28 14:12:20', '2018-03-28 14:12:20', '1');
INSERT INTO `files` VALUES ('489', '5abb406dd1755_1522221165_20180328.jpg', 'files/decision/2018/03/5abb406dd1755_1522221165_20180328.jpg', 'files/decision/2018/03/5abb406dd1755_1522221165_20180328.jpg', 'image/jpeg', '777835', '2018-03-28 14:12:45', '2018-03-28 14:12:45', '1');
INSERT INTO `files` VALUES ('490', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522221294_5abb40eeb7c5d3.70675695.jpeg', 'collection/2018/03/28/img_1522221294_5abb40eeb7c5d3.70675695.jpeg', 'image/jpeg', '620888', '2018-03-28 14:14:54', '2018-03-28 14:14:54', '1');
INSERT INTO `files` VALUES ('491', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522221341_5abb411d686ad9.29810651.jpeg', 'collection/2018/03/28/img_1522221341_5abb411d686ad9.29810651.jpeg', 'image/jpeg', '780831', '2018-03-28 14:15:41', '2018-03-28 14:15:41', '1');
INSERT INTO `files` VALUES ('492', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522221467_5abb419be2c147.12938987.jpeg', 'collection/2018/03/28/img_1522221467_5abb419be2c147.12938987.jpeg', 'image/jpeg', '879394', '2018-03-28 14:17:47', '2018-03-28 14:17:47', '1');
INSERT INTO `files` VALUES ('493', '5abb43eb76b9a_1522222059_20180328.jpg', 'files/decision/2018/03/5abb43eb76b9a_1522222059_20180328.jpg', 'files/decision/2018/03/5abb43eb76b9a_1522222059_20180328.jpg', 'image/jpeg', '775702', '2018-03-28 14:27:39', '2018-03-28 14:27:39', '1');
INSERT INTO `files` VALUES ('494', '5abb43f68caf1_1522222070_20180328.jpg', 'files/decision/2018/03/5abb43f68caf1_1522222070_20180328.jpg', 'files/decision/2018/03/5abb43f68caf1_1522222070_20180328.jpg', 'image/jpeg', '777835', '2018-03-28 14:27:50', '2018-03-28 14:27:50', '1');
INSERT INTO `files` VALUES ('495', '5abb4660dd3f4_1522222688_20180328.jpg', 'files/decision/2018/03/5abb4660dd3f4_1522222688_20180328.jpg', 'files/decision/2018/03/5abb4660dd3f4_1522222688_20180328.jpg', 'image/jpeg', '879394', '2018-03-28 14:38:08', '2018-03-28 14:38:08', '1');
INSERT INTO `files` VALUES ('496', '5abb467809f1f_1522222712_20180328.png', 'files/decision/2018/03/5abb467809f1f_1522222712_20180328.png', 'files/decision/2018/03/5abb467809f1f_1522222712_20180328.png', 'image/jpeg', '620888', '2018-03-28 14:38:32', '2018-03-28 14:38:32', '1');
INSERT INTO `files` VALUES ('497', '5abb468dc8e87_1522222733_20180328.jpg', 'files/decision/2018/03/5abb468dc8e87_1522222733_20180328.jpg', 'files/decision/2018/03/5abb468dc8e87_1522222733_20180328.jpg', 'image/jpeg', '845941', '2018-03-28 14:38:53', '2018-03-28 14:38:53', '1');
INSERT INTO `files` VALUES ('498', '5abb469ec6c11_1522222750_20180328.jpg', 'files/decision/2018/03/5abb469ec6c11_1522222750_20180328.jpg', 'files/decision/2018/03/5abb469ec6c11_1522222750_20180328.jpg', 'image/jpeg', '777835', '2018-03-28 14:39:10', '2018-03-28 14:39:10', '1');
INSERT INTO `files` VALUES ('499', '5abb46c292191_1522222786_20180328.jpg', 'files/decision/2018/03/5abb46c292191_1522222786_20180328.jpg', 'files/decision/2018/03/5abb46c292191_1522222786_20180328.jpg', 'image/jpeg', '620888', '2018-03-28 14:39:46', '2018-03-28 14:39:46', '1');
INSERT INTO `files` VALUES ('500', '5abb476e3fe09_1522222958_20180328.jpg', 'files/decision/2018/03/5abb476e3fe09_1522222958_20180328.jpg', 'files/decision/2018/03/5abb476e3fe09_1522222958_20180328.jpg', 'image/jpeg', '777835', '2018-03-28 14:42:38', '2018-03-28 14:42:38', '1');
INSERT INTO `files` VALUES ('501', '5abb482ad3e70_1522223146_20180328.jpg', 'files/decision/2018/03/5abb482ad3e70_1522223146_20180328.jpg', 'files/decision/2018/03/5abb482ad3e70_1522223146_20180328.jpg', 'image/jpeg', '845941', '2018-03-28 14:45:46', '2018-03-28 14:45:46', '1');
INSERT INTO `files` VALUES ('502', '5abb49137785b_1522223379_20180328.jpg', 'files/decision/2018/03/5abb49137785b_1522223379_20180328.jpg', 'files/decision/2018/03/5abb49137785b_1522223379_20180328.jpg', 'image/jpeg', '777835', '2018-03-28 14:49:39', '2018-03-28 14:49:39', '1');
INSERT INTO `files` VALUES ('503', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522223958_5abb4b5682b943.96337498.jpeg', 'collection/2018/03/28/img_1522223958_5abb4b5682b943.96337498.jpeg', 'image/jpeg', '620888', '2018-03-28 14:59:18', '2018-03-28 14:59:18', '1');
INSERT INTO `files` VALUES ('504', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522224136_5abb4c08593b30.91335632.jpeg', 'collection/2018/03/28/img_1522224136_5abb4c08593b30.91335632.jpeg', 'image/jpeg', '879394', '2018-03-28 15:02:16', '2018-03-28 15:02:16', '1');
INSERT INTO `files` VALUES ('505', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522224700_5abb4e3c07d872.81921245.jpeg', 'collection/2018/03/28/img_1522224700_5abb4e3c07d872.81921245.jpeg', 'image/jpeg', '595284', '2018-03-28 15:11:40', '2018-03-28 15:11:40', '1');
INSERT INTO `files` VALUES ('506', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522224772_5abb4e846990d1.04282085.jpeg', 'collection/2018/03/28/img_1522224772_5abb4e846990d1.04282085.jpeg', 'image/jpeg', '845941', '2018-03-28 15:12:52', '2018-03-28 15:12:52', '1');
INSERT INTO `files` VALUES ('507', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522224799_5abb4e9fd548f9.52369436.jpeg', 'collection/2018/03/28/img_1522224799_5abb4e9fd548f9.52369436.jpeg', 'image/jpeg', '879394', '2018-03-28 15:13:19', '2018-03-28 15:13:19', '1');
INSERT INTO `files` VALUES ('508', '5abb4f877fa8c_1522225031_20180328.jpg', 'files/decision/2018/03/5abb4f877fa8c_1522225031_20180328.jpg', 'files/decision/2018/03/5abb4f877fa8c_1522225031_20180328.jpg', 'image/jpeg', '775702', '2018-03-28 15:17:11', '2018-03-28 15:17:11', '1');
INSERT INTO `files` VALUES ('509', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522225692_5abb521c555252.83330728.jpeg', 'collection/2018/03/28/img_1522225692_5abb521c555252.83330728.jpeg', 'image/jpeg', '595284', '2018-03-28 15:28:12', '2018-03-28 15:28:12', '1');
INSERT INTO `files` VALUES ('510', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522227669_5abb59d5210218.83275092.jpeg', 'collection/2018/03/28/img_1522227669_5abb59d5210218.83275092.jpeg', 'image/jpeg', '777835', '2018-03-28 16:01:09', '2018-03-28 16:01:09', '1');
INSERT INTO `files` VALUES ('511', '5abb5aa0a649c_1522227872_20180328.jpg', 'files/decision/2018/03/5abb5aa0a649c_1522227872_20180328.jpg', 'files/decision/2018/03/5abb5aa0a649c_1522227872_20180328.jpg', 'image/jpeg', '780831', '2018-03-28 16:04:32', '2018-03-28 16:04:32', '1');
INSERT INTO `files` VALUES ('512', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522228354_5abb5c828ade79.21949849.jpeg', 'collection/2018/03/28/img_1522228354_5abb5c828ade79.21949849.jpeg', 'image/jpeg', '879394', '2018-03-28 16:12:34', '2018-03-28 16:12:34', '1');
INSERT INTO `files` VALUES ('513', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522229202_5abb5fd2bf2b45.48275156.jpeg', 'collection/2018/03/28/img_1522229202_5abb5fd2bf2b45.48275156.jpeg', 'image/jpeg', '595284', '2018-03-28 16:26:42', '2018-03-28 16:26:42', '1');
INSERT INTO `files` VALUES ('514', '5abb60b4080a4_1522229428_20180328.jpg', 'files/decision/2018/03/5abb60b4080a4_1522229428_20180328.jpg', 'files/decision/2018/03/5abb60b4080a4_1522229428_20180328.jpg', 'image/jpeg', '780831', '2018-03-28 16:30:28', '2018-03-28 16:30:28', '1');
INSERT INTO `files` VALUES ('515', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522232827_5abb6dfb5c7480.79449206.jpeg', 'collection/2018/03/28/img_1522232827_5abb6dfb5c7480.79449206.jpeg', 'image/jpeg', '595284', '2018-03-28 17:27:07', '2018-03-28 17:27:07', '1');
INSERT INTO `files` VALUES ('516', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522233182_5abb6f5e8306f5.07090865.jpeg', 'collection/2018/03/28/img_1522233182_5abb6f5e8306f5.07090865.jpeg', 'image/jpeg', '561276', '2018-03-28 17:33:02', '2018-03-28 17:33:02', '1');
INSERT INTO `files` VALUES ('517', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522233220_5abb6f842ce1f4.48042420.jpeg', 'collection/2018/03/28/img_1522233220_5abb6f842ce1f4.48042420.jpeg', 'image/jpeg', '775702', '2018-03-28 17:33:40', '2018-03-28 17:33:40', '1');
INSERT INTO `files` VALUES ('518', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/28/img_1522234288_5abb73b0765374.26968664.jpeg', 'collection/2018/03/28/img_1522234288_5abb73b0765374.26968664.jpeg', 'image/jpeg', '780831', '2018-03-28 17:51:28', '2018-03-28 17:51:28', '1');
INSERT INTO `files` VALUES ('519', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/29/img_1522286194_5abc3e726fe3f3.50226624.jpeg', 'collection/2018/03/29/img_1522286194_5abc3e726fe3f3.50226624.jpeg', 'image/jpeg', '595284', '2018-03-29 08:16:34', '2018-03-29 08:16:34', '1');
INSERT INTO `files` VALUES ('520', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/29/img_1522286278_5abc3ec6481985.83881824.jpeg', 'collection/2018/03/29/img_1522286278_5abc3ec6481985.83881824.jpeg', 'image/jpeg', '595284', '2018-03-29 08:17:58', '2018-03-29 08:17:58', '1');
INSERT INTO `files` VALUES ('521', '5abc4b7025f57_1522289520_20180329.jpg', 'files/decision/2018/03/5abc4b7025f57_1522289520_20180329.jpg', 'files/decision/2018/03/5abc4b7025f57_1522289520_20180329.jpg', 'image/jpeg', '845941', '2018-03-29 09:12:00', '2018-03-29 09:12:00', '1');
INSERT INTO `files` VALUES ('522', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/29/img_1522291965_5abc54fd78b865.50292075.jpeg', 'collection/2018/03/29/img_1522291965_5abc54fd78b865.50292075.jpeg', 'image/jpeg', '561276', '2018-03-29 09:52:45', '2018-03-29 09:52:45', '1');
INSERT INTO `files` VALUES ('523', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/29/img_1522292243_5abc561306dbd1.91129763.jpeg', 'collection/2018/03/29/img_1522292243_5abc561306dbd1.91129763.jpeg', 'image/jpeg', '845941', '2018-03-29 09:57:23', '2018-03-29 09:57:23', '1');
INSERT INTO `files` VALUES ('524', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/29/img_1522292618_5abc578a040171.82671273.jpeg', 'collection/2018/03/29/img_1522292618_5abc578a040171.82671273.jpeg', 'image/jpeg', '595284', '2018-03-29 10:03:38', '2018-03-29 10:03:38', '1');
INSERT INTO `files` VALUES ('525', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/29/img_1522292960_5abc58e0a8c242.77131384.jpeg', 'collection/2018/03/29/img_1522292960_5abc58e0a8c242.77131384.jpeg', 'image/jpeg', '845941', '2018-03-29 10:09:20', '2018-03-29 10:09:20', '1');
INSERT INTO `files` VALUES ('526', 'Lighthouse.jpg', '/var/www/html/public/files/collection/2018/03/29/img_1522293156_5abc59a4106c58.89624530.jpeg', 'collection/2018/03/29/img_1522293156_5abc59a4106c58.89624530.jpeg', 'image/jpeg', '561276', '2018-03-29 10:12:36', '2018-03-29 10:12:36', '1');
INSERT INTO `files` VALUES ('527', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/29/img_1522296058_5abc64fa624b21.92995182.jpeg', 'collection/2018/03/29/img_1522296058_5abc64fa624b21.92995182.jpeg', 'image/jpeg', '879394', '2018-03-29 11:00:58', '2018-03-29 11:00:58', '1');
INSERT INTO `files` VALUES ('528', '5abca383d545a_1522312067_20180329.pdf', 'files/decision/2018/03/5abca383d545a_1522312067_20180329.pdf', 'files/decision/2018/03/5abca383d545a_1522312067_20180329.pdf', 'application/pdf', '1032462', '2018-03-29 15:27:47', '2018-03-29 15:27:47', '1');
INSERT INTO `files` VALUES ('529', '5abcbe581fc58_1522318936_20180329.jpg', 'files/decision/2018/03/5abcbe581fc58_1522318936_20180329.jpg', 'files/decision/2018/03/5abcbe581fc58_1522318936_20180329.jpg', 'image/jpeg', '595284', '2018-03-29 17:22:16', '2018-03-29 17:22:16', '1');
INSERT INTO `files` VALUES ('530', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522373893_5abd95054b2d91.59641759.jpeg', 'collection/2018/03/30/img_1522373893_5abd95054b2d91.59641759.jpeg', 'image/jpeg', '879394', '2018-03-30 08:38:13', '2018-03-30 08:38:13', '1');
INSERT INTO `files` VALUES ('531', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522373996_5abd956c8daff1.80715444.jpeg', 'collection/2018/03/30/img_1522373996_5abd956c8daff1.80715444.jpeg', 'image/jpeg', '777835', '2018-03-30 08:39:56', '2018-03-30 08:39:56', '1');
INSERT INTO `files` VALUES ('532', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522381247_5abdb1bf625ae8.80232420.jpeg', 'collection/2018/03/30/img_1522381247_5abdb1bf625ae8.80232420.jpeg', 'image/jpeg', '845941', '2018-03-30 10:40:47', '2018-03-30 10:40:47', '1');
INSERT INTO `files` VALUES ('533', '5abdb2d8bd2e3_1522381528_20180330.jpg', 'files/decision/2018/03/5abdb2d8bd2e3_1522381528_20180330.jpg', 'files/decision/2018/03/5abdb2d8bd2e3_1522381528_20180330.jpg', 'image/jpeg', '620888', '2018-03-30 10:45:28', '2018-03-30 10:45:28', '1');
INSERT INTO `files` VALUES ('534', '5abdb2ea5db8e_1522381546_20180330.jpg', 'files/decision/2018/03/5abdb2ea5db8e_1522381546_20180330.jpg', 'files/decision/2018/03/5abdb2ea5db8e_1522381546_20180330.jpg', 'image/jpeg', '620888', '2018-03-30 10:45:46', '2018-03-30 10:45:46', '1');
INSERT INTO `files` VALUES ('535', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522382162_5abdb552c0c121.79618858.jpeg', 'collection/2018/03/30/img_1522382162_5abdb552c0c121.79618858.jpeg', 'image/jpeg', '879394', '2018-03-30 10:56:02', '2018-03-30 10:56:02', '1');
INSERT INTO `files` VALUES ('536', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522382341_5abdb60580f818.74758504.jpeg', 'collection/2018/03/30/img_1522382341_5abdb60580f818.74758504.jpeg', 'image/jpeg', '845941', '2018-03-30 10:59:01', '2018-03-30 10:59:01', '1');
INSERT INTO `files` VALUES ('537', '5abddd06ab1d7_1522392326_20180330.jpg', 'files/decision/2018/03/5abddd06ab1d7_1522392326_20180330.jpg', 'files/decision/2018/03/5abddd06ab1d7_1522392326_20180330.jpg', 'image/jpeg', '620888', '2018-03-30 13:45:26', '2018-03-30 13:45:26', '1');
INSERT INTO `files` VALUES ('538', '5abddd2922ec4_1522392361_20180330.jpg', 'files/decision/2018/03/5abddd2922ec4_1522392361_20180330.jpg', 'files/decision/2018/03/5abddd2922ec4_1522392361_20180330.jpg', 'image/jpeg', '775702', '2018-03-30 13:46:01', '2018-03-30 13:46:01', '1');
INSERT INTO `files` VALUES ('539', '5abdde1092ea2_1522392592_20180330.jpg', 'files/decision/2018/03/5abdde1092ea2_1522392592_20180330.jpg', 'files/decision/2018/03/5abdde1092ea2_1522392592_20180330.jpg', 'image/jpeg', '620888', '2018-03-30 13:49:52', '2018-03-30 13:49:52', '1');
INSERT INTO `files` VALUES ('540', '5abdde2123e16_1522392609_20180330.jpg', 'files/decision/2018/03/5abdde2123e16_1522392609_20180330.jpg', 'files/decision/2018/03/5abdde2123e16_1522392609_20180330.jpg', 'image/jpeg', '620888', '2018-03-30 13:50:09', '2018-03-30 13:50:09', '1');
INSERT INTO `files` VALUES ('541', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522393725_5abde27da2c8c9.56248181.jpeg', 'collection/2018/03/30/img_1522393725_5abde27da2c8c9.56248181.jpeg', 'image/jpeg', '879394', '2018-03-30 14:08:45', '2018-03-30 14:08:45', '1');
INSERT INTO `files` VALUES ('542', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522393897_5abde32907dab8.74687988.jpeg', 'collection/2018/03/30/img_1522393897_5abde32907dab8.74687988.jpeg', 'image/jpeg', '780831', '2018-03-30 14:11:37', '2018-03-30 14:11:37', '1');
INSERT INTO `files` VALUES ('543', 'Koala.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522393908_5abde334741132.74119549.jpeg', 'collection/2018/03/30/img_1522393908_5abde334741132.74119549.jpeg', 'image/jpeg', '780831', '2018-03-30 14:11:48', '2018-03-30 14:11:48', '1');
INSERT INTO `files` VALUES ('544', 'Tulips.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522399842_5abdfa62971df0.26402089.jpeg', 'collection/2018/03/30/img_1522399842_5abdfa62971df0.26402089.jpeg', 'image/jpeg', '620888', '2018-03-30 15:50:42', '2018-03-30 15:50:42', '1');
INSERT INTO `files` VALUES ('545', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522401811_5abe021386d353.64414278.jpeg', 'collection/2018/03/30/img_1522401811_5abe021386d353.64414278.jpeg', 'image/jpeg', '595284', '2018-03-30 16:23:31', '2018-03-30 16:23:31', '1');
INSERT INTO `files` VALUES ('546', 'Desert.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522403071_5abe06ff39c847.80636442.jpeg', 'collection/2018/03/30/img_1522403071_5abe06ff39c847.80636442.jpeg', 'image/jpeg', '845941', '2018-03-30 16:44:31', '2018-03-30 16:44:31', '1');
INSERT INTO `files` VALUES ('547', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522403079_5abe0707e329c0.08727869.jpeg', 'collection/2018/03/30/img_1522403079_5abe0707e329c0.08727869.jpeg', 'image/jpeg', '595284', '2018-03-30 16:44:39', '2018-03-30 16:44:39', '1');
INSERT INTO `files` VALUES ('548', '5abe0df53907c_1522404853_20180330.jpg', 'files/decision/2018/03/5abe0df53907c_1522404853_20180330.jpg', 'files/decision/2018/03/5abe0df53907c_1522404853_20180330.jpg', 'image/jpeg', '775702', '2018-03-30 17:14:13', '2018-03-30 17:14:13', '1');
INSERT INTO `files` VALUES ('549', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522405140_5abe0f14407250.88082579.jpeg', 'collection/2018/03/30/img_1522405140_5abe0f14407250.88082579.jpeg', 'image/jpeg', '775702', '2018-03-30 17:19:00', '2018-03-30 17:19:00', '1');
INSERT INTO `files` VALUES ('550', 'Penguins.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522405152_5abe0f20439fa3.59320380.jpeg', 'collection/2018/03/30/img_1522405152_5abe0f20439fa3.59320380.jpeg', 'image/jpeg', '777835', '2018-03-30 17:19:12', '2018-03-30 17:19:12', '1');
INSERT INTO `files` VALUES ('551', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522405164_5abe0f2c4f5668.53059748.jpeg', 'collection/2018/03/30/img_1522405164_5abe0f2c4f5668.53059748.jpeg', 'image/jpeg', '879394', '2018-03-30 17:19:24', '2018-03-30 17:19:24', '1');
INSERT INTO `files` VALUES ('552', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522427308_5abe65acf0b068.36241169.jpeg', 'collection/2018/03/30/img_1522427308_5abe65acf0b068.36241169.jpeg', 'image/jpeg', '89229', '2018-03-30 23:28:28', '2018-03-30 23:28:28', '1');
INSERT INTO `files` VALUES ('553', 'if_andrew_60496.png', '/var/www/html/public/files/collection/2018/03/30/img_1522427741_5abe675e006d42.41504645.png', 'collection/2018/03/30/img_1522427741_5abe675e006d42.41504645.png', 'image/png', '13038', '2018-03-30 23:35:42', '2018-03-30 23:35:42', '1');
INSERT INTO `files` VALUES ('554', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522427911_5abe68078fdb30.18446847.jpeg', 'collection/2018/03/30/img_1522427911_5abe68078fdb30.18446847.jpeg', 'image/jpeg', '1520', '2018-03-30 23:38:31', '2018-03-30 23:38:31', '1');
INSERT INTO `files` VALUES ('555', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522427952_5abe68308a12a8.59795029.jpeg', 'collection/2018/03/30/img_1522427952_5abe68308a12a8.59795029.jpeg', 'image/jpeg', '1520', '2018-03-30 23:39:12', '2018-03-30 23:39:12', '1');
INSERT INTO `files` VALUES ('556', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522427978_5abe684a266814.44119901.jpeg', 'collection/2018/03/30/img_1522427978_5abe684a266814.44119901.jpeg', 'image/jpeg', '1520', '2018-03-30 23:39:38', '2018-03-30 23:39:38', '1');
INSERT INTO `files` VALUES ('557', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522428172_5abe690c35fe98.34097932.jpeg', 'collection/2018/03/30/img_1522428172_5abe690c35fe98.34097932.jpeg', 'image/jpeg', '1520', '2018-03-30 23:42:52', '2018-03-30 23:42:52', '1');
INSERT INTO `files` VALUES ('558', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522428184_5abe6918c1bbb2.34300497.jpeg', 'collection/2018/03/30/img_1522428184_5abe6918c1bbb2.34300497.jpeg', 'image/jpeg', '1520', '2018-03-30 23:43:04', '2018-03-30 23:43:04', '1');
INSERT INTO `files` VALUES ('559', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522428262_5abe696633a921.41764023.jpeg', 'collection/2018/03/30/img_1522428262_5abe696633a921.41764023.jpeg', 'image/jpeg', '1520', '2018-03-30 23:44:22', '2018-03-30 23:44:22', '1');
INSERT INTO `files` VALUES ('560', 'jCOM-data-test.png', '/var/www/html/public/files/collection/2018/03/30/img_1522428266_5abe696aa651c8.87519859.png', 'collection/2018/03/30/img_1522428266_5abe696aa651c8.87519859.png', 'image/png', '114006', '2018-03-30 23:44:26', '2018-03-30 23:44:26', '1');
INSERT INTO `files` VALUES ('561', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522428272_5abe6970f34b53.82542620.jpeg', 'collection/2018/03/30/img_1522428272_5abe6970f34b53.82542620.jpeg', 'image/jpeg', '1520', '2018-03-30 23:44:32', '2018-03-30 23:44:32', '1');
INSERT INTO `files` VALUES ('562', 'jCOM-data-test.png', '/var/www/html/public/files/collection/2018/03/30/img_1522428761_5abe6b599c4f11.01755365.png', 'collection/2018/03/30/img_1522428761_5abe6b599c4f11.01755365.png', 'image/png', '114006', '2018-03-30 23:52:41', '2018-03-30 23:52:41', '1');
INSERT INTO `files` VALUES ('563', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522428763_5abe6b5b502b77.12938141.jpeg', 'collection/2018/03/30/img_1522428763_5abe6b5b502b77.12938141.jpeg', 'image/jpeg', '1520', '2018-03-30 23:52:43', '2018-03-30 23:52:43', '1');
INSERT INTO `files` VALUES ('564', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522428779_5abe6b6bb492f6.43350707.jpeg', 'collection/2018/03/30/img_1522428779_5abe6b6bb492f6.43350707.jpeg', 'image/jpeg', '1520', '2018-03-30 23:52:59', '2018-03-30 23:52:59', '1');
INSERT INTO `files` VALUES ('565', 'board-361516_640.jpg', '/var/www/html/public/files/collection/2018/03/30/img_1522429085_5abe6c9da95ec9.91630793.jpeg', 'collection/2018/03/30/img_1522429085_5abe6c9da95ec9.91630793.jpeg', 'image/jpeg', '1520', '2018-03-30 23:58:05', '2018-03-30 23:58:05', '1');
INSERT INTO `files` VALUES ('566', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522634215_5ac18de7061112.52958199.jpeg', 'collection/2018/04/02/img_1522634215_5ac18de7061112.52958199.jpeg', 'image/jpeg', '595284', '2018-04-02 08:56:55', '2018-04-02 08:56:55', '1');
INSERT INTO `files` VALUES ('567', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522637910_5ac19c562f7835.12655116.jpeg', 'collection/2018/04/02/img_1522637910_5ac19c562f7835.12655116.jpeg', 'image/jpeg', '595284', '2018-04-02 09:58:30', '2018-04-02 09:58:30', '1');
INSERT INTO `files` VALUES ('568', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522653230_5ac1d82e4ecfe3.57095945.jpeg', 'collection/2018/04/02/img_1522653230_5ac1d82e4ecfe3.57095945.jpeg', 'image/jpeg', '879394', '2018-04-02 14:13:50', '2018-04-02 14:13:50', '1');
INSERT INTO `files` VALUES ('569', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522653318_5ac1d88604e9c6.76036776.jpeg', 'collection/2018/04/02/img_1522653318_5ac1d88604e9c6.76036776.jpeg', 'image/jpeg', '595284', '2018-04-02 14:15:18', '2018-04-02 14:15:18', '1');
INSERT INTO `files` VALUES ('570', 'Desert.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522653950_5ac1dafe315384.20743179.jpeg', 'collection/2018/04/02/img_1522653950_5ac1dafe315384.20743179.jpeg', 'image/jpeg', '845941', '2018-04-02 14:25:50', '2018-04-02 14:25:50', '1');
INSERT INTO `files` VALUES ('571', 'Jellyfish.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522653990_5ac1db26081b71.77709848.jpeg', 'collection/2018/04/02/img_1522653990_5ac1db26081b71.77709848.jpeg', 'image/jpeg', '775702', '2018-04-02 14:26:30', '2018-04-02 14:26:30', '1');
INSERT INTO `files` VALUES ('572', 'Hydrangeas.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522656174_5ac1e3aee9abf0.98574137.jpeg', 'collection/2018/04/02/img_1522656174_5ac1e3aee9abf0.98574137.jpeg', 'image/jpeg', '595284', '2018-04-02 15:02:54', '2018-04-02 15:02:54', '1');
INSERT INTO `files` VALUES ('573', 'Chrysanthemum.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522656250_5ac1e3fab20026.34676420.jpeg', 'collection/2018/04/02/img_1522656250_5ac1e3fab20026.34676420.jpeg', 'image/jpeg', '879394', '2018-04-02 15:04:10', '2018-04-02 15:04:10', '1');
INSERT INTO `files` VALUES ('574', 'Desert.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522656296_5ac1e4280b3dd8.32443708.jpeg', 'collection/2018/04/02/img_1522656296_5ac1e4280b3dd8.32443708.jpeg', 'image/jpeg', '845941', '2018-04-02 15:04:56', '2018-04-02 15:04:56', '1');
INSERT INTO `files` VALUES ('575', '008.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522657317_5ac1e825120bf1.05097871.jpeg', 'collection/2018/04/02/img_1522657317_5ac1e825120bf1.05097871.jpeg', 'image/jpeg', '489818', '2018-04-02 15:21:57', '2018-04-02 15:21:57', '1');
INSERT INTO `files` VALUES ('576', '5ac1eab3596fb_1522657971_20180402.jpg', 'files/decision/2018/04/5ac1eab3596fb_1522657971_20180402.jpg', 'files/decision/2018/04/5ac1eab3596fb_1522657971_20180402.jpg', 'image/jpeg', '775702', '2018-04-02 15:32:51', '2018-04-02 15:32:51', '1');
INSERT INTO `files` VALUES ('577', '5ac1f2b95793c_1522660025_20180402.jpg', 'files/decision/2018/04/5ac1f2b95793c_1522660025_20180402.jpg', 'files/decision/2018/04/5ac1f2b95793c_1522660025_20180402.jpg', 'image/jpeg', '775702', '2018-04-02 16:07:05', '2018-04-02 16:07:05', '1');
INSERT INTO `files` VALUES ('578', '14.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522660626_5ac1f512504b06.64384524.jpeg', 'collection/2018/04/02/img_1522660626_5ac1f512504b06.64384524.jpeg', 'image/jpeg', '559486', '2018-04-02 16:17:06', '2018-04-02 16:17:06', '1');
INSERT INTO `files` VALUES ('579', '14.jpg', '/var/www/html/public/files/collection/2018/04/02/img_1522660683_5ac1f54b5110b5.96661630.jpeg', 'collection/2018/04/02/img_1522660683_5ac1f54b5110b5.96661630.jpeg', 'image/jpeg', '559486', '2018-04-02 16:18:03', '2018-04-02 16:18:03', '1');
INSERT INTO `files` VALUES ('580', '5ac1f7e13bb33_1522661345_20180402.png', 'files/decision/2018/04/5ac1f7e13bb33_1522661345_20180402.png', 'files/decision/2018/04/5ac1f7e13bb33_1522661345_20180402.png', 'image/png', '65981', '2018-04-02 16:29:05', '2018-04-02 16:29:05', '1');
INSERT INTO `files` VALUES ('581', '5ac339a6abbfd_1522743718_20180403.png', 'files/decision/2018/04/5ac339a6abbfd_1522743718_20180403.png', 'files/decision/2018/04/5ac339a6abbfd_1522743718_20180403.png', 'image/png', '286371', '2018-04-03 08:21:58', '2018-04-03 08:21:58', '1');
INSERT INTO `files` VALUES ('582', 'Chrysanthemum.jpg', '/var/www/html/dev.hoihuongdanvien.vn/public_html/dulich-phase2/code/public/files/collection/2018/04/04/img_1522804930_5ac428c20a4d43.04729902.jpeg', 'collection/2018/04/04/img_1522804930_5ac428c20a4d43.04729902.jpeg', 'image/jpeg', '879394', '2018-04-04 01:22:10', '2018-04-04 01:22:10', '1');
INSERT INTO `files` VALUES ('583', '5ac42bd17e172_1522805713_20180404.jpg', 'files/decision/2018/04/5ac42bd17e172_1522805713_20180404.jpg', 'files/decision/2018/04/5ac42bd17e172_1522805713_20180404.jpg', 'image/jpeg', '775702', '2018-04-04 01:35:13', '2018-04-04 01:35:13', '1');
INSERT INTO `files` VALUES ('584', '2018-03-17_11-42-05.png', '/var/www/html/dev.hoihuongdanvien.vn/public_html/dulich-phase2/code/public/files/collection/2018/04/05/img_1522894997_5ac5889515f3a7.76522798.png', 'collection/2018/04/05/img_1522894997_5ac5889515f3a7.76522798.png', 'image/png', '57803', '2018-04-05 02:23:17', '2018-04-05 02:23:17', '1');
INSERT INTO `files` VALUES ('585', 'SAM_3195.JPG', '/var/www/html/dev.hoihuongdanvien.vn/public_html/dulich-phase2/code/public/files/collection/2018/04/07/img_1523086780_5ac875bcc04472.57042813.jpeg', 'collection/2018/04/07/img_1523086780_5ac875bcc04472.57042813.jpeg', 'image/jpeg', '1382365', '2018-04-07 07:39:41', '2018-04-07 07:39:41', '1');
INSERT INTO `files` VALUES ('586', '5acb1d55cf7a5_1523260757_20180409.png', 'files/decision/2018/04/5acb1d55cf7a5_1523260757_20180409.png', 'files/decision/2018/04/5acb1d55cf7a5_1523260757_20180409.png', 'image/png', '1271', '2018-04-09 07:59:17', '2018-04-09 07:59:17', '1');
INSERT INTO `files` VALUES ('587', '5acb24f05691d_1523262704_20180409.png', 'files/decision/2018/04/5acb24f05691d_1523262704_20180409.png', 'files/decision/2018/04/5acb24f05691d_1523262704_20180409.png', 'image/png', '1271', '2018-04-09 08:31:44', '2018-04-09 08:31:44', '1');
INSERT INTO `files` VALUES ('588', 'new-field-group.jpg', '/var/www/html/dev.hoihuongdanvien.vn/public_html/dulich-phase2/code/public/files/collection/2018/04/10/img_1523331587_5acc3203193802.16881325.jpeg', 'collection/2018/04/10/img_1523331587_5acc3203193802.16881325.jpeg', 'image/jpeg', '53097', '2018-04-10 03:39:47', '2018-04-10 03:39:47', '1');

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `permissions` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'Quyền 1', '[\"password.request\",\"front_end_index\",\"front_end_clb\",\"front_end_hoivien\"]', '2018-04-21 14:33:44', '2018-04-22 15:48:47', '2018-04-22 15:48:47', 'quyen1', 'hihi');
INSERT INTO `groups` VALUES ('2', 'Quyền 2', '[\"password.request\",\"front_end_index\"]', '2018-04-21 14:35:33', '2018-04-21 15:29:49', null, 'quyen2', null);
INSERT INTO `groups` VALUES ('3', 'Hội viên bình thường', '[\"login\"]', '2018-04-21 17:14:27', '2018-04-21 17:14:27', null, 'hoivienbinhthuong', null);
INSERT INTO `groups` VALUES ('4', 'Kế toán Hội', '[\"login\",\"password.request\"]', '2018-04-21 17:15:28', '2018-04-22 13:52:21', null, 'KeToanHoi', null);
INSERT INTO `groups` VALUES ('5', 'Quản trị viên Hội', '[\"login\",\"logout\",\"register\",\"password.request\",\"password.email\",\"password.reset\",\"front_end_index\",\"front_end_gioithieu\",\"front_end_chihoi\",\"front_end_clb\",\"front_end_news\",\"front_end_news_detail\",\"front_end_hoivien\",\"front_end_redirect\",\"front_end_ajax_get_news\",\"front_end_ajax_get_member\",\"front_end_ajax_club_of_branches\",\"ajax_get_content_club_of_branch\",\"home\",\"profile_edit\",\"profile\",\"member_register_view\",\"member_register_action\",\"member_register_resend_sms_action\",\"download\",\"downloadZip\",\"member_rejected\",\"member_update\",\"member_detail\",\"admin_member\",\"admin_member_detail_view\",\"admin_member_detail_view_ajax\",\"admin_member_detail_update_view\",\"admin_member_detail_tmp_view\",\"admin_member_apiDetailUser_view\",\"admin_member_apiChangeStatus_view\",\"admin_member_create_card_view\",\"admin_member_do_create_card_view\",\"admin_member_print_excel_member_card_view\",\"admin_gen_pdf\",\"fakeSendMailApprove\",\"admin_member_list_payment_view\",\"admin_member_detail_payment_view\",\"payment_store\",\"payment_update\",\"payment_destroy\",\"payment_send_notifications\",\"admin_payment_fee_import_view\",\"admin_member_list_decision_view\",\"member_decision_do_create_card_view\",\"admin_member_update_decision_view\",\"decision\",\"admin_member_list_view\",\"list_member_print_file\",\"admin_list_member_detail_view\",\"delete_member\",\"update_member\",\"admin_representative_office_view\",\"admin_representative_office_edit\",\"admin_representative_office_delete\",\"admin_representative_office_update\",\"admin_representative_office_manager\",\"admin_representative_office_save_manager\",\"admin_employee_view\",\"admin_employee_edit\",\"admin_employee_delete\",\"admin_employee_update\",\"admin_employee_create\",\"admin_employee_doCreate\",\"users.index\",\"users.create\",\"users.store\",\"users.show\",\"users.edit\",\"users.update\",\"users.destroy\",\"introductions.index\",\"introductions.create\",\"introductions.store\",\"introductions.show\",\"introductions.edit\",\"introductions.update\",\"introductions.destroy\",\"categories.index\",\"categories.create\",\"categories.store\",\"categories.show\",\"categories.edit\",\"categories.update\",\"categories.destroy\",\"admin_introductions_detail\",\"admin_categories_sedit_detail\",\"admin_categories_deletdit_detail\",\"club_of_head_index\",\"club_of_head_create\",\"club_of_head_store\",\"club_of_head_edit\",\"club_of_head_update\",\"club_of_head_show\",\"club_of_head_delete\",\"club_of_head_manager\",\"club_of_head_ajax_save_member\",\"club_of_head_ajax_get_clb\",\"club_of_head_export_excel\",\"club_of_branch_index\",\"club_of_branch_create\",\"club_of_branch_create_store\",\"club_of_branch_edit\",\"club_of_branch_update\",\"club_of_branch_show\",\"club_of_branch_delete\",\"club_of_branches_manager\",\"club_of_branches_ajax_save_member\",\"club_of_branch_member_index\",\"club_of_branch_member_ajax_search\",\"club_of_branch_member_create\",\"club_of_branch_export_excel\",\"club_of_branch_member_delete\",\"club_of_branch_ajax_get_clb\",\"branches_manager\",\"branches_ajax_employees\",\"branches_export_excel\",\"branches_ajax_save_member\",\"branch_destroy\",\"infomation.index\",\"infomation.create\",\"infomation.store\",\"infomation.show\",\"infomation.edit\",\"infomation.update\",\"infomation.destroy\",\"branches.index\",\"branches.create\",\"branches.store\",\"branches.show\",\"branches.edit\",\"branches.update\",\"branches.destroy\",\"upload.store\",\"front_end_special_board\",\"front_end_standing_committee\",\"front_end_standing_advisory_board\",\"front_end_executive_committee\",\"front_end_news_1\",\"front_end_news_2\",\"front_end_news_3\",\"front_end_vanphongdaidien\",\"front_vechungtoi\",\"front_tamnhinvasumenh\",\"front_quychehoatdong\",\"front_quydinh\",\"front_huongdandangky\",\"front_info_traffic\",\"front_info_traffic_detail\",\"front_info_lehoi\",\"front_info_lehoi_detail\",\"front_info_diemden\",\"front_info_diemden_detail\",\"front_info_trangphuc\",\"front_info_trangphuc_detail\",\"front_info_customandpractices\",\"front_info_customandpractices_detail\"]', '2018-04-21 17:15:52', '2018-04-22 13:27:49', null, 'QuanTriVienHoi', null);
INSERT INTO `groups` VALUES ('6', 'Biên tập viên Chi hội / CLB thuộc Hội', '[\"login\"]', '2018-04-21 17:16:23', '2018-04-21 17:16:23', null, 'BTVChiHoiClbThuocHoi', null);
INSERT INTO `groups` VALUES ('7', 'Biên tập viên hội', '[\"login\"]', '2018-04-21 17:16:39', '2018-04-21 17:16:39', null, 'BienTapVienHoi', null);
INSERT INTO `groups` VALUES ('8', 'Chuyên viên thẩm định hồ sơ tại Hà Nội', '[\"login\"]', '2018-04-21 17:17:17', '2018-04-21 17:17:17', null, 'CVTDHSHN', null);
INSERT INTO `groups` VALUES ('9', 'Chuyên viên thẩm định hồ sơ tại Đà Nẵng', '[\"login\"]', '2018-04-21 17:17:54', '2018-04-21 17:17:54', null, 'CVTDHSDN', null);
INSERT INTO `groups` VALUES ('10', 'Chuyên viên thẩm định hồ sơ tại TP. HCM', '[\"login\"]', '2018-04-21 17:18:14', '2018-04-21 17:18:14', null, 'CVTDHSHCM', null);
INSERT INTO `groups` VALUES ('11', 'Lãnh đạo Ban tại Hà Nội', '[\"login\"]', '2018-04-21 17:18:29', '2018-04-21 17:18:29', null, 'LdBanHN', null);
INSERT INTO `groups` VALUES ('12', 'Lãnh đạo VPĐD tại Đà Nẵng', '[\"login\"]', '2018-04-21 17:18:56', '2018-04-21 17:18:56', null, 'LdVPDDDN', null);
INSERT INTO `groups` VALUES ('13', 'Lãnh đạo VPĐD tại TP. HCM', '[\"login\"]', '2018-04-21 17:19:14', '2018-04-21 17:19:14', null, 'LdVPDDHCM', null);
INSERT INTO `groups` VALUES ('14', 'Lãnh đạo Hội', '[\"login\"]', '2018-04-21 17:20:11', '2018-04-21 17:20:11', null, 'LanhDaoHoi', null);
INSERT INTO `groups` VALUES ('15', 'Chuyên viên cấp thẻ Hội viên', '[\"login\",\"password.request\",\"front_end_index\"]', '2018-04-21 17:20:49', '2018-04-21 17:20:49', null, 'ChuyenVienCapTheHV', null);

-- ----------------------------
-- Table structure for groupsizes
-- ----------------------------
DROP TABLE IF EXISTS `groupsizes`;
CREATE TABLE `groupsizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupSize` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of groupsizes
-- ----------------------------
INSERT INTO `groupsizes` VALUES ('1', 'Khách lẻ mai sửa', '2017-12-01 00:08:37', '2017-12-01 00:08:37', '0');
INSERT INTO `groupsizes` VALUES ('2', 'Từ 04 đến 10 khách', '2017-12-01 00:08:38', '2017-12-01 00:08:38', '1');
INSERT INTO `groupsizes` VALUES ('3', 'Từ 11 đến 20 khách', '2017-12-01 20:52:52', '2017-12-01 19:50:47', '1');
INSERT INTO `groupsizes` VALUES ('4', 'Từ 21 đến 30 khách', '2017-12-01 22:50:53', '2017-12-01 21:53:53', '1');
INSERT INTO `groupsizes` VALUES ('5', 'Trên 30 khách', '2017-12-01 20:52:52', '2017-12-01 19:50:47', '1');

-- ----------------------------
-- Table structure for helpinformation
-- ----------------------------
DROP TABLE IF EXISTS `helpinformation`;
CREATE TABLE `helpinformation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumnail_image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `main_image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `information_type_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of helpinformation
-- ----------------------------

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of jobs
-- ----------------------------

-- ----------------------------
-- Table structure for languagelevels
-- ----------------------------
DROP TABLE IF EXISTS `languagelevels`;
CREATE TABLE `languagelevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `levelName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of languagelevels
-- ----------------------------
INSERT INTO `languagelevels` VALUES ('1', 'Cao đẳng ngoại ngữ', '2017-12-01 00:08:37', '2017-12-01 00:08:37', '0');
INSERT INTO `languagelevels` VALUES ('2', 'Đại học ngoại ngữ', '2017-12-01 00:08:37', '2017-12-01 00:08:37', '1');
INSERT INTO `languagelevels` VALUES ('3', 'Trên đại học ngoại ngữ', '2017-12-01 11:27:29', '2017-12-01 13:33:34', '1');
INSERT INTO `languagelevels` VALUES ('4', 'Cao đẳng chuyên ngành khác bằng tiếng nước ngoài', '2017-12-01 22:53:54', '2017-12-01 21:56:56', '1');
INSERT INTO `languagelevels` VALUES ('5', 'Đại học chuyên ngành khác bằng tiếng nước ngoài', '2017-12-01 11:27:29', '2017-12-01 13:33:34', '1');
INSERT INTO `languagelevels` VALUES ('6', 'Trên đại học chuyên ngành khác bằng tiếng nước ngoài', '2017-12-01 22:53:54', '2017-12-01 21:56:56', '1');
INSERT INTO `languagelevels` VALUES ('7', 'Chứng chỉ ngoại ngữ do cơ quan có thẩm quyền cấp', '2017-12-01 11:27:29', '2017-12-01 13:33:34', '1');
INSERT INTO `languagelevels` VALUES ('8', 'Chứng  chỉ sử dụng thành  thạo ngoại ngữ do cơ sở đào tạo cấp', '2017-12-01 22:53:54', '2017-12-01 21:56:56', '1');

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `languageName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('1', 'Tiếng Anh mai sửa', '2017-12-01 00:08:36', '2017-12-01 00:08:36', '0', '1');
INSERT INTO `languages` VALUES ('2', 'Tiếng Nhật', '2017-12-01 00:08:36', '2017-12-01 00:08:36', '1', '2');
INSERT INTO `languages` VALUES ('3', 'Tiếng Trung', '2017-12-01 07:30:30', '2017-12-01 13:32:37', '1', '3');
INSERT INTO `languages` VALUES ('4', 'Tiếng Đức', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '4');
INSERT INTO `languages` VALUES ('5', 'Tiếng Pháp', '2017-12-01 07:30:30', '2017-12-01 13:32:37', '1', '5');
INSERT INTO `languages` VALUES ('6', 'Tiếng Tây Ban Nha', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '6');
INSERT INTO `languages` VALUES ('7', 'Tiếng Ý', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '7');
INSERT INTO `languages` VALUES ('8', 'Tiếng Nga', '2017-12-01 07:30:30', '2017-12-01 13:32:37', '1', '8');
INSERT INTO `languages` VALUES ('9', 'Tiếng Hàn Quốc', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '9');
INSERT INTO `languages` VALUES ('10', 'Tiếng khác', '2017-12-01 12:31:32', '2017-12-01 11:30:33', '1', '10');

-- ----------------------------
-- Table structure for languageskills
-- ----------------------------
DROP TABLE IF EXISTS `languageskills`;
CREATE TABLE `languageskills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `languageId` int(10) unsigned NOT NULL,
  `levelId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `languageskills_memberid_foreign` (`memberId`),
  KEY `languageskills_languageid_foreign` (`languageId`),
  KEY `languageskills_levelid_foreign` (`levelId`),
  KEY `languageskills_fileid_foreign` (`fileId`),
  CONSTRAINT `languageskills_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `languageskills_languageid_foreign` FOREIGN KEY (`languageId`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `languageskills_levelid_foreign` FOREIGN KEY (`levelId`) REFERENCES `languagelevels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `languageskills_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of languageskills
-- ----------------------------
INSERT INTO `languageskills` VALUES ('1', '1', '7', '8', null, '2018-03-13 09:53:09', '2018-03-26 23:38:11');
INSERT INTO `languageskills` VALUES ('2', '2', '1', '8', null, '2018-03-13 09:59:56', '2018-03-22 11:56:20');
INSERT INTO `languageskills` VALUES ('3', '3', '3', '7', null, '2018-03-13 10:05:04', '2018-03-13 10:05:04');
INSERT INTO `languageskills` VALUES ('4', '5', '1', '8', null, '2018-03-13 10:11:17', '2018-03-13 10:11:17');
INSERT INTO `languageskills` VALUES ('5', '7', '1', '8', null, '2018-03-13 10:14:55', '2018-03-13 10:14:55');
INSERT INTO `languageskills` VALUES ('6', '8', '6', '3', null, '2018-03-13 10:35:52', '2018-03-13 10:35:52');
INSERT INTO `languageskills` VALUES ('7', '11', '1', '8', null, '2018-03-13 14:00:48', '2018-03-13 14:00:48');
INSERT INTO `languageskills` VALUES ('8', '16', '4', '5', null, '2018-03-13 22:29:33', '2018-03-16 15:39:20');
INSERT INTO `languageskills` VALUES ('9', '17', '2', '2', null, '2018-03-14 09:37:17', '2018-03-19 08:50:33');
INSERT INTO `languageskills` VALUES ('10', '19', '8', '6', null, '2018-03-14 10:31:48', '2018-03-14 10:31:48');
INSERT INTO `languageskills` VALUES ('11', '26', '8', '5', null, '2018-03-14 11:04:03', '2018-03-22 10:54:29');
INSERT INTO `languageskills` VALUES ('12', '30', '8', '5', null, '2018-03-14 11:11:30', '2018-03-14 11:11:30');
INSERT INTO `languageskills` VALUES ('13', '39', '2', '8', null, '2018-03-15 10:53:49', '2018-03-15 10:53:49');
INSERT INTO `languageskills` VALUES ('14', '42', '7', '4', null, '2018-03-15 11:29:59', '2018-03-15 11:29:59');
INSERT INTO `languageskills` VALUES ('15', '45', '5', '7', null, '2018-03-15 11:38:59', '2018-03-15 11:38:59');
INSERT INTO `languageskills` VALUES ('16', '47', '5', '2', null, '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `languageskills` VALUES ('17', '48', '5', '2', null, '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `languageskills` VALUES ('18', '49', '4', '6', null, '2018-03-15 13:38:43', '2018-03-15 13:38:43');
INSERT INTO `languageskills` VALUES ('19', '50', '5', '5', null, '2018-03-15 13:44:37', '2018-03-15 13:44:37');
INSERT INTO `languageskills` VALUES ('21', '51', '2', '2', null, '2018-03-15 13:54:19', '2018-03-15 13:54:19');
INSERT INTO `languageskills` VALUES ('22', '52', '4', '2', null, '2018-03-15 14:30:20', '2018-03-15 14:30:20');
INSERT INTO `languageskills` VALUES ('23', '53', '2', '2', null, '2018-03-15 14:33:57', '2018-03-16 11:31:30');
INSERT INTO `languageskills` VALUES ('24', '54', '9', '7', null, '2018-03-15 14:38:02', '2018-03-15 14:38:02');
INSERT INTO `languageskills` VALUES ('25', '56', '2', '5', null, '2018-03-15 16:27:16', '2018-03-15 16:27:16');
INSERT INTO `languageskills` VALUES ('26', '58', '2', '5', null, '2018-03-15 16:28:35', '2018-03-15 16:28:35');
INSERT INTO `languageskills` VALUES ('27', '58', '2', '5', null, '2018-03-15 16:30:48', '2018-03-15 16:30:48');
INSERT INTO `languageskills` VALUES ('28', '58', '2', '5', null, '2018-03-15 16:31:06', '2018-03-15 16:31:06');
INSERT INTO `languageskills` VALUES ('29', '58', '2', '5', null, '2018-03-15 16:31:29', '2018-03-15 16:31:29');
INSERT INTO `languageskills` VALUES ('30', '61', '3', '7', null, '2018-03-16 11:00:54', '2018-03-16 11:00:54');
INSERT INTO `languageskills` VALUES ('31', '62', '3', '7', null, '2018-03-16 11:09:11', '2018-03-16 11:09:11');
INSERT INTO `languageskills` VALUES ('32', '63', '3', '7', null, '2018-03-16 11:34:15', '2018-03-16 11:34:15');
INSERT INTO `languageskills` VALUES ('33', '64', '7', '6', null, '2018-03-16 11:34:40', '2018-03-16 11:34:40');
INSERT INTO `languageskills` VALUES ('34', '65', '2', '7', null, '2018-03-16 13:32:45', '2018-03-16 13:32:45');
INSERT INTO `languageskills` VALUES ('35', '66', '2', '7', null, '2018-03-16 13:33:40', '2018-03-16 13:33:40');
INSERT INTO `languageskills` VALUES ('36', '67', '2', '7', null, '2018-03-16 13:34:19', '2018-03-16 13:34:19');
INSERT INTO `languageskills` VALUES ('37', '68', '2', '7', null, '2018-03-16 13:35:12', '2018-03-16 13:35:12');
INSERT INTO `languageskills` VALUES ('38', '69', '10', '3', null, '2018-03-16 13:39:48', '2018-03-16 13:39:48');
INSERT INTO `languageskills` VALUES ('39', '70', '2', '7', null, '2018-03-16 13:39:49', '2018-03-16 15:37:33');
INSERT INTO `languageskills` VALUES ('40', '71', '2', '7', null, '2018-03-16 13:44:56', '2018-03-16 13:44:56');
INSERT INTO `languageskills` VALUES ('41', '71', '2', '7', null, '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `languageskills` VALUES ('42', '72', '2', '7', null, '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `languageskills` VALUES ('43', '71', '2', '7', null, '2018-03-16 13:46:39', '2018-03-16 13:46:39');
INSERT INTO `languageskills` VALUES ('44', '71', '2', '7', null, '2018-03-16 13:47:45', '2018-03-16 13:47:45');
INSERT INTO `languageskills` VALUES ('45', '71', '2', '7', null, '2018-03-16 13:48:09', '2018-03-16 13:48:09');
INSERT INTO `languageskills` VALUES ('46', '72', '2', '7', null, '2018-03-16 14:28:15', '2018-03-16 14:28:15');
INSERT INTO `languageskills` VALUES ('47', '74', '2', '7', null, '2018-03-16 14:56:22', '2018-03-16 14:56:22');
INSERT INTO `languageskills` VALUES ('48', '75', '2', '7', null, '2018-03-16 15:01:23', '2018-03-16 15:01:23');
INSERT INTO `languageskills` VALUES ('49', '75', '2', '7', null, '2018-03-16 15:01:48', '2018-03-16 15:01:48');
INSERT INTO `languageskills` VALUES ('50', '76', '2', '7', null, '2018-03-16 15:18:48', '2018-03-16 15:18:48');
INSERT INTO `languageskills` VALUES ('51', '77', '2', '7', null, '2018-03-16 15:19:23', '2018-03-16 15:19:23');
INSERT INTO `languageskills` VALUES ('52', '78', '2', '7', null, '2018-03-16 15:24:01', '2018-03-16 15:24:01');
INSERT INTO `languageskills` VALUES ('53', '79', '10', '3', null, '2018-03-16 16:09:19', '2018-03-16 17:06:28');
INSERT INTO `languageskills` VALUES ('54', '80', '7', '6', null, '2018-03-16 17:37:11', '2018-03-16 17:37:11');
INSERT INTO `languageskills` VALUES ('55', '81', '1', '8', null, '2018-03-19 11:37:54', '2018-03-19 11:37:54');
INSERT INTO `languageskills` VALUES ('56', '81', '1', '8', null, '2018-03-19 11:38:39', '2018-03-19 11:38:39');
INSERT INTO `languageskills` VALUES ('57', '82', '1', '8', null, '2018-03-19 11:44:32', '2018-03-19 11:44:32');
INSERT INTO `languageskills` VALUES ('58', '83', '2', '2', null, '2018-03-19 15:42:18', '2018-03-29 08:18:39');
INSERT INTO `languageskills` VALUES ('59', '84', '5', '2', null, '2018-03-20 08:56:42', '2018-03-20 08:56:42');
INSERT INTO `languageskills` VALUES ('60', '86', '3', '8', null, '2018-03-20 10:22:59', '2018-03-20 10:24:38');
INSERT INTO `languageskills` VALUES ('61', '90', '2', '4', null, '2018-03-23 09:45:30', '2018-03-23 09:45:30');
INSERT INTO `languageskills` VALUES ('62', '92', '6', '5', null, '2018-03-23 09:58:34', '2018-03-23 09:58:34');
INSERT INTO `languageskills` VALUES ('63', '93', '2', '6', null, '2018-03-23 10:56:58', '2018-03-23 11:00:39');
INSERT INTO `languageskills` VALUES ('64', '94', '2', '7', null, '2018-03-23 15:12:15', '2018-03-23 15:12:15');
INSERT INTO `languageskills` VALUES ('65', '99', '2', '8', null, '2018-03-26 11:52:57', '2018-03-26 11:52:57');
INSERT INTO `languageskills` VALUES ('66', '100', '2', '4', null, '2018-03-26 16:14:06', '2018-03-26 16:14:06');
INSERT INTO `languageskills` VALUES ('67', '101', '2', '4', null, '2018-03-26 16:16:00', '2018-03-26 16:16:00');
INSERT INTO `languageskills` VALUES ('68', '102', '2', '4', null, '2018-03-26 16:16:53', '2018-03-26 16:16:53');
INSERT INTO `languageskills` VALUES ('69', '103', '2', '4', null, '2018-03-26 16:18:00', '2018-03-26 16:18:00');
INSERT INTO `languageskills` VALUES ('70', '104', '2', '4', null, '2018-03-26 17:03:03', '2018-03-26 17:03:03');
INSERT INTO `languageskills` VALUES ('71', '105', '2', '4', null, '2018-03-26 17:13:08', '2018-03-26 17:13:08');
INSERT INTO `languageskills` VALUES ('72', '107', '2', '4', null, '2018-03-26 17:20:39', '2018-03-26 17:20:39');
INSERT INTO `languageskills` VALUES ('73', '109', '2', '4', null, '2018-03-26 17:21:02', '2018-03-26 17:21:02');
INSERT INTO `languageskills` VALUES ('74', '110', '10', '7', null, '2018-03-26 17:22:31', '2018-03-26 17:22:31');
INSERT INTO `languageskills` VALUES ('75', '111', '9', '6', null, '2018-03-26 17:25:28', '2018-03-26 17:25:28');
INSERT INTO `languageskills` VALUES ('76', '113', '2', '5', null, '2018-03-27 08:20:51', '2018-03-27 08:20:51');
INSERT INTO `languageskills` VALUES ('77', '115', '2', '8', null, '2018-03-27 10:15:49', '2018-03-27 10:15:49');
INSERT INTO `languageskills` VALUES ('78', '116', '2', '8', null, '2018-03-27 14:00:43', '2018-03-27 14:00:43');
INSERT INTO `languageskills` VALUES ('79', '118', '2', '4', null, '2018-03-27 15:04:04', '2018-03-27 15:19:17');
INSERT INTO `languageskills` VALUES ('80', '119', '2', '4', null, '2018-03-27 15:36:31', '2018-03-27 15:36:31');
INSERT INTO `languageskills` VALUES ('81', '120', '2', '4', null, '2018-03-27 15:58:31', '2018-03-27 15:58:31');
INSERT INTO `languageskills` VALUES ('82', '121', '2', '4', null, '2018-03-27 16:00:55', '2018-03-27 16:00:55');
INSERT INTO `languageskills` VALUES ('83', '122', '2', '4', null, '2018-03-27 16:26:24', '2018-03-27 16:26:24');
INSERT INTO `languageskills` VALUES ('84', '123', '5', '8', null, '2018-03-27 16:56:59', '2018-03-28 08:33:33');
INSERT INTO `languageskills` VALUES ('85', '124', '8', '8', null, '2018-03-28 00:02:53', '2018-03-28 00:02:53');
INSERT INTO `languageskills` VALUES ('86', '125', '8', '6', null, '2018-03-28 00:06:48', '2018-03-28 00:06:48');
INSERT INTO `languageskills` VALUES ('87', '126', '2', '4', null, '2018-03-28 09:37:08', '2018-03-28 09:37:08');
INSERT INTO `languageskills` VALUES ('88', '127', '2', '4', null, '2018-03-28 09:38:41', '2018-03-28 09:38:41');
INSERT INTO `languageskills` VALUES ('89', '134', '2', '2', null, '2018-03-28 16:28:12', '2018-03-28 17:52:58');
INSERT INTO `languageskills` VALUES ('90', '135', '3', '7', null, '2018-03-28 17:28:50', '2018-03-28 17:28:50');
INSERT INTO `languageskills` VALUES ('91', '137', '8', '6', null, '2018-03-28 17:34:48', '2018-03-28 17:34:48');
INSERT INTO `languageskills` VALUES ('92', '138', '2', '4', null, '2018-03-29 09:53:54', '2018-03-29 09:53:54');
INSERT INTO `languageskills` VALUES ('93', '141', '2', '2', null, '2018-03-30 08:39:58', '2018-03-30 08:39:58');
INSERT INTO `languageskills` VALUES ('94', '145', '2', '8', null, '2018-03-30 15:50:44', '2018-03-30 15:50:44');
INSERT INTO `languageskills` VALUES ('95', '147', '2', '2', null, '2018-03-30 23:38:33', '2018-03-30 23:38:33');
INSERT INTO `languageskills` VALUES ('96', '148', '2', '2', null, '2018-03-30 23:39:40', '2018-03-30 23:39:40');
INSERT INTO `languageskills` VALUES ('97', '149', '2', '2', null, '2018-03-30 23:52:48', '2018-03-30 23:52:48');
INSERT INTO `languageskills` VALUES ('98', '157', '7', '8', null, '2018-04-02 15:22:09', '2018-04-02 15:22:09');
INSERT INTO `languageskills` VALUES ('99', '158', '7', '5', null, '2018-04-02 16:17:49', '2018-04-02 16:17:49');
INSERT INTO `languageskills` VALUES ('100', '159', '7', '5', null, '2018-04-02 16:18:05', '2018-04-02 16:18:05');
INSERT INTO `languageskills` VALUES ('101', '161', '6', '2', null, '2018-04-05 02:24:04', '2018-04-05 02:24:04');
INSERT INTO `languageskills` VALUES ('102', '162', '2', '3', null, '2018-04-07 07:40:07', '2018-04-07 07:40:07');

-- ----------------------------
-- Table structure for leader_signing_decision
-- ----------------------------
DROP TABLE IF EXISTS `leader_signing_decision`;
CREATE TABLE `leader_signing_decision` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(10) DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deletedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of leader_signing_decision
-- ----------------------------
INSERT INTO `leader_signing_decision` VALUES ('3', 'Nguyễn Văn Linh', '1', null, null, null, null);
INSERT INTO `leader_signing_decision` VALUES ('4', 'Đào Thanh Hương', '1', null, null, null, null);

-- ----------------------------
-- Table structure for majors
-- ----------------------------
DROP TABLE IF EXISTS `majors`;
CREATE TABLE `majors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `majorName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of majors
-- ----------------------------
INSERT INTO `majors` VALUES ('1', 'Giấy chứng nhận khóa nghiệp vụ hướng dẫn du lịch nội địa', '2017-12-01 00:08:36', '2017-12-01 00:08:36', '1');
INSERT INTO `majors` VALUES ('2', 'Giấy chứng nhận khóa nghiệp vụ hướng dẫn du lịch quốc tế', '2017-12-01 00:08:36', '2017-12-01 00:08:36', '1');
INSERT INTO `majors` VALUES ('3', 'Trung cấp chuyên ngành hướng dẫn du lịch', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `majors` VALUES ('4', 'Cao đẳng chuyên ngành hướng dẫn du lịch', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `majors` VALUES ('7', 'Đaị học chuyên ngành hướng dẫn du lịch', '2017-12-01 15:27:33', '2017-12-01 15:27:33', '1');
INSERT INTO `majors` VALUES ('8', 'Đạt yêu cầu kiểm tra nghiệp vụ hướng dẫn viên du lịch tại điểm', '2017-12-16 09:26:27', '2017-12-16 12:27:34', '1');

-- ----------------------------
-- Table structure for majorskillfiles
-- ----------------------------
DROP TABLE IF EXISTS `majorskillfiles`;
CREATE TABLE `majorskillfiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `majorskillfiles_memberid_foreign` (`memberId`),
  KEY `majorskillfiles_fileid_foreign` (`fileId`),
  CONSTRAINT `majorskillfiles_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `majorskillfiles_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of majorskillfiles
-- ----------------------------

-- ----------------------------
-- Table structure for majorskills
-- ----------------------------
DROP TABLE IF EXISTS `majorskills`;
CREATE TABLE `majorskills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `majorId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `majorskills_memberid_foreign` (`memberId`),
  KEY `majorskills_majorid_foreign` (`majorId`),
  CONSTRAINT `majorskills_majorid_foreign` FOREIGN KEY (`majorId`) REFERENCES `majors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `majorskills_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of majorskills
-- ----------------------------
INSERT INTO `majorskills` VALUES ('1', '1', '3', '2018-03-13 09:53:09', '2018-03-26 23:38:11');
INSERT INTO `majorskills` VALUES ('2', '2', '3', '2018-03-13 09:59:56', '2018-03-22 11:56:20');
INSERT INTO `majorskills` VALUES ('3', '3', '7', '2018-03-13 10:05:04', '2018-03-13 10:05:04');
INSERT INTO `majorskills` VALUES ('4', '4', '1', '2018-03-13 10:10:03', '2018-03-13 10:10:03');
INSERT INTO `majorskills` VALUES ('5', '5', '2', '2018-03-13 10:11:17', '2018-03-13 10:11:17');
INSERT INTO `majorskills` VALUES ('6', '6', '7', '2018-03-13 10:11:40', '2018-03-13 10:11:40');
INSERT INTO `majorskills` VALUES ('7', '7', '3', '2018-03-13 10:14:55', '2018-03-13 10:14:55');
INSERT INTO `majorskills` VALUES ('8', '8', '4', '2018-03-13 10:35:52', '2018-03-13 10:35:52');
INSERT INTO `majorskills` VALUES ('9', '9', '2', '2018-03-13 11:42:24', '2018-03-13 11:42:24');
INSERT INTO `majorskills` VALUES ('10', '10', '8', '2018-03-13 13:57:24', '2018-03-13 13:57:24');
INSERT INTO `majorskills` VALUES ('11', '11', '8', '2018-03-13 14:00:48', '2018-03-13 14:00:48');
INSERT INTO `majorskills` VALUES ('12', '12', '7', '2018-03-13 14:13:06', '2018-03-13 14:13:06');
INSERT INTO `majorskills` VALUES ('13', '13', '7', '2018-03-13 14:16:53', '2018-03-13 14:16:53');
INSERT INTO `majorskills` VALUES ('14', '14', '7', '2018-03-13 14:20:27', '2018-03-13 14:20:27');
INSERT INTO `majorskills` VALUES ('15', '15', '8', '2018-03-13 14:22:15', '2018-03-13 14:22:15');
INSERT INTO `majorskills` VALUES ('16', '16', '4', '2018-03-13 22:29:33', '2018-03-16 15:39:20');
INSERT INTO `majorskills` VALUES ('17', '17', '7', '2018-03-14 09:37:17', '2018-03-19 08:50:33');
INSERT INTO `majorskills` VALUES ('18', '18', '7', '2018-03-14 09:42:54', '2018-03-14 09:42:54');
INSERT INTO `majorskills` VALUES ('19', '19', '4', '2018-03-14 10:31:48', '2018-03-23 10:06:04');
INSERT INTO `majorskills` VALUES ('20', '20', '8', '2018-03-14 10:34:11', '2018-03-14 10:34:11');
INSERT INTO `majorskills` VALUES ('21', '21', '7', '2018-03-14 10:37:23', '2018-03-23 16:21:51');
INSERT INTO `majorskills` VALUES ('22', '22', '7', '2018-03-14 10:40:40', '2018-03-14 10:40:40');
INSERT INTO `majorskills` VALUES ('23', '23', '7', '2018-03-14 10:58:44', '2018-03-14 10:58:44');
INSERT INTO `majorskills` VALUES ('24', '24', '7', '2018-03-14 11:01:34', '2018-03-14 11:01:34');
INSERT INTO `majorskills` VALUES ('25', '25', '7', '2018-03-14 11:03:19', '2018-03-14 11:03:19');
INSERT INTO `majorskills` VALUES ('26', '26', '7', '2018-03-14 11:04:03', '2018-03-22 10:54:29');
INSERT INTO `majorskills` VALUES ('27', '27', '7', '2018-03-14 11:05:08', '2018-03-14 11:05:08');
INSERT INTO `majorskills` VALUES ('28', '28', '4', '2018-03-14 11:08:33', '2018-03-14 11:08:33');
INSERT INTO `majorskills` VALUES ('29', '29', '1', '2018-03-14 11:10:14', '2018-03-14 11:10:14');
INSERT INTO `majorskills` VALUES ('30', '30', '7', '2018-03-14 11:11:30', '2018-03-14 11:11:30');
INSERT INTO `majorskills` VALUES ('31', '31', '2', '2018-03-14 11:12:00', '2018-03-16 16:28:16');
INSERT INTO `majorskills` VALUES ('32', '32', '7', '2018-03-14 11:15:27', '2018-03-14 11:15:27');
INSERT INTO `majorskills` VALUES ('33', '33', '7', '2018-03-14 11:17:53', '2018-03-14 11:17:53');
INSERT INTO `majorskills` VALUES ('34', '34', '2', '2018-03-14 11:20:04', '2018-03-14 11:20:04');
INSERT INTO `majorskills` VALUES ('35', '35', '4', '2018-03-14 11:22:42', '2018-03-14 11:22:42');
INSERT INTO `majorskills` VALUES ('36', '36', '7', '2018-03-14 11:25:18', '2018-03-14 11:25:18');
INSERT INTO `majorskills` VALUES ('37', '37', '4', '2018-03-14 17:29:23', '2018-03-14 17:29:23');
INSERT INTO `majorskills` VALUES ('38', '38', '7', '2018-03-14 17:30:16', '2018-03-14 17:30:16');
INSERT INTO `majorskills` VALUES ('39', '38', '7', '2018-03-14 17:31:14', '2018-03-14 17:31:14');
INSERT INTO `majorskills` VALUES ('40', '39', '4', '2018-03-15 10:53:49', '2018-03-15 10:53:49');
INSERT INTO `majorskills` VALUES ('41', '40', '7', '2018-03-15 11:00:38', '2018-03-15 11:00:38');
INSERT INTO `majorskills` VALUES ('44', '42', '4', '2018-03-15 11:29:59', '2018-03-15 11:29:59');
INSERT INTO `majorskills` VALUES ('45', '43', '7', '2018-03-15 11:31:40', '2018-03-15 11:31:40');
INSERT INTO `majorskills` VALUES ('46', '41', '4', '2018-03-15 11:32:31', '2018-03-15 11:32:31');
INSERT INTO `majorskills` VALUES ('47', '44', '8', '2018-03-15 11:36:10', '2018-03-15 11:36:10');
INSERT INTO `majorskills` VALUES ('48', '45', '3', '2018-03-15 11:38:59', '2018-03-15 11:38:59');
INSERT INTO `majorskills` VALUES ('49', '46', '1', '2018-03-15 11:39:11', '2018-03-15 11:39:11');
INSERT INTO `majorskills` VALUES ('50', '47', '4', '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `majorskills` VALUES ('51', '48', '8', '2018-03-15 11:52:59', '2018-03-15 11:52:59');
INSERT INTO `majorskills` VALUES ('52', '49', '1', '2018-03-15 13:38:43', '2018-03-15 13:38:43');
INSERT INTO `majorskills` VALUES ('53', '50', '4', '2018-03-15 13:44:37', '2018-03-15 13:44:37');
INSERT INTO `majorskills` VALUES ('55', '51', '8', '2018-03-15 13:54:19', '2018-03-15 13:54:19');
INSERT INTO `majorskills` VALUES ('56', '52', '8', '2018-03-15 14:30:20', '2018-03-15 14:30:20');
INSERT INTO `majorskills` VALUES ('57', '53', '7', '2018-03-15 14:33:57', '2018-03-16 11:31:30');
INSERT INTO `majorskills` VALUES ('58', '54', '4', '2018-03-15 14:38:02', '2018-03-15 14:38:02');
INSERT INTO `majorskills` VALUES ('59', '55', '4', '2018-03-15 16:18:01', '2018-03-15 16:18:01');
INSERT INTO `majorskills` VALUES ('60', '55', '4', '2018-03-15 16:22:22', '2018-03-15 16:22:22');
INSERT INTO `majorskills` VALUES ('61', '56', '4', '2018-03-15 16:27:16', '2018-03-15 16:27:16');
INSERT INTO `majorskills` VALUES ('62', '57', '7', '2018-03-15 16:27:39', '2018-03-15 16:27:39');
INSERT INTO `majorskills` VALUES ('63', '58', '4', '2018-03-15 16:28:35', '2018-03-15 16:28:35');
INSERT INTO `majorskills` VALUES ('64', '58', '4', '2018-03-15 16:30:48', '2018-03-15 16:30:48');
INSERT INTO `majorskills` VALUES ('65', '58', '4', '2018-03-15 16:31:06', '2018-03-15 16:31:06');
INSERT INTO `majorskills` VALUES ('66', '58', '4', '2018-03-15 16:31:29', '2018-03-15 16:31:29');
INSERT INTO `majorskills` VALUES ('67', '59', '4', '2018-03-16 09:29:02', '2018-03-30 16:24:05');
INSERT INTO `majorskills` VALUES ('68', '60', '7', '2018-03-16 09:36:06', '2018-03-16 09:36:06');
INSERT INTO `majorskills` VALUES ('69', '61', '4', '2018-03-16 11:00:54', '2018-03-16 11:00:54');
INSERT INTO `majorskills` VALUES ('70', '62', '4', '2018-03-16 11:09:11', '2018-03-16 11:09:11');
INSERT INTO `majorskills` VALUES ('71', '63', '4', '2018-03-16 11:34:15', '2018-03-16 11:34:15');
INSERT INTO `majorskills` VALUES ('72', '64', '2', '2018-03-16 11:34:40', '2018-03-16 11:34:40');
INSERT INTO `majorskills` VALUES ('73', '65', '7', '2018-03-16 13:32:45', '2018-03-16 13:32:45');
INSERT INTO `majorskills` VALUES ('74', '66', '7', '2018-03-16 13:33:40', '2018-03-16 13:33:40');
INSERT INTO `majorskills` VALUES ('75', '67', '7', '2018-03-16 13:34:19', '2018-03-16 13:34:19');
INSERT INTO `majorskills` VALUES ('76', '68', '7', '2018-03-16 13:35:12', '2018-03-16 13:35:12');
INSERT INTO `majorskills` VALUES ('77', '69', '4', '2018-03-16 13:39:48', '2018-03-16 13:39:48');
INSERT INTO `majorskills` VALUES ('78', '70', '7', '2018-03-16 13:39:49', '2018-03-16 15:37:33');
INSERT INTO `majorskills` VALUES ('79', '71', '8', '2018-03-16 13:44:56', '2018-03-16 13:44:56');
INSERT INTO `majorskills` VALUES ('80', '71', '8', '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `majorskills` VALUES ('81', '72', '7', '2018-03-16 13:46:11', '2018-03-16 13:46:11');
INSERT INTO `majorskills` VALUES ('82', '71', '8', '2018-03-16 13:46:39', '2018-03-16 13:46:39');
INSERT INTO `majorskills` VALUES ('83', '71', '8', '2018-03-16 13:47:45', '2018-03-16 13:47:45');
INSERT INTO `majorskills` VALUES ('84', '71', '8', '2018-03-16 13:48:09', '2018-03-16 13:48:09');
INSERT INTO `majorskills` VALUES ('85', '73', '8', '2018-03-16 14:25:47', '2018-03-16 14:25:47');
INSERT INTO `majorskills` VALUES ('86', '73', '8', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `majorskills` VALUES ('87', '73', '8', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `majorskills` VALUES ('88', '73', '8', '2018-03-16 14:25:48', '2018-03-16 14:25:48');
INSERT INTO `majorskills` VALUES ('89', '73', '8', '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `majorskills` VALUES ('90', '73', '8', '2018-03-16 14:25:49', '2018-03-16 14:25:49');
INSERT INTO `majorskills` VALUES ('91', '73', '8', '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `majorskills` VALUES ('92', '73', '8', '2018-03-16 14:25:50', '2018-03-16 14:25:50');
INSERT INTO `majorskills` VALUES ('93', '73', '8', '2018-03-16 14:26:41', '2018-03-16 14:26:41');
INSERT INTO `majorskills` VALUES ('94', '73', '8', '2018-03-16 14:27:04', '2018-03-16 14:27:04');
INSERT INTO `majorskills` VALUES ('95', '72', '7', '2018-03-16 14:28:15', '2018-03-16 14:28:15');
INSERT INTO `majorskills` VALUES ('96', '74', '7', '2018-03-16 14:56:22', '2018-03-16 14:56:22');
INSERT INTO `majorskills` VALUES ('97', '75', '7', '2018-03-16 15:01:23', '2018-03-16 15:01:23');
INSERT INTO `majorskills` VALUES ('98', '75', '7', '2018-03-16 15:01:48', '2018-03-16 15:01:48');
INSERT INTO `majorskills` VALUES ('99', '76', '7', '2018-03-16 15:18:48', '2018-03-16 15:18:48');
INSERT INTO `majorskills` VALUES ('100', '77', '7', '2018-03-16 15:19:23', '2018-03-16 15:19:23');
INSERT INTO `majorskills` VALUES ('101', '78', '7', '2018-03-16 15:24:01', '2018-03-16 15:24:01');
INSERT INTO `majorskills` VALUES ('102', '79', '3', '2018-03-16 16:09:19', '2018-03-16 17:06:28');
INSERT INTO `majorskills` VALUES ('103', '80', '7', '2018-03-16 17:37:11', '2018-03-16 17:37:11');
INSERT INTO `majorskills` VALUES ('104', '81', '2', '2018-03-19 11:37:54', '2018-03-19 11:37:54');
INSERT INTO `majorskills` VALUES ('105', '81', '2', '2018-03-19 11:38:39', '2018-03-19 11:38:39');
INSERT INTO `majorskills` VALUES ('106', '82', '2', '2018-03-19 11:44:32', '2018-03-19 11:44:32');
INSERT INTO `majorskills` VALUES ('107', '83', '8', '2018-03-19 15:42:18', '2018-03-29 08:18:39');
INSERT INTO `majorskills` VALUES ('108', '84', '7', '2018-03-20 08:56:42', '2018-03-20 08:56:42');
INSERT INTO `majorskills` VALUES ('109', '85', '8', '2018-03-20 08:58:48', '2018-03-26 18:55:12');
INSERT INTO `majorskills` VALUES ('110', '86', '8', '2018-03-20 10:22:59', '2018-03-20 10:24:38');
INSERT INTO `majorskills` VALUES ('111', '87', '4', '2018-03-20 11:08:11', '2018-03-22 14:06:52');
INSERT INTO `majorskills` VALUES ('112', '88', '4', '2018-03-20 11:39:12', '2018-03-20 11:39:12');
INSERT INTO `majorskills` VALUES ('113', '89', '4', '2018-03-21 10:12:52', '2018-03-21 10:12:52');
INSERT INTO `majorskills` VALUES ('114', '90', '4', '2018-03-23 09:45:30', '2018-03-23 09:45:30');
INSERT INTO `majorskills` VALUES ('115', '91', '7', '2018-03-23 09:53:29', '2018-03-23 09:53:29');
INSERT INTO `majorskills` VALUES ('116', '92', '7', '2018-03-23 09:58:34', '2018-03-23 09:58:34');
INSERT INTO `majorskills` VALUES ('117', '93', '4', '2018-03-23 10:56:58', '2018-03-23 11:00:39');
INSERT INTO `majorskills` VALUES ('118', '94', '7', '2018-03-23 15:12:15', '2018-03-23 15:12:15');
INSERT INTO `majorskills` VALUES ('119', '95', '4', '2018-03-26 08:38:00', '2018-03-26 08:38:00');
INSERT INTO `majorskills` VALUES ('120', '96', '4', '2018-03-26 08:43:13', '2018-03-26 08:43:13');
INSERT INTO `majorskills` VALUES ('121', '97', '4', '2018-03-26 09:17:19', '2018-03-26 09:17:19');
INSERT INTO `majorskills` VALUES ('122', '98', '7', '2018-03-26 09:20:07', '2018-03-26 09:20:07');
INSERT INTO `majorskills` VALUES ('123', '99', '7', '2018-03-26 11:52:57', '2018-03-26 11:52:57');
INSERT INTO `majorskills` VALUES ('124', '100', '7', '2018-03-26 16:14:06', '2018-03-26 16:14:06');
INSERT INTO `majorskills` VALUES ('125', '101', '7', '2018-03-26 16:16:00', '2018-03-26 16:16:00');
INSERT INTO `majorskills` VALUES ('126', '102', '7', '2018-03-26 16:16:53', '2018-03-26 16:16:53');
INSERT INTO `majorskills` VALUES ('127', '103', '7', '2018-03-26 16:18:00', '2018-03-26 16:18:00');
INSERT INTO `majorskills` VALUES ('128', '104', '3', '2018-03-26 17:03:03', '2018-03-26 17:03:03');
INSERT INTO `majorskills` VALUES ('129', '105', '3', '2018-03-26 17:13:08', '2018-03-26 17:13:08');
INSERT INTO `majorskills` VALUES ('130', '106', '7', '2018-03-26 17:19:54', '2018-03-26 17:19:54');
INSERT INTO `majorskills` VALUES ('131', '107', '4', '2018-03-26 17:20:39', '2018-03-26 17:20:39');
INSERT INTO `majorskills` VALUES ('132', '108', '7', '2018-03-26 17:20:58', '2018-03-26 17:20:58');
INSERT INTO `majorskills` VALUES ('133', '109', '4', '2018-03-26 17:21:02', '2018-03-26 17:21:02');
INSERT INTO `majorskills` VALUES ('134', '110', '4', '2018-03-26 17:22:31', '2018-03-26 17:22:31');
INSERT INTO `majorskills` VALUES ('135', '111', '4', '2018-03-26 17:25:28', '2018-03-26 17:25:28');
INSERT INTO `majorskills` VALUES ('136', '112', '4', '2018-03-26 17:28:20', '2018-03-26 17:28:20');
INSERT INTO `majorskills` VALUES ('137', '113', '4', '2018-03-27 08:20:51', '2018-03-27 08:20:51');
INSERT INTO `majorskills` VALUES ('138', '114', '4', '2018-03-27 10:05:59', '2018-03-27 10:05:59');
INSERT INTO `majorskills` VALUES ('139', '115', '4', '2018-03-27 10:15:49', '2018-03-27 10:15:49');
INSERT INTO `majorskills` VALUES ('140', '116', '7', '2018-03-27 14:00:43', '2018-03-27 14:00:43');
INSERT INTO `majorskills` VALUES ('141', '117', '4', '2018-03-27 14:28:04', '2018-03-27 14:42:50');
INSERT INTO `majorskills` VALUES ('142', '118', '8', '2018-03-27 15:04:04', '2018-03-27 15:19:17');
INSERT INTO `majorskills` VALUES ('143', '119', '8', '2018-03-27 15:36:31', '2018-03-27 15:36:31');
INSERT INTO `majorskills` VALUES ('144', '120', '8', '2018-03-27 15:58:31', '2018-03-27 15:58:31');
INSERT INTO `majorskills` VALUES ('145', '121', '8', '2018-03-27 16:00:55', '2018-03-27 16:00:55');
INSERT INTO `majorskills` VALUES ('146', '122', '8', '2018-03-27 16:26:24', '2018-03-27 16:26:24');
INSERT INTO `majorskills` VALUES ('147', '123', '4', '2018-03-27 16:56:59', '2018-03-28 08:33:33');
INSERT INTO `majorskills` VALUES ('148', '124', '7', '2018-03-28 00:02:53', '2018-03-28 00:02:53');
INSERT INTO `majorskills` VALUES ('149', '125', '7', '2018-03-28 00:06:48', '2018-03-28 00:06:48');
INSERT INTO `majorskills` VALUES ('150', '126', '4', '2018-03-28 09:37:08', '2018-03-28 09:37:08');
INSERT INTO `majorskills` VALUES ('151', '127', '4', '2018-03-28 09:38:41', '2018-03-28 09:38:41');
INSERT INTO `majorskills` VALUES ('152', '128', '7', '2018-03-28 09:42:45', '2018-03-28 09:42:45');
INSERT INTO `majorskills` VALUES ('153', '129', '7', '2018-03-28 10:17:52', '2018-03-28 10:22:15');
INSERT INTO `majorskills` VALUES ('154', '130', '4', '2018-03-28 10:29:01', '2018-03-28 10:43:25');
INSERT INTO `majorskills` VALUES ('155', '131', '4', '2018-03-28 15:12:11', '2018-03-28 15:12:11');
INSERT INTO `majorskills` VALUES ('156', '132', '4', '2018-03-28 16:02:10', '2018-03-28 16:09:46');
INSERT INTO `majorskills` VALUES ('157', '133', '4', '2018-03-28 16:13:45', '2018-03-28 16:13:45');
INSERT INTO `majorskills` VALUES ('158', '134', '4', '2018-03-28 16:28:12', '2018-03-28 17:52:58');
INSERT INTO `majorskills` VALUES ('159', '135', '4', '2018-03-28 17:28:50', '2018-03-28 17:28:50');
INSERT INTO `majorskills` VALUES ('160', '136', '4', '2018-03-28 17:33:04', '2018-03-28 17:33:04');
INSERT INTO `majorskills` VALUES ('161', '137', '4', '2018-03-28 17:34:48', '2018-03-28 17:34:48');
INSERT INTO `majorskills` VALUES ('162', '138', '7', '2018-03-29 09:53:54', '2018-03-29 09:53:54');
INSERT INTO `majorskills` VALUES ('163', '139', '4', '2018-03-29 10:10:07', '2018-03-29 10:10:07');
INSERT INTO `majorskills` VALUES ('164', '140', '4', '2018-03-29 10:13:19', '2018-03-29 10:13:19');
INSERT INTO `majorskills` VALUES ('165', '141', '7', '2018-03-30 08:39:58', '2018-03-30 08:39:58');
INSERT INTO `majorskills` VALUES ('166', '142', '7', '2018-03-30 10:56:43', '2018-03-30 10:56:43');
INSERT INTO `majorskills` VALUES ('167', '143', '4', '2018-03-30 10:59:39', '2018-03-30 10:59:39');
INSERT INTO `majorskills` VALUES ('168', '144', '4', '2018-03-30 14:12:02', '2018-03-30 14:12:02');
INSERT INTO `majorskills` VALUES ('169', '145', '4', '2018-03-30 15:50:44', '2018-03-30 15:50:44');
INSERT INTO `majorskills` VALUES ('170', '146', '4', '2018-03-30 16:47:34', '2018-03-30 16:47:34');
INSERT INTO `majorskills` VALUES ('171', '147', '4', '2018-03-30 23:38:33', '2018-03-30 23:38:33');
INSERT INTO `majorskills` VALUES ('172', '148', '4', '2018-03-30 23:39:40', '2018-03-30 23:39:40');
INSERT INTO `majorskills` VALUES ('173', '149', '4', '2018-03-30 23:52:48', '2018-03-30 23:52:48');
INSERT INTO `majorskills` VALUES ('174', '150', '4', '2018-04-02 08:58:00', '2018-04-02 08:58:00');
INSERT INTO `majorskills` VALUES ('175', '151', '4', '2018-04-02 09:59:33', '2018-04-02 09:59:33');
INSERT INTO `majorskills` VALUES ('176', '152', '4', '2018-04-02 14:13:53', '2018-04-02 14:13:53');
INSERT INTO `majorskills` VALUES ('177', '153', '4', '2018-04-02 14:15:20', '2018-04-02 14:15:20');
INSERT INTO `majorskills` VALUES ('178', '154', '4', '2018-04-02 14:26:32', '2018-04-02 14:26:32');
INSERT INTO `majorskills` VALUES ('179', '155', '4', '2018-04-02 15:03:40', '2018-04-02 15:03:40');
INSERT INTO `majorskills` VALUES ('180', '156', '4', '2018-04-02 15:04:58', '2018-04-02 15:04:58');
INSERT INTO `majorskills` VALUES ('181', '157', '4', '2018-04-02 15:22:09', '2018-04-02 15:22:09');
INSERT INTO `majorskills` VALUES ('182', '158', '4', '2018-04-02 16:17:49', '2018-04-02 16:17:49');
INSERT INTO `majorskills` VALUES ('183', '159', '4', '2018-04-02 16:18:05', '2018-04-02 16:18:05');
INSERT INTO `majorskills` VALUES ('184', '160', '8', '2018-04-04 01:23:17', '2018-04-04 01:23:17');
INSERT INTO `majorskills` VALUES ('185', '161', '4', '2018-04-05 02:24:04', '2018-04-05 02:24:04');
INSERT INTO `majorskills` VALUES ('186', '162', '8', '2018-04-07 07:40:07', '2018-04-07 07:40:07');

-- ----------------------------
-- Table structure for memberavatars
-- ----------------------------
DROP TABLE IF EXISTS `memberavatars`;
CREATE TABLE `memberavatars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `fileId` int(10) unsigned NOT NULL,
  `current` tinyint(4) NOT NULL DEFAULT '0',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `memberavatars_memberid_foreign` (`memberId`),
  KEY `memberavatars_fileid_foreign` (`fileId`),
  CONSTRAINT `memberavatars_fileid_foreign` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `memberavatars_memberid_foreign` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of memberavatars
-- ----------------------------

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullName` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profileImg` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `touristGuideCode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `cmtCccd` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateIssued` datetime DEFAULT NULL,
  `issuedBy` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanentAddress` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `firstMobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondMobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstEmail` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondEmail` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeOfTravelGuide` tinyint(4) DEFAULT NULL,
  `typeOfPlace` tinyint(4) DEFAULT NULL,
  `experienceYear` int(11) DEFAULT NULL,
  `experienceLevel` tinyint(4) DEFAULT NULL,
  `otherSkills` text COLLATE utf8mb4_unicode_ci,
  `otherInformation` text COLLATE utf8mb4_unicode_ci,
  `touristGuideLevel` text COLLATE utf8mb4_unicode_ci,
  `achievements` text COLLATE utf8mb4_unicode_ci,
  `emailToken` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phoneToken` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emailVerified` tinyint(4) NOT NULL DEFAULT '0',
  `phoneVerified` tinyint(4) NOT NULL DEFAULT '0',
  `acceptTermsAndPolicies` tinyint(4) NOT NULL DEFAULT '0',
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `province_code` tinyint(4) NOT NULL,
  `is_verified` tinyint(4) DEFAULT NULL,
  `is_fee` int(4) DEFAULT NULL,
  `is_signed` int(4) DEFAULT NULL,
  `member_type` int(4) DEFAULT NULL,
  `member_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_qr_id` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_from` datetime DEFAULT NULL,
  `guideLanguage` tinyint(4) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `verified_by` int(11) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `inboundOutbound` tinyint(4) DEFAULT NULL,
  `deleteAt` timestamp NULL DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `file_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `initial_stock` tinyint(11) DEFAULT NULL,
  `province` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('1', 'Chi Ha binh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522082242_5ab921c26f4d01.91596632.jpeg', '1987-06-12 00:00:00', '1', '156156156', '2018-04-04 00:00:00', '112098866', '2018-02-21 00:00:00', 'CA TP Hà Nội', ' K39/2/5 Trần Cao Vân, quận Thanh Khê, thành phố Đà Nẵng', 'Địa chỉ liên hệ', '0978429296', null, 'maint18@viettel.com.vn', null, '1', '1', '2018', '31', 'các kỹ năng khác bình', 'Thông tin khác bình', 'Hạng hướng dẫn viên bình', 'Thành tích bình', 'O5Npx9Ms', '415125', '1', '1', '1', '2018-03-13 09:53:23', '2018-03-26 23:38:11', '13', '1', null, '1', null, '3', '100003', '155aaa4762e6ce6', '2018-03-15 10:13:54', '4', '2', '43', '2018-03-14 10:26:59', '2018-03-12 04:34:50', '1', null, null, 'C00031', null, '7');
INSERT INTO `members` VALUES ('2', 'Nguyễn Thị Mai - update profile', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/19/img_1521423836_5aaf15dc46dd99.83793664.png', '1987-06-12 00:00:00', '2', '157157157', '2018-04-04 00:00:00', '112098866', '2018-02-23 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '1', '1', '2015', '31', null, null, null, null, 'Eh2BqgzR', '290805', '1', '1', '1', '2018-03-13 10:00:14', '2018-03-28 11:41:50', '10', '1', null, '1', null, '3', null, null, null, '1', '17', '43', '2018-03-13 07:17:28', '2018-03-13 06:36:31', '2', null, null, 'C00010', null, '7');
INSERT INTO `members` VALUES ('3', 'Ngocngocngoc', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910196_5aa73f74c77975.74480818.png', '2008-05-06 00:00:00', '1', '111111119', '2018-04-04 00:00:00', '111111119', '2018-02-09 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '1', '1', '2014', '13', null, null, null, null, 'ZgOKvkOw', '040461', '1', '1', '1', '2018-03-13 10:05:23', '2018-03-28 15:43:03', '13', '49', null, '1', null, '3', '100002', '155aa7868902d5a', '2018-03-13 08:06:33', '4', '44', '43', '2018-03-13 03:45:55', '2018-03-13 03:44:34', '1', null, '0', 'C00003', null, '1');
INSERT INTO `members` VALUES ('4', 'abc2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910601_5aa741091ede56.60651325.png', '2009-02-06 00:00:00', '1', '111111118', '2018-04-05 00:00:00', '111111118', '2018-02-08 00:00:00', 'HN', 'sdfsdf', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '2', null, null, null, null, 'yJiT0SRy', '537477', '1', '1', '1', '2018-03-13 10:10:18', '2018-04-03 09:07:17', '13', '2', null, '1', null, '3', '200001', '155aa786974b440', '2018-03-13 08:06:47', '4', '53', '52', '2018-03-13 03:29:57', '2018-03-13 03:22:59', null, null, null, 'C00002', null, '2');
INSERT INTO `members` VALUES ('5', 'Nguyễn Thị Mai 12032018_nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910071_5aa73ef73e9a37.58212534.jpeg', '1987-06-12 00:00:00', '2', '158158158', '2018-04-04 00:00:00', '112098866', '2018-02-14 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.comend', '2', '1', '2015', '31', null, null, null, null, 'cDyZVslI', '377231', '1', '1', '1', '2018-03-13 10:11:38', '2018-04-03 09:17:23', '13', '1', null, '1', null, '3', '200023', '155ab1d6a90099a', '2018-03-21 03:51:05', '6', '44', '43', '2018-03-13 07:14:23', '2018-03-13 06:36:38', null, null, '1', 'C00009', null, '7');
INSERT INTO `members` VALUES ('6', 'abc3', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910634_5aa7412a0c2a93.69600324.png', '2012-01-27 00:00:00', '1', '111111117', '2018-04-04 00:00:00', '111111117', '2018-02-14 00:00:00', 'HN', 'sadfsdf', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '7', null, null, null, null, 'wjE37fzM', '337336', '1', '1', '1', '2018-03-13 10:11:58', '2018-03-25 00:17:07', '3', '2', null, '1', null, '3', '200003', '155aa786a3ca16e', '2018-03-13 08:06:59', '9', '53', '52', '2018-03-13 03:29:20', '2018-03-13 03:22:52', null, null, null, 'C00001', null, '2');
INSERT INTO `members` VALUES ('7', 'Nguyễn Thị Mai 13032018_tai diem', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520910754_5aa741a2063095.01134530.jpeg', '1987-06-12 00:00:00', '2', '159159159', null, '112098866', '2017-04-04 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', null, 'maint18@viettel.com.vn', null, '3', '1', '2015', '5', null, null, null, null, 'itgNoKuH', '533886', '1', '1', '1', '2018-03-13 10:15:35', '2018-03-30 10:20:21', '13', '1', null, '1', null, '3', '300009', '155abcb967b8a8d', '2018-03-29 10:01:11', '1', '17', '43', '2018-03-28 03:12:23', '2018-03-13 06:36:46', null, null, null, 'C00066', null, '7');
INSERT INTO `members` VALUES ('8', 'Nguyễn Thị mai 13032018_test ho so', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520912102_5aa746e6aa7ad6.23260536.jpeg', '2008-03-13 00:00:00', '2', '125125125', '2018-04-05 00:00:00', 'sdfasdfasdf', '2018-02-08 00:00:00', 'sdfasdf', 'ádfasdf', 'ádfasdfa', '0978429296', null, 'maint18@viettel.com.vn', null, '3', '1', '2018', '1', null, null, null, null, 'rZC02T4o', '918329', '1', '1', '1', '2018-03-13 10:36:30', '2018-03-28 15:43:03', '13', '1', null, '1', null, '3', '300002', '155aa786a995b2e', '2018-03-13 08:07:05', '1', '44', '43', '2018-03-13 07:13:56', '2018-03-13 06:36:53', '1', null, null, 'C00007', null, '2');
INSERT INTO `members` VALUES ('9', 'abc4', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520916141_5aa756ad53b774.87680943.png', '2008-01-13 00:00:00', '2', '111111114', '2018-04-12 00:00:00', '111111114', '2018-02-08 00:00:00', 'hn', 'dsfdsf', 'sdfdsf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2015', '5', null, null, null, null, 'AuWSvo5q', '678269', '1', '1', '1', '2018-03-13 11:42:48', '2018-04-03 10:08:51', '13', '1', null, '1', null, '3', '200038', '155abe0d6392cd2', '2018-03-30 10:11:47', '2', '60', '43', '2018-03-13 07:11:06', '2018-03-13 06:37:01', null, null, null, 'C00005', null, '1');
INSERT INTO `members` VALUES ('10', 'abc5', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520924242_5aa776520efc94.72533112.png', '2012-06-27 00:00:00', '1', '111111113', '2018-04-19 00:00:00', '111111113', '2018-02-22 00:00:00', 'HN', 'sdfsdf', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2015', '8', null, null, null, null, 'vgLq01YC', '750686', '1', '1', '1', '2018-03-13 14:00:42', '2018-03-29 17:23:06', '13', '1', null, '1', null, '3', '200030', '155abca5116f601', '2018-03-29 08:34:25', '9', '60', '59', '2018-03-13 07:10:52', '2018-03-13 07:10:09', null, null, null, 'C00004', null, '1');
INSERT INTO `members` VALUES ('11', 'Nguyễn Thị Mai 1303 _td', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520924322_5aa776a27461f1.96027366.jpeg', '2009-03-13 00:00:00', '1', '145145145', '2018-04-05 00:00:00', '112098866', '2018-02-01 00:00:00', 'CA TP Hà nội', 'sdfasf', 'ádfasf', '0978429296', null, 'maint18@viettel.com.vn', null, '2', '1', '2015', '9', null, null, null, null, 'Rh7JgSKF', '053259', '1', '1', '1', '2018-03-13 14:01:07', '2018-03-14 16:41:26', '5', '1', null, '1', null, null, null, null, null, '6', '44', '43', '2018-03-14 08:47:01', '2018-03-13 09:37:43', null, null, '1', 'C00022', null, '4');
INSERT INTO `members` VALUES ('12', 'abc6', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520925126_5aa779c62515f7.62061228.png', '2008-06-13 00:00:00', '2', '111111112', '2018-04-24 00:00:00', '111111112', '2018-02-15 00:00:00', 'hn', 'sdfsdfds', 'ádfsafd', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2015', '15', null, null, null, null, 'PC2EeI3N', '344651', '1', '1', '1', '2018-03-13 14:13:37', '2018-03-14 11:00:00', '13', '1', null, '1', null, '3', '200006', '155aa786b3d7f5a', '2018-03-13 08:07:15', '6', '60', '59', '2018-03-13 07:25:25', '2018-03-13 07:23:47', null, null, null, 'C00014', null, '1');
INSERT INTO `members` VALUES ('13', 'abc7', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520925411_5aa77ae338ecc5.08126084.png', '2012-02-27 00:00:00', '1', '111111111', '2018-04-04 00:00:00', '111111111', '2018-01-31 00:00:00', 'hn', 'sdfsdfds', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2015', '4', null, null, null, null, 'AiXTC0KA', '966495', '1', '1', '1', '2018-03-13 14:17:14', '2018-03-14 11:00:00', '13', '1', null, '1', null, '3', '200007', '155aa786b76023d', '2018-03-13 08:07:19', '3', '60', '59', '2018-03-13 07:25:13', '2018-03-13 07:23:53', null, null, null, 'C00013', null, '1');
INSERT INTO `members` VALUES ('14', 'abc8', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520925624_5aa77bb8c88a67.25408616.png', '2012-01-28 00:00:00', '1', '111111110', '2018-04-11 00:00:00', '11111110', '2018-02-13 00:00:00', 'HN', 'sdfsd', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2015', '12', null, null, null, null, 'ORtj0TUS', '357090', '1', '1', '1', '2018-03-13 14:20:42', '2018-03-28 11:24:13', '13', '1', null, '1', null, '3', '200008', '155aa7eb42355a4', '2018-03-13 15:16:18', '7', '60', '59', '2018-03-13 07:24:50', '2018-03-13 07:23:59', null, null, null, 'C00012', null, '1');
INSERT INTO `members` VALUES ('15', 'abcd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520925662_5aa77bde6d8a10.88546578.png', '2008-01-10 00:00:00', '2', '101111111', '2018-04-11 00:00:00', '101111111', '2018-02-21 00:00:00', 'HN', 'sdfsdf', 'sdfsdfsd', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2014', '15', null, null, null, null, 'n3XJNpwD', '695511', '1', '1', '1', '2018-03-13 14:22:31', '2018-03-28 11:38:23', '13', '1', null, '1', null, '3', '200020', '155aaa475e2902b', '2018-03-15 10:13:50', '2', '60', '59', '2018-03-13 07:24:38', '2018-03-13 07:24:05', null, null, null, 'C00011', null, '11');
INSERT INTO `members` VALUES ('16', 'tran thi thanh ha update bo sung yemail', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/13/img_1520954904_5aa7ee18e17c42.56431147.jpeg', '2013-06-07 00:00:00', '2', '111111421', null, '111111421', '2018-02-09 00:00:00', '111111421', '111111421', '111111421', '111111421', null, 'tranha084@gmail.com', null, '3', '1', '2018', '31', 'adadada', 'afafa', null, 'afaf', '8K8g3vDq', '230213', '1', '1', '1', '2018-03-13 22:30:01', '2018-03-24 22:17:32', '2', '1', null, null, null, null, null, null, null, '2', null, '17', null, '2018-03-24 15:17:32', null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('17', 'Viettel Test wwww', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/19/img_1521424051_5aaf16b3407510.12927624.png', '1980-01-14 00:00:00', '2', '100100011', '2019-07-28 00:00:00', '123456789', '2016-06-29 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '5', null, null, null, null, 'Ontt4jFC', '815776', '1', '1', '1', '2018-03-14 09:38:08', '2018-03-19 08:50:33', '13', '1', null, '1', null, '3', '200009', '155aa89959e7f6b', '2018-03-14 03:39:05', '2', '44', '43', '2018-03-14 03:04:01', '2018-03-14 02:54:47', null, null, null, 'C00015', null, '1');
INSERT INTO `members` VALUES ('18', '123', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520995370_5aa88c2ad091d4.55577605.png', '2009-03-27 00:00:00', '1', '333333339', '2018-04-04 00:00:00', '33333339', '2018-01-31 00:00:00', 'HN', 'sfdsf', 'dfsdfd', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2013', '12', null, null, null, null, 'Kc0kmX9Y', '545354', '1', '1', '1', '2018-03-14 09:43:12', '2018-03-15 17:57:29', '13', '1', null, '1', null, '3', '200019', '155aaa47593d70a', '2018-03-15 10:13:45', '5', '44', '59', '2018-03-14 08:49:28', '2018-03-14 07:35:31', null, null, null, 'C00030', '4', '1');
INSERT INTO `members` VALUES ('19', 'test upload ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/23/img_1521774304_5ab46ee06250c8.02506241.jpeg', '2008-03-30 00:00:00', '1', '333333338', '2018-04-29 00:00:00', '345345', '2018-02-02 00:00:00', 'CA Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'ngandt5@viettel.com.vn', '2', '1', '2013', '31', null, null, null, null, 'jQ21SJNa', '102054', '1', '1', '1', '2018-03-23 10:06:19', '2018-03-23 17:05:30', '8', '1', null, null, null, null, null, null, null, '3', '44', '43', '2018-03-23 10:05:30', '2018-03-23 03:51:15', null, null, '1', 'C00021', null, '1');
INSERT INTO `members` VALUES ('20', '1234567', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520998448_5aa89830333874.71676028.png', '2008-05-06 00:00:00', '1', '333333337', '2018-04-04 00:00:00', '333333337', '2018-02-08 00:00:00', 'HN', 'sadasd', 'ádasd', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '14', null, null, null, null, 'FaprSEwt', '960842', '1', '1', '1', '2018-03-14 10:34:30', '2018-03-14 10:34:30', '1', '2', null, null, null, null, null, null, null, '4', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('21', 'check trạng thái khi DK HDV đã bị xóa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/23/img_1521796242_5ab4c492d0db12.65300852.jpeg', '2008-01-27 00:00:00', '1', '146146146', '2018-04-20 00:00:00', '15155', '2018-01-31 00:00:00', 'CA Hà Nội', 'fgdg', 'dge54', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '2', '1', '2014', '31', null, null, null, null, 'h3Sx4kud', '882831', '1', '1', '1', '2018-03-23 16:22:18', '2018-03-29 14:38:37', '14', '1', null, null, null, null, null, null, null, '2', null, '59', null, '2018-03-14 10:32:18', null, null, null, null, '1', '16');
INSERT INTO `members` VALUES ('22', 'Nguyễn Thị Mai 13032018_outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520998764_5aa8996cd76048.57500578.jpeg', '2009-02-01 00:00:00', '1', '136136136', '2018-04-04 00:00:00', '112098866', '2018-02-02 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '3', '1', '2018', '2', null, null, null, null, 'y22z88z7', '930569', '1', '1', '1', '2018-03-14 10:40:54', '2018-03-15 18:00:42', '13', '61', null, '1', null, '3', '300004', '155aaa475652803', '2018-03-15 10:13:42', '1', '44', '43', '2018-03-14 03:55:27', '2018-03-14 03:51:56', '2', null, null, 'C00016', null, '15');
INSERT INTO `members` VALUES ('23', 'test1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1520999922_5aa89df264c614.53045359.png', '2012-06-26 00:00:00', '1', '444444449', '2018-04-05 00:00:00', '444444449', '2018-02-01 00:00:00', 'Hà Nội', 'dsfsd', 'fsdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '8', null, null, null, null, '4sOoktYI', '453199', '1', '1', '1', '2018-03-14 10:59:01', '2018-03-16 10:43:03', '13', '1', null, '1', null, '3', '200018', '155aaa4753860b6', '2018-03-15 10:13:39', '8', '44', '59', '2018-03-14 08:49:18', '2018-03-14 06:59:11', null, null, null, 'C00029', null, '1');
INSERT INTO `members` VALUES ('24', 'test2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000092_5aa89e9cab2b26.23505432.png', '2012-05-27 00:00:00', '1', '444444448', '2018-04-05 00:00:00', '444444448', '2018-02-15 00:00:00', 'HN', 'sdfsd', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '12', null, null, null, null, '2GkJkifL', '933453', '1', '1', '1', '2018-03-14 11:01:51', '2018-03-16 10:45:17', '13', '1', null, '1', null, '3', '200017', '155aaa4750c709c', '2018-03-15 10:13:36', '8', '44', '59', '2018-03-14 08:49:05', '2018-03-14 07:35:14', null, null, null, 'C00028', null, '1');
INSERT INTO `members` VALUES ('25', 'test3', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000157_5aa89edd3e2789.18421697.png', '2009-06-27 00:00:00', '1', '444444447', '2018-04-04 00:00:00', '444444447', '2018-02-08 00:00:00', 'HN', 'dsfds', 'sdfds', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '14', null, null, null, null, 'V1yM0kts', '655372', '1', '1', '1', '2018-03-14 11:03:35', '2018-03-28 16:14:11', '13', '1', null, '1', null, '3', '200029', '155abb5cdadeb47', '2018-03-28 09:14:02', '9', '44', '59', '2018-03-14 08:48:41', '2018-03-14 04:34:57', null, null, null, 'C00027', null, '1');
INSERT INTO `members` VALUES ('26', 'Nguyễn Thị mai - tran thi thanh ha update bo sung', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000074_5aa89e8ab8c867.46209850.jpeg', '1988-06-12 00:00:00', '2', '135135135', '2018-04-05 00:00:00', '112098866', '2018-02-24 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', null, 'tranha084@gmail.com', 'maint18@viettel.com.vn', '1', '1', '2018', '31', 'Các kỹ năng khác', 'Thông tin khác', 'Hạng hướng dẫn viên', 'Thành tích', 'UP3Kx9Uh', '836637', '1', '1', '1', '2018-03-14 11:04:18', '2018-03-24 22:14:17', '7', '18', null, null, null, null, null, null, null, '7', null, '17', null, '2018-03-24 15:14:17', '1', null, null, null, null, '2');
INSERT INTO `members` VALUES ('27', 'test4', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000306_5aa89f721a09c5.11702751.png', '2012-02-07 00:00:00', '1', '444444446', '2018-04-27 00:00:00', '444444446', '2018-02-08 00:00:00', 'hn', 'dsfds', 'ssdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2014', '11', null, null, null, null, '09NH0ipZ', '836505', '1', '1', '1', '2018-03-14 11:05:23', '2018-03-16 10:49:43', '13', '3', null, '1', null, '3', '200015', '155aaa47476b302', '2018-03-15 10:13:27', '1', '44', '59', '2018-03-14 08:48:29', '2018-03-14 04:34:16', null, null, null, 'C00026', null, '1');
INSERT INTO `members` VALUES ('28', 'test5', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000510_5aa8a03e918673.63219191.png', '2009-06-06 00:00:00', '1', '444444445', '2018-04-04 00:00:00', '44444445', '2018-02-08 00:00:00', 'hn', 'sdfsd', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '11', null, null, null, null, 'OlKGWayZ', '354102', '1', '1', '1', '2018-03-14 11:08:51', '2018-03-16 14:58:58', '13', '1', null, '1', null, '3', '200014', '155aaa47445d11e', '2018-03-15 10:13:24', '6', '44', '59', '2018-03-14 08:48:18', '2018-03-14 04:33:45', null, null, null, 'C00025', null, '1');
INSERT INTO `members` VALUES ('29', 'test6', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000611_5aa8a0a3be9418.75209168.png', '2009-06-28 00:00:00', '2', '444444444', '2018-04-05 00:00:00', '444444444', '2018-02-07 00:00:00', 'HN', 'sdfsd', 'sdfsdf', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '3', null, null, null, null, 'hIaUiJDZ', '218236', '1', '1', '1', '2018-03-14 11:10:31', '2018-03-24 23:42:29', '13', '1', null, '1', null, '3', '200024', '155ab67fb690e7a', '2018-03-24 16:41:26', '9', '44', '59', '2018-03-14 08:48:05', '2018-03-14 04:33:07', null, null, null, 'C00024', null, '1');
INSERT INTO `members` VALUES ('30', 'Nguyễn Thị Mai 14032018_outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000303_5aa89f6f1dde48.51656426.jpeg', '1988-06-01 00:00:00', '1', '126126126', '2018-04-04 00:00:00', '112098866', '2018-02-24 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', null, '1', '2', '2015', '8', 'Kỹ năng khác', 'Thông tin khác', 'Hạng hướng dẫn viên', 'Thành tích', 'U7Trx8k2', '715647', '1', '1', '1', '2018-03-14 11:11:48', '2018-03-30 17:09:32', '13', '65', null, null, null, '3', '100015', '155abddb4fbd30e', '2018-03-30 06:38:07', '7', '44', '43', '2018-03-28 02:54:05', '2018-03-15 09:32:45', '2', null, null, 'C00065', null, '4');
INSERT INTO `members` VALUES ('31', 'test7 _ chị hà xinh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000718_5aa8a10e36ca45.30924792.png', '2010-01-01 00:00:00', '2', '190190190', '2018-04-06 00:00:00', '112098866 sửa', '2018-01-03 00:00:00', 'hn sửa', 'sadfsd sửa', 'sdfsdf sửa', '0978429296', '0976831235', 'maint18@viettel.com.vn', 'thuyenht@viettel.com.vn', '3', '1', '2014', '31', '8 sửa', '9 sửa', '10 sửa', '11 sửa', 'knnwWBX1', '210779', '1', '1', '1', '2018-03-14 11:12:19', '2018-03-16 16:28:16', '13', '18', null, '1', null, '3', '200022', '155aab7aa623476', '2018-03-16 08:04:54', '8', '44', '59', '2018-03-14 08:47:14', '2018-03-14 04:32:26', null, null, null, 'C00023', null, '7');
INSERT INTO `members` VALUES ('32', 'Nguyễn Thị Mai 14032018 nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000776_5aa8a148b02fa4.58657616.jpeg', '2007-02-09 00:00:00', '2', '178178178', '2018-04-05 00:00:00', '112098866', '2018-02-09 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '2', '1', '2014', '29', null, null, null, null, 'beskTTDx', '118388', '1', '1', '1', '2018-03-14 11:15:41', '2018-03-30 09:20:32', '13', '26', null, '1', null, '3', '200031', '155abca53ce300c', '2018-03-29 08:35:08', '7', '44', '59', '2018-03-16 02:41:55', '2018-03-15 09:31:01', null, null, null, 'C00038', null, '4');
INSERT INTO `members` VALUES ('33', 'Nguyễn Thị Mai 14032018_tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521000979_5aa8a213a90524.43442784.jpeg', '2010-03-28 00:00:00', '1', '154154154', null, '112098866', '2018-02-04 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '3', '2', '2014', '31', null, null, null, null, 'CMdLSjUc', '786069', '1', '1', '1', '2018-03-14 11:18:13', '2018-03-16 15:00:11', '13', '65', null, '1', null, '3', '300003', '155aaa474190eb1', '2018-03-15 10:13:21', '7', '44', '43', '2018-03-14 07:45:44', '2018-03-14 07:23:46', null, null, null, 'C00020', null, '4');
INSERT INTO `members` VALUES ('34', 'Viettel 1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521001094_5aa8a286a6e802.64848611.png', '2008-03-14 00:00:00', '1', '123456789', '2018-09-02 00:00:00', '123456789', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '5', null, null, null, null, 'Hvz8oAtX', '959846', '1', '1', '1', '2018-03-14 11:20:25', '2018-03-16 15:01:14', '13', '1', null, '1', null, '3', '200010', '155aa8e0bd5d225', '2018-03-14 08:43:41', '2', '44', '43', '2018-03-14 04:31:24', '2018-03-14 04:29:26', '1', null, null, 'C00018', null, '1');
INSERT INTO `members` VALUES ('35', 'Viettel 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521001300_5aa8a354bf9383.26169011.png', '2008-03-14 00:00:00', '1', '123456781', '2018-05-06 00:00:00', '123456789', '2018-02-16 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '5', null, null, null, null, '0F8pxkH4', '728027', '1', '1', '1', '2018-03-14 11:22:59', '2018-03-16 15:01:14', '13', '1', null, '1', null, '3', '200011', '155aaa2a2f7690a', '2018-03-15 08:09:19', '8', '44', '43', '2018-03-14 04:31:40', '2018-03-14 04:29:38', null, null, null, 'C00019', null, '1');
INSERT INTO `members` VALUES ('36', 'Viettel 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521001456_5aa8a3f0885c33.93377903.png', '1980-03-14 00:00:00', '1', '123456782', '2018-05-06 00:00:00', '123456789', '2018-02-24 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '5', null, null, null, null, 'q2Qgf7Vc', '543453', '1', '1', '1', '2018-03-14 11:25:35', '2018-03-16 15:01:14', '13', '1', null, '1', null, '3', '200013', '155aaa473e484cf', '2018-03-15 10:13:18', '2', '44', '43', '2018-03-14 04:31:04', '2018-03-14 04:29:45', null, null, null, 'C00017', null, '1');
INSERT INTO `members` VALUES ('37', 'fdvfd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521023353_5aa8f979cbdc89.65386207.png', '2008-03-14 00:00:00', '1', '435345344', null, 'dgfdg', '2018-02-08 00:00:00', 'gvfdgdf', '4543', '345435', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '5', null, null, null, null, '4yEEbq6A', '391206', '1', '1', '1', '2018-03-14 17:30:04', '2018-03-25 10:56:38', '14', '1', null, '1', null, '3', '300007', '155ab67fba6b3ac', '2018-03-24 16:41:30', '7', '44', '43', '2018-03-14 10:32:12', '2018-03-14 10:30:52', null, null, null, 'C00032', '9', '1');
INSERT INTO `members` VALUES ('38', 'aaaaaaaaaaa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/14/img_1521023457_5aa8f9e192b5a3.78287630.png', '2009-02-13 00:00:00', '1', '555555559', '2018-04-05 00:00:00', '555555559', '2018-02-08 00:00:00', 'hn', 'dsfsd', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '2', '1', '2015', '14', null, null, null, null, 'ACTJNfDR', '564806', '1', '1', '1', '2018-03-14 17:31:33', '2018-03-29 16:57:47', '13', '1', null, '1', null, '3', '200033', '155abcb89241bd8', '2018-03-29 09:57:38', '3', '60', '59', '2018-03-16 03:53:14', '2018-03-14 10:32:08', null, null, null, 'C00044', null, '1');
INSERT INTO `members` VALUES ('39', 'Tran Thi Thanh Ha', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521085962_5aa9ee0ad475d4.46558244.png', '2013-06-15 00:00:00', '2', '123567895', '2018-04-04 00:00:00', '123567895', '2018-02-25 00:00:00', '123567895123567895', '123567895', '123567895', '123567895', null, 'tranha084@gmail.com', null, '1', '1', '2018', '19', null, null, null, null, 'TmYqLtDo', '650053', '1', '1', '1', '2018-03-15 10:54:42', '2018-03-19 14:54:40', '1', '2', null, null, null, null, null, null, null, '2', null, null, null, null, '2', null, null, null, null, '2');
INSERT INTO `members` VALUES ('40', 'Viettel 4', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521086361_5aa9ef99a931c1.10860793.jpeg', '2013-03-22 00:00:00', '1', '123456543', '2018-05-06 00:00:00', '12345678', '2018-01-31 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2013', '31', null, null, null, null, 's5o3yuac', '766752', '1', '1', '1', '2018-03-15 11:21:28', '2018-03-15 11:42:56', '2', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('41', 'viettel 5', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521087979_5aa9f5eb5da5c8.99504258.jpeg', '2013-03-22 00:00:00', '1', '123122331', '2018-04-28 00:00:00', '21321312312321321321', '2018-02-17 00:00:00', 'Hà Nội', 'ê', 'fwfew', '0977635498', '0977635498', 'xuantoi90@gmail.com', 'thuyenht@viettel.com.vn', '2', '1', '2013', '30', null, null, null, null, 'tkfAAuof', '996285', '1', '1', '1', '2018-03-15 11:34:58', '2018-03-29 17:11:40', '13', '1', null, '1', null, '3', '200034', '155abcb911c207e', '2018-03-29 09:59:45', '6', '44', '43', '2018-03-21 02:46:43', '2018-03-15 07:42:44', null, null, null, 'C00048', null, '1');
INSERT INTO `members` VALUES ('42', 'Nguyễn Thị Mai 15032018 _ inbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521087489_5aa9f401692d25.28679830.jpeg', '1987-06-12 00:00:00', '2', '167167167', '2018-04-05 00:00:00', '112098866', '2018-02-08 00:00:00', 'CA TP HN', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', null, 'maint18@viettel.com.vn', null, '1', '1', '2018', '31', 'Các kỹ năng chuyên môn khác', 'Thông tin khác', 'Hạng hướng dẫn viên', 'Thành tích', 'S1KWcUko', '341469', '1', '1', '1', '2018-03-15 11:30:19', '2018-03-30 09:19:19', '13', '1', null, '1', null, '3', '100007', '155abca51a37b9b', '2018-03-29 08:34:34', '7', '44', '43', '2018-03-16 02:39:46', '2018-03-15 09:27:31', '1', null, null, 'C00036', null, '2');
INSERT INTO `members` VALUES ('43', 'test bổ sung 1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521088297_5aa9f7296fcaf7.17732179.png', '2012-01-06 00:00:00', '1', '999999999', null, '999999999', '2018-01-31 00:00:00', 'Hà Nội', 'dsfsdfds', 'sdfsdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '3', '1', '2014', '31', null, null, null, null, 'Bl9PmX9m', '450974', '1', '1', '1', '2018-03-15 11:32:09', '2018-03-29 16:55:10', '13', '1', null, null, null, '3', null, null, null, '4', '60', '59', '2018-03-16 03:48:31', '2018-03-15 07:31:15', null, null, null, 'C00042', null, '1');
INSERT INTO `members` VALUES ('44', 'test bổ sung 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521088561_5aa9f8318dfa69.34264909.png', '2012-05-14 00:00:00', '1', '999999998', '2018-04-05 00:00:00', '999999998', '2018-01-31 00:00:00', 'Hà Nội', 'sdfsd', 'adfsadfds', '0976831235', null, 'tranha084@gmail.com', null, '2', '1', '2014', '16', null, null, null, null, 'IcQ4eNAG', '094893', '1', '1', '1', '2018-03-15 11:36:29', '2018-03-29 16:48:29', '13', '1', null, '1', null, '3', '200032', '155abcb63f883c5', '2018-03-29 09:47:43', '3', '60', '59', '2018-03-16 03:47:55', '2018-03-15 07:31:34', null, null, '1', 'C00041', null, '1');
INSERT INTO `members` VALUES ('45', 'Nguyễn Thị Mai 15032018', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521088608_5aa9f860021707.21099739.jpeg', '1988-06-01 00:00:00', '1', '187187187', '2018-04-04 00:00:00', '112098866', '2018-02-16 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '1', '2', '2018', '9', null, null, null, null, 'Cfq5CARm', '552927', '1', '1', '1', '2018-03-15 11:39:17', '2018-03-30 09:20:32', '13', '65', null, '1', null, '3', '100008', '155abca5494227b', '2018-03-29 08:35:21', '3', '44', '43', '2018-03-16 02:43:42', '2018-03-15 09:27:18', '2', null, null, 'C00039', null, '2');
INSERT INTO `members` VALUES ('46', 'test bổ sung 3', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521088654_5aa9f88e426f73.60831979.png', '2012-06-27 00:00:00', '1', '999999997', '2018-04-26 00:00:00', '999999997', '2018-02-09 00:00:00', 'Hà Nội', 'sdasdas', 'ádasdasdasdasdas', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '14', null, null, null, null, 'mFBOxi7f', '415095', '1', '1', '1', '2018-03-15 11:39:27', '2018-03-28 09:53:23', '13', '1', null, '1', null, '3', '200026', '155ab880111c66c', '2018-03-26 05:07:29', '5', '60', '59', '2018-03-16 03:47:36', '2018-03-15 07:31:44', null, null, null, 'C00040', null, '1');
INSERT INTO `members` VALUES ('47', 'sâs', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521089533_5aa9fbfdab41b7.34213664.png', '2013-06-15 00:00:00', '1', '535353211', null, '535353211', '2018-02-08 00:00:00', '535353211', '535353211', '535353211', '535353211', null, 'tranha084@gmail.com', null, '3', '1', '2014', '17', null, null, null, null, 'pL0mb4hR', '920641', '1', '0', '0', '2018-03-15 11:52:59', '2018-03-15 11:52:59', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('48', 'Nguyễn Thị Mai 15032018 _ Nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521089540_5aa9fc04cd84a2.49142164.jpeg', '1987-06-12 00:00:00', '2', '148148148', '2018-05-11 00:00:00', '112098866', '2018-02-09 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', null, '2', '1', '2017', '13', null, null, null, null, 'zyvjZwnQ', '078553', '1', '1', '1', '2018-03-15 11:53:16', '2018-03-26 10:34:40', '13', '18', null, '1', null, '3', '200025', '155ab67fbf63f12', '2018-03-24 16:41:35', '10', '44', '43', '2018-03-16 02:41:41', '2018-03-15 09:27:01', null, null, null, 'C00037', null, '7');
INSERT INTO `members` VALUES ('49', 'Nguyễn Thị Mai 15032018 tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521095722_5aaa142ac1a529.28993798.jpeg', '1987-06-12 00:00:00', '2', '169169169', null, '112098866', '2018-02-08 00:00:00', 'CA TP Hà nội', 'Địa chỉ thường trú', 'Địa chỉ liên hệ', '0978429296', '0913207826', 'maint18@viettel.com.vn', null, '3', '1', '2010', '1', null, null, null, null, 'lYq26pg2', '396767', '1', '1', '1', '2018-03-15 13:40:14', '2018-03-28 16:14:58', '13', '9', null, '1', null, '3', '300008', '155abb5d0bdb3b2', '2018-03-28 09:14:51', '7', '44', '43', '2018-03-16 02:39:13', '2018-03-01 09:22:33', null, null, null, 'C00035', null, '58');
INSERT INTO `members` VALUES ('50', 'Tran Thi Thanh Ha - Check send email', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521096217_5aaa16197fe6a9.26303312.png', '2013-06-15 00:00:00', '1', '678978998', null, '678978998', '2018-02-23 00:00:00', '678978998', '678978998', '678978998', '678978998', null, 'tranha084@gmail.com', null, '3', '1', '2018', '19', null, null, null, null, 'hTEbItEa', '135998', '1', '1', '1', '2018-03-15 13:47:38', '2018-03-21 17:22:03', '14', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('51', 'viettel 6', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521096785_5aaa18516c5c95.82943798.jpeg', '2013-02-28 00:00:00', '1', '123456788', '2018-05-06 00:00:00', '23213123131321324234', '2018-02-17 00:00:00', 'Hà Nội', 'Hà Nam', 'Nam định', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '1973', '31', 'dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fds', 'dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fds', 'dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fds', 'dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fdsfdfds dsfdsfds fds', 'ESDJa3gI', '943111', '1', '1', '1', '2018-03-15 13:56:04', '2018-03-23 17:08:50', '14', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 03:19:13', null, null, null, null, '7', '1');
INSERT INTO `members` VALUES ('52', 'viettel 7', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521099017_5aaa2109e09aa4.26538832.jpeg', '2013-03-15 00:00:00', '1', '255342153', '2018-05-06 00:00:00', '32423453543343453543', '2018-02-23 00:00:00', 'Hà Nội', 'hải dương', 'hải phòng', '0977635498', '097763549832432', 'nghianm.dev@gmail.com', 'thuyenht@dsfgdg.ewe', '1', '1', '2018', '30', 'êw', 'ewrwerew', 'ẻwerwe', 'rẻw', 'rrMJ4t0C', '943958', '1', '1', '1', '2018-03-15 14:30:54', '2018-03-15 16:14:40', '2', '1', null, null, null, null, null, null, null, '4', '17', '43', '2018-03-15 09:14:40', '2018-03-15 07:42:04', '1', null, null, null, null, '1');
INSERT INTO `members` VALUES ('53', 'Tran Thi Thanh Ha - Check send email', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521099182_5aaa21ae76da72.44122017.png', '2013-03-15 00:00:00', '2', '632222222', '2018-04-11 00:00:00', '632222222', '2018-02-23 00:00:00', '632222222632222222632222222', '632222222', '632222222', '632222222', null, 'tranha084@gmail.com', null, '1', '1', '2018', '18', null, null, null, null, 'h8drT2AT', '142559', '1', '1', '1', '2018-03-15 14:34:35', '2018-03-16 11:31:30', '10', '1', null, null, null, null, null, null, null, '2', null, '17', null, '2018-03-16 04:29:57', '1', null, null, null, null, '1');
INSERT INTO `members` VALUES ('54', 'Tran Thi Thanh Ha 222', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521099437_5aaa22ade093d0.43878124.png', '2013-06-15 00:00:00', '2', '323232323', '2018-04-04 00:00:00', '323232323', '2018-02-15 00:00:00', '323232323', '323232323', '323232323', '323232323', null, 'tranha084@gmail.com', null, '1', '1', '2018', '12', null, null, null, null, 'Ze8tuug8', '970938', '1', '1', '1', '2018-03-15 14:38:29', '2018-03-29 16:57:05', '13', '1', null, '1', null, '3', '100009', '155abcb85d40a6a', '2018-03-29 09:56:45', '1', '60', '43', '2018-03-16 03:52:49', '2018-03-16 03:20:32', '2', null, null, 'C00043', null, '18');
INSERT INTO `members` VALUES ('55', 'viettel 8', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521105233_5aaa3951ead131.12556780.jpeg', '2013-03-15 00:00:00', '1', '123456712', '2018-06-10 00:00:00', '678', '2018-02-11 00:00:00', 'Hà Nội', 'hà nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2018', '31', null, null, null, null, '61O4YPJ8', '069910', '1', '1', '1', '2018-03-15 16:22:42', '2018-03-15 16:44:29', '2', '1', null, null, null, null, null, null, null, '2', '60', '59', '2018-03-15 09:44:29', '2018-03-15 09:40:59', null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('56', 'fdsfdsfsd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521105921_5aaa3c0160c8f4.25210043.jpeg', '2013-03-15 00:00:00', '1', '123456714', '2018-04-15 00:00:00', '34', '2018-02-01 00:00:00', 'ểwr', 'ewr', 'rrerwe', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2015', '12', null, null, null, null, 'vFxcZwIV', '986297', '1', '1', '0', '2018-03-15 16:27:16', '2018-03-15 16:27:52', '1', '1', null, null, null, null, null, null, null, '1', null, null, null, null, '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('57', 'thuyên_test1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521106057_5aaa3c89715ce3.22080173.png', '2008-10-13 00:00:00', '1', '888888888', '2018-04-05 00:00:00', '888888888', '2018-02-25 00:00:00', 'HN', 'dsfsdfsd', 'sdfsdfds', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2015', '31', null, null, null, null, '4DmBHGOQ', '471309', '1', '1', '1', '2018-03-15 16:27:59', '2018-03-16 15:01:14', '13', '8', null, '1', null, '3', '200012', '155aaa41259421f', '2018-03-15 09:47:17', '3', '60', '59', '2018-03-15 09:45:45', '2018-03-15 09:37:57', null, null, null, 'C00033', null, '1');
INSERT INTO `members` VALUES ('58', 'fdsfdsfsd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/15/img_1521105921_5aaa3c0160c8f4.25210043.jpeg', '2013-03-15 00:00:00', '1', '123456713', '2018-04-15 00:00:00', '34', '2018-02-01 00:00:00', 'ểwr', 'ewr', 'rrerwe', '0977635498', null, 'ngandt5@viettel.com.vn', 'gvdfdf@fdf.gdf', '1', '1', '2015', '12', null, null, null, null, 'zuS8YFhv', '092237', '1', '0', '0', '2018-03-15 16:28:35', '2018-03-15 16:31:29', '1', '4', null, null, null, null, null, null, null, '1', null, null, null, null, '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('59', 'Viettel 9_ngân update ảnh khác', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522401811_5abe021386d353.64414278.jpeg', '2013-03-23 00:00:00', '1', '232332243', '2018-04-05 00:00:00', '65473', '2018-02-01 00:00:00', 'Hà Nội', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '2', '2', '2015', '31', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', 'cNwtglDw', '575706', '1', '1', '1', '2018-03-30 16:23:42', '2018-03-30 16:46:44', '10', '65', null, '1', null, '3', '200021', '155aab366059fc6', '2018-03-16 03:13:36', '2', '92', '43', '2018-03-30 09:24:05', '2018-03-16 02:31:12', null, null, null, 'C00034', null, '1');
INSERT INTO `members` VALUES ('60', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt q', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521167765_5aab2d955deb76.02266190.png', '2012-05-27 00:00:00', '1', '555555551', '2018-04-05 00:00:00', '55555555945454545454', '2018-02-22 00:00:00', 'Hà Nội', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt qua được nỗi ám ảnh của cuộc thảm sát Mỹ Lai.\r\nMike Hastie, một cựu binh và cũng là phóng viên chiến trường thời chiến tranh Việt Nam, rút trong túi ra', 'Sau nửa thế kỷ, nhiều người Mỹ vẫn chưa thể vượt qua được nỗi ám ảnh của cuộc thảm sát Mỹ Lai.\r\nMike Hastie, một cựu binh và cũng là phóng viên chiến trường thời chiến tranh Việt Nam, rút trong túi ra', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '2', '1', '2014', '12', null, null, null, null, 'go8ptAst', '332869', '1', '1', '1', '2018-03-16 09:36:30', '2018-03-24 09:47:41', '2', '8', null, null, null, null, null, null, null, '5', null, '59', null, '2018-03-16 02:44:35', null, null, '1', null, null, '1');
INSERT INTO `members` VALUES ('61', 'Viettel 9', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521172808_5aab4148712340.65793374.jpeg', '2008-03-01 00:00:00', '1', '232332241', '2018-04-29 00:00:00', '65473', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2013', '31', '1', '2', '3', '4', 'EFoT5Tsi', '252351', '1', '1', '1', '2018-03-16 11:01:13', '2018-03-16 11:03:18', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 04:03:18', '1', null, null, null, null, '1');
INSERT INTO `members` VALUES ('62', 'Viettel 10', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521173341_5aab435db17c56.13080195.png', '2008-03-01 00:00:00', '1', '232332240', '2018-04-29 00:00:00', '65473', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2013', '31', '1', '2', '3', '4', 'k8jXFrIb', '236050', '1', '1', '1', '2018-03-16 11:10:22', '2018-03-16 11:19:07', '7', '1', null, null, null, null, null, null, null, '2', null, '17', null, '2018-03-16 04:19:07', '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('63', 'Viettel 10', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521174851_5aab4943cc0cf8.42700808.jpeg', '2008-03-01 00:00:00', '1', '232332212', '2018-04-29 00:00:00', '65473', '2018-02-01 00:00:00', 'Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2013', '31', '1', '2', '3', '4', 'yt72n8Wv', '333147', '1', '0', '0', '2018-03-16 11:34:15', '2018-03-16 11:42:48', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('64', 'Nguyễn Thị MaiNguyễn Thị MaiNguyễn Thị MaiNguyễEnd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521174852_5aab494418e6e8.66516951.jpeg', '2009-03-08 00:00:00', '1', '124124124', '2018-04-12 00:00:00', '11209886611209886610', '2018-02-07 00:00:00', 'CA TP Hà nộiCA TP Hà nộiCA TP Hà nộiCA TP Hà nộEnd', 'Địa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trEnđ', 'Địa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịEnd', '0978429296', null, 'maint18@viettel.com.vn', null, '1', '1', '2015', '31', null, null, null, null, '4bSdIkbC', '776172', '1', '0', '0', '2018-03-16 11:34:40', '2018-03-16 11:34:55', '1', '1', null, null, null, null, null, null, null, '6', null, null, null, null, '1', null, null, null, null, '16');
INSERT INTO `members` VALUES ('65', 'Xem hồ sơ_Inbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', '2008-03-16 00:00:00', '1', '454654656', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'zVY40fHi', '623115', '1', '1', '1', '2018-03-16 13:33:04', '2018-03-30 10:30:52', '13', '1', null, null, null, '3', null, null, null, '2', '44', '43', '2018-03-16 07:21:27', '2018-03-16 07:21:07', '1', null, null, 'C00045', null, '1');
INSERT INTO `members` VALUES ('66', 'Xem hồ sơ_Outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', '2008-03-16 00:00:00', '1', '222222222', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'E5yrZhDT', '318929', '1', '1', '1', '2018-03-16 13:33:53', '2018-03-16 15:33:42', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 08:33:42', '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('67', 'Xem hồ sơ_Nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521181959_5aab6507b64694.16100161.jpeg', '2008-03-16 00:00:00', '1', '333333333', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'HxclH76t', '934399', '1', '1', '1', '2018-03-16 13:34:35', '2018-03-16 15:33:27', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 08:33:27', '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('68', 'Xem hồ sơ_Tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', '2008-03-16 00:00:00', '1', '555555555', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'fTOn7UXx', '519087', '1', '1', '1', '2018-03-16 13:35:28', '2018-03-21 09:58:07', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-21 02:58:07', null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('69', '11111111111111111111111111111111111111111111111111', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182376_5aab66a82d3aa8.89669388.png', '2012-06-28 00:00:00', '1', '121212121', '2018-04-11 00:00:00', '11111111111111111111', '2018-02-15 00:00:00', 'Hà Nội', 'sdfsdf', 'sdfsdfsd', '0976831235', '0976831235', 'thuyenht@viettel.com.vn', 'thuyenht@viettel.com.vn', '1', '1', '2014', '31', null, null, null, null, 'j4N1p4F8', '406035', '1', '1', '1', '2018-03-16 13:40:08', '2018-04-02 17:20:44', '3', '37', null, null, null, null, null, null, null, '10', null, '43', null, '2018-04-02 10:20:44', '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('70', 'Xem hồ sơ_Tại điểm sua', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', '2008-03-16 00:00:00', '1', '666666666', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'bLTuKZx0', '980429', '1', '1', '1', '2018-03-16 13:40:05', '2018-04-02 16:33:03', '13', '18', null, '1', null, '3', '300019', '155ac1f88680ad1', '2018-04-02 09:31:50', '2', '44', '43', '2018-03-30 08:12:26', '2018-03-26 03:27:39', null, null, null, 'C00076', '9', '11');
INSERT INTO `members` VALUES ('71', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182473_5aab67090e2454.09445286.jpeg', '2009-03-01 00:00:00', '1', '142142142', '2018-04-05 00:00:00', '11209886611209886610', '2018-02-21 00:00:00', 'CA TP Hà nộiCA TP Hà nộiCA TP Hà nộiCA dddddTPdEnđ', 'Địa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trúĐịa chỉ thường trEnd', 'Địa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịa chỉ liên hệĐịEnd', '0978429296', null, 'maint18@viettel.com.vn', 'sdfaseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeng@gmail.com', '1', '1', '2018', '31', 'Các chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên môn khácCác chuyên mEnđ', 'Thông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThông tin khácThôEnđ', 'Hạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dẫn viênHạng hướng dEnd', 'Thành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tíchThành tEnđ', '4tNUre6d', '669195', '1', '1', '1', '2018-03-16 13:50:02', '2018-03-30 10:10:08', '13', '1', null, '1', null, '3', '100006', '155abc9a0ddf723', '2018-03-29 07:47:25', '4', '44', '43', '2018-03-16 08:00:40', '2018-03-16 08:00:09', '1', null, null, 'C00046', null, '4');
INSERT INTO `members` VALUES ('72', 'Xem hồ sơ_Tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521182106_5aab659a0a9606.37950183.png', '2008-03-16 00:00:00', '1', '777777777', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'Rxy457NJ', '261062', '1', '1', '1', '2018-03-16 14:28:35', '2018-03-23 16:06:23', '1', '6', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, '1', null, null, '2');
INSERT INTO `members` VALUES ('73', 'sdasdasd', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521185137_5aab7171bfdb67.94907905.jpeg', '2009-03-01 00:00:00', '1', '196196196', null, '112098866', '2018-02-01 00:00:00', 'sdfasdf', 'ádfas', 'sdfasdfa', '0978429296', '0913207826', 'maint18@viettel.com.vn', 'mainguyen.it@gmail.com', '3', '2', '2015', '31', null, null, null, null, 'rhTqKZTs', '782645', '1', '1', '1', '2018-03-16 14:28:31', '2018-04-02 16:49:31', '7', '65', null, null, null, null, null, null, null, '4', null, '43', null, '2018-04-02 09:49:31', null, null, null, null, '1', '15');
INSERT INTO `members` VALUES ('74', 'Xem hồ sơ_Tại điểm Xem hồ sơ_Tại điểmXem hồ sơ_Tại', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521186969_5aab789974cb56.82377981.jpeg', '2008-03-16 00:00:00', '1', '000000001', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'sDoYf1K2', '766303', '1', '1', '1', '2018-03-16 14:56:36', '2018-03-16 14:56:36', '1', '6', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('75', 'check lap du lieu', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', '2008-03-16 00:00:00', '1', '000000002', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'F0lH2NHD', '086954', '1', '1', '1', '2018-03-16 15:02:02', '2018-03-16 15:02:02', '1', '6', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('76', 'check xóa hồ sơ', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/23/img_1521796242_5ab4c492d0db12.65300852.jpeg', '2008-03-23 00:00:00', '1', '000000003', '2018-05-06 00:00:00', '15155', '2018-02-07 00:00:00', 'CA Hà Nội', 'fgdg', 'dge54', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'tranha084@gmail.com', '2', '1', '2013', '31', null, null, null, null, 'cg9LcvET', '584375', '1', '1', '1', '2018-03-23 16:12:16', '2018-03-30 13:51:15', '9', '1', null, '1', null, '3', '200037', '155abdde63e84b5', '2018-03-30 06:51:15', '2', '17', '43', '2018-03-29 10:16:06', '2018-03-26 03:20:34', null, null, null, 'C00075', null, '16');
INSERT INTO `members` VALUES ('77', 'check BSHS tham dinh 1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521187276_5aab79cc1cda11.94248856.jpeg', '2008-03-16 00:00:00', '1', '000000004', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'CUHXgn09', '283157', '1', '1', '1', '2018-03-16 15:19:42', '2018-03-16 15:20:05', '7', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-16 08:20:05', null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('78', 'check BSHS tham dinh 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521188636_5aab7f1cb429a3.47387654.jpeg', '2008-03-16 00:00:00', '1', '000000005', '2018-05-06 00:00:00', '23432434343', '2018-02-17 00:00:00', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN V', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊC', '0977635498', null, 'dinhngan.tk61@gmail.com', null, '3', '1', '2018', '13', 'HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAMHỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆ', '<script> alert(\"test\") </script>', '<script><script>alert(\"test\")</script></script>', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', 'H38rpH1X', '057237', '1', '1', '1', '2018-03-16 15:24:34', '2018-04-02 17:12:15', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, '1', null, null, '1');
INSERT INTO `members` VALUES ('79', 'bổ sung thẩm định lần 2bổ sung thẩm định lần 3', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521191335_5aab89a77e0e21.51858982.png', '2009-01-06 00:00:00', '1', '111131111', '2018-05-05 00:00:00', '11111111111sua3', '2018-01-01 00:00:00', '11111111111sua2', 'bổ sung thẩm định lần 3', 'bổ sung thẩm định lần 3', '0976831235', null, 'thuyenht@viettel.com.vn', null, '1', '1', '2012', '31', 'dsfsdfds sửa', 'dsfsdfds sửa', 'dsfsdfds sửa', 'dsfsdfds sửa', 'X06e98Xj', '300338', '1', '1', '1', '2018-03-16 16:09:44', '2018-03-19 15:00:16', '7', '61', null, null, null, null, null, null, null, '8', null, '43', null, '2018-03-19 08:00:16', '2', null, null, null, null, '7');
INSERT INTO `members` VALUES ('80', 'bổ sung đăng ký', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/16/img_1521196610_5aab9e42252ea7.13632401.png', '2012-05-27 00:00:00', '1', '222322222', '2018-04-18 00:00:00', '222222222', '2018-02-13 00:00:00', 'Hà Nội', 'dsfsdfsd', 'sdfsdf', '0976831235', null, 'thuyenht@viettel.com.vn', null, '1', '1', '2014', '14', null, null, null, null, 'hqyMmfRu', '061233', '1', '1', '1', '2018-03-16 17:37:31', '2018-03-30 13:39:38', '9', '9', null, null, null, '3', '100016', '155abddbaaeb545', '2018-03-30 06:39:38', '3', '17', '59', '2018-03-28 03:21:55', '2018-03-16 10:38:24', '2', null, null, 'C00068', null, '15');
INSERT INTO `members` VALUES ('81', 'viettel 11', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/19/img_1521434269_5aaf3e9dc877f9.89915900.jpeg', '2008-03-19 00:00:00', '2', '131313131', '2018-05-06 00:00:00', '121212121212', '2018-02-02 00:00:00', 'CA Hà Nội', '1. Vào acc kế toán\r\n2. Vào Danh sách hồ sơ\r\n3. Click combo Nơi đăng ký sinh hoạt\r\n==> Mong muốn: hiển thị CLB thuộc hội… khi văn phòng đại diện là trống hoặc Hội Hà Nội', '1. Vào acc kế toán\r\n2. Vào Danh sách hồ sơ\r\n3. Click combo Nơi đăng ký sinh hoạt\r\n==> Mong muốn: hiển thị CLB thuộc hội… khi văn phòng đại diện là trống hoặc Hội Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'ngandt5@viettel.com.vn', '1', '1', '2013', '31', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'XQo7cF11', '712989', '1', '1', '1', '2018-03-19 11:43:25', '2018-03-29 17:11:40', '13', '1', null, '1', null, '3', '100011', '155abcbbd522f78', '2018-03-29 10:11:33', '6', '60', '43', '2018-03-21 02:51:38', '2018-03-21 02:49:25', '1', null, null, 'C00049', null, '26');
INSERT INTO `members` VALUES ('82', 'viettel 12_Outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/19/img_1521434670_5aaf402e31be81.55316577.jpeg', '2008-03-19 00:00:00', '2', '232332242', '2018-05-06 00:00:00', '121212121212', '2018-02-02 00:00:00', 'CA Hà Nội', '1. Vào acc kế toán\r\n2. Vào Danh sách hồ sơ\r\n3. Click combo Nơi đăng ký sinh hoạt\r\n==> Mong muốn: hiển thị CLB thuộc hội… khi văn phòng đại diện là trống hoặc Hội Hà Nội', '1. Vào acc kế toán\r\n2. Vào Danh sách hồ sơ\r\n3. Click combo Nơi đăng ký sinh hoạt\r\n==> Mong muốn: hiển thị CLB thuộc hội… khi văn phòng đại diện là trống hoặc Hội Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'ngandt5@viettel.com.vn', '1', '1', '2013', '31', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'Tên doanh nghiệp đã và đang công tác', 'TiblfGFo', '745825', '1', '1', '1', '2018-03-19 11:44:45', '2018-03-19 14:59:55', '2', '1', null, null, null, null, null, null, null, '6', null, '43', null, '2018-03-19 07:59:55', '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('83', 'viettel 18', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/29/img_1522286278_5abc3ec6481985.83881824.jpeg', '2009-03-14 00:00:00', '1', '141415141', '2018-06-10 00:00:00', '15155', '2018-02-03 00:00:00', 'viettel 18 viettel 18 viettel 18 viettel 18 viette', 'viettel 18 viettel 18 viettel 18 viettel 18', 'viettel 18 viettel 18 viettel 18 viettel 18', '0977635498', '0977635498564', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '1', '2', '2016', '31', 'viettel 18 viettel 18 viettel 18 viettel 18', 'viettel 18 viettel 18 viettel 18 viettel 18', 'viettel 18 viettel 18 viettel 18 viettel 18', 'viettel 18 viettel 18 viettel 18 viettel 18', '7ZVAdJQP', '560714', '1', '1', '1', '2018-03-29 08:18:02', '2018-03-29 08:18:39', '13', '65', null, '1', null, '3', '300005', '155ab0ce7302678', '2018-03-20 09:03:47', '2', '92', '43', '2018-03-29 01:18:39', '2018-03-19 09:00:32', '1', null, null, 'C00047', null, '26');
INSERT INTO `members` VALUES ('84', 'ngày 20 test', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/20/img_1521510934_5ab06a16ac0551.58971158.png', '2013-06-07 00:00:00', '2', '134422111', null, '134422111', '2018-02-16 00:00:00', '134422111', '134422111', '134422111', '134422111', null, 'tranha084@gmail.com', null, '3', '1', '2018', '9', null, null, null, null, 'fAT6G1a5', '084472', '1', '0', '0', '2018-03-20 08:56:42', '2018-03-20 08:56:42', '1', '17', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '16');
INSERT INTO `members` VALUES ('85', 'check fdadadada', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522065180_5ab8df1ccdb287.49349005.jpeg', '2008-01-20 00:00:00', '1', '151515151', '2018-05-06 00:00:00', '1212', '2018-01-29 00:00:00', 'CA Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', null, 'tranha084@gmail.com', null, '2', '1', '2013', '11', null, 'oki', null, null, 'nD0oZFME', '700138', '1', '1', '1', '2018-03-20 09:00:31', '2018-03-28 15:45:15', '13', '1', null, null, null, '3', null, null, null, '2', '17', '43', '2018-03-20 02:09:19', '2018-03-20 02:08:59', null, null, '1', null, null, '1');
INSERT INTO `members` VALUES ('86', 'check PD_BS', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/20/img_1521516106_5ab07e4a49efc9.94556175.jpeg', '2008-06-27 00:00:00', '2', '212121212', '2018-05-06 00:00:00', '534534', '2018-02-02 00:00:00', 'Bà Rịa', 'hà nội', 'hà nội', '0977635498', null, 'ngandt5@viettel.com.vn', 'tranha084@gmail.com', '1', '1', '2014', '17', null, null, null, null, 'PMWPPGb3', '892480', '1', '1', '1', '2018-03-20 10:23:19', '2018-04-03 08:21:58', '14', '1', null, '1', null, null, null, null, null, '3', '44', '43', '2018-03-30 08:12:42', '2018-03-20 03:24:58', '2', null, null, 'C00077', null, '1');
INSERT INTO `members` VALUES ('87', 'check lặp dữ liệu_update CLB', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/20/img_1521518838_5ab088f605faf8.42492612.jpeg', '2007-03-20 00:00:00', '1', '161616616', null, '2326', '2018-02-05 00:00:00', 'Bà Rịa', 'hà nội', 'hà nội', '0977635498', null, 'ngandt5@viettel.com.vn', 'tranha084@gmail.com', '3', '2', '2014', '14', null, null, null, null, 'eAXF8X8k', '933508', '1', '1', '1', '2018-03-20 11:08:50', '2018-04-03 10:12:28', '13', '65', null, null, null, '3', '300014', '155abddb59b1bdc', '2018-03-30 06:38:17', '2', '17', '43', '2018-03-28 03:21:19', '2018-03-22 07:07:42', null, null, null, 'C00067', null, '1');
INSERT INTO `members` VALUES ('88', 'check  dữ liệu HCM', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/20/img_1521520750_5ab0906ed01de8.62584430.jpeg', '2007-03-20 00:00:00', '1', '161616617', null, '2326', '2018-02-05 00:00:00', 'Bà Rịa', 'hà nội', 'hà nội', '0977635498', null, 'ngandt5@viettel.com.vn', 'tranha084@gmail.com', '3', '1', '2014', '14', null, null, null, null, 'HqIftpuA', '511293', '1', '1', '1', '2018-03-20 11:39:31', '2018-03-20 11:39:31', '1', '2', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('89', 'check xác thực SDT', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/21/img_1521601970_5ab1cdb218cfb7.63607810.jpeg', '2012-03-28 00:00:00', '1', '345456465', '2018-04-22 00:00:00', '3232', '2018-01-31 00:00:00', 'CA Hà Nội', 'fd', 'df', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2014', '31', null, null, null, null, 'WBZoVRlj', '692209', '1', '1', '1', '2018-03-21 10:21:46', '2018-04-02 16:23:02', '9', '1', null, '1', null, '3', '200040', '155ac1f6768dc12', '2018-04-02 09:23:02', '2', '44', '43', '2018-03-30 08:13:05', '2018-03-21 03:46:52', null, null, null, 'C00078', null, '1');
INSERT INTO `members` VALUES ('90', 'check upload ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/23/img_1521773064_5ab46a08a61a33.87975399.jpeg', '2013-07-23 00:00:00', '2', '112341123', '2018-04-12 00:00:00', '112341123', '2018-02-22 00:00:00', '112341123', '112341123', '112341123', '112341123', null, 'tranha084@gmail.com', null, '1', '1', '2018', '8', null, null, null, null, 'cb2x0Npv', '799596', '1', '0', '0', '2018-03-23 09:45:30', '2018-03-23 09:45:30', '1', '18', null, null, null, null, null, null, null, '4', null, null, null, null, '2', null, null, null, null, '14');
INSERT INTO `members` VALUES ('91', 'Check anh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/23/img_1521773602_5ab46c22064906.51526330.jpeg', '2007-03-30 00:00:00', '1', '141415148', '2018-04-14 00:00:00', '141415141', '2018-02-01 00:00:00', 'CA Hà Nội', 'hà Nội', 'Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '2', '1', '2013', '31', null, null, null, null, 'D1OfqiHn', '909699', '1', '0', '0', '2018-03-23 09:53:29', '2018-03-23 09:53:37', '1', '1', null, null, null, null, null, null, null, '8', null, null, null, null, null, null, null, null, null, '41');
INSERT INTO `members` VALUES ('92', 'Ha check upload ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/23/img_1521773840_5ab46d10429491.73126883.jpeg', '2013-02-27 00:00:00', '1', '122341211', '2018-04-05 00:00:00', '122341211', '2018-02-26 00:00:00', '122341211', '122341211', '122341211', '2323232323233', null, 'tranha084@gmail.com', null, '1', '1', '2018', '11', null, null, null, null, 'MLe0pmJc', '557235', '1', '1', '1', '2018-03-23 09:59:54', '2018-04-02 17:14:11', '1', '8', null, null, null, null, null, null, null, '9', null, null, null, null, '1', null, '1', null, null, '8');
INSERT INTO `members` VALUES ('93', 'Viettel 14_outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/23/img_1521777259_5ab47a6b82da77.36810206.jpeg', '1986-03-23 00:00:00', '1', '868686868', '2018-08-26 00:00:00', '125487', '2018-02-05 00:00:00', 'CA Hà Nội_outbound', 'Hà Nội', 'Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '1', '1', '2013', '31', 'MC dẫn chương trình', 'từng đi nước ngoài', 'Hạng B', 'HDV tiêu biểu của năm', 'qHX3ApKw', '734408', '1', '1', '1', '2018-03-23 10:57:17', '2018-03-23 14:45:56', '13', '1', null, '1', null, '3', '100004', '155ab4b08de1c30', '2018-03-23 07:45:17', '2', '44', '43', '2018-03-23 04:22:56', '2018-03-23 04:11:36', '2', null, null, 'C00050', null, '1');
INSERT INTO `members` VALUES ('94', 'Viettel 15_inBound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/23/img_1521792651_5ab4b68ba553b1.86924360.jpeg', '2008-03-23 00:00:00', '2', '878787878', '2018-05-06 00:00:00', '324', '2018-01-30 00:00:00', 'CA Hà Nội_inbound', 'Hà Nội', 'Hà Nam', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '1', '1', '2013', '31', null, null, null, null, 'rqI8B9w5', '663404', '1', '1', '1', '2018-03-23 15:12:35', '2018-03-29 17:00:22', '13', '1', null, '1', null, '3', '100010', '155abcb92ba2289', '2018-03-29 10:00:11', '5', '44', '43', '2018-03-23 08:14:17', '2018-03-23 08:13:02', '1', null, null, 'C00051', null, '3');
INSERT INTO `members` VALUES ('95', 'Viettel 17_tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522028273_5ab84ef1cb4635.77182738.jpeg', '2008-03-26 00:00:00', '1', '868686867', null, '333', '2018-01-30 00:00:00', 'CA Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '3', '1', '2013', '29', null, null, null, null, 'Epf6O1kU', '510436', '1', '0', '0', '2018-03-26 08:38:00', '2018-03-26 08:40:49', '1', '1', null, null, null, null, null, null, null, '3', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('96', 'Viettel 17_tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522028273_5ab84ef1cb4635.77182738.jpeg', '2008-03-26 00:00:00', '1', '868686867', null, '333', '2018-01-30 00:00:00', 'CA Hà Nội', 'Hà Nội', 'Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '3', '1', '2013', '29', null, null, null, null, 'raby4AbF', '548667', '1', '1', '1', '2018-03-26 08:43:26', '2018-03-29 17:12:27', '13', '1', null, '1', null, '3', '300010', '155abcbc01f05d7', '2018-03-29 10:12:17', '3', '44', '43', '2018-03-26 01:44:33', '2018-03-26 01:43:50', null, null, null, 'C00052', null, '1');
INSERT INTO `members` VALUES ('97', 'check radio', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522030589_5ab857fdd941a2.99713462.jpeg', '2008-03-26 00:00:00', '1', '313133131', '2018-05-05 00:00:00', '234', '2018-01-31 00:00:00', 'CA Hà Nội', '234', 'fsdfsd', '0977635498', '022462906570', 'ngandt5@viettel.com.vn', null, '2', '1', '2014', '13', null, null, null, null, 'K4FWDRtb', '070478', '1', '0', '0', '2018-03-26 09:17:19', '2018-03-26 09:17:33', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('98', 'check radio34', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522030763_5ab858ab34d813.65221874.jpeg', '2008-02-27 00:00:00', '1', '213131313', null, '4', '2018-02-03 00:00:00', 'CA Hà Nội', '435', '345234', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '16', null, null, null, null, 'HG8e2d41', '195985', '1', '0', '0', '2018-03-26 09:20:07', '2018-03-26 09:20:37', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('99', 'In hồ sơ_outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522039975_5ab87ca70dce97.89707261.jpeg', '2008-03-26 00:00:00', '1', '134346234', '2018-04-28 00:00:00', '12', '2018-01-31 00:00:00', 'CA Hà Nội', 'Outbound', 'Hà Nội', '0977635498', '0977635498', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '1', '1', '2013', '12', null, null, null, null, '3MXBHqBo', '227414', '1', '1', '1', '2018-03-26 11:53:24', '2018-03-29 17:33:46', '13', '1', null, '1', null, '3', '100012', '155abcbe770749a', '2018-03-29 10:22:47', '2', '44', '43', '2018-03-26 04:54:44', '2018-03-26 04:54:02', '2', null, null, 'C00053', null, '1');
INSERT INTO `members` VALUES ('100', 'check_inbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522055542_5ab8b976c8e649.28057464.jpeg', '2008-03-26 00:00:00', '1', '898989898', '2018-06-10 00:00:00', '12', '2018-01-31 00:00:00', 'CA Hà Nội', 'check in hồ sơ inbound', 'check in hồ sơ inbound', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '1', '1', '2014', '11', null, null, null, null, 'C2cLx2Ke', '685658', '1', '1', '1', '2018-03-26 16:14:25', '2018-03-30 14:53:11', '13', '1', null, null, null, '3', '100017', '155abdddda2b11f', '2018-03-30 06:48:58', '2', '44', '43', '2018-03-26 09:25:37', '2018-03-26 09:19:29', '1', null, '1', 'C00057', null, '2');
INSERT INTO `members` VALUES ('101', 'check_outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522055758_5ab8ba4ea7c3c9.03627970.jpeg', '2008-03-26 00:00:00', '1', '898989899', '2018-06-10 00:00:00', '12', '2018-01-31 00:00:00', 'CA Hà Nội', 'check in hồ sơ outbound', 'check in hồ sơ outbound', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '1', '1', '2014', '11', 'Các kỹ năng chuyên môn khác', 'Thông tin khác', 'hạng Hướng dẫn viên', 'Thành tích khen thưởng', 'Nciutww1', '752804', '1', '1', '1', '2018-03-26 16:16:19', '2018-03-28 15:04:31', '5', '1', null, null, null, null, null, null, null, '2', '44', '43', '2018-03-26 09:25:24', '2018-03-26 09:19:37', '2', null, '1', 'C00056', null, '2');
INSERT INTO `members` VALUES ('102', 'check_nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522055800_5ab8ba7869c5c2.14002459.jpeg', '2008-03-26 00:00:00', '1', '898989897', '2018-06-10 00:00:00', '12', '2018-01-31 00:00:00', 'CA Hà Nội', 'check in hồ sơ nội địa', 'check in hồ sơ nội địa', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '2', '1', '2014', '11', 'Các kỹ năng chuyên môn khác', 'Thông tin khác', 'hạng Hướng dẫn viên', 'Thành tích khen thưởng', 'j3BxUy7S', '161377', '1', '1', '1', '2018-03-26 16:17:05', '2018-03-30 10:31:23', '13', '1', null, '1', null, '3', '200035', '155abdaf84b9437', '2018-03-30 03:31:16', '2', '44', '43', '2018-03-26 09:25:06', '2018-03-26 09:19:44', '2', null, null, 'C00055', null, '2');
INSERT INTO `members` VALUES ('103', 'check_tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522055848_5ab8baa8efb975.84247809.jpeg', '2008-03-26 00:00:00', '1', '898989896', '2018-06-10 00:00:00', '12', '2018-01-31 00:00:00', 'CA Hà Nội', 'check in hồ sơ tại điểm', 'check in hồ sơ tại điểm', '0977635498', '01649727674', 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '3', '1', '2014', '11', 'Các kỹ năng chuyên môn khác', 'Thông tin khác', 'hạng Hướng dẫn viên', 'Thành tích khen thưởng', 'S2r7BYEG', '920093', '1', '1', '1', '2018-03-26 16:18:38', '2018-03-29 17:33:40', '13', '1', null, '1', null, '3', '300011', '155abcc0f61b25a', '2018-03-29 10:33:26', '2', '44', '43', '2018-03-26 09:24:53', '2018-03-26 09:19:51', '2', null, null, 'C00054', null, '2');
INSERT INTO `members` VALUES ('104', 'check_ATTT', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522058580_5ab8c554add3b8.59978484.jpeg', '2009-02-27 00:00:00', '1', '898989895', '2018-05-06 00:00:00', '34534', '2018-02-02 00:00:00', 'CA Hà Nội', 'check in hồ sơ tại điểm', 'check in hồ sơ tại điểm', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '3', '1', '2014', '15', null, null, null, null, 'GgNVjIPH', '309879', '1', '1', '1', '2018-03-26 17:03:24', '2018-04-09 08:34:38', '5', '1', null, '1', null, null, null, null, null, '2', '44', '43', '2018-04-09 04:17:35', '2018-04-02 10:20:58', null, null, null, 'C00081', null, '2');
INSERT INTO `members` VALUES ('105', 'check_ATTT', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522058580_5ab8c554add3b8.59978484.jpeg', '2009-02-27 00:00:00', '1', '898989894', '2018-05-06 00:00:00', '34534', '2018-02-02 00:00:00', 'CA Hà Nội', 'check in hồ sơ tại điểm', 'check in hồ sơ tại điểm', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '3', '1', '2014', '15', null, null, null, null, 'i8gZJ0nc', '122895', '1', '1', '1', '2018-03-26 17:13:22', '2018-03-26 17:13:22', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '2');
INSERT INTO `members` VALUES ('106', 'check lại ATTT', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522059535_5ab8c90f678de5.67800777.jpeg', '2008-03-27 00:00:00', '1', '124323542', null, '3', '2018-02-01 00:00:00', 'CA Hà Nội', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2015', '7', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'wCyWNi0x', '232751', '1', '1', '1', '2018-03-26 17:20:13', '2018-03-26 17:20:13', '1', '2', null, null, null, null, null, null, null, '3', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('107', '<script> alert(\"test\") </script>', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522059637_5ab8c975ad3493.98713843.jpeg', '2012-03-26 00:00:00', '1', '000002001', '2018-04-01 00:00:00', '1', '2018-02-22 00:00:00', '1', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '11111', null, '1@gmail.com', null, '1', '1', '2018', '1', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', 'H8LcwO8A', '027436', '1', '0', '0', '2018-03-26 17:20:39', '2018-03-26 17:20:49', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '1');
INSERT INTO `members` VALUES ('108', 'check lại ATTT Hà Nội', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522059535_5ab8c90f678de5.67800777.jpeg', '2008-03-27 00:00:00', '1', '124323570', null, '3', '2018-02-01 00:00:00', 'CA Hà Nội', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2015', '7', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'OoIbIjTq', '523641', '1', '1', '1', '2018-03-26 17:21:10', '2018-03-26 17:21:10', '1', '1', null, null, null, null, null, null, null, '3', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('109', '<script> alert(\"test\") </script>', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522059637_5ab8c975ad3493.98713843.jpeg', '2012-03-26 00:00:00', '1', '000002001', '2018-04-01 00:00:00', '1', '2018-02-22 00:00:00', '1', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '0978643838', null, '1@gmail.com', null, '1', '1', '2018', '1', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', 'lcq8MlBV', '005992', '1', '1', '1', '2018-03-26 17:21:21', '2018-03-26 17:21:21', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '1');
INSERT INTO `members` VALUES ('110', 'ha check attt 1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522059447_5ab8c8b76f53e7.12672449.jpeg', '2013-07-06 00:00:00', '2', '654345675', '2018-04-04 00:00:00', '654345675', '2018-01-31 00:00:00', '654345675', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '222232323', null, 'tranha084@gmail.com', null, '1', '1', '2018', '8', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'vSP3ZBQJ', '856178', '1', '1', '1', '2018-03-26 17:23:09', '2018-03-29 10:56:14', '2', '18', null, null, null, null, null, null, null, '9', null, '17', null, '2018-03-29 03:56:14', '1', null, null, null, null, '19');
INSERT INTO `members` VALUES ('111', 'ha check script', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522059861_5ab8ca55dfeed4.88178022.png', '2013-06-21 00:00:00', '2', '552134522', null, '552134522', '2018-02-08 00:00:00', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '11111111', null, 'tranha084@gmail.com', null, '3', '1', '2014', '17', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', 'E9UekiwG', '278070', '1', '1', '1', '2018-03-26 17:25:49', '2018-03-26 17:25:49', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '17');
INSERT INTO `members` VALUES ('112', 'check lại ATTT', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/26/img_1522060029_5ab8cafd38a396.73309787.jpeg', '2008-03-26 00:00:00', '1', '894512165', null, '34534', '2018-02-10 00:00:00', 'CA Hà Nội', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '11', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'GeWu2H3S', '947038', '1', '1', '1', '2018-03-26 17:28:39', '2018-03-26 17:28:39', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('113', 'outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522113549_5ab99c0d10bd11.79695417.jpeg', '2008-03-03 00:00:00', '1', '897978978', '2018-05-06 00:00:00', '324', '2018-02-02 00:00:00', 'CA Hà Nội', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '1', '2', '2014', '14', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'lX2C7t9A', '040856', '1', '1', '1', '2018-03-27 08:21:18', '2018-03-30 14:54:05', '13', '65', null, null, null, '3', '100018', '155abdde5eb6047', '2018-03-30 06:51:10', '2', '44', '43', '2018-03-27 01:22:45', '2018-03-27 01:22:23', '2', null, '1', 'C00058', null, '1');
INSERT INTO `members` VALUES ('114', 'check in hồ sơ tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522119847_5ab9b4a7ac6573.55710639.jpeg', '2008-03-27 00:00:00', '1', '369369369', null, '369', '2018-01-29 00:00:00', 'CA Hà Nội_tại điểm', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x3C;&#x2F;&#x73;&#x63;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x3C;&#x2F;&#x73;&#x63;&#', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '3', '1', '2013', '18', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;', 'ESbA2cDh', '468000', '1', '1', '1', '2018-03-27 10:06:15', '2018-03-27 10:11:51', '4', '1', null, null, null, null, null, null, null, '2', '44', '43', '2018-03-27 03:07:27', '2018-03-27 03:06:55', null, null, '1', 'C00059', null, '1');
INSERT INTO `members` VALUES ('115', 'check đăng ký lại với mã HDV đã bị xóa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522120419_5ab9b6e344e696.35478645.jpeg', '2008-03-27 00:00:00', '1', '369369369', null, '369', '2018-01-29 00:00:00', 'CA Hà Nội_tại điểm', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', 'giangbh1@viettel.com.vn', '3', '1', '2014', '10', '<script><script>alert(\"test\")</script></script> hoặc <!--script> alert(\"test\")<!--/script>', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', '<script>alert(“XSS”)</script>', 'pplcbQpp', '714733', '1', '1', '1', '2018-03-27 10:16:02', '2018-03-30 17:13:29', '13', '1', null, '1', null, '3', '300012', '155abdb3e251790', '2018-03-30 03:49:54', '2', '44', '43', '2018-03-27 03:17:54', '2018-03-27 03:17:30', null, null, null, 'C00060', null, '3');
INSERT INTO `members` VALUES ('116', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522133930_5ab9ebaa5788c5.25299161.jpeg', '2009-03-27 00:00:00', '1', '258258258', '2018-07-03 00:00:00', '&#x3C;&#x73;&#x63;&#', '2018-02-23 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2014', '17', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'f4oaFy3D', '269027', '1', '1', '1', '2018-03-27 14:01:01', '2018-03-30 16:48:23', '13', '11', null, null, null, '3', '100013', '155abdc3d91eab9', '2018-03-30 04:58:01', '2', '44', '43', '2018-03-27 07:08:15', '2018-03-27 07:07:24', '2', null, null, 'C00061', null, '4');
INSERT INTO `members` VALUES ('117', 'check bổ sung thông tin khi bị LD từ chối PD', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522135604_5ab9f23494bac5.56776952.jpeg', '2008-03-27 00:00:00', '1', '147147147', '2018-05-06 00:00:00', 'acd', '2018-01-29 00:00:00', 'CA Hà Nội', 'Hà Nội', 'Hà Nôi', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '15', '8', '9', '10', '11', 'fvdpR7WC', '616267', '1', '1', '1', '2018-03-27 14:28:17', '2018-03-27 14:42:50', '1', '1', null, null, null, null, null, null, null, '2', '44', '43', '2018-03-27 07:39:25', '2018-03-27 07:38:50', null, null, null, null, null, '5');
INSERT INTO `members` VALUES ('118', 'đăng ký outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522137759_5ab9fa9fcce257.45374061.jpeg', '2008-01-27 00:00:00', '1', '852852852', '2018-04-22 00:00:00', 'acd', '2018-01-31 00:00:00', 'CA Hà Nội', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2014', '15', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', 'PxWEPRq3', '201004', '1', '1', '1', '2018-03-27 15:04:19', '2018-04-03 10:29:42', '13', '1', null, '1', null, '3', '100019', '155ac1f7e6a9247', '2018-04-02 09:29:10', '2', '44', '43', '2018-03-27 09:46:25', '2018-03-27 09:45:57', '2', null, null, 'C00062', null, '5');
INSERT INTO `members` VALUES ('119', 'đăng ký inbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522139753_5aba0269127eb7.08666750.jpeg', '2008-01-27 00:00:00', '1', '963963963', '2018-04-22 00:00:00', 'acd', '2018-01-31 00:00:00', 'CA Hà Nội', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2014', '15', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'LvgVCui1', '058246', '1', '1', '1', '2018-03-27 15:36:47', '2018-03-27 15:38:17', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-27 08:38:17', '1', null, null, null, null, '5');
INSERT INTO `members` VALUES ('120', 'đăng ký nội địa', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522139753_5aba0269127eb7.08666750.jpeg', '2008-01-27 00:00:00', '1', '789456123', '2018-04-22 00:00:00', 'acd', '2018-01-31 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2014', '15', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'yk5FLGgL', '961699', '1', '1', '1', '2018-03-27 15:58:44', '2018-03-27 15:59:17', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-27 08:59:17', '1', null, null, null, null, '5');
INSERT INTO `members` VALUES ('121', 'đăng ký tại điểm', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522139753_5aba0269127eb7.08666750.jpeg', '2008-01-27 00:00:00', '1', '321654987', '2018-04-22 00:00:00', 'acd', '2018-01-31 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '15', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'jiD62WbM', '244061', '1', '1', '1', '2018-03-27 16:01:05', '2018-03-27 16:01:35', '2', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-27 09:01:35', '1', null, null, null, null, '6');
INSERT INTO `members` VALUES ('122', 'đăng ký lại Outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522142782_5aba0e3e03f7f6.70033512.jpeg', '2008-01-27 00:00:00', '1', '854632145', '2018-04-22 00:00:00', 'acd', '2018-01-31 00:00:00', '<script>&#', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2014', '15', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', 'sEpAReId', '869476', '1', '1', '1', '2018-03-27 16:26:35', '2018-03-27 16:26:35', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '2', null, null, null, null, '6');
INSERT INTO `members` VALUES ('123', 'check outbound', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522144413_5aba149d259604.20521497.jpeg', '2008-03-27 00:00:00', '1', '159874632', '2018-05-06 00:00:00', '35', '2018-01-30 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2014', '9', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'frsEFUlQ', '223333', '1', '1', '1', '2018-03-27 16:57:16', '2018-03-30 16:58:36', '13', '1', null, null, null, '3', '100014', '155abddb1729178', '2018-03-30 06:37:11', '2', '44', '43', '2018-03-28 01:35:03', '2018-03-28 01:34:30', '2', null, null, 'C00063', null, '1');
INSERT INTO `members` VALUES ('124', 'hattt-đăng ký script 1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522170074_5aba78da1e5229.60135159.jpeg', '2013-06-21 00:00:00', '1', '123456765', '2018-04-04 00:00:00', '123456765', '2018-02-21 00:00:00', '123456765', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '21321321', null, 'tranha084@gmail.com', null, '1', '1', '2018', '19', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'cOucZfm7', '488495', '1', '1', '1', '2018-03-28 00:04:42', '2018-03-29 10:58:16', '7', '3', null, null, null, null, null, null, null, '4', null, '17', null, '2018-03-29 03:58:16', '2', null, null, null, null, '3');
INSERT INTO `members` VALUES ('125', 'hattt check script 2', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/27/img_1522170308_5aba79c4733170.64970502.jpeg', '2013-06-14 00:00:00', '2', '442213342', '2018-04-11 00:00:00', '442213342', '2018-02-24 00:00:00', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script><script> alert(\"test\") </script>', '<script> alert(\"test\") </script><script> alert(\"test\") </script>', '4422', null, 'tranha084@gmail.com', null, '1', '1', '2018', '17', '<script> alert(\"test\") </script><script> alert(\"test\") </script><script> alert(\"test\") </script>', '<script> alert(\"test\") </script><script> alert(\"test\") </script><script> alert(\"test\") </script>', '<script> alert(\"test\") </script><script> alert(\"test\") </script><script> alert(\"test\") </script>', '<script> alert(\"test\") </script><script> alert(\"test\") </script><script> alert(\"test\") </script>', 'zJyFfya3', '227974', '1', '1', '1', '2018-03-28 00:07:49', '2018-03-28 00:07:49', '1', '1', null, null, null, null, null, null, null, '3', null, null, null, null, '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('126', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522204555_5abaff8b4768d5.14101522.jpeg', '2008-01-27 00:00:00', '1', '159624863', '2018-05-20 00:00:00', '15', '2018-02-03 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2013', '15', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'lD1PQXup', '078295', '1', '1', '1', '2018-03-28 09:37:23', '2018-03-28 09:37:23', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '4');
INSERT INTO `members` VALUES ('127', '<script>&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522204716_5abb002c2e7801.33573484.jpeg', '2008-01-26 00:00:00', '1', '159624869', '2018-04-29 00:00:00', '<sc&#', '2018-02-03 00:00:00', '<script>&#', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2014', '15', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', 'qjDaJkqg', '627846', '1', '1', '1', '2018-03-28 09:38:57', '2018-03-28 09:38:57', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '4');
INSERT INTO `members` VALUES ('128', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522204908_5abb00ec579376.97567911.jpeg', '2009-01-13 00:00:00', '1', '987541236', null, '&#x3C;&#x73;&#x63;&#', '2018-01-29 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '17', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'TCLSCaYn', '372630', '1', '1', '1', '2018-03-28 09:42:59', '2018-03-29 11:52:52', '5', '1', null, null, null, null, null, null, null, '2', '44', '43', '2018-03-28 02:51:29', '2018-03-28 02:45:09', null, null, '1', 'C00064', null, '4');
INSERT INTO `members` VALUES ('129', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522207001_5abb0919e16bb4.40660733.jpeg', '2009-02-02 00:00:00', '1', '789654123', '2018-05-06 00:00:00', '&#x3C;&#x73;&#x63;&#', '2018-01-30 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2015', '14', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'BETQe7f7', '660865', '1', '1', '1', '2018-03-28 10:18:18', '2018-03-28 10:22:15', '1', '1', null, null, null, null, null, null, null, '2', null, '43', null, '2018-03-28 03:21:13', '2', null, null, null, null, '11');
INSERT INTO `members` VALUES ('130', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522207694_5abb0bce135fb8.05084874.jpeg', '2008-03-28 00:00:00', '1', '565656545', null, '&#x3C;&#x73;&#x63;&#', '2018-01-30 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '15', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'O1P5TNl7', '798655', '1', '1', '1', '2018-03-28 10:30:25', '2018-03-30 13:40:20', '9', '1', null, null, null, '3', '300015', '155abddbd424fda', '2018-03-30 06:40:20', '2', '44', '43', '2018-03-28 03:47:14', '2018-03-28 03:44:33', null, null, null, 'C00069', null, '1');
INSERT INTO `members` VALUES ('131', 'check in hồ sơ của hội viên chính thức', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522224700_5abb4e3c07d872.81921245.jpeg', '2009-02-27 00:00:00', '1', '365897458', '2018-04-21 00:00:00', '342', '2018-01-31 00:00:00', 'CA Hà Nội', 'check in hồ sơ của hội viên chính thức', 'check in hồ sơ của hội viên chính thức', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '10', 'check in hồ sơ của hội viên chính thức', 'check in hồ sơ của hội viên chính thức', 'check in hồ sơ của hội viên chính thức', 'check in hồ sơ của hội viên chính thức', 'WodpkQRD', '241654', '1', '1', '1', '2018-03-28 15:12:24', '2018-03-30 10:20:21', '13', '1', null, '1', null, '3', '200027', '155abb4f97ba356', '2018-03-28 08:17:27', '2', '44', '43', '2018-03-28 08:14:20', '2018-03-28 08:13:52', null, null, null, 'C00070', null, '11');
INSERT INTO `members` VALUES ('132', 'đăng ký lại1', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522227669_5abb59d5210218.83275092.jpeg', '2008-06-27 00:00:00', '1', '789547852', '2018-05-06 00:00:00', '34', '2018-02-11 00:00:00', 'đăng ký lại1', 'đăng ký lại1', 'đăng ký lại1', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2014', '11', 'đăng ký lại1', 'đăng ký lại1', 'đăng ký lại1', 'đăng ký lại1', 'oGGpkyQW', '945987', '1', '1', '1', '2018-03-28 16:07:51', '2018-03-30 10:20:00', '13', '1', null, '1', null, '3', '200028', '155abb5aacaac5f', '2018-03-28 09:04:44', '2', '92', '43', '2018-03-28 09:09:46', '2018-03-28 09:03:08', null, null, null, 'C00071', null, '5');
INSERT INTO `members` VALUES ('133', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522228354_5abb5c828ade79.21949849.jpeg', '2008-01-27 00:00:00', '2', '010230120', '2018-06-10 00:00:00', '345', '2018-01-30 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2014', '10', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'N4fvsLKH', '576537', '1', '1', '1', '2018-03-28 16:14:01', '2018-04-03 10:10:00', '13', '1', null, '1', null, '3', '200039', '155ac1f65902ffe', '2018-04-02 09:22:33', '2', '44', '43', '2018-03-28 09:14:43', '2018-03-28 09:14:26', null, null, null, 'C00072', null, '1');
INSERT INTO `members` VALUES ('134', 'in hội viên => ảnh con thu', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522234288_5abb73b0765374.26968664.jpeg', '2008-03-04 00:00:00', '1', '030303030', '2018-05-27 00:00:00', '0303', '2018-01-30 00:00:00', 'Hà Nội', 'hà Nội', 'Hà Nội', '0977635498', null, 'tranha084@gmail.com', null, '1', '1', '2013', '31', 'chuyên môn', 'thông tin khác', 'Hạng A', 'Thành tích: giấy khen', 'pLWkgBWG', '074428', '1', '1', '1', '2018-03-28 17:51:33', '2018-03-29 11:01:01', '10', '1', null, '1', null, '3', '100005', '155abb60bc94fac', '2018-03-28 09:30:36', '2', '17', '43', '2018-03-28 10:52:58', '2018-03-28 09:28:58', '1', null, null, 'C00073', null, '1');
INSERT INTO `members` VALUES ('135', '<script> alert(\"test\") </script>', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522232827_5abb6dfb5c7480.79449206.jpeg', '2013-03-28 00:00:00', '2', '211444221', '2018-04-10 00:00:00', '211444221', '2018-02-07 00:00:00', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '<script> alert(\"test\") </script>', '1111', null, 'tranha084@gmail.com', null, '1', '1', '2018', '7', null, null, null, null, '2ULRasvq', '752911', '1', '1', '1', '2018-03-28 17:29:31', '2018-03-28 17:29:31', '1', '13', null, null, null, null, null, null, null, '7', null, null, null, null, '1', null, null, null, null, '17');
INSERT INTO `members` VALUES ('136', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522233182_5abb6f5e8306f5.07090865.jpeg', '2008-01-27 00:00:00', '1', '435231253', null, '34534', '2018-01-31 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '10', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'Nc1jkPNY', '677976', '1', '1', '1', '2018-03-28 17:33:40', '2018-03-29 23:49:17', '2', '1', null, null, null, null, null, null, null, '2', null, '10', null, '2018-03-29 16:49:17', null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('137', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/28/img_1522233220_5abb6f842ce1f4.48042420.jpeg', '2013-06-14 00:00:00', '2', '424242323', null, '424242323', '2018-02-22 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '3333333', null, 'tranha084@gmail.com', null, '3', '1', '2018', '18', null, null, null, null, '6apd9ZLo', '990234', '1', '1', '1', '2018-03-28 17:35:18', '2018-03-28 17:35:18', '1', '18', null, null, null, null, null, null, null, '3', null, null, null, null, null, null, null, null, null, '17');
INSERT INTO `members` VALUES ('138', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/29/img_1522291965_5abc54fd78b865.50292075.jpeg', '2010-01-27 00:00:00', '1', '020202020', '2018-06-03 00:00:00', '&#x3C;&#x73;&#x63;&#', '2018-01-31 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '15', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'XzvQhxEf', '793243', '1', '0', '0', '2018-03-29 09:53:54', '2018-03-29 09:57:59', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '18');
INSERT INTO `members` VALUES ('139', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/29/img_1522292960_5abc58e0a8c242.77131384.jpeg', '2008-01-27 00:00:00', '2', '010101010', null, '01', '2018-01-29 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '15', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'Kn52isA9', '646318', '1', '0', '0', '2018-03-29 10:10:07', '2018-03-29 10:10:10', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('140', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/29/img_1522293156_5abc59a4106c58.89624530.jpeg', '2008-01-27 00:00:00', '1', '010101010', null, '02', '2018-02-02 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2015', '5', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'dWOjxy14', '451906', '1', '1', '1', '2018-03-29 10:13:31', '2018-04-05 04:03:15', '10', '1', null, null, null, '3', '300013', '155abdc36a65b3d', '2018-03-30 04:56:10', '3', '44', '43', '2018-03-29 08:22:57', '2018-03-29 03:46:27', null, null, null, 'C00074', null, '1');
INSERT INTO `members` VALUES ('141', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522373996_5abd956c8daff1.80715444.jpeg', '2008-01-26 00:00:00', '2', '040404040', '2018-04-28 00:00:00', '&#x3C;&#x73;&#x63;&#', '2018-01-31 00:00:00', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '0977635498', null, 'ngandt5@viettel.com.vn', null, '1', '1', '2013', '15', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '9fBzjE5T', '743348', '1', '0', '0', '2018-03-30 08:39:58', '2018-03-30 08:40:17', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '3');
INSERT INTO `members` VALUES ('142', '135135135', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522382162_5abdb552c0c121.79618858.jpeg', '2008-03-27 00:00:00', '1', '135135138', null, '345ê', '2018-01-31 00:00:00', 'CA Hà Nội', '135135135', '135135135', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '16', null, null, null, null, '49OirVdS', '549506', '1', '1', '1', '2018-03-30 10:57:37', '2018-03-30 10:57:37', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('143', '135135135', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522382341_5abdb60580f818.74758504.jpeg', '2008-03-28 00:00:00', '1', '135135139', null, '135135135', '2018-01-31 00:00:00', '135135135', '135135135', '135135135', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '15', null, null, null, null, 'cM9BoObH', '575385', '1', '1', '1', '2018-03-30 11:00:43', '2018-03-30 11:00:43', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('144', '234', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522393908_5abde334741132.74119549.jpeg', '2013-03-30 00:00:00', '1', '100000001', '2018-04-05 00:00:00', '423', '2018-02-07 00:00:00', '423', '234', '234', '345345', null, '23@gmail.com', null, '2', '1', '2018', '17', null, null, null, null, '2kLykO3r', '941003', '1', '0', '0', '2018-03-30 14:12:02', '2018-03-30 14:12:05', '1', '16', null, null, null, null, null, null, null, '3', null, null, null, null, null, null, null, null, null, '15');
INSERT INTO `members` VALUES ('145', '6456', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522399842_5abdfa62971df0.26402089.jpeg', '2013-03-30 00:00:00', '1', '456456666', '2018-04-05 00:00:00', '456', '2018-02-26 00:00:00', '45', '456', '456', '456456', null, '456@gmail.com', null, '1', '1', '2018', '15', null, null, null, null, 'CYgchM5P', '177107', '1', '0', '0', '2018-03-30 15:50:44', '2018-03-30 15:50:44', '1', '11', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '11');
INSERT INTO `members` VALUES ('146', 'check ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522403079_5abe0707e329c0.08727869.jpeg', '2008-03-30 00:00:00', '1', '050505050', null, '5654654', '2018-01-31 00:00:00', 'CA Hà Nội_inbound', 'CA Hà Nội_inbound', 'CA Hà Nội_inbound', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '11', null, null, null, null, 'JTqCJGtf', '932161', '1', '0', '0', '2018-03-30 16:47:34', '2018-03-30 16:47:36', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('147', 'anh huy', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522427911_5abe68078fdb30.18446847.jpeg', '2013-03-30 00:00:00', '1', '156156151', '2018-04-06 00:00:00', '111111111', '2018-02-26 00:00:00', 'Ha Noi', 'Ha Noi', 'Ha Noi', '01648888888', null, 'test@gmail.com', null, '1', '1', '2018', '1', null, null, null, null, 'HqbhXiky', '143845', '1', '0', '0', '2018-03-30 23:38:33', '2018-03-30 23:39:20', '1', '14', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '14');
INSERT INTO `members` VALUES ('148', 'anh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522427978_5abe684a266814.44119901.jpeg', '2013-03-30 00:00:00', '1', '156156166', '2018-04-06 00:00:00', '111111111', '2018-02-26 00:00:00', 'Ha Noi', 'Ha Noi', 'Ha Noi', '01648887798', null, 'test@gmail.com', null, '1', '1', '2018', '1', null, null, null, null, 'nCaOg5xw', '537533', '1', '0', '0', '2018-03-30 23:39:40', '2018-03-30 23:52:24', '1', '14', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '14');
INSERT INTO `members` VALUES ('149', 'ignanh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/03/30/img_1522428763_5abe6b5b502b77.12938141.jpeg', '2013-03-30 00:00:00', '1', '156156155', '2018-04-06 00:00:00', '111111111', '2018-02-26 00:00:00', 'Ha Noi', 'Ha Noi', 'Ha Noi', '01648887798', null, 'test@gmail.com', null, '1', '1', '2018', '1', null, null, null, null, 'E0A6eBTw', '098589', '1', '0', '0', '2018-03-30 23:52:48', '2018-03-30 23:54:17', '1', '14', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '14');
INSERT INTO `members` VALUES ('150', 'check trạng thái', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522634215_5ac18de7061112.52958199.jpeg', '2008-04-02 00:00:00', '1', '070707070', null, '023235', '2018-02-26 00:00:00', 'CA Hà Nội', 'Hà Nam', 'nam Định', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2014', '31', null, null, null, null, '7yJeSV5h', '308382', '1', '1', '1', '2018-04-02 08:58:16', '2018-04-02 11:28:53', '22', '1', null, null, null, null, null, null, null, '2', '44', '43', '2018-04-02 04:28:53', '2018-04-02 02:29:26', null, null, null, null, '8', '41');
INSERT INTO `members` VALUES ('151', 'check status', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522637910_5ac19c562f7835.12655116.jpeg', '2008-04-09 00:00:00', '1', '080808080', null, '08', '2018-02-26 00:00:00', 'CA Hà Nội', 'Hà Nam', 'Nam Định', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '14', null, null, null, null, 'aOhikBA4', '354536', '1', '1', '1', '2018-04-02 09:59:52', '2018-04-02 11:41:23', '14', '1', null, null, null, null, null, null, null, '2', '44', '59', '2018-04-02 03:10:59', '2018-04-02 03:06:42', null, null, null, 'C00079', '4', '41');
INSERT INTO `members` VALUES ('152', 'ko nhập ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522653230_5ac1d82e4ecfe3.57095945.jpeg', '2008-04-02 00:00:00', '1', '090909090', null, '4234', '2018-02-28 00:00:00', 'CA Hà Nội', 'Hà Nam', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '15', null, null, null, null, 'IxreB6cu', '368941', '1', '0', '0', '2018-04-02 14:13:53', '2018-04-02 14:14:58', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('153', 'ko nhập ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522653318_5ac1d88604e9c6.76036776.jpeg', '2008-04-02 00:00:00', '1', '090909090', null, '4234', '2018-02-28 00:00:00', 'CA Hà Nội', 'Hà Nam', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '15', null, null, null, null, 'DeZ71Lnz', '224641', '1', '0', '0', '2018-04-02 14:15:20', '2018-04-02 14:25:52', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('154', 'ko nhập ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522653990_5ac1db26081b71.77709848.jpeg', '2008-04-02 00:00:00', '1', '070707071', null, '4234', '2018-02-28 00:00:00', 'CA Hà Nội', 'Hà Nam', 'Hà Nội', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '15', null, null, null, null, 'LutBlnPh', '454642', '1', '0', '0', '2018-04-02 14:26:32', '2018-04-02 14:27:37', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('155', 'ko nhập ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522656174_5ac1e3aee9abf0.98574137.jpeg', '2010-01-27 00:00:00', '1', '090909090', null, 'rt', '2018-02-02 00:00:00', 'CA Hà Nội', 'Đỗ Minh Hải', 'Đỗ Minh Hải', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '14', null, null, null, null, 'Wf4BnnrE', '105774', '1', '0', '0', '2018-04-02 15:03:40', '2018-04-02 15:04:12', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('156', 'ko nhập ảnh', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522656296_5ac1e4280b3dd8.32443708.jpeg', '2010-01-27 00:00:00', '1', '080808081', null, 'rt', '2018-02-02 00:00:00', 'CA Hà Nội', 'Đỗ Minh Hải', 'Đỗ Minh Hải', '0977635498', null, 'ngandt5@viettel.com.vn', null, '3', '1', '2013', '14', null, null, null, null, 'eDoga6rM', '265669', '1', '0', '0', '2018-04-02 15:04:58', '2018-04-02 15:04:58', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `members` VALUES ('157', 'Duong Dai Dao', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522657317_5ac1e825120bf1.05097871.jpeg', '2008-01-27 00:00:00', '1', '987456321', '2018-05-01 00:00:00', '1234567899', '2018-02-28 00:00:00', 'CA HN', 'aaaaa', 'Ha Noi\r\nHa Noi', '0988023924', null, 'daiduong47@gmail.com', 'daiduong47@gmail.com', '1', '1', '2016', '7', null, null, null, null, 'GsQleaA0', '304099', '1', '0', '0', '2018-04-02 15:22:09', '2018-04-02 15:22:12', '1', '14', null, null, null, null, null, null, null, '2', null, null, null, null, '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('158', 'Duong Dai Dao', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522660626_5ac1f512504b06.64384524.jpeg', '2008-01-26 00:00:00', '1', '987456321', '2018-05-01 00:00:00', '1234567899', '2018-02-26 00:00:00', 'CA HN', 'qaaaaaaaa', 'Ha Noi\r\nHa Noi', '0988023924', null, 'daiduong47@gmail.com', 'daiduong47@gmail.com', '1', '1', '2013', '13', null, null, null, null, 'gcoLrErH', '540220', '1', '0', '0', '2018-04-02 16:17:49', '2018-04-02 16:17:53', '1', '14', null, null, null, null, null, null, null, '3', null, null, null, null, '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('159', 'Duong Dai Dao', 'http://demo.hoihuongdanvien.vn/files/collection/2018/04/02/img_1522660683_5ac1f54b5110b5.96661630.jpeg', '2008-01-26 00:00:00', '1', '987456321', '2018-05-01 00:00:00', '1234567899', '2018-02-26 00:00:00', 'CA HN', 'qaaaaaaaa', 'Ha Noi\r\nHa Noi', '0988023924', null, 'daiduong47@gmail.com', 'daiduong47@gmail.com', '1', '1', '2013', '13', null, null, null, null, '5WQfnOYb', '610646', '1', '0', '0', '2018-04-02 16:18:05', '2018-04-02 16:18:39', '1', '14', null, null, null, null, null, null, null, '3', null, null, null, null, '2', null, null, null, null, '1');
INSERT INTO `members` VALUES ('160', 'chuyển server', 'http://dev.hoihuongdanvien.vn/files/collection/2018/04/04/img_1522804930_5ac428c20a4d43.04729902.jpeg', '2008-04-04 00:00:00', '1', '121010101', '2018-06-10 00:00:00', '78978', '2018-02-27 00:00:00', 'CA Hà Nội', 'Hà Nội', 'hà Nam', '0977635498', '0', 'ngandt5@viettel.com.vn', null, '2', '1', '2013', '10', null, null, null, null, 'a8sFXXZu', '058092', '1', '1', '1', '2018-04-04 01:25:26', '2018-04-04 01:35:28', '9', '1', null, '1', null, '3', '200041', '155ac42be0b844d', '2018-04-04 01:35:28', '2', '44', '43', '2018-04-04 01:33:12', '2018-04-04 01:32:45', null, null, null, 'C00080', null, '1');
INSERT INTO `members` VALUES ('161', '11111', 'http://dev.hoihuongdanvien.vn/files/collection/2018/04/05/img_1522894997_5ac5889515f3a7.76522798.png', '2013-06-05 00:00:00', '2', '113332131', '2018-05-11 00:00:00', '113332131', '2018-03-28 00:00:00', '113332131', '113332131', '113332131', '113332131', null, 'tranha084@gmail.com', 'tranha084@gmail.com', '1', '1', '2018', '18', null, null, null, null, 'pFE6MO1A', '027111', '1', '1', '1', '2018-04-05 02:25:14', '2018-04-09 08:34:52', '5', '1', null, '1', null, null, null, null, null, '3', '44', '43', '2018-04-09 04:53:03', '2018-04-09 04:46:58', '1', null, null, 'C00082', null, '18');
INSERT INTO `members` VALUES ('162', 'NguyenNgoc', 'http://dev.hoihuongdanvien.vn/files/collection/2018/04/07/img_1523086780_5ac875bcc04472.57042813.jpeg', '2013-06-07 00:00:00', '2', '123456780', '2019-02-03 00:00:00', '123456789', '2018-02-26 00:00:00', 'Ha Noi', 'Ba Dinh - Ha Noi', 'Nguyen Hong, Ba Dinh, Ha Noi', '01216344683', null, 'ngocnguen@gmail.com', null, '1', '1', '2017', '2', 'Choi game rat tot', 'Yeu the thao', 'A', 'Bang khen quoc te', 'pmkLfRqY', '051393', '1', '0', '0', '2018-04-07 07:40:07', '2018-04-07 07:40:07', '1', '1', null, null, null, null, null, null, null, '2', null, null, null, null, '1', null, null, null, null, '1');

-- ----------------------------
-- Table structure for members_tmp
-- ----------------------------
DROP TABLE IF EXISTS `members_tmp`;
CREATE TABLE `members_tmp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of members_tmp
-- ----------------------------
INSERT INTO `members_tmp` VALUES ('61', '134', '{\"fullName\":\"in h\\u1ed9i vi\\u00ean => \\u1ea3nh con thu => bong hoa\",\"gender\":\"1\",\"birthday\":\"2008-03-04\",\"touristGuideCode\":\"030303030\",\"expirationDate\":\"2018-05-27\",\"cmtCccd\":\"0303\",\"dateIssued\":\"2018-01-30\",\"issuedBy\":\"H\\u00e0 N\\u1ed9i\",\"permanentAddress\":\"h\\u00e0 N\\u1ed9i\",\"address\":\"H\\u00e0 N\\u1ed9i\",\"firstMobile\":\"0977635498\",\"secondMobile\":null,\"firstEmail\":\"tranha084@gmail.com\",\"secondEmail\":null,\"typeOfTravelGuide\":\"1\",\"typeOfPlace\":1,\"experienceYear\":\"2013\",\"inboundOutbound\":\"1\",\"experienceLevel\":\"31\",\"otherSkills\":\"chuy\\u00ean m\\u00f4n\",\"otherInformation\":\"th\\u00f4ng tin kh\\u00e1c\",\"touristGuideLevel\":\"H\\u1ea1ng A\",\"achievements\":\"Th\\u00e0nh t\\u00edch: gi\\u1ea5y khen\",\"status\":1,\"province_code\":\"1\",\"is_verified\":null,\"is_fee\":null,\"is_signed\":null,\"member_type\":null,\"guideLanguage\":\"2\",\"educations\":[{\"branchId\":\"1\",\"degreeId\":\"2\",\"file\":0}],\"majorSkills\":{\"ids\":\"4\",\"files\":[]},\"languageSkills\":[{\"languageId\":\"2\",\"levelId\":\"2\",\"file\":null}],\"elements\":[{\"groupSizeId\":\"5\",\"typeGuideId\":\"5\"}],\"workHistory\":[{\"elementName\":\"Viettrantour\",\"typeOfContract\":\"1\",\"fromDate\":\"2013-01-01 00:00:00\",\"toDate\":\"2016-01-01 00:00:00\"}],\"avatar\":\"http:\\/\\/demo.hoihuongdanvien.vn\\/files\\/collection\\/2018\\/03\\/29\\/img_1522296058_5abc64fa624b21.92995182.jpeg\",\"achievementFiles\":[],\"province\":\"1\",\"member_id\":\"134\"}', '2018-03-29 11:01:01', '2018-03-29 11:01:01');
INSERT INTO `members_tmp` VALUES ('66', '59', '{\"fullName\":\"Viettel 9_ng\\u00e2n update \\u1ea3nh kh\\u00e1c\",\"gender\":\"1\",\"birthday\":\"2013-03-23\",\"touristGuideCode\":\"232332243\",\"expirationDate\":\"2018-04-05\",\"cmtCccd\":\"65473\",\"dateIssued\":\"2018-02-01\",\"issuedBy\":\"H\\u00e0 N\\u1ed9i\",\"permanentAddress\":\"<script> alert(\\\"test\\\") <\\/script>\",\"address\":\"<script> alert(\\\"test\\\") <\\/script>\",\"firstMobile\":\"0977635498\",\"secondMobile\":null,\"firstEmail\":\"ngandt5@viettel.com.vn\",\"secondEmail\":\"giangbh1@viettel.com.vn\",\"typeOfTravelGuide\":\"2\",\"typeOfPlace\":2,\"experienceYear\":\"2015\",\"inboundOutbound\":null,\"experienceLevel\":\"31\",\"otherSkills\":\"<script> alert(\\\"test\\\") <\\/script>\",\"otherInformation\":\"<script> alert(\\\"test\\\") <\\/script>\",\"touristGuideLevel\":\"<script> alert(\\\"test\\\") <\\/script>\",\"achievements\":\"<script> alert(\\\"test\\\") <\\/script>\",\"status\":1,\"province_code\":\"65\",\"is_verified\":null,\"is_fee\":null,\"is_signed\":null,\"member_type\":null,\"guideLanguage\":\"2\",\"educations\":[{\"branchId\":\"1\",\"degreeId\":\"2\",\"file\":0}],\"majorSkills\":{\"ids\":\"4\",\"files\":[]},\"languageSkills\":[[]],\"elements\":[{\"groupSizeId\":\"4\",\"typeGuideId\":\"7\"}],\"workHistory\":[{\"elementName\":\"<script> alert(\\\"&#x7\",\"typeOfContract\":\"1\",\"fromDate\":\"2014-01-01 00:00:00\",\"toDate\":\"2018-01-01 00:00:00\"}],\"avatar\":\"http:\\/\\/demo.hoihuongdanvien.vn\\/files\\/collection\\/2018\\/03\\/30\\/img_1522401811_5abe021386d353.64414278.jpeg\",\"achievementFiles\":[],\"province\":\"1\",\"member_id\":\"59\"}', '2018-03-30 16:46:44', '2018-03-30 16:46:44');
INSERT INTO `members_tmp` VALUES ('67', '140', '{\"fullName\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#\",\"gender\":\"1\",\"birthday\":\"2008-01-27\",\"touristGuideCode\":\"010101010\",\"expirationDate\":\"2018-05-11\",\"cmtCccd\":\"02\",\"dateIssued\":\"2018-02-02\",\"issuedBy\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#\",\"permanentAddress\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;\",\"address\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;\",\"firstMobile\":\"0977635498\",\"secondMobile\":null,\"firstEmail\":\"ngandt5@viettel.com.vn\",\"secondEmail\":null,\"typeOfTravelGuide\":\"1\",\"typeOfPlace\":1,\"experienceYear\":\"2015\",\"inboundOutbound\":\"1\",\"experienceLevel\":\"5\",\"otherSkills\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;\",\"otherInformation\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;\",\"touristGuideLevel\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;\",\"achievements\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;\",\"status\":1,\"province_code\":\"1\",\"is_verified\":null,\"is_fee\":null,\"is_signed\":null,\"member_type\":null,\"guideLanguage\":\"3\",\"educations\":[{\"branchId\":\"1\",\"degreeId\":\"3\",\"file\":0}],\"majorSkills\":{\"ids\":\"4\",\"files\":[]},\"languageSkills\":[{\"languageId\":\"5\",\"levelId\":\"7\",\"file\":null}],\"elements\":[{\"groupSizeId\":\"2\",\"typeGuideId\":\"5\"}],\"workHistory\":[{\"elementName\":\"&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7\",\"typeOfContract\":\"1\",\"fromDate\":\"2013-01-01 00:00:00\",\"toDate\":\"2014-01-01 00:00:00\"}],\"avatar\":\"http:\\/\\/demo.hoihuongdanvien.vn\\/files\\/collection\\/2018\\/03\\/29\\/img_1522293156_5abc59a4106c58.89624530.jpeg\",\"achievementFiles\":[],\"province\":\"1\",\"member_id\":\"140\"}', '2018-04-05 04:03:15', '2018-04-05 04:03:15');

-- ----------------------------
-- Table structure for members_tmp_bk
-- ----------------------------
DROP TABLE IF EXISTS `members_tmp_bk`;
CREATE TABLE `members_tmp_bk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `fullName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `touristGuideCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `cmtCccd` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateIssued` datetime DEFAULT NULL,
  `issuedBy` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanentAddress` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstMobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondMobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeOfTravelGuide` tinyint(4) DEFAULT NULL,
  `typeOfPlace` tinyint(4) DEFAULT NULL,
  `experienceYear` int(11) DEFAULT NULL,
  `experienceLevel` tinyint(4) DEFAULT NULL,
  `otherSkills` text COLLATE utf8mb4_unicode_ci,
  `otherInformation` text COLLATE utf8mb4_unicode_ci,
  `touristGuideLevel` text COLLATE utf8mb4_unicode_ci,
  `achievements` text COLLATE utf8mb4_unicode_ci,
  `emailToken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `province_code` tinyint(4) NOT NULL,
  `guideLanguage` tinyint(4) DEFAULT NULL,
  `inboundOutbound` tinyint(4) DEFAULT NULL,
  `province` int(4) DEFAULT NULL,
  `branchId_tmp` int(10) DEFAULT NULL,
  `degreeId_tmp` int(10) DEFAULT NULL,
  `majorId_tmp` int(10) DEFAULT NULL,
  `languageId_tmp` int(10) DEFAULT NULL,
  `levelId_tmp` int(10) DEFAULT NULL,
  `elementId_tmp` int(10) DEFAULT NULL,
  `typeGuideId_tmp` int(10) DEFAULT NULL,
  `groupSizeId_tmp` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of members_tmp_bk
-- ----------------------------

-- ----------------------------
-- Table structure for member_decision_file
-- ----------------------------
DROP TABLE IF EXISTS `member_decision_file`;
CREATE TABLE `member_decision_file` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `number_decisive` varchar(20) DEFAULT NULL,
  `leaderId` varchar(191) CHARACTER SET utf8mb4 DEFAULT NULL,
  `sign_date` date DEFAULT NULL,
  `fileId` int(10) DEFAULT NULL,
  `is_print` tinyint(4) DEFAULT NULL,
  `is_signed` tinyint(4) DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleteAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`,`memberId`),
  KEY `memberId` (`memberId`),
  CONSTRAINT `member_decision_file_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member_decision_file
-- ----------------------------
INSERT INTO `member_decision_file` VALUES ('5', '3', '123', '3', '2018-03-13', '210', '1', '1', '2018-03-13 11:21:59', '2018-03-13 11:21:59', '2018-03-13 11:21:59', '0');
INSERT INTO `member_decision_file` VALUES ('7', '6', '123', '3', '2018-03-13', '213', '1', '1', '2018-03-13 11:34:07', '2018-03-13 11:34:07', '2018-03-13 11:34:07', '0');
INSERT INTO `member_decision_file` VALUES ('8', '4', '123', '3', '2018-03-13', '212', '1', '1', '2018-03-13 11:33:53', '2018-03-13 11:33:53', '2018-03-13 11:33:53', '0');
INSERT INTO `member_decision_file` VALUES ('9', '2', '123', '3', '2018-03-13', '223', '1', '1', '2018-03-13 14:34:51', '2018-03-13 14:34:51', '2018-03-13 14:34:51', '0');
INSERT INTO `member_decision_file` VALUES ('10', '1', 'www', '3', '2018-03-15', '308', '1', '1', '2018-03-15 17:11:52', '2018-03-15 17:11:52', '2018-03-15 17:11:52', '0');
INSERT INTO `member_decision_file` VALUES ('11', '7', 'ngan', '3', '2018-03-28', '498', '1', '1', '2018-03-28 14:39:10', '2018-03-28 14:39:10', '2018-03-28 14:39:10', '0');
INSERT INTO `member_decision_file` VALUES ('12', '8', '123', '4', '2018-03-13', '225', '1', '1', '2018-03-13 14:35:42', '2018-03-13 14:35:42', '2018-03-13 14:35:42', '0');
INSERT INTO `member_decision_file` VALUES ('13', '9', '11212', '3', '2018-03-31', '537', '1', '1', '2018-03-30 13:45:26', '2018-03-30 13:45:26', '2018-03-30 13:45:26', '0');
INSERT INTO `member_decision_file` VALUES ('14', '10', '!#@#$#%$^%&^*&(*)(__', '3', '2018-03-29', '521', '1', '1', '2018-03-29 09:12:00', '2018-03-29 09:12:00', '2018-03-29 09:12:00', '0');
INSERT INTO `member_decision_file` VALUES ('15', '12', '123', '3', '2018-03-06', '228', '1', '1', '2018-03-13 14:36:22', '2018-03-13 14:36:22', '2018-03-13 14:36:22', '0');
INSERT INTO `member_decision_file` VALUES ('16', '13', '1', '3', '2018-03-13', '229', '1', '1', '2018-03-13 14:36:45', '2018-03-13 14:36:45', '2018-03-13 14:36:45', '0');
INSERT INTO `member_decision_file` VALUES ('17', '14', '123', '3', '2018-03-13', '230', '1', '1', '2018-03-13 14:49:11', '2018-03-13 14:49:11', '2018-03-13 14:49:11', '0');
INSERT INTO `member_decision_file` VALUES ('18', '5', '45', '4', '2018-03-21', '393', '1', '1', '2018-03-21 10:50:19', '2018-03-21 10:50:19', '2018-03-21 10:50:19', '0');
INSERT INTO `member_decision_file` VALUES ('19', '15', '123aa', '4', '2018-03-14', '259', '1', '1', '2018-03-14 14:54:30', '2018-03-14 14:54:30', '2018-03-14 14:54:30', '0');
INSERT INTO `member_decision_file` VALUES ('20', '17', '21', '4', '2018-03-14', '236', '1', '1', '2018-03-14 10:31:45', '2018-03-14 10:31:45', '2018-03-14 10:31:45', '0');
INSERT INTO `member_decision_file` VALUES ('24', '34', '888888', '4', '2018-03-14', '261', '1', '1', '2018-03-14 15:07:36', '2018-03-14 15:07:36', '2018-03-14 15:07:36', '0');
INSERT INTO `member_decision_file` VALUES ('25', '35', '123456', '4', '2018-03-14', '262', '1', '1', '2018-03-14 15:13:48', '2018-03-14 15:13:48', '2018-03-14 15:13:48', '0');
INSERT INTO `member_decision_file` VALUES ('26', '28', 'sdsad', '3', '2018-03-15', '294', '1', '1', '2018-03-15 16:11:35', '2018-03-15 16:11:35', '2018-03-15 16:11:35', '0');
INSERT INTO `member_decision_file` VALUES ('27', '27', 'sadasd', '3', '2018-03-15', '297', '1', '1', '2018-03-15 16:13:54', '2018-03-15 16:13:54', '2018-03-15 16:13:54', '0');
INSERT INTO `member_decision_file` VALUES ('29', '57', '123', '3', '2018-02-26', '304', '1', '1', '2018-03-15 16:47:05', '2018-03-15 16:47:05', '2018-03-15 16:47:05', '0');
INSERT INTO `member_decision_file` VALUES ('30', '18', '123', '3', '2018-03-15', '306', '1', '1', '2018-03-15 17:11:27', '2018-03-15 17:11:27', '2018-03-15 17:11:27', '0');
INSERT INTO `member_decision_file` VALUES ('31', '22', 'sad', '3', '2018-03-15', '305', '1', '1', '2018-03-15 17:09:40', '2018-03-15 17:09:40', '2018-03-15 17:09:40', '0');
INSERT INTO `member_decision_file` VALUES ('32', '36', 'dsfds', '3', '2018-03-15', '307', '1', '1', '2018-03-15 17:11:39', '2018-03-15 17:11:39', '2018-03-15 17:11:39', '0');
INSERT INTO `member_decision_file` VALUES ('33', '33', '234', '4', '2018-03-15', '309', '1', '1', '2018-03-15 17:12:18', '2018-03-15 17:12:18', '2018-03-15 17:12:18', '0');
INSERT INTO `member_decision_file` VALUES ('34', '24', '123', '3', '2018-03-15', '310', '1', '1', '2018-03-15 17:12:29', '2018-03-15 17:12:29', '2018-03-15 17:12:29', '0');
INSERT INTO `member_decision_file` VALUES ('35', '23', '345', '3', '2018-03-15', '311', '1', '1', '2018-03-15 17:12:40', '2018-03-15 17:12:40', '2018-03-15 17:12:40', '0');
INSERT INTO `member_decision_file` VALUES ('36', '59', 'Q157', '4', '2018-03-16', '317', '1', '1', '2018-03-16 09:56:09', '2018-03-16 09:56:09', '2018-03-16 09:56:09', '0');
INSERT INTO `member_decision_file` VALUES ('38', '37', '21212', '3', '2018-03-19', '409', '1', '1', '2018-03-24 23:40:29', '2018-03-24 23:40:29', '2018-03-24 23:40:29', '0');
INSERT INTO `member_decision_file` VALUES ('39', '31', 'dfds', '3', '2018-03-16', '346', '1', '1', '2018-03-16 14:55:49', '2018-03-16 14:55:49', '2018-03-16 14:55:49', '0');
INSERT INTO `member_decision_file` VALUES ('41', '46', '14654-457/454*4/45', '3', '2018-03-26', '421', '1', '1', '2018-03-26 10:31:13', '2018-03-26 10:31:13', '2018-03-26 10:31:13', '0');
INSERT INTO `member_decision_file` VALUES ('42', '38', '@$#^&%&*', '3', '2018-03-12', '422', '1', '1', '2018-03-26 10:31:31', '2018-03-26 10:31:31', '2018-03-26 10:31:31', '0');
INSERT INTO `member_decision_file` VALUES ('43', '29', '3', '3', '2018-03-21', '396', '1', '1', '2018-03-21 14:14:26', '2018-03-21 14:14:26', '2018-03-21 14:14:26', '0');
INSERT INTO `member_decision_file` VALUES ('44', '83', '43534', '3', '2018-03-20', '386', '1', '1', '2018-03-20 16:02:52', '2018-03-20 16:02:52', '2018-03-20 16:02:52', '0');
INSERT INTO `member_decision_file` VALUES ('45', '93', '12398', '4', '2018-02-26', '406', '1', '1', '2018-03-23 14:44:04', '2018-03-23 14:44:04', '2018-03-23 14:44:04', '0');
INSERT INTO `member_decision_file` VALUES ('46', '48', '121212', '4', '2018-03-05', '410', '1', '1', '2018-03-24 23:41:08', '2018-03-24 23:41:08', '2018-03-24 23:41:08', '0');
INSERT INTO `member_decision_file` VALUES ('47', '32', '456-55/52656-565/265', '3', '2018-03-26', '420', '1', '1', '2018-03-26 10:30:49', '2018-03-26 10:30:49', '2018-03-26 10:30:49', '0');
INSERT INTO `member_decision_file` VALUES ('48', '25', 'QD34534', '3', '2018-03-28', '431', '1', '1', '2018-03-28 13:39:53', '2018-03-28 13:39:53', '2018-03-28 13:39:53', '0');
INSERT INTO `member_decision_file` VALUES ('49', '49', 'QD23423', '3', '2018-03-26', '432', '1', '1', '2018-03-26 14:01:01', '2018-03-26 14:01:01', '2018-03-26 14:01:01', '0');
INSERT INTO `member_decision_file` VALUES ('50', '81', 'hakyquyetdinh', '3', '2018-03-27', '472', '1', '1', '2018-03-27 17:51:23', '2018-03-27 17:51:23', '2018-03-27 17:51:23', '0');
INSERT INTO `member_decision_file` VALUES ('51', '42', 'hahakyquyetdinh', '3', '2018-03-27', '473', '1', '1', '2018-03-27 17:53:08', '2018-03-27 17:53:08', '2018-03-27 17:53:08', '0');
INSERT INTO `member_decision_file` VALUES ('52', '45', '0', '4', '2018-03-29', '476', '1', '1', '2018-03-28 08:30:19', '2018-03-28 08:30:19', '2018-03-28 08:30:19', '0');
INSERT INTO `member_decision_file` VALUES ('53', '44', '564', '4', '2018-03-28', '483', '1', '1', '2018-03-28 11:18:57', '2018-03-28 11:18:57', '2018-03-28 11:18:57', '0');
INSERT INTO `member_decision_file` VALUES ('54', '43', 'hacreatequyetdinh', '4', '2018-03-27', '485', '1', '1', '2018-03-28 11:44:11', '2018-03-28 11:44:11', '2018-03-28 11:44:11', '0');
INSERT INTO `member_decision_file` VALUES ('55', '54', '23232', '4', '2018-03-21', '484', '1', '1', '2018-03-28 11:36:50', '2018-03-28 11:36:50', '2018-03-28 11:36:50', '0');
INSERT INTO `member_decision_file` VALUES ('56', '65', '4987', '3', '2018-03-28', '486', '1', '1', '2018-03-28 13:46:19', '2018-03-28 13:46:19', '2018-03-28 13:46:19', '0');
INSERT INTO `member_decision_file` VALUES ('57', '71', 'i', '3', '2018-03-28', '496', '1', '1', '2018-03-28 14:38:32', '2018-03-28 14:38:32', '2018-03-28 14:38:32', '0');
INSERT INTO `member_decision_file` VALUES ('58', '41', 'hataolan3', '4', '2018-03-28', '489', '1', '1', '2018-03-28 14:12:45', '2018-03-28 14:12:45', '2018-03-28 14:12:45', '0');
INSERT INTO `member_decision_file` VALUES ('59', '94', 'hataolan2update2', '3', '2018-03-28', '488', '1', '1', '2018-03-28 14:15:24', '2018-03-28 14:15:24', '2018-03-28 14:15:24', '0');
INSERT INTO `member_decision_file` VALUES ('60', '96', 'hataolan1-update', '4', '2018-03-28', '487', '1', '1', '2018-03-28 14:15:15', '2018-03-28 14:15:15', '2018-03-28 14:15:15', '0');
INSERT INTO `member_decision_file` VALUES ('61', '99', 'hataoluon', '3', '2018-03-28', '493', '1', '1', '2018-03-29 17:22:36', '2018-03-29 17:22:36', '2018-03-29 17:22:36', '0');
INSERT INTO `member_decision_file` VALUES ('62', '103', '435', '3', '2018-03-28', '494', '1', '1', '2018-03-28 14:27:50', '2018-03-28 14:27:50', '2018-03-28 14:27:50', '0');
INSERT INTO `member_decision_file` VALUES ('63', '102', 'giang546 $^&*&()*0', '3', '2018-03-29', '495', '1', '1', '2018-03-30 09:17:39', '2018-03-30 09:17:39', '2018-03-30 09:17:39', '0');
INSERT INTO `member_decision_file` VALUES ('64', '123', 'fg', '3', '2018-03-28', '497', '1', '1', '2018-03-28 14:38:53', '2018-03-28 14:38:53', '2018-03-28 14:38:53', '0');
INSERT INTO `member_decision_file` VALUES ('65', '30', '12312312', '3', '2018-03-28', '499', '1', '1', '2018-03-28 14:39:46', '2018-03-28 14:39:46', '2018-03-28 14:39:46', '0');
INSERT INTO `member_decision_file` VALUES ('66', '87', 'f', '3', '2018-03-28', '500', '1', '1', '2018-03-28 14:42:38', '2018-03-28 14:42:38', '2018-03-28 14:42:38', '0');
INSERT INTO `member_decision_file` VALUES ('67', '80', 'h', '3', '2018-03-28', '501', '1', '1', '2018-03-28 14:45:46', '2018-03-28 14:45:46', '2018-03-28 14:45:46', '0');
INSERT INTO `member_decision_file` VALUES ('68', '130', 'f', '3', '2018-03-28', '502', '1', '1', '2018-03-28 14:49:39', '2018-03-28 14:49:39', '2018-03-28 14:49:39', '0');
INSERT INTO `member_decision_file` VALUES ('69', '131', '4534', '3', '2018-03-29', '508', '1', '1', '2018-03-28 15:17:11', '2018-03-28 15:17:11', '2018-03-28 15:17:11', '0');
INSERT INTO `member_decision_file` VALUES ('70', '132', '4356', '3', '2018-03-29', '511', '1', '1', '2018-03-28 16:04:32', '2018-03-28 16:04:32', '2018-03-28 16:04:32', '0');
INSERT INTO `member_decision_file` VALUES ('71', '134', '67-hu/88', '3', '2018-03-29', '514', '1', '1', '2018-03-28 16:30:28', '2018-03-28 16:30:28', '2018-03-28 16:30:28', '0');
INSERT INTO `member_decision_file` VALUES ('72', '133', '34534', '3', '2018-03-31', '548', '1', '1', '2018-03-30 17:14:13', '2018-03-30 17:14:13', '2018-03-30 17:14:13', '0');
INSERT INTO `member_decision_file` VALUES ('73', '140', '#$^&*(*(*&^&%&#*()@$', '3', '2018-03-30', '528', '1', '1', '2018-03-29 15:27:47', '2018-03-29 15:27:47', '2018-03-29 15:27:47', '0');
INSERT INTO `member_decision_file` VALUES ('74', '101', null, null, null, null, '1', null, '2018-03-29 17:21:43', '2018-03-29 17:21:43', null, '0');
INSERT INTO `member_decision_file` VALUES ('76', '115', 'taoquyetdinhcapma', '4', '2018-03-31', '533', '1', '1', '2018-03-30 10:45:28', '2018-03-30 10:45:28', '2018-03-30 10:45:28', '0');
INSERT INTO `member_decision_file` VALUES ('77', '116', 'taoquyetdinhcapma', '4', '2018-03-30', '534', '1', '1', '2018-03-30 10:45:46', '2018-03-30 10:45:46', '2018-03-30 10:45:46', '0');
INSERT INTO `member_decision_file` VALUES ('78', '100', '343434', '3', '2018-03-31', '538', '1', '1', '2018-03-30 13:46:01', '2018-03-30 13:46:01', '2018-03-30 13:46:01', '0');
INSERT INTO `member_decision_file` VALUES ('79', '113', '232323232', '3', '2018-03-30', '540', '1', '1', '2018-03-30 13:50:09', '2018-03-30 13:50:09', '2018-03-30 13:50:09', '0');
INSERT INTO `member_decision_file` VALUES ('80', '76', '232323', '4', '2018-03-30', '539', '1', '1', '2018-03-30 13:49:52', '2018-03-30 13:49:52', '2018-03-30 13:49:52', '0');
INSERT INTO `member_decision_file` VALUES ('81', '70', '56', '3', '2018-04-03', '576', '1', '1', '2018-04-02 15:32:51', '2018-04-02 15:32:51', '2018-04-02 15:32:51', '0');
INSERT INTO `member_decision_file` VALUES ('82', '89', '1578', '3', '2018-04-02', '577', '1', '1', '2018-04-02 16:07:05', '2018-04-02 16:07:05', '2018-04-02 16:07:05', '0');
INSERT INTO `member_decision_file` VALUES ('83', '118', '32323', '3', '2018-04-02', '580', '1', '1', '2018-04-02 16:29:05', '2018-04-02 16:29:05', '2018-04-02 16:29:05', '0');
INSERT INTO `member_decision_file` VALUES ('84', '86', 'q223', '4', '2018-04-03', '581', '1', '1', '2018-04-03 15:21:58', '2018-04-03 08:21:58', '2018-04-03 15:21:58', '0');
INSERT INTO `member_decision_file` VALUES ('85', '160', '65745', '3', '2018-04-05', '583', '1', '1', '2018-04-04 08:35:13', '2018-04-04 01:35:13', '2018-04-04 08:35:13', '0');

-- ----------------------------
-- Table structure for member_payments
-- ----------------------------
DROP TABLE IF EXISTS `member_payments`;
CREATE TABLE `member_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number_payment` varchar(100) NOT NULL,
  `date_payment` datetime NOT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `memberId` int(10) unsigned NOT NULL,
  `currency` varchar(100) NOT NULL,
  `currency_type` varchar(10) NOT NULL,
  `payment_method` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_content` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleteAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `memberId` (`memberId`),
  CONSTRAINT `member_payments_ibfk_1` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member_payments
-- ----------------------------
INSERT INTO `member_payments` VALUES ('1', '111111118', '2018-03-13 00:00:00', '0', '4', '10023', 'vnd', 'payment', 'sdfsdf', 'sdfsdfsd', '2018-03-13 03:34:26', '2018-03-13 10:34:26', null);
INSERT INTO `member_payments` VALUES ('2', '100', '2018-03-13 00:00:00', '0', '6', '100', 'vnd', 'payment', 'sdfsdf', 'sdfsdf', '2018-03-13 03:36:07', '2018-03-13 10:36:07', null);
INSERT INTO `member_payments` VALUES ('3', 'sdf', '2018-03-01 00:00:00', '0', '3', '111', 'vnd', 'payment', 'ưerqw', 'qưerqwr', '2018-03-13 03:47:45', '2018-03-13 10:47:45', null);
INSERT INTO `member_payments` VALUES ('4', 'DDDDDDD#$^#$^@#^$&%&', '2018-03-30 00:00:00', '0', '9', '5464563452', 'vnd', 'payment', '7vvvvvvvvvvvvvvvvvvvvf dty5 bfhgsdurf ewyr78werghsc x z hgvhdgfyds ghfsdhfv wervghfsd gfwerbwbrjw cbhsdgfuysdufweh cbhsdghfuyweubweghr bhfsdyfgwhefewbfw bhsdgfywhrlkwebrhjwegbruwe cbshdgfywegrbwehgreư', '7vvvvvvvvvvvvvvvvvvvvf dty5 bfhgsdurf ewyr78werghsc x z hgvhdgfyds ghfsdhfv wervghfsd gfwerbwbrjw cbhsdgfuysdufweh cbhsdghfuyweubweghr bhfsdyfgwhefewbfw bhsdgfywhrlkwebrhjwegbruwe cbshdgfywegrbwehgreư', '2018-03-30 01:52:42', '2018-03-30 08:52:42', '2018-03-30 08:52:42');
INSERT INTO `member_payments` VALUES ('5', '1223', '2018-03-13 00:00:00', '0', '14', '23234', 'vnd', 'payment', 'sdfdsf', null, '2018-03-13 07:27:31', '2018-03-13 14:27:31', null);
INSERT INTO `member_payments` VALUES ('6', '154', '2018-03-27 10:21:53', '0', '10', '200000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-28 10:21:53', '2018-03-28 17:21:53', '2018-03-28 17:21:53');
INSERT INTO `member_payments` VALUES ('7', '123', '2018-03-13 00:00:00', '0', '12', '1213', 'vnd', 'payment', 'sdf', 'sdf', '2018-03-13 07:27:56', '2018-03-13 14:27:56', null);
INSERT INTO `member_payments` VALUES ('8', '23214243', '2018-03-13 00:00:00', '0', '13', '232434', 'vnd', 'payment', 'dsfdsf', 'sdfdsf', '2018-03-13 07:28:13', '2018-03-13 14:28:13', null);
INSERT INTO `member_payments` VALUES ('9', '23434343', '2018-03-13 00:00:00', '0', '8', '3434343', 'vnd', 'payment', 'sdfsdfdsf', null, '2018-03-13 07:28:44', '2018-03-13 14:28:44', null);
INSERT INTO `member_payments` VALUES ('10', '571', '2018-03-27 04:40:15', '0', '7', '200000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-28 04:40:15', '2018-03-28 11:40:15', '2018-03-28 11:40:15');
INSERT INTO `member_payments` VALUES ('11', '1235', '2018-03-13 00:00:00', '0', '1', '1234', 'vnd', 'payment', 'sdfsdf', 'sdfsdf', '2018-03-13 07:33:03', '2018-03-13 14:33:03', null);
INSERT INTO `member_payments` VALUES ('12', '123', '2018-03-13 00:00:00', '0', '2', '123', 'vnd', 'payment', 'sdfsdf', 'sdfsdf', '2018-03-13 07:33:16', '2018-03-13 14:33:16', null);
INSERT INTO `member_payments` VALUES ('14', 'UNC001', '2017-12-12 10:35:44', '0', '5', '500000', 'vnd', '1', 'Nộp tiền đăng ký hội viên', '', '2018-03-16 10:35:44', '2018-03-16 17:35:44', '2018-03-16 17:35:44');
INSERT INTO `member_payments` VALUES ('15', '1', '2018-03-14 00:00:00', '0', '17', '1000000', 'vnd', 'payment', 'Nộp tiền đăng ký gia nhập hội', 'OK', '2018-03-14 03:18:02', '2018-03-14 10:18:02', null);
INSERT INTO `member_payments` VALUES ('16', '2', '2018-03-14 00:00:00', '0', '34', '1000000', 'vnd', 'payment', 'th', null, '2018-03-14 04:41:42', '2018-03-14 11:41:42', null);
INSERT INTO `member_payments` VALUES ('17', '3', '2018-03-14 00:00:00', '0', '35', '1500000', 'vnd', 'payment', '0', null, '2018-03-14 04:42:29', '2018-03-14 11:42:29', null);
INSERT INTO `member_payments` VALUES ('18', '3', '2018-03-14 00:00:00', '0', '36', '3000', 'vnd', 'payment', '00', null, '2018-03-14 04:42:56', '2018-03-14 11:42:56', null);
INSERT INTO `member_payments` VALUES ('19', '4545', '2018-03-07 00:00:00', '0', '15', '45', 'vnd', 'payment', '54', '67', '2018-03-14 06:23:11', '2018-03-14 13:23:11', null);
INSERT INTO `member_payments` VALUES ('20', '111111111111111111111111111119', '2018-03-14 00:00:00', '0', '22', '-234234', 'vnd', 'payment', 'Nội dung nộp tiền Nội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung nộp tiềnNội dung End', 'Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi chú Ghi cEnd', '2018-03-14 08:10:14', '2018-03-14 15:10:14', null);
INSERT INTO `member_payments` VALUES ('23', '123456', '2018-03-12 00:00:00', '0', '33', '100', 'vnd', 'payment', 'Nội dung nộp tiền', 'Ghi chú', '2018-03-14 08:38:44', '2018-03-14 15:38:44', null);
INSERT INTO `member_payments` VALUES ('25', '1234', '2018-03-08 00:00:00', '0', '11', '5000000', 'vnd', 'payment', 'ádfasdfa', 'ádfsadfaf', '2018-03-14 09:41:26', '2018-03-14 16:41:26', null);
INSERT INTO `member_payments` VALUES ('26', '3233', '2018-03-06 00:00:00', '0', '18', '234234', 'vnd', 'payment', 'dfsdf', 'ádfa', '2018-03-14 10:08:44', '2018-03-14 17:08:44', null);
INSERT INTO `member_payments` VALUES ('27', '111111111111111111111111111111', '2018-03-14 00:00:00', '0', '23', '111111111', 'vnd', 'payment', '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '2018-03-14 11:40:13', '2018-03-14 18:40:13', null);
INSERT INTO `member_payments` VALUES ('28', '11111', '2018-03-07 00:00:00', '0', '24', '-1111111111', 'vnd', 'payment', 'ádadaf', 'âfafaf', '2018-03-14 11:43:06', '2018-03-14 18:43:06', null);
INSERT INTO `member_payments` VALUES ('31', 'ABD123', '2018-03-07 00:00:00', '0', '27', '2343', 'vnd', 'payment', 'Nội dung nộp tiền', null, '2018-03-15 04:03:35', '2018-03-15 11:03:35', null);
INSERT INTO `member_payments` VALUES ('32', 'aaaaa', '2018-03-15 00:00:00', '0', '28', '1111121212', 'vnd', 'payment', 'ládlkajdklajdlksajd', null, '2018-03-15 06:32:42', '2018-03-15 13:32:42', null);
INSERT INTO `member_payments` VALUES ('33', 'sdsds', '2018-03-01 00:00:00', '0', '57', '123', 'vnd', 'payment', 'dsfsd', 'sdfsd', '2018-03-15 09:46:15', '2018-03-15 16:46:15', null);
INSERT INTO `member_payments` VALUES ('34', 'lajldjaljfdlka', '2018-03-11 00:00:00', '0', '29', '111', 'vnd', 'payment', '1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafafafafaf1212121sdfafdafa', '212123 sad âd', '2018-03-19 02:58:35', '2018-03-19 09:58:35', '2018-03-19 09:58:35');
INSERT INTO `member_payments` VALUES ('35', 'CT157', '2018-03-16 00:00:00', '0', '59', '1000000', 'vnd', 'payment', 'thanh toán lệ phí', 'fdsfd', '2018-03-16 02:46:18', '2018-03-16 09:46:18', null);
INSERT INTO `member_payments` VALUES ('37', '123', '2018-03-16 00:00:00', '0', '31', '123', 'vnd', 'payment', '<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<scri', '<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<script> alert(1) </script>\nhoặc: <script>alert(“XSS”)</script> \n<scri', '2018-03-16 04:22:54', '2018-03-16 11:22:54', null);
INSERT INTO `member_payments` VALUES ('39', '21132321', '2018-03-16 00:00:00', '0', '46', '1133', 'vnd', 'payment', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'sdasd', '2018-03-19 19:40:03', '2018-03-20 02:40:03', '2018-03-20 02:40:03');
INSERT INTO `member_payments` VALUES ('42', '123-/676', '2018-03-16 00:00:00', '0', '38', '123', 'vnd', 'payment', 'dsfdsfdsfds', 'null', '2018-03-21 10:18:25', '2018-03-21 17:18:25', '2018-03-21 17:18:25');
INSERT INTO `member_payments` VALUES ('45', '123', '2018-03-16 00:00:00', '0', '44', '123', 'vnd', 'payment', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '\' + (SELECT\nTOP 1 FieldName FROM TableName) + \'', '2018-03-16 07:38:06', '2018-03-16 14:38:06', '2018-03-16 14:38:06');
INSERT INTO `member_payments` VALUES ('46', 'adb234123412', '2018-03-06 00:00:00', '0', '37', '234444', 'vnd', 'payment', 'ffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsd', 'ffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsdfsadfadsffffffsd', '2018-03-19 02:58:22', '2018-03-19 09:58:22', '2018-03-19 09:58:22');
INSERT INTO `member_payments` VALUES ('49', '165-58/5654', '2018-03-16 00:00:00', '0', '42', '120000', 'vnd', 'payment', 'nộp chứng từ', 'fsdf', '2018-03-27 02:55:28', '2018-03-27 09:55:28', '2018-03-27 09:55:28');
INSERT INTO `member_payments` VALUES ('50', '123fdsfd', '2018-03-16 00:00:00', '0', '48', '1234', 'vnd', 'payment', 'dss', null, '2018-03-19 02:58:46', '2018-03-19 09:58:46', '2018-03-19 09:58:46');
INSERT INTO `member_payments` VALUES ('52', 'aabbcc#!@#!', '2018-03-19 02:45:08', null, '43', '200000000', 'vnd', '1', 'Nộp tiền', 'Nt', '2018-03-19 02:45:08', '2018-03-19 09:45:08', '2018-03-19 09:45:08');
INSERT INTO `member_payments` VALUES ('54', 'aabbcc#!@#!', '2018-03-19 03:31:56', null, '32', '200000000', 'vnd', '1', 'Nộp tiền', 'Nt', '2018-03-19 03:31:56', '2018-03-19 10:31:56', null);
INSERT INTO `member_payments` VALUES ('55', '54654', '2018-03-19 00:00:00', '0', '45', '0', 'vnd', 'payment', '56756', null, '2018-03-19 03:50:19', '2018-03-19 10:50:19', null);
INSERT INTO `member_payments` VALUES ('56', '345345', '2018-02-25 00:00:00', '0', '19', '4545', 'vnd', 'payment', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', 'null', '2018-03-21 10:20:21', '2018-03-21 17:20:21', '2018-03-21 17:20:21');
INSERT INTO `member_payments` VALUES ('57', '756756', '2018-03-20 00:00:00', '0', '54', '000001', 'vnd', 'payment', '56756', '7567567', '2018-03-20 03:59:56', '2018-03-20 10:59:56', null);
INSERT INTO `member_payments` VALUES ('58', '234324', '2018-02-26 00:00:00', '0', '71', '343', 'vnd', 'payment', '234', '23423423423', '2018-03-20 04:03:27', '2018-03-20 11:03:27', null);
INSERT INTO `member_payments` VALUES ('59', '56', '2018-03-20 00:00:00', '0', '83', '57657', 'vnd', 'payment', '567', '5675674634m6', '2018-03-20 04:44:55', '2018-03-20 11:44:55', null);
INSERT INTO `member_payments` VALUES ('63', '345-/', '2018-03-21 00:00:00', '0', '41', '12000', 'vnd', 'payment', 'ngân', 'ngậnfhfgh', '2018-03-21 10:31:36', '2018-03-21 17:31:36', null);
INSERT INTO `member_payments` VALUES ('65', '435', '2018-03-21 00:00:00', '0', '81', '123', 'vnd', 'payment', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-22 06:57:48', '2018-03-22 13:57:48', null);
INSERT INTO `member_payments` VALUES ('66', '123-/7892265656623154564685415', '2018-03-22 00:00:00', '0', '93', '1200000000', 'vnd', 'payment', 'Nộp tiền', 'ghic hú', '2018-03-23 04:25:43', '2018-03-23 11:25:43', null);
INSERT INTO `member_payments` VALUES ('67', '1489', '2018-03-26 00:00:00', '0', '25', '12300000', 'vnd', 'payment', '1. Đăng nhập acc Kế toán\n2. Vào menu Đóng Lệ phí + Hội phí\n3. Click icon Xem chứng từ, kiểm tra thông tin trên màn hình Xem chi tiết\n=> Thông tin ghi chú hiển thị như ảnh đính kèm\n=> MM: Thực hiện ng5', '1. Đăng nhập acc Kế toán2. Vào menu Đóng Lệ phí + Hội phí3. Click icon Xem chứng từ, kiểm tra thông tin trên màn hình Xem chi tiết=> Thông tin ghi chú hiển thị như ảnh đính kèm=> MM: Thực hiện ng5', '2018-03-26 02:54:33', '2018-03-26 09:54:33', null);
INSERT INTO `member_payments` VALUES ('68', '4353', '2018-03-26 00:00:00', '0', '49', '34535', 'vnd', 'payment', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-26 02:55:44', '2018-03-26 09:55:44', null);
INSERT INTO `member_payments` VALUES ('69', '34qw3123', '2018-03-24 00:00:00', '0', '94', '12500', 'vnd', 'payment', 'fdg', 'dfgd', '2018-03-27 02:56:45', '2018-03-27 09:56:45', null);
INSERT INTO `member_payments` VALUES ('70', '34', '2018-03-27 00:00:00', '0', '96', '34534', 'vnd', 'payment', 'fdg', 'dfgd', '2018-03-27 02:57:18', '2018-03-27 09:57:18', null);
INSERT INTO `member_payments` VALUES ('71', '34534', '2018-03-27 00:00:00', '0', '99', '5345', 'vnd', 'payment', 'fdg', 'dfgd', '2018-03-27 02:57:49', '2018-03-27 09:57:49', null);
INSERT INTO `member_payments` VALUES ('72', '456456', '2018-03-27 00:00:00', '0', '103', '45654', 'vnd', 'payment', '456', '54654', '2018-03-27 02:58:15', '2018-03-27 09:58:15', null);
INSERT INTO `member_payments` VALUES ('73', '435-ghj/fghf', '2018-03-28 00:00:00', '0', '102', '2565', 'vnd', 'payment', 'noopj chnwgs tuwf thuees thu nhapj cas nah', 'noopj chnwgs tuwf thuees thu nhapj cas nah', '2018-03-28 01:58:36', '2018-03-28 08:58:36', '2018-03-28 08:58:36');
INSERT INTO `member_payments` VALUES ('75', '571', '2018-03-27 04:38:21', '0', '130', '200000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-28 04:38:21', '2018-03-28 11:38:21', '2018-03-28 11:38:21');
INSERT INTO `member_payments` VALUES ('76', '157', '2018-03-27 04:38:21', '0', '80', '2000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-28 04:38:21', '2018-03-28 11:38:21', '2018-03-28 11:38:21');
INSERT INTO `member_payments` VALUES ('77', '157', '2018-03-27 04:40:15', '0', '30', '2000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-28 04:40:15', '2018-03-28 11:40:15', null);
INSERT INTO `member_payments` VALUES ('78', '571', '2018-03-27 04:40:45', '0', '87', '200000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-28 04:40:45', '2018-03-28 11:40:45', null);
INSERT INTO `member_payments` VALUES ('79', '571', '2018-03-27 04:41:43', '0', '123', '200000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-28 04:41:43', '2018-03-28 11:41:43', null);
INSERT INTO `member_payments` VALUES ('80', '154', '2018-03-27 10:19:43', '0', '101', '200000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-28 10:19:43', '2018-03-28 17:19:43', '2018-03-28 17:19:43');
INSERT INTO `member_payments` VALUES ('81', '15-986/5465Ad', '2018-03-28 00:00:00', '0', '131', '1200000', 'vnd', 'payment', 'gdfg', '546546', '2018-03-28 08:15:48', '2018-03-28 15:15:48', null);
INSERT INTO `member_payments` VALUES ('82', '34534', '2018-03-28 00:00:00', '0', '132', '34353', 'vnd', 'payment', 'dfgfd', 'g', '2018-03-28 09:03:55', '2018-03-28 16:03:55', null);
INSERT INTO `member_payments` VALUES ('83', '1569-78/45A', '2018-03-28 00:00:00', '0', '134', '1200000', 'vnd', 'payment', 'nộp tiền hội viên', 'năm 2018', '2018-03-28 09:29:56', '2018-03-28 16:29:56', null);
INSERT INTO `member_payments` VALUES ('84', '#@#$%$^%&*%^*(', '2018-03-27 01:42:52', '0', '100', '2000000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-29 01:42:52', '2018-03-29 08:42:52', null);
INSERT INTO `member_payments` VALUES ('85', '#@#$%$^%&*%^*(', '2018-03-27 04:31:17', '0', '113', '200000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-29 04:31:17', '2018-03-29 11:31:17', null);
INSERT INTO `member_payments` VALUES ('86', '7546-3456GDFG/34523452VDFGDFGD', '2018-03-30 00:00:00', '0', '115', '324523', 'vnd', 'payment', '7vvvvvvvvvvvvvvvvvvvvf dty5 bfhgsdurf ewyr78werghsc x z hgvhdgfyds ghfsdhfv wervghfsd gfwerbwbrjw cbhsdgfuysdufweh cbhsdghfuyweubweghr bhfsdyfgwhefewbfw bhsdgfywhrlkwebrhjwegbruwe cbshdgfywegrbwehgreư', '7vvvvvvvvvvvvvvvvvvvvf dty5 bfhgsdurf ewyr78werghsc x z hgvhdgfyds ghfsdhfv wervghfsd gfwerbwbrjw cbhsdgfuysdufweh cbhsdghfuyweubweghr bhfsdyfgwhefewbfw bhsdgfywhrlkwebrhjwegbruwe cbshdgfywegrbwehgreư', '2018-03-30 01:46:36', '2018-03-30 08:46:36', '2018-03-30 08:46:36');
INSERT INTO `member_payments` VALUES ('87', '12352346257362894ghdfasghdfhiheuirf', '2018-03-26 04:45:33', '0', '116', '200000000', 'vnd', '1', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x74;&#x65;&#x73;&#x74;&#x22;&#x29;&#x20;&#x3C;&#x2F;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;', '2018-03-29 04:45:33', '2018-03-29 11:45:33', null);
INSERT INTO `member_payments` VALUES ('88', '345234', '2018-03-30 00:00:00', '0', '118', '2354', 'vnd', 'payment', '34534', 'rệtrh', '2018-03-30 09:06:57', '2018-03-30 16:06:57', '2018-03-30 16:06:57');
INSERT INTO `member_payments` VALUES ('89', '7567', '2018-03-30 00:00:00', '0', '133', '45745', 'vnd', 'payment', '3454', '34534', '2018-03-30 06:53:15', '2018-03-30 13:53:15', '2018-03-30 13:53:15');
INSERT INTO `member_payments` VALUES ('90', '34534', '2018-03-29 00:00:00', '0', '140', '43534', 'vnd', 'payment', '34534', '534534', '2018-03-29 08:23:25', '2018-03-29 15:23:25', null);
INSERT INTO `member_payments` VALUES ('91', 'hattt-check', '2018-03-29 00:00:00', '0', '76', '2000000', 'vnd', 'payment', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '2018-03-29 10:19:36', '2018-03-29 17:19:36', null);
INSERT INTO `member_payments` VALUES ('92', '345435', '2018-04-02 00:00:00', '0', '70', '34534', 'vnd', 'payment', '34534', '534534534', '2018-04-02 08:12:54', '2018-04-02 15:12:54', '2018-04-02 15:12:54');
INSERT INTO `member_payments` VALUES ('93', 'jlajdlkjakldjalkds', '2018-03-30 00:00:00', '0', '86', '2323', 'vnd', 'payment', '2323', '2323', '2018-03-30 08:56:12', '2018-03-30 15:56:12', '2018-03-30 15:56:12');
INSERT INTO `member_payments` VALUES ('94', '2323', '2018-03-30 00:00:00', '0', '89', '232323', 'vnd', 'payment', '2323', '232323232', '2018-03-30 08:54:34', '2018-03-30 15:54:34', '2018-03-30 15:54:34');
INSERT INTO `member_payments` VALUES ('95', '34534', '2018-04-04 00:00:00', '0', '160', '3242342', 'vnd', 'payment', '33', '3', '2018-04-04 01:34:40', '2018-04-04 01:34:40', null);
INSERT INTO `member_payments` VALUES ('96', 'CT09042018', '2018-04-10 00:00:00', '0', '161', '500000', 'vnd', 'payment', 'Ninh nộp tiền phí', 'Ninh nộp tiền phí', '2018-04-09 14:29:32', '2018-04-09 07:29:32', '2018-04-09 14:29:32');
INSERT INTO `member_payments` VALUES ('97', 'CT09042018', '2018-04-09 00:00:00', '0', '104', '9999999999', 'vnd', 'payment', 'NinhCập nhật thông tin hội phí & lệ phíCập nhật thông tin hội phí & lệ phíCập nhật thông tin hội phí & lệ phíCập nhật thông tin hội phí & lệ phíCập nhật thông tin hội phí & lệ phíCập nhật thông tin hộ', 'Cập nhật thông tin hội phí & lệ phíCập nhật thông tin hội phí & lệ phíCập nhật thông tin hội phí & lệ phíCập nhật thông tin hội phí & lệ phíCập nhật thông tin hội phí & lệ phíCập nhật thông tin hội ph', '2018-04-09 15:16:44', '2018-04-09 08:16:44', '2018-04-09 15:16:44');

-- ----------------------------
-- Table structure for member_position
-- ----------------------------
DROP TABLE IF EXISTS `member_position`;
CREATE TABLE `member_position` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member_position
-- ----------------------------
INSERT INTO `member_position` VALUES ('2', '30', '1', null, '2018-03-23 20:05:47', '2018-03-23 20:05:49');
INSERT INTO `member_position` VALUES ('4', '30', '3', null, '2018-03-23 20:05:59', '2018-03-23 20:06:02');
INSERT INTO `member_position` VALUES ('33', '1', '4', null, '2018-03-29 17:41:10', '2018-03-29 17:45:47');
INSERT INTO `member_position` VALUES ('34', '30', '8', null, '2018-04-10 09:12:30', '2018-04-10 09:12:30');
INSERT INTO `member_position` VALUES ('35', '30', '7', null, '2018-04-10 09:44:27', '2018-04-10 09:44:27');
INSERT INTO `member_position` VALUES ('36', '10', '54', null, '2018-04-12 06:51:48', '2018-04-12 06:51:48');
INSERT INTO `member_position` VALUES ('37', '11', '1', null, '2018-04-12 07:19:36', '2018-04-12 07:19:36');
INSERT INTO `member_position` VALUES ('38', '11', '31', null, '2018-04-12 07:20:18', '2018-04-12 07:20:18');
INSERT INTO `member_position` VALUES ('39', '11', '4', null, '2018-04-12 08:00:18', '2018-04-12 08:00:18');
INSERT INTO `member_position` VALUES ('40', '11', '12', null, '2018-04-12 08:06:13', '2018-04-12 08:06:13');
INSERT INTO `member_position` VALUES ('41', '13', '10', null, '2018-04-12 08:11:46', '2018-04-12 08:11:46');
INSERT INTO `member_position` VALUES ('42', '22', '9', null, '2018-04-13 04:50:33', '2018-04-13 04:50:33');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_11_27_000000_create_files_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_11_28_000000_create_members_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_11_28_000001_create_educationBranches_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_11_28_000002_create_educationDegrees_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_11_28_000003_create_educations_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_11_28_000004_create_languages_table', '1');
INSERT INTO `migrations` VALUES ('9', '2017_11_28_000005_create_languageLevels_table', '1');
INSERT INTO `migrations` VALUES ('10', '2017_11_28_000006_create_languageSkills_table', '1');
INSERT INTO `migrations` VALUES ('11', '2017_11_28_000007_create_works_table', '1');
INSERT INTO `migrations` VALUES ('12', '2017_11_28_000008_create_workHistory_table', '1');
INSERT INTO `migrations` VALUES ('13', '2017_11_28_000009_create_typeGuides_table', '1');
INSERT INTO `migrations` VALUES ('14', '2017_11_28_000010_create_groupSizes_table', '1');
INSERT INTO `migrations` VALUES ('15', '2017_11_28_000011_create_elements_table', '1');
INSERT INTO `migrations` VALUES ('16', '2017_11_28_000012_create_majors_table', '1');
INSERT INTO `migrations` VALUES ('17', '2017_11_28_000013_create_majorSkills_table', '1');
INSERT INTO `migrations` VALUES ('18', '2017_11_28_000014_create_majorSkillFiles_table', '1');
INSERT INTO `migrations` VALUES ('19', '2017_11_28_000015_create_achievementFiles_table', '1');
INSERT INTO `migrations` VALUES ('20', '2017_12_24_071226_create_notes_table', '1');
INSERT INTO `migrations` VALUES ('21', '2018_02_27_161019_create_jobs_table', '2');
INSERT INTO `migrations` VALUES ('22', '2018_02_27_170752_create_failed_jobs_table', '2');
INSERT INTO `migrations` VALUES ('23', '2018_03_06_034605_add_column_to_members_table', '3');
INSERT INTO `migrations` VALUES ('24', '2018_03_14_164607_rename_avatar_to_profile_img_members_table', '4');
INSERT INTO `migrations` VALUES ('25', '2018_03_24_091806_offices', '4');
INSERT INTO `migrations` VALUES ('26', '2018_04_02_091806_offices_2', '4');
INSERT INTO `migrations` VALUES ('27', '2018_04_02_171851_create_options_table', '4');
INSERT INTO `migrations` VALUES ('28', '2018_04_02_171934_create_employees_table', '4');
INSERT INTO `migrations` VALUES ('29', '2018_04_02_173055_create_employee_position_table', '4');
INSERT INTO `migrations` VALUES ('30', '2018_04_02_173219_create_news_table', '4');
INSERT INTO `migrations` VALUES ('31', '2018_04_02_173223_create_help_information_table', '4');
INSERT INTO `migrations` VALUES ('32', '2018_04_02_173223_rename_column', '4');
INSERT INTO `migrations` VALUES ('33', '2018_04_08_170609_update_employee_postion_table', '4');
INSERT INTO `migrations` VALUES ('34', '2018_04_08_170609_update_employee_postion_table2', '5');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumnail_image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `main_image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `information_type_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `show_home_page` tinyint(4) DEFAULT NULL,
  `option_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', '9 điểm đến hấp dẫn ở Nam Định', '3.jpg', 'Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) là nơi thờ tự 14 vị vua nhà Trần cùng với gia quyến, các vị quan đã có công phù tá nhà Trần…', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\"><strong><img src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/%C4%90%E1%BB%81n%20Tr%E1%BA%A7n.jpg\" alt=\"\" width=\"1000\" height=\"562\" /></strong></p>\r\n<p style=\"text-align: justify;\"><strong>Đền Trần</strong></p>\r\n<p style=\"text-align: justify;\">Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) l&agrave; nơi thờ tự 14 vị vua nh&agrave; Trần c&ugrave;ng với gia quyến, c&aacute;c vị quan đ&atilde; c&oacute; c&ocirc;ng ph&ugrave; t&aacute; nh&agrave; Trần&hellip; v&agrave; l&agrave; một trong những điểm đến m&agrave; rất nhiều du kh&aacute;ch muốn gh&eacute; qua khi đặt ch&acirc;n đến Nam Định. Đền Trần được x&acirc;y dựng v&agrave;o năm 1695 tr&ecirc;n nền Th&aacute;i miếu cũ (tức l&agrave; Phủ Thi&ecirc;n Trường xưa) - nơi được coi l&agrave; ph&aacute;t t&iacute;ch của vương triều nh&agrave; Trần.</p>\r\n<p style=\"text-align: justify;\">Đền bao gồm tổ hợp ba c&ocirc;ng tr&igrave;nh kiến tr&uacute;c: đền Thi&ecirc;n Trường (đền Thượng), đền Cổ Trạch (đền Hạ) v&agrave; đền Tr&ugrave;ng Hoa c&ugrave;ng mang một kiểu kiến tr&uacute;c v&agrave; quy m&ocirc; ngang nhau. Mỗi đền gồm t&ograve;a tiền đường 5 gian, t&ograve;a trung đường 5 gian v&agrave; t&ograve;a ch&iacute;nh tẩm 3 gian. Nối tiền đường v&agrave; trung đường l&agrave; kinh đ&agrave;n (thi&ecirc;u hương) v&agrave; 2 gian tả hữu. V&agrave;o ng&agrave;y 15 th&aacute;ng Gi&ecirc;ng hằng năm diễn ra Lễ hội đền Trần, thu h&uacute;t đ&ocirc;ng đảo du kh&aacute;ch đến tham quan chi&ecirc;m b&aacute;i. Th&agrave;nh cổ Nam Định Nằm ở ph&iacute;a T&acirc;y Bắc th&agrave;nh phố, Th&agrave;nh cổ Nam Định c&oacute; 4 cửa, mỗi cửa lại mở ra một g&oacute;c nh&igrave;n kh&aacute;c với những n&eacute;t đặc trưng ri&ecirc;ng. Đến đ&acirc;y du kh&aacute;ch c&oacute; cơ hội thưởng ngoạn h&agrave;ng loạt c&aacute;c c&ocirc;ng tr&igrave;nh phụ b&ecirc;n trong như: Cột cờ, Trường Thi, Văn Miếu hay đ&igrave;nh Vọng Cung&hellip; nơi in dấu một thời kỳ ph&aacute;t triển v&agrave;ng son - kinh đ&ocirc; thứ 2 của nh&agrave; Trần. Th&agrave;nh cổ Nam Định l&agrave; c&ocirc;ng tr&igrave;nh mang gi&aacute; trị lịch sử, kiến tr&uacute;c độc đ&aacute;o của Nam Định, l&agrave; chứng t&iacute;ch cho l&ograve;ng quả cảm của qu&acirc;n v&agrave; d&acirc;n Th&agrave;nh Nam trong c&aacute;c thời kỳ chống qu&acirc;n x&acirc;m lược.</p>\r\n<p style=\"text-align: justify;\"><strong>Th&aacute;p Phổ Minh </strong></p>\r\n<p style=\"text-align: justify;\">Ch&ugrave;a Phổ Minh (hay c&ograve;n gọi l&agrave; ch&ugrave;a Th&aacute;p) tọa lạc tại th&ocirc;n Tức Mạc, phường Lộc Vượng, TP. Nam Định, nằm c&aacute;ch trung t&acirc;m TP. Nam Định khoảng 5km về ph&iacute;a Bắc l&agrave; một c&ocirc;ng tr&igrave;nh trong khu&ocirc;n vi&ecirc;n ch&ugrave;a Th&aacute;p c&oacute; hơn 700 năm tuổi. Năm 2012, di t&iacute;ch lịch sử v&agrave; kiến tr&uacute;c nghệ thuật đền Trần v&agrave; ch&ugrave;a Phổ Minh được xếp hạng di t&iacute;ch quốc gia đặc biệt. Th&aacute;p được x&acirc;y bằng gạch, bắt mạch để trần kh&ocirc;ng tr&aacute;t, c&oacute; h&igrave;nh vu&ocirc;ng, cao 19,5m với 14 tầng. Mặc d&ugrave; trải qua nhiều lần tu bổ nhưng ch&ugrave;a vẫn giữ được nhiều dấu t&iacute;ch nghệ thuật quan trọng của một thời H&agrave;o kh&iacute; Đ&ocirc;ng A. Đ&acirc;y cũng l&agrave; một trong số &iacute;t c&ocirc;ng tr&igrave;nh đời Trần c&ograve;n giữ được tương đối to&agrave;n vẹn đến ng&agrave;y nay.</p>\r\n<p style=\"text-align: justify;\"><strong>Khu di t&iacute;ch Phủ D&agrave;y </strong></p>\r\n<p style=\"text-align: justify;\">Thuộc địa phận x&atilde; Kim Th&aacute;i, huyện Vụ Bản, nằm c&aacute;ch TP. Nam Định 17km về ph&iacute;a T&acirc;y Nam, Khu di t&iacute;ch Phủ D&agrave;y l&agrave; nơi thờ b&agrave; ch&uacute;a Liễu Hạnh (l&agrave; một vị thần, tượng trưng cho cuộc sống tinh thần, ph&uacute;c đức, sự thịnh vượng) &ndash; một trong &ldquo;tứ bất tử&rdquo; của Việt Nam. Phủ D&agrave;y gồm c&oacute;: phủ Ti&ecirc;n Hương, phủ V&acirc;n C&aacute;t v&agrave; lăng b&agrave; Ch&uacute;a Liễu. Phủ Ti&ecirc;n Hương c&oacute; từ thời L&ecirc; &ndash; Cảnh Trị (1663 &ndash; 1671), gồm 81 gian lớn nhỏ trong tổng số 19 t&ograve;a v&agrave; c&oacute; tất cả 4 cung. C&aacute;c h&igrave;nh họa đi&ecirc;u khắc hết sức tinh vi, bề thế, nơi đ&acirc;y c&ograve;n c&oacute; 5 pho tượng từ thế kỷ thứ XIX. Phủ V&acirc;n C&aacute;t c&oacute; 7 t&ograve;a với 30 gian, c&oacute; Ngọ m&ocirc;n với 5 g&aacute;c lầu. Trước Ngọ m&ocirc;n l&agrave; hồ b&aacute;n nguyệt v&agrave; nh&agrave; thủy l&acirc;u được đặt giữa hồ. Được x&acirc;y dựng bằng đ&aacute; v&agrave;o năm 1938, lăng b&agrave; Ch&uacute;a Liễu mang những đường n&eacute;t chạm trổ tinh tế. C&aacute;c trụ cổng đắp nổi h&igrave;nh b&ocirc;ng sen. Nh&igrave;n lăng từ xa giống như một hồ sen cạn với 60 b&uacute;p sen hồng nổi bật. Đến với Khu di t&iacute;ch Phủ D&agrave;y, du kh&aacute;ch kh&ocirc;ng chỉ c&oacute; dịp h&agrave;nh lễ cầu những điều may mắn m&agrave; c&ograve;n c&oacute; cơ hội thưởng ngoạn cảnh sắc thi&ecirc;n nhi&ecirc;n tươi đẹp, ngắm nh&igrave;n một c&ocirc;ng tr&igrave;nh kiến tr&uacute;c trang nghi&ecirc;m, bề thế.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/V%C6%B0%E1%BB%9Dn%20qu%E1%BB%91c%20gia%20Xu%C3%A2n%20Th%E1%BB%A7y.jpg\" alt=\"\" width=\"550\" height=\"295\" /></p>\r\n<p style=\"text-align: justify;\"><strong>Vườn quốc gia Xu&acirc;n Thủy </strong></p>\r\n<p style=\"text-align: justify;\">Được UNESCO c&ocirc;ng nhận l&agrave; rừng ngập mặn đầu ti&ecirc;n ở Việt Nam theo c&ocirc;ng ước Ramsar v&agrave; rừng ngập mặn thứ 50 của thế giới, Vườn quốc gia Xu&acirc;n Thủy c&oacute; tổng diện t&iacute;ch l&agrave; 12.000 ha, thuộc địa phận x&atilde; Giao Thi&ecirc;n, huyện Giao Thủy. Hệ động thực vật sinh trưởng tại đ&acirc;y rất phong ph&uacute;, c&oacute; nhiều lo&agrave;i thực vật bậc cao v&agrave; động vật qu&yacute; hiếm, khoảng 100 lo&agrave;i chim trong qu&aacute; tr&igrave;nh di cư về phương Nam lưu tr&uacute; tại đ&acirc;y.</p>\r\n<p style=\"text-align: justify;\">Đ&acirc;y cũng l&agrave; điểm du lịch sinh th&aacute;i thu h&uacute;t rất nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước đến tham quan v&agrave; kh&aacute;m ph&aacute;&hellip; Ch&ugrave;a Cổ Lễ Ch&ugrave;a Cổ Lễ được x&acirc;y dựng từ năm 1920, do thiền sư Nguyễn Minh Kh&ocirc;ng (thời L&yacute;) s&aacute;ng lập. Hiện nay, ch&ugrave;a thuộc địa phận thị trấn Cổ Lễ, huyện Trực Ninh. Ch&ugrave;a c&oacute; th&aacute;p 12 tầng được x&acirc;y từ năm 1926 &ndash; 1927. Ngo&agrave;i ra, ch&ugrave;a c&ograve;n lưu giữ được nhiều hiện vật văn h&oacute;a qu&yacute; hiếm thời L&yacute; như trống đồng, t&uacute;i đựng đồng&hellip; v&agrave; đặc biệt l&agrave; đại hồng chung.</p>\r\n<p style=\"text-align: justify;\"><strong>T&ograve;a gi&aacute;m mục B&ugrave;i Chu</strong></p>\r\n<p style=\"text-align: justify;\">Sở hữu rất nhiều nh&agrave; thờ cổ nguy nga với kiến tr&uacute;c đặc sắc, Nam Định được coi l&agrave; &ldquo;thủ phủ&rdquo; của c&aacute;c nh&agrave; thờ tại Việt Nam. T&ograve;a gi&aacute;m mục B&ugrave;i Chu l&agrave; một c&ocirc;ng tr&igrave;nh đạo gi&aacute;o, mang phong c&aacute;ch kiến tr&uacute;c Gothique độc đ&aacute;o, được x&acirc;y dựng từ năm 1885 v&agrave; được coi l&agrave; nơi lưu dấu t&iacute;ch của gi&aacute;o sĩ truyền gi&aacute;o đầu ti&ecirc;n (đầu thế kỷ XVIII). Hiện nay, nơi đ&acirc;y vẫn lưu giữ được nhiều t&agrave;i liệu li&ecirc;n quan đến gi&aacute;o hội, đến việc nhận nu&ocirc;i trẻ mồ c&ocirc;i v&agrave; người khuyết tật&hellip;</p>\r\n<p style=\"text-align: justify;\"><strong>Chợ Viềng </strong></p>\r\n<p style=\"text-align: justify;\">C&ograve;n gọi l&agrave; chợ &Acirc;m Phủ, chợ Viềng l&agrave; phi&ecirc;n chợ v&ocirc; c&ugrave;ng nổi tiếng v&agrave; độc đ&aacute;o chỉ họp duy nhất một lần v&agrave;o đ&ecirc;m mồng 7 đến rạng ng&agrave;y mồng 8 th&aacute;ng Gi&ecirc;ng &Acirc;m lịch. Đ&acirc;y l&agrave; n&eacute;t văn h&oacute;a truyền thống từ xa xưa của v&ugrave;ng. Người d&acirc;n đến với chợ Viềng với &yacute; niệm &ldquo;mua may b&aacute;n rủi&rdquo; cho năm mới được b&igrave;nh an, may mắn. Ch&iacute;nh v&igrave; vậy, người đến chợ kh&ocirc;ng đặt nặng vấn đề lời l&atilde;i, người b&aacute;n kh&ocirc;ng n&oacute;i th&aacute;ch v&agrave; người mua kh&ocirc;ng mặc cả. H&agrave;ng h&oacute;a được b&agrave;y b&aacute;n ở phi&ecirc;n chợ n&agrave;y chủ yếu l&agrave; c&aacute;c loại c&acirc;y trồng, n&ocirc;ng cụ, đồ cổ v&agrave; đồ giả cổ, những vật dụng nh&agrave; n&ocirc;ng, c&aacute;c mặt h&agrave;ng nhu yếu phẩm, những bộ đồ đồng,... đa chủng loại, chất lượng v&agrave; gi&aacute; th&agrave;nh.</p>\r\n<p style=\"text-align: justify;\"><strong>B&atilde;i biển Thịnh Long</strong></p>\r\n<p style=\"text-align: justify;\">Nằm tr&ecirc;n địa phận thị trấn Thịnh Long, huyện Hải Hậu, b&atilde;i biển Thịnh Long l&agrave; một trong những b&atilde;i biển đẹp, hoang sơ của Nam Định với sức quyến rũ kh&ocirc;ng hề nhỏ. B&atilde;i biển n&agrave;y sở hữu bờ c&aacute;t trải d&agrave;i, h&agrave;ng phi lao r&igrave; r&agrave;o trước gi&oacute;, s&oacute;ng biển dập d&igrave;u.</p>\r\n</body>\r\n</html>', '1', '2018-04-12 00:00:00', '2018-08-22 00:00:00', null, null, '2018-04-12 04:14:51', '2018-04-12 17:00:38', '1', 'GT001', null);
INSERT INTO `news` VALUES ('2', '9 điểm đến hấp dẫn ở Nam Định', 'images/news/img-2.jpg', 'Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) là nơi thờ tự 14 vị vua nhà Trần cùng với gia quyến, các vị quan đã có công phù tá nhà Trần…', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\"><strong><img src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/%C4%90%E1%BB%81n%20Tr%E1%BA%A7n.jpg\" alt=\"\" width=\"1000\" height=\"562\" /></strong></p>\r\n<p style=\"text-align: justify;\"><strong>Đền Trần</strong></p>\r\n<p style=\"text-align: justify;\">Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) l&agrave; nơi thờ tự 14 vị vua nh&agrave; Trần c&ugrave;ng với gia quyến, c&aacute;c vị quan đ&atilde; c&oacute; c&ocirc;ng ph&ugrave; t&aacute; nh&agrave; Trần&hellip; v&agrave; l&agrave; một trong những điểm đến m&agrave; rất nhiều du kh&aacute;ch muốn gh&eacute; qua khi đặt ch&acirc;n đến Nam Định. Đền Trần được x&acirc;y dựng v&agrave;o năm 1695 tr&ecirc;n nền Th&aacute;i miếu cũ (tức l&agrave; Phủ Thi&ecirc;n Trường xưa) - nơi được coi l&agrave; ph&aacute;t t&iacute;ch của vương triều nh&agrave; Trần.</p>\r\n<p style=\"text-align: justify;\">Đền bao gồm tổ hợp ba c&ocirc;ng tr&igrave;nh kiến tr&uacute;c: đền Thi&ecirc;n Trường (đền Thượng), đền Cổ Trạch (đền Hạ) v&agrave; đền Tr&ugrave;ng Hoa c&ugrave;ng mang một kiểu kiến tr&uacute;c v&agrave; quy m&ocirc; ngang nhau. Mỗi đền gồm t&ograve;a tiền đường 5 gian, t&ograve;a trung đường 5 gian v&agrave; t&ograve;a ch&iacute;nh tẩm 3 gian. Nối tiền đường v&agrave; trung đường l&agrave; kinh đ&agrave;n (thi&ecirc;u hương) v&agrave; 2 gian tả hữu. V&agrave;o ng&agrave;y 15 th&aacute;ng Gi&ecirc;ng hằng năm diễn ra Lễ hội đền Trần, thu h&uacute;t đ&ocirc;ng đảo du kh&aacute;ch đến tham quan chi&ecirc;m b&aacute;i. Th&agrave;nh cổ Nam Định Nằm ở ph&iacute;a T&acirc;y Bắc th&agrave;nh phố, Th&agrave;nh cổ Nam Định c&oacute; 4 cửa, mỗi cửa lại mở ra một g&oacute;c nh&igrave;n kh&aacute;c với những n&eacute;t đặc trưng ri&ecirc;ng. Đến đ&acirc;y du kh&aacute;ch c&oacute; cơ hội thưởng ngoạn h&agrave;ng loạt c&aacute;c c&ocirc;ng tr&igrave;nh phụ b&ecirc;n trong như: Cột cờ, Trường Thi, Văn Miếu hay đ&igrave;nh Vọng Cung&hellip; nơi in dấu một thời kỳ ph&aacute;t triển v&agrave;ng son - kinh đ&ocirc; thứ 2 của nh&agrave; Trần. Th&agrave;nh cổ Nam Định l&agrave; c&ocirc;ng tr&igrave;nh mang gi&aacute; trị lịch sử, kiến tr&uacute;c độc đ&aacute;o của Nam Định, l&agrave; chứng t&iacute;ch cho l&ograve;ng quả cảm của qu&acirc;n v&agrave; d&acirc;n Th&agrave;nh Nam trong c&aacute;c thời kỳ chống qu&acirc;n x&acirc;m lược.</p>\r\n<p style=\"text-align: justify;\"><strong>Th&aacute;p Phổ Minh </strong></p>\r\n<p style=\"text-align: justify;\">Ch&ugrave;a Phổ Minh (hay c&ograve;n gọi l&agrave; ch&ugrave;a Th&aacute;p) tọa lạc tại th&ocirc;n Tức Mạc, phường Lộc Vượng, TP. Nam Định, nằm c&aacute;ch trung t&acirc;m TP. Nam Định khoảng 5km về ph&iacute;a Bắc l&agrave; một c&ocirc;ng tr&igrave;nh trong khu&ocirc;n vi&ecirc;n ch&ugrave;a Th&aacute;p c&oacute; hơn 700 năm tuổi. Năm 2012, di t&iacute;ch lịch sử v&agrave; kiến tr&uacute;c nghệ thuật đền Trần v&agrave; ch&ugrave;a Phổ Minh được xếp hạng di t&iacute;ch quốc gia đặc biệt. Th&aacute;p được x&acirc;y bằng gạch, bắt mạch để trần kh&ocirc;ng tr&aacute;t, c&oacute; h&igrave;nh vu&ocirc;ng, cao 19,5m với 14 tầng. Mặc d&ugrave; trải qua nhiều lần tu bổ nhưng ch&ugrave;a vẫn giữ được nhiều dấu t&iacute;ch nghệ thuật quan trọng của một thời H&agrave;o kh&iacute; Đ&ocirc;ng A. Đ&acirc;y cũng l&agrave; một trong số &iacute;t c&ocirc;ng tr&igrave;nh đời Trần c&ograve;n giữ được tương đối to&agrave;n vẹn đến ng&agrave;y nay.</p>\r\n<p style=\"text-align: justify;\"><strong>Khu di t&iacute;ch Phủ D&agrave;y </strong></p>\r\n<p style=\"text-align: justify;\">Thuộc địa phận x&atilde; Kim Th&aacute;i, huyện Vụ Bản, nằm c&aacute;ch TP. Nam Định 17km về ph&iacute;a T&acirc;y Nam, Khu di t&iacute;ch Phủ D&agrave;y l&agrave; nơi thờ b&agrave; ch&uacute;a Liễu Hạnh (l&agrave; một vị thần, tượng trưng cho cuộc sống tinh thần, ph&uacute;c đức, sự thịnh vượng) &ndash; một trong &ldquo;tứ bất tử&rdquo; của Việt Nam. Phủ D&agrave;y gồm c&oacute;: phủ Ti&ecirc;n Hương, phủ V&acirc;n C&aacute;t v&agrave; lăng b&agrave; Ch&uacute;a Liễu. Phủ Ti&ecirc;n Hương c&oacute; từ thời L&ecirc; &ndash; Cảnh Trị (1663 &ndash; 1671), gồm 81 gian lớn nhỏ trong tổng số 19 t&ograve;a v&agrave; c&oacute; tất cả 4 cung. C&aacute;c h&igrave;nh họa đi&ecirc;u khắc hết sức tinh vi, bề thế, nơi đ&acirc;y c&ograve;n c&oacute; 5 pho tượng từ thế kỷ thứ XIX. Phủ V&acirc;n C&aacute;t c&oacute; 7 t&ograve;a với 30 gian, c&oacute; Ngọ m&ocirc;n với 5 g&aacute;c lầu. Trước Ngọ m&ocirc;n l&agrave; hồ b&aacute;n nguyệt v&agrave; nh&agrave; thủy l&acirc;u được đặt giữa hồ. Được x&acirc;y dựng bằng đ&aacute; v&agrave;o năm 1938, lăng b&agrave; Ch&uacute;a Liễu mang những đường n&eacute;t chạm trổ tinh tế. C&aacute;c trụ cổng đắp nổi h&igrave;nh b&ocirc;ng sen. Nh&igrave;n lăng từ xa giống như một hồ sen cạn với 60 b&uacute;p sen hồng nổi bật. Đến với Khu di t&iacute;ch Phủ D&agrave;y, du kh&aacute;ch kh&ocirc;ng chỉ c&oacute; dịp h&agrave;nh lễ cầu những điều may mắn m&agrave; c&ograve;n c&oacute; cơ hội thưởng ngoạn cảnh sắc thi&ecirc;n nhi&ecirc;n tươi đẹp, ngắm nh&igrave;n một c&ocirc;ng tr&igrave;nh kiến tr&uacute;c trang nghi&ecirc;m, bề thế.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/V%C6%B0%E1%BB%9Dn%20qu%E1%BB%91c%20gia%20Xu%C3%A2n%20Th%E1%BB%A7y.jpg\" alt=\"\" width=\"550\" height=\"295\" /></p>\r\n<p style=\"text-align: justify;\"><strong>Vườn quốc gia Xu&acirc;n Thủy </strong></p>\r\n<p style=\"text-align: justify;\">Được UNESCO c&ocirc;ng nhận l&agrave; rừng ngập mặn đầu ti&ecirc;n ở Việt Nam theo c&ocirc;ng ước Ramsar v&agrave; rừng ngập mặn thứ 50 của thế giới, Vườn quốc gia Xu&acirc;n Thủy c&oacute; tổng diện t&iacute;ch l&agrave; 12.000 ha, thuộc địa phận x&atilde; Giao Thi&ecirc;n, huyện Giao Thủy. Hệ động thực vật sinh trưởng tại đ&acirc;y rất phong ph&uacute;, c&oacute; nhiều lo&agrave;i thực vật bậc cao v&agrave; động vật qu&yacute; hiếm, khoảng 100 lo&agrave;i chim trong qu&aacute; tr&igrave;nh di cư về phương Nam lưu tr&uacute; tại đ&acirc;y.</p>\r\n<p style=\"text-align: justify;\">Đ&acirc;y cũng l&agrave; điểm du lịch sinh th&aacute;i thu h&uacute;t rất nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước đến tham quan v&agrave; kh&aacute;m ph&aacute;&hellip; Ch&ugrave;a Cổ Lễ Ch&ugrave;a Cổ Lễ được x&acirc;y dựng từ năm 1920, do thiền sư Nguyễn Minh Kh&ocirc;ng (thời L&yacute;) s&aacute;ng lập. Hiện nay, ch&ugrave;a thuộc địa phận thị trấn Cổ Lễ, huyện Trực Ninh. Ch&ugrave;a c&oacute; th&aacute;p 12 tầng được x&acirc;y từ năm 1926 &ndash; 1927. Ngo&agrave;i ra, ch&ugrave;a c&ograve;n lưu giữ được nhiều hiện vật văn h&oacute;a qu&yacute; hiếm thời L&yacute; như trống đồng, t&uacute;i đựng đồng&hellip; v&agrave; đặc biệt l&agrave; đại hồng chung.</p>\r\n<p style=\"text-align: justify;\"><strong>T&ograve;a gi&aacute;m mục B&ugrave;i Chu</strong></p>\r\n<p style=\"text-align: justify;\">Sở hữu rất nhiều nh&agrave; thờ cổ nguy nga với kiến tr&uacute;c đặc sắc, Nam Định được coi l&agrave; &ldquo;thủ phủ&rdquo; của c&aacute;c nh&agrave; thờ tại Việt Nam. T&ograve;a gi&aacute;m mục B&ugrave;i Chu l&agrave; một c&ocirc;ng tr&igrave;nh đạo gi&aacute;o, mang phong c&aacute;ch kiến tr&uacute;c Gothique độc đ&aacute;o, được x&acirc;y dựng từ năm 1885 v&agrave; được coi l&agrave; nơi lưu dấu t&iacute;ch của gi&aacute;o sĩ truyền gi&aacute;o đầu ti&ecirc;n (đầu thế kỷ XVIII). Hiện nay, nơi đ&acirc;y vẫn lưu giữ được nhiều t&agrave;i liệu li&ecirc;n quan đến gi&aacute;o hội, đến việc nhận nu&ocirc;i trẻ mồ c&ocirc;i v&agrave; người khuyết tật&hellip;</p>\r\n<p style=\"text-align: justify;\"><strong>Chợ Viềng </strong></p>\r\n<p style=\"text-align: justify;\">C&ograve;n gọi l&agrave; chợ &Acirc;m Phủ, chợ Viềng l&agrave; phi&ecirc;n chợ v&ocirc; c&ugrave;ng nổi tiếng v&agrave; độc đ&aacute;o chỉ họp duy nhất một lần v&agrave;o đ&ecirc;m mồng 7 đến rạng ng&agrave;y mồng 8 th&aacute;ng Gi&ecirc;ng &Acirc;m lịch. Đ&acirc;y l&agrave; n&eacute;t văn h&oacute;a truyền thống từ xa xưa của v&ugrave;ng. Người d&acirc;n đến với chợ Viềng với &yacute; niệm &ldquo;mua may b&aacute;n rủi&rdquo; cho năm mới được b&igrave;nh an, may mắn. Ch&iacute;nh v&igrave; vậy, người đến chợ kh&ocirc;ng đặt nặng vấn đề lời l&atilde;i, người b&aacute;n kh&ocirc;ng n&oacute;i th&aacute;ch v&agrave; người mua kh&ocirc;ng mặc cả. H&agrave;ng h&oacute;a được b&agrave;y b&aacute;n ở phi&ecirc;n chợ n&agrave;y chủ yếu l&agrave; c&aacute;c loại c&acirc;y trồng, n&ocirc;ng cụ, đồ cổ v&agrave; đồ giả cổ, những vật dụng nh&agrave; n&ocirc;ng, c&aacute;c mặt h&agrave;ng nhu yếu phẩm, những bộ đồ đồng,... đa chủng loại, chất lượng v&agrave; gi&aacute; th&agrave;nh.</p>\r\n<p style=\"text-align: justify;\"><strong>B&atilde;i biển Thịnh Long</strong></p>\r\n<p style=\"text-align: justify;\">Nằm tr&ecirc;n địa phận thị trấn Thịnh Long, huyện Hải Hậu, b&atilde;i biển Thịnh Long l&agrave; một trong những b&atilde;i biển đẹp, hoang sơ của Nam Định với sức quyến rũ kh&ocirc;ng hề nhỏ. B&atilde;i biển n&agrave;y sở hữu bờ c&aacute;t trải d&agrave;i, h&agrave;ng phi lao r&igrave; r&agrave;o trước gi&oacute;, s&oacute;ng biển dập d&igrave;u.</p>\r\n</body>\r\n</html>', '1', '2018-04-12 00:00:00', '2018-05-03 00:00:00', null, null, '2018-04-12 04:16:30', '2018-04-12 17:00:26', '1', 'PT001', null);
INSERT INTO `news` VALUES ('3', '9 điểm đến hấp dẫn ở Nam Định', 'images/news/img-3.jpg', 'Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) là nơi thờ tự 14 vị vua nhà Trần cùng với gia quyến, các vị quan đã có công phù tá nhà Trần…', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\"><strong><img src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/%C4%90%E1%BB%81n%20Tr%E1%BA%A7n.jpg\" alt=\"\" width=\"1000\" height=\"562\" /></strong></p>\r\n<p style=\"text-align: justify;\"><strong>Đền Trần</strong></p>\r\n<p style=\"text-align: justify;\">Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) l&agrave; nơi thờ tự 14 vị vua nh&agrave; Trần c&ugrave;ng với gia quyến, c&aacute;c vị quan đ&atilde; c&oacute; c&ocirc;ng ph&ugrave; t&aacute; nh&agrave; Trần&hellip; v&agrave; l&agrave; một trong những điểm đến m&agrave; rất nhiều du kh&aacute;ch muốn gh&eacute; qua khi đặt ch&acirc;n đến Nam Định. Đền Trần được x&acirc;y dựng v&agrave;o năm 1695 tr&ecirc;n nền Th&aacute;i miếu cũ (tức l&agrave; Phủ Thi&ecirc;n Trường xưa) - nơi được coi l&agrave; ph&aacute;t t&iacute;ch của vương triều nh&agrave; Trần.</p>\r\n<p style=\"text-align: justify;\">Đền bao gồm tổ hợp ba c&ocirc;ng tr&igrave;nh kiến tr&uacute;c: đền Thi&ecirc;n Trường (đền Thượng), đền Cổ Trạch (đền Hạ) v&agrave; đền Tr&ugrave;ng Hoa c&ugrave;ng mang một kiểu kiến tr&uacute;c v&agrave; quy m&ocirc; ngang nhau. Mỗi đền gồm t&ograve;a tiền đường 5 gian, t&ograve;a trung đường 5 gian v&agrave; t&ograve;a ch&iacute;nh tẩm 3 gian. Nối tiền đường v&agrave; trung đường l&agrave; kinh đ&agrave;n (thi&ecirc;u hương) v&agrave; 2 gian tả hữu. V&agrave;o ng&agrave;y 15 th&aacute;ng Gi&ecirc;ng hằng năm diễn ra Lễ hội đền Trần, thu h&uacute;t đ&ocirc;ng đảo du kh&aacute;ch đến tham quan chi&ecirc;m b&aacute;i. Th&agrave;nh cổ Nam Định Nằm ở ph&iacute;a T&acirc;y Bắc th&agrave;nh phố, Th&agrave;nh cổ Nam Định c&oacute; 4 cửa, mỗi cửa lại mở ra một g&oacute;c nh&igrave;n kh&aacute;c với những n&eacute;t đặc trưng ri&ecirc;ng. Đến đ&acirc;y du kh&aacute;ch c&oacute; cơ hội thưởng ngoạn h&agrave;ng loạt c&aacute;c c&ocirc;ng tr&igrave;nh phụ b&ecirc;n trong như: Cột cờ, Trường Thi, Văn Miếu hay đ&igrave;nh Vọng Cung&hellip; nơi in dấu một thời kỳ ph&aacute;t triển v&agrave;ng son - kinh đ&ocirc; thứ 2 của nh&agrave; Trần. Th&agrave;nh cổ Nam Định l&agrave; c&ocirc;ng tr&igrave;nh mang gi&aacute; trị lịch sử, kiến tr&uacute;c độc đ&aacute;o của Nam Định, l&agrave; chứng t&iacute;ch cho l&ograve;ng quả cảm của qu&acirc;n v&agrave; d&acirc;n Th&agrave;nh Nam trong c&aacute;c thời kỳ chống qu&acirc;n x&acirc;m lược.</p>\r\n<p style=\"text-align: justify;\"><strong>Th&aacute;p Phổ Minh </strong></p>\r\n<p style=\"text-align: justify;\">Ch&ugrave;a Phổ Minh (hay c&ograve;n gọi l&agrave; ch&ugrave;a Th&aacute;p) tọa lạc tại th&ocirc;n Tức Mạc, phường Lộc Vượng, TP. Nam Định, nằm c&aacute;ch trung t&acirc;m TP. Nam Định khoảng 5km về ph&iacute;a Bắc l&agrave; một c&ocirc;ng tr&igrave;nh trong khu&ocirc;n vi&ecirc;n ch&ugrave;a Th&aacute;p c&oacute; hơn 700 năm tuổi. Năm 2012, di t&iacute;ch lịch sử v&agrave; kiến tr&uacute;c nghệ thuật đền Trần v&agrave; ch&ugrave;a Phổ Minh được xếp hạng di t&iacute;ch quốc gia đặc biệt. Th&aacute;p được x&acirc;y bằng gạch, bắt mạch để trần kh&ocirc;ng tr&aacute;t, c&oacute; h&igrave;nh vu&ocirc;ng, cao 19,5m với 14 tầng. Mặc d&ugrave; trải qua nhiều lần tu bổ nhưng ch&ugrave;a vẫn giữ được nhiều dấu t&iacute;ch nghệ thuật quan trọng của một thời H&agrave;o kh&iacute; Đ&ocirc;ng A. Đ&acirc;y cũng l&agrave; một trong số &iacute;t c&ocirc;ng tr&igrave;nh đời Trần c&ograve;n giữ được tương đối to&agrave;n vẹn đến ng&agrave;y nay.</p>\r\n<p style=\"text-align: justify;\"><strong>Khu di t&iacute;ch Phủ D&agrave;y </strong></p>\r\n<p style=\"text-align: justify;\">Thuộc địa phận x&atilde; Kim Th&aacute;i, huyện Vụ Bản, nằm c&aacute;ch TP. Nam Định 17km về ph&iacute;a T&acirc;y Nam, Khu di t&iacute;ch Phủ D&agrave;y l&agrave; nơi thờ b&agrave; ch&uacute;a Liễu Hạnh (l&agrave; một vị thần, tượng trưng cho cuộc sống tinh thần, ph&uacute;c đức, sự thịnh vượng) &ndash; một trong &ldquo;tứ bất tử&rdquo; của Việt Nam. Phủ D&agrave;y gồm c&oacute;: phủ Ti&ecirc;n Hương, phủ V&acirc;n C&aacute;t v&agrave; lăng b&agrave; Ch&uacute;a Liễu. Phủ Ti&ecirc;n Hương c&oacute; từ thời L&ecirc; &ndash; Cảnh Trị (1663 &ndash; 1671), gồm 81 gian lớn nhỏ trong tổng số 19 t&ograve;a v&agrave; c&oacute; tất cả 4 cung. C&aacute;c h&igrave;nh họa đi&ecirc;u khắc hết sức tinh vi, bề thế, nơi đ&acirc;y c&ograve;n c&oacute; 5 pho tượng từ thế kỷ thứ XIX. Phủ V&acirc;n C&aacute;t c&oacute; 7 t&ograve;a với 30 gian, c&oacute; Ngọ m&ocirc;n với 5 g&aacute;c lầu. Trước Ngọ m&ocirc;n l&agrave; hồ b&aacute;n nguyệt v&agrave; nh&agrave; thủy l&acirc;u được đặt giữa hồ. Được x&acirc;y dựng bằng đ&aacute; v&agrave;o năm 1938, lăng b&agrave; Ch&uacute;a Liễu mang những đường n&eacute;t chạm trổ tinh tế. C&aacute;c trụ cổng đắp nổi h&igrave;nh b&ocirc;ng sen. Nh&igrave;n lăng từ xa giống như một hồ sen cạn với 60 b&uacute;p sen hồng nổi bật. Đến với Khu di t&iacute;ch Phủ D&agrave;y, du kh&aacute;ch kh&ocirc;ng chỉ c&oacute; dịp h&agrave;nh lễ cầu những điều may mắn m&agrave; c&ograve;n c&oacute; cơ hội thưởng ngoạn cảnh sắc thi&ecirc;n nhi&ecirc;n tươi đẹp, ngắm nh&igrave;n một c&ocirc;ng tr&igrave;nh kiến tr&uacute;c trang nghi&ecirc;m, bề thế.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/V%C6%B0%E1%BB%9Dn%20qu%E1%BB%91c%20gia%20Xu%C3%A2n%20Th%E1%BB%A7y.jpg\" alt=\"\" width=\"550\" height=\"295\" /></p>\r\n<p style=\"text-align: justify;\"><strong>Vườn quốc gia Xu&acirc;n Thủy </strong></p>\r\n<p style=\"text-align: justify;\">Được UNESCO c&ocirc;ng nhận l&agrave; rừng ngập mặn đầu ti&ecirc;n ở Việt Nam theo c&ocirc;ng ước Ramsar v&agrave; rừng ngập mặn thứ 50 của thế giới, Vườn quốc gia Xu&acirc;n Thủy c&oacute; tổng diện t&iacute;ch l&agrave; 12.000 ha, thuộc địa phận x&atilde; Giao Thi&ecirc;n, huyện Giao Thủy. Hệ động thực vật sinh trưởng tại đ&acirc;y rất phong ph&uacute;, c&oacute; nhiều lo&agrave;i thực vật bậc cao v&agrave; động vật qu&yacute; hiếm, khoảng 100 lo&agrave;i chim trong qu&aacute; tr&igrave;nh di cư về phương Nam lưu tr&uacute; tại đ&acirc;y.</p>\r\n<p style=\"text-align: justify;\">Đ&acirc;y cũng l&agrave; điểm du lịch sinh th&aacute;i thu h&uacute;t rất nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước đến tham quan v&agrave; kh&aacute;m ph&aacute;&hellip; Ch&ugrave;a Cổ Lễ Ch&ugrave;a Cổ Lễ được x&acirc;y dựng từ năm 1920, do thiền sư Nguyễn Minh Kh&ocirc;ng (thời L&yacute;) s&aacute;ng lập. Hiện nay, ch&ugrave;a thuộc địa phận thị trấn Cổ Lễ, huyện Trực Ninh. Ch&ugrave;a c&oacute; th&aacute;p 12 tầng được x&acirc;y từ năm 1926 &ndash; 1927. Ngo&agrave;i ra, ch&ugrave;a c&ograve;n lưu giữ được nhiều hiện vật văn h&oacute;a qu&yacute; hiếm thời L&yacute; như trống đồng, t&uacute;i đựng đồng&hellip; v&agrave; đặc biệt l&agrave; đại hồng chung.</p>\r\n<p style=\"text-align: justify;\"><strong>T&ograve;a gi&aacute;m mục B&ugrave;i Chu</strong></p>\r\n<p style=\"text-align: justify;\">Sở hữu rất nhiều nh&agrave; thờ cổ nguy nga với kiến tr&uacute;c đặc sắc, Nam Định được coi l&agrave; &ldquo;thủ phủ&rdquo; của c&aacute;c nh&agrave; thờ tại Việt Nam. T&ograve;a gi&aacute;m mục B&ugrave;i Chu l&agrave; một c&ocirc;ng tr&igrave;nh đạo gi&aacute;o, mang phong c&aacute;ch kiến tr&uacute;c Gothique độc đ&aacute;o, được x&acirc;y dựng từ năm 1885 v&agrave; được coi l&agrave; nơi lưu dấu t&iacute;ch của gi&aacute;o sĩ truyền gi&aacute;o đầu ti&ecirc;n (đầu thế kỷ XVIII). Hiện nay, nơi đ&acirc;y vẫn lưu giữ được nhiều t&agrave;i liệu li&ecirc;n quan đến gi&aacute;o hội, đến việc nhận nu&ocirc;i trẻ mồ c&ocirc;i v&agrave; người khuyết tật&hellip;</p>\r\n<p style=\"text-align: justify;\"><strong>Chợ Viềng </strong></p>\r\n<p style=\"text-align: justify;\">C&ograve;n gọi l&agrave; chợ &Acirc;m Phủ, chợ Viềng l&agrave; phi&ecirc;n chợ v&ocirc; c&ugrave;ng nổi tiếng v&agrave; độc đ&aacute;o chỉ họp duy nhất một lần v&agrave;o đ&ecirc;m mồng 7 đến rạng ng&agrave;y mồng 8 th&aacute;ng Gi&ecirc;ng &Acirc;m lịch. Đ&acirc;y l&agrave; n&eacute;t văn h&oacute;a truyền thống từ xa xưa của v&ugrave;ng. Người d&acirc;n đến với chợ Viềng với &yacute; niệm &ldquo;mua may b&aacute;n rủi&rdquo; cho năm mới được b&igrave;nh an, may mắn. Ch&iacute;nh v&igrave; vậy, người đến chợ kh&ocirc;ng đặt nặng vấn đề lời l&atilde;i, người b&aacute;n kh&ocirc;ng n&oacute;i th&aacute;ch v&agrave; người mua kh&ocirc;ng mặc cả. H&agrave;ng h&oacute;a được b&agrave;y b&aacute;n ở phi&ecirc;n chợ n&agrave;y chủ yếu l&agrave; c&aacute;c loại c&acirc;y trồng, n&ocirc;ng cụ, đồ cổ v&agrave; đồ giả cổ, những vật dụng nh&agrave; n&ocirc;ng, c&aacute;c mặt h&agrave;ng nhu yếu phẩm, những bộ đồ đồng,... đa chủng loại, chất lượng v&agrave; gi&aacute; th&agrave;nh.</p>\r\n<p style=\"text-align: justify;\"><strong>B&atilde;i biển Thịnh Long</strong></p>\r\n<p style=\"text-align: justify;\">Nằm tr&ecirc;n địa phận thị trấn Thịnh Long, huyện Hải Hậu, b&atilde;i biển Thịnh Long l&agrave; một trong những b&atilde;i biển đẹp, hoang sơ của Nam Định với sức quyến rũ kh&ocirc;ng hề nhỏ. B&atilde;i biển n&agrave;y sở hữu bờ c&aacute;t trải d&agrave;i, h&agrave;ng phi lao r&igrave; r&agrave;o trước gi&oacute;, s&oacute;ng biển dập d&igrave;u.</p>\r\n</body>\r\n</html>', '1', '2018-04-12 00:00:00', '2018-08-30 00:00:00', null, null, '2018-04-12 04:16:57', '2018-04-12 17:00:04', '1', 'PT001', null);
INSERT INTO `news` VALUES ('4', '9 điểm đến hấp dẫn ở Nam Định', 'images/news/news1.jpg', 'Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) là nơi thờ tự 14 vị vua nhà Trần cùng với gia quyến, các vị quan đã có công phù tá nhà Trần…', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\"><strong><img src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/%C4%90%E1%BB%81n%20Tr%E1%BA%A7n.jpg\" alt=\"\" width=\"1000\" height=\"562\" /></strong></p>\r\n<p style=\"text-align: justify;\"><strong>Đền Trần</strong></p>\r\n<p style=\"text-align: justify;\">Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) l&agrave; nơi thờ tự 14 vị vua nh&agrave; Trần c&ugrave;ng với gia quyến, c&aacute;c vị quan đ&atilde; c&oacute; c&ocirc;ng ph&ugrave; t&aacute; nh&agrave; Trần&hellip; v&agrave; l&agrave; một trong những điểm đến m&agrave; rất nhiều du kh&aacute;ch muốn gh&eacute; qua khi đặt ch&acirc;n đến Nam Định. Đền Trần được x&acirc;y dựng v&agrave;o năm 1695 tr&ecirc;n nền Th&aacute;i miếu cũ (tức l&agrave; Phủ Thi&ecirc;n Trường xưa) - nơi được coi l&agrave; ph&aacute;t t&iacute;ch của vương triều nh&agrave; Trần.</p>\r\n<p style=\"text-align: justify;\">Đền bao gồm tổ hợp ba c&ocirc;ng tr&igrave;nh kiến tr&uacute;c: đền Thi&ecirc;n Trường (đền Thượng), đền Cổ Trạch (đền Hạ) v&agrave; đền Tr&ugrave;ng Hoa c&ugrave;ng mang một kiểu kiến tr&uacute;c v&agrave; quy m&ocirc; ngang nhau. Mỗi đền gồm t&ograve;a tiền đường 5 gian, t&ograve;a trung đường 5 gian v&agrave; t&ograve;a ch&iacute;nh tẩm 3 gian. Nối tiền đường v&agrave; trung đường l&agrave; kinh đ&agrave;n (thi&ecirc;u hương) v&agrave; 2 gian tả hữu. V&agrave;o ng&agrave;y 15 th&aacute;ng Gi&ecirc;ng hằng năm diễn ra Lễ hội đền Trần, thu h&uacute;t đ&ocirc;ng đảo du kh&aacute;ch đến tham quan chi&ecirc;m b&aacute;i. Th&agrave;nh cổ Nam Định Nằm ở ph&iacute;a T&acirc;y Bắc th&agrave;nh phố, Th&agrave;nh cổ Nam Định c&oacute; 4 cửa, mỗi cửa lại mở ra một g&oacute;c nh&igrave;n kh&aacute;c với những n&eacute;t đặc trưng ri&ecirc;ng. Đến đ&acirc;y du kh&aacute;ch c&oacute; cơ hội thưởng ngoạn h&agrave;ng loạt c&aacute;c c&ocirc;ng tr&igrave;nh phụ b&ecirc;n trong như: Cột cờ, Trường Thi, Văn Miếu hay đ&igrave;nh Vọng Cung&hellip; nơi in dấu một thời kỳ ph&aacute;t triển v&agrave;ng son - kinh đ&ocirc; thứ 2 của nh&agrave; Trần. Th&agrave;nh cổ Nam Định l&agrave; c&ocirc;ng tr&igrave;nh mang gi&aacute; trị lịch sử, kiến tr&uacute;c độc đ&aacute;o của Nam Định, l&agrave; chứng t&iacute;ch cho l&ograve;ng quả cảm của qu&acirc;n v&agrave; d&acirc;n Th&agrave;nh Nam trong c&aacute;c thời kỳ chống qu&acirc;n x&acirc;m lược.</p>\r\n<p style=\"text-align: justify;\"><strong>Th&aacute;p Phổ Minh </strong></p>\r\n<p style=\"text-align: justify;\">Ch&ugrave;a Phổ Minh (hay c&ograve;n gọi l&agrave; ch&ugrave;a Th&aacute;p) tọa lạc tại th&ocirc;n Tức Mạc, phường Lộc Vượng, TP. Nam Định, nằm c&aacute;ch trung t&acirc;m TP. Nam Định khoảng 5km về ph&iacute;a Bắc l&agrave; một c&ocirc;ng tr&igrave;nh trong khu&ocirc;n vi&ecirc;n ch&ugrave;a Th&aacute;p c&oacute; hơn 700 năm tuổi. Năm 2012, di t&iacute;ch lịch sử v&agrave; kiến tr&uacute;c nghệ thuật đền Trần v&agrave; ch&ugrave;a Phổ Minh được xếp hạng di t&iacute;ch quốc gia đặc biệt. Th&aacute;p được x&acirc;y bằng gạch, bắt mạch để trần kh&ocirc;ng tr&aacute;t, c&oacute; h&igrave;nh vu&ocirc;ng, cao 19,5m với 14 tầng. Mặc d&ugrave; trải qua nhiều lần tu bổ nhưng ch&ugrave;a vẫn giữ được nhiều dấu t&iacute;ch nghệ thuật quan trọng của một thời H&agrave;o kh&iacute; Đ&ocirc;ng A. Đ&acirc;y cũng l&agrave; một trong số &iacute;t c&ocirc;ng tr&igrave;nh đời Trần c&ograve;n giữ được tương đối to&agrave;n vẹn đến ng&agrave;y nay.</p>\r\n<p style=\"text-align: justify;\"><strong>Khu di t&iacute;ch Phủ D&agrave;y </strong></p>\r\n<p style=\"text-align: justify;\">Thuộc địa phận x&atilde; Kim Th&aacute;i, huyện Vụ Bản, nằm c&aacute;ch TP. Nam Định 17km về ph&iacute;a T&acirc;y Nam, Khu di t&iacute;ch Phủ D&agrave;y l&agrave; nơi thờ b&agrave; ch&uacute;a Liễu Hạnh (l&agrave; một vị thần, tượng trưng cho cuộc sống tinh thần, ph&uacute;c đức, sự thịnh vượng) &ndash; một trong &ldquo;tứ bất tử&rdquo; của Việt Nam. Phủ D&agrave;y gồm c&oacute;: phủ Ti&ecirc;n Hương, phủ V&acirc;n C&aacute;t v&agrave; lăng b&agrave; Ch&uacute;a Liễu. Phủ Ti&ecirc;n Hương c&oacute; từ thời L&ecirc; &ndash; Cảnh Trị (1663 &ndash; 1671), gồm 81 gian lớn nhỏ trong tổng số 19 t&ograve;a v&agrave; c&oacute; tất cả 4 cung. C&aacute;c h&igrave;nh họa đi&ecirc;u khắc hết sức tinh vi, bề thế, nơi đ&acirc;y c&ograve;n c&oacute; 5 pho tượng từ thế kỷ thứ XIX. Phủ V&acirc;n C&aacute;t c&oacute; 7 t&ograve;a với 30 gian, c&oacute; Ngọ m&ocirc;n với 5 g&aacute;c lầu. Trước Ngọ m&ocirc;n l&agrave; hồ b&aacute;n nguyệt v&agrave; nh&agrave; thủy l&acirc;u được đặt giữa hồ. Được x&acirc;y dựng bằng đ&aacute; v&agrave;o năm 1938, lăng b&agrave; Ch&uacute;a Liễu mang những đường n&eacute;t chạm trổ tinh tế. C&aacute;c trụ cổng đắp nổi h&igrave;nh b&ocirc;ng sen. Nh&igrave;n lăng từ xa giống như một hồ sen cạn với 60 b&uacute;p sen hồng nổi bật. Đến với Khu di t&iacute;ch Phủ D&agrave;y, du kh&aacute;ch kh&ocirc;ng chỉ c&oacute; dịp h&agrave;nh lễ cầu những điều may mắn m&agrave; c&ograve;n c&oacute; cơ hội thưởng ngoạn cảnh sắc thi&ecirc;n nhi&ecirc;n tươi đẹp, ngắm nh&igrave;n một c&ocirc;ng tr&igrave;nh kiến tr&uacute;c trang nghi&ecirc;m, bề thế.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/V%C6%B0%E1%BB%9Dn%20qu%E1%BB%91c%20gia%20Xu%C3%A2n%20Th%E1%BB%A7y.jpg\" alt=\"\" width=\"550\" height=\"295\" /></p>\r\n<p style=\"text-align: justify;\"><strong>Vườn quốc gia Xu&acirc;n Thủy </strong></p>\r\n<p style=\"text-align: justify;\">Được UNESCO c&ocirc;ng nhận l&agrave; rừng ngập mặn đầu ti&ecirc;n ở Việt Nam theo c&ocirc;ng ước Ramsar v&agrave; rừng ngập mặn thứ 50 của thế giới, Vườn quốc gia Xu&acirc;n Thủy c&oacute; tổng diện t&iacute;ch l&agrave; 12.000 ha, thuộc địa phận x&atilde; Giao Thi&ecirc;n, huyện Giao Thủy. Hệ động thực vật sinh trưởng tại đ&acirc;y rất phong ph&uacute;, c&oacute; nhiều lo&agrave;i thực vật bậc cao v&agrave; động vật qu&yacute; hiếm, khoảng 100 lo&agrave;i chim trong qu&aacute; tr&igrave;nh di cư về phương Nam lưu tr&uacute; tại đ&acirc;y.</p>\r\n<p style=\"text-align: justify;\">Đ&acirc;y cũng l&agrave; điểm du lịch sinh th&aacute;i thu h&uacute;t rất nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước đến tham quan v&agrave; kh&aacute;m ph&aacute;&hellip; Ch&ugrave;a Cổ Lễ Ch&ugrave;a Cổ Lễ được x&acirc;y dựng từ năm 1920, do thiền sư Nguyễn Minh Kh&ocirc;ng (thời L&yacute;) s&aacute;ng lập. Hiện nay, ch&ugrave;a thuộc địa phận thị trấn Cổ Lễ, huyện Trực Ninh. Ch&ugrave;a c&oacute; th&aacute;p 12 tầng được x&acirc;y từ năm 1926 &ndash; 1927. Ngo&agrave;i ra, ch&ugrave;a c&ograve;n lưu giữ được nhiều hiện vật văn h&oacute;a qu&yacute; hiếm thời L&yacute; như trống đồng, t&uacute;i đựng đồng&hellip; v&agrave; đặc biệt l&agrave; đại hồng chung.</p>\r\n<p style=\"text-align: justify;\"><strong>T&ograve;a gi&aacute;m mục B&ugrave;i Chu</strong></p>\r\n<p style=\"text-align: justify;\">Sở hữu rất nhiều nh&agrave; thờ cổ nguy nga với kiến tr&uacute;c đặc sắc, Nam Định được coi l&agrave; &ldquo;thủ phủ&rdquo; của c&aacute;c nh&agrave; thờ tại Việt Nam. T&ograve;a gi&aacute;m mục B&ugrave;i Chu l&agrave; một c&ocirc;ng tr&igrave;nh đạo gi&aacute;o, mang phong c&aacute;ch kiến tr&uacute;c Gothique độc đ&aacute;o, được x&acirc;y dựng từ năm 1885 v&agrave; được coi l&agrave; nơi lưu dấu t&iacute;ch của gi&aacute;o sĩ truyền gi&aacute;o đầu ti&ecirc;n (đầu thế kỷ XVIII). Hiện nay, nơi đ&acirc;y vẫn lưu giữ được nhiều t&agrave;i liệu li&ecirc;n quan đến gi&aacute;o hội, đến việc nhận nu&ocirc;i trẻ mồ c&ocirc;i v&agrave; người khuyết tật&hellip;</p>\r\n<p style=\"text-align: justify;\"><strong>Chợ Viềng </strong></p>\r\n<p style=\"text-align: justify;\">C&ograve;n gọi l&agrave; chợ &Acirc;m Phủ, chợ Viềng l&agrave; phi&ecirc;n chợ v&ocirc; c&ugrave;ng nổi tiếng v&agrave; độc đ&aacute;o chỉ họp duy nhất một lần v&agrave;o đ&ecirc;m mồng 7 đến rạng ng&agrave;y mồng 8 th&aacute;ng Gi&ecirc;ng &Acirc;m lịch. Đ&acirc;y l&agrave; n&eacute;t văn h&oacute;a truyền thống từ xa xưa của v&ugrave;ng. Người d&acirc;n đến với chợ Viềng với &yacute; niệm &ldquo;mua may b&aacute;n rủi&rdquo; cho năm mới được b&igrave;nh an, may mắn. Ch&iacute;nh v&igrave; vậy, người đến chợ kh&ocirc;ng đặt nặng vấn đề lời l&atilde;i, người b&aacute;n kh&ocirc;ng n&oacute;i th&aacute;ch v&agrave; người mua kh&ocirc;ng mặc cả. H&agrave;ng h&oacute;a được b&agrave;y b&aacute;n ở phi&ecirc;n chợ n&agrave;y chủ yếu l&agrave; c&aacute;c loại c&acirc;y trồng, n&ocirc;ng cụ, đồ cổ v&agrave; đồ giả cổ, những vật dụng nh&agrave; n&ocirc;ng, c&aacute;c mặt h&agrave;ng nhu yếu phẩm, những bộ đồ đồng,... đa chủng loại, chất lượng v&agrave; gi&aacute; th&agrave;nh.</p>\r\n<p style=\"text-align: justify;\"><strong>B&atilde;i biển Thịnh Long</strong></p>\r\n<p style=\"text-align: justify;\">Nằm tr&ecirc;n địa phận thị trấn Thịnh Long, huyện Hải Hậu, b&atilde;i biển Thịnh Long l&agrave; một trong những b&atilde;i biển đẹp, hoang sơ của Nam Định với sức quyến rũ kh&ocirc;ng hề nhỏ. B&atilde;i biển n&agrave;y sở hữu bờ c&aacute;t trải d&agrave;i, h&agrave;ng phi lao r&igrave; r&agrave;o trước gi&oacute;, s&oacute;ng biển dập d&igrave;u.</p>\r\n</body>\r\n</html>', '1', '2018-04-12 00:00:00', '2018-04-27 00:00:00', null, null, '2018-04-12 04:18:05', '2018-04-12 16:59:57', '1', 'PT001', null);
INSERT INTO `news` VALUES ('5', '9 điểm đến hấp dẫn ở Nam Định', 'images/news/news4.jpg', 'Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) là nơi thờ tự 14 vị vua nhà Trần cùng với gia quyến, các vị quan đã có công phù tá nhà Trần…', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\"><strong><img src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/%C4%90%E1%BB%81n%20Tr%E1%BA%A7n.jpg\" alt=\"\" width=\"1000\" height=\"562\" /></strong></p>\r\n<p style=\"text-align: justify;\"><strong>Đền Trần</strong></p>\r\n<p style=\"text-align: justify;\">Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) l&agrave; nơi thờ tự 14 vị vua nh&agrave; Trần c&ugrave;ng với gia quyến, c&aacute;c vị quan đ&atilde; c&oacute; c&ocirc;ng ph&ugrave; t&aacute; nh&agrave; Trần&hellip; v&agrave; l&agrave; một trong những điểm đến m&agrave; rất nhiều du kh&aacute;ch muốn gh&eacute; qua khi đặt ch&acirc;n đến Nam Định. Đền Trần được x&acirc;y dựng v&agrave;o năm 1695 tr&ecirc;n nền Th&aacute;i miếu cũ (tức l&agrave; Phủ Thi&ecirc;n Trường xưa) - nơi được coi l&agrave; ph&aacute;t t&iacute;ch của vương triều nh&agrave; Trần.</p>\r\n<p style=\"text-align: justify;\">Đền bao gồm tổ hợp ba c&ocirc;ng tr&igrave;nh kiến tr&uacute;c: đền Thi&ecirc;n Trường (đền Thượng), đền Cổ Trạch (đền Hạ) v&agrave; đền Tr&ugrave;ng Hoa c&ugrave;ng mang một kiểu kiến tr&uacute;c v&agrave; quy m&ocirc; ngang nhau. Mỗi đền gồm t&ograve;a tiền đường 5 gian, t&ograve;a trung đường 5 gian v&agrave; t&ograve;a ch&iacute;nh tẩm 3 gian. Nối tiền đường v&agrave; trung đường l&agrave; kinh đ&agrave;n (thi&ecirc;u hương) v&agrave; 2 gian tả hữu. V&agrave;o ng&agrave;y 15 th&aacute;ng Gi&ecirc;ng hằng năm diễn ra Lễ hội đền Trần, thu h&uacute;t đ&ocirc;ng đảo du kh&aacute;ch đến tham quan chi&ecirc;m b&aacute;i. Th&agrave;nh cổ Nam Định Nằm ở ph&iacute;a T&acirc;y Bắc th&agrave;nh phố, Th&agrave;nh cổ Nam Định c&oacute; 4 cửa, mỗi cửa lại mở ra một g&oacute;c nh&igrave;n kh&aacute;c với những n&eacute;t đặc trưng ri&ecirc;ng. Đến đ&acirc;y du kh&aacute;ch c&oacute; cơ hội thưởng ngoạn h&agrave;ng loạt c&aacute;c c&ocirc;ng tr&igrave;nh phụ b&ecirc;n trong như: Cột cờ, Trường Thi, Văn Miếu hay đ&igrave;nh Vọng Cung&hellip; nơi in dấu một thời kỳ ph&aacute;t triển v&agrave;ng son - kinh đ&ocirc; thứ 2 của nh&agrave; Trần. Th&agrave;nh cổ Nam Định l&agrave; c&ocirc;ng tr&igrave;nh mang gi&aacute; trị lịch sử, kiến tr&uacute;c độc đ&aacute;o của Nam Định, l&agrave; chứng t&iacute;ch cho l&ograve;ng quả cảm của qu&acirc;n v&agrave; d&acirc;n Th&agrave;nh Nam trong c&aacute;c thời kỳ chống qu&acirc;n x&acirc;m lược.</p>\r\n<p style=\"text-align: justify;\"><strong>Th&aacute;p Phổ Minh </strong></p>\r\n<p style=\"text-align: justify;\">Ch&ugrave;a Phổ Minh (hay c&ograve;n gọi l&agrave; ch&ugrave;a Th&aacute;p) tọa lạc tại th&ocirc;n Tức Mạc, phường Lộc Vượng, TP. Nam Định, nằm c&aacute;ch trung t&acirc;m TP. Nam Định khoảng 5km về ph&iacute;a Bắc l&agrave; một c&ocirc;ng tr&igrave;nh trong khu&ocirc;n vi&ecirc;n ch&ugrave;a Th&aacute;p c&oacute; hơn 700 năm tuổi. Năm 2012, di t&iacute;ch lịch sử v&agrave; kiến tr&uacute;c nghệ thuật đền Trần v&agrave; ch&ugrave;a Phổ Minh được xếp hạng di t&iacute;ch quốc gia đặc biệt. Th&aacute;p được x&acirc;y bằng gạch, bắt mạch để trần kh&ocirc;ng tr&aacute;t, c&oacute; h&igrave;nh vu&ocirc;ng, cao 19,5m với 14 tầng. Mặc d&ugrave; trải qua nhiều lần tu bổ nhưng ch&ugrave;a vẫn giữ được nhiều dấu t&iacute;ch nghệ thuật quan trọng của một thời H&agrave;o kh&iacute; Đ&ocirc;ng A. Đ&acirc;y cũng l&agrave; một trong số &iacute;t c&ocirc;ng tr&igrave;nh đời Trần c&ograve;n giữ được tương đối to&agrave;n vẹn đến ng&agrave;y nay.</p>\r\n<p style=\"text-align: justify;\"><strong>Khu di t&iacute;ch Phủ D&agrave;y </strong></p>\r\n<p style=\"text-align: justify;\">Thuộc địa phận x&atilde; Kim Th&aacute;i, huyện Vụ Bản, nằm c&aacute;ch TP. Nam Định 17km về ph&iacute;a T&acirc;y Nam, Khu di t&iacute;ch Phủ D&agrave;y l&agrave; nơi thờ b&agrave; ch&uacute;a Liễu Hạnh (l&agrave; một vị thần, tượng trưng cho cuộc sống tinh thần, ph&uacute;c đức, sự thịnh vượng) &ndash; một trong &ldquo;tứ bất tử&rdquo; của Việt Nam. Phủ D&agrave;y gồm c&oacute;: phủ Ti&ecirc;n Hương, phủ V&acirc;n C&aacute;t v&agrave; lăng b&agrave; Ch&uacute;a Liễu. Phủ Ti&ecirc;n Hương c&oacute; từ thời L&ecirc; &ndash; Cảnh Trị (1663 &ndash; 1671), gồm 81 gian lớn nhỏ trong tổng số 19 t&ograve;a v&agrave; c&oacute; tất cả 4 cung. C&aacute;c h&igrave;nh họa đi&ecirc;u khắc hết sức tinh vi, bề thế, nơi đ&acirc;y c&ograve;n c&oacute; 5 pho tượng từ thế kỷ thứ XIX. Phủ V&acirc;n C&aacute;t c&oacute; 7 t&ograve;a với 30 gian, c&oacute; Ngọ m&ocirc;n với 5 g&aacute;c lầu. Trước Ngọ m&ocirc;n l&agrave; hồ b&aacute;n nguyệt v&agrave; nh&agrave; thủy l&acirc;u được đặt giữa hồ. Được x&acirc;y dựng bằng đ&aacute; v&agrave;o năm 1938, lăng b&agrave; Ch&uacute;a Liễu mang những đường n&eacute;t chạm trổ tinh tế. C&aacute;c trụ cổng đắp nổi h&igrave;nh b&ocirc;ng sen. Nh&igrave;n lăng từ xa giống như một hồ sen cạn với 60 b&uacute;p sen hồng nổi bật. Đến với Khu di t&iacute;ch Phủ D&agrave;y, du kh&aacute;ch kh&ocirc;ng chỉ c&oacute; dịp h&agrave;nh lễ cầu những điều may mắn m&agrave; c&ograve;n c&oacute; cơ hội thưởng ngoạn cảnh sắc thi&ecirc;n nhi&ecirc;n tươi đẹp, ngắm nh&igrave;n một c&ocirc;ng tr&igrave;nh kiến tr&uacute;c trang nghi&ecirc;m, bề thế.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/V%C6%B0%E1%BB%9Dn%20qu%E1%BB%91c%20gia%20Xu%C3%A2n%20Th%E1%BB%A7y.jpg\" alt=\"\" width=\"550\" height=\"295\" /></p>\r\n<p style=\"text-align: justify;\"><strong>Vườn quốc gia Xu&acirc;n Thủy </strong></p>\r\n<p style=\"text-align: justify;\">Được UNESCO c&ocirc;ng nhận l&agrave; rừng ngập mặn đầu ti&ecirc;n ở Việt Nam theo c&ocirc;ng ước Ramsar v&agrave; rừng ngập mặn thứ 50 của thế giới, Vườn quốc gia Xu&acirc;n Thủy c&oacute; tổng diện t&iacute;ch l&agrave; 12.000 ha, thuộc địa phận x&atilde; Giao Thi&ecirc;n, huyện Giao Thủy. Hệ động thực vật sinh trưởng tại đ&acirc;y rất phong ph&uacute;, c&oacute; nhiều lo&agrave;i thực vật bậc cao v&agrave; động vật qu&yacute; hiếm, khoảng 100 lo&agrave;i chim trong qu&aacute; tr&igrave;nh di cư về phương Nam lưu tr&uacute; tại đ&acirc;y.</p>\r\n<p style=\"text-align: justify;\">Đ&acirc;y cũng l&agrave; điểm du lịch sinh th&aacute;i thu h&uacute;t rất nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước đến tham quan v&agrave; kh&aacute;m ph&aacute;&hellip; Ch&ugrave;a Cổ Lễ Ch&ugrave;a Cổ Lễ được x&acirc;y dựng từ năm 1920, do thiền sư Nguyễn Minh Kh&ocirc;ng (thời L&yacute;) s&aacute;ng lập. Hiện nay, ch&ugrave;a thuộc địa phận thị trấn Cổ Lễ, huyện Trực Ninh. Ch&ugrave;a c&oacute; th&aacute;p 12 tầng được x&acirc;y từ năm 1926 &ndash; 1927. Ngo&agrave;i ra, ch&ugrave;a c&ograve;n lưu giữ được nhiều hiện vật văn h&oacute;a qu&yacute; hiếm thời L&yacute; như trống đồng, t&uacute;i đựng đồng&hellip; v&agrave; đặc biệt l&agrave; đại hồng chung.</p>\r\n<p style=\"text-align: justify;\"><strong>T&ograve;a gi&aacute;m mục B&ugrave;i Chu</strong></p>\r\n<p style=\"text-align: justify;\">Sở hữu rất nhiều nh&agrave; thờ cổ nguy nga với kiến tr&uacute;c đặc sắc, Nam Định được coi l&agrave; &ldquo;thủ phủ&rdquo; của c&aacute;c nh&agrave; thờ tại Việt Nam. T&ograve;a gi&aacute;m mục B&ugrave;i Chu l&agrave; một c&ocirc;ng tr&igrave;nh đạo gi&aacute;o, mang phong c&aacute;ch kiến tr&uacute;c Gothique độc đ&aacute;o, được x&acirc;y dựng từ năm 1885 v&agrave; được coi l&agrave; nơi lưu dấu t&iacute;ch của gi&aacute;o sĩ truyền gi&aacute;o đầu ti&ecirc;n (đầu thế kỷ XVIII). Hiện nay, nơi đ&acirc;y vẫn lưu giữ được nhiều t&agrave;i liệu li&ecirc;n quan đến gi&aacute;o hội, đến việc nhận nu&ocirc;i trẻ mồ c&ocirc;i v&agrave; người khuyết tật&hellip;</p>\r\n<p style=\"text-align: justify;\"><strong>Chợ Viềng </strong></p>\r\n<p style=\"text-align: justify;\">C&ograve;n gọi l&agrave; chợ &Acirc;m Phủ, chợ Viềng l&agrave; phi&ecirc;n chợ v&ocirc; c&ugrave;ng nổi tiếng v&agrave; độc đ&aacute;o chỉ họp duy nhất một lần v&agrave;o đ&ecirc;m mồng 7 đến rạng ng&agrave;y mồng 8 th&aacute;ng Gi&ecirc;ng &Acirc;m lịch. Đ&acirc;y l&agrave; n&eacute;t văn h&oacute;a truyền thống từ xa xưa của v&ugrave;ng. Người d&acirc;n đến với chợ Viềng với &yacute; niệm &ldquo;mua may b&aacute;n rủi&rdquo; cho năm mới được b&igrave;nh an, may mắn. Ch&iacute;nh v&igrave; vậy, người đến chợ kh&ocirc;ng đặt nặng vấn đề lời l&atilde;i, người b&aacute;n kh&ocirc;ng n&oacute;i th&aacute;ch v&agrave; người mua kh&ocirc;ng mặc cả. H&agrave;ng h&oacute;a được b&agrave;y b&aacute;n ở phi&ecirc;n chợ n&agrave;y chủ yếu l&agrave; c&aacute;c loại c&acirc;y trồng, n&ocirc;ng cụ, đồ cổ v&agrave; đồ giả cổ, những vật dụng nh&agrave; n&ocirc;ng, c&aacute;c mặt h&agrave;ng nhu yếu phẩm, những bộ đồ đồng,... đa chủng loại, chất lượng v&agrave; gi&aacute; th&agrave;nh.</p>\r\n<p style=\"text-align: justify;\"><strong>B&atilde;i biển Thịnh Long</strong></p>\r\n<p style=\"text-align: justify;\">Nằm tr&ecirc;n địa phận thị trấn Thịnh Long, huyện Hải Hậu, b&atilde;i biển Thịnh Long l&agrave; một trong những b&atilde;i biển đẹp, hoang sơ của Nam Định với sức quyến rũ kh&ocirc;ng hề nhỏ. B&atilde;i biển n&agrave;y sở hữu bờ c&aacute;t trải d&agrave;i, h&agrave;ng phi lao r&igrave; r&agrave;o trước gi&oacute;, s&oacute;ng biển dập d&igrave;u.</p>\r\n</body>\r\n</html>', '1', null, null, null, null, '2018-04-12 04:33:28', '2018-04-12 11:08:29', null, 'GT001', null);
INSERT INTO `news` VALUES ('6', 'd', null, null, null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>d</p>\r\n</body>\r\n</html>', '1', null, null, null, '8', '2018-04-12 11:34:28', '2018-04-12 17:01:13', null, 'GT001', null);
INSERT INTO `news` VALUES ('7', 'binh test cod moi ko thay dau', null, null, null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>', '0', null, null, null, null, '2018-04-13 01:43:33', '2018-04-13 01:43:59', null, 'GT001', '2018-04-13 01:43:59');
INSERT INTO `news` VALUES ('8', 'bản mới về chúng tôi', null, null, null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Nội dung ti&ecirc;u đề</p>\r\n</body>\r\n</html>', '0', null, null, null, null, '2018-04-13 02:52:35', '2018-04-20 14:30:43', null, 'INTRODUC01', null);
INSERT INTO `news` VALUES ('9', 'Cột trái thống kê', '1danh-chia-khoa-xe-may-1.jpg', '123', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>444</p>\r\n</body>\r\n</html>', '1', '2018-04-20 00:00:00', '2018-04-21 00:00:00', null, null, '2018-04-20 16:01:10', '2018-04-20 16:01:10', null, 'GT001', null);
INSERT INTO `news` VALUES ('10', 'Thống kê truy cập 2', '5.jpg', '1111', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>22222</p>\r\n</body>\r\n</html>', '1', '2018-04-20 00:00:00', '2018-04-28 00:00:00', null, null, '2018-04-20 16:07:00', '2018-04-20 16:07:00', null, 'LH001', null);
INSERT INTO `news` VALUES ('11', 'Thống kê truy cập 2', '123.jpg', '111', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>222</p>\r\n</body>\r\n</html>', '0', '2018-04-20 00:00:00', '2018-04-21 00:00:00', null, null, '2018-04-20 16:11:30', '2018-04-20 16:11:30', null, 'DD001', null);
INSERT INTO `news` VALUES ('12', 'Thống kê truy cập 2', '123.jpg', '111', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>222</p>\r\n</body>\r\n</html>', '0', '2018-04-20 00:00:00', '2018-04-21 00:00:00', null, null, '2018-04-20 16:13:53', '2018-04-20 16:13:53', null, 'DD001', null);
INSERT INTO `news` VALUES ('13', 'ductest2', '3.jpg', '111', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>222</p>\r\n</body>\r\n</html>', '0', '2018-04-20 00:00:00', '2018-04-27 00:00:00', null, null, '2018-04-20 16:15:53', '2018-04-20 16:15:53', null, 'LH001', null);
INSERT INTO `news` VALUES ('14', 'Cột trái thống kê', null, '44', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>555</p>\r\n</body>\r\n</html>', '0', '1970-01-01 00:00:00', '1970-01-01 00:00:00', null, null, '2018-04-20 16:26:47', '2018-04-20 16:26:47', null, 'PT001', null);
INSERT INTO `news` VALUES ('15', '9 điểm đến hấp dẫn ở Nam Định 1', null, 'Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) là nơi thờ tự 14 vị vua nhà Trần cùng với gia quyến, các vị quan đã có công phù tá nhà Trần… 1', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\">111</p>\r\n<p style=\"text-align: justify;\"><strong><img src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/%C4%90%E1%BB%81n%20Tr%E1%BA%A7n.jpg\" alt=\"\" width=\"1000\" height=\"562\" /></strong></p>\r\n<p style=\"text-align: justify;\"><strong>Đền Trần</strong></p>\r\n<p style=\"text-align: justify;\">Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) l&agrave; nơi thờ tự 14 vị vua nh&agrave; Trần c&ugrave;ng với gia quyến, c&aacute;c vị quan đ&atilde; c&oacute; c&ocirc;ng ph&ugrave; t&aacute; nh&agrave; Trần&hellip; v&agrave; l&agrave; một trong những điểm đến m&agrave; rất nhiều du kh&aacute;ch muốn gh&eacute; qua khi đặt ch&acirc;n đến Nam Định. Đền Trần được x&acirc;y dựng v&agrave;o năm 1695 tr&ecirc;n nền Th&aacute;i miếu cũ (tức l&agrave; Phủ Thi&ecirc;n Trường xưa) - nơi được coi l&agrave; ph&aacute;t t&iacute;ch của vương triều nh&agrave; Trần.</p>\r\n<p style=\"text-align: justify;\">Đền bao gồm tổ hợp ba c&ocirc;ng tr&igrave;nh kiến tr&uacute;c: đền Thi&ecirc;n Trường (đền Thượng), đền Cổ Trạch (đền Hạ) v&agrave; đền Tr&ugrave;ng Hoa c&ugrave;ng mang một kiểu kiến tr&uacute;c v&agrave; quy m&ocirc; ngang nhau. Mỗi đền gồm t&ograve;a tiền đường 5 gian, t&ograve;a trung đường 5 gian v&agrave; t&ograve;a ch&iacute;nh tẩm 3 gian. Nối tiền đường v&agrave; trung đường l&agrave; kinh đ&agrave;n (thi&ecirc;u hương) v&agrave; 2 gian tả hữu. V&agrave;o ng&agrave;y 15 th&aacute;ng Gi&ecirc;ng hằng năm diễn ra Lễ hội đền Trần, thu h&uacute;t đ&ocirc;ng đảo du kh&aacute;ch đến tham quan chi&ecirc;m b&aacute;i. Th&agrave;nh cổ Nam Định Nằm ở ph&iacute;a T&acirc;y Bắc th&agrave;nh phố, Th&agrave;nh cổ Nam Định c&oacute; 4 cửa, mỗi cửa lại mở ra một g&oacute;c nh&igrave;n kh&aacute;c với những n&eacute;t đặc trưng ri&ecirc;ng. Đến đ&acirc;y du kh&aacute;ch c&oacute; cơ hội thưởng ngoạn h&agrave;ng loạt c&aacute;c c&ocirc;ng tr&igrave;nh phụ b&ecirc;n trong như: Cột cờ, Trường Thi, Văn Miếu hay đ&igrave;nh Vọng Cung&hellip; nơi in dấu một thời kỳ ph&aacute;t triển v&agrave;ng son - kinh đ&ocirc; thứ 2 của nh&agrave; Trần. Th&agrave;nh cổ Nam Định l&agrave; c&ocirc;ng tr&igrave;nh mang gi&aacute; trị lịch sử, kiến tr&uacute;c độc đ&aacute;o của Nam Định, l&agrave; chứng t&iacute;ch cho l&ograve;ng quả cảm của qu&acirc;n v&agrave; d&acirc;n Th&agrave;nh Nam trong c&aacute;c thời kỳ chống qu&acirc;n x&acirc;m lược.</p>\r\n<p style=\"text-align: justify;\"><strong>Th&aacute;p Phổ Minh </strong></p>\r\n<p style=\"text-align: justify;\">Ch&ugrave;a Phổ Minh (hay c&ograve;n gọi l&agrave; ch&ugrave;a Th&aacute;p) tọa lạc tại th&ocirc;n Tức Mạc, phường Lộc Vượng, TP. Nam Định, nằm c&aacute;ch trung t&acirc;m TP. Nam Định khoảng 5km về ph&iacute;a Bắc l&agrave; một c&ocirc;ng tr&igrave;nh trong khu&ocirc;n vi&ecirc;n ch&ugrave;a Th&aacute;p c&oacute; hơn 700 năm tuổi. Năm 2012, di t&iacute;ch lịch sử v&agrave; kiến tr&uacute;c nghệ thuật đền Trần v&agrave; ch&ugrave;a Phổ Minh được xếp hạng di t&iacute;ch quốc gia đặc biệt. Th&aacute;p được x&acirc;y bằng gạch, bắt mạch để trần kh&ocirc;ng tr&aacute;t, c&oacute; h&igrave;nh vu&ocirc;ng, cao 19,5m với 14 tầng. Mặc d&ugrave; trải qua nhiều lần tu bổ nhưng ch&ugrave;a vẫn giữ được nhiều dấu t&iacute;ch nghệ thuật quan trọng của một thời H&agrave;o kh&iacute; Đ&ocirc;ng A. Đ&acirc;y cũng l&agrave; một trong số &iacute;t c&ocirc;ng tr&igrave;nh đời Trần c&ograve;n giữ được tương đối to&agrave;n vẹn đến ng&agrave;y nay.</p>\r\n<p style=\"text-align: justify;\"><strong>Khu di t&iacute;ch Phủ D&agrave;y </strong></p>\r\n<p style=\"text-align: justify;\">Thuộc địa phận x&atilde; Kim Th&aacute;i, huyện Vụ Bản, nằm c&aacute;ch TP. Nam Định 17km về ph&iacute;a T&acirc;y Nam, Khu di t&iacute;ch Phủ D&agrave;y l&agrave; nơi thờ b&agrave; ch&uacute;a Liễu Hạnh (l&agrave; một vị thần, tượng trưng cho cuộc sống tinh thần, ph&uacute;c đức, sự thịnh vượng) &ndash; một trong &ldquo;tứ bất tử&rdquo; của Việt Nam. Phủ D&agrave;y gồm c&oacute;: phủ Ti&ecirc;n Hương, phủ V&acirc;n C&aacute;t v&agrave; lăng b&agrave; Ch&uacute;a Liễu. Phủ Ti&ecirc;n Hương c&oacute; từ thời L&ecirc; &ndash; Cảnh Trị (1663 &ndash; 1671), gồm 81 gian lớn nhỏ trong tổng số 19 t&ograve;a v&agrave; c&oacute; tất cả 4 cung. C&aacute;c h&igrave;nh họa đi&ecirc;u khắc hết sức tinh vi, bề thế, nơi đ&acirc;y c&ograve;n c&oacute; 5 pho tượng từ thế kỷ thứ XIX. Phủ V&acirc;n C&aacute;t c&oacute; 7 t&ograve;a với 30 gian, c&oacute; Ngọ m&ocirc;n với 5 g&aacute;c lầu. Trước Ngọ m&ocirc;n l&agrave; hồ b&aacute;n nguyệt v&agrave; nh&agrave; thủy l&acirc;u được đặt giữa hồ. Được x&acirc;y dựng bằng đ&aacute; v&agrave;o năm 1938, lăng b&agrave; Ch&uacute;a Liễu mang những đường n&eacute;t chạm trổ tinh tế. C&aacute;c trụ cổng đắp nổi h&igrave;nh b&ocirc;ng sen. Nh&igrave;n lăng từ xa giống như một hồ sen cạn với 60 b&uacute;p sen hồng nổi bật. Đến với Khu di t&iacute;ch Phủ D&agrave;y, du kh&aacute;ch kh&ocirc;ng chỉ c&oacute; dịp h&agrave;nh lễ cầu những điều may mắn m&agrave; c&ograve;n c&oacute; cơ hội thưởng ngoạn cảnh sắc thi&ecirc;n nhi&ecirc;n tươi đẹp, ngắm nh&igrave;n một c&ocirc;ng tr&igrave;nh kiến tr&uacute;c trang nghi&ecirc;m, bề thế.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/V%C6%B0%E1%BB%9Dn%20qu%E1%BB%91c%20gia%20Xu%C3%A2n%20Th%E1%BB%A7y.jpg\" alt=\"\" width=\"550\" height=\"295\" /></p>\r\n<p style=\"text-align: justify;\"><strong>Vườn quốc gia Xu&acirc;n Thủy </strong></p>\r\n<p style=\"text-align: justify;\">Được UNESCO c&ocirc;ng nhận l&agrave; rừng ngập mặn đầu ti&ecirc;n ở Việt Nam theo c&ocirc;ng ước Ramsar v&agrave; rừng ngập mặn thứ 50 của thế giới, Vườn quốc gia Xu&acirc;n Thủy c&oacute; tổng diện t&iacute;ch l&agrave; 12.000 ha, thuộc địa phận x&atilde; Giao Thi&ecirc;n, huyện Giao Thủy. Hệ động thực vật sinh trưởng tại đ&acirc;y rất phong ph&uacute;, c&oacute; nhiều lo&agrave;i thực vật bậc cao v&agrave; động vật qu&yacute; hiếm, khoảng 100 lo&agrave;i chim trong qu&aacute; tr&igrave;nh di cư về phương Nam lưu tr&uacute; tại đ&acirc;y.</p>\r\n<p style=\"text-align: justify;\">Đ&acirc;y cũng l&agrave; điểm du lịch sinh th&aacute;i thu h&uacute;t rất nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước đến tham quan v&agrave; kh&aacute;m ph&aacute;&hellip; Ch&ugrave;a Cổ Lễ Ch&ugrave;a Cổ Lễ được x&acirc;y dựng từ năm 1920, do thiền sư Nguyễn Minh Kh&ocirc;ng (thời L&yacute;) s&aacute;ng lập. Hiện nay, ch&ugrave;a thuộc địa phận thị trấn Cổ Lễ, huyện Trực Ninh. Ch&ugrave;a c&oacute; th&aacute;p 12 tầng được x&acirc;y từ năm 1926 &ndash; 1927. Ngo&agrave;i ra, ch&ugrave;a c&ograve;n lưu giữ được nhiều hiện vật văn h&oacute;a qu&yacute; hiếm thời L&yacute; như trống đồng, t&uacute;i đựng đồng&hellip; v&agrave; đặc biệt l&agrave; đại hồng chung.</p>\r\n<p style=\"text-align: justify;\"><strong>T&ograve;a gi&aacute;m mục B&ugrave;i Chu</strong></p>\r\n<p style=\"text-align: justify;\">Sở hữu rất nhiều nh&agrave; thờ cổ nguy nga với kiến tr&uacute;c đặc sắc, Nam Định được coi l&agrave; &ldquo;thủ phủ&rdquo; của c&aacute;c nh&agrave; thờ tại Việt Nam. T&ograve;a gi&aacute;m mục B&ugrave;i Chu l&agrave; một c&ocirc;ng tr&igrave;nh đạo gi&aacute;o, mang phong c&aacute;ch kiến tr&uacute;c Gothique độc đ&aacute;o, được x&acirc;y dựng từ năm 1885 v&agrave; được coi l&agrave; nơi lưu dấu t&iacute;ch của gi&aacute;o sĩ truyền gi&aacute;o đầu ti&ecirc;n (đầu thế kỷ XVIII). Hiện nay, nơi đ&acirc;y vẫn lưu giữ được nhiều t&agrave;i liệu li&ecirc;n quan đến gi&aacute;o hội, đến việc nhận nu&ocirc;i trẻ mồ c&ocirc;i v&agrave; người khuyết tật&hellip;</p>\r\n<p style=\"text-align: justify;\"><strong>Chợ Viềng </strong></p>\r\n<p style=\"text-align: justify;\">C&ograve;n gọi l&agrave; chợ &Acirc;m Phủ, chợ Viềng l&agrave; phi&ecirc;n chợ v&ocirc; c&ugrave;ng nổi tiếng v&agrave; độc đ&aacute;o chỉ họp duy nhất một lần v&agrave;o đ&ecirc;m mồng 7 đến rạng ng&agrave;y mồng 8 th&aacute;ng Gi&ecirc;ng &Acirc;m lịch. Đ&acirc;y l&agrave; n&eacute;t văn h&oacute;a truyền thống từ xa xưa của v&ugrave;ng. Người d&acirc;n đến với chợ Viềng với &yacute; niệm &ldquo;mua may b&aacute;n rủi&rdquo; cho năm mới được b&igrave;nh an, may mắn. Ch&iacute;nh v&igrave; vậy, người đến chợ kh&ocirc;ng đặt nặng vấn đề lời l&atilde;i, người b&aacute;n kh&ocirc;ng n&oacute;i th&aacute;ch v&agrave; người mua kh&ocirc;ng mặc cả. H&agrave;ng h&oacute;a được b&agrave;y b&aacute;n ở phi&ecirc;n chợ n&agrave;y chủ yếu l&agrave; c&aacute;c loại c&acirc;y trồng, n&ocirc;ng cụ, đồ cổ v&agrave; đồ giả cổ, những vật dụng nh&agrave; n&ocirc;ng, c&aacute;c mặt h&agrave;ng nhu yếu phẩm, những bộ đồ đồng,... đa chủng loại, chất lượng v&agrave; gi&aacute; th&agrave;nh.</p>\r\n<p style=\"text-align: justify;\"><strong>B&atilde;i biển Thịnh Long</strong></p>\r\n<p style=\"text-align: justify;\">Nằm tr&ecirc;n địa phận thị trấn Thịnh Long, huyện Hải Hậu, b&atilde;i biển Thịnh Long l&agrave; một trong những b&atilde;i biển đẹp, hoang sơ của Nam Định với sức quyến rũ kh&ocirc;ng hề nhỏ. B&atilde;i biển n&agrave;y sở hữu bờ c&aacute;t trải d&agrave;i, h&agrave;ng phi lao r&igrave; r&agrave;o trước gi&oacute;, s&oacute;ng biển dập d&igrave;u.</p>\r\n</body>\r\n</html>', '0', '2018-04-13 00:00:00', '2018-08-23 00:00:00', null, null, '2018-04-22 16:33:24', '2018-04-22 16:33:24', null, 'GT001', null);
INSERT INTO `news` VALUES ('16', '9 điểm đến hấp dẫn ở Nam Định 11111', null, 'Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) là nơi thờ tự 14 vị vua nhà Trần cùng với gia quyến, các vị quan đã có công phù tá nhà Trần… 1111', null, '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p style=\"text-align: justify;\">1112222</p>\r\n<p style=\"text-align: justify;\"><strong><img src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/%C4%90%E1%BB%81n%20Tr%E1%BA%A7n.jpg\" alt=\"\" width=\"1000\" height=\"562\" /></strong></p>\r\n<p style=\"text-align: justify;\"><strong>Đền Trần</strong></p>\r\n<p style=\"text-align: justify;\">Đền Trần (đường Trần Thừa, phường Lộc Vượng, TP. Nam Định) l&agrave; nơi thờ tự 14 vị vua nh&agrave; Trần c&ugrave;ng với gia quyến, c&aacute;c vị quan đ&atilde; c&oacute; c&ocirc;ng ph&ugrave; t&aacute; nh&agrave; Trần&hellip; v&agrave; l&agrave; một trong những điểm đến m&agrave; rất nhiều du kh&aacute;ch muốn gh&eacute; qua khi đặt ch&acirc;n đến Nam Định. Đền Trần được x&acirc;y dựng v&agrave;o năm 1695 tr&ecirc;n nền Th&aacute;i miếu cũ (tức l&agrave; Phủ Thi&ecirc;n Trường xưa) - nơi được coi l&agrave; ph&aacute;t t&iacute;ch của vương triều nh&agrave; Trần.</p>\r\n<p style=\"text-align: justify;\">Đền bao gồm tổ hợp ba c&ocirc;ng tr&igrave;nh kiến tr&uacute;c: đền Thi&ecirc;n Trường (đền Thượng), đền Cổ Trạch (đền Hạ) v&agrave; đền Tr&ugrave;ng Hoa c&ugrave;ng mang một kiểu kiến tr&uacute;c v&agrave; quy m&ocirc; ngang nhau. Mỗi đền gồm t&ograve;a tiền đường 5 gian, t&ograve;a trung đường 5 gian v&agrave; t&ograve;a ch&iacute;nh tẩm 3 gian. Nối tiền đường v&agrave; trung đường l&agrave; kinh đ&agrave;n (thi&ecirc;u hương) v&agrave; 2 gian tả hữu. V&agrave;o ng&agrave;y 15 th&aacute;ng Gi&ecirc;ng hằng năm diễn ra Lễ hội đền Trần, thu h&uacute;t đ&ocirc;ng đảo du kh&aacute;ch đến tham quan chi&ecirc;m b&aacute;i. Th&agrave;nh cổ Nam Định Nằm ở ph&iacute;a T&acirc;y Bắc th&agrave;nh phố, Th&agrave;nh cổ Nam Định c&oacute; 4 cửa, mỗi cửa lại mở ra một g&oacute;c nh&igrave;n kh&aacute;c với những n&eacute;t đặc trưng ri&ecirc;ng. Đến đ&acirc;y du kh&aacute;ch c&oacute; cơ hội thưởng ngoạn h&agrave;ng loạt c&aacute;c c&ocirc;ng tr&igrave;nh phụ b&ecirc;n trong như: Cột cờ, Trường Thi, Văn Miếu hay đ&igrave;nh Vọng Cung&hellip; nơi in dấu một thời kỳ ph&aacute;t triển v&agrave;ng son - kinh đ&ocirc; thứ 2 của nh&agrave; Trần. Th&agrave;nh cổ Nam Định l&agrave; c&ocirc;ng tr&igrave;nh mang gi&aacute; trị lịch sử, kiến tr&uacute;c độc đ&aacute;o của Nam Định, l&agrave; chứng t&iacute;ch cho l&ograve;ng quả cảm của qu&acirc;n v&agrave; d&acirc;n Th&agrave;nh Nam trong c&aacute;c thời kỳ chống qu&acirc;n x&acirc;m lược.</p>\r\n<p style=\"text-align: justify;\"><strong>Th&aacute;p Phổ Minh </strong></p>\r\n<p style=\"text-align: justify;\">Ch&ugrave;a Phổ Minh (hay c&ograve;n gọi l&agrave; ch&ugrave;a Th&aacute;p) tọa lạc tại th&ocirc;n Tức Mạc, phường Lộc Vượng, TP. Nam Định, nằm c&aacute;ch trung t&acirc;m TP. Nam Định khoảng 5km về ph&iacute;a Bắc l&agrave; một c&ocirc;ng tr&igrave;nh trong khu&ocirc;n vi&ecirc;n ch&ugrave;a Th&aacute;p c&oacute; hơn 700 năm tuổi. Năm 2012, di t&iacute;ch lịch sử v&agrave; kiến tr&uacute;c nghệ thuật đền Trần v&agrave; ch&ugrave;a Phổ Minh được xếp hạng di t&iacute;ch quốc gia đặc biệt. Th&aacute;p được x&acirc;y bằng gạch, bắt mạch để trần kh&ocirc;ng tr&aacute;t, c&oacute; h&igrave;nh vu&ocirc;ng, cao 19,5m với 14 tầng. Mặc d&ugrave; trải qua nhiều lần tu bổ nhưng ch&ugrave;a vẫn giữ được nhiều dấu t&iacute;ch nghệ thuật quan trọng của một thời H&agrave;o kh&iacute; Đ&ocirc;ng A. Đ&acirc;y cũng l&agrave; một trong số &iacute;t c&ocirc;ng tr&igrave;nh đời Trần c&ograve;n giữ được tương đối to&agrave;n vẹn đến ng&agrave;y nay.</p>\r\n<p style=\"text-align: justify;\"><strong>Khu di t&iacute;ch Phủ D&agrave;y </strong></p>\r\n<p style=\"text-align: justify;\">Thuộc địa phận x&atilde; Kim Th&aacute;i, huyện Vụ Bản, nằm c&aacute;ch TP. Nam Định 17km về ph&iacute;a T&acirc;y Nam, Khu di t&iacute;ch Phủ D&agrave;y l&agrave; nơi thờ b&agrave; ch&uacute;a Liễu Hạnh (l&agrave; một vị thần, tượng trưng cho cuộc sống tinh thần, ph&uacute;c đức, sự thịnh vượng) &ndash; một trong &ldquo;tứ bất tử&rdquo; của Việt Nam. Phủ D&agrave;y gồm c&oacute;: phủ Ti&ecirc;n Hương, phủ V&acirc;n C&aacute;t v&agrave; lăng b&agrave; Ch&uacute;a Liễu. Phủ Ti&ecirc;n Hương c&oacute; từ thời L&ecirc; &ndash; Cảnh Trị (1663 &ndash; 1671), gồm 81 gian lớn nhỏ trong tổng số 19 t&ograve;a v&agrave; c&oacute; tất cả 4 cung. C&aacute;c h&igrave;nh họa đi&ecirc;u khắc hết sức tinh vi, bề thế, nơi đ&acirc;y c&ograve;n c&oacute; 5 pho tượng từ thế kỷ thứ XIX. Phủ V&acirc;n C&aacute;t c&oacute; 7 t&ograve;a với 30 gian, c&oacute; Ngọ m&ocirc;n với 5 g&aacute;c lầu. Trước Ngọ m&ocirc;n l&agrave; hồ b&aacute;n nguyệt v&agrave; nh&agrave; thủy l&acirc;u được đặt giữa hồ. Được x&acirc;y dựng bằng đ&aacute; v&agrave;o năm 1938, lăng b&agrave; Ch&uacute;a Liễu mang những đường n&eacute;t chạm trổ tinh tế. C&aacute;c trụ cổng đắp nổi h&igrave;nh b&ocirc;ng sen. Nh&igrave;n lăng từ xa giống như một hồ sen cạn với 60 b&uacute;p sen hồng nổi bật. Đến với Khu di t&iacute;ch Phủ D&agrave;y, du kh&aacute;ch kh&ocirc;ng chỉ c&oacute; dịp h&agrave;nh lễ cầu những điều may mắn m&agrave; c&ograve;n c&oacute; cơ hội thưởng ngoạn cảnh sắc thi&ecirc;n nhi&ecirc;n tươi đẹp, ngắm nh&igrave;n một c&ocirc;ng tr&igrave;nh kiến tr&uacute;c trang nghi&ecirc;m, bề thế.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://baodulich.net.vn/data/data/haubg/2018/thuong-hieu-du-lich/4-2018/V%C6%B0%E1%BB%9Dn%20qu%E1%BB%91c%20gia%20Xu%C3%A2n%20Th%E1%BB%A7y.jpg\" alt=\"\" width=\"550\" height=\"295\" /></p>\r\n<p style=\"text-align: justify;\"><strong>Vườn quốc gia Xu&acirc;n Thủy </strong></p>\r\n<p style=\"text-align: justify;\">Được UNESCO c&ocirc;ng nhận l&agrave; rừng ngập mặn đầu ti&ecirc;n ở Việt Nam theo c&ocirc;ng ước Ramsar v&agrave; rừng ngập mặn thứ 50 của thế giới, Vườn quốc gia Xu&acirc;n Thủy c&oacute; tổng diện t&iacute;ch l&agrave; 12.000 ha, thuộc địa phận x&atilde; Giao Thi&ecirc;n, huyện Giao Thủy. Hệ động thực vật sinh trưởng tại đ&acirc;y rất phong ph&uacute;, c&oacute; nhiều lo&agrave;i thực vật bậc cao v&agrave; động vật qu&yacute; hiếm, khoảng 100 lo&agrave;i chim trong qu&aacute; tr&igrave;nh di cư về phương Nam lưu tr&uacute; tại đ&acirc;y.</p>\r\n<p style=\"text-align: justify;\">Đ&acirc;y cũng l&agrave; điểm du lịch sinh th&aacute;i thu h&uacute;t rất nhiều du kh&aacute;ch trong v&agrave; ngo&agrave;i nước đến tham quan v&agrave; kh&aacute;m ph&aacute;&hellip; Ch&ugrave;a Cổ Lễ Ch&ugrave;a Cổ Lễ được x&acirc;y dựng từ năm 1920, do thiền sư Nguyễn Minh Kh&ocirc;ng (thời L&yacute;) s&aacute;ng lập. Hiện nay, ch&ugrave;a thuộc địa phận thị trấn Cổ Lễ, huyện Trực Ninh. Ch&ugrave;a c&oacute; th&aacute;p 12 tầng được x&acirc;y từ năm 1926 &ndash; 1927. Ngo&agrave;i ra, ch&ugrave;a c&ograve;n lưu giữ được nhiều hiện vật văn h&oacute;a qu&yacute; hiếm thời L&yacute; như trống đồng, t&uacute;i đựng đồng&hellip; v&agrave; đặc biệt l&agrave; đại hồng chung.</p>\r\n<p style=\"text-align: justify;\"><strong>T&ograve;a gi&aacute;m mục B&ugrave;i Chu</strong></p>\r\n<p style=\"text-align: justify;\">Sở hữu rất nhiều nh&agrave; thờ cổ nguy nga với kiến tr&uacute;c đặc sắc, Nam Định được coi l&agrave; &ldquo;thủ phủ&rdquo; của c&aacute;c nh&agrave; thờ tại Việt Nam. T&ograve;a gi&aacute;m mục B&ugrave;i Chu l&agrave; một c&ocirc;ng tr&igrave;nh đạo gi&aacute;o, mang phong c&aacute;ch kiến tr&uacute;c Gothique độc đ&aacute;o, được x&acirc;y dựng từ năm 1885 v&agrave; được coi l&agrave; nơi lưu dấu t&iacute;ch của gi&aacute;o sĩ truyền gi&aacute;o đầu ti&ecirc;n (đầu thế kỷ XVIII). Hiện nay, nơi đ&acirc;y vẫn lưu giữ được nhiều t&agrave;i liệu li&ecirc;n quan đến gi&aacute;o hội, đến việc nhận nu&ocirc;i trẻ mồ c&ocirc;i v&agrave; người khuyết tật&hellip;</p>\r\n<p style=\"text-align: justify;\"><strong>Chợ Viềng </strong></p>\r\n<p style=\"text-align: justify;\">C&ograve;n gọi l&agrave; chợ &Acirc;m Phủ, chợ Viềng l&agrave; phi&ecirc;n chợ v&ocirc; c&ugrave;ng nổi tiếng v&agrave; độc đ&aacute;o chỉ họp duy nhất một lần v&agrave;o đ&ecirc;m mồng 7 đến rạng ng&agrave;y mồng 8 th&aacute;ng Gi&ecirc;ng &Acirc;m lịch. Đ&acirc;y l&agrave; n&eacute;t văn h&oacute;a truyền thống từ xa xưa của v&ugrave;ng. Người d&acirc;n đến với chợ Viềng với &yacute; niệm &ldquo;mua may b&aacute;n rủi&rdquo; cho năm mới được b&igrave;nh an, may mắn. Ch&iacute;nh v&igrave; vậy, người đến chợ kh&ocirc;ng đặt nặng vấn đề lời l&atilde;i, người b&aacute;n kh&ocirc;ng n&oacute;i th&aacute;ch v&agrave; người mua kh&ocirc;ng mặc cả. H&agrave;ng h&oacute;a được b&agrave;y b&aacute;n ở phi&ecirc;n chợ n&agrave;y chủ yếu l&agrave; c&aacute;c loại c&acirc;y trồng, n&ocirc;ng cụ, đồ cổ v&agrave; đồ giả cổ, những vật dụng nh&agrave; n&ocirc;ng, c&aacute;c mặt h&agrave;ng nhu yếu phẩm, những bộ đồ đồng,... đa chủng loại, chất lượng v&agrave; gi&aacute; th&agrave;nh.</p>\r\n<p style=\"text-align: justify;\"><strong>B&atilde;i biển Thịnh Long</strong></p>\r\n<p style=\"text-align: justify;\">Nằm tr&ecirc;n địa phận thị trấn Thịnh Long, huyện Hải Hậu, b&atilde;i biển Thịnh Long l&agrave; một trong những b&atilde;i biển đẹp, hoang sơ của Nam Định với sức quyến rũ kh&ocirc;ng hề nhỏ. B&atilde;i biển n&agrave;y sở hữu bờ c&aacute;t trải d&agrave;i, h&agrave;ng phi lao r&igrave; r&agrave;o trước gi&oacute;, s&oacute;ng biển dập d&igrave;u.</p>\r\n</body>\r\n</html>', '1', '2018-04-14 00:00:00', '2018-08-24 00:00:00', null, null, '2018-04-22 16:34:23', '2018-04-22 17:52:24', null, 'GT001', '2018-04-22 17:52:24');

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `function_type` int(11) DEFAULT NULL COMMENT '1: thẩm định 2: phê duyệt',
  `action_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=347 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notes
-- ----------------------------
INSERT INTO `notes` VALUES ('1', '1', '43', 'Từ chối thẩm định lần 1', '2018-03-13 10:58:13', '2018-03-13 10:58:13', '1', '0');
INSERT INTO `notes` VALUES ('2', '1', '43', 'Yêu cầu bổ sung lần 1', '2018-03-13 11:27:33', '2018-03-13 11:27:33', '1', '0');
INSERT INTO `notes` VALUES ('3', '1', '43', 'từ chối lần 2', '2018-03-13 11:29:14', '2018-03-13 11:29:14', '1', '0');
INSERT INTO `notes` VALUES ('4', '2', '43', 'Yêu cầu bổ sung 1', '2018-03-13 11:36:33', '2018-03-13 11:36:33', '1', '0');
INSERT INTO `notes` VALUES ('5', '5', '43', 'Từ chối lần 1', '2018-03-13 11:36:47', '2018-03-13 11:36:47', '1', '0');
INSERT INTO `notes` VALUES ('6', '7', '43', 'Từ chối lần 1', '2018-03-13 11:37:15', '2018-03-13 11:37:15', '1', '0');
INSERT INTO `notes` VALUES ('7', '2', '43', 'từ chối lần 1', '2018-03-13 11:42:23', '2018-03-13 11:42:23', '1', '0');
INSERT INTO `notes` VALUES ('8', '1', '44', 'Từ chối lần 1', '2018-03-13 13:51:04', '2018-03-13 13:51:04', '2', '0');
INSERT INTO `notes` VALUES ('9', '1', '44', 'Yêu cầu bổ sung phê duyệt 1', '2018-03-13 13:53:00', '2018-03-13 13:53:00', '2', '0');
INSERT INTO `notes` VALUES ('10', '1', '44', 'Từ chối lần 2', '2018-03-13 13:57:28', '2018-03-13 13:57:28', '2', '0');
INSERT INTO `notes` VALUES ('11', '1', '44', 'Yêu cầu bổ sung lần 3', '2018-03-13 13:57:45', '2018-03-13 13:57:45', '2', '0');
INSERT INTO `notes` VALUES ('12', '11', '43', 'yêu cầu bổ sung 1', '2018-03-13 14:02:09', '2018-03-13 14:02:09', '1', '0');
INSERT INTO `notes` VALUES ('13', '11', '43', 'Từ chối lần 1', '2018-03-13 16:10:32', '2018-03-13 16:10:32', '1', '0');
INSERT INTO `notes` VALUES ('14', '11', '43', 'Yêu cầu bổ sung lần 2', '2018-03-13 16:10:53', '2018-03-13 16:10:53', '1', '0');
INSERT INTO `notes` VALUES ('15', '11', '43', 'Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2Từ chối lần 2TừEnd', '2018-03-13 16:13:43', '2018-03-13 16:13:43', '1', '0');
INSERT INTO `notes` VALUES ('16', '11', '43', 'Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ su', '2018-03-13 16:14:00', '2018-03-13 16:14:00', '1', '0');
INSERT INTO `notes` VALUES ('17', '11', '43', 'Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổEnđ', '2018-03-13 16:14:27', '2018-03-13 16:14:27', '1', '0');
INSERT INTO `notes` VALUES ('18', '11', '43', 'Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổ sung 3Yêu cầu bổEnđ', '2018-03-13 16:37:37', '2018-03-13 16:37:37', '1', '0');
INSERT INTO `notes` VALUES ('19', '11', '43', 'Hồ sơ đã được thẩm định', '2018-03-13 16:37:43', '2018-03-13 16:37:43', '1', '0');
INSERT INTO `notes` VALUES ('20', '16', '17', 'alaklfkljfkalf', '2018-03-13 22:31:11', '2018-03-13 22:31:11', '1', '2');
INSERT INTO `notes` VALUES ('21', '17', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 09:54:47', '2018-03-14 09:54:47', '1', '3');
INSERT INTO `notes` VALUES ('22', '17', '44', 'thiếu hồ sơ', '2018-03-14 10:02:21', '2018-03-14 10:02:21', '2', '8');
INSERT INTO `notes` VALUES ('23', '17', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 10:04:01', '2018-03-14 10:04:01', '2', '4');
INSERT INTO `notes` VALUES ('24', '22', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 10:51:56', '2018-03-14 10:51:56', '1', '3');
INSERT INTO `notes` VALUES ('25', '22', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 10:55:27', '2018-03-14 10:55:27', '2', '4');
INSERT INTO `notes` VALUES ('26', '34', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 11:29:26', '2018-03-14 11:29:26', '1', '3');
INSERT INTO `notes` VALUES ('27', '35', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 11:29:38', '2018-03-14 11:29:38', '1', '3');
INSERT INTO `notes` VALUES ('28', '36', '43', 'Hồ sơ đã được thẩm định', '2018-03-14 11:29:45', '2018-03-14 11:29:45', '1', '3');
INSERT INTO `notes` VALUES ('29', '34', '44', 'd', '2018-03-14 11:30:26', '2018-03-14 11:30:26', '2', '8');
INSERT INTO `notes` VALUES ('30', '35', '44', 'c', '2018-03-14 11:30:34', '2018-03-14 11:30:34', '2', '8');
INSERT INTO `notes` VALUES ('31', '36', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 11:31:04', '2018-03-14 11:31:04', '2', '4');
INSERT INTO `notes` VALUES ('32', '34', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 11:31:24', '2018-03-14 11:31:24', '2', '4');
INSERT INTO `notes` VALUES ('33', '35', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 11:31:40', '2018-03-14 11:31:40', '2', '4');
INSERT INTO `notes` VALUES ('34', '31', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:32:26', '2018-03-14 11:32:26', '1', '3');
INSERT INTO `notes` VALUES ('35', '29', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:33:07', '2018-03-14 11:33:07', '1', '3');
INSERT INTO `notes` VALUES ('36', '28', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:33:45', '2018-03-14 11:33:45', '1', '3');
INSERT INTO `notes` VALUES ('37', '27', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:34:16', '2018-03-14 11:34:16', '1', '3');
INSERT INTO `notes` VALUES ('38', '25', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 11:34:57', '2018-03-14 11:34:57', '1', '3');
INSERT INTO `notes` VALUES ('39', '24', '59', 'dfsdfsdfsdf', '2018-03-14 11:43:24', '2018-03-14 11:43:24', '1', '7');
INSERT INTO `notes` VALUES ('40', '33', '1', 'Từ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thẩm địnhTừ chối thEnd', '2018-03-14 14:25:30', '2018-03-14 14:25:30', '1', '7');
INSERT INTO `notes` VALUES ('41', '33', '43', 'Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cầu bổ sung Yêu cEnđ', '2018-03-11 13:40:26', '2018-03-14 14:21:04', '1', '2');
INSERT INTO `notes` VALUES ('42', '32', '43', 'Từ chối thẩm định', '2018-03-14 13:41:42', '2018-03-14 13:41:42', '1', '7');
INSERT INTO `notes` VALUES ('43', '32', '43', 'Yêu cầu bổ sung lần 2', '2018-03-14 13:43:34', '2018-03-14 13:43:34', '1', '2');
INSERT INTO `notes` VALUES ('44', '30', '43', 'Từ chối thẩm định lần 1', '2018-03-26 09:52:49', '2018-03-26 09:52:49', '1', '7');
INSERT INTO `notes` VALUES ('45', '23', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 13:59:11', '2018-03-14 13:59:11', '1', '3');
INSERT INTO `notes` VALUES ('46', '18', '59', 'dfdsfsdfsdfsd', '2018-03-14 14:00:03', '2018-03-14 14:00:03', '1', '2');
INSERT INTO `notes` VALUES ('47', '33', '43', 'Từ chối lần 2', '2018-03-09 14:06:44', '2018-03-14 14:22:10', '1', '7');
INSERT INTO `notes` VALUES ('48', '32', '43', 'Từ chối lần 2', '2018-03-14 14:06:59', '2018-03-14 14:06:59', '1', '7');
INSERT INTO `notes` VALUES ('49', '30', '43', 'từ chối lần 2', '2018-03-26 09:52:52', '2018-03-26 09:52:52', '1', '7');
INSERT INTO `notes` VALUES ('50', '26', '43', 'Từ chối lần 2', '2018-03-14 14:07:28', '2018-03-14 14:07:28', '1', '7');
INSERT INTO `notes` VALUES ('51', '19', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 14:20:39', '2018-03-14 14:20:39', '1', '3');
INSERT INTO `notes` VALUES ('52', '33', '43', 'Yêu cầu bổ sung 2', '2018-03-12 14:22:44', '2018-03-14 14:23:14', '1', '2');
INSERT INTO `notes` VALUES ('53', '33', '43', 'Hồ sơ đã được thẩm định', '2018-03-13 14:23:46', '2018-03-14 14:34:26', '1', '3');
INSERT INTO `notes` VALUES ('54', '32', '43', 'Yêu cầu bổ sung 3', '2018-03-14 14:34:11', '2018-03-14 14:34:11', '1', '2');
INSERT INTO `notes` VALUES ('55', '24', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 14:35:14', '2018-03-14 14:35:14', '1', '3');
INSERT INTO `notes` VALUES ('56', '18', '59', 'Hồ sơ đã được thẩm định', '2018-03-14 14:35:31', '2018-03-14 14:35:31', '1', '3');
INSERT INTO `notes` VALUES ('57', '33', '44', 'Từ chối phê duyệt lần 1', '2018-03-07 14:38:32', '2018-03-14 14:39:49', '2', '8');
INSERT INTO `notes` VALUES ('58', '33', '44', 'Yêu cầu bổ sung phê duyệt lần 1', '2018-03-08 14:44:48', '2018-03-14 14:45:28', '2', '2');
INSERT INTO `notes` VALUES ('59', '33', '44', 'Hồ sơ đã được phê duyệt', '2018-03-14 14:45:44', '2018-03-14 14:45:44', '2', '4');
INSERT INTO `notes` VALUES ('60', '19', '60', 'dsfsdgfsdgsdfsdfsdfsdfsdf', '2018-03-14 15:27:18', '2018-03-14 15:27:18', '2', '2');
INSERT INTO `notes` VALUES ('61', '19', '60', 'lần 2', '2018-03-14 15:27:50', '2018-03-14 15:27:50', '2', '2');
INSERT INTO `notes` VALUES ('62', '19', '60', 'lần 3', '2018-03-14 15:28:38', '2018-03-14 15:28:38', '2', '8');
INSERT INTO `notes` VALUES ('63', '19', '60', 'lần 4', '2018-03-14 15:28:52', '2018-03-14 15:28:52', '2', '2');
INSERT INTO `notes` VALUES ('64', '19', '60', 'Hồ sơ đã được phê duyệt', '2018-03-14 15:29:07', '2018-03-14 15:29:07', '2', '4');
INSERT INTO `notes` VALUES ('65', '11', '44', 'Đồng ý phê duyệt', '2018-03-14 15:47:01', '2018-03-14 15:47:01', '2', '4');
INSERT INTO `notes` VALUES ('66', '31', '44', 'Đồng ý phê duyệt', '2018-03-14 15:47:14', '2018-03-14 15:47:14', '2', '4');
INSERT INTO `notes` VALUES ('67', '29', '44', 'Đồng ý phê duyệt', '2018-03-14 15:48:05', '2018-03-14 15:48:05', '2', '4');
INSERT INTO `notes` VALUES ('68', '28', '44', 'Đồng ý phê duyệt', '2018-03-14 15:48:18', '2018-03-14 15:48:18', '2', '4');
INSERT INTO `notes` VALUES ('69', '27', '44', 'Đồng ý phê duyệt', '2018-03-14 15:48:29', '2018-03-14 15:48:29', '2', '4');
INSERT INTO `notes` VALUES ('70', '25', '44', 'Đồng ý phê duyệt', '2018-03-14 15:48:41', '2018-03-14 15:48:41', '2', '4');
INSERT INTO `notes` VALUES ('71', '24', '44', 'Đồng ý phê duyệt', '2018-03-14 15:49:05', '2018-03-14 15:49:05', '2', '4');
INSERT INTO `notes` VALUES ('72', '23', '44', 'Đồng ý phê duyệt', '2018-03-14 15:49:18', '2018-03-14 15:49:18', '2', '4');
INSERT INTO `notes` VALUES ('73', '18', '44', 'Đồng ý phê duyệt', '2018-03-14 15:49:28', '2018-03-14 15:49:28', '2', '4');
INSERT INTO `notes` VALUES ('74', '1', '17', 'Đồng ý phê duyệt', '2018-03-14 17:26:59', '2018-03-14 17:26:59', '2', '4');
INSERT INTO `notes` VALUES ('75', '37', '43', 'Đồng ý thẩm định', '2018-03-14 17:30:52', '2018-03-14 17:30:52', '1', '3');
INSERT INTO `notes` VALUES ('76', '38', '59', 'Đồng ý thẩm định', '2018-03-14 17:32:08', '2018-03-14 17:32:08', '1', '3');
INSERT INTO `notes` VALUES ('77', '37', '44', 'Đồng ý phê duyệt', '2018-03-14 17:32:12', '2018-03-14 17:32:12', '2', '4');
INSERT INTO `notes` VALUES ('78', '21', '59', 'Đồng ý thẩm định', '2018-03-14 17:32:18', '2018-03-14 17:32:18', '1', '3');
INSERT INTO `notes` VALUES ('79', '21', '44', 'sdfasdfa', '2018-03-15 10:00:18', '2018-03-15 10:00:18', '2', '2');
INSERT INTO `notes` VALUES ('80', '21', '44', 'sdfasdfasf', '2018-03-15 10:00:31', '2018-03-15 10:00:31', '2', '8');
INSERT INTO `notes` VALUES ('81', '21', '44', 'sdfasdfasf\r\nsdfasf', '2018-03-15 10:00:39', '2018-03-15 10:00:39', '2', '2');
INSERT INTO `notes` VALUES ('82', '21', '44', 'ssd', '2018-03-15 10:00:45', '2018-03-15 10:00:45', '2', '8');
INSERT INTO `notes` VALUES ('83', '21', '44', 'yeu cau bo sung', '2018-03-15 10:35:48', '2018-03-15 10:35:48', '2', '2');
INSERT INTO `notes` VALUES ('84', '21', '44', 'yeu cau bo sung', '2018-03-15 10:37:09', '2018-03-15 10:37:09', '2', '8');
INSERT INTO `notes` VALUES ('85', '40', '43', 'yêu cầu bổ sung hồ sơ', '2018-03-15 11:42:56', '2018-03-15 11:42:56', '1', '2');
INSERT INTO `notes` VALUES ('86', '49', '17', 'jajaja', '2018-03-08 13:54:40', '2018-03-15 14:32:17', '1', '2');
INSERT INTO `notes` VALUES ('87', '49', '43', 'Từ chối 1', '2018-03-09 13:58:49', '2018-03-15 14:32:25', '1', '7');
INSERT INTO `notes` VALUES ('88', '43', '59', 'bổ sung 1', '2018-03-15 14:07:21', '2018-03-15 14:07:21', '1', '2');
INSERT INTO `notes` VALUES ('89', '41', '10', 'yeu cau bo sung lan 1', '2018-03-15 14:22:50', '2018-03-15 14:22:50', '1', '2');
INSERT INTO `notes` VALUES ('90', '41', '10', 'tu choi lan 1', '2018-03-15 14:25:55', '2018-03-15 14:25:55', '1', '7');
INSERT INTO `notes` VALUES ('91', '49', '43', 'Yêu cầu bổ sung 2', '2018-03-10 14:29:19', '2018-03-15 14:32:32', '1', '2');
INSERT INTO `notes` VALUES ('92', '43', '59', 'Đồng ý thẩm định', '2018-03-15 14:31:15', '2018-03-15 14:31:15', '1', '3');
INSERT INTO `notes` VALUES ('93', '44', '59', 'Đồng ý thẩm định', '2018-03-15 14:31:34', '2018-03-15 14:31:34', '1', '3');
INSERT INTO `notes` VALUES ('94', '46', '59', 'Đồng ý thẩm định', '2018-03-15 14:31:44', '2018-03-15 14:31:44', '1', '3');
INSERT INTO `notes` VALUES ('95', '53', '17', 'aaaaaaa', '2018-03-15 14:34:57', '2018-03-15 14:34:57', '1', '2');
INSERT INTO `notes` VALUES ('96', '49', '43', 'Từ chối 2', '2018-03-11 14:35:44', '2018-03-15 14:38:33', '1', '7');
INSERT INTO `notes` VALUES ('97', '54', '17', 'ssssss', '2018-03-15 14:38:49', '2018-03-15 14:38:49', '1', '7');
INSERT INTO `notes` VALUES ('98', '52', '43', 'Đồng ý thẩm định', '2018-03-15 14:42:04', '2018-03-15 14:42:04', '1', '3');
INSERT INTO `notes` VALUES ('99', '41', '43', 'Đồng ý thẩm định', '2018-03-15 14:42:44', '2018-03-15 14:42:44', '1', '3');
INSERT INTO `notes` VALUES ('100', '48', '43', 'Từ chối lần 1', '2018-03-15 15:06:35', '2018-03-15 15:06:35', '1', '7');
INSERT INTO `notes` VALUES ('101', '48', '43', 'Yêu cầu bổ xung lần 1', '2018-03-15 15:55:10', '2018-03-15 15:55:10', '1', '2');
INSERT INTO `notes` VALUES ('102', '45', '43', 'từ chối lần 1', '2018-03-15 15:57:02', '2018-03-15 15:57:02', '1', '7');
INSERT INTO `notes` VALUES ('103', '52', '17', 'Yêu cầu bổ sung hồ sơ từ Hà', '2018-03-15 16:14:40', '2018-03-15 16:14:40', '2', '2');
INSERT INTO `notes` VALUES ('104', '42', '43', 'từ chối thẩm định lần 1', '2018-03-15 16:20:50', '2018-03-15 16:20:50', '1', '7');
INSERT INTO `notes` VALUES ('105', '49', '43', 'Đồng ý thẩm định', '2018-03-02 16:22:33', '2018-03-15 16:58:02', '1', '3');
INSERT INTO `notes` VALUES ('106', '48', '43', 'Đồng ý thẩm định', '2018-03-15 16:27:01', '2018-03-15 16:27:01', '1', '3');
INSERT INTO `notes` VALUES ('107', '45', '43', 'Đồng ý thẩm định', '2018-03-15 16:27:18', '2018-03-15 16:27:18', '1', '3');
INSERT INTO `notes` VALUES ('108', '42', '43', 'Đồng ý thẩm định', '2018-03-15 16:27:31', '2018-03-15 16:27:31', '1', '3');
INSERT INTO `notes` VALUES ('109', '32', '59', 'Đồng ý thẩm định', '2018-03-15 16:31:01', '2018-03-15 16:31:01', '1', '3');
INSERT INTO `notes` VALUES ('110', '30', '59', 'từ chối', '2018-03-26 09:52:55', '2018-03-26 09:52:55', '1', '7');
INSERT INTO `notes` VALUES ('111', '30', '43', 'Đồng ý thẩm định', '2018-03-26 09:52:58', '2018-03-26 09:52:58', '1', '3');
INSERT INTO `notes` VALUES ('112', '26', '59', 'Đồng ý thẩm định', '2018-03-15 16:33:43', '2018-03-15 16:33:43', '1', '3');
INSERT INTO `notes` VALUES ('113', '26', '43', 'Từ chối lần 2', '2018-03-15 16:33:43', '2018-03-15 16:33:43', '1', '2');
INSERT INTO `notes` VALUES ('114', '26', '43', 'Từ chối lần 2', '2018-03-15 16:36:07', '2018-03-15 16:36:07', '1', '2');
INSERT INTO `notes` VALUES ('115', '26', '43', 'bổ sung lần 3', '2018-03-15 16:36:47', '2018-03-15 16:36:47', '1', '2');
INSERT INTO `notes` VALUES ('116', '57', '59', 'Đồng ý thẩm định', '2018-03-15 16:37:57', '2018-03-15 16:37:57', '1', '3');
INSERT INTO `notes` VALUES ('117', '55', '59', 'Đồng ý thẩm định', '2018-03-15 16:40:59', '2018-03-15 16:40:59', '1', '3');
INSERT INTO `notes` VALUES ('118', '55', '60', 'dgrtfrtfr', '2018-03-15 16:44:08', '2018-03-15 16:44:08', '2', '8');
INSERT INTO `notes` VALUES ('119', '55', '60', '87897987', '2018-03-15 16:44:29', '2018-03-15 16:44:29', '2', '2');
INSERT INTO `notes` VALUES ('120', '57', '60', 'Đồng ý phê duyệt', '2018-03-15 16:45:45', '2018-03-15 16:45:45', '2', '4');
INSERT INTO `notes` VALUES ('121', '49', '44', 'Từ chối thẩm định 1', '2018-03-01 17:07:32', '2018-03-15 17:09:25', '2', '8');
INSERT INTO `notes` VALUES ('122', '49', '44', 'yêu cầu bổ sung lần 1', '2018-03-16 09:28:25', '2018-03-16 09:28:25', '2', '22');
INSERT INTO `notes` VALUES ('123', '59', '43', '', '2018-03-16 09:31:12', '2018-03-16 09:31:12', '1', '3');
INSERT INTO `notes` VALUES ('124', '49', '44', 'từ chối lần 2', '2018-03-16 09:31:20', '2018-03-16 09:31:20', '2', '8');
INSERT INTO `notes` VALUES ('125', '49', '44', 'yêu cầu bổ sung lần 2', '2018-03-16 09:31:46', '2018-03-16 09:31:46', '2', '22');
INSERT INTO `notes` VALUES ('126', '49', '44', 'từ chối lần 3', '2018-03-16 09:32:10', '2018-03-16 09:32:10', '2', '8');
INSERT INTO `notes` VALUES ('127', '49', '44', 'bổ sung thông tin lần 3', '2018-03-16 09:34:03', '2018-03-16 09:34:03', '2', '22');
INSERT INTO `notes` VALUES ('128', '42', '44', 'yêu cầu bổ sung', '2018-03-16 09:35:08', '2018-03-16 09:35:08', '2', '22');
INSERT INTO `notes` VALUES ('129', '59', '44', '', '2018-03-16 09:35:54', '2018-03-16 09:35:54', '2', '4');
INSERT INTO `notes` VALUES ('130', '49', '44', '', '2018-03-16 09:39:13', '2018-03-16 09:39:13', '2', '4');
INSERT INTO `notes` VALUES ('131', '42', '44', '', '2018-03-16 09:39:46', '2018-03-16 09:39:46', '2', '4');
INSERT INTO `notes` VALUES ('132', '48', '44', '', '2018-03-16 09:41:41', '2018-03-16 09:41:41', '2', '4');
INSERT INTO `notes` VALUES ('133', '32', '44', '', '2018-03-16 09:41:55', '2018-03-16 09:41:55', '2', '4');
INSERT INTO `notes` VALUES ('134', '45', '44', '', '2018-03-16 09:43:42', '2018-03-16 09:43:42', '2', '4');
INSERT INTO `notes` VALUES ('135', '60', '59', 'bổ sung lần 1', '2018-03-16 09:44:35', '2018-03-16 09:44:35', '1', '2');
INSERT INTO `notes` VALUES ('136', '3', '44', 'vsdfs', '2018-03-26 09:53:04', '2018-03-26 09:53:04', '2', '22');
INSERT INTO `notes` VALUES ('137', '51', '43', 'xcvzxv', '2018-03-16 10:19:13', '2018-03-16 10:19:13', '1', '7');
INSERT INTO `notes` VALUES ('138', '54', '43', 'dfasdfsd', '2018-03-16 10:19:37', '2018-03-16 10:19:37', '1', '7');
INSERT INTO `notes` VALUES ('139', '54', '43', 'dfasdfsd\r\nsdfsdf', '2018-03-16 10:20:11', '2018-03-16 10:20:11', '1', '2');
INSERT INTO `notes` VALUES ('140', '54', '43', '', '2018-03-16 10:20:32', '2018-03-16 10:20:32', '1', '3');
INSERT INTO `notes` VALUES ('141', '46', '60', '', '2018-03-16 10:47:36', '2018-03-16 10:47:36', '2', '4');
INSERT INTO `notes` VALUES ('142', '44', '60', '', '2018-03-16 10:47:55', '2018-03-16 10:47:55', '2', '4');
INSERT INTO `notes` VALUES ('143', '43', '60', '', '2018-03-16 10:48:31', '2018-03-16 10:48:31', '2', '4');
INSERT INTO `notes` VALUES ('144', '54', '60', '', '2018-03-16 10:52:49', '2018-03-16 10:52:49', '2', '4');
INSERT INTO `notes` VALUES ('145', '38', '60', '', '2018-03-16 10:53:14', '2018-03-16 10:53:14', '2', '4');
INSERT INTO `notes` VALUES ('146', '61', '43', '!@@$$^*(*)()\r\nboor sung', '2018-03-16 11:03:18', '2018-03-16 11:03:18', '1', '2');
INSERT INTO `notes` VALUES ('147', '62', '17', 'Yeeu caau bo sung abce x', '2018-03-16 11:16:55', '2018-03-16 11:16:55', '1', '2');
INSERT INTO `notes` VALUES ('148', '62', '17', 'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer to', '2018-03-16 11:19:07', '2018-03-16 11:19:07', '1', '7');
INSERT INTO `notes` VALUES ('149', '53', '17', 'Yêu cầu bổ sung, check mail', '2018-03-16 11:29:57', '2018-03-16 11:29:57', '1', '2');
INSERT INTO `notes` VALUES ('150', '71', '43', 'ádaas', '2018-03-16 14:00:18', '2018-03-16 14:00:18', '1', '7');
INSERT INTO `notes` VALUES ('151', '65', '43', '', '2018-03-16 14:21:07', '2018-03-16 14:21:07', '1', '3');
INSERT INTO `notes` VALUES ('152', '65', '44', '', '2018-03-16 14:21:27', '2018-03-16 14:21:27', '2', '4');
INSERT INTO `notes` VALUES ('153', '71', '43', '', '2018-03-16 15:00:09', '2018-03-16 15:00:09', '1', '3');
INSERT INTO `notes` VALUES ('154', '71', '44', '', '2018-03-16 15:00:40', '2018-03-16 15:00:40', '2', '4');
INSERT INTO `notes` VALUES ('155', '77', '43', 'từ chối', '2018-03-16 15:20:05', '2018-03-16 15:20:05', '1', '7');
INSERT INTO `notes` VALUES ('156', '70', '43', 'từ chối', '2018-03-16 15:30:01', '2018-03-16 15:30:01', '1', '7');
INSERT INTO `notes` VALUES ('157', '67', '43', 'từ chối', '2018-03-16 15:30:20', '2018-03-16 15:30:20', '1', '7');
INSERT INTO `notes` VALUES ('158', '66', '43', 'từ', '2018-03-16 15:30:36', '2018-03-16 15:30:36', '1', '7');
INSERT INTO `notes` VALUES ('159', '70', '43', 'yêu cầu bổ sung', '2018-03-16 15:33:08', '2018-03-16 15:33:08', '1', '2');
INSERT INTO `notes` VALUES ('160', '67', '43', 'yêu cầu bổ sung', '2018-03-16 15:33:27', '2018-03-16 15:33:27', '1', '2');
INSERT INTO `notes` VALUES ('161', '66', '43', 'yêu cầu bổ sung', '2018-03-16 15:33:42', '2018-03-16 15:33:42', '1', '2');
INSERT INTO `notes` VALUES ('162', '16', '24', 'asssssssssssss', '2018-03-16 15:38:19', '2018-03-16 15:38:19', '1', '2');
INSERT INTO `notes` VALUES ('163', '79', '59', 'từ chối lần 1', '2018-03-16 16:20:45', '2018-03-16 16:20:45', '1', '7');
INSERT INTO `notes` VALUES ('164', '79', '59', 'bổ sung lần 1', '2018-03-16 16:22:09', '2018-03-16 16:22:09', '1', '2');
INSERT INTO `notes` VALUES ('165', '79', '59', 'bổ sung lần 2', '2018-03-16 16:27:13', '2018-03-16 16:27:13', '1', '2');
INSERT INTO `notes` VALUES ('166', '79', '59', 'bổ sung lần 3', '2018-03-16 16:49:08', '2018-03-16 16:49:08', '1', '2');
INSERT INTO `notes` VALUES ('167', '79', '59', 'bổ sung lần 4', '2018-03-16 16:58:12', '2018-03-16 16:58:12', '1', '2');
INSERT INTO `notes` VALUES ('168', '79', '59', 'bổ sung lần 5', '2018-03-16 17:06:48', '2018-03-16 17:06:48', '1', '2');
INSERT INTO `notes` VALUES ('169', '80', '59', '', '2018-03-16 17:38:24', '2018-03-16 17:38:24', '1', '3');
INSERT INTO `notes` VALUES ('170', '80', '60', 'bổ sung phê duyệt 1', '2018-03-16 17:38:54', '2018-03-16 17:38:54', '2', '22');
INSERT INTO `notes` VALUES ('171', '82', '43', 'bs', '2018-03-19 14:59:55', '2018-03-19 14:59:55', '1', '2');
INSERT INTO `notes` VALUES ('172', '79', '43', 'gfdgd', '2018-03-19 15:00:16', '2018-03-19 15:00:16', '1', '7');
INSERT INTO `notes` VALUES ('173', '83', '43', 'thiếu thông tin cá nhân', '2018-03-19 15:44:25', '2018-03-19 15:44:25', '1', '2');
INSERT INTO `notes` VALUES ('174', '83', '43', 'thiếu thông tin cá nhân 3', '2018-03-19 15:58:44', '2018-03-19 15:58:44', '1', '2');
INSERT INTO `notes` VALUES ('175', '83', '43', '', '2018-03-19 16:00:32', '2018-03-19 16:00:32', '1', '3');
INSERT INTO `notes` VALUES ('176', '83', '44', 'bổ sung thêm ghi chú', '2018-03-19 16:09:25', '2018-03-19 16:09:25', '2', '22');
INSERT INTO `notes` VALUES ('177', '83', '44', '', '2018-03-19 16:27:28', '2018-03-19 16:27:28', '2', '4');
INSERT INTO `notes` VALUES ('178', '85', '43', 'ycbs', '2018-03-20 09:01:40', '2018-03-20 09:01:40', '1', '2');
INSERT INTO `notes` VALUES ('179', '85', '43', '', '2018-03-20 09:08:59', '2018-03-20 09:08:59', '1', '3');
INSERT INTO `notes` VALUES ('180', '85', '44', 'thiếu hs', '2018-03-20 09:09:19', '2018-03-20 09:09:19', '2', '22');
INSERT INTO `notes` VALUES ('181', '86', '43', 'ytryrt', '2018-03-20 10:23:36', '2018-03-20 10:23:36', '1', '2');
INSERT INTO `notes` VALUES ('182', '86', '43', '', '2018-03-20 10:24:58', '2018-03-20 10:24:58', '1', '3');
INSERT INTO `notes` VALUES ('183', '86', '44', 'ỷtyrtyrty', '2018-03-20 10:25:23', '2018-03-20 10:25:23', '2', '22');
INSERT INTO `notes` VALUES ('184', '87', '43', 'yêu cầu bổ sung', '2018-03-21 09:03:38', '2018-03-21 09:03:38', '1', '2');
INSERT INTO `notes` VALUES ('185', '41', '44', '', '2018-03-21 09:46:43', '2018-03-21 09:46:43', '2', '4');
INSERT INTO `notes` VALUES ('186', '81', '43', '', '2018-03-21 09:49:25', '2018-03-21 09:49:25', '1', '3');
INSERT INTO `notes` VALUES ('187', '81', '60', '', '2018-03-21 09:51:38', '2018-03-21 09:51:38', '2', '4');
INSERT INTO `notes` VALUES ('188', '68', '43', 'yêu cầu bổ sung', '2018-03-21 09:58:07', '2018-03-21 09:58:07', '1', '2');
INSERT INTO `notes` VALUES ('189', '89', '43', '', '2018-03-21 10:46:52', '2018-03-21 10:46:52', '1', '3');
INSERT INTO `notes` VALUES ('190', '26', '17', 'Trần Thị Thanh Hà, Yêu cầu bổ sung', '2018-03-22 10:51:10', '2018-03-22 10:51:10', '1', '2');
INSERT INTO `notes` VALUES ('191', '87', '43', 'yêu cầu bổ sung', '2018-03-22 14:05:46', '2018-03-22 14:05:46', '1', '2');
INSERT INTO `notes` VALUES ('192', '87', '43', '', '2018-03-22 14:07:42', '2018-03-22 14:07:42', '1', '3');
INSERT INTO `notes` VALUES ('193', '87', '44', 'bs2', '2018-03-22 14:08:33', '2018-03-22 14:08:33', '2', '22');
INSERT INTO `notes` VALUES ('194', '89', '44', 'bs', '2018-03-22 14:12:59', '2018-03-22 14:12:59', '2', '22');
INSERT INTO `notes` VALUES ('195', '19', '43', '', '2018-03-23 10:51:15', '2018-03-23 10:51:15', '1', '3');
INSERT INTO `notes` VALUES ('196', '93', '43', 'yêu cầu bổ sug', '2018-03-23 10:59:10', '2018-03-23 10:59:10', '1', '2');
INSERT INTO `notes` VALUES ('197', '93', '43', 'từ chối', '2018-03-23 11:02:25', '2018-03-23 11:02:25', '1', '7');
INSERT INTO `notes` VALUES ('198', '93', '43', '', '2018-03-23 11:11:36', '2018-03-23 11:11:36', '1', '3');
INSERT INTO `notes` VALUES ('199', '93', '44', 'YCBS', '2018-03-23 11:13:46', '2018-03-23 11:13:46', '2', '22');
INSERT INTO `notes` VALUES ('200', '93', '44', 'từ chối', '2018-03-23 11:20:00', '2018-03-23 11:20:00', '2', '8');
INSERT INTO `notes` VALUES ('201', '93', '44', '', '2018-03-23 11:22:56', '2018-03-23 11:22:56', '2', '4');
INSERT INTO `notes` VALUES ('202', '94', '43', '', '2018-03-23 15:13:02', '2018-03-23 15:13:02', '1', '3');
INSERT INTO `notes` VALUES ('203', '94', '44', '', '2018-03-23 15:14:17', '2018-03-23 15:14:17', '2', '4');
INSERT INTO `notes` VALUES ('204', '19', '44', 'fedf', '2018-03-23 17:05:30', '2018-03-23 17:05:30', '2', '8');
INSERT INTO `notes` VALUES ('205', '16', '17', 'Từ chối check giao diện', '2018-03-24 16:23:18', '2018-03-24 16:23:18', '1', '7');
INSERT INTO `notes` VALUES ('206', '26', '17', 'Yêu cầu bổ sung', '2018-03-24 22:13:33', '2018-03-24 22:13:33', '1', '2');
INSERT INTO `notes` VALUES ('207', '26', '17', 'hahahaha', '2018-03-24 22:14:17', '2018-03-24 22:14:17', '1', '7');
INSERT INTO `notes` VALUES ('208', '16', '17', 'nnnn', '2018-03-24 22:17:32', '2018-03-24 22:17:32', '1', '2');
INSERT INTO `notes` VALUES ('209', '96', '43', '', '2018-03-26 08:43:50', '2018-03-26 08:43:50', '1', '3');
INSERT INTO `notes` VALUES ('210', '96', '44', '', '2018-03-26 08:44:33', '2018-03-26 08:44:33', '2', '4');
INSERT INTO `notes` VALUES ('211', '80', '44', 'bổ sung phê duyệt lần 2', '2018-03-26 09:58:50', '2018-03-26 09:58:50', '2', '22');
INSERT INTO `notes` VALUES ('212', '76', '43', '', '2018-03-26 10:20:34', '2018-03-26 10:20:34', '1', '3');
INSERT INTO `notes` VALUES ('213', '76', '44', 'bổ sung lần 1', '2018-03-26 10:21:03', '2018-03-26 10:21:03', '2', '22');
INSERT INTO `notes` VALUES ('214', '70', '43', 'từ chối', '2018-03-26 10:26:20', '2018-03-26 10:26:20', '1', '7');
INSERT INTO `notes` VALUES ('215', '70', '43', '', '2018-03-26 10:27:39', '2018-03-26 10:27:39', '1', '3');
INSERT INTO `notes` VALUES ('216', '70', '44', 'tgrfgy', '2018-03-26 10:27:56', '2018-03-26 10:27:56', '2', '8');
INSERT INTO `notes` VALUES ('217', '76', '24', 'Hà thực hiện yêu cầu bổ sung', '2018-03-26 11:12:41', '2018-03-26 11:12:41', '2', '22');
INSERT INTO `notes` VALUES ('218', '99', '43', 'fgdgd', '2018-03-26 11:53:53', '2018-03-26 11:53:53', '1', '7');
INSERT INTO `notes` VALUES ('219', '99', '43', '', '2018-03-26 11:54:02', '2018-03-26 11:54:02', '1', '3');
INSERT INTO `notes` VALUES ('220', '99', '44', 'gff', '2018-03-26 11:54:22', '2018-03-26 11:54:22', '2', '8');
INSERT INTO `notes` VALUES ('221', '99', '44', 'dfgdfg', '2018-03-26 11:54:34', '2018-03-26 11:54:34', '2', '22');
INSERT INTO `notes` VALUES ('222', '99', '44', '', '2018-03-26 11:54:44', '2018-03-26 11:54:44', '2', '4');
INSERT INTO `notes` VALUES ('223', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 15:46:46', '2018-03-26 15:46:46', '2', '22');
INSERT INTO `notes` VALUES ('224', '100', '43', '', '2018-03-26 16:19:29', '2018-03-26 16:19:29', '1', '3');
INSERT INTO `notes` VALUES ('225', '101', '43', '', '2018-03-26 16:19:37', '2018-03-26 16:19:37', '1', '3');
INSERT INTO `notes` VALUES ('226', '102', '43', '', '2018-03-26 16:19:44', '2018-03-26 16:19:44', '1', '3');
INSERT INTO `notes` VALUES ('227', '103', '43', '', '2018-03-26 16:19:51', '2018-03-26 16:19:51', '1', '3');
INSERT INTO `notes` VALUES ('228', '103', '44', '', '2018-03-26 16:24:53', '2018-03-26 16:24:53', '2', '4');
INSERT INTO `notes` VALUES ('229', '102', '44', '', '2018-03-26 16:25:06', '2018-03-26 16:25:06', '2', '4');
INSERT INTO `notes` VALUES ('230', '101', '44', '', '2018-03-26 16:25:24', '2018-03-26 16:25:24', '2', '4');
INSERT INTO `notes` VALUES ('231', '100', '44', '', '2018-03-26 16:25:37', '2018-03-26 16:25:37', '2', '4');
INSERT INTO `notes` VALUES ('232', '3', '17', 'Từ chối Phê duyệt cập nhật hồ sơ', '2018-03-26 18:40:39', '2018-03-26 18:40:39', '2', '8');
INSERT INTO `notes` VALUES ('233', '85', '17', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 18:55:18', '2018-03-26 18:55:18', '2', '22');
INSERT INTO `notes` VALUES ('234', '1', '2', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 23:04:02', '2018-03-26 23:04:02', '2', '22');
INSERT INTO `notes` VALUES ('235', '1', '2', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 23:06:38', '2018-03-26 23:06:38', '2', '22');
INSERT INTO `notes` VALUES ('236', '1', '2', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 23:16:21', '2018-03-26 23:16:21', '2', '22');
INSERT INTO `notes` VALUES ('237', '1', '2', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 23:21:05', '2018-03-26 23:21:05', '2', '22');
INSERT INTO `notes` VALUES ('238', '1', '2', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 23:23:51', '2018-03-26 23:23:51', '2', '22');
INSERT INTO `notes` VALUES ('239', '1', '2', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 23:25:34', '2018-03-26 23:25:34', '2', '22');
INSERT INTO `notes` VALUES ('240', '1', '2', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-26 23:38:17', '2018-03-26 23:38:17', '2', '22');
INSERT INTO `notes` VALUES ('241', '113', '43', '', '2018-03-27 08:22:23', '2018-03-27 08:22:23', '1', '3');
INSERT INTO `notes` VALUES ('242', '113', '44', '', '2018-03-27 08:22:45', '2018-03-27 08:22:45', '2', '4');
INSERT INTO `notes` VALUES ('243', '59', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 08:37:56', '2018-03-27 08:37:56', '2', '22');
INSERT INTO `notes` VALUES ('244', '114', '43', '', '2018-03-27 10:06:55', '2018-03-27 10:06:55', '1', '3');
INSERT INTO `notes` VALUES ('245', '114', '44', '', '2018-03-27 10:07:27', '2018-03-27 10:07:27', '2', '4');
INSERT INTO `notes` VALUES ('246', '115', '43', '', '2018-03-27 10:17:30', '2018-03-27 10:17:30', '1', '3');
INSERT INTO `notes` VALUES ('247', '115', '44', '', '2018-03-27 10:17:54', '2018-03-27 10:17:54', '2', '4');
INSERT INTO `notes` VALUES ('248', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 10:48:30', '2018-03-27 10:48:30', '2', '22');
INSERT INTO `notes` VALUES ('249', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 10:48:56', '2018-03-27 10:48:56', '2', '22');
INSERT INTO `notes` VALUES ('250', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 10:51:48', '2018-03-27 10:51:48', '2', '22');
INSERT INTO `notes` VALUES ('251', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 10:52:35', '2018-03-27 10:52:35', '2', '22');
INSERT INTO `notes` VALUES ('252', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 10:52:47', '2018-03-27 10:52:47', '2', '22');
INSERT INTO `notes` VALUES ('253', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 10:53:25', '2018-03-27 10:53:25', '2', '22');
INSERT INTO `notes` VALUES ('254', '83', '17', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 11:27:13', '2018-03-27 11:27:13', '2', '22');
INSERT INTO `notes` VALUES ('255', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 11:34:08', '2018-03-27 11:34:08', '2', '22');
INSERT INTO `notes` VALUES ('256', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-27 13:48:43', '2018-03-27 13:48:43', '2', '22');
INSERT INTO `notes` VALUES ('257', '116', '43', '', '2018-03-27 14:07:24', '2018-03-27 14:07:24', '1', '3');
INSERT INTO `notes` VALUES ('258', '116', '44', '', '2018-03-27 14:08:15', '2018-03-27 14:08:15', '2', '4');
INSERT INTO `notes` VALUES ('259', '117', '43', 'bổ sung', '2018-03-27 14:29:39', '2018-03-27 14:29:39', '1', '2');
INSERT INTO `notes` VALUES ('260', '117', '43', '', '2018-03-27 14:38:50', '2018-03-27 14:38:50', '1', '3');
INSERT INTO `notes` VALUES ('261', '117', '44', 'check lại', '2018-03-27 14:39:25', '2018-03-27 14:39:25', '2', '22');
INSERT INTO `notes` VALUES ('262', '118', '43', 'gdfgdf', '2018-03-27 15:05:19', '2018-03-27 15:05:19', '1', '2');
INSERT INTO `notes` VALUES ('263', '119', '43', 'bs', '2018-03-27 15:38:17', '2018-03-27 15:38:17', '1', '2');
INSERT INTO `notes` VALUES ('264', '120', '43', 'dfgdfgd', '2018-03-27 15:59:17', '2018-03-27 15:59:17', '1', '2');
INSERT INTO `notes` VALUES ('265', '121', '43', 'dgfdgdf', '2018-03-27 16:01:35', '2018-03-27 16:01:35', '1', '2');
INSERT INTO `notes` VALUES ('266', '118', '43', '', '2018-03-27 16:45:57', '2018-03-27 16:45:57', '1', '3');
INSERT INTO `notes` VALUES ('267', '118', '44', '', '2018-03-27 16:46:25', '2018-03-27 16:46:25', '2', '4');
INSERT INTO `notes` VALUES ('268', '123', '43', 'gdfgfg', '2018-03-27 16:58:58', '2018-03-27 16:58:58', '1', '2');
INSERT INTO `notes` VALUES ('269', '124', '17', 'Check ký tự ATTT từ chối', '2018-03-28 00:10:08', '2018-03-28 00:10:08', '1', '7');
INSERT INTO `notes` VALUES ('270', '124', '17', 'Check ký tự ATTT yêu cầu bổ sung khi thẩm định', '2018-03-28 00:11:14', '2018-03-28 00:11:14', '1', '2');
INSERT INTO `notes` VALUES ('271', '123', '43', '', '2018-03-28 08:34:30', '2018-03-28 08:34:30', '1', '3');
INSERT INTO `notes` VALUES ('272', '123', '44', '', '2018-03-28 08:35:03', '2018-03-28 08:35:03', '2', '4');
INSERT INTO `notes` VALUES ('273', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 09:09:59', '2018-03-28 09:09:59', '2', '22');
INSERT INTO `notes` VALUES ('274', '128', '43', '', '2018-03-28 09:45:09', '2018-03-28 09:45:09', '1', '3');
INSERT INTO `notes` VALUES ('275', '128', '44', '', '2018-03-28 09:51:29', '2018-03-28 09:51:29', '2', '4');
INSERT INTO `notes` VALUES ('276', '30', '44', '', '2018-03-28 09:54:05', '2018-03-28 09:54:05', '2', '4');
INSERT INTO `notes` VALUES ('277', '7', '17', '', '2018-03-28 10:12:23', '2018-03-28 10:12:23', '2', '4');
INSERT INTO `notes` VALUES ('278', '129', '43', 'booer sung', '2018-03-28 10:18:40', '2018-03-28 10:18:40', '1', '2');
INSERT INTO `notes` VALUES ('279', '129', '43', 'booer sung', '2018-03-28 10:21:13', '2018-03-28 10:21:13', '1', '2');
INSERT INTO `notes` VALUES ('280', '87', '17', '', '2018-03-28 10:21:19', '2018-03-28 10:21:19', '2', '4');
INSERT INTO `notes` VALUES ('281', '80', '17', '', '2018-03-28 10:21:55', '2018-03-28 10:21:55', '2', '4');
INSERT INTO `notes` VALUES ('282', '130', '43', 'vxfgvdf', '2018-03-28 10:30:53', '2018-03-28 10:30:53', '1', '2');
INSERT INTO `notes` VALUES ('283', '130', '43', '', '2018-03-28 10:40:54', '2018-03-28 10:40:54', '1', '3');
INSERT INTO `notes` VALUES ('284', '130', '44', 'laptop', '2018-03-28 10:41:27', '2018-03-28 10:41:27', '2', '22');
INSERT INTO `notes` VALUES ('285', '130', '43', '', '2018-03-28 10:44:33', '2018-03-28 10:44:33', '1', '3');
INSERT INTO `notes` VALUES ('286', '130', '44', '', '2018-03-28 10:47:14', '2018-03-28 10:47:14', '2', '4');
INSERT INTO `notes` VALUES ('287', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 14:18:46', '2018-03-28 14:18:46', '2', '22');
INSERT INTO `notes` VALUES ('288', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 14:19:12', '2018-03-28 14:19:12', '2', '22');
INSERT INTO `notes` VALUES ('289', '83', '17', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 14:28:08', '2018-03-28 14:28:08', '2', '22');
INSERT INTO `notes` VALUES ('290', '83', '17', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 14:31:14', '2018-03-28 14:31:14', '2', '22');
INSERT INTO `notes` VALUES ('291', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 15:01:45', '2018-03-28 15:01:45', '2', '22');
INSERT INTO `notes` VALUES ('292', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 15:02:39', '2018-03-28 15:02:39', '2', '22');
INSERT INTO `notes` VALUES ('293', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 15:03:17', '2018-03-28 15:03:17', '2', '22');
INSERT INTO `notes` VALUES ('294', '131', '43', '', '2018-03-28 15:13:52', '2018-03-28 15:13:52', '1', '3');
INSERT INTO `notes` VALUES ('295', '131', '44', '', '2018-03-28 15:14:20', '2018-03-28 15:14:20', '2', '4');
INSERT INTO `notes` VALUES ('296', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 15:30:22', '2018-03-28 15:30:22', '2', '22');
INSERT INTO `notes` VALUES ('297', '132', '43', '', '2018-03-28 16:03:08', '2018-03-28 16:03:08', '1', '3');
INSERT INTO `notes` VALUES ('298', '132', '44', '', '2018-03-28 16:03:29', '2018-03-28 16:03:29', '2', '4');
INSERT INTO `notes` VALUES ('299', '132', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 16:09:51', '2018-03-28 16:09:51', '2', '22');
INSERT INTO `notes` VALUES ('300', '133', '43', '', '2018-03-28 16:14:26', '2018-03-28 16:14:26', '1', '3');
INSERT INTO `notes` VALUES ('301', '133', '44', '', '2018-03-28 16:14:43', '2018-03-28 16:14:43', '2', '4');
INSERT INTO `notes` VALUES ('302', '134', '43', '', '2018-03-28 16:28:58', '2018-03-28 16:28:58', '1', '3');
INSERT INTO `notes` VALUES ('303', '134', '44', '', '2018-03-28 16:29:17', '2018-03-28 16:29:17', '2', '4');
INSERT INTO `notes` VALUES ('304', '134', '17', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-28 17:53:04', '2018-03-28 17:53:04', '2', '22');
INSERT INTO `notes` VALUES ('305', '83', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-29 08:18:45', '2018-03-29 08:18:45', '2', '22');
INSERT INTO `notes` VALUES ('306', '140', '43', 'gdfgd', '2018-03-29 10:27:30', '2018-03-29 10:27:30', '1', '7');
INSERT INTO `notes` VALUES ('307', '140', '43', '', '2018-03-29 10:46:27', '2018-03-29 10:46:27', '1', '3');
INSERT INTO `notes` VALUES ('308', '140', '44', 'bcvbcv', '2018-03-29 10:46:52', '2018-03-29 10:46:52', '2', '8');
INSERT INTO `notes` VALUES ('309', '110', '17', 'yêu cầu bổ sung', '2018-03-29 10:56:14', '2018-03-29 10:56:14', '1', '2');
INSERT INTO `notes` VALUES ('310', '124', '17', 'từ chối', '2018-03-29 10:58:16', '2018-03-29 10:58:16', '1', '7');
INSERT INTO `notes` VALUES ('311', '140', '44', '', '2018-03-29 15:22:57', '2018-03-29 15:22:57', '2', '4');
INSERT INTO `notes` VALUES ('312', '76', '17', '', '2018-03-29 17:16:06', '2018-03-29 17:16:06', '2', '4');
INSERT INTO `notes` VALUES ('313', '136', '10', 'hhhh', '2018-03-29 23:49:17', '2018-03-29 23:49:17', '1', '2');
INSERT INTO `notes` VALUES ('314', '59', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-30 14:03:51', '2018-03-30 14:03:51', '2', '22');
INSERT INTO `notes` VALUES ('315', '59', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-30 15:03:33', '2018-03-30 15:03:33', '2', '22');
INSERT INTO `notes` VALUES ('316', '70', '44', '', '2018-03-30 15:12:26', '2018-03-30 15:12:26', '2', '4');
INSERT INTO `notes` VALUES ('317', '86', '44', '', '2018-03-30 15:12:42', '2018-03-30 15:12:42', '2', '4');
INSERT INTO `notes` VALUES ('318', '89', '44', '', '2018-03-30 15:13:05', '2018-03-30 15:13:05', '2', '4');
INSERT INTO `notes` VALUES ('319', '59', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-30 16:22:46', '2018-03-30 16:22:46', '2', '22');
INSERT INTO `notes` VALUES ('320', '59', '92', 'Đồng ý Phê duyệt cập nhật hồ sơ', '2018-03-30 16:24:11', '2018-03-30 16:24:11', '2', '22');
INSERT INTO `notes` VALUES ('321', '150', '59', 'thẩm định hà nội từ chối', '2018-04-02 09:14:36', '2018-04-02 09:14:36', '1', '7');
INSERT INTO `notes` VALUES ('322', '150', '59', 'thẩm định hà nội từ chối', '2018-04-02 09:16:10', '2018-04-02 09:16:10', '1', '2');
INSERT INTO `notes` VALUES ('323', '150', '43', '', '2018-04-02 09:29:26', '2018-04-02 09:29:26', '1', '3');
INSERT INTO `notes` VALUES ('324', '150', '60', 'iyuiyu', '2018-04-02 09:51:00', '2018-04-02 09:51:00', '2', '8');
INSERT INTO `notes` VALUES ('325', '150', '60', 'iyuiyuuyiuyiuy', '2018-04-02 09:51:14', '2018-04-02 09:51:14', '2', '22');
INSERT INTO `notes` VALUES ('326', '150', '60', 'iyuiyuuyiuyiuy57678', '2018-04-02 09:56:14', '2018-04-02 09:56:14', '2', '8');
INSERT INTO `notes` VALUES ('327', '150', '60', 'iyuiyuuyiuyiuy57678gdfgfgf', '2018-04-02 09:56:34', '2018-04-02 09:56:34', '2', '8');
INSERT INTO `notes` VALUES ('328', '151', '43', 'chuyên viên FF', '2018-04-02 10:05:22', '2018-04-02 10:05:22', '1', '2');
INSERT INTO `notes` VALUES ('329', '151', '59', 'thẩm định Ch', '2018-04-02 10:05:41', '2018-04-02 10:05:41', '1', '7');
INSERT INTO `notes` VALUES ('330', '151', '43', 'chueyen viên từ chối', '2018-04-02 10:06:26', '2018-04-02 10:06:26', '1', '7');
INSERT INTO `notes` VALUES ('331', '151', '59', '', '2018-04-02 10:06:42', '2018-04-02 10:06:42', '1', '3');
INSERT INTO `notes` VALUES ('332', '151', '44', 'Lãnh đạo FF', '2018-04-02 10:09:44', '2018-04-02 10:09:44', '2', '22');
INSERT INTO `notes` VALUES ('333', '151', '60', 'Phê duyệt Ch', '2018-04-02 10:10:07', '2018-04-02 10:10:07', '2', '8');
INSERT INTO `notes` VALUES ('334', '151', '44', '', '2018-04-02 10:10:59', '2018-04-02 10:10:59', '2', '4');
INSERT INTO `notes` VALUES ('335', '150', '44', 'iyuiyuuyiuyiuy57678gdfgfgf', '2018-04-02 10:29:38', '2018-04-02 10:29:38', '2', '8');
INSERT INTO `notes` VALUES ('336', '150', '44', 'iyuiyuuyiuyiuy57678gdfgfgf', '2018-04-02 10:47:23', '2018-04-02 10:47:23', '2', '22');
INSERT INTO `notes` VALUES ('337', '150', '44', 'iyuiyuuyiuyiuy57678gdfgfgf', '2018-04-02 11:28:16', '2018-04-02 11:28:16', '2', '8');
INSERT INTO `notes` VALUES ('338', '150', '44', 'iyuiyuuyiuyiuy57678gdfgfgf dfgdfgdf', '2018-04-02 11:28:53', '2018-04-02 11:28:53', '2', '22');
INSERT INTO `notes` VALUES ('339', '73', '43', 'dftgdg', '2018-04-02 16:49:31', '2018-04-02 16:49:31', '1', '7');
INSERT INTO `notes` VALUES ('340', '69', '43', '', '2018-04-02 17:20:44', '2018-04-02 17:20:44', '1', '3');
INSERT INTO `notes` VALUES ('341', '104', '43', '', '2018-04-02 17:20:58', '2018-04-02 17:20:58', '1', '3');
INSERT INTO `notes` VALUES ('342', '160', '43', '', '2018-04-04 01:32:45', '2018-04-04 01:32:45', '1', '3');
INSERT INTO `notes` VALUES ('343', '160', '44', '', '2018-04-04 01:33:12', '2018-04-04 01:33:12', '2', '4');
INSERT INTO `notes` VALUES ('344', '104', '44', '', '2018-04-09 04:17:35', '2018-04-09 04:17:35', '2', '4');
INSERT INTO `notes` VALUES ('345', '161', '43', '', '2018-04-09 04:46:58', '2018-04-09 04:46:58', '1', '3');
INSERT INTO `notes` VALUES ('346', '161', '44', '', '2018-04-09 04:53:03', '2018-04-09 04:53:03', '2', '4');

-- ----------------------------
-- Table structure for offices
-- ----------------------------
DROP TABLE IF EXISTS `offices`;
CREATE TABLE `offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addr` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_info` text COLLATE utf8mb4_unicode_ci,
  `note` text COLLATE utf8mb4_unicode_ci,
  `images` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of offices
-- ----------------------------

-- ----------------------------
-- Table structure for options
-- ----------------------------
DROP TABLE IF EXISTS `options`;
CREATE TABLE `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of options
-- ----------------------------
INSERT INTO `options` VALUES ('1', 'club', 'Câu lạc bộ tiếng nhật', null, '2018-03-23 11:43:14', '2018-03-23 11:43:17', 'CLB01');
INSERT INTO `options` VALUES ('2', 'club', 'Câu lạc bộ tiếng anh', null, '2018-03-23 11:43:34', '2018-03-23 11:43:38', 'CLB02');
INSERT INTO `options` VALUES ('4', 'position', 'Giám đốc', null, '2018-03-27 22:13:57', '2018-03-27 22:13:59', 'cdddd');
INSERT INTO `options` VALUES ('5', 'position', 'Ủy viên', null, '2018-03-27 22:14:12', '2018-03-27 22:14:15', 'ddg33');
INSERT INTO `options` VALUES ('6', 'branches', 'Chi Hội', null, '2018-04-01 21:41:33', '2018-04-01 21:41:33', 'BRANCHES01');
INSERT INTO `options` VALUES ('7', 'branches', 'CLB Thuộc Chi Hội', null, '2018-04-01 21:41:33', '2018-04-01 21:41:33', 'BRANCHES02');
INSERT INTO `options` VALUES ('8', 'branches', 'CLB Thuộc Hội', null, '2018-04-01 21:41:33', '2018-04-01 21:41:33', 'BRANCHES03');
INSERT INTO `options` VALUES ('9', 'club', 'Câu lạc bộ tiếng hàn', '0000-00-00 00:00:00', '2018-03-23 11:43:14', '2018-03-23 11:43:17', 'CLB03');
INSERT INTO `options` VALUES ('10', 'club', 'Câu lạc bộ tiếng mỹ', '0000-00-00 00:00:00', '2018-03-23 11:43:34', '2018-03-23 11:43:38', 'CLB04');
INSERT INTO `options` VALUES ('11', 'club', 'Câu lạc bộ tiếng nga', '0000-00-00 00:00:00', '2018-03-23 11:43:14', '2018-03-23 11:43:17', 'CLB05');
INSERT INTO `options` VALUES ('12', 'club', 'Câu lạc bộ tiếng pháp', '0000-00-00 00:00:00', '2018-03-23 11:43:34', '2018-03-23 11:43:38', 'CLB06');
INSERT INTO `options` VALUES ('13', 'infomation', 'Giao thông', null, '2018-04-04 22:53:53', '2018-04-04 22:53:56', 'GT001');
INSERT INTO `options` VALUES ('14', 'infomation', 'Phong tục tập quán', null, '2018-04-04 22:54:31', '2018-04-04 22:54:35', 'PT001');
INSERT INTO `options` VALUES ('15', 'infomation', 'Lễ hội', '0000-00-00 00:00:00', '2018-04-04 22:54:31', '2018-04-04 22:54:35', 'LH001');
INSERT INTO `options` VALUES ('16', 'infomation', 'Điểm đến', '0000-00-00 00:00:00', '2018-04-04 22:54:31', '2018-04-04 22:54:35', 'DD001');
INSERT INTO `options` VALUES ('17', 'infomation', 'Trang phục', '0000-00-00 00:00:00', '2018-04-04 22:54:31', '2018-04-04 22:54:35', 'TP001');
INSERT INTO `options` VALUES ('18', 'introduction', 'Về chúng tôi', null, '2018-04-09 09:23:28', '2018-04-09 09:23:28', 'INTRODUC01');
INSERT INTO `options` VALUES ('19', 'introduction', 'Tầm nhìn và sứ mệnh', null, '2018-04-09 09:23:28', '2018-04-09 09:23:28', 'INTRODUC02');
INSERT INTO `options` VALUES ('20', 'introduction', 'Quy định', null, '2018-04-09 09:23:28', '2018-04-09 09:23:28', 'INTRODUC03');
INSERT INTO `options` VALUES ('21', 'introduction', 'Quy chế hoạt động', null, '2018-04-09 09:23:28', '2018-04-09 09:23:28', 'INTRODUC04');
INSERT INTO `options` VALUES ('22', 'introduction', 'Hướng dẫn đăng ký hội viên', null, '2018-04-09 09:23:28', '2018-04-09 09:23:28', 'INTRODUC05');
INSERT INTO `options` VALUES ('23', 'news', 'Tin Tưc và Sự Kiện Hội', null, '2018-04-10 12:31:33', '2018-04-10 11:28:28', 'NEWS01');
INSERT INTO `options` VALUES ('24', 'news', 'Tin Tưc và Sự Kiện Chi Hội', null, '2018-04-10 12:31:33', '2018-04-10 12:31:33', 'NEWS02');
INSERT INTO `options` VALUES ('25', 'news', 'Tin Tưc và Sự Kiện CLB thuộc Hội', null, '2018-04-10 12:31:33', '2018-04-10 12:31:33', 'NEWS03');
INSERT INTO `options` VALUES ('26', 'department', 'Ban Thường Trực', null, '2018-04-12 12:32:30', '2018-04-12 12:32:30', 'DEPARTMENT01');
INSERT INTO `options` VALUES ('27', 'department', 'Ban Cố Vấn', null, '2018-04-12 13:32:32', '2018-04-12 09:28:29', 'DEPARTMENT02');
INSERT INTO `options` VALUES ('28', 'department', 'Ban Chuyên Môn', null, '2018-04-12 10:28:29', '2018-04-12 11:26:28', 'DEPARTMENT03');
INSERT INTO `options` VALUES ('30', 'department', 'Ban Chấp Hành', null, '2018-04-12 00:00:00', '2018-04-12 00:00:00', 'DEPARTMENT04');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` tinytext CHARACTER SET utf8,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('2', 'login', 'login', 'login', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('3', 'logout', 'logout', 'logout', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('4', 'register', 'register', 'register', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('5', 'password.request', 'password.request', 'password.request', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('6', 'password.email', 'password.email', 'password.email', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('7', 'password.reset', 'password.reset', 'password.reset', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('8', 'front_end_index', 'front_end_index', 'front_end_index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('9', 'front_end_gioithieu', 'front_end_gioithieu', 'front_end_gioithieu', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('10', 'front_end_chihoi', 'front_end_chihoi', 'front_end_chihoi', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('11', 'front_end_clb', 'front_end_clb', 'front_end_clb', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('12', 'front_end_news', 'front_end_news', 'front_end_news', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('13', 'front_end_news_detail', 'front_end_news_detail', 'front_end_news_detail', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('14', 'front_end_hoivien', 'front_end_hoivien', 'front_end_hoivien', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('15', 'front_end_redirect', 'front_end_redirect', 'front_end_redirect', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('16', 'front_end_ajax_get_news', 'front_end_ajax_get_news', 'front_end_ajax_get_news', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('17', 'front_end_ajax_get_member', 'front_end_ajax_get_member', 'front_end_ajax_get_member', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('18', 'front_end_ajax_club_of_branches', 'front_end_ajax_club_of_branches', 'front_end_ajax_club_of_branches', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('19', 'ajax_get_content_club_of_branch', 'ajax_get_content_club_of_branch', 'ajax_get_content_club_of_branch', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('20', 'home', 'home', 'home', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('21', 'profile_edit', 'profile_edit', 'profile_edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('22', 'profile', 'profile', 'profile', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('23', 'member_register_view', 'member_register_view', 'member_register_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('24', 'member_register_action', 'member_register_action', 'member_register_action', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('25', 'member_register_resend_sms_action', 'member_register_resend_sms_action', 'member_register_resend_sms_action', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('26', 'download', 'download', 'download', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('27', 'downloadZip', 'downloadZip', 'downloadZip', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('28', 'member_rejected', 'member_rejected', 'member_rejected', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('29', 'member_update', 'member_update', 'member_update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('30', 'member_update', 'member_update', 'member_update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('31', 'member_detail', 'member_detail', 'member_detail', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('32', 'admin_member', 'admin_member', 'admin_member', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('33', 'admin_member_detail_view', 'admin_member_detail_view', 'admin_member_detail_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('34', 'admin_member_detail_view_ajax', 'admin_member_detail_view_ajax', 'admin_member_detail_view_ajax', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('35', 'admin_member_detail_update_view', 'admin_member_detail_update_view', 'admin_member_detail_update_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('36', 'admin_member_detail_tmp_view', 'admin_member_detail_tmp_view', 'admin_member_detail_tmp_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('37', 'admin_member_detail_update_view', 'admin_member_detail_update_view', 'admin_member_detail_update_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('38', 'admin_member_apiDetailUser_view', 'admin_member_apiDetailUser_view', 'admin_member_apiDetailUser_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('39', 'admin_member_apiChangeStatus_view', 'admin_member_apiChangeStatus_view', 'admin_member_apiChangeStatus_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('40', 'admin_member_create_card_view', 'admin_member_create_card_view', 'admin_member_create_card_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('41', 'admin_member_do_create_card_view', 'admin_member_do_create_card_view', 'admin_member_do_create_card_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('42', 'admin_member_print_excel_member_card_view', 'admin_member_print_excel_member_card_view', 'admin_member_print_excel_member_card_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('43', 'admin_gen_pdf', 'admin_gen_pdf', 'admin_gen_pdf', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('44', 'fakeSendMailApprove', 'fakeSendMailApprove', 'fakeSendMailApprove', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('45', 'admin_member_list_payment_view', 'admin_member_list_payment_view', 'admin_member_list_payment_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('46', 'admin_member_detail_payment_view', 'admin_member_detail_payment_view', 'admin_member_detail_payment_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('47', 'payment_store', 'payment_store', 'payment_store', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('48', 'payment_update', 'payment_update', 'payment_update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('49', 'payment_destroy', 'payment_destroy', 'payment_destroy', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('50', 'payment_send_notifications', 'payment_send_notifications', 'payment_send_notifications', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('51', 'admin_payment_fee_import_view', 'admin_payment_fee_import_view', 'admin_payment_fee_import_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('52', 'admin_member_list_decision_view', 'admin_member_list_decision_view', 'admin_member_list_decision_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('53', 'member_decision_do_create_card_view', 'member_decision_do_create_card_view', 'member_decision_do_create_card_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('54', 'admin_member_update_decision_view', 'admin_member_update_decision_view', 'admin_member_update_decision_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('55', 'decision', 'decision', 'decision', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('56', 'admin_member_list_view', 'admin_member_list_view', 'admin_member_list_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('57', 'list_member_print_file', 'list_member_print_file', 'list_member_print_file', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('58', 'admin_list_member_detail_view', 'admin_list_member_detail_view', 'admin_list_member_detail_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('59', 'delete_member', 'delete_member', 'delete_member', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('60', 'update_member', 'update_member', 'update_member', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('61', 'admin_representative_office_view', 'admin_representative_office_view', 'admin_representative_office_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('62', 'admin_representative_office_edit', 'admin_representative_office_edit', 'admin_representative_office_edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('63', 'admin_representative_office_delete', 'admin_representative_office_delete', 'admin_representative_office_delete', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('64', 'admin_representative_office_update', 'admin_representative_office_update', 'admin_representative_office_update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('65', 'admin_representative_office_manager', 'admin_representative_office_manager', 'admin_representative_office_manager', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('66', 'admin_representative_office_save_manager', 'admin_representative_office_save_manager', 'admin_representative_office_save_manager', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('67', 'admin_employee_view', 'admin_employee_view', 'admin_employee_view', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('68', 'admin_employee_edit', 'admin_employee_edit', 'admin_employee_edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('69', 'admin_employee_delete', 'admin_employee_delete', 'admin_employee_delete', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('70', 'admin_employee_update', 'admin_employee_update', 'admin_employee_update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('71', 'admin_employee_create', 'admin_employee_create', 'admin_employee_create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('72', 'admin_employee_doCreate', 'admin_employee_doCreate', 'admin_employee_doCreate', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('73', 'users.index', 'users.index', 'users.index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('74', 'users.create', 'users.create', 'users.create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('75', 'users.store', 'users.store', 'users.store', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('76', 'users.show', 'users.show', 'users.show', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('77', 'users.edit', 'users.edit', 'users.edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('78', 'users.update', 'users.update', 'users.update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('79', 'users.destroy', 'users.destroy', 'users.destroy', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('80', 'introductions.index', 'introductions.index', 'introductions.index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('81', 'introductions.create', 'introductions.create', 'introductions.create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('82', 'introductions.store', 'introductions.store', 'introductions.store', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('83', 'introductions.show', 'introductions.show', 'introductions.show', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('84', 'introductions.edit', 'introductions.edit', 'introductions.edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('85', 'introductions.update', 'introductions.update', 'introductions.update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('86', 'introductions.destroy', 'introductions.destroy', 'introductions.destroy', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('87', 'categories.index', 'categories.index', 'categories.index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('88', 'categories.create', 'categories.create', 'categories.create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('89', 'categories.store', 'categories.store', 'categories.store', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('90', 'categories.show', 'categories.show', 'categories.show', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('91', 'categories.edit', 'categories.edit', 'categories.edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('92', 'categories.update', 'categories.update', 'categories.update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('93', 'categories.destroy', 'categories.destroy', 'categories.destroy', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('94', 'admin_introductions_detail', 'admin_introductions_detail', 'admin_introductions_detail', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('95', 'admin_categories_sedit_detail', 'admin_categories_sedit_detail', 'admin_categories_sedit_detail', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('96', 'admin_categories_deletdit_detail', 'admin_categories_deletdit_detail', 'admin_categories_deletdit_detail', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('97', 'club_of_head_index', 'club_of_head_index', 'club_of_head_index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('98', 'club_of_head_create', 'club_of_head_create', 'club_of_head_create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('99', 'club_of_head_store', 'club_of_head_store', 'club_of_head_store', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('100', 'club_of_head_edit', 'club_of_head_edit', 'club_of_head_edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('101', 'club_of_head_update', 'club_of_head_update', 'club_of_head_update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('102', 'club_of_head_show', 'club_of_head_show', 'club_of_head_show', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('103', 'club_of_head_delete', 'club_of_head_delete', 'club_of_head_delete', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('104', 'club_of_head_manager', 'club_of_head_manager', 'club_of_head_manager', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('105', 'club_of_head_ajax_save_member', 'club_of_head_ajax_save_member', 'club_of_head_ajax_save_member', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('106', 'club_of_head_ajax_get_clb', 'club_of_head_ajax_get_clb', 'club_of_head_ajax_get_clb', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('107', 'club_of_head_export_excel', 'club_of_head_export_excel', 'club_of_head_export_excel', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('108', 'club_of_branch_index', 'club_of_branch_index', 'club_of_branch_index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('109', 'club_of_branch_create', 'club_of_branch_create', 'club_of_branch_create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('110', 'club_of_branch_create_store', 'club_of_branch_create_store', 'club_of_branch_create_store', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('111', 'club_of_branch_edit', 'club_of_branch_edit', 'club_of_branch_edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('112', 'club_of_branch_update', 'club_of_branch_update', 'club_of_branch_update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('113', 'club_of_branch_show', 'club_of_branch_show', 'club_of_branch_show', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('114', 'club_of_branch_delete', 'club_of_branch_delete', 'club_of_branch_delete', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('115', 'club_of_branches_manager', 'club_of_branches_manager', 'club_of_branches_manager', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('116', 'club_of_branches_ajax_save_member', 'club_of_branches_ajax_save_member', 'club_of_branches_ajax_save_member', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('117', 'club_of_branch_member_index', 'club_of_branch_member_index', 'club_of_branch_member_index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('118', 'club_of_branch_member_ajax_search', 'club_of_branch_member_ajax_search', 'club_of_branch_member_ajax_search', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('119', 'club_of_branch_member_create', 'club_of_branch_member_create', 'club_of_branch_member_create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('120', 'club_of_branch_member_create', 'club_of_branch_member_create', 'club_of_branch_member_create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('121', 'club_of_branch_export_excel', 'club_of_branch_export_excel', 'club_of_branch_export_excel', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('122', 'club_of_branch_member_delete', 'club_of_branch_member_delete', 'club_of_branch_member_delete', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('123', 'club_of_branch_ajax_get_clb', 'club_of_branch_ajax_get_clb', 'club_of_branch_ajax_get_clb', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('124', 'branches_manager', 'branches_manager', 'branches_manager', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('125', 'branches_ajax_employees', 'branches_ajax_employees', 'branches_ajax_employees', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('126', 'branches_export_excel', 'branches_export_excel', 'branches_export_excel', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('127', 'branches_ajax_save_member', 'branches_ajax_save_member', 'branches_ajax_save_member', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('128', 'branch_destroy', 'branch_destroy', 'branch_destroy', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('129', 'infomation.index', 'infomation.index', 'infomation.index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('130', 'infomation.create', 'infomation.create', 'infomation.create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('131', 'infomation.store', 'infomation.store', 'infomation.store', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('132', 'infomation.show', 'infomation.show', 'infomation.show', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('133', 'infomation.edit', 'infomation.edit', 'infomation.edit', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('134', 'infomation.update', 'infomation.update', 'infomation.update', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('135', 'infomation.destroy', 'infomation.destroy', 'infomation.destroy', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('136', 'branches.index', 'branches.index', 'branches.index', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('137', 'branches.create', 'branches.create', 'branches.create', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('138', 'branches.store', 'branches.store', 'branches.store', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('139', 'branches.show', 'branches.show', 'branches.show', '2018-04-21 08:50:30', '2018-04-21 08:50:30', null);
INSERT INTO `permission` VALUES ('140', 'branches.edit', 'branches.edit', 'branches.edit', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('141', 'branches.update', 'branches.update', 'branches.update', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('142', 'branches.destroy', 'branches.destroy', 'branches.destroy', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('143', 'upload.store', 'upload.store', 'upload.store', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('144', 'front_end_special_board', 'front_end_special_board', 'front_end_special_board', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('145', 'front_end_standing_committee', 'front_end_standing_committee', 'front_end_standing_committee', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('146', 'front_end_standing_advisory_board', 'front_end_standing_advisory_board', 'front_end_standing_advisory_board', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('147', 'front_end_executive_committee', 'front_end_executive_committee', 'front_end_executive_committee', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('148', 'front_end_news_1', 'front_end_news_1', 'front_end_news_1', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('149', 'front_end_news_2', 'front_end_news_2', 'front_end_news_2', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('150', 'front_end_news_3', 'front_end_news_3', 'front_end_news_3', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('151', 'front_end_vanphongdaidien', 'front_end_vanphongdaidien', 'front_end_vanphongdaidien', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('152', 'front_vechungtoi', 'front_vechungtoi', 'front_vechungtoi', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('153', 'front_tamnhinvasumenh', 'front_tamnhinvasumenh', 'front_tamnhinvasumenh', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('154', 'front_quychehoatdong', 'front_quychehoatdong', 'front_quychehoatdong', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('155', 'front_quydinh', 'front_quydinh', 'front_quydinh', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('156', 'front_huongdandangky', 'front_huongdandangky', 'front_huongdandangky', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('157', 'front_info_traffic', 'front_info_traffic', 'front_info_traffic', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('158', 'front_info_traffic_detail', 'front_info_traffic_detail', 'front_info_traffic_detail', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('159', 'front_info_lehoi', 'front_info_lehoi', 'front_info_lehoi', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('160', 'front_info_lehoi_detail', 'front_info_lehoi_detail', 'front_info_lehoi_detail', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('161', 'front_info_diemden', 'front_info_diemden', 'front_info_diemden', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('162', 'front_info_diemden_detail', 'front_info_diemden_detail', 'front_info_diemden_detail', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('163', 'front_info_trangphuc', 'front_info_trangphuc', 'front_info_trangphuc', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('164', 'front_info_trangphuc_detail', 'front_info_trangphuc_detail', 'front_info_trangphuc_detail', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('165', 'front_info_customandpractices', 'front_info_customandpractices', 'front_info_customandpractices', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);
INSERT INTO `permission` VALUES ('166', 'front_info_customandpractices_detail', 'front_info_customandpractices_detail', 'front_info_customandpractices_detail', '2018-04-21 08:50:31', '2018-04-21 08:50:31', null);

-- ----------------------------
-- Table structure for permission_user
-- ----------------------------
DROP TABLE IF EXISTS `permission_user`;
CREATE TABLE `permission_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_route_name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13213 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permission_user
-- ----------------------------
INSERT INTO `permission_user` VALUES ('1', '1', '1', '2018-04-21 08:19:00', '2018-04-21 08:19:00', null);
INSERT INTO `permission_user` VALUES ('2', '1', '1', '2018-04-21 08:33:52', '2018-04-21 08:33:52', null);
INSERT INTO `permission_user` VALUES ('3', 'login', '174', '2018-04-22 10:27:25', '2018-04-22 13:06:26', null);
INSERT INTO `permission_user` VALUES ('4', 'register', '174', '2018-04-22 10:27:25', '2018-04-22 13:06:26', null);
INSERT INTO `permission_user` VALUES ('5', 'password.email', '174', '2018-04-22 12:58:56', '2018-04-22 13:06:26', null);
INSERT INTO `permission_user` VALUES ('6', 'password.email', '174', '2018-04-22 12:58:56', '2018-04-22 12:59:51', '2018-04-22 12:59:51');
INSERT INTO `permission_user` VALUES ('7', 'front_end_index', '174', '2018-04-22 12:58:56', '2018-04-22 13:06:26', null);
INSERT INTO `permission_user` VALUES ('8', 'front_end_chihoi', '174', '2018-04-22 13:04:49', '2018-04-22 13:04:53', '2018-04-22 13:04:53');
INSERT INTO `permission_user` VALUES ('13212', 'login', '24', '2018-04-22 13:52:40', '2018-04-22 13:52:40', null);

-- ----------------------------
-- Table structure for typeguides
-- ----------------------------
DROP TABLE IF EXISTS `typeguides`;
CREATE TABLE `typeguides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `typeName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of typeguides
-- ----------------------------
INSERT INTO `typeguides` VALUES ('1', 'Du lịch sinh thái mai sửa', '2017-12-01 00:08:38', '2017-12-01 00:08:38', '0');
INSERT INTO `typeguides` VALUES ('2', 'Du lịch văn hóa- lịch sử', '2017-12-01 00:08:38', '2017-12-01 00:08:38', '1');
INSERT INTO `typeguides` VALUES ('3', 'Du lịch tâm linh', '2017-12-01 21:55:54', '2017-12-01 22:54:56', '1');
INSERT INTO `typeguides` VALUES ('4', 'Du lịch thể thao-mạo hiểm', '2017-12-01 21:53:53', '2017-12-01 21:53:53', '1');
INSERT INTO `typeguides` VALUES ('5', 'Du lịch MICE, sự kiện', '2017-12-01 21:55:54', '2017-12-01 22:54:56', '1');
INSERT INTO `typeguides` VALUES ('6', 'Du lịch nghiên cứu-học tập', '2017-12-01 21:53:53', '2017-12-01 21:53:53', '1');
INSERT INTO `typeguides` VALUES ('7', 'Du lịch nghỉ dưỡng-chữa bệnh', '2017-12-01 21:55:54', '2017-12-01 22:54:56', '1');
INSERT INTO `typeguides` VALUES ('8', 'Loại hình khác', '2017-12-01 21:53:53', '2017-12-01 21:53:53', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `province_type` tinyint(4) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `memberId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Duong Dai Dao', 'daiduong47', '3', '1', 'daiduong47@gmail.com', '$2y$10$xjrhWR8C.xoczR8El5yoUO7tMThDxRWJgArFcQ06miutXeV4t6RyO', '1268ZarE5VXHJZs8sXiOrWmxLfFA7qZz9zikLRconXffN4BYsL7IvVD3xxAi', '2017-12-01 02:22:22', '2017-12-01 02:22:22', '1', null, null);
INSERT INTO `users` VALUES ('2', 'hattt', 'hattt', '3', '1', 'hattt@eprtech.com', '$2y$10$Oe0H487tJeD1qieoDT2GOuelC.qeKhSkgGar9Gr8124GOOxE8Fgby', 'S3OAyStpL1bNc9iDLJsd6q7UuarQTwzrVA8guaXqHgwdQxXBIrtfjeJk1EYk', '2017-12-01 08:53:00', '2017-12-01 08:53:00', '1', null, '1');
INSERT INTO `users` VALUES ('3', 'Ha Tran Thi Thanh', 'hattt123', '1', '1', 'hattt1@eprtech.com', '$2y$10$Lj9VIoxwvEyfq/fhkoA.zufAx97lcbzflYSLBve9Gck8fRlDB8BHW', '8cjQIYFR2BqImR0uBmqtvDdrPKda6uqaOveg6mZHq3xvwJvP1ecpQLgQYWbI', '2017-12-12 16:37:46', '2017-12-12 16:37:46', '3', null, '3');
INSERT INTO `users` VALUES ('4', 'admin', 'administrator', '3', '1', 'admin@gmail.com', '$2y$10$f4mlE8POn6gP31kJN3B1rexIE/8jDHtj0L.XNASYisvS.8/2l12fy', '7XJNB7hlBRXFzo5M2RneiKiBu8ls5Q55MWbeYOpPyO5IxkYMPCBboHldoAyB', '2017-12-12 16:39:40', '2017-12-12 16:39:40', '1', null, null);
INSERT INTO `users` VALUES ('5', 'Chuyên viên', 'daiduong472', '6', '1', 'daiduong472@gmail.com', '$2y$10$K3Ao1Gk9Kyj7igIIgP94I.YaPDEXAkePe6fSAoO6USr3f5jmSGfSC', 'MzIvyAdbDzMBYf9PDdRgpnQwZdUkThON3c108yqhoDDPIuLgF874QpcBOMzv', '2017-12-12 23:51:37', '2017-12-12 23:51:37', '1', null, null);
INSERT INTO `users` VALUES ('6', 'Dương Văn Phượng', 'phuongdv', '6', '1', 'duongphuongit@gmail.com', '$2y$10$FkLr.d382OSDEuuCAmw16e5RcB99q0PGhB0BnrsNUoO4qoSwFK4Iq', 'MZEliNFD2LiMZPtSDBqJS91UNiaKjCtpE8XEY6AOJt2QnX8GyHdGPxrMMjtE', '2017-12-13 01:34:58', '2017-12-13 01:34:58', '1', null, null);
INSERT INTO `users` VALUES ('7', 'tran thi thanh ha', 'hattt2', '73', '1', 'hattt+1@eprtech.com', '$2y$10$i7vbLHQ0kJzq4cwyThULPueRUjpXcN4tV0IFi9lTWEPL7ApulaqqS', 'EDAqjlxlMtOwCoj9nOuAlTI0YTH8NZX8IhWF1d9swbPJiMIOpcEQLEoxZJm9', '2017-12-15 03:05:51', '2017-12-15 03:05:51', '1', null, null);
INSERT INTO `users` VALUES ('8', 'bui van dung', 'trusohanoi', '7', '1', 'buivandungvn@gmail.com', '$2y$10$M4Q0e1I9r2qRe3XugiZn/epHBbNvufMJGznFqBu0bYetmiDgltwTu', null, '2017-12-15 06:33:35', '2017-12-15 06:33:35', '1', null, null);
INSERT INTO `users` VALUES ('9', 'nguyễn thị thanh thủy', 'thanhthuy123', '6', '1', 'nguyenthanhthuy21490@gmail.com', '$2y$10$1o6f77KMpbhZit6Gxpmw9OQd70UP9KyI6ngyjvnGMmoebcGEhXZi.', null, '2017-12-15 09:50:47', '2017-12-15 09:50:47', '1', null, null);
INSERT INTO `users` VALUES ('10', 'Chuyên viên Hội', 'cvh', '3', '1', 'chuyenvienhoi@hhdulichvn.vn', '$2y$10$Gf7MLSNEaFhHH62pcMB5su3cAiDT/7UtoNGJwbx.Z5ls/zq6yUdSy', 'qbxkS8D79m7IHPiL6WgqNHeIq8tG9wrZqHgBzmZoLBQDJBRh2MOF1DWjHl2g', '2017-12-20 15:06:48', '2017-12-20 15:06:48', '1', null, null);
INSERT INTO `users` VALUES ('11', 'Lãnh đạo Hội', 'ldh', '8', '1', 'lanhdaohoi@hhdulichvn.vn', '$2y$10$Oe0H487tJeD1qieoDT2GOuelC.qeKhSkgGar9Gr8124GOOxE8Fgby', 'qimkEZWkf48tUeI2MwQuZLrurZK7RfTR0mLUHS8J7jg2ZgrdZ1ulBC8t0F8n', '2017-12-20 15:28:49', '2017-12-20 15:28:49', '1', null, null);
INSERT INTO `users` VALUES ('12', 'Chuyên Viên Đà Nẵng', 'chuyenviendn', '62', '1', 'chuyenviendn@hhdulich.vn', '$2y$10$3Zc5GeJwQII6Kmzi3CyMH.ygEeERP1K4VDaM/A4vntdZhFSwid0l.', 'hXplMiCrdguYdQN9rddimZZUPC1H4QDXjlMWLznuFdskgKndN0o2pzsAp6tt', '2017-12-21 02:35:08', '2017-12-21 02:35:08', '2', null, null);
INSERT INTO `users` VALUES ('13', 'Lãnh đạo VPĐD Đà Nẵng', 'lanhdaodn', '72', '1', 'lanhdaodn@hhdulich.vn', '$2y$10$voMAlwUJK.YSblHkQza2F.NhP42Y43DtgrrWVydjPz8UZMWwHqIOS', 'T1nHnI8ez7BcjelrIfjCBcc41Ik9Euw11xpbRUZJaPQ7KBnENw7hjV2E00sh', '2017-12-21 02:38:58', '2017-12-21 02:38:58', '2', null, null);
INSERT INTO `users` VALUES ('14', 'Chuyên viên Hồ Chí Minh', 'chuyenvienhcm', '63', '1', 'chuyenvienhcm@hhdulich.vn', '$2y$10$6Sr52wNLwfpDu.jc6XlVDuGcX2rFn2Zbz2S/UH.ehAafgDS7Wt0Nm', 'BI5zjOKqcRp8F4u8LdavkZ6ML1ynMqyjigyvOcAsIVY8x0l0xjgrFPZz4T1n', '2017-12-21 02:40:47', '2017-12-21 02:40:47', '3', null, null);
INSERT INTO `users` VALUES ('15', 'Lãnh đạo VPĐD Hồ Chí Minh', 'lanhdaohcm', '73', '1', 'lanhdaohcm@hhdulich.vn', '$2y$10$yrKS1PKZD4zP6fGdO77WL.ussx2J2MG.Z.uhQN6JyTB4GHPjqm8bS', 'IHk9zpU5mpiaVZfTGeVRZWGQamtGHzuechZIQjRSzGUxUTwjZYqnY55LotvW', '2017-12-21 02:41:37', '2017-12-21 02:41:37', '3', null, null);
INSERT INTO `users` VALUES ('16', 'Chuyên viên hội đà nẵng 2', 'chuyenviendn2', '9', '1', 'chuyenviendn2@gmail.com', '$2y$10$61BfafM7HyrrtCA6i1LWyeG5Grr/YlfeTYPX7ePdOADHJ1BLUsJpG', 'FGThjmQXrSkyn40q9toakhAq1HbChjKzjXZD8eWm7JPJql3eGyeg4X8nLIlV', '2017-12-21 02:59:26', '2017-12-21 02:59:26', '2', null, null);
INSERT INTO `users` VALUES ('17', 'Kế toán hội', 'ktoanhoi', '3', '1', 'kethoanhoi@hhdulich.vn', '$2y$10$eDh1dAzfeMg/rRO1YcSZJenMyURsBvQPkphEHsj22Uz3GF5CMjXs6', 'ehhnFQqLONRORDSJSB56SdeNgV3d8ON7rZ0Nig4MKxhHwTrBwL8WyRvJqP8T', '2017-12-23 14:23:05', '2018-01-17 16:14:10', '1', null, '140');
INSERT INTO `users` VALUES ('18', 'Nguyễn Xuân Bình', 'binhnx2000', '1', '1', 'binhnx2000@yahoo.com', '$2y$10$u.pnTRVbn3Gkh/25c0qvuOw2mmCWjfWNSzSykCU7JNjUi/R9lKZ3a', null, '2018-01-12 02:32:35', '2018-01-12 02:32:35', '1', null, null);
INSERT INTO `users` VALUES ('19', 'Đỗ Tuấn Việt', 'amiviet2012', '1', '1', 'amiviet2012@gmail.com', '$2y$10$442T12bSoDHn/j7PwlOr7eXzZKih0aPcy3ExHgBzSXP49MuopbDb2', null, '2018-01-12 02:33:06', '2018-01-12 02:33:06', '1', null, null);
INSERT INTO `users` VALUES ('20', 'Chuyên viên Hà Nội', 'cvhn', '71', '1', 'cvhn@gmail.com', '$2y$10$4cekl0IFLGWFGYjlVQI.nu06bSSmOQgaK.assOtiFTXyQqtm3wd1G', 'qimkEZWkf48tUeI2MwQuZLrurZK7RfTR0mLUHS8J7jg2ZgrdZ1ulBC8t0F8n', '2018-01-17 16:13:05', '2018-01-17 16:13:05', '1', null, null);
INSERT INTO `users` VALUES ('21', 'Lãnh đạo hà nội', 'ldhn', '71', '1', 'ldhn@gmail.com', '$2y$10$BwJL3cvO56fMHcd0MGkD0OIEagY56m89QzFletzQV1k1tVyAu3Wia', 'KDfutHTJNgOU3SBso1dJFZ7HHUtXps2kTstK9JOOJXOvJEJ1LXMloGrA2KGk', '2018-01-17 16:13:36', '2018-01-17 16:13:36', '1', null, null);
INSERT INTO `users` VALUES ('22', 'Chuyên viên Hà Nội', 'HoangLong', '9', '1', 'hoanglonghna@gmail.com', '$2y$10$nwef2nMGZKgil6AVkKC7H.s/nGZLFFfwJFmfhpDD4aBi44Eo0dxMC', 'Ftuh3iWwnGs605hCGh3yLMZRbzBt8izFcZr09LCFkpBzOZrazmJaJ7vcEi9j', '2018-01-17 16:13:05', '2018-01-17 16:13:05', '1', null, null);
INSERT INTO `users` VALUES ('23', 'Nguyễn Xuân Bình', 'binhnx2000', '1', '1', '1binhnx2000@yahoo.com', '$2y$10$CKgLekARuwwi713ivAjE3.pcP7f9dv8kiWbP5aW2pumCvz/gQkCk2', null, '2018-02-03 14:54:15', '2018-02-03 14:54:15', '1', null, null);
INSERT INTO `users` VALUES ('24', 'Administrator', 'admin', 'QuanTriVienHoi', '1', 'admin@hhdulich.vn', '$2y$10$Oe0H487tJeD1qieoDT2GOuelC.qeKhSkgGar9Gr8124GOOxE8Fgby', 'KwjGTPt0BxMFI9uV1dCbOgtEQmYLB1Hadcr2oqp42C05GTd6wBzTpPStcg54', '2017-12-22 17:23:05', '2018-04-22 13:21:46', '1', null, '1');
INSERT INTO `users` VALUES ('25', 'Họ tên cvtd HN', 'Tên đăng nhập cvtd HN', '61', '1', 'cvtdhn@gmail.com.vn', '$2y$10$IIFOcmbc2VWmLfuS6dyNnuOIcK.PVSOpuhG4DIz2APR2Pe2jtrgYK', 'AjJuOSsiOVBLjxVqEvKX20oUa9OAOUijzJ6ICKSFIWmn7ljudEt2UHFaxN7M', '2018-02-09 07:45:45', '2018-02-09 07:45:45', '1', null, null);
INSERT INTO `users` VALUES ('26', 'Họ tên cvtd HCM', 'Tên đăng nhập cvtd HCM', '63', '1', 'cvtdhcm@gmail.com', '$2y$10$wYwR5RA/rieqRRgQnv684Oxynb7/Ym6y.pGmM2slWSY73T3Zi77c2', 'ziMxWFv4dCvjcjF8KMML9lHF4JJouXTeJblblsNBqvrq33JKaoBiyLsLXVdS', '2018-02-09 08:08:12', '2018-02-09 08:08:12', '3', null, null);
INSERT INTO `users` VALUES ('27', 'họ tên ld hcm', 'tên đăng nhập ldhcm', '73', '1', 'ldhcm@gmail.com', '$2y$10$bDo1srWkIIfUWQuscMVUGeJK4GLzkHBMaq/VCQEjIikM5eEn1aTe.', 'xA3xuuKk0oguaXDMmdzt6bFUZa3UD1hRHbPmEHvjlqZ9DL77qtmlI2elDs1F', '2018-02-09 09:34:47', '2018-02-09 10:11:25', '3', null, null);
INSERT INTO `users` VALUES ('28', 'nguyễn thị an', 'annthn@hhdulich.vn', '1', '1', 'annt@hhdulich.vn', '$2y$10$VQ2RtjT1xwrT8AvvgKxKJeknpLVD0beGjwQVx4xkRCoGSnjyn/KZ6', null, '2018-02-09 09:37:48', '2018-02-09 09:37:48', '1', null, null);
INSERT INTO `users` VALUES ('29', 'họ tên kế toán', 'tên đăng nhập kế toán', '2', '1', 'kt@gmail.com', '$2y$10$AIzzZfTOhWlnQYOZgIfgm.cchV3Pzo.UpTY9j008KE3iOrDA2AiCy', 'yZDPrYTu6bqEvizuO5DecynXBmvg4GXOlishH1rGIHQUolE9NNjXz65golp3', '2018-02-09 09:38:19', '2018-02-09 09:38:19', '1', null, null);
INSERT INTO `users` VALUES ('30', 'Ngoan Cố', 'tranha08430', '1', '1', '65_tranha084@gmail.com', '$2y$10$XLgBGzRHq0hBd4OoWhvVkOQ.hfgyf/cLLISRCVcRVQNUi01O764jq', null, '2018-02-10 21:21:50', '2018-02-10 21:21:50', '2', null, null);
INSERT INTO `users` VALUES ('31', 'NGHIÊM TRUNG VIỆT', 'nghiemtrung.viet31', '1', '1', '2_nghiemtrung.viet@gmail.com', '$2y$10$.3o1ir7/ficittpRnrW4bu7cDL3DPlv0uUHJFccR5B56yboLvaFfO', null, '2018-02-10 21:40:56', '2018-02-10 21:40:56', '1', null, '11');
INSERT INTO `users` VALUES ('32', 'Ngoan Cố', 'tranha08418', '1', '1', '98_tranha084@gmail.com', '$2y$10$R5cVGQKQONTwC3TydjSThOBPvhpZH29QEOxxnxzxMtRtrVRtSO30a', null, '2018-02-10 22:00:29', '2018-02-10 22:00:29', '2', null, '114');
INSERT INTO `users` VALUES ('33', 'Nguyễn Quang Trung', 'quangtrung.dl081214', '1', '1', '13_quangtrung.dl0812@gmail.com', '$2y$10$yAmwZ2J1r3Yt6ADsYb/oGONwqcPbreoY.KGGVhNN0mKFVQvKUasyO', null, '2018-02-11 01:52:23', '2018-02-11 01:52:23', '1', null, '1');
INSERT INTO `users` VALUES ('34', 'Nguyễn Quang Trung', 'quangtrung.dl081278', '1', '1', '63_quangtrung.dl0812@gmail.com', '$2y$10$7fEfNf9mAVQr.E0y6w8aQO08F2Wq5.IypvElhRgAtOYmJ36go8FAe', null, '2018-02-11 02:27:34', '2018-02-11 02:27:34', '1', null, '1');
INSERT INTO `users` VALUES ('35', 'Tran Xuan Toi', 'xuantoi9099', '1', '1', '42_xuantoi90@gmail.com', '$2y$10$7tlCjhlW5.R6XTEEl7jFxeGYCRt4im4SjAZ6WA3wMDNG5N7CVQT7S', null, '2018-02-11 03:23:15', '2018-02-11 03:23:15', '1', null, '115');
INSERT INTO `users` VALUES ('36', 'Nguyễn Quang Trung', 'quangtrung.dl081296', '1', '1', '30_quangtrung.dl0812@gmail.com', '$2y$10$UWg3BXFTEHcEcrGsQmIoHeHR6ZG/DDXTs5LVtB4umDMWZYrZYNYhe', null, '2018-02-11 04:40:31', '2018-02-11 04:40:31', '1', null, '1');
INSERT INTO `users` VALUES ('37', 'Nguyễn Quang Trung', 'quangtrung.dl081240', '1', '1', '21_quangtrung.dl0812@gmail.com', '$2y$10$BuoEsfkRNIIPjN.iMb7cXuxe0LDfClER/IkA9dh0y3u0hZaEN4S9S', null, '2018-02-11 04:42:42', '2018-02-11 04:42:42', '1', null, '1');
INSERT INTO `users` VALUES ('38', 'Nguyễn Quang Trung', 'quangtrung.dl08121', '1', '1', '60_quangtrung.dl0812@gmail.com', '$2y$10$etVqW4GmXwkPuMhxBkwaJO08g1UIZyaCk3BHVeQuq6zsEzLXsrHHK', null, '2018-02-11 04:43:36', '2018-02-11 04:43:36', '1', null, '1');
INSERT INTO `users` VALUES ('39', 'Nguyễn Đức Hệ', 'dexi19752', '1', '1', '40_dexi1975@yahoo.com.vn', '$2y$10$XfImDwc77/hlhWZSBATIyelCllGcnYryLM4dadyznAzt1ZiTXlwu.', null, '2018-02-11 04:52:07', '2018-02-11 04:52:07', '1', null, '3');
INSERT INTO `users` VALUES ('40', 'Đỗ Tuấn Việt', 'amiviet201254', '1', '1', '33_amiviet2012@gmail.com', '$2y$10$L0TiaJkQT6rB9lfEuZlj0u7kiKkEXM9tPMDVZbYEZ6WU.AlxA1RPe', null, '2018-02-11 08:22:48', '2018-02-11 08:22:48', '1', null, '10');
INSERT INTO `users` VALUES ('41', 'Nguyễn Quang Trung', 'quangtrung.dl081292', '1', '1', '26_quangtrung.dl0812@gmail.com', '$2y$10$K3iaWb4Rsia5ZKuQF.RDbOFHdKzbAHPEnFeapZzsh4MH5pEnLdL4.', null, '2018-02-11 10:04:45', '2018-02-11 10:04:45', '1', null, '1');
INSERT INTO `users` VALUES ('42', 'Tran Xuan Toi', 'xuantoi9062', '1', '1', '58_xuantoi90@gmail.com', '$2y$10$6fZWkr5edut51NHCEtld7unYLQWyuHKZmv82kB2MzOFcz6i4i50L2', null, '2018-02-12 17:54:07', '2018-02-12 17:54:07', '1', null, '115');
INSERT INTO `users` VALUES ('43', 'Chuyên viên thẩm định hà nội', 'cvtdhn', '61', '1', 'cvtdhn@viettel.com.vn', '$2y$10$nAbCJky.ePJh9WncGXypaOVG3/3yUgErQu/z2O3iLBf2eBYrB/9Vq', '9mNYtkDwCdKnnxlUcBql28m2xpZEBdpgj1Dn5Z8WZoEvuiIsWdCdnrZM7GlR', '2018-02-22 07:14:04', '2018-02-22 07:14:04', '1', null, null);
INSERT INTO `users` VALUES ('44', 'Lãnh đạo thẩm định hà nội', 'ldtdhn', '71', '1', 'ldtdhn@viettel.com.vn', '$2y$10$WGKYR/vsnZx4ahku3xl8y.a862ngkTh3ehRD/GVSzQ4Iu8Xo.RJp2', 'uiuQ8v40Fy0T9KHyZ7TKwpXTwtntaER2aQ9U2mAaSeUN70a9aIXcSlB7XVfm', '2018-02-22 07:43:09', '2018-02-22 07:43:09', '1', null, null);
INSERT INTO `users` VALUES ('45', 'Nguyễn Đức Hệ', 'dexi19758', '1', '1', '80_dexi1975@yahoo.com.vn', '$2y$10$lnEThrSbR.4e1EF/vPTrTumaGiioAtKEsEJrIaRCFL0zC8p7NTQce', null, '2018-02-22 20:20:52', '2018-02-22 20:20:52', '1', null, '3');
INSERT INTO `users` VALUES ('46', 'Cấp thẻ', 'ct', '9', '1', 'ct@viettel.com.vn', '$2y$10$9MZIrqVF0FRhkRQ86YzX7uMgeqBGxH7QDfygIARQBCM.GyAttqa/u', '1jL0Z5baYihjQXOD355LpT38SzsF92guKmytUfUNhOdrWVTYUaDZl1vuuV6A', '2018-02-23 02:08:07', '2018-02-23 02:08:07', '1', null, null);
INSERT INTO `users` VALUES ('47', 'Lãnh đạo hội', 'ldhhn', '8', '1', 'ldhoi@viettel.com.vn', '$2y$10$AKIfbqsDFUC62hY5P/d3fOo/bmapYTMLZ9Z9O87aXHfCeYeqDV3aG', 'sjy2IRprNulZwBgPEsDuMhU23YUEze2e4EBXjgTBgK2iKT2wuDYT2KFArVrd', '2018-02-23 02:46:11', '2018-02-23 02:46:37', '1', null, null);
INSERT INTO `users` VALUES ('48', 'kế toán', 'kt', '2', '1', 'kt@viettel.com.vn', '$2y$10$7MKt8lAAtcWlCn2JQeixp.R8ZZrihXyJKHCOzKk11hChuucpSg1AO', 'Ykcgkjh7j9FRzxlWBwoJWkpYdKRNoyuI0B5ZxE70IzmWpooEhgSmofMb8Pfm', '2018-02-23 08:35:30', '2018-02-23 08:35:30', '1', null, null);
INSERT INTO `users` VALUES ('49', 'Tran Xuan Toi', 'xuantoi9067', '1', '1', '50_xuantoi90@gmail.com', '$2y$10$OwgMGtE4XiK9lErqCwfvserU66DEt24437ifILUukMb4QBvbvguQi', null, '2018-02-23 09:54:50', '2018-02-23 09:54:50', '1', null, '115');
INSERT INTO `users` VALUES ('50', 'Tran Xuan Toi', 'xuantoi9083', '1', '1', '46_xuantoi90@gmail.com', '$2y$10$I14.b33/LyP577EyQ4o4VOUXNC78VSFQ2Ehrge6qqqCPXqJNDpfg2', null, '2018-02-23 09:58:05', '2018-02-23 09:58:05', '1', null, '115');
INSERT INTO `users` VALUES ('51', 'Tran Xuan Toi', 'tranha08439', '1', '1', '5_tranha084@gmail.com', '$2y$10$Ip0kbLT7wJF3vpU1L2VsteED7JNdU21zDUzbsxEIhQ6PHjTaCRQSa', null, '2018-02-23 09:59:23', '2018-02-23 09:59:23', '1', null, '115');
INSERT INTO `users` VALUES ('52', 'Chuyên viên thẩm định hồ chí minh', 'cvtdhcm', '63', '1', 'cvtdhcm@viettel.com.vn', '$2y$10$6/bEOe/ZivKzbFdSjZJfCerf1XlcTfHd7UOmKdprMqNsHbMCK98Vy', 'zoI0hAWky2lz84BEphvEPnLctZUeYHyxOFdawQ3j63utzSWpNk5l1gQ3wOYi', '2018-02-23 10:08:04', '2018-02-23 10:08:04', '3', null, null);
INSERT INTO `users` VALUES ('53', 'Lãnh đạo thẩm định hcm', 'ldtdhcm', '73', '1', 'ldtdhcm@viettel.com.vn', '$2y$10$1QRu1fYyywGEyqz2TinZueYl025/Ygg3LSY4FrJw8yOLGvLAVJBuW', 'FsvFV1gbrTmiIvq9ALm6JKpviwxkKLkXwNbg9CdwqmZjxcm7sskFLNulKnup', '2018-02-23 10:09:12', '2018-02-23 10:09:12', '3', null, null);
INSERT INTO `users` VALUES ('54', 'Chuyên viên thẩm định đà nẵng', 'cvtddn', '62', '1', 'cvtddn@viettel.com.vn', '$2y$10$Hx8c8YsjbCbcErQGB2K7t.Cba2O4DwZxV.kW1gHGl0HvwTBaatRWa', 'n4MJAR6sxMfYrHkxLWY8lzj0ftkZQpVhhVUXsBNPdeTfujhWz8muRHbIPyoh', '2018-02-23 10:10:21', '2018-02-23 10:12:02', '2', null, null);
INSERT INTO `users` VALUES ('55', 'Lãnh đạo thẩm định đà nẵng', 'ldtddn', '72', '1', 'ldtddn@viettel.com.vn', '$2y$10$GTyc/UWJeHcgUm6a5jTRte.Ta4Sz7fFMYFNm.3l4D4BwdJ4GeSWGe', 'jPyx8Ex3fB7Y7VS7uk6yXw6I73fqQ135kGel6cwjB0G4oKhbXx9JeglNX6zF', '2018-02-23 10:11:28', '2018-02-23 10:11:28', '2', null, null);
INSERT INTO `users` VALUES ('56', 'Tran Xuan Toi', 'tranha08415', '1', '1', '73_tranha084@gmail.com', '$2y$10$TPXLK4dSxtLwfik2i5drAOgDK9IoLxAGnukGSOpjTxMlPoFx.bIdu', null, '2018-02-23 10:52:45', '2018-02-23 10:52:45', '1', null, '115');
INSERT INTO `users` VALUES ('57', 'ádfasdfa', 'maint18100', '1', '1', '2_maint18@viettel.com.vn', '$2y$10$GOI0fK/Xct/xWRA2vJvWKO5c/7qpMhVa6i.neA7b7WK6UJ/SQkT02', null, '2018-02-25 23:00:28', '2018-02-25 23:00:28', '1', null, '10');
INSERT INTO `users` VALUES ('58', 'dsfasdfa', 'maint1818', '1', '1', '74_maint18@viettel.com.vn', '$2y$10$H3sVcIGAg/gk3MpUGU2WrOBEdzXraBxZiLpcflfp0rlDBNgMmyq2m', null, '2018-02-26 15:12:54', '2018-02-26 15:12:54', '1', null, '7');
INSERT INTO `users` VALUES ('59', 'Thầm định hồ sơ', 'thamdinhhn', '61', '1', 'abc@gmail.com', '$2y$10$Q4exwiERs9Brf6YMDraO9u3e3MiAVoXbvWVSKFGxsFU.2hpvnRDQW', 'nv9Om0mwyCyyDDXwQjftdj69gshb23snmqyGVHOrvubcbk0UJUT46QUK1NCO', '2018-02-26 15:36:58', '2018-02-26 15:36:58', '1', null, null);
INSERT INTO `users` VALUES ('60', 'Phê duyệt hồ sơ', 'pheduyethn', '71', '1', 'pheduyet@gmail.com', '$2y$10$NpTW738lE19sXmej0KN9S.UeOBfkz9h//2unXgSKiFq84WU8jaCFi', '5j4yflB2NtIX90TfRijC2ORZroGDuYSqBwQ0C0sPy61PkiOQPRoS3GR4rvot', '2018-02-26 15:41:06', '2018-02-26 15:41:06', '1', null, null);
INSERT INTO `users` VALUES ('61', 'Kế toán', 'ketoanhn', '2', '1', 'ketoan@gmail.com', '$2y$10$SULSmiejljDb24WbLhAMB.xyYVGliUi5mZTsVJnwJ4G8M5cCidmnm', '08ifDyfhI3DxhuRPGQbFeSvyS9QTZB5t57cc91gsI8BJSK54Ijhw5o7CRzbF', '2018-02-26 15:42:36', '2018-02-26 15:42:36', '1', null, null);
INSERT INTO `users` VALUES ('62', 'Chuyên viên cấp thẻ hội viên', 'capthehn', '9', '1', 'capthe@gmail.com', '$2y$10$vqPaupnYWPhZEzVV/vRiD.1OxkRfxoR4uRVEnpS0Sig49I8BGKSki', 'cPB5UHINzXjYikzalyFpILnxg4c4mzMM9HWT3GyHoMFg5z3Cy6eNPrNALzXd', '2018-02-26 15:43:38', '2018-02-26 15:43:38', '1', null, null);
INSERT INTO `users` VALUES ('63', 'ha3', 'tranha08440', '1', '1', '90_tranha084@gmail.com', '$2y$10$wqUyu5KopB55z2/7jNl0jO4kByesqn8UM1enckKPUsN8jxE0cbMYK', null, '2018-02-26 15:53:51', '2018-02-26 15:53:51', '1', null, '1');
INSERT INTO `users` VALUES ('64', 'ha3', 'tranha08498', '1', '1', '81_tranha084@gmail.com', '$2y$10$CFiaLzWaDsY6MZW8Lmtun.6RbOxOMXhIxKzzWvqijA3lZkJui9Wz2', null, '2018-02-26 15:55:38', '2018-02-26 15:55:38', '1', null, '1');
INSERT INTO `users` VALUES ('65', 'ádfasf', 'maint1874', '1', '1', '5_maint18@viettel.com.vn', '$2y$10$sn6twuxeLYfo1RQLCmXxXejqlemJ6g0xBobLDrIn3Vao7HPOFkb4m', null, '2018-02-26 16:00:25', '2018-02-26 16:00:25', '1', null, '11');
INSERT INTO `users` VALUES ('66', 'ha4', 'tranha08445', '1', '1', '28_tranha084@gmail.com', '$2y$10$AqtwkceO8mA6lZL/lQCy..lz3Y4OQypfVP7FhrHwW.NJCHwTFYCd.', null, '2018-02-26 16:16:14', '2018-02-26 16:16:14', '1', null, '4');
INSERT INTO `users` VALUES ('68', 'ha1', 'tranha084116', '1', '1', '27_tranha08411@gmail.com', '$2y$10$3nWnRmR6IL4BnUmyzXXFHez3a3QDKbMOSDa8MJV3K3cZK3x6K15Ky', null, '2018-02-26 16:50:00', '2018-02-26 16:50:00', '1', null, '3');
INSERT INTO `users` VALUES ('69', 'ha1', 'tranha0841121', '1', '1', '93_tranha08411@gmail.com', '$2y$10$md14hSlaDSIXkWX2tXVQwu4WUfod5OVSRqR3X5k8iPrJEQYQrzm.i', null, '2018-02-26 17:00:28', '2018-02-26 17:00:28', '1', null, '3');
INSERT INTO `users` VALUES ('70', 'test02_2602', 'abc79', '1', '1', '96_abc@gmail.com', '$2y$10$v9hVGf8gGU54LAzlxBVb0eLC.vTWQYDtjoXuWDr8RghSIY5jI7IHu', null, '2018-02-26 17:08:19', '2018-02-26 17:08:19', '1', null, '18');
INSERT INTO `users` VALUES ('71', 'sdfasf', 'maint1856', '1', '1', '13_maint18@viettel.com.vn', '$2y$10$75ZouqbFLBOdrMC3ZpTANe2opq2deVE2NojSqtiPwnzIw9208XVcG', null, '2018-02-27 17:50:31', '2018-02-27 17:50:31', '1', null, '9');
INSERT INTO `users` VALUES ('72', 'test04_2701', 'abc19', '1', '1', '11_abc@gmail.com', '$2y$10$PZdOsDUrU/px0Buh3rP9OePiEjn7eVutGVYRwW1iqPvsi4oUY3WIu', null, '2018-03-01 10:06:07', '2018-03-01 10:06:07', '1', null, '35');
INSERT INTO `users` VALUES ('73', 'test01_2802', 'abc40', '1', '1', '89_abc@gmail.com', '$2y$10$XouVt.a8cIRe9Cdgw/X7UORFiRMBomeDMBzuCIAJXPXenmdmxxCg2', null, '2018-03-01 10:07:03', '2018-03-01 10:07:03', '1', null, '13');
INSERT INTO `users` VALUES ('74', 'ha2', 'ngocpbw110283', '1', '1', '29_ngocpbw1102@gmail.com', '$2y$10$oIVJh1sIfxs8JjvkAocfYOBwA6PCJQjbAo1hifF/FMW5HFtGhfU5O', null, '2018-03-01 11:03:12', '2018-03-01 11:03:12', '1', null, '2');
INSERT INTO `users` VALUES ('75', 'test03_2702', 'abc87', '1', '1', '21_abc@gmail.com', '$2y$10$REijmsj/26qXyJPoaYUrDuLHpmoAftidLZZXK3IZEzIFja/FPHZqW', null, '2018-03-01 11:28:25', '2018-03-01 11:28:25', '1', null, '34');
INSERT INTO `users` VALUES ('76', 'test02_2702', 'abc14', '1', '1', '59_abc@gmail.com', '$2y$10$Ud/Wqcp/ww3rSm0WpwX7COKCgHI8Y9rInct.bedRA3KSLuLFMTzji', null, '2018-03-01 11:31:12', '2018-03-01 11:31:12', '1', null, '22');
INSERT INTO `users` VALUES ('77', 'test03_2702', 'abc45', '1', '1', '78_abc@gmail.com', '$2y$10$JqXecDWXA3rFSecWH2MKI.Og2rl/jUdZRFoMAuK2MkB65bgRf3Rru', null, '2018-03-01 14:59:30', '2018-03-01 14:59:30', '1', null, '23');
INSERT INTO `users` VALUES ('78', 'test02_0103', 'thuyenht90', '1', '1', '41_thuyenht@viettel.com.vn', '$2y$10$o9rfBwLSpF.x7MT75ASXaemkK.A7iAs4./F8D8D1epbxEmh3UKs06', 'J6N3XWD3WTeaHWftefHoyXJ6e5UHDUy34waExcFJe0KcHFg55FRSiSn5tNd2', '2018-03-01 14:59:41', '2018-03-01 14:59:41', '1', null, '49');
INSERT INTO `users` VALUES ('79', 'Tes02_0103', 'thuyenht74', '1', '1', '2_thuyenht@viettel.com.vn', '$2y$10$9ecX72bQv8OvhgVg7Rgll.I3EErjSQiNIS8YyACSH8TT9vMrSltJO', 'GmWsbSVM9WFGS0eb8H2ywLXFC5JMnVLeMesoWOyWICD3hYOqzZI5UNNP2OyM', '2018-03-01 14:59:44', '2018-03-01 14:59:44', '1', null, '50');
INSERT INTO `users` VALUES ('80', 'Lãnh đạo hội', 'lanhdaohoi', '8', '1', 'lanhdaohoi@gmail.com', '$2y$10$b4d5FbxqYMTIsKTZF0VXCer9YepyjSNQpZy8J0cJ.XIMFKaCuLpGW', 'c1V3SBwRFGT5tAf9owhdWYzKBg0t1MkWXx4QLffIzPA1JbN6HkjR2zZKH4Eh', '2018-03-02 10:35:17', '2018-03-02 10:35:17', '1', null, null);
INSERT INTO `users` VALUES ('81', 'test bổ sung thông tin hội viên chính thức', 'thuyenht11', '1', '1', '53_thuyenht@viettel.com.vn', '$2y$10$i.euqsjooktdn8pngOQAN.b/SrDJqYykFwf6BUSYpMJ5nml9kv4NK', 'L83i9Kz2rZ7bdQr8bjF0NpA51w9gaDhEfimcRAdJZ32ANp9IyPXQhO40tyA0', '2018-03-02 10:56:43', '2018-03-02 10:56:43', '1', null, '52');
INSERT INTO `users` VALUES ('82', 'Nguyễn Thị Mai dữ liệu 24022018_2', 'maint1824', '1', '1', '15_maint18@viettel.com.vn', '$2y$10$dzL39agD3W6QZKSjbOH1xuaYMeJzy11gOTCBOlHeDtUE5iR.xbATW', '0i6sI7Nt4TKch86cliqfBmvSOxfXBQ8K5CoGucQHFRKNJt2jp0x9LgWAX7DB', '2018-03-02 11:42:08', '2018-03-10 09:06:51', '3', null, '6');
INSERT INTO `users` VALUES ('83', 'Nguyễn Thị Mai dữ liệu 27022018_1', 'maint1882', '1', '1', '46_maint18@viettel.com.vn', '$2y$10$vBUl4EZpRZGkMZxNJUYuIe/T.4v7jiouckcrR0XXtkj9lQRBKC7hq', null, '2018-03-02 14:32:44', '2018-03-02 14:32:44', '1', null, '25');
INSERT INTO `users` VALUES ('84', 'Dương Văn Phượng', 'phuongdv16', '1', '1', '12_phuongdv@viettel.com.vn', '$2y$10$G88Dax2JmFVT3tfxYKyFgu2x.vefiU4GY9L2O1.TzMYJc7Tkdak4e', null, '2018-03-02 14:40:58', '2018-03-02 14:40:58', '1', null, '46');
INSERT INTO `users` VALUES ('85', 'test02_0103', 'thuyenht65', '1', '1', '22_thuyenht@viettel.com.vn', '$2y$10$rNTrazGyBkNQXq0ohWaIdeDu0rcEYLs1oGMivO5WvCq/KaKD9w7Ry', 'xo04FaCPBPIOrk5P6ui8gI9L44kvvXlqNhjki2Sw3cnoz3EFf4GDaO6Ik44k', '2018-03-02 14:42:32', '2018-03-02 14:42:32', '1', null, '49');
INSERT INTO `users` VALUES ('86', 'test03_0103', 'thuyenht86', '1', '1', '16_thuyenht@viettel.com.vn', '$2y$10$.0UB3B5BFPMR8XL7eMXGIO39BtRucaG1Q7lLWyXZz78648JcDjIH.', 'B7MGCwQj0WAHMxmnvxXfCFP9Q15nZFwqd0TKANcm3Bb8j7CegVf8qGBKAja2', '2018-03-02 14:44:44', '2018-03-02 14:44:44', '1', null, '51');
INSERT INTO `users` VALUES ('87', 'test bổ sung 01', 'thuyenht27', '1', '1', '70_thuyenht@viettel.com.vn', '$2y$10$Jj4oMqTuoCrt3u397nX1XOi0tna6iOQin1YdPiHFdgT9wCpHlOpQO', 'sTh5TQXURDiJJdPC63BUlSwGdNULSJ4QMPX3COEYFbSqq13RksiyClgmIzlt', '2018-03-02 14:47:53', '2018-03-02 14:47:53', '1', null, '53');
INSERT INTO `users` VALUES ('88', 'test bổ sung 02', 'thuyenht24', '1', '1', '8_thuyenht@viettel.com.vn', '$2y$10$ymwMm0KS3Ksr0IOpvBTNz.ka3fsDSRdSq4jtT62dKybEO6eVFtGk2', null, '2018-03-02 16:14:09', '2018-03-02 16:14:09', '1', null, '54');
INSERT INTO `users` VALUES ('89', 'testcapma01', 'thuyenht58', '1', '1', '68_thuyenht@viettel.com.vn', '$2y$10$UXFWbqV.SFrAfZfE8sTnde4rCCPVjysCdjR.sLbVDP/FCmMUWX/qC', null, '2018-03-02 16:36:37', '2018-03-02 16:36:37', '1', null, '56');
INSERT INTO `users` VALUES ('90', 'testcapma02', 'thuyenht71', '1', '1', '65_thuyenht@viettel.com.vn', '$2y$10$xWd4ehczrKrHzDJsoTDXJev9ZbyHH9gy7vNgTNfHjRy5fncERRDOy', null, '2018-03-02 16:45:56', '2018-03-02 16:45:56', '1', null, '57');
INSERT INTO `users` VALUES ('91', 'ldhdl', 'ldhdl', '8', '1', 'ldhdl@gmail.com', '$2y$10$e/tZ8yT.o5cNVV/g0um9ReHnV6.UBL/3XRHmlON/pU.QfgkvUiNwC', null, '2018-03-06 09:07:45', '2018-03-06 09:07:45', '1', null, null);
INSERT INTO `users` VALUES ('92', 'Lãnh đạo hội', 'ldhoi', '8', '1', 'lanhdaohoi@viettel.com.vn', '$2y$10$zYtqwGvZlCnwtKWXm8Ip0.NOJFsYwbB.sGkjpGDThFdUdpLmYRoVW', '7A3LF3JhrkJH3ZU6Ii4Hu6dLdWJwkjzwR7sd05tMHu2xW9Kl3Q2WqNh0fKIY', '2018-03-06 09:33:19', '2018-03-06 09:33:19', '1', null, null);
INSERT INTO `users` VALUES ('93', 'Nguyễn Thị Hương dữ liệu 27022018_1', 'ngocpbw110276', '1', '1', '89_ngocpbw1102@gmail.com', '$2y$10$9vx1Cr2V2wbNykT45mesu.4N6JuHNJP7/wysA8cXdQmejb091HW/m', null, '2018-03-06 11:29:23', '2018-03-06 11:29:23', '1', null, '31');
INSERT INTO `users` VALUES ('94', 'testbosungtt_01', 'thuyenht80', '1', '1', '65_thuyenht@viettel.com.vn', '$2y$10$NavI4PjbcJUDZXwRZpm59umNGe3t0MP4uxO5vM/ic8FodmGyVataG', 'NpXm7L7fwFPEIWb3jzmWyeXDK1TS29bCPSQNIUnRa9WQ7s7Pv4bjoDsZuQFs', '2018-03-06 11:40:14', '2018-03-06 11:40:14', '3', null, '58');
INSERT INTO `users` VALUES ('95', 'bổ sung 07', 'thuyenht82', '1', '1', '62_thuyenht@viettel.com.vn', '$2y$10$NrZjP5MNTBsvKv4mUr5GiuRSxa56X8U00xXrO/s742pJaNi6DyiyC', 'YnVkjh5daIlGzZlPN2XDw3V9uvtXUxZcH9zmH4tt1t5zZkamgDYjkmJE6y4o', '2018-03-06 11:41:19', '2018-03-06 11:41:19', '1', null, '64');
INSERT INTO `users` VALUES ('96', 'Nguyễn Thị Mai 9/3/2018', 'maint1882', '1', '1', '74_maint18@viettel.com.vn', '$2y$10$rcuNeDrSIcqTJ0zYEC3Ar.FEWGAkkHPs7vKLc8rWfc11lnNabW4Ym', null, '2018-03-10 08:32:39', '2018-03-10 09:05:44', '2', null, '66');
INSERT INTO `users` VALUES ('97', 'Tập Cận Bình', 'demo69', '1', '1', '79_demo@gmail.com', '$2y$10$qXhM4ln90PH6Cfeo2ZOfEeNUskJv1PMvxtX8i5x0qHpTvbC0mYdBu', null, '2018-03-12 14:37:10', '2018-03-12 14:37:10', '2', null, '29');
INSERT INTO `users` VALUES ('98', 'Cao Thi Thao', 'demo15', '1', '1', '1_demo@gmail.com', '$2y$10$FCj32H2CxdEK08H1fmDkq.5ufRamHbt5XodGZWyv4LmQrjZddThY2', null, '2018-03-12 14:37:19', '2018-03-12 14:37:19', '1', null, '33');
INSERT INTO `users` VALUES ('99', 'test bố sung 02', 'demo17', '1', '1', '96_demo@gmail.com', '$2y$10$nqWC5IQnMj50AzQ4fOMZKOtMi4.hEAZT5ZqvhIaCX.uFcSbO6HkuW', null, '2018-03-12 14:44:26', '2018-03-12 14:44:26', '1', null, '59');
INSERT INTO `users` VALUES ('100', 'test bố sung 02', 'demo46', '1', '1', '69_demo@gmail.com', '$2y$10$HXpjowyovt95VYO0SQfb9e0X0RR9/BBANM2lYDFzJar51nsmPwPxi', null, '2018-03-12 14:44:26', '2018-03-12 14:44:26', '1', null, '59');
INSERT INTO `users` VALUES ('101', 'Nguyễn Thị Mai 12032018_outbound', 'maint1899', '1', '1', '100_maint18@viettel.com', '$2y$10$SegU/GnSZWcJC90pNA.GXu/DiAuYAQSe1keQ5ejwj5NZqWASCatgG', null, '2018-03-12 16:02:23', '2018-03-12 16:02:23', '1', null, '79');
INSERT INTO `users` VALUES ('102', 'Nguyễn Thị Mai 12032018_nội địa', 'maint1810', '1', '1', '81_maint18@viettel.com', '$2y$10$byFZyXvqJbUc3UP7nFn0we8L/Ft6oOf6xWqmSX3uiVyFtjP3uu9jC', null, '2018-03-12 16:02:28', '2018-03-12 16:02:28', '1', null, '80');
INSERT INTO `users` VALUES ('103', 'Nguyễn Thị Mai 13032018_tai diem', 'maint1858', '1', '1', '13_maint18@viettel.com.vn', '$2y$10$QlQnFhN4EzWssq39jdLDd.zWulQqVQrYuNMZE9sOxoxGbmwnNCmzG', 'tNAtOmjJ7gweVfks69OfsPl5cimWlDnq7QM80RgWg2tpLrO4EJbH0b4BqQa6', '2018-03-13 14:37:31', '2018-03-13 14:37:31', '1', null, '7');
INSERT INTO `users` VALUES ('104', 'Nguyễn Thị Mai 13032018_ inbound', 'maint1861', '1', '1', '8_maint18@viettel.com.vn', '$2y$10$FH1NIk5yKSX1aLV.n4CdJulEwbYUFLwoiSYRG.yNOlvPchtiPERn.', null, '2018-03-13 15:06:16', '2018-03-13 15:06:16', '1', null, '1');
INSERT INTO `users` VALUES ('105', 'abc2', 'thuyenht42', '1', '1', '17_thuyenht@viettel.com.vn', '$2y$10$VxYXx7qb1V3ySX0fzAUAjO0HiOZH8GuvCdwyWZ1vR5WW9EDd/WeyW', null, '2018-03-13 15:06:47', '2018-03-13 15:06:47', '3', null, '4');
INSERT INTO `users` VALUES ('106', 'Nguyễn Thị Mai 12032018_nội địa', 'maint1838', '1', '1', '27_maint18@viettel.com.vn', '$2y$10$1/PhWQIvJMvt1QAnQ/732.jzGn2oiAVKDkYlt0jFo4Pbvzpcvu7.q', null, '2018-03-13 15:06:51', '2018-03-13 15:06:51', '1', null, '5');
INSERT INTO `users` VALUES ('107', 'abc3', 'thuyenht62', '1', '1', '67_thuyenht@viettel.com.vn', '$2y$10$5e2XZLGH2Sse3DGs.B96DuY207L6hc8iCDpo6AaJXvdlq9IyOXNLa', null, '2018-03-13 15:06:59', '2018-03-13 15:06:59', '3', null, '6');
INSERT INTO `users` VALUES ('108', 'Nguyễn Thị mai 13032018_test ho so', 'maint1869', '1', '1', '57_maint18@viettel.com.vn', '$2y$10$LX9eoelbZ4VnvMyzAejudOJOGaY0ek7pPUW7d1vxBWF/mwQLetmR2', null, '2018-03-13 15:07:05', '2018-03-13 15:07:05', '1', null, '8');
INSERT INTO `users` VALUES ('109', 'abc4', 'thuyenht91', '1', '1', '85_thuyenht@viettel.com.vn', '$2y$10$YUpBkiDJjDiMUQWX7Yllp..9rgrqjlViYWBHO6si6naBQC2qeBRYe', null, '2018-03-13 15:07:08', '2018-03-13 15:07:08', '1', null, '9');
INSERT INTO `users` VALUES ('110', 'abc5', 'thuyenht52', '1', '1', '81_thuyenht@viettel.com.vn', '$2y$10$8biLn8la8yQmmYsNSUYPPeGowBXFIXly7G07idcvuFLaq5FMPbxVy', null, '2018-03-13 15:07:12', '2018-03-13 15:07:12', '1', null, '10');
INSERT INTO `users` VALUES ('111', 'abc6', 'thuyenht1', '1', '1', '7_thuyenht@viettel.com.vn', '$2y$10$anJekrMadTf/YvLbvCPn2.jGB3N49DKD0trf2HqANqilZD5hVgq7O', null, '2018-03-13 15:07:15', '2018-03-13 15:07:15', '1', null, '12');
INSERT INTO `users` VALUES ('112', 'abc7', 'thuyenht3', '1', '1', '26_thuyenht@viettel.com.vn', '$2y$10$ShbUwuCjcZtmLdDi4RLdN.gQau.k5d1mQ9w59llGTzcZ3zLNp1Q.u', 'YvEWSrFsNPRHQcfF4VtAvazJjs3SSyEKrVQnOIE2j53uTWvjZiZzAA6FfPlt', '2018-03-13 15:07:19', '2018-03-13 15:07:19', '1', null, '13');
INSERT INTO `users` VALUES ('113', 'abc8', 'thuyenht88', '1', '1', '60_thuyenht@viettel.com.vn', '$2y$10$9Lddcs.VqfxuJ1HoxLLCQOlDLCyJ1cXlZ069saxgCaPfb0GSeEfmW', null, '2018-03-13 22:16:18', '2018-03-13 22:16:18', '1', null, '14');
INSERT INTO `users` VALUES ('114', 'Viettel Test', 'ngandt589', '1', '1', '5_ngandt5@viettel.com.vn', '$2y$10$Aq3w8AAwV33/dJJ0q3vVx.T4FRphPt7hbZMWMAVhvef/7WXkCec..', null, '2018-03-14 10:39:06', '2018-03-14 10:39:06', '1', null, '17');
INSERT INTO `users` VALUES ('115', 'Viettel 1', 'ngandt532', '1', '1', '21_ngandt5@viettel.com.vn', '$2y$10$ZflaMzfSV3HrESaq/HqUz.1XP81yT33BEv.kS4Qrp7RLuZ0Oh9xvS', null, '2018-03-14 15:43:41', '2018-03-14 15:43:41', '1', null, '34');
INSERT INTO `users` VALUES ('116', 'Viettel 2', '123456781', '1', '1', '74_ngandt5@viettel.com.vn', '$2y$10$bbFRXGncF6WAihYVvAhGquXo0JUiizbsUMYryH0JhdqRLSajOZS2G', null, '2018-03-15 15:09:19', '2018-03-15 15:09:19', '1', null, '35');
INSERT INTO `users` VALUES ('117', 'thuyên_test1', '888888888', '1', '1', '28_thuyenht@viettel.com.vn', '$2y$10$U3ZaGdecdDVl8s/AYXg9QOSllmMp54VzAwVGILLWn0ifnbQsz8/ge', null, '2018-03-15 16:47:17', '2018-03-15 16:47:17', '1', null, '57');
INSERT INTO `users` VALUES ('118', 'Viettel 2', '123456782', '1', '1', '28_ngandt5@viettel.com.vn', '$2y$10$XCABn56j43OTurP5xgbc4.huFF9B7DQU5qQfZBD9t2tp8RtgcC/g.', null, '2018-03-15 17:13:18', '2018-03-15 17:13:18', '1', null, '36');
INSERT INTO `users` VALUES ('119', 'Nguyễn Thị Mai 14032018_tại điểm', '154154154', '1', '1', '46_maint18@viettel.com.vn', '$2y$10$M6UqDwCNNkDEn0FseOOCvOYLodNIR8iPBuFSQEbkC1Sw00dQILO5y', null, '2018-03-15 17:13:21', '2018-03-15 17:13:21', '1', null, '33');
INSERT INTO `users` VALUES ('120', 'test5', '444444445', '1', '1', '82_thuyenht@viettel.com.vn', '$2y$10$t3hqqYJ8Mlyh3ZNYAKC2CeEl.KW0Hg1o3RleRuFigmpYqxLpP15/e', null, '2018-03-15 17:13:24', '2018-03-15 17:13:24', '1', null, '28');
INSERT INTO `users` VALUES ('121', 'test4', '444444446', '1', '1', '75_thuyenht@viettel.com.vn', '$2y$10$mLr277uWXo5CmQWlHL/tdeL8sCNkONKlZLhIwtwCAEDfzEBVtAipe', null, '2018-03-15 17:13:27', '2018-03-15 17:13:27', '1', null, '27');
INSERT INTO `users` VALUES ('122', 'test2', '444444448', '1', '1', '74_thuyenht@viettel.com.vn', '$2y$10$Fu2O6xbbX.hswHYJ.RGV/OOXKRWmo9eGsm5D/YUXmfaEp2DHtW7IC', null, '2018-03-15 17:13:36', '2018-03-15 17:13:36', '1', null, '24');
INSERT INTO `users` VALUES ('123', 'test1', '444444449', '1', '1', '88_thuyenht@viettel.com.vn', '$2y$10$VCtBPTu97sg9HyVGzJvHP.EcgbNqo664QEgKF10wbL0C2jDGbKdnm', null, '2018-03-15 17:13:39', '2018-03-15 17:13:39', '1', null, '23');
INSERT INTO `users` VALUES ('124', 'Nguyễn Thị Mai 13032018_outbound', '136136136', '1', '1', '78_maint18@viettel.com.vn', '$2y$10$jrZR6G3.ppcLSJdfYi6Bye3nSG2CiAZSTQfaRxqFm5Uc4ZZs4QT6W', null, '2018-03-15 17:13:42', '2018-03-15 17:13:42', '1', null, '22');
INSERT INTO `users` VALUES ('125', '123', '333333339', '1', '1', '82_thuyenht@viettel.com.vn', '$2y$10$nO13ohMspEO38z8vbqx4ceRsTSVOW2J8ee8wvHuBsrt2y5WEERMo6', null, '2018-03-15 17:13:45', '2018-03-15 17:13:45', '1', null, '18');
INSERT INTO `users` VALUES ('126', 'abcd', '101111111', '1', '1', '81_thuyenht@viettel.com.vn', '$2y$10$nrQDdbm0rxfqVo3gnFOIb.OnAarTrcNrIb/v.2nJf6inYQav12v7u', 'C9K5d8BAshtvz2cI4hfsTaM2KMIX62kD51I3f40bKieeB1rEB1nGtV31Fukp', '2018-03-15 17:13:50', '2018-03-15 17:13:50', '1', null, '15');
INSERT INTO `users` VALUES ('127', 'Nguyễn Thị Mai 13032018_ inbound', '156156156', '1', '1', '85_maint18@viettel.com.vn', '$2y$10$02rgHba.kj5KEDJY2WBgye5K6H5hiqKNRjlTAnjLkkxdRbsyBhDXS', null, '2018-03-15 17:13:55', '2018-03-15 17:13:55', '1', null, '1');
INSERT INTO `users` VALUES ('128', 'Viettel 9', '232332243', '1', '1', '63_ngandt5@viettel.com.vn', '$2y$10$Ze29EHWlXCWVF2xxbzyVD.dkxiqZwv.xBElvaiQOPDZ34ry1.HUQe', 'bTYIIVUlWUwNSBFK138l8NA4xnB4c4cA32agawMDB3fiSbn5QvBDiL7t8p3b', '2018-03-16 10:13:36', '2018-03-16 10:13:36', '1', null, '59');
INSERT INTO `users` VALUES ('129', 'test7', '444444443', '1', '1', '97_thuyenht@viettel.com.vn', '$2y$10$d9PSm.r5n8ygg.ZBLM80iuDM7A7cCBDMEK2I4GD/1UBj114V088gO', 'le61JdK0zLp1tiJcBUdVKeXvXmOdh5FfbaOBfaP6S1F6bBw7TEe1V7CRrw30', '2018-03-16 15:04:54', '2018-03-16 15:04:54', '1', null, '31');
INSERT INTO `users` VALUES ('130', 'viettel 13_BSHS thẩm định', '141415141', '1', '1', '66_ngandt5@viettel.com.vn', '$2y$10$k8FTpKig3Ou9/UN3QO.JHOGzXNXA7BUx3d5l68rYnL9s1fPdE9Q4a', 'VguCh7icRetSl9JmDg9f7tkj3WqRbgeRUJIgdQvKVaAPchlKO7B2oHF9R1ZB', '2018-03-20 16:03:47', '2018-03-20 16:03:47', '1', null, '83');
INSERT INTO `users` VALUES ('131', 'Nguyễn Thị Mai 13032018_tai diem', '159159159', '1', '1', '78_maint18@viettel.com.vn', '$2y$10$jcPoxc1DfkM/v6PfGRYmj.LmfceGl.Wp94LBeHtP2dVwwBKZwYwG.', null, '2018-03-21 10:36:11', '2018-03-21 10:36:11', '1', null, '7');
INSERT INTO `users` VALUES ('132', 'Nguyễn Thị Mai 12032018_nội địa', '158158158', '1', '1', '39_maint18@viettel.com.vn', '$2y$10$5eNFUpjXvnlSRlAIiUM5y.p7xyEl.aiBHLAR1/T3hcEti8P5Gdd2S', null, '2018-03-21 10:51:05', '2018-03-21 10:51:05', '1', null, '5');
INSERT INTO `users` VALUES ('133', 'Viettel 14_outbound', '868686868', '1', '1', '55_ngandt5@viettel.com.vn', '$2y$10$XoU2oSyjmdLjRwXlcAOxaeLcu1FjOS.tuxvlhqN4ONdGpsFsVA74O', null, '2018-03-23 14:45:18', '2018-03-23 14:45:18', '1', null, '93');
INSERT INTO `users` VALUES ('134', 'test6', '444444444', '1', '1', '11_thuyenht@viettel.com.vn', '$2y$10$UVPu4nECdXIfj4XQ0l4F8eXOREISZfZvSBs7L83KnDcix/NDIh9XG', null, '2018-03-24 23:41:26', '2018-03-24 23:41:26', '1', null, '29');
INSERT INTO `users` VALUES ('135', 'fdvfd', '435345344', '1', '1', '71_ngandt5@viettel.com.vn', '$2y$10$3gdwYn/s3oAFzejxEKekpuvkirdjMQJh6iAo/tEM8RSD3bOYRQv8.', null, '2018-03-24 23:41:30', '2018-03-24 23:41:30', '1', null, '37');
INSERT INTO `users` VALUES ('136', 'test bổ sung 3', '999999997', '1', '1', '79_thuyenht@viettel.com.vn', '$2y$10$u.49D6LTcGLot1lHfIAEXevZP.mcKTHYXps61IrILWQvmwwzXrh4G', null, '2018-03-26 12:07:29', '2018-03-26 12:07:29', '1', null, '46');
INSERT INTO `users` VALUES ('137', 'đăng ký lại', '789547852', '1', '1', '39_ngandt5@viettel.com.vn', '$2y$10$eFq/OAe.QMaCbaHslr/jf.1cFwIA3xej4mdSGyu2vY23ju1WLZHRC', null, '2018-03-28 16:04:44', '2018-03-28 16:04:44', '1', null, '132');
INSERT INTO `users` VALUES ('138', 'test3', '444444447', '1', '1', '46_thuyenht@viettel.com.vn', '$2y$10$DjZQ73I/MwE8Pv9Ik6mUbe.SZAd.aRPLR7h.Q3ThLDgOt/XcgwHm.', null, '2018-03-28 16:14:02', '2018-03-28 16:14:02', '1', null, '25');
INSERT INTO `users` VALUES ('139', 'Nguyễn Thị Mai 15032018 tại điểm', '169169169', '1', '1', '72_maint18@viettel.com.vn', '$2y$10$72Q4GmwRfgyOgyHUFFnHTu.466T4oD.diYXKe3fBo2M9.cZRqVgT2', null, '2018-03-28 16:14:51', '2018-03-28 16:14:51', '1', null, '49');
INSERT INTO `users` VALUES ('140', 'in hội viên', '030303030', '1', '1', '6_ngandt5@viettel.com.vn', '$2y$10$.yhD0xMSdXduDnlp1Jqus.oqVmC0wJcXEOkn1IvuLg5hnYQTfh40S', null, '2018-03-28 16:30:36', '2018-03-28 16:30:36', '1', null, '134');
INSERT INTO `users` VALUES ('141', 'abc5', '111111113', '1', '1', '42_thuyenht@viettel.com.vn', '$2y$10$7.LtNRIsGpd/bPS1MuCX9Omh.4Hgn1DR3t1dIHdkvEu9cSjvc9/aq', null, '2018-03-29 15:34:25', '2018-03-29 15:34:25', '1', null, '10');
INSERT INTO `users` VALUES ('142', 'Nguyễn Thị Mai 14032018 nội địa', '178178178', '1', '1', '69_maint18@viettel.com.vn', '$2y$10$dBgjMDD0tPUSxBpQcZwu9erM5qQK6yvQW.879fx0g/fnQZMxZ4w1i', null, '2018-03-29 15:35:09', '2018-03-29 15:35:09', '1', null, '32');
INSERT INTO `users` VALUES ('143', 'Nguyễn Thị Mai 15032018', '187187187', '1', '1', '80_maint18@viettel.com.vn', '$2y$10$MiMnqm1.M7BM3jYbC4ks/O63jw7NxQqS2wwgcB03CUPj33CC6a0rO', null, '2018-03-29 15:35:21', '2018-03-29 15:35:21', '1', null, '45');
INSERT INTO `users` VALUES ('144', 'test bổ sung 2', '999999998', '1', '1', '95_tranha084@gmail.com', '$2y$10$mY.NklQqgnDlKc/hSxE5zu6cU6Vt7t2QTuwKjAwL90khvK3bhJyXu', null, '2018-03-29 16:47:43', '2018-03-29 16:47:43', '1', null, '44');
INSERT INTO `users` VALUES ('145', 'aaaaaaaaaaa', '555555559', '1', '1', '39_thuyenht@viettel.com.vn', '$2y$10$6.6GN/YecMWZg93qY1s6L.E7azLJy5ed0WpBXeF8yJJGQNNBqGzV2', null, '2018-03-29 16:57:38', '2018-03-29 16:57:38', '1', null, '38');
INSERT INTO `users` VALUES ('146', 'viettel 5', '123122331', '1', '1', '6_xuantoi90@gmail.com', '$2y$10$J1NjvMY19lrrnvt/7si25eq7DVFfGjfgjyHE4dD.gIMkkW0x.bvFq', null, '2018-03-29 16:59:45', '2018-03-29 16:59:45', '1', null, '41');
INSERT INTO `users` VALUES ('147', 'Viettel 15_inBound', '878787878', '1', '1', '15_ngandt5@viettel.com.vn', '$2y$10$vpPzwNNjIpqfKO/pH0f6yOJSObtxCeR32CaclBEnf7ojfA6YRrVqa', null, '2018-03-29 17:00:11', '2018-03-29 17:00:11', '1', null, '94');
INSERT INTO `users` VALUES ('148', 'Nguyễn Thị Mai 13032018_tai diem', '159159159', '1', '1', '15_maint18@viettel.com.vn', '$2y$10$TTshRx.SEHB5jhY4dM0kJO5PfdbJ3b92vKoHU9jREjTfiDhbaNeCm', null, '2018-03-29 17:01:11', '2018-03-29 17:01:11', '1', null, '7');
INSERT INTO `users` VALUES ('149', 'viettel 11', '131313131', '1', '1', '44_ngandt5@viettel.com.vn', '$2y$10$G1gpk4jdzH2HyUG/7qoHT.PMEJrmThKzXRy0WaUiGsHz6dBaRbF16', null, '2018-03-29 17:11:33', '2018-03-29 17:11:33', '1', null, '81');
INSERT INTO `users` VALUES ('150', 'Viettel 17_tại điểm', '868686867', '1', '1', '30_ngandt5@viettel.com.vn', '$2y$10$WT7JUnjSNX37/ZsTiKhhbetFq2VpCxw4.LEDkq8zuoR3WMDKDMmBC', null, '2018-03-29 17:12:18', '2018-03-29 17:12:18', '1', null, '96');
INSERT INTO `users` VALUES ('151', 'In hồ sơ_outbound', '134346234', '1', '1', '41_ngandt5@viettel.com.vn', '$2y$10$.F9jxlkexsaybqKoW43useSg5SiwoDmrWgfnW0xLxVpthZZd0O75q', null, '2018-03-29 17:22:47', '2018-03-29 17:22:47', '1', null, '99');
INSERT INTO `users` VALUES ('152', 'check_tại điểm', '898989896', '1', '1', '1_ngandt5@viettel.com.vn', '$2y$10$smBY1tBShApeXIwCw6U1W.WYsOTGjKdoioIZJZtjb2civD.p2Mm2K', null, '2018-03-29 17:33:26', '2018-03-29 17:33:26', '1', null, '103');
INSERT INTO `users` VALUES ('153', 'check_nội địa', '898989897', '1', '1', '56_ngandt5@viettel.com.vn', '$2y$10$xb.Ym/ZM/gVJGdFO3slOruM2QCPD58AE4bTUgSJGzjD9M7uk3g.Qm', null, '2018-03-30 10:31:16', '2018-03-30 10:31:16', '1', null, '102');
INSERT INTO `users` VALUES ('154', 'check đăng ký lại với mã HDV đã bị xóa', '369369369', '1', '1', '100_ngandt5@viettel.com.vn', '$2y$10$5X1bnUNI7bbZzqTrj0z7wehMVYvLaekbewD6YIqeYdWCfm83dBmZu', null, '2018-03-30 10:49:54', '2018-03-30 10:49:54', '1', null, '115');
INSERT INTO `users` VALUES ('155', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '010101010', '1', '1', '79_ngandt5@viettel.com.vn', '$2y$10$wnKs.40DRd3rkekOqKn5uuKDxTW3fxmgq2/Az.zOBGYhghOnc.7ri', null, '2018-03-30 11:56:10', '2018-03-30 11:56:10', '1', null, '140');
INSERT INTO `users` VALUES ('156', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#', '258258258', '1', '1', '1_ngandt5@viettel.com.vn', '$2y$10$ECr9EE.yWJ0jnzbEGSb3i.oZSDv5QNClT18/1BVhEZeEqUDjfyDg2', null, '2018-03-30 11:58:01', '2018-03-30 11:58:01', '1', null, '116');
INSERT INTO `users` VALUES ('157', 'check outbound', '159874632', '1', '1', '100_ngandt5@viettel.com.vn', '$2y$10$K.0rW1zOKkXUy5OK8QpG.uV0isYdF2nmjCwgLapi0BXX6/S2ReJrC', null, '2018-03-30 13:37:11', '2018-03-30 13:37:11', '1', null, '123');
INSERT INTO `users` VALUES ('158', 'Nguyễn Thị Mai 14032018_outbound', '126126126', '1', '1', '87_maint18@viettel.com.vn', '$2y$10$YqDo.CXlJcw1DPmFGaRXK.MhuFNiOuIRl59tylL7tbaaEfOvk9ZGe', null, '2018-03-30 13:38:07', '2018-03-30 13:38:07', '1', null, '30');
INSERT INTO `users` VALUES ('159', 'check lặp dữ liệu_update CLB', '161616616', '1', '1', '43_ngandt5@viettel.com.vn', '$2y$10$yf7o1g2wmhrHDKdBThKVzOEltiXE5HpdudQ8H0UVKqLPpNkfFnHBW', null, '2018-03-30 13:38:17', '2018-03-30 13:38:17', '1', null, '87');
INSERT INTO `users` VALUES ('160', 'bổ sung đăng ký', '222322222', '1', '1', '1_thuyenht@viettel.com.vn', '$2y$10$HGKYerA3nPS5zBhUBWjamOlZERRzhoKt5lFoGd27ju5XNZHkPH1FC', null, '2018-03-30 13:39:39', '2018-03-30 13:39:39', '1', null, '80');
INSERT INTO `users` VALUES ('161', 'abc4', '111111114', '1', '1', '98_thuyenht@viettel.com.vn', '$2y$10$dQwj63v7W2U34xDqN3gy/OeNDVDh5UfXHy.oyf9A7ebRA25xOpow2', null, '2018-03-30 13:48:46', '2018-03-30 13:48:46', '1', null, '9');
INSERT INTO `users` VALUES ('162', 'check_inbound', '898989898', '1', '1', '35_ngandt5@viettel.com.vn', '$2y$10$RG12wtvAsddmDDQ7IGgmKeVQJkJLLoPcgWk.wXL/gd4oINbcPH9jO', null, '2018-03-30 13:48:58', '2018-03-30 13:48:58', '1', null, '100');
INSERT INTO `users` VALUES ('163', 'outbound', '897978978', '1', '1', '87_ngandt5@viettel.com.vn', '$2y$10$nPyRCyWvi1B5b4psje./UeMea9EEYy58ejhNy76HiTLs8DVMZJsJO', null, '2018-03-30 13:51:10', '2018-03-30 13:51:10', '1', null, '113');
INSERT INTO `users` VALUES ('164', 'check xóa hồ sơ', '000000003', '1', '1', '80_ngandt5@viettel.com.vn', '$2y$10$RS9szO5jW0cJu1mlhCExpOVNqTNc4CKnGyF6U2jDDNLfh5gCAnO0a', null, '2018-03-30 13:51:16', '2018-03-30 13:51:16', '1', null, '76');
INSERT INTO `users` VALUES ('165', '1121212', '213131', '8', '1', 'tranha084@gmail.com', '$2y$10$hP16lKspe9IqhCSBAL9Eau9uD3WHWpEklFQZQoBh7FwrVejcB7rxK', null, '2018-03-30 15:21:34', '2018-03-30 15:21:34', '1', null, null);
INSERT INTO `users` VALUES ('166', 'abc4', '200036', '1', '1', '75_thuyenht@viettel.com.vn', '$2y$10$9q19FW2kdx2.PqSFpkTCDOu5N9SHPD/zzeVhz04vYiys8hyf7W8n.', null, '2018-03-30 17:11:47', '2018-03-30 17:11:47', '1', null, '9');
INSERT INTO `users` VALUES ('167', 'Xem hồ sơ_Tại điểm sua', '300016', '1', '1', '23_ngandt5@viettel.com.vn', '$2y$10$DpX.owqCMXfQNnydhExrOOCsU606jJ67lyoyc3LzFvD.YNsNvCwRS', null, '2018-04-02 16:26:10', '2018-04-02 16:26:10', '1', null, '70');
INSERT INTO `users` VALUES ('168', 'đăng ký outbound', '100019', '1', '1', '30_ngandt5@viettel.com.vn', '$2y$10$hwR3yVks0EvnwhDYedg2DeYCIqIp6Qf7V7wq7RCiQmyRE384JK/DK', null, '2018-04-02 16:29:10', '2018-04-02 16:29:10', '1', null, '118');
INSERT INTO `users` VALUES ('169', 'Xem hồ sơ_Tại điểm sua', '300017', '1', '1', '63_ngandt5@viettel.com.vn', '$2y$10$z9ACzg/QN3defO.HdCJhF.wjfqM3g5YGkkdNh7mzocuzqxCeXOCEK', null, '2018-04-02 16:30:16', '2018-04-02 16:30:16', '1', null, '70');
INSERT INTO `users` VALUES ('170', 'Xem hồ sơ_Tại điểm sua', '300018', '1', '1', '35_ngandt5@viettel.com.vn', '$2y$10$abV6u8Ha3gja8inJSJgd5uN0yJZMLY1orrQ5RE1mBNEb/WqIxz1J.', null, '2018-04-02 16:30:57', '2018-04-02 16:30:57', '1', null, '70');
INSERT INTO `users` VALUES ('171', 'Xem hồ sơ_Tại điểm sua', '300019', '1', '1', '38_ngandt5@viettel.com.vn', '$2y$10$T5T/UtbxKrhH2jJTQwiGTu168WTqWQyg67zODUdcneaiqfD47vF92', null, '2018-04-02 16:31:50', '2018-04-02 16:31:50', '1', null, '70');
INSERT INTO `users` VALUES ('172', 'chuyển server', '200041', '1', '1', '3_ngandt5@viettel.com.vn', '$2y$10$V4RJWV056dsTOOjg07kpZuKwhK2B7iXs/Uja6I6/slxMTZ1.4hXgW', null, '2018-04-04 01:35:28', '2018-04-04 01:35:28', '1', null, '160');
INSERT INTO `users` VALUES ('173', 'Nguyễn Thị Anh Tú', 'anhtunguyen', '3', '1', 'tunguyenanh900@gmail.com', '$2y$10$9DLCRLhZ7xyBulqQMZ0omuJne5TGmPwYmuKY81dVNfILjLtSSxLT.', 'kp43YITGToR6sb5t8JE47MPYl56Htm0wUVcjNvt9yyG97TEKNCPTKf41S7CV', '2018-04-05 08:01:27', '2018-04-05 08:01:27', '1', null, null);
INSERT INTO `users` VALUES ('174', 'Do Duy Duc', 'laven9696', 'hoivienbinhthuong', '1', 'laven9696@gmail.com', '$2y$10$Cr2PhTE8Uk1ICEAcrMRZQeNvxaMfJgOAMFI2f3DmRoT5CyZhuSkHy', null, '2018-04-22 10:27:25', '2018-04-22 13:00:16', '1', null, null);

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users_groups
-- ----------------------------

-- ----------------------------
-- Table structure for vita
-- ----------------------------
DROP TABLE IF EXISTS `vita`;
CREATE TABLE `vita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `deptec` tinyint(1) DEFAULT NULL COMMENT '1: Ban Chấp hành',
  `deptsc` tinyint(1) DEFAULT NULL COMMENT 'Ban Thường trực',
  `deptad` tinyint(1) DEFAULT NULL COMMENT 'Ban Cố vấn',
  `deptex` tinyint(1) DEFAULT NULL COMMENT 'Ban Chuyên môn: 1 - Ban Hội viên, 2 - Ban Đào tạo , 3 -	Ban Truyền thông và sự kiện ',
  `email` varchar(255) DEFAULT NULL,
  `phone` int(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vita
-- ----------------------------
INSERT INTO `vita` VALUES ('1', '1', '1', 'Trần Trọng Kiên', 'CT', 'Chủ tịch HĐQT \r\nCông ty CP Du lịch Thiên Minh', 'Tầng 9, số 70-72 Bà Triệu, Q. Hoàn Kiếm, Hà Nội', '1', '1', null, null, null, null);
INSERT INTO `vita` VALUES ('2', '1', '1', 'Phạm Lê Thảo', 'PCT', 'Phó Vụ trưởng \nVụ Lữ hành Tổng cục Du lịch ', '80 Quán Sứ, Q. Hoàn Kiếm, HN', '1', '1', null, null, null, null);
INSERT INTO `vita` VALUES ('3', '1', '1', 'Bùi Văn Dũng', 'PCT', 'Trưởng ban Hội viên và Đào tạo HHDLVN', 'tầng 7, số 58 Kim Mã, Q. Ba Đình, HN', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('4', '1', '1', 'Hoàng Hữu Lộc', 'PCT', 'Chủ tịch HĐTV Cty TNHH MTV Dịch vụ Lữ hành Saigontourist', '45 Lê Thánh Tôn, Q1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('5', '1', '1', 'Trần Văn Long', 'PCT', 'Tổng Giám đốc Công ty CP Truyền thông Du lịch Việt', '175 Nguyễn Thái Bình, Q.1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('6', '1', '1', 'Cao Trí Dũng', 'PCT', 'Chủ tịch Chi hội Lữ hành Đà Nẵng', '68 Nguyễn Thị Minh Khai, Q. hải Châu, Đà Nẵng', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('7', '1', '1', 'Huỳnh Chí Công', 'UV', 'Phó Giám đốc Phòng hướng dẫn Công ty CP Dịch vụ Du lịch Bến Thành', '82 Calmetle, Q1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('8', '1', '1', 'Nguyễn Đức Cường', 'UV', 'Hiệp hội Du lịch TP. HCM', '18 Trương Định, Q3, TP. HCM', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('9', '1', '1', 'Lâm Hải Đào', 'UV', 'Giám đốc Phòng hướng dẫn Công ty Cổ phần FIDITOUR', '127 Nguyễn Huệ, Q1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('10', '1', '1', 'Huỳnh Công Hiếu', 'UV', 'Phó phòng Hướng dẫn viên quốc tế Công ty TNHH MTV Dịch vụ lữ hành Saigontourist', '45 Lê Thánh Tôn, Q1, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('11', '1', '1', 'Trần Thị Việt Hương', 'UV', 'Giám đốc ban Hướng dẫn viên Cty CP Du lịch và Tiếp thị GTVT Việt Nam', '160 Pasteur, P6,Q3, Hồ Chí Minh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('12', '1', '1', 'Phan Bửu Toàn', 'UV', 'Phó Hiệu trưởng thường trực Trường Cao Đẳng nghề Du lịch Sài Gòn', '347A Nguyễn Thượng Hiền,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('13', '1', '1', 'Nguyễn Hữu Bình', 'UV', 'Phó Giám đốc Chi nhánh Công ty TNHH JTB-TNT tại Hà Nội', 'Phòng số 6, tầng M Khách sạn  Pan Pacific- Số 1 Đường Thanh Niên, Q. Ba Đình, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('14', '1', '1', 'Vũ Thái Chính', 'UV', 'Chủ tịch CLB Hướng dẫn viên tiếng Nhật Hà Nội', 'Tầng 7, 58 Kim Mã, Ba Đình, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('15', '1', '1', 'Lại Quốc Cường', 'UV', 'Giám đốc Công ty CP Du lịch Vẻ đẹp Việt', '41 Lô 3, Tổ 37A, Thanh Lương,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('16', '1', '1', 'Nguyễn Hồng Đài', 'UV', 'Giám đốc Công tyTNHH Du lịch Quốc tế Thái Bình Dương', 'Số 5 Hàng Chiếu, Hoàn Kiếm, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('17', '1', '1', 'Nguyễn Tiến Đạt', 'UV', 'Phó Giám đốc Công ty Du lịch Trần Việt', 'Tầng 4 Tòa nhà Bắc Á', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('18', '1', '1', 'Nguyễn Thị Huyền', 'UV', 'Giám đốc điều hành Công TY TNHH VIETRANTOUR', 'Tầng 1, Tòa nhà Coalimex', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('19', '1', '1', 'Nguyễn Thị Thu Mai', 'UV', 'Phó trưởng khoa Du lịch Viện Đại học Mở', 'Nhà B101, Nguyễn Hiền,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('20', '1', '1', 'Nguyễn Hồng Nguyên', '', 'Phó phòng Hướng dẫn Cty Lữ hành Hanoitourist', '18 Lý Thường Kiệt, Q. Hoàn Kiếm, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('21', '1', '1', 'Nguyễn Minh Thắng', 'UV', 'Trợ lý Trưởng phòng Điều hành Công ty TNHH Du lịch Trâu Việt Nam', 'Tầng 9, số 70-72 Bà Triệu, Q. Hoàn Kiếm, Hà Nội', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('22', '1', '1', 'Nguyễn Đình Thành', 'UV', 'Hiệp hội Du lịch Đà Nẵng ', '14 Nguyễn Chí Thanh, Q. Hải Châu, TP. Đà Nẵng', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('23', '1', '1', 'Lê Tấn Thanh Tùng', 'UV', 'Phó Tổng Giám đốc Công ty Cổ phần Du lịch Việt Nam Vitour', '83 Nguyễn Thị minh Khai, Q. Hải Châu, TP. Đà Nẵng', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('24', '1', '1', 'Trần Trà ', 'UV', 'Chủ tịch Chi hội Hướng dẫn viên Du lịch Đà Nẵng', 'J226/1 Trương Nữ Vương,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('25', '1', '1', 'Nguyễn Thế Huệ', 'UV', 'Phó Chủ tịch Hiệp hội Du lịch Quảng Ninh', 'Trụ sở tại Cty CP Du lịch Hạ Long. Đường Hậu Cần, TP. Hạ Long, Quảng Ninh', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('26', '1', '1', 'Trần Nhuận Vinh', 'UV', 'Chủ tịch CLB Hướng dẫn viên du lịch Quảng Ninh', 'Số 1 Cảng Mới, Bạch Đằng, TP.', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('27', '1', '1', 'Hoàng Tuấn Anh', 'UV', 'Giám đốc Công ty CP Quốc tế nguồn nhân lực', '85/261 Trần Nguyên Hãn, Q.', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('28', '1', '1', 'Nguyễn Hữu Bắc', 'UV', 'Chủ tịch Công ty TNHH MTV Đầu tư Du lịch Phuc Group', 'P.11 17 CT1, Tòa nhà Kim Thi,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('29', '1', '1', 'Nguyễn Phương Đông', 'UV', 'Trưởng văn phòng hướng dẫn thăm quan Đô thị cổ Hội An', '01 Cao Lãnh, TP. Hội An, Quảng Nam', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('30', '1', '1', 'Nguyễn Tiến Giáp', 'UV', 'Chủ nhiệm CLB Hướng dẫn viên du lịch Thanh Hóa', 'Số 411 Trần Hưng Đạo, TP. Thanh Hóa', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('31', '1', '1', 'Lê Đình Huy', 'UV', 'Chủ nhiệm CLB Hướng dẫn viên du lịch Huế', '1/63 La Sơn Phu Tử, Tp. Huế', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('32', '1', '1', 'Nguyễn Vũ Khắc Huy', 'UV', 'Giám đốc Công ty TNHH MTV Du lịch VINA Phú Quốc', 'Tổ 14, Đường 30/4 Khu phố 1', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('33', '1', '1', 'Trần Mạnh Khang', 'UV', 'Tổ trưởng Hướng dẫn viên Vietravel Cần Thơ', '05-07 Trần Văn Khéo, P. Cái Khế,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('34', '1', '1', 'Nguyễn Thị Xuân Lan', 'UV', 'Giám đốc Công ty TNHH Golden Life', '43A Lê Thánh Tôn, TP. Quy Nhơn, Bình Định', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('35', '1', '1', 'Phan Long', 'UV', 'Giám đốc Vietravel Quảng Ngãi', '516 Quang Trung, TP. Quảng Ngãi', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('36', '1', '1', 'Hoàng Bình Minh', 'UV', 'Giám đốc Công ty CP Du lịch Ninh Bình', '50 Lý Tự Trọng, TP. Ninh Bình', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('37', '1', '1', 'Võ Thanh Mỹ', 'UV', 'Giám đốc Công ty Du lịch Vietravel Vũng Tầu', '150 Trương Công Định,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('38', '1', '1', 'Phạm Chí Ta', 'UV', 'Giám đốc Công ty TNHH Lữ hành Cao nguyên Việt Nam', '24 Lý Thường Kiệt, TP.', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('39', '1', '1', 'Bùi Minh Thắng', 'UV', 'Giám đốc Công ty TNHH DVDL Thương mại Phương Thắng', '31 Nguyễn Cảnh Đức, TP. Nha Trang, Khánh Hòa', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('40', '1', '1', 'Đinh Văn Thép', 'UV', 'Chủ tịch HĐQT Công ty TNHH MTV Du lịch Thái Hân', '68 Nguyễn Thái Học,', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('41', '1', '1', 'Võ Đức Trung', 'UV', 'Giám đốc Chi nhánh Công ty CP Mạo Hiểm Việt tại Đà Lạt', '109 Nguyễn Văn Trỗi, TP.Đà Lạt, Lâm Đồng', '1', null, null, null, null, null);
INSERT INTO `vita` VALUES ('42', '1', '1', 'Nguyễn Cường Hiền', 'Ủy viên', null, null, null, '1', null, null, null, null);
INSERT INTO `vita` VALUES ('43', '1', '1', 'Nguyễn Hữu Trà', 'Ủy viên', null, null, null, '1', null, null, null, null);
INSERT INTO `vita` VALUES ('44', '1', '1', 'Chu Đức Ảnh', 'Ủy viên', null, null, null, '1', null, null, null, null);
INSERT INTO `vita` VALUES ('45', '1', '1', 'Phạm Mạnh Cương', 'Trưởng ban', null, null, null, null, null, '1', 'cuongpm84@gmail.com', '986989661');
INSERT INTO `vita` VALUES ('46', '1', '1', 'Nguyễn Quang Trung', 'Trưởng ban', null, null, null, null, null, '2', 'quangtrung.dl0812@gmail.com', '973581219');

-- ----------------------------
-- Table structure for workhistory
-- ----------------------------
DROP TABLE IF EXISTS `workhistory`;
CREATE TABLE `workhistory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberId` int(10) unsigned NOT NULL,
  `elementId` int(10) unsigned NOT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `workhistory_memberid_foreign` (`memberId`),
  KEY `workhistory_elementid_foreign` (`elementId`)
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of workhistory
-- ----------------------------
INSERT INTO `workhistory` VALUES ('5', '3', '5', null, null);
INSERT INTO `workhistory` VALUES ('6', '4', '6', null, null);
INSERT INTO `workhistory` VALUES ('7', '4', '7', null, null);
INSERT INTO `workhistory` VALUES ('8', '5', '8', null, null);
INSERT INTO `workhistory` VALUES ('9', '5', '9', null, null);
INSERT INTO `workhistory` VALUES ('10', '6', '10', null, null);
INSERT INTO `workhistory` VALUES ('11', '7', '3', null, null);
INSERT INTO `workhistory` VALUES ('12', '7', '11', null, null);
INSERT INTO `workhistory` VALUES ('13', '8', '12', null, null);
INSERT INTO `workhistory` VALUES ('14', '8', '13', null, null);
INSERT INTO `workhistory` VALUES ('15', '9', '14', null, null);
INSERT INTO `workhistory` VALUES ('16', '10', '15', null, null);
INSERT INTO `workhistory` VALUES ('17', '11', '3', null, null);
INSERT INTO `workhistory` VALUES ('18', '11', '16', null, null);
INSERT INTO `workhistory` VALUES ('19', '12', '17', null, null);
INSERT INTO `workhistory` VALUES ('20', '13', '18', null, null);
INSERT INTO `workhistory` VALUES ('21', '14', '19', null, null);
INSERT INTO `workhistory` VALUES ('22', '15', '20', null, null);
INSERT INTO `workhistory` VALUES ('25', '18', '23', null, null);
INSERT INTO `workhistory` VALUES ('27', '20', '25', null, null);
INSERT INTO `workhistory` VALUES ('30', '22', '28', null, null);
INSERT INTO `workhistory` VALUES ('31', '22', '29', null, null);
INSERT INTO `workhistory` VALUES ('32', '23', '30', null, null);
INSERT INTO `workhistory` VALUES ('33', '24', '31', null, null);
INSERT INTO `workhistory` VALUES ('34', '25', '32', null, null);
INSERT INTO `workhistory` VALUES ('37', '27', '34', null, null);
INSERT INTO `workhistory` VALUES ('38', '28', '35', null, null);
INSERT INTO `workhistory` VALUES ('39', '28', '36', null, null);
INSERT INTO `workhistory` VALUES ('40', '29', '37', null, null);
INSERT INTO `workhistory` VALUES ('41', '30', '3', null, null);
INSERT INTO `workhistory` VALUES ('42', '30', '38', null, null);
INSERT INTO `workhistory` VALUES ('44', '32', '40', null, null);
INSERT INTO `workhistory` VALUES ('45', '32', '11', null, null);
INSERT INTO `workhistory` VALUES ('46', '32', '41', null, null);
INSERT INTO `workhistory` VALUES ('47', '33', '40', null, null);
INSERT INTO `workhistory` VALUES ('48', '34', '42', null, null);
INSERT INTO `workhistory` VALUES ('49', '35', '43', null, null);
INSERT INTO `workhistory` VALUES ('50', '36', '43', null, null);
INSERT INTO `workhistory` VALUES ('51', '37', '44', null, null);
INSERT INTO `workhistory` VALUES ('53', '38', '46', null, null);
INSERT INTO `workhistory` VALUES ('54', '39', '47', null, null);
INSERT INTO `workhistory` VALUES ('55', '40', '43', null, null);
INSERT INTO `workhistory` VALUES ('56', '41', '48', null, null);
INSERT INTO `workhistory` VALUES ('57', '41', '49', null, null);
INSERT INTO `workhistory` VALUES ('58', '42', '40', null, null);
INSERT INTO `workhistory` VALUES ('59', '42', '50', null, null);
INSERT INTO `workhistory` VALUES ('60', '43', '51', null, null);
INSERT INTO `workhistory` VALUES ('61', '44', '52', null, null);
INSERT INTO `workhistory` VALUES ('62', '44', '53', null, null);
INSERT INTO `workhistory` VALUES ('63', '45', '54', null, null);
INSERT INTO `workhistory` VALUES ('64', '46', '55', null, null);
INSERT INTO `workhistory` VALUES ('65', '46', '56', null, null);
INSERT INTO `workhistory` VALUES ('66', '46', '57', null, null);
INSERT INTO `workhistory` VALUES ('67', '47', '58', null, null);
INSERT INTO `workhistory` VALUES ('68', '48', '59', null, null);
INSERT INTO `workhistory` VALUES ('69', '49', '60', null, null);
INSERT INTO `workhistory` VALUES ('70', '49', '16', null, null);
INSERT INTO `workhistory` VALUES ('71', '50', '61', null, null);
INSERT INTO `workhistory` VALUES ('72', '51', '62', null, null);
INSERT INTO `workhistory` VALUES ('73', '51', '63', null, null);
INSERT INTO `workhistory` VALUES ('74', '51', '64', null, null);
INSERT INTO `workhistory` VALUES ('75', '52', '65', null, null);
INSERT INTO `workhistory` VALUES ('76', '52', '66', null, null);
INSERT INTO `workhistory` VALUES ('78', '54', '68', null, null);
INSERT INTO `workhistory` VALUES ('79', '55', '69', null, null);
INSERT INTO `workhistory` VALUES ('80', '55', '70', null, null);
INSERT INTO `workhistory` VALUES ('81', '56', '71', null, null);
INSERT INTO `workhistory` VALUES ('82', '57', '72', null, null);
INSERT INTO `workhistory` VALUES ('83', '57', '73', null, null);
INSERT INTO `workhistory` VALUES ('85', '58', '74', null, null);
INSERT INTO `workhistory` VALUES ('86', '58', '75', null, null);
INSERT INTO `workhistory` VALUES ('88', '60', '77', null, null);
INSERT INTO `workhistory` VALUES ('89', '60', '78', null, null);
INSERT INTO `workhistory` VALUES ('90', '60', '79', null, null);
INSERT INTO `workhistory` VALUES ('91', '60', '80', null, null);
INSERT INTO `workhistory` VALUES ('92', '61', '76', null, null);
INSERT INTO `workhistory` VALUES ('93', '62', '76', null, null);
INSERT INTO `workhistory` VALUES ('94', '53', '67', '2018-03-16 11:31:30', '2018-03-16 11:31:30');
INSERT INTO `workhistory` VALUES ('95', '63', '76', null, null);
INSERT INTO `workhistory` VALUES ('96', '64', '81', null, null);
INSERT INTO `workhistory` VALUES ('97', '64', '82', null, null);
INSERT INTO `workhistory` VALUES ('98', '64', '83', null, null);
INSERT INTO `workhistory` VALUES ('99', '64', '84', null, null);
INSERT INTO `workhistory` VALUES ('100', '64', '85', null, null);
INSERT INTO `workhistory` VALUES ('101', '64', '86', null, null);
INSERT INTO `workhistory` VALUES ('102', '64', '87', null, null);
INSERT INTO `workhistory` VALUES ('103', '64', '88', null, null);
INSERT INTO `workhistory` VALUES ('104', '64', '89', null, null);
INSERT INTO `workhistory` VALUES ('105', '64', '90', null, null);
INSERT INTO `workhistory` VALUES ('106', '64', '91', null, null);
INSERT INTO `workhistory` VALUES ('107', '64', '92', null, null);
INSERT INTO `workhistory` VALUES ('108', '64', '93', null, null);
INSERT INTO `workhistory` VALUES ('109', '64', '36', null, null);
INSERT INTO `workhistory` VALUES ('110', '65', '94', null, null);
INSERT INTO `workhistory` VALUES ('111', '65', '95', null, null);
INSERT INTO `workhistory` VALUES ('112', '66', '94', null, null);
INSERT INTO `workhistory` VALUES ('113', '66', '95', null, null);
INSERT INTO `workhistory` VALUES ('114', '67', '94', null, null);
INSERT INTO `workhistory` VALUES ('115', '67', '95', null, null);
INSERT INTO `workhistory` VALUES ('116', '68', '94', null, null);
INSERT INTO `workhistory` VALUES ('117', '68', '95', null, null);
INSERT INTO `workhistory` VALUES ('118', '69', '96', null, null);
INSERT INTO `workhistory` VALUES ('121', '71', '97', null, null);
INSERT INTO `workhistory` VALUES ('122', '71', '90', null, null);
INSERT INTO `workhistory` VALUES ('123', '71', '98', null, null);
INSERT INTO `workhistory` VALUES ('124', '71', '36', null, null);
INSERT INTO `workhistory` VALUES ('125', '72', '94', null, null);
INSERT INTO `workhistory` VALUES ('126', '72', '95', null, null);
INSERT INTO `workhistory` VALUES ('127', '73', '99', null, null);
INSERT INTO `workhistory` VALUES ('130', '74', '94', null, null);
INSERT INTO `workhistory` VALUES ('131', '74', '95', null, null);
INSERT INTO `workhistory` VALUES ('132', '75', '94', null, null);
INSERT INTO `workhistory` VALUES ('133', '75', '95', null, null);
INSERT INTO `workhistory` VALUES ('136', '77', '94', null, null);
INSERT INTO `workhistory` VALUES ('137', '77', '95', null, null);
INSERT INTO `workhistory` VALUES ('138', '78', '94', null, null);
INSERT INTO `workhistory` VALUES ('139', '78', '95', null, null);
INSERT INTO `workhistory` VALUES ('142', '70', '94', '2018-03-16 15:37:33', '2018-03-16 15:37:33');
INSERT INTO `workhistory` VALUES ('143', '70', '95', '2018-03-16 15:37:33', '2018-03-16 15:37:33');
INSERT INTO `workhistory` VALUES ('144', '16', '21', '2018-03-16 15:39:20', '2018-03-16 15:39:20');
INSERT INTO `workhistory` VALUES ('154', '31', '112', null, null);
INSERT INTO `workhistory` VALUES ('155', '31', '27', null, null);
INSERT INTO `workhistory` VALUES ('166', '79', '116', '2018-03-16 17:06:28', '2018-03-16 17:06:28');
INSERT INTO `workhistory` VALUES ('167', '79', '116', '2018-03-16 17:06:28', '2018-03-16 17:06:28');
INSERT INTO `workhistory` VALUES ('168', '79', '117', '2018-03-16 17:06:28', '2018-03-16 17:06:28');
INSERT INTO `workhistory` VALUES ('169', '79', '118', '2018-03-16 17:06:28', '2018-03-16 17:06:28');
INSERT INTO `workhistory` VALUES ('172', '80', '123', null, null);
INSERT INTO `workhistory` VALUES ('173', '17', '124', null, null);
INSERT INTO `workhistory` VALUES ('174', '81', '125', null, null);
INSERT INTO `workhistory` VALUES ('175', '81', '126', null, null);
INSERT INTO `workhistory` VALUES ('176', '82', '125', null, null);
INSERT INTO `workhistory` VALUES ('177', '82', '126', null, null);
INSERT INTO `workhistory` VALUES ('186', '84', '130', null, null);
INSERT INTO `workhistory` VALUES ('193', '86', '133', '2018-03-20 10:24:38', '2018-03-20 10:24:38');
INSERT INTO `workhistory` VALUES ('194', '86', '134', '2018-03-20 10:24:38', '2018-03-20 10:24:38');
INSERT INTO `workhistory` VALUES ('196', '88', '135', null, null);
INSERT INTO `workhistory` VALUES ('198', '89', '136', null, null);
INSERT INTO `workhistory` VALUES ('209', '26', '3', '2018-03-22 10:54:29', '2018-03-22 10:54:29');
INSERT INTO `workhistory` VALUES ('210', '26', '33', '2018-03-22 10:54:29', '2018-03-22 10:54:29');
INSERT INTO `workhistory` VALUES ('211', '2', '147', null, null);
INSERT INTO `workhistory` VALUES ('212', '2', '148', null, null);
INSERT INTO `workhistory` VALUES ('213', '87', '23', '2018-03-22 14:06:52', '2018-03-22 14:06:52');
INSERT INTO `workhistory` VALUES ('220', '90', '151', null, null);
INSERT INTO `workhistory` VALUES ('221', '91', '152', null, null);
INSERT INTO `workhistory` VALUES ('222', '92', '153', null, null);
INSERT INTO `workhistory` VALUES ('223', '19', '154', null, null);
INSERT INTO `workhistory` VALUES ('225', '93', '155', '2018-03-23 11:00:39', '2018-03-23 11:00:39');
INSERT INTO `workhistory` VALUES ('226', '94', '156', null, null);
INSERT INTO `workhistory` VALUES ('227', '76', '157', null, null);
INSERT INTO `workhistory` VALUES ('228', '21', '158', null, null);
INSERT INTO `workhistory` VALUES ('233', '95', '162', null, null);
INSERT INTO `workhistory` VALUES ('234', '96', '162', null, null);
INSERT INTO `workhistory` VALUES ('235', '97', '163', null, null);
INSERT INTO `workhistory` VALUES ('236', '98', '164', null, null);
INSERT INTO `workhistory` VALUES ('237', '99', '165', null, null);
INSERT INTO `workhistory` VALUES ('238', '99', '166', null, null);
INSERT INTO `workhistory` VALUES ('241', '100', '169', null, null);
INSERT INTO `workhistory` VALUES ('242', '100', '170', null, null);
INSERT INTO `workhistory` VALUES ('243', '101', '169', null, null);
INSERT INTO `workhistory` VALUES ('244', '101', '171', null, null);
INSERT INTO `workhistory` VALUES ('245', '102', '169', null, null);
INSERT INTO `workhistory` VALUES ('246', '102', '171', null, null);
INSERT INTO `workhistory` VALUES ('247', '103', '169', null, null);
INSERT INTO `workhistory` VALUES ('248', '103', '171', null, null);
INSERT INTO `workhistory` VALUES ('249', '104', '172', null, null);
INSERT INTO `workhistory` VALUES ('250', '105', '173', null, null);
INSERT INTO `workhistory` VALUES ('251', '106', '174', null, null);
INSERT INTO `workhistory` VALUES ('252', '106', '175', null, null);
INSERT INTO `workhistory` VALUES ('253', '107', '176', null, null);
INSERT INTO `workhistory` VALUES ('254', '108', '174', null, null);
INSERT INTO `workhistory` VALUES ('255', '108', '175', null, null);
INSERT INTO `workhistory` VALUES ('256', '109', '176', null, null);
INSERT INTO `workhistory` VALUES ('257', '110', '177', null, null);
INSERT INTO `workhistory` VALUES ('258', '111', '178', null, null);
INSERT INTO `workhistory` VALUES ('259', '112', '174', null, null);
INSERT INTO `workhistory` VALUES ('260', '85', '179', null, null);
INSERT INTO `workhistory` VALUES ('261', '85', '180', null, null);
INSERT INTO `workhistory` VALUES ('274', '1', '193', null, null);
INSERT INTO `workhistory` VALUES ('275', '1', '194', null, null);
INSERT INTO `workhistory` VALUES ('276', '113', '195', null, null);
INSERT INTO `workhistory` VALUES ('278', '114', '197', null, null);
INSERT INTO `workhistory` VALUES ('279', '115', '195', null, null);
INSERT INTO `workhistory` VALUES ('280', '115', '198', null, null);
INSERT INTO `workhistory` VALUES ('287', '116', '205', null, null);
INSERT INTO `workhistory` VALUES ('290', '117', '206', '2018-03-27 14:42:50', '2018-03-27 14:42:50');
INSERT INTO `workhistory` VALUES ('292', '118', '207', '2018-03-27 15:19:18', '2018-03-27 15:19:18');
INSERT INTO `workhistory` VALUES ('293', '119', '195', null, null);
INSERT INTO `workhistory` VALUES ('294', '120', '195', null, null);
INSERT INTO `workhistory` VALUES ('295', '121', '195', null, null);
INSERT INTO `workhistory` VALUES ('296', '122', '207', null, null);
INSERT INTO `workhistory` VALUES ('298', '124', '208', null, null);
INSERT INTO `workhistory` VALUES ('299', '125', '209', null, null);
INSERT INTO `workhistory` VALUES ('300', '123', '174', '2018-03-28 08:33:33', '2018-03-28 08:33:33');
INSERT INTO `workhistory` VALUES ('306', '126', '215', null, null);
INSERT INTO `workhistory` VALUES ('307', '127', '216', null, null);
INSERT INTO `workhistory` VALUES ('308', '128', '174', null, null);
INSERT INTO `workhistory` VALUES ('311', '129', '175', '2018-03-28 10:22:15', '2018-03-28 10:22:15');
INSERT INTO `workhistory` VALUES ('314', '130', '175', '2018-03-28 10:43:25', '2018-03-28 10:43:25');
INSERT INTO `workhistory` VALUES ('335', '131', '237', null, null);
INSERT INTO `workhistory` VALUES ('338', '132', '240', null, null);
INSERT INTO `workhistory` VALUES ('339', '133', '205', null, null);
INSERT INTO `workhistory` VALUES ('341', '135', '242', null, null);
INSERT INTO `workhistory` VALUES ('342', '136', '175', null, null);
INSERT INTO `workhistory` VALUES ('343', '137', '243', null, null);
INSERT INTO `workhistory` VALUES ('344', '134', '244', null, null);
INSERT INTO `workhistory` VALUES ('345', '83', '245', null, null);
INSERT INTO `workhistory` VALUES ('346', '138', '195', null, null);
INSERT INTO `workhistory` VALUES ('347', '139', '195', null, null);
INSERT INTO `workhistory` VALUES ('348', '140', '174', null, null);
INSERT INTO `workhistory` VALUES ('349', '141', '215', null, null);
INSERT INTO `workhistory` VALUES ('350', '142', '246', null, null);
INSERT INTO `workhistory` VALUES ('351', '143', '247', null, null);
INSERT INTO `workhistory` VALUES ('353', '144', '249', null, null);
INSERT INTO `workhistory` VALUES ('355', '145', '251', null, null);
INSERT INTO `workhistory` VALUES ('357', '59', '253', null, null);
INSERT INTO `workhistory` VALUES ('358', '146', '254', null, null);
INSERT INTO `workhistory` VALUES ('359', '147', '255', null, null);
INSERT INTO `workhistory` VALUES ('360', '148', '255', null, null);
INSERT INTO `workhistory` VALUES ('361', '149', '255', null, null);
INSERT INTO `workhistory` VALUES ('362', '150', '256', null, null);
INSERT INTO `workhistory` VALUES ('363', '151', '257', null, null);
INSERT INTO `workhistory` VALUES ('364', '152', '258', null, null);
INSERT INTO `workhistory` VALUES ('365', '153', '258', null, null);
INSERT INTO `workhistory` VALUES ('366', '154', '258', null, null);
INSERT INTO `workhistory` VALUES ('367', '155', '256', null, null);
INSERT INTO `workhistory` VALUES ('368', '156', '256', null, null);
INSERT INTO `workhistory` VALUES ('369', '157', '259', null, null);
INSERT INTO `workhistory` VALUES ('370', '158', '259', null, null);
INSERT INTO `workhistory` VALUES ('371', '159', '259', null, null);
INSERT INTO `workhistory` VALUES ('372', '160', '260', null, null);
INSERT INTO `workhistory` VALUES ('373', '161', '261', null, null);
INSERT INTO `workhistory` VALUES ('374', '162', '262', null, null);

-- ----------------------------
-- Table structure for works
-- ----------------------------
DROP TABLE IF EXISTS `works`;
CREATE TABLE `works` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `elementName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeOfContract` tinyint(4) DEFAULT NULL,
  `fromDate` datetime DEFAULT NULL,
  `toDate` datetime DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of works
-- ----------------------------
INSERT INTO `works` VALUES ('5', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 10:05:04', '2018-03-13 10:05:04', null);
INSERT INTO `works` VALUES ('6', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 10:10:03', '2018-03-13 10:10:03', null);
INSERT INTO `works` VALUES ('7', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 10:10:03', '2018-03-13 10:10:03', null);
INSERT INTO `works` VALUES ('8', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2015-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-13 10:11:17', '2018-03-13 10:11:17', null);
INSERT INTO `works` VALUES ('9', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2014-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-13 10:11:17', '2018-03-13 10:11:17', null);
INSERT INTO `works` VALUES ('10', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-13 10:11:40', '2018-03-13 10:11:40', null);
INSERT INTO `works` VALUES ('11', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2016-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-13 10:14:55', '2018-03-13 10:14:55', null);
INSERT INTO `works` VALUES ('12', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 10:35:52', '2018-03-13 10:35:52', null);
INSERT INTO `works` VALUES ('13', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', null, null, '2018-03-13 10:35:52', '2018-03-13 10:35:52', null);
INSERT INTO `works` VALUES ('14', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 11:42:24', '2018-03-13 11:42:24', null);
INSERT INTO `works` VALUES ('16', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2015-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-13 14:00:48', '2018-03-13 14:00:48', null);
INSERT INTO `works` VALUES ('17', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-13 14:13:06', '2018-03-13 14:13:06', null);
INSERT INTO `works` VALUES ('18', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-13 14:16:53', '2018-03-13 14:16:53', null);
INSERT INTO `works` VALUES ('19', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 14:20:27', '2018-03-13 14:20:27', null);
INSERT INTO `works` VALUES ('20', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-13 14:22:15', '2018-03-13 14:22:15', null);
INSERT INTO `works` VALUES ('21', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-13 22:29:33', '2018-03-13 22:29:33', null);
INSERT INTO `works` VALUES ('23', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-14 09:42:54', '2018-03-14 09:42:54', null);
INSERT INTO `works` VALUES ('24', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2017-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 10:31:48', '2018-03-14 10:31:48', null);
INSERT INTO `works` VALUES ('27', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', null, null, '2018-03-14 10:37:23', '2018-03-14 10:37:23', null);
INSERT INTO `works` VALUES ('28', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-14 10:40:40', '2018-03-14 10:40:40', null);
INSERT INTO `works` VALUES ('29', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', null, null, '2018-03-14 10:40:40', '2018-03-14 10:40:40', null);
INSERT INTO `works` VALUES ('30', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-14 10:58:44', '2018-03-14 10:58:44', null);
INSERT INTO `works` VALUES ('32', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-14 11:03:19', '2018-03-14 11:03:19', null);
INSERT INTO `works` VALUES ('33', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2017-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-14 11:04:03', '2018-03-14 11:04:03', null);
INSERT INTO `works` VALUES ('34', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-14 11:05:08', '2018-03-14 11:05:08', null);
INSERT INTO `works` VALUES ('36', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-14 11:08:33', '2018-03-14 11:08:33', null);
INSERT INTO `works` VALUES ('37', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-14 11:10:14', '2018-03-14 11:10:14', null);
INSERT INTO `works` VALUES ('38', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2016-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-14 11:11:30', '2018-03-14 11:11:30', null);
INSERT INTO `works` VALUES ('40', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 11:15:27', '2018-03-14 11:15:27', null);
INSERT INTO `works` VALUES ('41', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 11:15:27', '2018-03-14 11:15:27', null);
INSERT INTO `works` VALUES ('42', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-14 11:20:04', '2018-03-14 11:20:04', null);
INSERT INTO `works` VALUES ('43', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-14 11:22:42', '2018-03-14 11:22:42', null);
INSERT INTO `works` VALUES ('44', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-14 17:29:23', '2018-03-14 17:29:23', null);
INSERT INTO `works` VALUES ('45', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-14 17:30:16', '2018-03-14 17:30:16', null);
INSERT INTO `works` VALUES ('46', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2015-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-14 17:31:14', '2018-03-14 17:31:14', null);
INSERT INTO `works` VALUES ('47', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 10:53:49', '2018-03-15 10:53:49', null);
INSERT INTO `works` VALUES ('48', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-15 11:29:04', '2018-03-15 11:29:04', null);
INSERT INTO `works` VALUES ('49', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2016-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:29:04', '2018-03-15 11:29:04', null);
INSERT INTO `works` VALUES ('50', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2016-01-01 00:00:00', '2020-01-01 00:00:00', '2018-03-15 11:29:59', '2018-03-15 11:29:59', null);
INSERT INTO `works` VALUES ('51', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-15 11:31:40', '2018-03-15 11:31:40', null);
INSERT INTO `works` VALUES ('52', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:36:10', '2018-03-15 11:36:10', null);
INSERT INTO `works` VALUES ('53', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:36:10', '2018-03-15 11:36:10', null);
INSERT INTO `works` VALUES ('54', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:38:59', '2018-03-15 11:38:59', null);
INSERT INTO `works` VALUES ('55', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:39:11', '2018-03-15 11:39:11', null);
INSERT INTO `works` VALUES ('56', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-15 11:39:11', '2018-03-15 11:39:11', null);
INSERT INTO `works` VALUES ('57', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-15 11:39:11', '2018-03-15 11:39:11', null);
INSERT INTO `works` VALUES ('58', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 11:52:59', '2018-03-15 11:52:59', null);
INSERT INTO `works` VALUES ('59', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2015-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-15 11:52:59', '2018-03-15 11:52:59', null);
INSERT INTO `works` VALUES ('60', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-15 13:38:43', '2018-03-15 13:38:43', null);
INSERT INTO `works` VALUES ('61', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 13:44:37', '2018-03-15 13:44:37', null);
INSERT INTO `works` VALUES ('62', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-15 13:53:07', '2018-03-15 13:53:07', null);
INSERT INTO `works` VALUES ('63', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2015-01-01 00:00:00', '2019-01-01 00:00:00', '2018-03-15 13:53:07', '2018-03-15 13:53:07', null);
INSERT INTO `works` VALUES ('64', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-15 13:53:07', '2018-03-15 13:53:07', null);
INSERT INTO `works` VALUES ('65', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-15 14:30:20', '2018-03-15 14:30:20', null);
INSERT INTO `works` VALUES ('66', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-15 14:30:20', '2018-03-15 14:30:20', null);
INSERT INTO `works` VALUES ('67', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 14:33:57', '2018-03-15 14:33:57', null);
INSERT INTO `works` VALUES ('68', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-15 14:38:02', '2018-03-15 14:38:02', null);
INSERT INTO `works` VALUES ('69', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-15 16:18:01', '2018-03-15 16:18:01', null);
INSERT INTO `works` VALUES ('70', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-15 16:18:01', '2018-03-15 16:18:01', null);
INSERT INTO `works` VALUES ('71', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-15 16:27:16', '2018-03-15 16:27:16', null);
INSERT INTO `works` VALUES ('72', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-15 16:27:39', '2018-03-15 16:27:39', null);
INSERT INTO `works` VALUES ('73', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2018-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-15 16:27:39', '2018-03-15 16:27:39', null);
INSERT INTO `works` VALUES ('74', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-15 16:31:06', '2018-03-15 16:31:06', null);
INSERT INTO `works` VALUES ('75', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-15 16:31:29', '2018-03-15 16:31:29', null);
INSERT INTO `works` VALUES ('77', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 09:36:06', '2018-03-16 09:36:06', null);
INSERT INTO `works` VALUES ('78', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 09:36:06', '2018-03-16 09:36:06', null);
INSERT INTO `works` VALUES ('79', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2015-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 09:36:06', '2018-03-16 09:36:06', null);
INSERT INTO `works` VALUES ('80', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 09:36:06', '2018-03-16 09:36:06', null);
INSERT INTO `works` VALUES ('81', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('82', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2015-01-01 00:00:00', '2023-01-01 00:00:00', '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('83', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('84', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('85', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('86', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('87', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('88', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('89', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('90', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', null, null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('91', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, '2019-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('92', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, '2018-01-01 00:00:00', null, '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('93', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, null, '2015-01-01 00:00:00', '2018-03-16 11:34:40', '2018-03-16 11:34:40', null);
INSERT INTO `works` VALUES ('94', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 13:32:45', '2018-03-16 13:32:45', null);
INSERT INTO `works` VALUES ('95', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2015-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 13:32:45', '2018-03-16 13:32:45', null);
INSERT INTO `works` VALUES ('96', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 13:39:48', '2018-03-16 13:39:48', null);
INSERT INTO `works` VALUES ('97', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-16 13:44:56', '2018-03-16 13:44:56', null);
INSERT INTO `works` VALUES ('98', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', null, '2015-01-01 00:00:00', null, '2018-03-16 13:44:56', '2018-03-16 13:44:56', null);
INSERT INTO `works` VALUES ('99', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 14:25:47', '2018-03-16 14:25:47', null);
INSERT INTO `works` VALUES ('104', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 16:09:19', '2018-03-16 16:09:19', null);
INSERT INTO `works` VALUES ('107', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 16:25:25', '2018-03-16 16:25:25', null);
INSERT INTO `works` VALUES ('108', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:25:25', '2018-03-16 16:25:25', null);
INSERT INTO `works` VALUES ('109', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:25:25', '2018-03-16 16:25:25', null);
INSERT INTO `works` VALUES ('112', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-16 16:28:16', '2018-03-16 16:28:16', null);
INSERT INTO `works` VALUES ('113', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 16:37:41', '2018-03-16 16:37:41', null);
INSERT INTO `works` VALUES ('114', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:37:41', '2018-03-16 16:37:41', null);
INSERT INTO `works` VALUES ('115', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:37:41', '2018-03-16 16:37:41', null);
INSERT INTO `works` VALUES ('116', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-16 16:55:06', '2018-03-16 16:55:06', null);
INSERT INTO `works` VALUES ('117', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:55:06', '2018-03-16 16:55:06', null);
INSERT INTO `works` VALUES ('118', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 16:55:06', '2018-03-16 16:55:06', null);
INSERT INTO `works` VALUES ('123', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-16 17:37:11', '2018-03-16 17:37:11', null);
INSERT INTO `works` VALUES ('124', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-19 08:50:33', '2018-03-19 08:50:33', null);
INSERT INTO `works` VALUES ('125', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-19 11:37:54', '2018-03-19 11:37:54', null);
INSERT INTO `works` VALUES ('126', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2016-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-19 11:37:54', '2018-03-19 11:37:54', null);
INSERT INTO `works` VALUES ('130', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-20 08:56:42', '2018-03-20 08:56:42', null);
INSERT INTO `works` VALUES ('133', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-20 10:22:59', '2018-03-20 10:22:59', null);
INSERT INTO `works` VALUES ('134', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2014-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-20 10:22:59', '2018-03-20 10:22:59', null);
INSERT INTO `works` VALUES ('135', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-20 11:08:11', '2018-03-20 11:08:11', null);
INSERT INTO `works` VALUES ('136', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-21 10:12:52', '2018-03-21 10:12:52', null);
INSERT INTO `works` VALUES ('147', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '1', '2014-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-22 11:56:20', '2018-03-22 11:56:20', null);
INSERT INTO `works` VALUES ('148', 'sdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssdfsdfsdfsddsssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', '2', '2015-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-22 11:56:20', '2018-03-22 11:56:20', null);
INSERT INTO `works` VALUES ('151', '112341123', '2', '2018-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-23 09:45:30', '2018-03-23 09:45:30', null);
INSERT INTO `works` VALUES ('152', 'DN1', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-23 09:53:29', '2018-03-23 09:53:29', null);
INSERT INTO `works` VALUES ('153', '122341211', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-23 09:58:34', '2018-03-23 09:58:34', null);
INSERT INTO `works` VALUES ('154', 'DN 1', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-23 10:06:04', '2018-03-23 10:06:04', null);
INSERT INTO `works` VALUES ('155', 'Doanh nghiệp Haminco', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-23 10:56:58', '2018-03-23 10:56:58', null);
INSERT INTO `works` VALUES ('156', 'DN 1', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-23 15:12:15', '2018-03-23 15:12:15', null);
INSERT INTO `works` VALUES ('157', 'DN1', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-23 16:11:57', '2018-03-23 16:11:57', null);
INSERT INTO `works` VALUES ('158', 'DN1', '1', '2014-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-23 16:21:51', '2018-03-23 16:21:51', null);
INSERT INTO `works` VALUES ('162', 'DN 1', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-26 08:38:00', '2018-03-26 08:38:00', null);
INSERT INTO `works` VALUES ('163', 'DN 1', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-26 09:17:19', '2018-03-26 09:17:19', null);
INSERT INTO `works` VALUES ('164', 'DN1', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-26 09:20:07', '2018-03-26 09:20:07', null);
INSERT INTO `works` VALUES ('165', 'DN 1 DN 1 DN 1 DN 1 DN 1 DN 1 DN 1 DN 1 DN 1 DN 1', '1', '2013-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-26 11:52:57', '2018-03-26 11:52:57', null);
INSERT INTO `works` VALUES ('166', null, null, null, null, '2018-03-26 11:52:57', '2018-03-26 11:52:57', null);
INSERT INTO `works` VALUES ('169', '%3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%22%74%65%73%74%22%29%3C%2F%73%63%72%69%70%74%3E', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-26 16:14:06', '2018-03-26 16:14:06', null);
INSERT INTO `works` VALUES ('170', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '2', '2014-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-26 16:14:06', '2018-03-26 16:14:06', null);
INSERT INTO `works` VALUES ('171', '<script> alert(\"&#x7', '2', '2014-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-26 16:16:00', '2018-03-26 16:16:00', null);
INSERT INTO `works` VALUES ('172', '<script> alert(\"&#x7', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-26 17:03:03', '2018-03-26 17:03:03', null);
INSERT INTO `works` VALUES ('173', '<script> alert(\"test\") </script>', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-26 17:13:08', '2018-03-26 17:13:08', null);
INSERT INTO `works` VALUES ('174', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-26 17:19:54', '2018-03-26 17:19:54', null);
INSERT INTO `works` VALUES ('175', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '2', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-26 17:19:54', '2018-03-26 17:19:54', null);
INSERT INTO `works` VALUES ('176', '<script> alert(\"test\") </script>', '1', '2017-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-26 17:20:39', '2018-03-26 17:20:39', null);
INSERT INTO `works` VALUES ('177', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-26 17:22:31', '2018-03-26 17:22:31', null);
INSERT INTO `works` VALUES ('178', '<script> alert(\"test\") </script>', '1', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-26 17:25:28', '2018-03-26 17:25:28', null);
INSERT INTO `works` VALUES ('179', '111', '1', '2013-01-01 00:00:00', '2017-01-01 00:00:00', '2018-03-26 18:55:12', '2018-03-26 18:55:12', null);
INSERT INTO `works` VALUES ('180', '222222', '2', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-26 18:55:12', '2018-03-26 18:55:12', null);
INSERT INTO `works` VALUES ('193', 'Tên doanh nghiệp 123355', '1', '2014-01-01 00:00:00', '2013-01-01 00:00:00', '2018-03-26 23:38:11', '2018-03-26 23:38:11', null);
INSERT INTO `works` VALUES ('194', 'Tên doanh nghiệp 223355', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-26 23:38:11', '2018-03-26 23:38:11', null);
INSERT INTO `works` VALUES ('195', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-27 08:20:51', '2018-03-27 08:20:51', null);
INSERT INTO `works` VALUES ('197', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x6', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-27 10:05:59', '2018-03-27 10:05:59', null);
INSERT INTO `works` VALUES ('198', '<script><script>alert(\"test\")</script></script> hoặc <!--script> alert(\"test\")<!--/script>', '2', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-27 10:15:49', '2018-03-27 10:15:49', null);
INSERT INTO `works` VALUES ('205', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '2', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-27 14:00:43', '2018-03-27 14:00:43', null);
INSERT INTO `works` VALUES ('206', 'Tên doanh nghiệp đã và đang công tác Tên doanh nghiệp đã và đang công tác', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-27 14:28:04', '2018-03-27 14:28:04', null);
INSERT INTO `works` VALUES ('207', '<script> alert(\"&#x7', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-27 15:19:17', '2018-03-27 15:19:17', null);
INSERT INTO `works` VALUES ('208', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '2', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-28 00:02:53', '2018-03-28 00:02:53', null);
INSERT INTO `works` VALUES ('209', '<script> alert(\"test\") </script><script> alert(\"test\") </script>', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-28 00:06:48', '2018-03-28 00:06:48', null);
INSERT INTO `works` VALUES ('215', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-28 09:37:08', '2018-03-28 09:37:08', null);
INSERT INTO `works` VALUES ('216', '<script> alert(\"&#x7', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-28 09:38:41', '2018-03-28 09:38:41', null);
INSERT INTO `works` VALUES ('237', 'check in hồ sơ của hội viên chính thức', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-28 15:12:11', '2018-03-28 15:12:11', null);
INSERT INTO `works` VALUES ('240', 'đăng ký lại1', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-28 16:09:46', '2018-03-28 16:09:46', null);
INSERT INTO `works` VALUES ('242', '<script> alert(\"test\") </script>', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-28 17:28:50', '2018-03-28 17:28:50', null);
INSERT INTO `works` VALUES ('243', '&#x3C;&#x73;&#x63;&#x72;&#x69;&#x70;&#x74;&#x3E;&#x20;&#x61;&#x6C;&#x65;&#x72;&#x74;&#x28;&#x22;&#x7', '2', '2017-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-28 17:34:48', '2018-03-28 17:34:48', null);
INSERT INTO `works` VALUES ('244', 'Viettrantour', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-03-28 17:52:58', '2018-03-28 17:52:58', null);
INSERT INTO `works` VALUES ('245', 'viettel 18 viettel 18 viettel 18 viettel 18', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-29 08:18:39', '2018-03-29 08:18:39', null);
INSERT INTO `works` VALUES ('246', '135135135', '2', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-30 10:56:43', '2018-03-30 10:56:43', null);
INSERT INTO `works` VALUES ('247', '135135135', '2', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-03-30 10:59:39', '2018-03-30 10:59:39', null);
INSERT INTO `works` VALUES ('249', '3', '1', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-30 14:12:02', '2018-03-30 14:12:02', null);
INSERT INTO `works` VALUES ('251', '234', '1', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-30 15:50:44', '2018-03-30 15:50:44', null);
INSERT INTO `works` VALUES ('253', '<script> alert(\"&#x7', '1', '2014-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-30 16:24:05', '2018-03-30 16:24:05', null);
INSERT INTO `works` VALUES ('254', 'CA Hà Nội_inbound', '1', '2014-01-01 00:00:00', '2015-01-01 00:00:00', '2018-03-30 16:47:34', '2018-03-30 16:47:34', null);
INSERT INTO `works` VALUES ('255', 'vt', '1', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-03-30 23:38:33', '2018-03-30 23:38:33', null);
INSERT INTO `works` VALUES ('256', 'Doanh nghiệp 1', '1', '2013-01-01 00:00:00', '2015-01-01 00:00:00', '2018-04-02 08:58:00', '2018-04-02 08:58:00', null);
INSERT INTO `works` VALUES ('257', 'Doanh nghiệp 1', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-04-02 09:59:33', '2018-04-02 09:59:33', null);
INSERT INTO `works` VALUES ('258', 'Doanh nghiệp 3', '1', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-04-02 14:13:53', '2018-04-02 14:13:53', null);
INSERT INTO `works` VALUES ('259', '11111111', '1', '2013-01-01 00:00:00', '2014-01-01 00:00:00', '2018-04-02 15:22:09', '2018-04-02 15:22:09', null);
INSERT INTO `works` VALUES ('260', 'Doanh nghiệp 1', '2', '2013-01-01 00:00:00', '2016-01-01 00:00:00', '2018-04-04 01:23:17', '2018-04-04 01:23:17', null);
INSERT INTO `works` VALUES ('261', '113332131', '2', '2018-01-01 00:00:00', '2018-01-01 00:00:00', '2018-04-05 02:24:04', '2018-04-05 02:24:04', null);
INSERT INTO `works` VALUES ('262', 'Du lich bốn mùa', '2', '2017-01-01 00:00:00', '2018-01-01 00:00:00', '2018-04-07 07:40:07', '2018-04-07 07:40:07', null);
