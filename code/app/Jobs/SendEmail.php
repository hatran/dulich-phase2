<?php

namespace App\Jobs;

use App\Libs\Helpers\LogsHelper;
use App\Mail\SendMail;
use App\Mail\SendMailWithAttach;
use App\Providers\LogServiceProvider;
use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $template;

    protected $data;

    protected $to;

    protected $bcc;

    protected $subject;

    protected $attach;

    /**
     * Create a new job instance.
     * @param $template
     * @param $data
     * @param $to
     * @param $bcc
     * @param $subject;
     * @return void
     */
    public function __construct($template = 'default', $data = array(), $to = array(), $bcc = array(), $subject = 'Subject', $attach = null)
    {
        //
        $this->template = $template;
        $this->data = $data;
        $this->to = $to;
        $this->bcc = $bcc;
        $this->subject = $subject;
        $this->attach = $attach;
    }

    /**
     * Execute the job.
     *
     */
    public function handle()
    {
        LogServiceProvider::createEmailHistory($this->to, 'Một email vừa được gửi với mẫu ' . $this->template, $this->subject, view($this->template, $this->data));
        if (empty($this->attach)) {
            Mail::send(new SendMail($this->template, $this->to, $this->data ,$this->subject));
        } else {
            Mail::send(new SendMailWithAttach($this->template, $this->to, $this->data ,$this->subject, $this->attach));
        }
    }
}
