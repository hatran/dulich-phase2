<?php

namespace App\Jobs;

use App\Libs\Helpers\Utils;
use Illuminate\Bus\Queueable;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $phoneNumber;

    protected $content;

    /**
     * SendSms constructor.
     *
     * @param $phoneNumber
     * @param string $content
     */
    public function __construct($phoneNumber, $content = '')
    {
        //
        $this->phoneNumber = $phoneNumber;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     */
    public function handle()
    {
        Utils::sendSms($this->phoneNumber, $this->content);
		//Utils::sendBulkSms($this->phoneNumber, $this->content);
    }
}
