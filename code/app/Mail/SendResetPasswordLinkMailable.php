<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendResetPasswordLinkMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $fullname;
    public $username;
    public $password;

    /**
     * Create a new message instance.
     *
     * @param $token
     * @param $email
     * @param $createdTime
     */
    public function __construct($fullname, $username, $password)
    {
        $this->fullname = $fullname;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $verifyUrl = route('front_end_index');
        return $this->subject('Cấp lại mật khẩu')->view('emails.forgotpassword', ['verifyUrl' => $verifyUrl, 'fullname' => $this->fullname, 'username' => $this->username, 'password' => $this->password]);
    }
}
