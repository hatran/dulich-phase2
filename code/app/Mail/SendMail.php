<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $template;
    public $to;
    public $data;
    public $subject;

    /**
     * SendMail constructor.
     * @param $template
     * @param $to
     * @param $data
     * @param $subject
     */
    public function __construct($template, $to, $data ,$subject)
    {
        $this->template = $template;
        $this->to = $to;
        $this->data = $data;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->template, $this->data);
    }
}
