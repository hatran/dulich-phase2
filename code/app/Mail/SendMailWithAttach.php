<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailWithAttach extends Mailable
{
    use Queueable, SerializesModels;

    public $template;
    public $to;
    public $data;
    public $subject;
    public $attach;

    /**
     * SendMail constructor.
     * @param $template
     * @param $to
     * @param $data
     * @param $subject
     */
    public function __construct($template, $to, $data, $subject, $attach)
    {
        $this->template = $template;
        $this->to = $to;
        $this->data = $data;
        $this->subject = $subject;
        $this->attach = $attach;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->template, $this->data)
                    ->attach($this->attach['realpath'],
                    [
                        'as' => $this->attach['as'],
                        'mime' => $this->attach['mime'],
                    ]);
    }
}
