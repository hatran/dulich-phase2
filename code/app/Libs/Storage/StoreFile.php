<?php

namespace App\Libs\Storage;


use Illuminate\Support\Str;

class StoreFile implements IStoreFile
{
    protected $fileUrl;

    protected $storePath;
    protected $collectionPath;
    protected $tmpPath;

    /**
     * @var string
     */
    protected $prefix;
    protected $userFolderPrefix;

    /**
     * @var File
     */
    protected $fileInfo;

    /**
     * StoreFile constructor.
     * @param \SplFileInfo|string $sourceFile
     * @param string $prefix
     */
    public function __construct($sourceFile, $prefix = 'file', $userFolderPrefix = 'user_')
    {
        $this->fileUrl = config('vita.file_url');
        $this->storePath = config('vita.file_path');
        $this->collectionPath = self::concatDirectories($this->storePath, 'collection');
        $this->tmpPath = self::concatDirectories($this->collectionPath, 'tmp');

        $this->fileInfo = $this->checkSourceFile($sourceFile);
        $this->prefix = $prefix;
        $this->userFolderPrefix = $userFolderPrefix;
    }

    /**
     * @param string|\SplFileInfo $sourceFile
     * @return File
     * @throws \Exception
     */
    private function checkSourceFile($sourceFile)
    {
        if (is_string($sourceFile)) {
            if (!file_exists($sourceFile)) {
                throw new \Exception(trans('error.file_not_found'));
            }

            return new File($sourceFile);
        }

        if (!is_a($sourceFile, \SplFileInfo::class)) {
            throw new \Exception(trans('error.file_not_found'));
        }
        return new File($sourceFile->getRealPath());
    }

    public function collectionPath($time = 'now')
    {
        $date = new \DateTime($time, new \DateTimeZone('UTC'));
        return self::concatDirectories($this->collectionPath, $date->format('Y'), $date->format('m'), $date->format('d'));
    }

    public function userFolder($userId)
    {
        return $this->userFolderPrefix . $userId;
    }

    public function userPath($userId, $relativePath = '')
    {
        if (empty($relativePath)) {
            return self::concatDirectories($this->storePath, $this->userFolder($userId));
        }
        return self::concatDirectories($this->storePath, $this->userFolder($userId), $relativePath);
    }

    protected function autoFilename()
    {
        $extension = $this->fileInfo->guessExtension();
        if (empty($extension)) {
            $extension = $this->fileInfo->getExtension();
        }
        return self::randomizeFilename($this->prefix, $extension);
    }

    public function moveRelative($targetDirectory, $name = null)
    {
        $this->move(self::concatDirectories($this->storePath, $targetDirectory), $name);
    }

    public function move($targetDirectory, $name = null)
    {
        self::checkDirectory($targetDirectory);

        if (empty($name)) {
            $name = $this->autoFilename();
        }

        $this->fileInfo = $this->fileInfo->move($targetDirectory, $name);
    }

    public function copyRelative($targetDirectory, $name = null)
    {
        $this->copy(self::concatDirectories($this->storePath, $targetDirectory), $name);
    }

    public function copy($targetDirectory, $name = null)
    {
        self::checkDirectory($targetDirectory);

        if (empty($name)) {
            $name = $this->autoFilename();
        }

        $this->fileInfo = $this->fileInfo->copy($targetDirectory, $name);
    }

    public function duplicateRelative($targetDirectory, $name = null)
    {
        return $this->duplicate(self::concatDirectories($this->storePath, $targetDirectory), $name);
    }

    public function duplicate($targetDirectory, $name = null)
    {
        self::checkDirectory($targetDirectory);

        if (empty($name)) {
            $name = $this->autoFilename();
        }

        $targetFileInfo = $this->fileInfo->copy($targetDirectory, $name);

        $clonedStoreFile = clone $this;
        $clonedStoreFile->fileInfo = $targetFileInfo;

        return $clonedStoreFile;
    }

    public function moveToTmp()
    {
        $this->move($this->tmpPath);
    }

    public function moveToCollection($time = 'now', $name = null)
    {
        $this->move($this->collectionPath($time), $name);
    }

    public function moveToUser($userId, $relativePath = '')
    {
        $this->move($this->userPath($userId, $relativePath));
    }

    public function getUrl()
    {
        return self::urlSeparator(self::concatDirectories($this->fileUrl, $this->getRelativePath()));
    }

    public function getRelativeUrl()
    {
        return self::urlSeparator($this->getRelativePath());
    }

    public function getRelativePath()
    {
        return trim(str_replace($this->storePath, '', $this->fileInfo->getRealPath()), DIRECTORY_SEPARATOR);
    }

    public function getRealPath()
    {
        return $this->fileInfo->getRealPath();
    }

    #region Static
    public static function checkDirectory($directory)
    {
        if (self::containBackDirectory($directory)) {
            throw new \Exception(trans('error.directory_not_allowed') . ' (' . $directory . ')');
        }
        if (!is_dir($directory)) {
            if (false === @mkdir($directory, 0777, true) && !is_dir($directory)) {
                throw new \Exception(trans('error.directory_not_found') . ' (' . $directory . ')');
            }
        }
        if (!is_writable($directory)) {
            throw new \Exception(trans('error.directory_not_writable') . ' (' . $directory . ')');
        }
    }

    public static function randomizeFilename($prefix = null, $extension = null, $needTime = true, $needUnique = true, $moreUnique = true)
    {
        return self::format('{0}{1}{2}{3}',
            empty($prefix) ? '' : $prefix . '_',
            $needTime ? time() . '_' : '',
            $needUnique ? uniqid('', $moreUnique) : '',
            empty($extension) ? '' : '.' . $extension
        );
    }

    public static function urlSeparator($path)
    {
        return str_replace('\\', '/', $path);
    }

    public static function dirSeparator($path)
    {
        return str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
    }

    public static function containBackDirectory($path)
    {
        return Str::startsWith('..\\', $path)
            || Str::contains('\\..\\', $path)
            || Str::startsWith('../', $path)
            || Str::contains('/../', $path);
    }

    public static function concatDirectories()
    {
        $args = func_get_args();
        return implode(DIRECTORY_SEPARATOR, $args);
    }

    public static function delete($url)
    {
        $file = self::concatDirectories(config('vita.file_path'), str_replace(config('vita.file_url'), '', $url));
        if (file_exists($file)) {
            return @unlink($file);
        }

        return false;
    }

    public static function format($value)
    {
        $args = func_get_args();
        return preg_replace_callback('/\{(\d+)\}/',
            function ($match) use ($args) {
                // might want to add more error handling here...
                return $args[$match[1] + 1];
            },
            $value
        );
    }

    public function getFileInfo()
    {
        return $this->fileInfo;
    }
    #endregion
}
