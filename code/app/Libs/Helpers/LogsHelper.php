<?php

namespace App\Libs\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class LogsHelper
{
    /**
     * Write custom logs
     *
     * @param $text
     */
    public static function wr ($text)
    {
        File::append(storage_path() . '/logs/custom.log', Carbon::now() . ' ' . print_r($text, true) . PHP_EOL);
    }

    /**
     * Write log to custom file name
     *
     * @param string $fileName
     * @param $text
     */
    public static function trackByFile ($fileName = 'custom', $text)
    {
        File::append(storage_path() . '/logs/' . $fileName . '_' . Carbon::now()->toDateString() . '.log', Carbon::now() . ' ' . print_r($text, true) . PHP_EOL);
    }

    public static function exceptionByFile ($fileName = 'custom', $exception, $data = '')
    {
        try {
            self::trackByFile($fileName, 'Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$data], true) . ')');
        } catch (\Exception $exception) {
            self::trackByFile('cannot_write_log_exception', $exception);
        }
    }
}
