<?php

namespace App\Libs\Helpers;


use App\Constants\FileConstants;
use App\Constants\QuizConstants;
use App\Constants\RankConstants;
use App\Providers\LogServiceProvider;

class Utils
{
    /**
     * Random string
     *
     * @param int $length
     * @param bool $includeChar
     * @return bool|string
     */
    public static function randomString ($length = 10, $includeChar = true)
    {
        $characters = '0123456789';

        if ($includeChar) {
            $characters .= 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        $randString = '';

        for ($i = 0; $i < $length; $i++) {
            if ($includeChar) {
                $randString .= $characters[rand(0, 10)];
            } else {
                $randString .= rand(0, 9);
            }
        }

        if (! $includeChar && strlen($randString) > $length) {
            $randString = substr($randString, 0, $length);
        }

        return $randString;
    }

    /**
     * Send sms
     *
     * @param $phoneNumber
     * @param $content
     * @return bool
     */
    public static function sendSms ($phoneNumber, $content)
    {
        if (empty($phoneNumber) || empty($content)) {
            return false;
        }

        // beauty phone number
        $phoneNumber = ltrim($phoneNumber, '84');
        $phoneNumber = ltrim($phoneNumber, '0');
        $phoneNumber = '84' . $phoneNumber;

        LogServiceProvider::createSmsHistory($phoneNumber, $content);
		/* version 2019 ---*/
		// link chính: http://203.190.170.43:9998/bulkapi?wsdl
		/** các link phụ
		*   http://203.190.170.41:8998/bulkapi?wsdl
        *   http://203.190.170.42:8998/bulkapi?wsdl
        *   http://125.235.4.202:8998/bulkapi?wsdl
		*/
		/*	version 2022
			Dành cho kết nối qua Internet:
			http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl
			https://ams.tinnhanthuonghieu.vn:8998/bulkapi?wsdl (Add thêm IP này vào whitelist: 10.60.106.251)
			Dành cho kết nối qua DCN:
			http://10.60.106.216:8009/bulkapi?wsdl
			https://10.60.106.216:8998/bulkapi?wsdl (Add thêm IP này vào whitelist: 10.60.106.251)

		*/
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_PORT           => "8009",
            CURLOPT_URL            => "http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:impl="http://impl.bulkSms.ws/">
									<soapenv:Header/>
									<soapenv:Body>
									<impl:wsCpMt>
								         <!--Optional:-->
								         <User>' . config('app.sms_user') . '</User>
								         <!--Optional:-->
								         <Password>' . config('app.sms_password') . '</Password>
								         <!--Optional:-->
								         <CPCode>HHDLVIETNAM</CPCode>
								         <!--Optional:-->
								         <RequestID>1</RequestID>
								         <!--Optional:-->
								         <UserID>' . $phoneNumber . '</UserID>
								         <!--Optional:-->
								         <ReceiverID>' . $phoneNumber . '</ReceiverID>
								         <!--Optional:-->
								         <ServiceID>HOI HDV</ServiceID>
								         <!--Optional:-->
								         <CommandCode>bulksms</CommandCode>
								         <!--Optional:-->
								         <Content>' . $content . '</Content>
								         <!--Optional:-->
								         <ContentType>0</ContentType>
								      </impl:wsCpMt>
									</soapenv:Body>
									</soapenv:Envelope>',
            CURLOPT_HTTPHEADER  => [
                "cache-control: no-cache",
                "content-type: text/xml",
                "postman-token: 9d02b266-cc1b-66c5-3fe8-4ba4ea4ff6e1"
            ],
        ]);
        $result = curl_exec($curl);
        if (curl_error($curl)) {
            LogsHelper::trackByFile('send_sms_fail', 'So dien thoai' . $phoneNumber . ' content ' . $content);
			
			$curl_errno= curl_errno($curl);
			LogsHelper::trackByFile('send_sms_fail-infor: ', curl_getinfo($curl, CURLINFO_HTTP_CODE));
			LogsHelper::trackByFile('send_sms_fail_error: ', curl_error($curl));
        }

        curl_close($curl);
    }

	public static function sendBulkSms($toPhoneNumber, $message) {		
		$SERVICE_URI = 'http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl';
				
		$USER = 'smsbrand_hhdlvietnam';
		$PASS = '123456a@';
		$CPCODE = 'HHDLVIETNAM';
		$ALIAS = 'HOI HDV';
		
		if (empty($toPhoneNumber) || empty($message)) {
            return false;
        }

        // beauty phone number
        $toPhoneNumber = ltrim($toPhoneNumber, '84');
        $toPhoneNumber = ltrim($toPhoneNumber, '0');
        $toPhoneNumber = '84' . $toPhoneNumber;
		try {
			$client = new SoapClient($SERVICE_URI);
			$params = array(    "User" => $USER,    "Password" => $PASS,    "CPCode" => $CPCODE,    "RequestID" => "1",    "UserID" => $toPhoneNumber,     "ReceiverID" => $toPhoneNumber,    "ServiceID" => $ALIAS,    "CommandCode" => "bulksms",    "Content" => $message,    "ContentType" => "0"     );
			$response = $client->__soapCall("wsCpMt", array($params));
		} catch (Exception $e) {
			LogsHelper::trackByFile('send_sms_fail', $toPhoneNumber . ' Error content: ' . $e->getMessage());
		}
		
		LogServiceProvider::createSmsHistory($phoneNumber, $message);
	}
	
    public static function validateDateFormat($date, $format = 'Y-m-d'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public static function getFile($path){
        $path = FileConstants::FOLDER_NAME . DIRECTORY_SEPARATOR . $path;
        return file_exists(public_path($path)) ? public_path($path) : '';
    }
    
    public static function formatDatetimeVietnam($date) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $timeStamp = strtotime($date);
        $weekday = date("l", $timeStamp);
        $weekday = strtolower($weekday);
        switch($weekday) {
            case 'monday':
                $weekday = 'Thứ hai';
                break;
            case 'tuesday':
                $weekday = 'Thứ ba';
                break;
            case 'wednesday':
                $weekday = 'Thứ tư';
                break;
            case 'thursday':
                $weekday = 'Thứ năm';
                break;
            case 'friday':
                $weekday = 'Thứ sáu';
                break;
            case 'saturday':
                $weekday = 'Thứ bảy';
                break;
            default:
                $weekday = 'Chủ nhật';
                break;
        }
        return $weekday . ', ' . date('d/m/Y', $timeStamp) . ' ' . date('H:i', $timeStamp);
    }

    /**
     * Highlight menu via request url
     *
     * @param $arrMenus
     * @return bool
     */
    public static function highlightMenu ($arrMenus)
    {
        foreach ($arrMenus as $menu) {
            if (request()->is($menu) || request()->is($menu . '/*')) {
                return true;
            }
        }

        return false;
    }

    public static function getPath ()
    {
        return request()->path();
    }

    public static function stripUnicode($str){
        if(!$str) return false;
           $unicode = array(
                'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
                'd'=>'đ',
                'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
                'i'=>'í|ì|ỉ|ĩ|ị',
                'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
                'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
                'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
        );
        foreach($unicode as $nonUnicode=>$uni) $str = preg_replace("/($uni)/i",$nonUnicode,$str);
        return $str;
    }

    public static function getYears() {
        $year = [];
        for ($i = 2018; $i <= 2099; $i++) {
            $year[] = $i;
        }
        return $year;
    }

    public static function count_words($str){
        $str = str_replace(" words", "", $str);
        return (int) $str;
    }

    public static function getBaseAndSubjectKnowledge($rankId)
    {
        $quizRanks = [
            RankConstants::HDV_QT_5_STAR => [
                QuizConstants::$total_base_knowledge['5_STAR_INTERNATIONAL'],
                QuizConstants::$total_subject_knowledge['5_STAR_INTERNATIONAL'],
            ],
            RankConstants::HDV_QT_4_STAR => [
                QuizConstants::$total_base_knowledge['4_STAR_INTERNATIONAL'],
                QuizConstants::$total_subject_knowledge['4_STAR_INTERNATIONAL'],
            ],
            RankConstants::HDV_QT_3_STAR => [
                QuizConstants::$total_base_knowledge['3_STAR_INTERNATIONAL'],
                QuizConstants::$total_subject_knowledge['3_STAR_INTERNATIONAL'],
            ],
            RankConstants::HDV_ND_5_STAR => [
                QuizConstants::$total_base_knowledge['5_STAR_LOCAL'],
                QuizConstants::$total_subject_knowledge['5_STAR_LOCAL'],
            ],
            RankConstants::HDV_ND_4_STAR => [
                QuizConstants::$total_base_knowledge['4_STAR_LOCAL'],
                QuizConstants::$total_subject_knowledge['4_STAR_LOCAL'],
            ],
            RankConstants::HDV_ND_3_STAR => [
                QuizConstants::$total_base_knowledge['3_STAR_LOCAL'],
                QuizConstants::$total_subject_knowledge['3_STAR_LOCAL'],
            ],
        ];

        return $quizRanks[$rankId] ?? [0, 0];
    }
}
