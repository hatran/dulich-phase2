<?php

namespace App\Libs\Helpers;


class MemberRegisterHelper
{
    public static function getValueInputView($inputName, $objMember)
    {
        if (!empty(old($inputName))) {
            $inputValue = old($inputName);
        } elseif (!empty($objMember->{$inputName})) {
            $inputValue = $objMember->{$inputName};
        } else {
            $inputValue = '';
        }

        return $inputValue;
    }

    public static function getValueInputHasRelationView($inputName, $relationName, $objMember)
    {
        if (!empty(old($inputName))) {
            $inputValue = old($inputName);
        } elseif (!empty($objMember->{$relationName})) {
            $inputValue = $objMember->{$relationName};
        } else {
            $inputValue = '';
        }

        return $inputValue;
    }

    public static function getValueDateTimeView($inputName, $objMember)
    {
        if (!empty(old($inputName))) {
            $inputValue = \Carbon\Carbon::createFromFormat('d/m/Y', old($inputName));
        } elseif (!empty($objMember->{$inputName})) {
            $inputValue = $objMember->{$inputName};
        } else {
            $inputValue = '';
        }
        return $inputValue;
    }

    public static function getBirthdayValueInputView($inputName, $format, $objMember)
    {
        if (!empty(old($inputName))) {
            $inputValue = old($inputName);
        } elseif (!empty($objMember->birthday)) {
            try {
                $inputValue = $objMember->birthday->format($format);
            } catch (Exception $exception) {
                $inputValue = '';
            }
        } else {
            $inputValue = '';
        }

        return $inputValue;
    }

    public static function getExpirationDate($objMember)
    {
        if (!empty(self::getValueDateTimeView('expirationDate', $objMember))) {
            try {
                $expirationDate = self::getValueDateTimeView('expirationDate', $objMember)->format('d/m/Y');
            } catch (Exception $exception) {
                $expirationDate = old('expirationDate');
            }
        } else {
            $expirationDate = '';
        }

        return $expirationDate;
    }

    public static function getDateIssued($objMember)
    {
        if (!empty(self::getValueDateTimeView('dateIssued', $objMember))) {
            try {
                $dateIssued = self::getValueDateTimeView('dateIssued', $objMember)->format('d/m/Y');
            } catch (Exception $exception) {
                $dateIssued = old('dateIssued');
            }
        } else {
            $dateIssued = '';
        }

        return $dateIssued;
    }
}
