<?php

namespace App\Libs\Mail;

use App\Models\FeePolicy;
use App\Models\Option;
use Illuminate\Support\Facades\Mail;

class MailHelper
{
    public static function queueSendTemplate($path, $params)
    {
        try {
            Mail::queue(new BaseMailable($path, $params));
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public static function queueSend($mailable)
    {
        try {
            Mail::queue($mailable);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public static function sendTemplate($path, $params)
    {
        try {
            Mail::send(new BaseMailable($path, $params));
            return true;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function send($mailable)
    {
        try {
            Mail::send($mailable);
            return true;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function getFeeMoney($email_id) {
        $key_email_fee = 'key_email_fee_'.$email_id;
        $fee_oprion = Option::where('code', '=', 'EMAIL_HOIPHI')->where('key', '=', $key_email_fee)->first();
        if ($fee_oprion) {
            $fee = FeePolicy::where('id', '=', $fee_oprion->value)->first();
            return self::currency_format($fee->money);
        }
        $fee = FeePolicy::whereNull('list_id')->first();

        return self::currency_format($fee->money);
    }
    /**
     *
     * Chuyển đổi chuỗi kí tự thành dạng slug dùng cho việc tạo friendly url.
     *
     * @access    public
     * @param    string
     * @return    string
     */
    public static function currency_format($number, $suffix = 'đ') {
        if (empty($number)) {
            return "0{$suffix}";
        }
        $number = str_replace($suffix, "", $number);
        if (strpos($number, ",") !== false) {
            return $number."{$suffix}";
        }elseif (strpos($number, ".") !== false) {
            return $number."{$suffix}";
        }else {
            if(is_numeric($number)) {
                return number_format($number, 0, ',', '.') . "{$suffix}";
            }else {
                $number = (int)$number;
                return $number."{$suffix}";
            }
        }
    }
}
