<?php

namespace App\Providers;

use App\Constants\MemberConstants;
use App\Libs\Helpers\LogsHelper;
use App\Models\File;
use App\Models\Banner;
use Illuminate\Support\ServiceProvider;
use Response;
use Validator;
use Carbon\Carbon;
use App\Libs\Helpers\Utils;
use App\Libs\Storage\StoreFile;
use App\Libs\Storage\StorePhoto;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Constants\BranchConstants;
use App\Constants\CommontConstants;
use App\Models\Page;


class BannerServiceProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const TITLE = 'title';
    const OPTION_CODE = 'option_code';
    const ID = 'id';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_VERIFIED = 'is_verified';
    const IS_DELETE = 'is_delete';
    const MEMBER_TYPE = 'member_type';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';


    public static function getAllPage ()
    {
        $objPage = Page::all();
        return $objPage;
    }

    public static function newsSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];

        if (!empty($arrConditions['name'])) {
            $arrSearchCondition = [['name', 'like', '%' . $arrConditions['name'] . '%']];
        }

        if ($arrConditions['page_code'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['page_code', $arrConditions['page_code']]]);
        }

        if ($arrConditions['status'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['status', $arrConditions['status']]]);
        }

        if ($arrConditions['start_time'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['start_time', '>=', $arrConditions['start_time'].' 00:00:00']]);
        }

        if ($arrConditions['end_time'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['end_time', '<=', $arrConditions['end_time'].' 23:59:59']]);
        }

        if ($arrConditions['created_at'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['created_at', '<=', $arrConditions['created_at'].' 23:59:59']]);
            $arrSearchCondition = array_merge($arrSearchCondition, [['created_at', '>=', $arrConditions['created_at'].' 00:00:00']]);
        }
        if(!$isCount){
            $objNew = Banner::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
        }else{
            $objNew = Banner::where($arrSearchCondition)->get();
        }

//print_r(json_decode(json_encode($objNew)));die;
        return $objNew;
    }

    public static function getDataById($id)
    {
        return Banner::where('id',$id)->first()->toArray();
    }

    public static function getBannerByPage ($page_code)
    {
        $objBanners = Banner::where(
            [
                'page_code'       => $page_code,
                'status'            => 2,
            ]
        )
            ->orderBy('created_at', 'des')
            ->get()
            ->toArray();
        $data = [];
        if(count($objBanners) > 0){
            foreach ($objBanners as $value) {

                if($value['start_time'] == '' && $value['end_time'] == ''){
                    if($page_code == 'HOME') {
                        $data['home'][] = $value;
                    }else{
                        $data = $value;
                        break;
                    }
                }

                if($value['start_time'] != '' && $value['end_time'] == ''){
                    if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s'))){
                        if($page_code == 'HOME') {
                            $data['home'][] = $value;
                        }else{
                            $data = $value;
                            break;
                        }
                    }
                }

                if($value['start_time'] == '' && $value['end_time'] != ''){
                    if(strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                        if($page_code == 'HOME') {
                            $data['home'][] = $value;
                        }else{
                            $data = $value;
                            break;
                        }
                    }
                }

                if($value['start_time'] != '' && $value['end_time'] != ''){
                    if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s')) && strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                        if($page_code == 'HOME') {
                            $data['home'][] = $value;
                        }else{
                            $data = $value;
                            break;
                        }
                    }
                }
            }
        }
        $home = [];
        if($page_code == 'HOME') {
            $home = self::getBannerHome();
        }else{
            $home = self::getBannerSlide();
        }
        return array_merge($home, $data);
    }

    public static function getBannerHome()
    {
        $arr = [/*'VPDD_HN', 'VPDD_HCM', 'VPDD_DN',*/'THONGTIN_PTTQ', 'THONGTIN_TP', 'THONGTIN_LH','THONGTIN_DD', 'THONGTIN_GT', 'THONGTIN_TDL'];
        $tt = ['THONGTIN_PTTQ', 'THONGTIN_TP', 'THONGTIN_LH','THONGTIN_DD', 'THONGTIN_GT', 'THONGTIN_TDL'];
        $count_hcm = $count_dn = 0;
        foreach($arr as $key => $result){
            $home[$result] = [];
            $objBanners = Banner::where(
                [
                    'page_code'       => $result,
                    'status'            => 2,
                ]
            )
                ->orderBy('created_at', 'des')
                ->get()
                ->toArray();
            if(count($objBanners) > 0){
                foreach ($objBanners as $value) {

                    if($value['start_time'] == '' && $value['end_time'] == ''){
                        if($result == 'VPDD_HN' || in_array($result, $tt)) {
                            $home[$result] = $value;
                            break;
                        }elseif($result == 'VPDD_HCM'){
                            $home['VPDD_HCM'][] = $value;
                            $count_hcm ++;
                            if($count_hcm == 2){
                                break;
                            }
                        }elseif($result == 'VPDD_DN'){
                            $home['VPDD_DN'][] = $value;
                            $count_dn ++;
                            if($count_dn == 2){
                                break;
                            }
                        }
                    }

                    if($value['start_time'] != '' && $value['end_time'] == ''){
                        if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s'))){
                            if($result == 'VPDD_HN' || in_array($result, $tt)) {
                                $home[$result] = $value;
                                break;
                            }elseif($result == 'VPDD_HCM'){
                                $home['VPDD_HCM'][] = $value;
                                $count_hcm ++;
                                if($count_hcm == 2){
                                    break;
                                }
                            }elseif($result == 'VPDD_DN'){
                                $home['VPDD_DN'][] = $value;
                                $count_dn ++;
                                if($count_dn == 2){
                                    break;
                                }
                            }
                        }
                    }

                    if($value['start_time'] == '' && $value['end_time'] != ''){
                        if(strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                            if($result == 'VPDD_HN' || in_array($result, $tt)) {
                                $home[$result] = $value;
                                break;
                            }elseif($result == 'VPDD_HCM'){
                                $home['VPDD_HCM'][] = $value;
                                $count_hcm ++;
                                if($count_hcm == 2){
                                    break;
                                }
                            }elseif($result == 'VPDD_DN'){
                                $home['VPDD_DN'][] = $value;
                                $count_dn ++;
                                if($count_dn == 2){
                                    break;
                                }
                            }
                        }
                    }

                    if($value['start_time'] != '' && $value['end_time'] != ''){
                        if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s')) && strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                            if($result == 'VPDD_HN' || in_array($result, $tt)) {
                                $home[$result] = $value;
                                break;
                            }elseif($result == 'VPDD_HCM'){
                                $home['VPDD_HCM'][] = $value;
                                $count_hcm ++;
                                if($count_hcm == 2){
                                    break;
                                }
                            }elseif($result == 'VPDD_DN'){
                                $home['VPDD_DN'][] = $value;
                                $count_dn ++;
                                if($count_dn == 2){
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $home;
    }
    public static function getBannerSlide()
    {
        $arr = ['SLIDE_LEFT', 'SLIDE_RIGHT'];
        $tt = ['SLIDE_LEFT', 'SLIDE_RIGHT'];
        foreach($arr as $key => $result){
            $home[$result] = [];
            $objBanners = Banner::where(
                [
                    'page_code'       => $result,
                    'status'            => 2,
                ]
            )
                ->orderBy('created_at', 'des')
                ->get()
                ->toArray();
            if(count($objBanners) > 0){
                foreach ($objBanners as $value) {

                    if($value['start_time'] == '' && $value['end_time'] == ''){
                        $home[$result] = $value;
                        break;
                    }

                    if($value['start_time'] != '' && $value['end_time'] == ''){
                        if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s'))){
                            $home[$result] = $value;
                            break;
                        }
                    }

                    if($value['start_time'] == '' && $value['end_time'] != ''){
                        if(strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                            $home[$result] = $value;
                            break;
                        }
                    }

                    if($value['start_time'] != '' && $value['end_time'] != ''){
                        if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s')) && strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                            $home[$result] = $value;
                            break;
                        }
                    }
                }
            }
        }
        return $home;
    }
    public static function getDeleteById($id)
    {
        return Banner::where('id',$id)->first();
    }

    public static function delete ($id)
    {
        $objNew = self::getDeleteById($id);
        if (empty($objNew)) {
            return false;
        }

        try {
            $objNew->delete();
//            DB::commit();
            return true;
        } catch (\Exception $exception) {
//            DB::rollBack();
            LogsHelper::trackByFile('delete_new_fail', 'Error when delete user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . $id);
            return false;
        }
    }



    public static function update ($arrUpdateData)
    {
        $id = $arrUpdateData['id'];
        if (empty($id)) {
            return false;
        }

        $objIntro = self::getNewById($id);

        if (!empty($arrUpdateData['option_code'])) {
            $objIntro->option_code = $arrUpdateData['option_code'];
        }

        if (!empty($arrUpdateData['title'])) {
            $objIntro->title = $arrUpdateData['title'];
        }

        if (!empty($arrUpdateData['start_time'])) {
            $objIntro->start_time = $arrUpdateData['start_time'];
        }

        if (isset($arrUpdateData['end_time']) && $arrUpdateData['end_time'] != '') {
            $objIntro->end_time = $arrUpdateData['end_time'];
        }

        if (isset($arrUpdateData['status'])) {
            $objIntro->status = $arrUpdateData['status'];
        }

        if (!empty($arrUpdateData['content'])) {
            $objIntro->content = $arrUpdateData['content'];
        }
        if (!empty($arrUpdateData['content2'])) {
            $objIntro->content2 = $arrUpdateData['content2'];
        }

        if (!empty($arrUpdateData['password'])) {
            $objIntro->password = $arrUpdateData['password'];
        }

        try {
            $objIntro->save();
            DB::commit();
            return $objIntro;
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_intro_fail', 'Error when update user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$arrUpdateData], true) . ')');
            return null;
        }
    }


}
