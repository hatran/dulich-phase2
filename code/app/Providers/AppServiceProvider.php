<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Branches\BranchesInterface',
            'App\Repositories\Branches\BranchesRepository'
        );
        $this->app->bind(
            'App\Repositories\ClubOfHead\ClubOfHeadInterface',
            'App\Repositories\ClubOfHead\ClubOfHeadRepository'
        );
        $this->app->bind(
            'App\Repositories\ClubOfBranch\ClubOfBranchInterface',
            'App\Repositories\ClubOfBranch\ClubOfBranchRepository'
        );
    }
}
