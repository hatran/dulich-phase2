<?php

namespace App\Providers;

use App\Models\EvaluationCriteria;

class EvaluationCriteriaProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;

    public static function evaluationsSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];
        if (!empty($arrConditions['name'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['name', 'like', '%' . $arrConditions['name'] . '%']]);
        }

        if (!empty($arrConditions['status'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', $arrConditions['status']]]);
        }
        $objEvaluation = EvaluationCriteria::query()->where($arrSearchCondition)->whereNull('is_deleted');
        if (! $isCount) {
            return $objEvaluation->orderBy('created_at', 'des')->paginate($limit);
        } else {
            return $objEvaluation->get();
        }
    }

    public static function getDataById($id)
    {
        return EvaluationCriteria::query()->where('id',$id)->whereNull('is_deleted')->first();
    }

    public static function getStatusZeroById($id)
    {
        return EvaluationCriteria::where('id', $id)->where('status', 0)->whereNull('is_deleted')->first();
    }

    public static function getEvaluationCriteria()
    {
        return EvaluationCriteria::select('code', 'name')->where('status', 1)->whereNull('is_deleted')->pluck('name', 'code')->all();
    }

    public static function getPercentage()
    {
        return EvaluationCriteria::select('code', 'percentage')->where('status', 1)->whereNull('is_deleted')->pluck('percentage', 'code')->all();
    }

}
