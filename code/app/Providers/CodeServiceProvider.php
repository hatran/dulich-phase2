<?php
/**
 * Created by PhpStorm.
 * User: daiduong47
 * Date: 18/01/2018
 * Time: 17:28 PM
 */

namespace App\Providers;


use App\Libs\Helpers\LogsHelper;
use App\Models\Code;

class CodeServiceProvider extends AppServiceProvider
{
    public static function getCurrentCodeByPrefix ($prefix)
    {
        if (empty($prefix)) {
            return null;
        }

        $objCode = Code::where('prefix', $prefix)->first();
        if (!empty($objCode) && !empty($objCode->current_code)) {
            return $objCode->current_code;
        } else {
            return null;
        }
    }

    public static function updateOrCreate ($prefix, $currentCode)
    {
		
        try {
            Code::updateOrCreate(
                ['prefix'       => $prefix],
                ['current_code' => $currentCode]
            );
        } catch (\Exception $exception) {
            
            LogsHelper::trackByFile('create_code_fail', 'Error when create code, Data code: ' . print_r(['prefix' => $prefix, 'current_code' => $currentCode] ,true));
        }
    }
}
