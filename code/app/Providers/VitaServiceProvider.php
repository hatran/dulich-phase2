<?php

namespace App\Providers;


use App\Models\Vita;

class VitaServiceProvider extends AppServiceProvider
{
    public static function getAllExecutiveBoardMember ()
    {
        return Vita::where('status', 1)
            ->where('type', 1)
            ->where('deptec', 1)
            ->get();
    }
}
