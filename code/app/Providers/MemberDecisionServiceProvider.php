<?php

namespace App\Providers;

use App\Constants\MemberConstants;
use App\Libs\Helpers\LogsHelper;
use App\Models\Branches;
use App\Models\File;
use App\Models\MemberDecision;
use App\Repositories\Branches\BranchesRepository;
use Illuminate\Support\ServiceProvider;
use Response;
use Validator;
use Carbon\Carbon;
use App\Libs\Helpers\Utils;
use App\Libs\Storage\StoreFile;
use App\Libs\Storage\StorePhoto;
use App\Models\AchievementFile;
use App\Models\Education;
use App\Models\Element;
use App\Models\LanguageSkill;
use App\Models\MajorSkill;
use App\Models\Member;
use App\Models\MemberAvatar;
//use App\Models\MemberPayments;
use App\Models\MemberPaymentsYearly;
use App\Models\Work;
use App\Models\WorkHistory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Mail;
use DomPDF;
use Auth;
use App\Constants\BranchConstants;
use App\Providers\MemberServiceProvider;
use App\Providers\UserServiceProvider;

class MemberDecisionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const MEMBER_ID = 'memberId';
    const CREATED_AT = 'createdAt';
    const NUMBER_DECISIVE = 'number_decisive';
    const THE_SIGNER = 'leaderId';
    const SIGN_DATE = 'sign_date';
    const FILE_ID = 'fileId';
    const IS_PRINT = 'is_print';
    const IS_SIGNED = 'is_signed';
    const IS_DELETE = 'is_delete';
    const D_M_Y_FORMAT = 'd/m/Y';

    const ORIGINAL_NAME = 'originalName';
    const ABSOLUTE_PATH = 'absPath';
    const RELATIVE_URL = 'relativeUrl';
    const STATUS = 'status';

    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_VERIFIED = 'is_verified';
    const IS_FEE = 'is_fee';
    const MEMBER_TYPE = 'member_type';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';

    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function getMemberDecisionByMemberId($memberId)
    {

        if (empty($memberId)) {
            $objectMemberPayment = null;
        } else {
            $objectMemberPayment = MemberDecisionServiceProvider:: _getDecisionByMemberId($memberId);
        }

        return $objectMemberPayment;
    }

    public static function getAllMembersByPlaceType($limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE, $isCount = false)
    {
        if (!$isCount) {
            $objMember = self::getAllMembersByPlaceTypeQuery()->paginate($limit);
        } else {
            $objMember = self::getAllMembersByPlaceTypeQuery()->get();
        }


        return $objMember;
    }

    private static function getAllMembersByPlaceTypeQuery()
    {
        $query = Member::where(MemberServiceProvider::PHONE_VERIFIED, 1)
            ->where(MemberServiceProvider::ACCEPT_TERMS_AND_POLICIES, 1);

        $statusDict = [];
        $typeOfPlaces = [];
        if (UserServiceProvider::isMemberShipCardIssuer()) {
            $statusDict = MemberConstants::$file_membership_card_issuer_const;

            // update logic 20180614
            $branches = new BranchesRepository(new Branches());
            $typeOfPlaces = $branches->getAllByParentId(null);

            // $typeOfPlaces = array_merge(BranchConstants::$hn_typeOfPlace, BranchConstants::$dn_typeOfPlace, BranchConstants::$hcm_typeOfPlace);
        }

        return $query->whereIn(self::STATUS, self::extractStatusValues($statusDict))
            ->whereIn(self::PROVINCE_CODE, $typeOfPlaces)
            ->orderBy(MemberServiceProvider::CREATED_AT, 'des');
    }

    private static function extractStatusValues($statusDict)
    {
        return collect($statusDict)->keys()->all();
    }

    /***
     * @param $memberId
     * @return null
     */
    public static function _getDecisionByMemberId($memberId)
    {
        $memberDecision = MemberDecision::where('memberId', $memberId)
            ->where('is_delete', 0)
            ->first();

        return empty($memberDecision) ? null : $memberDecision;
    }


    public static function updateMemberWhenDoReport($memberId, $arrInfoUpdate)
    {
        try {

            $memberDecision = MemberDecision::where('memberId', $memberId)
                ->first();

            if (empty($memberDecision)) {
                $memberDecision = new MemberDecision();
                $memberDecision->memberId = $memberId;
                if (isset($arrInfoUpdate[MemberDecisionServiceProvider::IS_PRINT])) {
                    $memberDecision->is_print = $arrInfoUpdate[MemberDecisionServiceProvider::IS_PRINT];
                }
                $memberDecision->number_decisive = ' ';
                $memberDecision->is_delete = 0;
                $memberDecision->number_decisive = null;
                $memberDecision->leaderId = null;
                $memberDecision->sign_date = null;
                $memberDecision->fileId = null;
                $memberDecision->is_signed = null;

                $memberDecision->save();
                MemberDecision::created($memberDecision);
            } else {
                if ($memberDecision->is_delete == 1) {
                    if (isset($arrInfoUpdate[MemberDecisionServiceProvider::IS_PRINT])) {
                        $memberDecision->is_print = $arrInfoUpdate[MemberDecisionServiceProvider::IS_PRINT];
                    }
                    $memberDecision->number_decisive = ' ';
                    $memberDecision->is_delete = 0;
                    $memberDecision->number_decisive = null;
                    $memberDecision->leaderId = null;
                    $memberDecision->sign_date = null;
                    $memberDecision->fileId = null;
                    $memberDecision->is_signed = null;
                    $memberDecision->save();
                }
            }

            return $memberDecision;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_decision_when_do_report_fail', 'Error when update member decision do report (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }


    public static function updateMemberDecision($memberId, $request, $isUpdateFile)
    {
        DB::beginTransaction();
        try {
            // if ($isUpdateFile) {
            //     $file = $request->file('file');
            //     $fileUpload = self::_uploadFile($file, $request);
            //     $fileId = $fileUpload->id;
            // } else {
            //     $fileId = null;
            // }
            $fileId = null;
            if (!empty($request->input('date_sign'))) {
                $time = strtotime(Carbon::createFromFormat('d/m/Y', $request->input('date_sign'))->toDateString());
                $date_sign = date('Y-m-d', $time);
            }
            else {
                $date_sign = null;
            }
            $decicionObject = [
                'memberId' => $memberId,
                'number_decisive' => $request->input('number_decision').'/QĐ-HHDVDLVN',
                'leaderId' => $request->input('leader_sign'),
                'sign_date' => $date_sign,
                'fileId' => $fileId,
                'is_signed' => '1',
                'is_delete' => '0',
                'is_print' => '1',
            ];

            $memberDetail = MemberDecision::where('memberId', $memberId)->first();
            if($memberDetail) {
                $memberDecision = MemberDecision::updateOrCreate(['memberId' => $memberId], $decicionObject);
            } else {
                $memberDecision = MemberDecision::create($decicionObject);
            }
            // update member status
            MemberServiceProvider::updateMemberStatus($memberId, MemberConstants::CODE_PROVIDING_WAITING);
            DB::commit();
            return $memberDecision;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_decision_when_do_report_fail', 'Error when update member decision do report (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    /***
     * @param $file
     * @return null|void
     */
    private static function _uploadFile($file, $request)
    {
        try {
            if (empty($file)) {
                return;
            }

            $id = $request->input('file-id');
            $objFiles = File::find($id);


            $photoDir = 'files/decision/' . date('Y') . '/' . date('m');
            $extension = $file->getClientOriginalExtension(); // getting excel extension
            $photoName = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
            $storeFile = $file->storeAs($photoDir, $photoName, 'public_folder');

            if (!empty($objFiles)) {

                $objFiles->originalName = $photoName;
                $objFiles->absPath = $storeFile;
                $objFiles->relativeUrl = $storeFile;
                $objFiles->mime = $file->getMimeType();
                $objFiles->size = $file->getClientSize();
                $objFiles->save();

                return $objFiles;
            } else {

                $fileData = File::create([
                    MemberDecisionServiceProvider::ORIGINAL_NAME => $photoName,
                    MemberDecisionServiceProvider::ABSOLUTE_PATH => $storeFile,
                    MemberDecisionServiceProvider::RELATIVE_URL => $storeFile,
                    'mime' => $file->getMimeType(),
                    'size' => $file->getClientSize(),
                    MemberServiceProvider::STATUS => 1,
                ]);

                return $fileData;
            }

        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_decision_when_do_report_fail', 'Error when update member decision do report (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$file], true) . ')');
            return null;
        }
    }


    public static function checkUpdateFile($request)
    {

        if (empty($request)) {
            return false;
        }

        $id       = $request->input('file-id');
        $filename = $request->input('file_name');

        if ($id != -1) {
            $objFiles = File::find($id);
            if (!empty($objFiles)) {
                if (strcmp($objFiles->originalName, $filename) == 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public static function softDeletebyMemberId($memberId)
    {
        try {
            $decision = MemberDecisionServiceProvider::getMemberDecisionByMemberId($memberId);
            if (!(empty($decision))) {
                $decision->is_delete = 1;
                $decision->save();
                //$decision->delete();
            }
            // update member status
            MemberServiceProvider::updateMemberStatus($memberId, MemberConstants::SIGNING_WAITING);
            return $decision;
        } catch
        (\Exception $exception) {

            LogsHelper::trackByFile('update_member_decision_when_do_report_fail', 'Error when update member decision do report (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    public static function getFileById($fileId)
    {

        if (empty($fileId))
            return null;
        else
            $objFile = File::find($fileId);
        return $objFile;
    }

    public static function decisionSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $query = MemberPaymentsYearly::rightJoin('members', 'members.id', '=', 'member_payments_yearly.memberId')
                ->where(self::PHONE_VERIFIED, 1)
                ->where(self::ACCEPT_TERMS_AND_POLICIES, 1)
                ->where('member_payments_yearly.flag', '0')
                ->where('member_payments_yearly.is_delete', '0')
                ->with(['decision' => function($query){
                    $query->where('is_delete', 0); //you may use any condition here or manual select operation
                    $query->with('file');
                    $query->select(); //select operation
                }]);

        $arrKeyConditions = array_keys($arrConditions);

        if (in_array(self::STATUS, $arrKeyConditions) && !empty($arrConditions[self::STATUS])) {
            // search theo trạng thái được lựa chọn
            $query->where('members.status' , $arrConditions[self::STATUS]);
        } else {
            // list  all hồ sơ có status = 5 chờ ký quyết đinh và status = 6 chờ cấp mã thẻ hội viên
            $query->whereIn('members.status' , [MemberConstants::SIGNING_WAITING, MemberConstants::CODE_PROVIDING_WAITING]);
        }

        if(!empty($arrConditions[self::PROVINCE_CODE])) {
            MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
        } 
       
        if (in_array(self::PROVINCE_TYPE, array_keys($arrConditions)) && !empty($arrConditions[self::PROVINCE_TYPE])) {
            //$arrBranchesIds = Offices::getProvinceCodeByProvinceType($arrConditions[self::PROVINCE_TYPE]);
            $query->whereIn(self::PROVINCE_CODE, $arrConditions[self::PROVINCE_TYPE]);
        }   

        // search by ngày tạo hồ sơ
        MemberServiceProvider::searchByCreatedAtMember($arrConditions, $query);
        
        // search by common
        MemberServiceProvider::searchByCommon($arrConditions, $query);

        $query->where('members.is_delete' , null);

        $query = ListMemberServiceProvider::filterBranchIdByRole($query);

        $query->orderBy('members.' . self::FILE_CODE , 'asc');

        if (!$isCount){
            $objDecision = $query->paginate($limit);
        } else {
            $objDecision = $query->get();
        }
        return $objDecision;
    }
    public static function getDataByMemberID($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false, $memberID)
    {
        $query = MemberPaymentsYearly::rightJoin('members', 'members.id', '=', 'member_payments_yearly.memberId')
            ->where(self::PHONE_VERIFIED, 1)
            ->where('memberId', $memberID)
            ->where(self::ACCEPT_TERMS_AND_POLICIES, 1)
            ->with(['decision' => function($query){
                $query->where('is_delete', 0); //you may use any condition here or manual select operation
                $query->with('file');
                $query->select(); //select operation
            }]);

        $arrKeyConditions = array_keys($arrConditions);

        if (in_array(self::STATUS, $arrKeyConditions) && !empty($arrConditions[self::STATUS])) {
            // search theo trạng thái được lựa chọn
            $query->where('members.status' , $arrConditions[self::STATUS]);
        } else {
            // list  all hồ sơ có status = 5 chờ ký quyết đinh và status = 6 chờ cấp mã thẻ hội viên
            $query->whereIn('members.status' , [MemberConstants::SIGNING_WAITING, MemberConstants::CODE_PROVIDING_WAITING]);
        }

        // search by ngày tạo hồ sơ
        MemberServiceProvider::searchByCreatedAtMember($arrConditions, $query);

        // search by văn phòng đại diện
        MemberServiceProvider::searchByVpd($arrConditions, $query);

        //search by chi hội
        MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
        // search by common
        MemberServiceProvider::searchByCommon($arrConditions, $query);

        $query->where('members.is_delete' , null);
        $query->orderBy('members.' . self::FILE_CODE , 'asc');
        if(!$isCount){
            $objDecision = $query->paginate($limit);
        }else {
            $objDecision = $query->get();
        }
        return $objDecision;
    }
    public static function updateMember($memberId)
    {
        Member::where('id', $memberId)->update([
            'is_signed' => 1,
        ]);
    }
}
