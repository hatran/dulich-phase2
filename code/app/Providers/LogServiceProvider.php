<?php

namespace App\Providers;


use App\Libs\Helpers\LogsHelper;
use App\Models\History;
use App\Models\HistoryEmailSMS;
use App\Models\Permission;
use Illuminate\Support\Facades\Auth;

class LogServiceProvider
{
    const LOG_EXCLUDES = [];

    public static function createSystemHistory ($functionName = null, $data = [])
    {
        if (empty(auth()->user()->id)) {
            return false;
        }

        if (self::excludeRouter()) {
            return false;
        }

        $model = new History();
        $model->user_id = auth()->user()->id;
        $model->router = request()->path();
        $model->router_key = request()->route()->getName();
        $model->ip = request()->ip();

        if (!empty($data)) {
            $model->description = json_encode($data, JSON_UNESCAPED_UNICODE);
        } elseif (!empty(request()->input())) {
            $model->description = json_encode(request()->input());
        } else {
            $model->description = trans('history.description.view_action');
        }

        $objPermission = Permission::where('route_name', request()->route()->getName())->first();
        $model->function_name = !empty($objPermission) ? $objPermission->name : $functionName;
        $model->permission_id = !empty($objPermission) ? $objPermission->id : 0;

        try {
            $model->save();
        } catch (\Exception $exception) {
            LogsHelper::exceptionByFile('write_system_log_fail', $exception, request()->all());
        }

    }

    public static function createEmailHistory ($email_sms_to, $email_sms_name, $subject, $content)
    {
        $model = new HistoryEmailSMS();
        $model->type = HistoryEmailSMS::EMAIL_LOG_TYPE;
        $model->email_sms_to = array_get($email_sms_to, '0.address', '');
        $model->email_sms_name = $email_sms_name;
        $model->subject = $subject;
        $model->content = $content;
       	if (Auth::check()) {
       		$model->user_id = auth()->user()->id;
       	}
        $model->ip = request()->ip();
        try {
            $model->save();
        } catch (\Exception $exception) {
            LogsHelper::exceptionByFile('write_email_history_fail', $exception, [
                'email_sms_to'   => $email_sms_to,
                'email_sms_name' => $email_sms_name,
                'subject'    => $subject,
                'content'    => $content
            ]);
        }
    }

    public static function createSmsHistory ($email_sms_to, $content, $email_sms_name = '', $subject = '')
    {
        $model = new HistoryEmailSMS();
        $model->type = HistoryEmailSMS::SMS_LOG_TYPE;
        $model->email_sms_to = $email_sms_to;
        $model->email_sms_name = $email_sms_name;
        $model->subject = $subject;
        $model->content = $content;

        if (empty(auth()->user()->id)) {
            $userId = 0; // system
        } else {
            $userId = auth()->user()->id;
        }

        $model->user_id = $userId;
        $model->ip = request()->ip();
        try {
            $model->save();
        } catch (\Exception $exception) {
            LogsHelper::exceptionByFile('write_sms_history_fail', $exception, [
                'email_sms_to'   => $email_sms_to,
                'email_sms_name' => $email_sms_name,
                'subject'    => $subject,
                'content'    => $content
            ]);
        }
    }

    public static function excludeRouter()
    {
        foreach (self::LOG_EXCLUDES as $router) {
            if (strpos(request()->path(), $router)) {
                return true;
            }
        }
        return false;
    }
}
