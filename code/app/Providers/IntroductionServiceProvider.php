<?php

namespace App\Providers;

use App\Constants\MemberConstants;
use App\Libs\Helpers\LogsHelper;
use App\Models\File;
use App\Models\MemberDecision;
use App\Models\NewOption;
use Illuminate\Support\ServiceProvider;
use Response;
use Validator;
use Carbon\Carbon;
use App\Libs\Helpers\Utils;
use App\Libs\Storage\StoreFile;
use App\Libs\Storage\StorePhoto;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Constants\BranchConstants;
use App\Constants\CommontConstants;
use App\Models\Option;

class IntroductionServiceProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const TITLE = 'title';
    const OPTION_CODE = 'option_code';
    const ID = 'id';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_VERIFIED = 'is_verified';
    const IS_DELETE = 'is_delete';
    const MEMBER_TYPE = 'member_type';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';


    public static function getAllOption ($options)
    {
        $objOption = Option::where('key', $options)
//            ->orderBy('id', 'des')
            ->get();

        return $objOption;
    }
    public static function getNewsByOption ($option_code)
    {
        $objNews = NewOption::where(
            [
                'option_code'       => $option_code,
                'status'            => 1,
            ]
        )
//            ->whereDate('end_time', '>=', date('Y-m-d H:i:s'))
            ->orderBy('created_at', 'des')
            ->get()
            ->toArray();
        $data = [];
        if(count($objNews) > 0){
            foreach ($objNews as $value) {

                if($value['start_time'] == '' && $value['end_time'] == ''){
                    $data = $value;
                    break;
                }

                if($value['start_time'] != '' && $value['end_time'] == ''){
                    if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s'))){
                        $data = $value;
                        break;
                    }
                }

                if($value['start_time'] == '' && $value['end_time'] != ''){
                    if(strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                        $data = $value;
                        break;
                    }
                }

                if($value['start_time'] != '' && $value['end_time'] != ''){
                    if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s')) && strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                        $data = $value;
                        break;
                    }
                }
            }
        }
        return $data;
    }

    public static function newsSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];

        if (!empty($arrConditions['title'])) {
            $arrSearchCondition = [['title', 'like', '%' . $arrConditions['title'] . '%']];
        }

        if ($arrConditions['created_at'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['created_at', '<=', $arrConditions['created_at'].' 23:59:59']]);
            $arrSearchCondition = array_merge($arrSearchCondition, [['created_at', '>=', $arrConditions['created_at'].' 00:00:00']]);
        }

        $objNew = NewOption::where($arrSearchCondition);

        if ($arrConditions['status'] != '') {
            $objNew = $objNew->where('status', $arrConditions['status']);
        }

        if(!$isCount){
            $objNew = $objNew->whereIn('option_code', $arrConditions['option_code'])->orderBy('created_at', 'des')->paginate($limit);
        }else{
            $objNew = $objNew->whereIn('option_code', $arrConditions['option_code'])->get();
        }

//print_r(json_decode(json_encode($objNew)));die;
        return $objNew;
    }

    public static function delete ($id)
    {
        $objNew = self::getNewById($id);
        if (empty($objNew)) {
            return false;
        }

        try {
            $objNew->delete();
//            DB::commit();
            return true;
        } catch (\Exception $exception) {
//            DB::rollBack();
            LogsHelper::trackByFile('delete_new_fail', 'Error when delete user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . $id);
            return false;
        }
    }

    public static function getNewById($id)
    {
        return NewOption::find($id);
    }

    public static function getNewByOptionCode($id, $status, $option_code)
    {
        //check add
        $arrSearchCondition = [];
        $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', $status]]);
        $arrSearchCondition = array_merge($arrSearchCondition, [['option_code','=', $option_code]]);
        if($id != 'null'){
            $arrSearchCondition = array_merge($arrSearchCondition, [['id','<>', $id]]);
        }
        $objNew = NewOption::where($arrSearchCondition)->get();
        return count($objNew) > 0 ? false : true;

        //print_r(json_decode(json_encode($objNew)));die;
    }

    public static function update ($arrUpdateData)
    {
        $id = $arrUpdateData['id'];
        if (empty($id)) {
            return false;
        }

        $objIntro = self::getNewById($id);

        if (!empty($arrUpdateData['option_code'])) {
            $objIntro->option_code = $arrUpdateData['option_code'];
        }

        if (!empty($arrUpdateData['title'])) {
            $objIntro->title = $arrUpdateData['title'];
        }

        if (!empty($arrUpdateData['start_time'])) {
            $objIntro->start_time = $arrUpdateData['start_time'];
        }
        else {
            $objIntro->start_time = null;
        }

        if (isset($arrUpdateData['end_time']) && $arrUpdateData['end_time'] != '') {
            $objIntro->end_time = $arrUpdateData['end_time'];
        }

        if (empty($arrUpdateData['end_time'])) {
            $objIntro->end_time = null;
        }

        if (isset($arrUpdateData['status'])) {
            $objIntro->status = $arrUpdateData['status'];
        }

        if (!empty($arrUpdateData['content'])) {
            $objIntro->content = $arrUpdateData['content'];
        }

        if (!empty($arrUpdateData['password'])) {
            $objIntro->password = $arrUpdateData['password'];
        }

        try {
            $objIntro->save();
            DB::commit();
            return $objIntro;
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_intro_fail', 'Error when update user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$arrUpdateData], true) . ')');
            return null;
        }
    }


}
