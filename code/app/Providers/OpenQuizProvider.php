<?php
namespace App\Providers;

use App\Models\Member;
use Illuminate\Support\ServiceProvider;
use App\Models\OpenQuiz;
use App\Providers\MemberServiceProvider;
use Illuminate\Support\Facades\DB;

class OpenQuizProvider extends AppServiceProvider
{
	const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const FILE_CODE = 'file_code';

    public static function getQuizStatus($memberId)
    {
        return OpenQuiz::select('open')->where('member_id', $memberId)->first();
    }

    public static function searchMembers($arrConditions, $limit, $isCount) {
    	$status = $arrConditions['status'] ?? '';
        if (empty($status)) {
            $query = Member::query()->select('members.*', DB::RAW('IFNULL(open_quiz.open, 2) as open'))
                ->leftJoin('open_quiz', 'members.id', '=', 'open_quiz.member_id');
        } else {
            if ($status == 1) {
                $query = Member::query()->select('members.*', 'open_quiz.open')
                    ->leftJoin('open_quiz', 'members.id', '=', 'open_quiz.member_id')
                    ->where('open_quiz.open', $status);
            }

            if ($status == 2) {
                $arrQuiz = OpenQuiz::query()->select('member_id')->where('open', 1)->pluck('member_id')->toArray();
                $query = Member::query()->select('members.*', 'open_quiz.open')
                    ->leftJoin('open_quiz', 'members.id', '=', 'open_quiz.member_id')
                    ->whereNotIn('members.id', $arrQuiz);
            }
        }

        $query->where(function($query) {
            $query->where(['members.'. self::PHONE_VERIFIED => 1, 'members.'. self::ACCEPT_TERMS_AND_POLICIES => 1, 'members.status' => 13])
                ->orWhere(function ($query) {
                    $query->where('members.status', -1);
                });
        })->whereNull('members.is_delete');

        // search by ngày tạo hồ sơ
        MemberServiceProvider::searchByCreatedAtMember($arrConditions, $query);
        
        // search by common
        MemberServiceProvider::searchByCommon($arrConditions, $query);

        $query->orderByDesc('members.status')
            ->orderBy('members.' . self::FILE_CODE , 'asc');

        if (!$isCount){
            $objMember = $query->paginate($limit);
        } else {
            $objMember = $query->get();
        }
       
        return $objMember;
    }

    public static function deleteMembers($arrMemberId) {
    	OpenQuiz::whereIn('member_id', $arrMemberId)->delete();
    }

    public static function insertMembers($arrMemberId) {
    	OpenQuiz::insert($arrMemberId);
    }
}
