<?php
namespace App\Providers;

use App\Libs\Helpers\LogsHelper;
use App\Models\Note;
use Auth;
use DB;

class NoteServiceProvider extends AppServiceProvider
{
    public static function createNote($memberId, $content, $function, $action)
    {
    	DB::beginTransaction();
    	try {
    		$note = Note::create([
    			'member_id' => $memberId,
    			'user_id' => Auth::id(),
    			'note' => $content,
                'function_type' => $function,
                'action_type' => $action,
    		]);
    		DB::commit();

            return $note;
    	} catch (\Exception $exception) {
    		DB::rollBack();
            LogsHelper::trackByFile('save_note_fail', 'Error when create note (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    public static function createNoteWithMemberCode($memberId, $content, $function, $action, $attribute, $old=false, $new=false)
    {
        DB::beginTransaction();
        try {
            $note = Note::create([
                'member_id' => $memberId,
                'user_id' => Auth::id(),
                'note' => $content,
                'function_type' => $function,
                'action_type' => $action,
				'value_old' => $old,
				'value_new' => $new,
                'content' => json_encode($attribute)			
            ]);			
            DB::commit();

            return $note;
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('save_note_fail', 'Error when create note (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }
}
