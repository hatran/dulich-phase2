<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Libs\Helpers\LogsHelper;
use App\Libs\Storage\StoreFile;
use App\Libs\Storage\StorePhoto;
use App\Models\AchievementFile;
use App\Models\Education;
use App\Models\Element;
use App\Models\File;
use App\Models\LanguageSkill;
use App\Models\MajorSkill;
use App\Models\Member;
use App\Models\MemberAvatar;
use App\Models\MemberPayments;
use App\Models\MemberPaymentsYearly;
use App\Models\Offices;
use App\Models\Work;
use App\Models\WorkHistory;
use App\Models\Branches;
use App\Models\Language;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Mail;
use DomPDF;
use App\Constants\MemberConstants;
use App\Constants\CommontConstants;
use Auth;
use App\Constants\BranchConstants;
use App\Repositories\Branches\BranchesRepository;
use App\Models\ForteTour;
use App\Models\WaitingPrint;
use App\Models\Printed;

class ListMemberServiceProvider extends ServiceProvider
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const CREATED_AT = 'createdAt';
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const STATUS = 'status';
    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const IS_VERIFIED = 'is_verified';
    const IS_FEE = 'is_fee';
    const IS_SIGNED = 'is_signed';
    const MEMBER_TYPE = 'member_type';
    const ORIGINAL_NAME = 'originalName';
    const ABSOLUTE_PATH = 'absPath';
    const RELATIVE_URL = 'relativeUrl';
    const MEMBER_ID = 'memberId';
    const FILE_ID = 'fileId';
    const FILE_CODE = 'file_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_DELETE = 'is_delete';
    const MEMBER_CODE = 'member_code';

    const CARD_PRINT_WAITING_STATUS = 90;
    const EXTEND_CARD_PRINT_WAITING_STATUS = 91;
    const RENEW_CARD_PRINT_WAITING_STATUS = 92;
    const CARD_PRINTED_STATUS = 130; 
    const EXTEND_CARD_PRINTED_STATUS = 131;
    const RENEW_CARD_PRINTED_STATUS = 132;

    const IS_PHONE_VERIFIED = MemberConstants::IS_VERIFIED;
    const IS_ACCEPT_TERMS_AND_POLICIES = MemberConstants::IS_VERIFIED;
    const OFFICIAL_STATUS = MemberConstants::MEMBER_OFFICIAL_MEMBER;

    private static $statusNotOfficial = [
        MemberConstants::VERIFICATION_WAITING,
        MemberConstants::VERIFICATION_REJECTED,
        MemberConstants::UPDATE_INFO_1,
        MemberConstants::APPROVAL_REJECTED,
        MemberConstants::APPROVAL_WAITING,
        MemberConstants::SIGNING_WAITING,
        MemberConstants::CODE_PROVIDING_WAITING,
        MemberConstants::UPDATE_INFO_2,
        MemberConstants::MEMBER_STORED,
        MemberConstants::MEMBER_PENDDING,
        MemberConstants::MEMBER_DISTROY,
        MemberConstants::CODE_PROVIDING_REWAITING,
        MemberConstants::FEE_WAITING
    ];
    private static $statusWaitingPrint = [
        self::CARD_PRINT_WAITING_STATUS,
        self::EXTEND_CARD_PRINT_WAITING_STATUS,
        self::RENEW_CARD_PRINT_WAITING_STATUS
    ];
    private static $statusPrinted = [
        self::CARD_PRINTED_STATUS,
        self::EXTEND_CARD_PRINTED_STATUS,
        self::RENEW_CARD_PRINTED_STATUS
    ];


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function getListMembersByPlace($limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount)
    {
        if(!$isCount){
            $objMember = self::getAllMembersByPlaceQuery()->paginate($limit);
        }else{
            $objMember = self::getAllMembersByPlaceQuery()->get();
        }
        return $objMember;
    }


    public static function countMembers()
    {
        $objMember = self::getAllMembersByPlaceQuery()->get();
        if (empty($objMember))
            return 0;
        else
            count($objMember);
        return count($objMember);
    }

    private static function getAllMembersByPlaceQuery()
    {
        $query = MemberPayments::rightJoin('members', 'members.id', '=', 'member_payments.memberId')
            ->where(ListMemberServiceProvider::PHONE_VERIFIED, 1)
            ->where(ListMemberServiceProvider::ACCEPT_TERMS_AND_POLICIES, 1)
            ->where(ListMemberServiceProvider::IS_DELETE, '!=', 1)
            ->orWhereNull(ListMemberServiceProvider::IS_DELETE);

        $statusDict = MemberConstants::$file_const_display_status;
        if ((UserServiceProvider::isExpertRole()) || (UserServiceProvider::isLeaderRole())) {
            // Thay đổi logic bởi Dương 20180612
            /*$typeOfPlaces = [];
            if (UserServiceProvider::isExpertRole()) {
                if (UserServiceProvider::isHnExpertUser()) {
                    $typeOfPlaces = BranchConstants::$hn_typeOfPlace;
                } else if (UserServiceProvider::isDnExpertUser()) {
                    $typeOfPlaces = BranchConstants::$dn_typeOfPlace;
                } else if (UserServiceProvider::isHcmExpertUser()) {
                    $typeOfPlaces = BranchConstants::$hcm_typeOfPlace;
                }
            } elseif (UserServiceProvider::isLeaderRole()) {

                if (UserServiceProvider::isHnLeaderUser()) {
                    $typeOfPlaces = BranchConstants::$hn_typeOfPlace;
                } else if (UserServiceProvider::isDnLeaderUser()) {
                    $typeOfPlaces = BranchConstants::$dn_typeOfPlace;
                } else if (UserServiceProvider::isHcmLeaderUser()) {
                    $typeOfPlaces = BranchConstants::$hcm_typeOfPlace;
                }
            }*/

            $typeOfPlaces = UserServiceProvider::getTypeOfPlace();

            return $query->whereIn(self::STATUS, self::extractStatusValues($statusDict))
                ->whereIn(self::PROVINCE_CODE, $typeOfPlaces)
                ->orderBy(ListMemberServiceProvider::CREATED_AT, 'des');
        } else {
            return $query->whereIn(self::STATUS, self::extractStatusValues($statusDict))
            ->orderBy(ListMemberServiceProvider::CREATED_AT, 'des');
        }

    }

    private static function extractStatusValues($statusDict)
    {
        return collect($statusDict)->keys()->all();
    }

    public static function getMemberById($id)
    {
        if (empty($id)) {
            return null;
        }

        return Member::where('id', $id)->first();
    }

    public static function getMembersByIds($ids)
    {
        if (count($ids) == 0) {
            return null;
        }

        return Member::whereIn('id', $ids)->whereNull('is_delete')->get();
    }

    public static function getAllLanguages() {
        return Language::getAllLanguages();
    }

    public static function getAllForteTours() {
        return ForteTour::getAllForteTour();
    }

    public static function softDelete($id)
    {
        try {
            $objMember = self::getMemberById($id);
            if (!(empty($objMember))) {
                $objMember->is_delete = 1;
                $objMember->save();
            }
            return $objMember;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_decision_when_do_report_fail', 'Error when update member decision do report (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$id], true) . ')');
            return null;
        }
    }

    public static function updateMemberStatus($request, $id)
    {
        try {
            $type = $request->input('type');

            $objMember = self::getMemberById($id);
            if (!(empty($objMember))) {
                if ($type == 1) {
                    // Lưu kho hồ sơ
                    $objMember->initial_stock = $objMember->status;
                    $objMember->status = MemberConstants::MEMBER_STORED;
                } else if ($type == 2){
                    // khôi phục hồ sơ
                    $objMember->status = $objMember->initial_stock;
                    $objMember->initial_stock = null;
                }
                else if ($type == 3){
                    // chấm dứt tư cách hội viên
                    $objMember->status = MemberConstants::MEMBER_DISTROY;
                }
                else if ($type == 4){
                    // Tạm dừng tư cách hội viên
                    $objMember->pending_status = $objMember->status;
                    $objMember->status = MemberConstants::MEMBER_PENDDING;
                } else  {
                    // khôi phục tư cách hội viên
                    $objMember->status = $objMember->pending_status;
                    $objMember->pending_status = null;
                }
                $objMember->save();
            }
            return $objMember;
        } catch (\Exception $exception) {

            LogsHelper::trackByFile('update_member_decision_when_do_report_fail', 'Error when update member decision do report (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$id], true) . ')');
            return null;
        }
    }

    public static function fetchAllMembersForCardPrinted() {
        return Printed::select(
                    'memberId', 
                    'cardType'
                )
                ->whereIn('id', [
                    DB::raw('select MAX(id) as maxid 
                            from printed 
                            group by memberId')
                ])
                ->orderBy('memberId');
    }

    public static function fetchMembersForCardWaiting($queryPrinted) {
        return WaitingPrint::select(
                    'waiting_print.memberId', 
                    'printTable.cardType as pCardType', 
                    'waiting_print.cardType as wCardType'
                )
                ->leftJoin(
                    DB::raw("({$queryPrinted->toSql()}) AS printTable"), 
                    'waiting_print.memberId', 
                    '=', 
                    'printTable.memberId'
                );
    }

    public static function fetchUnionMembersForWaitingAndPrinted($queryPrinted, $queryWaitingOnly) {
        return WaitingPrint::select(
                    'printTable.memberId', 
                    'printTable.cardType as pCardType', 
                    'waiting_print.cardType as wCardType'
                )
                ->rightJoin(
                    DB::raw("({$queryPrinted->toSql()}) AS printTable"), 
                    'waiting_print.memberId', 
                    '=', 
                    'printTable.memberId')
                ->union($queryWaitingOnly);
    }

    public static function fetchOfficialMembers($queryWaiting) {
        $query = Member::withoutGlobalScopes()->selectRaw('
                members.file_code, 
                members.touristGuideCode, 
                members.member_code, 
                members.fullName, 
                members.birthday, 
                members.firstMobile, 
                members.createdAt, 
                members.approved_at, 
                members.typeOfTravelGuide, 
                members.status, 
                members.id, 
                members.is_fee,
                members.firstEmail,
                members.member_from,
                members.member_code_expiration,
                members.province_code,
                printed.wCardType, 
                printed.pCardType
            ')
            ->join(
                DB::raw("({$queryWaiting->toSql()}) AS printed"), 
                'members.id', 
                '=', 
                'printed.memberId')
            ->where('members.phoneVerified', self::IS_PHONE_VERIFIED)
            ->where('members.acceptTermsAndPolicies', self::IS_ACCEPT_TERMS_AND_POLICIES)
            ->whereNull('members.is_delete')
            ->where('members.status', self::OFFICIAL_STATUS);

        $query = self::filterBranchIdByRole($query);

        return $query;
    }

    private static function fetchNotOfficialMembers($status = null) {
        $query = Member::withoutGlobalScopes()->selectRaw('
                members.file_code, 
                members.touristGuideCode, 
                members.member_code, 
                members.fullName, 
                members.birthday,
                members.firstMobile, 
                members.createdAt, 
                members.approved_at, 
                members.typeOfTravelGuide, 
                members.status, 
                members.id, 
                members.is_fee,
                members.firstEmail,
                members.member_from,
                members.member_code_expiration,
                members.province_code,
                NULL as wCardType, 
                NULL as pCardType
            ')
            ->where('members.phoneVerified', self::IS_PHONE_VERIFIED)
            ->where('members.acceptTermsAndPolicies', self::IS_ACCEPT_TERMS_AND_POLICIES)
            ->whereNull('members.is_delete');

        if (empty($status)) {
            $query->whereIn('members.status', self::$statusNotOfficial);
        } else {
            $query->where('status', $status);
        }

        $query = self::filterBranchIdByRole($query);

        return $query;
    }

    private static function fetchAllMembersListSearch($queryMembers) {
        return DB::table(
            DB::raw("({$queryMembers->toSql()}) AS tableUnion"))
            ->mergeBindings($queryMembers->getQuery())
            ->selectRaw('
                tableUnion.file_code, 
                tableUnion.touristGuideCode, 
                tableUnion.member_code, 
                tableUnion.fullName, 
                tableUnion.birthday, 
                tableUnion.firstMobile, 
                tableUnion.createdAt, 
                tableUnion.approved_at, 
                tableUnion.typeOfTravelGuide, 
                tableUnion.status, 
                tableUnion.id, 
                tableUnion.is_fee,
                tableUnion.firstEmail,
                tableUnion.member_from,
                tableUnion.member_code_expiration,
                tableUnion.province_code,
                tableUnion.wCardType, 
                tableUnion.pCardType
            ')
            ->orderBy('tableUnion.id');
    }

    /**
     * Search by condition, always filter by province_code
     *
     * @param $arrConditions
     * @param int $limit
     * @param $isCount
     * @return mixed
     */
    public static function listSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount)
    {
        $status = $arrConditions['status'];
        $print_status = $arrConditions['print_status'];
        $guideLanguage = $arrConditions['guideLanguage'];

        if (empty($status) && empty($print_status)) {
            // query dữ liệu toàn bộ member đã in thẻ mới nhất
            $cardPrintedAllMembers = self::fetchAllMembersForCardPrinted();
            // query dữ liệu member chờ in thẻ
            $cardWaitingMembers = self::fetchMembersForCardWaiting($cardPrintedAllMembers);
            // query dữ liệu member đã in thẻ và merge với dữ liệu member chờ in thẻ
            $unionMembersForWaitingAndPrinted = self::fetchUnionMembersForWaitingAndPrinted($cardPrintedAllMembers, $cardWaitingMembers);
            // query dữ liệu member chính thức
            $officialMembers = self::fetchOfficialMembers($unionMembersForWaitingAndPrinted);
            // them dieu kien search neu co
            self::listSearchCommonNew($arrConditions, $officialMembers);
            // query dữ liệu thông tin member không chính thức
            $notOfficialMembers = self::fetchNotOfficialMembers();
            // them dieu kien search neu co
            self::listSearchCommonNew($arrConditions, $notOfficialMembers);
            // query merge toàn bộ member chính thức với không chính thức
            $notOfficialMembers->unionAll($officialMembers);
            // fetch dữ liệu thông tin members
            $query = self::fetchAllMembersListSearch($notOfficialMembers);
        } else {
            if ($status == 13 || (!empty($print_status) && empty($status))) {
                // query dữ liệu toàn bộ member đã in thẻ mới nhất
                $cardPrintedAllMembers = self::fetchAllMembersForCardPrinted();
                // query dữ liệu member chờ in thẻ
                $cardWaitingMembers = self::fetchMembersForCardWaiting($cardPrintedAllMembers);
                // query dữ liệu member đã in thẻ và merge với dữ liệu member chờ in thẻ
                $unionMembersForWaitingAndPrinted = self::fetchUnionMembersForWaitingAndPrinted($cardPrintedAllMembers, $cardWaitingMembers);
                // query dữ liệu member chính thức
                $query = self::fetchOfficialMembers($unionMembersForWaitingAndPrinted);

                if (in_array($print_status, self::$statusWaitingPrint)) {
                    if ($print_status == self::EXTEND_CARD_PRINT_WAITING_STATUS) {
                        $query->where('printed.wCardType', 1);
                    }
                    else if ($print_status == self::RENEW_CARD_PRINT_WAITING_STATUS) {
                        $query->where('printed.wCardType', 2);
                    }
                    else {
                        $query->where('printed.wCardType', 0);
                    }
                }

                if (in_array($print_status, self::$statusPrinted)) {
                    if ($print_status == self::EXTEND_CARD_PRINTED_STATUS) {
                        $query->where('printed.pCardType', 1);
                    }
                    else if ($print_status == self::RENEW_CARD_PRINTED_STATUS) {
                        $query->where('printed.pCardType', 2);
                    }
                    else {
                        $query->where('printed.pCardType', 0);
                    }

                    $query->whereNull('printed.wCardType');
                }
                
            }

            if (in_array($status, self::$statusNotOfficial, false)) {
                $query = self::fetchNotOfficialMembers($status);
                if (!empty($print_status)) {
                    $query->where('members.status', -999);
                }
            }
            self::listSearchCommonNew($arrConditions, $query);
            $query->orderBy('members.id');
        }

        if (!$isCount){
            $objMember = $query->paginate($limit);
        } else {
            $objMember = $query->get();
        }

        return $objMember;
    }

    public static function listSearchCommonNew($arrConditions, &$query, $str = '') {
        // search by ngày tạo hồ sơ
        MemberServiceProvider::searchByCreatedAtMember($arrConditions, $query);
        
        if (empty($str)) {
            if(!empty($arrConditions[self::PROVINCE_CODE])) {
                MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
            }

            if (array_key_exists(self::PROVINCE_TYPE, $arrConditions) && !empty($arrConditions[self::PROVINCE_TYPE])) {
                $query->whereIn(self::PROVINCE_CODE, $arrConditions[self::PROVINCE_TYPE]);
            }
        } else {
            if ($str === 'editor') {
                $vpddCode = auth()->user()->province_type;
                if (!empty($vpddCode)) {
                    $branches = new BranchesRepository(new Branches());
                    $arrTypeOfPlaceBelongToThisUser = $branches->getAllByParentId($vpddCode);
                    if (!empty($arrTypeOfPlaceBelongToThisUser)) {
                        $query->whereIn('members.'.self::PROVINCE_CODE, $arrTypeOfPlaceBelongToThisUser);
                    }
                }
            }
        }
        
        // search by common
        MemberServiceProvider::searchByCommon($arrConditions, $query);
    }

    public static function countMemberByStatus($status, $branchList, $province_code = '', $fromDate, $toDate)
    {
        if($province_code == '') {
            $query = Member::whereIn('status', $status)
                ->where('is_delete', null);
        }else{
            $query = Member::whereIn('status', $status)
                ->where('province_code', $province_code)
                ->where('is_delete', null);
        }

        if(!empty($toDate)) {
            $query->whereDate(self::CREATED_AT, '<=', $toDate . ' 23:59:59');
        }

        if(!empty($fromDate)) {
            $query->whereDate(self::CREATED_AT, '>=', $fromDate . ' 00:00:00');
        }
        $countMember = $query->get()->count();

        $arrMember = [];
        $arrMember['count'] = $countMember;
        $arrMember['all'] = array();
        if(($province_code == '') && count($branchList) > 0) {
            foreach($branchList as $key => $value){
                $queryChildren = Member::whereIn('status', $status)
                    ->where('province_code', $key)
                    ->where('is_delete', null);

                    if(!empty($toDate)) {
                        $queryChildren->whereDate(self::CREATED_AT, '<=', $toDate . ' 23:59:59');
                    }

                    if(!empty($fromDate)) {
                        $queryChildren->whereDate(self::CREATED_AT, '>=', $fromDate . ' 00:00:00');
                    }
                    $countMemberByProvince = $queryChildren->get()->count();

                    if($countMemberByProvince > 0){
                        $arrMember['all'][$key]['name'] = $value;
                        $arrMember['all'][$key]['count'] = $countMemberByProvince;
                    }
            }
        }

        return $arrMember;
    }
    public static function listSearchByConditionsRank($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount)
    {
        $query = Member::query()->select(DB::raw("count(member_evaluation.id) as total,
                    count(DISTINCT member_evaluation.taxcode_company) as cnt,
                    ROUND(SUM(member_evaluation.criteria1) / count(member_evaluation.id)) as ct1,
                    ROUND(SUM(member_evaluation.criteria2) / count(member_evaluation.id)) as ct2,
                    ROUND(SUM(member_evaluation.criteria3) / count(member_evaluation.id)) as ct3,
                    ROUND(SUM(member_evaluation.criteria4) / count(member_evaluation.id)) as ct4,
                    ROUND(SUM(member_evaluation.criteria5) / count(member_evaluation.id)) as ct5,
                    criteria1_code, 
                    criteria2_code,
                    criteria3_code,
                    criteria4_code,
                    criteria5_code, members.fullName, members.member_code, members.touristGuideCode, members.file_code, member_rank.createdAt as registerday, member_rank.score, member_file_award.score_file, member_file_award.score_award, members.rank_status, members.birthday, member_rank.join_date, members.id, member_rank.is_deleted, member_rank.id as mrid, member_rank.rank_id as mrrid"))
                ->leftJoin(DB::raw("(SELECT * FROM member_rank a WHERE id = (SELECT id FROM member_rank WHERE a.member_id = member_id order by rank_id asc, score desc limit 1)) as member_rank"), 'member_rank.member_id', '=', 'members.id')
                ->leftJoin(DB::raw("(SELECT * FROM member_evaluation m where (select count(*) from member_evaluation m1 where m1.member_id = m.member_id and m1.rank_id <= m.rank_id and m1.id >= m.id order by m1.rank_id asc) <= 30) as member_evaluation"), function ($join) {
                    $join->on('member_evaluation.rank_id', '=', 'member_rank.rank_id')
                        ->on('member_evaluation.member_id', '=', 'member_rank.member_id')
                        ->orderByDesc('member_evaluation.id')
                        ->limit(30);
                })
                ->leftJoin('member_file_award', function ($join) {
                    $join->on('member_rank.member_id', '=', 'member_file_award.member_id')
                        ->on('member_rank.rank_id', '=', 'member_file_award.rank_id');
                });

        $query->whereIn('members.status' , [$arrConditions[self::STATUS], -1]);

        if(!empty($arrConditions[self::PROVINCE_CODE])) {
            MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
        }

        if (array_key_exists(self::PROVINCE_TYPE, $arrConditions) && !empty($arrConditions[self::PROVINCE_TYPE])) {
            $query->whereIn(self::PROVINCE_CODE, $arrConditions[self::PROVINCE_TYPE]);
        }
        // search by common
        MemberServiceProvider::searchByCommon($arrConditions, $query);
        $query->where('members.is_delete' , null);
        $query->orderBy('members.' . self::CREATED_AT, 'asc');
        $query->groupBy(DB::raw("members.id, member_rank.id, member_evaluation.criteria1_code, 
                    member_evaluation.criteria2_code,
                    member_evaluation.criteria3_code,
                    member_evaluation.criteria4_code,
                    member_evaluation.criteria5_code"));
        if (!$isCount){
            return $query->paginate($limit);
        }

        return $query->get();
    }

    public static function listMemberByRoleEditor($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount)
    {
        $status = $arrConditions['status'];
        if (empty($status)) {
            $cardPrintedAllMembers = self::fetchAllMembersForCardPrinted();

            $cardWaitingMembers = self::fetchMembersForCardWaiting($cardPrintedAllMembers);

            $unionMembersForWaitingAndPrinted = self::fetchUnionMembersForWaitingAndPrinted($cardPrintedAllMembers, $cardWaitingMembers);

            $officialMembers = self::fetchOfficialMembers($unionMembersForWaitingAndPrinted);

            self::listSearchCommonNew($arrConditions, $officialMembers, 'editor');

            $notOfficialMembers = self::fetchNotOfficialMembers();

            self::listSearchCommonNew($arrConditions, $notOfficialMembers, 'editor');

            $notOfficialMembers->unionAll($officialMembers);

            $query = self::fetchAllMembersListSearch($notOfficialMembers);
        } else {
            if (!in_array($status, self::$statusNotOfficial)) {
                $cardPrintedAllMembers = self::fetchAllMembersForCardPrinted();

                $cardWaitingMembers = self::fetchMembersForCardWaiting($cardPrintedAllMembers);

                $unionMembersForWaitingAndPrinted = self::fetchUnionMembersForWaitingAndPrinted($cardPrintedAllMembers, $cardWaitingMembers);

                $query = self::fetchOfficialMembers($unionMembersForWaitingAndPrinted);

                if (in_array($arrConditions[self::STATUS], self::$statusWaitingPrint)) {
                    if ($arrConditions[self::STATUS] == self::EXTEND_CARD_PRINT_WAITING_STATUS) {
                        $query->where('printed.wCardType', 1);
                    }
                    else if ($arrConditions[self::STATUS] == self::RENEW_CARD_PRINT_WAITING_STATUS) {
                        $query->where('printed.wCardType', 2);
                    }
                    else {
                        $query->where('printed.wCardType', 0);
                    }
                }

                if (in_array($arrConditions[self::STATUS], self::$statusPrinted)) {
                    if ($arrConditions[self::STATUS] == self::EXTEND_CARD_PRINTED_STATUS) {
                        $query->where('printed.pCardType', 1);
                    }
                    else if ($arrConditions[self::STATUS] == self::RENEW_CARD_PRINTED_STATUS) {
                        $query->where('printed.pCardType', 2);
                    }
                    else {
                        $query->where('printed.pCardType', 0);
                    }

                    $query->whereNull('printed.wCardType');
                }
            } else {
                $query = self::fetchNotOfficialMembers($status);
            }
            self::listSearchCommonNew($arrConditions, $query, 'editor');
            $query->orderBy('members.id');
        }
                
        if(!$isCount){
            $objMember = $query->paginate($limit);
        } else {
            $objMember = $query->get();
        }

        return $objMember;
    }

    public static function getListNotMember($arrConditions, $limit ,$isCount) {
        $notMemberStatus = -1;
        $query = Member::select('id', 'fullName', 'firstMobile', 'firstEmail', 'birthday', 'touristGuideCode')->where('status', $notMemberStatus)->whereNull('is_delete');
        if (!empty($arrConditions['fullName'])) {
            $query = $query->where('fullName', 'like', '%' . addslashes($arrConditions['fullName']) . '%');
        }

        if (!empty($arrConditions['birthday'])) {
            $query = $query->whereDate('birthday', '=', $arrConditions['birthday'] . ' 00:00:00');
        }

        if (!empty($arrConditions['firstMobile'])) {
            $query = $query->where('firstMobile', 'like', '%' . addslashes($arrConditions['firstMobile']) . '%');
        }

        if (!empty($arrConditions['firstEmail'])) {
            $query = $query->where('firstEmail', 'like', '%' . addslashes($arrConditions['firstEmail']) . '%');
        }

        $query->orderBy(self::CREATED_AT, 'asc');

        if(!$isCount){
            $objMember = $query->paginate($limit);
        }else {
            $objMember = $query->get();
        }

        return $objMember;
    }

    public static function generalReportSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE , $isCount) {
        $status = $arrConditions['member_status'];
        if (!empty($status)) {
            if (in_array($status, [1,2,3,4,5,6,7,8,10,13,14,15,16,61], false)) {
                $query = Member::withoutGlobalScopes()->select('members.*', DB::raw('NULL as cardType'))
                    ->whereNull('members.is_delete')
                    ->where('members.phoneVerified', 1)
                    ->where('members.acceptTermsAndPolicies', 1)
                    ->where('members.status', $status);
            } else {
                $query = Member::withoutGlobalScopes()->select('members.*', 'printed.cardType')
                    ->join('printed', 'printed.memberId', '=', 'members.id')
                    ->where('members.status', 13)
                    ->where('members.phoneVerified', 1)
                    ->where('members.acceptTermsAndPolicies', 1)
                    ->whereNull('members.is_delete');

                switch ($status) {
                    case 130:
                        $query->where('printed.cardType', 0);
                        break;
                    case 131:
                        $query->where('printed.cardType', 1);
                        break;
                    case 132:
                        $query->where('printed.cardType', 2);
                        break;
                }
            }

            // search by ngày tạo hồ sơ
            MemberServiceProvider::searchByCreatedAtMember($arrConditions, $query);
            if(!empty($arrConditions[self::PROVINCE_CODE])) {
                MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
            }

            $query = self::filterBranchIdByRole($query);

            $query->orderBy('members.' . self::CREATED_AT, 'asc');
        } else {
            $query1 = Member::withoutGlobalScopes()->select('members.*', DB::raw('NULL as cardType'))
                    ->whereNull('members.is_delete')
                    ->where('members.phoneVerified', 1)
                    ->where('members.acceptTermsAndPolicies', 1)
                    ->whereIn('members.status', [1,2,3,4,5,6,7,8,10,14,15,16,61]);

            // search by ngày tạo hồ sơ
            MemberServiceProvider::searchByCreatedAtMember($arrConditions, $query1);
            if(!empty($arrConditions[self::PROVINCE_CODE])) {
                MemberServiceProvider::searchByProvinceCode($arrConditions, $query1);
            }

            $query1 = self::filterBranchIdByRole($query1);

            $query1->orderBy('members.' . self::CREATED_AT, 'asc');

            $query2 = Member::withoutGlobalScopes()->select('members.*', 'printed.cardType')
                    ->join('printed', 'printed.memberId', '=', 'members.id')
                    ->where('members.status', 13)
                    ->whereNull('members.is_delete')
                    ->where('members.phoneVerified', 1)
                    ->where('members.acceptTermsAndPolicies', 1);
            // search by ngày tạo hồ sơ
            MemberServiceProvider::searchByCreatedAtMember($arrConditions, $query2);
            if(!empty($arrConditions[self::PROVINCE_CODE])) {
                MemberServiceProvider::searchByProvinceCode($arrConditions, $query2);
            }

            $query2 = self::filterBranchIdByRole($query2);

            $query2->orderBy('members.' . self::CREATED_AT, 'asc')->unionAll($query1);

            $query = DB::table(
                DB::raw("({$query2->toSql()}) AS tableUnion"))
                ->mergeBindings($query2->getQuery())
                ->selectRaw('
                    tableUnion.file_code, 
                    tableUnion.touristGuideCode, 
                    tableUnion.member_code, 
                    tableUnion.member_from,
                    tableUnion.fullName, 
                    tableUnion.birthday, 
                    tableUnion.firstMobile, 
                    tableUnion.createdAt, 
                    tableUnion.verified_at, 
                    tableUnion.approved_at, 
                    tableUnion.typeOfTravelGuide, 
                    tableUnion.status, 
                    tableUnion.id, 
                    tableUnion.is_fee,
                    tableUnion.firstEmail,
                    tableUnion.member_from,
                    tableUnion.member_code_expiration,
                    tableUnion.province_code, 
                    tableUnion.cardType,
                    tableUnion.rank_status,
                    tableUnion.address
                ');
        }
        
        if (!$isCount){
            $objMember = $query->paginate($limit);
        } else {
            $objMember = $query->get();
        }

        return $objMember;
    }

    public static function filterBranchIdByRole($query)
    {
        $user = auth()->user();
        $branchIds = $user->branch_id ? [$user->branch_id] : [];
        if (empty($branchIds)) {
            $provinceType = $user->province_type ?? null;
            if ($provinceType) {
                $getProvinceCode = Offices::getProvinceCodeByProvinceType($provinceType);
                foreach ($getProvinceCode as $provinceCode) {
                    $branchIds[] = $provinceCode['id'];
                }
            }
        }
        if (count($branchIds)) {
            $query->whereIn('members.province_code', $branchIds);
        }

        return $query;
    }
}
