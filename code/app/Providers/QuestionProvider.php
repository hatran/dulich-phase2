<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Response;
use Auth;
use App\Constants\BranchConstants;
use App\Constants\CommontConstants;
use App\Models\Knowledge;
use App\Models\Question;
use App\Models\SubjectKnowledge;
use Validator as V;
use DB;
use App\Libs\Helpers\LogsHelper;

class QuestionProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;

    public static $rules = [
        'code' => 'required',
        'status' => 'required|integer|in:0,1',
        'content' => 'required',
        'answer_correct' => 'required',
        'answer1' => 'required',
        'answer2' => 'required',
        'answer3' => 'required',
        'answer4' => 'required',
        'question_rank' => 'required',
        'subject_knowledge_id' => 'required'
    ];

    public static $messages = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'integer' => ':attribute phải là số',
        'in' => 'Dữ liệu không nằm trong dữ liệu cho phép',
    ];

    public static $niceAttributeName = [
        'code' => 'Mã câu hỏi',
        'question_rank' => 'Loại câu hỏi',
        'status' => 'Trạng thái',
        'content' => 'Nội dung câu hỏi',
        'answer1' => 'Đáp án 1',
        'answer2' => 'Đáp án 2',
        'answer3' => 'Đáp án 3',
        'answer4' => 'Đáp án 4',
        'answer_correct' => 'Đáp án đúng',
        'subject_knowledge_id' => 'Danh mục câu hỏi'
    ];

    public static function getAllSubjectKnowledge ()
    {
        $objSubject = SubjectKnowledge::getAllSubjectKnowledgeIDName();
        return $objSubject;
    }

    public static function getAllKnowledge() 
    {
        $objKnowledge = Knowledge::getAllKnowledge();
        return $objKnowledge;
    }


    public static function questionsSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];
      
        if (!empty($arrConditions['contentall'])) {
            $arrSearchCondition = array_merge($arrSearchCondition,[['content', 'like', '%' . $arrConditions['contentall'] . '%']]);
        }

        if (!empty($arrConditions['question_rank'])) {
            $arrSearchCondition = array_merge($arrSearchCondition,[['question_rank', '=', $arrConditions['question_rank']]]);
        }

        $arrSub = array();
        if (!empty($arrConditions['subject_knowledge_id'])) {
            $arrSearchCondition = array_merge($arrSearchCondition,[['subject_knowledge_id', '=', $arrConditions['subject_knowledge_id']]]);
        }
        else {
            if (!empty($arrConditions['knowledge_id'])) {
                $sub = SubjectKnowledge::getSubjectKnowledgeByKnowledgeID($arrConditions['knowledge_id']);
                
                if (count($sub) != 0) {
                    foreach ($sub as $s) {
                        $arrSub[] = $s->id;
                    }
                }
            }
        }       

        if(!$isCount){
            if (count($arrSub) != 0) {
                $objQuestion = Question::where($arrSearchCondition)->whereIn('subject_knowledge_id', $arrSub)->where(function($q)  {
                                $q->where('is_deleted', 0)
                                ->orWhereNull('is_deleted');
                            })->orderBy('created_at', 'des')->paginate($limit);
            }
            else {
                $objQuestion = Question::where($arrSearchCondition)->where(function($q)  {
                                $q->where('is_deleted', 0)
                                ->orWhereNull('is_deleted');
                            })->orderBy('created_at', 'des')->paginate($limit);
            }
            
        }else{
            if (count($arrSub) != 0) {
                $objQuestion = Question::where($arrSearchCondition)->whereIn('subject_knowledge_id', $arrSub)->where(function($q)  {
                                $q->where('is_deleted', 0)
                                ->orWhereNull('is_deleted');
                            })->get();
            }
            else {
                $objQuestion = Question::where($arrSearchCondition)->where(function($q)  {
                                $q->where('is_deleted', 0)
                                ->orWhereNull('is_deleted');
                            })->get();
            }
        }

        return $objQuestion;
    }

    public static function getDataById($id)
    {
        return Question::where('id',$id)->whereNull('is_deleted')->first();
    }

    public static function getStatusZeroById($id)
    {
        return Question::where('id', $id)->where('status', 0)->whereNull('is_deleted')->first();
    }

    public static function checkCode($code)
    {
        $getCodeExist = Question::where('code', $code)->whereNull('is_deleted')->first();
        return $getCodeExist;
    }

    public static function findQuestion($id) {
        return Question::find($id);
    }

    public static function isValid(array $attributes, array $rules = null, array $messages = null, array $niceAttributeName = null) {
        if (empty($messages)) {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules);
        } else {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        }
        $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        if (!empty($niceAttributeName)) {
            $v->setAttributeNames($niceAttributeName);
        }
        if ($v->fails()) {
            return $v->messages();
        }

        return true;
    }

    public static function getFirstRandomQuestions($subject_knowledge_id, $rank_constant, $limit) {
        return DB::table('question')
                    ->where('status', '=', '1')
                    ->where(function($query) {
                        $query->whereNull('is_deleted')
                              ->orWhere('is_deleted', 0);
                    })
                    ->where('subject_knowledge_id', '=', key($subject_knowledge_id))
                    ->where('question_rank', '=', $rank_constant)
                    ->orderBy(DB::raw('RAND()'))
                    ->limit($limit);
    }

    public static function unionAllQuestion($question, $subject_knowledge_id, $rank_constant, $limit) {
        $question_subquery = DB::table('question')
                    ->where('status', '=', '1')
                    ->where(function($query) {
                        $query->whereNull('is_deleted')
                              ->orWhere('is_deleted', 0);
                    })
                    ->where('subject_knowledge_id', '=', $subject_knowledge_id)
                    ->where('question_rank', '=', $rank_constant)
                    ->orderBy(DB::raw('RAND()'))
                    ->limit($limit);
        return $question->unionAll($question_subquery);
    }

    public static function getAnswerMember($memberId) {
        return DB::table('subject_knowledge')
            ->selectRaw('subject_knowledge.knowledge_id, COUNT(*) as total')
            ->join('question_tmp', function ($join) {
                $join->on('subject_knowledge.id', '=', 'question_tmp.subject_knowledge_id')
                    ->where('subject_knowledge.status', 1)
                    ->whereNull('subject_knowledge.is_deleted');
            })
            ->join('answer_tmp', function ($join) {
                $join->on('question_tmp.member_id', '=', 'answer_tmp.member_id')
                    ->on('question_tmp.answer_correct', '=', 'answer_tmp.answer')
                    ->on('question_tmp.question_id', '=', 'answer_tmp.question_id');
            })
            ->where('question_tmp.member_id', $memberId)
            ->groupBy('subject_knowledge.knowledge_id')
            ->pluck('total', 'knowledge_id')
            ->all();
    }
}
