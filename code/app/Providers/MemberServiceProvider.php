<?php

namespace App\Providers;

use App\Libs\Helpers\LogsHelper;
use App\Libs\Helpers\Utils;
use App\Libs\Mail\MailHelper;
use App\Libs\Storage\StoreFile;
use App\Libs\Storage\StorePhoto;
use App\Models\AchievementFile;
use App\Models\Branches;
use App\Models\Education;
use App\Models\Element;
use App\Models\File;
use App\Models\LanguageSkill;
use App\Models\MemberPayments;
use App\Models\MajorSkill;
use App\Models\Major;
use App\Models\Member;
use App\Models\MemberPrintCardSelected;
use App\Models\MemberTmp;
use App\Models\MemberAvatar;
use App\Models\Note;
use App\Models\Offices;
use App\Models\User;
use App\Models\Work;
use App\Models\WorkHistory;
use App\Constants\CommontConstants;
use App\Repositories\Branches\BranchesRepository;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Constants\MemberConstants;
use Auth;
use App\Constants\BranchConstants;
use App\Jobs\SendEmail;
use App\Models\OtherConstant;
use App\Models\WorkConstant;
use App\Models\Option;
use App\Models\Language;
use App\Models\ForteTour;
use App\Models\Emails;
use App\Models\WaitingPrint;
use App\Models\Printed;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\MemberRank;
use DomPDF;

class MemberServiceProvider extends AppServiceProvider
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const CREATED_AT = 'createdAt';
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const STATUS = 'status';
    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const VERIFIED_AT = 'verified_at';
    const APPROVED_AT = 'approved_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_VERIFIED = 'is_verified';
    const IS_FEE = 'is_fee';
    const IS_SIGNED = 'is_signed';
    const MEMBER_TYPE = 'member_type';
    const ORIGINAL_NAME = 'originalName';
    const ABSOLUTE_PATH = 'absPath';
    const RELATIVE_URL = 'relativeUrl';
    const MEMBER_ID = 'memberId';
    const FILE_ID = 'fileId';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';
    const IS_PRINTING = 'is_printed_card';
    const MEMBER_CODE = 'member_code';
    const IS_DELETED = 'is_delete';
    const BRAND = 'objBrand';
    const DEGREE = 'objDegree';
    const LANGUAGE = 'objLanguage';
    const LANGUAGE_LEVEL = 'objLanguageLevel';
    const TYPE_GUIDE_NAME = 'typeGuideName';
    const GROUP_SIZE_NAME = 'groupSizeName';
    const EMAIL01 = 'EMAIL01';
    const HANOI = 66;
    const DANANG = 67;
    const HCM = 68;
    const MEMBER_FROM = 'member_from';
    const BCC_EMAIL = 'hoihdv@gmail.com';
    const TOANQUOC = 0;

    public static $mappingPrintCardSelectedType = [
        9 => 9,
		90 => 0,
        91 => 1,
        92 => 2,
        130 => 3,
        131 => 4,
        132 => 5
    ];

    public static $mappingPrintCardSelectedName = [
        'Chờ in thẻ tổng hợp',
		'Chờ in thẻ lần đầu',
        'Chờ in thẻ gia hạn',
        'Chờ in thẻ cấp lại',
        'Đã in thẻ lần đầu',
        'Đã in thẻ gia hạn',
        'Đã in thẻ cấp lại'
    ];

    public static function getAllMember ($limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE)
    {
        $objMember = Member::orderBy(MemberServiceProvider::CREATED_AT, 'des')->paginate($limit);

        return $objMember;
    }

    public static function getMemberById ($id)
    {
        return Member::find($id);
    }

    public static function getMemberForteTourAndGuideLanguageById ($id)
    {
        $objMember = Member::select('forteTour', 'guideLanguage')->where('id', $id)->first();
        return $objMember;
    }

    public static function getMemberTmpById ($id)
    {
        $objMember = MemberTmp::where('member_id', $id)->first();
        return $objMember;
    }
    public static function getDetailMemberById($id)
    {
        $query = Note::select("notes.*", "users.fullname")->rightJoin('users', 'notes.user_id', '=', 'users.id')
                ->where('notes.member_id' , $id);
        return $query->get();
    }

    public static function getMemberNeedUpdate($id)
    {
        $query = Member::where('id', $id)->where(function($query) use ($id) {
            $query->where('status', MemberConstants::UPDATE_INFO_1)->orWhere('status', MemberConstants::UPDATE_INFO_2)->orWhere('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)->orWhere('status', MemberConstants::APPROVE_WAITING_UPDATE);
        })->firstOrFail();
        return $query;
    }

    public static function undoPhoneVerified($id)
    {
        Member::where('members.id', $id)->update([
            'phoneVerified' => 0
        ]);
    }

    public static function getMemberByUserName($userName)
    {
        $objMember = Member::where('username', $userName)->first();

        return $objMember;
    }

    public static function searchByConditionsForCreateCard($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $query = Member::select('members.*', 'waiting_print.cardType')->leftJoin('waiting_print', 'waiting_print.memberId', '=', 'members.id');
        if (in_array($arrConditions[self::STATUS], [90 ,91, 92, 9])) {
            if ($arrConditions[self::STATUS] == 91) {
                $query->where('waiting_print.cardType', 1);
            }
            else if ($arrConditions[self::STATUS] == 92) {
                $query->where('waiting_print.cardType', 2);
            }
            else if ($arrConditions[self::STATUS] == 90) {
                $query->where('waiting_print.cardType', 0);
            }
			 else {
				  // search danh sách hội viên có trạng thái chờ in thẻ bao gồm in mới, in gia hạn, in cấp lại
				  $query->where('waiting_print.cardType', [90 ,91, 92]);
			 }
        }
        $query->where(self::PHONE_VERIFIED, 1)->where(self::ACCEPT_TERMS_AND_POLICIES, 1);

        $arrKeyConditions = array_keys($arrConditions);

        if (in_array(self::STATUS, $arrKeyConditions) && !empty($arrConditions[self::STATUS])) {
            // search theo trạng thái được lựa chọn
            if (in_array($arrConditions[self::STATUS], [6, 61])) {
                $query->where('members.status' , $arrConditions[self::STATUS]);
            }
        } else {
            // list  all hồ sơ có status = 9  chờ in thẻ và status = 6 chờ cấp mã thẻ hội viên
            // $statusDict = MemberConstants::$file_membership_card_issuer_create_card_const;
            // $query->whereIn('members.status' , self::extractStatusValues($statusDict));
            $query->where(function($q) {
                $q->whereIn('members.status' , [6, 61])
                ->orWhereIn('waiting_print.cardType', [0, 1, 2]);
            });
        }

        if(!empty($arrConditions[self::PROVINCE_CODE])) {
            MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
        }

        if (in_array(self::PROVINCE_TYPE, array_keys($arrConditions)) && !empty($arrConditions[self::PROVINCE_TYPE])) {
            //$arrBranchesIds = Offices::getProvinceCodeByProvinceType($arrConditions[self::PROVINCE_TYPE]);
            $query->whereIn(self::PROVINCE_CODE, $arrConditions[self::PROVINCE_TYPE]);
        }

        self::searchByCreatedAtMember($arrConditions, $query);

        self::searchByCommon($arrConditions, $query);

        $query->where('members.is_delete' , null);
        $query->orderBy('members.' . self::FILE_CODE, 'asc');

        if(!$isCount){
            $objService = $query->paginate($limit);
        }else {
            $objService = $query->get();
        }

        return $objService;
    }

    public static function searchByConditionsForPrintCard($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        if (!empty($arrConditions[self::IS_PRINTING])) {
            if (in_array($arrConditions[self::IS_PRINTING], [90 ,91, 92, 998])) {
                $query = WaitingPrint::select(DB::raw('NULL as printDate, NULL as pCardType'), 'waiting_print.cardType as wCardType');
                $query->join('members', 'waiting_print.memberId', '=', 'members.id');
                if ($arrConditions[self::IS_PRINTING] == 998) {
                    $query->whereIn('waiting_print.cardType', [0,1,2]);
                }else if ($arrConditions[self::IS_PRINTING] == 91) {
                    $query->where('waiting_print.cardType', 1);
                }
                else if ($arrConditions[self::IS_PRINTING] == 92) {
                    $query->where('waiting_print.cardType', 2);
                }
                else {
                    $query->where('waiting_print.cardType', 0);
                }
            }

            if (in_array($arrConditions[self::IS_PRINTING], [130 ,131, 132, 999])) {
                $query = Printed::select('printed.created_at as printDate', DB::raw('NULL as wCardType'), 'printed.cardType as pCardType');
                $query->join('members', 'printed.memberId', '=', 'members.id');
                if ($arrConditions[self::IS_PRINTING] == 999) {
                    $query->whereIn('printed.cardType', [0,1,2]);
                }else if ($arrConditions[self::IS_PRINTING] == 131) {
                    $query->where('printed.cardType', 1);
                }
                else if ($arrConditions[self::IS_PRINTING] == 132) {
                    $query->where('printed.cardType', 2);
                }
                else {
                    $query->where('printed.cardType', 0);
                }
            }
            $query->addSelect('members.*');
            self::searchPrintCardCommon($query, $arrConditions);

            $query = ListMemberServiceProvider::filterBranchIdByRole($query);

            if (!$isCount){
                $objPrintCard = $query->paginate($limit);
            } else {
                $objPrintCard = $query->get();
            }
        }
        else if (empty($arrConditions[self::IS_PRINTING]) || !isset($arrConditions[self::IS_PRINTING])) {
            $page = \Request::input('page', 1);
            $queryWaitingPrint = DB::table('waiting_print')->select('members.file_code', 'members.id', 'members.member_code', 'members.fullName', 'members.birthday', 'members.member_from', 'members.touristGuideCode', 'members.status', DB::raw('NULL as printDate, NULL as pCardType'), 'waiting_print.cardType as wCardType');
            $queryWaitingPrint->join('members', 'waiting_print.memberId', '=', 'members.id');
            $queryWaitingPrint->whereIn('waiting_print.cardType', [0, 1, 2]);
            self::searchPrintCardCommon($queryWaitingPrint, $arrConditions);
            $queryWaitingPrint = ListMemberServiceProvider::filterBranchIdByRole($queryWaitingPrint);

            $query = DB::table('printed')->select('members.file_code', 'members.id', 'members.member_code', 'members.fullName', 'members.birthday', 'members.member_from', 'members.touristGuideCode', 'members.status', 'printed.created_at as printDate', 'printed.cardType as pCardType', DB::raw('NULL as wCardType'));
            $query->join('members', 'printed.memberId', '=', 'members.id');
            $query->whereIn('printed.cardType', [0, 1, 2]);
            self::searchPrintCardCommon($query, $arrConditions);
            $query = ListMemberServiceProvider::filterBranchIdByRole($query);

            $items = $query->unionAll($queryWaitingPrint)->get();
            
            if (!$isCount){
                $objPrintCard = new LengthAwarePaginator(
                    $items->forPage($page, $limit), $items->count(), $limit, $page
                );
                $objPrintCard->setPath(request()->url());
            } else {
                $objPrintCard = $items;
            }
        }

        return $objPrintCard ?? [];
    }

    public static function searchPrintCardCommon(&$query, $arrConditions) {
        $query->where(self::PHONE_VERIFIED, 1)
              ->where(self::ACCEPT_TERMS_AND_POLICIES, 1);

        if(!empty($arrConditions[self::PROVINCE_CODE])) {
            MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
        }

        if (in_array(self::PROVINCE_TYPE, array_keys($arrConditions)) && !empty($arrConditions[self::PROVINCE_TYPE])) {
            $query->whereIn(self::PROVINCE_CODE, $arrConditions[self::PROVINCE_TYPE]);
        }

        self::searchByCreatedAtMember($arrConditions, $query);
        // theo văn phòng đại diện

        self::searchByCommon($arrConditions, $query);

        $query->where('members.is_delete' , null);
        $query->where('members.status', MemberConstants::MEMBER_OFFICIAL_MEMBER);
        $query->orderBy('members.' . self::MEMBER_CODE, 'desc');
    }

    /**
     * Search by condition, always filter by province_code
     *
     * @param $arrConditions
     * @param int $limit
     * @return mixed
     */
    public static function searchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $objMember = Member::where(MemberServiceProvider::PHONE_VERIFIED, 1)
                 ->where(MemberServiceProvider::ACCEPT_TERMS_AND_POLICIES, 1);

        self::searchByCommon($arrConditions, $objMember);
        // search by ngày tạo hồ sơ
        self::searchByCreatedAtMember($arrConditions, $objMember);
        if(!empty($arrConditions[self::PROVINCE_CODE])) {
            MemberServiceProvider::searchByProvinceCode($arrConditions, $objMember);
        }
        if (in_array(self::PROVINCE_TYPE, array_keys($arrConditions)) && !empty($arrConditions[self::PROVINCE_TYPE])) {
            //$arrBranchesIds = Offices::getProvinceCodeByProvinceType($arrConditions[self::PROVINCE_TYPE]);
            $objMember->whereIn(self::PROVINCE_CODE, $arrConditions[self::PROVINCE_TYPE]);
        }
        $vpddCode = auth()->user()->province_type;
        if (!empty($vpddCode)) {
            $branches = new BranchesRepository(new Branches());
            $arrTypeOfPlaceBelongToThisUser = $branches->getAllByParentId($vpddCode);
            if (!empty($arrTypeOfPlaceBelongToThisUser)) {
                $objMember->whereIn(self::PROVINCE_CODE, $arrTypeOfPlaceBelongToThisUser);
            }
        }

        if (!empty($arrConditions[self::STATUS])) {
            $objMember = $objMember->where('members.' . self::STATUS, $arrConditions[self::STATUS]);

        } else {
            if (UserServiceProvider::isExpertRole()) {
                $statusDict = MemberConstants::$file_expert_const;
            } else if (UserServiceProvider::isLeaderRole()) {
                $statusDict = MemberConstants::$file_leader_const;
            } else {
                abort(404);
            }
            $objMember = $objMember->whereIn('members.' . self::STATUS, self::extractStatusValues($statusDict));
        }

        $objMember = $objMember->where('members.is_delete' , null);
        $objMember = $objMember->orderBy('members.' . MemberServiceProvider::CREATED_AT, 'asc');

        if(!$isCount){
            $objMember = $objMember->paginate($limit);
        }else {
            $objMember = $objMember->get();
        }


        return $objMember;
    }

    public static function searchByVpd($arrConditions, $objMember)
    {
        if(in_array(self::PROVINCE_TYPE, array_keys($arrConditions)) && !empty($arrConditions[self::PROVINCE_TYPE])){
            $objMember->whereIn('members.' . self::PROVINCE_CODE,  $arrConditions['province_type']);
            // if ($arrConditions[self::PROVINCE_TYPE] == 1) {
            //     $objMember->whereIn('members.' . self::PROVINCE_CODE,  BranchConstants::$hn_typeOfPlace);
            // }
            // if ($arrConditions[self::PROVINCE_TYPE] == 2) {
            //     $objMember->whereIn('members.' . self::PROVINCE_CODE,  BranchConstants::$dn_typeOfPlace);
            // }
            // if ($arrConditions[self::PROVINCE_TYPE] == 3) {
            //     $objMember->whereIn('members.' . self::PROVINCE_CODE,  BranchConstants::$hcm_typeOfPlace);
            // }
        }

    }

    public static function searchByCommon($arrConditions, $objMember)
    {
        if (!empty($arrConditions[self::FILE_CODE])) {
            $objMember = $objMember->where(self::FILE_CODE, 'like', '%' . $arrConditions[self::FILE_CODE] . '%');
        }

        if (!empty($arrConditions[self::TOURIST_GUIDE_CODE])) {
            $objMember = $objMember->where(self::TOURIST_GUIDE_CODE, 'like', '%' . $arrConditions[self::TOURIST_GUIDE_CODE] . '%');
        }

        if (!empty($arrConditions[self::FULL_NAME])) {
            $objMember = $objMember->where(self::FULL_NAME, 'like', '%' . addslashes($arrConditions[self::FULL_NAME]) . '%');
        }

        if(in_array(self::MEMBER_CODE, array_keys($arrConditions)) && !empty($arrConditions[self::MEMBER_CODE])) {
            $objMember->where('members.member_code' , 'like', '%' . $arrConditions[self::MEMBER_CODE] . '%');
        }

        if (!empty($arrConditions['rank_status'])) {
            $objMember = $objMember->where('rank_status', '=', $arrConditions['rank_status']);
        }

        if (!empty($arrConditions['tourist_guide_type'])) {
            $objMember = $objMember->where('typeOfTravelGuide', '=', $arrConditions['tourist_guide_type']);
        }

        if (!empty($arrConditions['guideLanguage'])) {
            $objMember = $objMember->where('guideLanguage', '=', $arrConditions['guideLanguage']);
        }
    }

    public static function searchByProvinceCode($arrConditions, $objMember)
    {
        if(!empty($arrConditions[self::PROVINCE_CODE])) {
            $objMember = $objMember->where(self::PROVINCE_CODE, $arrConditions[self::PROVINCE_CODE]);
        }
    }

    public static function searchByProvinceWithRole($objMember, $branchesId)
    {
        $getAllProvinceType = array();
        foreach ($branchesId as $id) {
            $getProvinceType = array();
            $getProvinceCode = Offices::getProvinceCodeByProvinceType($id);
            foreach ($getProvinceCode as $provinceCode) {
                $getProvinceType[] = $provinceCode['id'];
            }

            $getAllProvinceType = array_merge($getAllProvinceType, $getProvinceType);
        }
        if (count($getAllProvinceType) == 0) {
            $typeOfPlaces[] = -1;
        }
        else {
            $typeOfPlaces = $getAllProvinceType;
        }

        if (!empty($typeOfPlaces)) {
            $objMember->whereIn(self::PROVINCE_CODE, $typeOfPlaces);
        }
    }

    public static function searchByCreatedAtMember($arrConditions, $objMember)
    {
        if (!empty($arrConditions[self::FROM_DATE]) || !empty($arrConditions[self::TO_DATE])) {
            if (empty($arrConditions[self::FROM_DATE])) {
                $objMember = $objMember->whereDate('members.' . self::CREATED_AT, '<=', $arrConditions[self::TO_DATE] . ' 23:59:59');
            } elseif (empty($arrConditions[self::TO_DATE])) {
                $objMember = $objMember->whereDate('members.' . self::CREATED_AT, '>=', $arrConditions[self::FROM_DATE] . ' 00:00:00');
            } else {
                $objMember = $objMember->whereBetween('members.' . self::CREATED_AT, [$arrConditions[self::FROM_DATE]  . ' 00:00:00', $arrConditions[self::TO_DATE] . ' 23:59:59']);
            }
        }

        if (isset($arrConditions[self::IS_PRINTING]) && in_array($arrConditions[self::IS_PRINTING], [130 ,131, 132], false)) {
            if (!empty($arrConditions['from_date_expiration']) || !empty($arrConditions['to_date_expiration'])) {
                if (empty($arrConditions['from_date_expiration'])) {
                    $objMember = $objMember->whereDate('printed.created_at', '<=', $arrConditions['to_date_expiration'] . ' 23:59:59');
                } elseif (empty($arrConditions['to_date_expiration'])) {
                    $objMember = $objMember->whereDate('printed.created_at', '>=', $arrConditions['from_date_expiration'] . ' 00:00:00');
                } else {
                    $objMember = $objMember->whereBetween('printed.created_at', [$arrConditions['from_date_expiration']  . ' 00:00:00', $arrConditions['to_date_expiration'] . ' 23:59:59']);
                }
            }
        }

        if (!empty($arrConditions[self::MEMBER_FROM])) {
            $objMember = $objMember->whereDate('members.' . self::MEMBER_FROM, '>=', $arrConditions[self::MEMBER_FROM] . '-01-01 00:00:00');
        }

        if (!empty($arrConditions['tourist_expiration_year'])) {
            $objMember = $objMember->whereDate('members.member_code_expiration', '>=', $arrConditions['tourist_expiration_year'] . '-01-01 00:00:00');
        }
    }

    public static function createMemberWithRegistering($arrMemberInfo)
    {
        DB::beginTransaction();
        try {
            $memberDetail = Member::where('touristGuideCode', $arrMemberInfo[MemberServiceProvider::TOURIST_GUIDE_CODE])->first();
            $memberObject = [
                    MemberServiceProvider::FULL_NAME => $arrMemberInfo[MemberServiceProvider::FULL_NAME],
                    'profileImg' => $arrMemberInfo['avatar'],
                    'birthday' => $arrMemberInfo['birthday'],
                    'gender' => $arrMemberInfo['gender'],
                    MemberServiceProvider::TOURIST_GUIDE_CODE => $arrMemberInfo[MemberServiceProvider::TOURIST_GUIDE_CODE],
                    'expirationDate' => $arrMemberInfo['expirationDate'],
                    'cmtCccd' => $arrMemberInfo['cmtCccd'],
                    'dateIssued' => $arrMemberInfo['dateIssued'],
                    'issuedBy' => $arrMemberInfo['issuedBy'],
                    'permanentAddress' => $arrMemberInfo['permanentAddress'],
                    'address' => $arrMemberInfo['address'],
                    'firstMobile' => $arrMemberInfo['firstMobile'],
                    'secondMobile' => $arrMemberInfo['secondMobile'],
                    'firstEmail' => $arrMemberInfo['firstEmail'],
                    'secondEmail' => $arrMemberInfo['secondEmail'],
                    MemberServiceProvider::TYPE_OF_TRAVEL_GUIDE => $arrMemberInfo[MemberServiceProvider::TYPE_OF_TRAVEL_GUIDE],
                    'typeOfPlace' => $arrMemberInfo['typeOfPlace'],
                    'experienceYear' => $arrMemberInfo['experienceYear'],
                    'inboundOutbound' => $arrMemberInfo['inboundOutbound'],
                    'experienceLevel' => $arrMemberInfo['experienceLevel'],
                    'otherSkills' => $arrMemberInfo['otherSkills'],
                    'otherInformation' => $arrMemberInfo['otherInformation'],
                    'touristGuideLevel' => $arrMemberInfo['touristGuideLevel'],
                    'achievements' => $arrMemberInfo['achievements'],
                    MemberServiceProvider::EMAIL_VERIFIED => 1,
                    MemberServiceProvider::PHONE_VERIFIED => 0,
                    'emailToken' => str_random(8),
                    'phoneToken' => Utils::randomString(6, false),
                    MemberServiceProvider::STATUS => $arrMemberInfo[MemberServiceProvider::STATUS],
                    MemberServiceProvider::PROVINCE_CODE => $arrMemberInfo[MemberServiceProvider::PROVINCE_CODE],
                    MemberServiceProvider::IS_VERIFIED => $arrMemberInfo[MemberServiceProvider::IS_VERIFIED],
                    MemberServiceProvider::IS_FEE => $arrMemberInfo[MemberServiceProvider::IS_FEE],
                    MemberServiceProvider::IS_SIGNED => $arrMemberInfo[MemberServiceProvider::IS_SIGNED],
                    MemberServiceProvider::MEMBER_TYPE => $arrMemberInfo[MemberServiceProvider::MEMBER_TYPE],
                    'guideLanguage' => rtrim(implode(",", $arrMemberInfo['guideLanguage']),","),
                    'forteTour' => rtrim(implode(",", $arrMemberInfo['forteTour']),","),
                    'province' => $arrMemberInfo['province'],
                    MemberServiceProvider::IS_DELETED => null,
                ];

            if($memberDetail) {
                $memberDetailNotDeletedStep2 = Member::where('touristGuideCode', $arrMemberInfo[MemberServiceProvider::TOURIST_GUIDE_CODE])->where('phoneVerified', 0)->where('acceptTermsAndPolicies', 0)->whereNull('is_delete')->first();

                $memberDetailNotDeletedStep3 = Member::where('touristGuideCode', $arrMemberInfo[MemberServiceProvider::TOURIST_GUIDE_CODE])->where('phoneVerified', 1)->where('acceptTermsAndPolicies', 0)->whereNull('is_delete')->first();

                if ($memberDetailNotDeletedStep2) {

                    $objMember = Member::updateOrCreate(['touristGuideCode' => $arrMemberInfo[MemberServiceProvider::TOURIST_GUIDE_CODE], 'phoneVerified' => 0, 'acceptTermsAndPolicies' => 0, 'is_delete' => NULL],$memberObject);
                }
                else if ($memberDetailNotDeletedStep3) {
                    $objMember = Member::updateOrCreate(['touristGuideCode' => $arrMemberInfo[MemberServiceProvider::TOURIST_GUIDE_CODE], 'phoneVerified' => 1, 'acceptTermsAndPolicies' => 0, 'is_delete' => NULL],$memberObject);
                }
                else {
                    $objMember = Member::create($memberObject);
                }

            } else {
                $objMember = Member::updateOrCreate(['touristGuideCode' => $arrMemberInfo[MemberServiceProvider::TOURIST_GUIDE_CODE]], $memberObject);
            }

            self::createEducations($arrMemberInfo['educations'], $objMember->id);
            self::createMajorSkills($arrMemberInfo['majorSkills'], $objMember->id);
            self::createLanguageSkills($arrMemberInfo['languageSkills'], $objMember->id);
            self::createWorkHistory($arrMemberInfo['workHistory'], $objMember->id);
            self::createElements($arrMemberInfo['elements'], $objMember->id);
            self::createAchievementFiles($arrMemberInfo['achievementFiles'], $objMember->id);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            LogsHelper::trackByFile('create_member_fail', 'Error when create new member (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r($arrMemberInfo, true) . ')');
            $objMember = null;
        }

        return $objMember;
    }
    //binhnd 26022018
    public static function updateMemberFromMemberTmp($arrMemberInfoFull)
    {
        $arrMemberInfo = (array)json_decode($arrMemberInfoFull->content);
        DB::beginTransaction();
        try {
            $objMember = Member::where('id', $arrMemberInfo['member_id'])->update([
                MemberServiceProvider::FULL_NAME => $arrMemberInfo[MemberServiceProvider::FULL_NAME],
                'status' => MemberConstants::MEMBER_OFFICIAL_MEMBER,
                'birthday' => $arrMemberInfo['birthday'],
                'profileImg' => $arrMemberInfo['avatar'],
                'gender' => $arrMemberInfo['gender'],
                MemberServiceProvider::TOURIST_GUIDE_CODE => $arrMemberInfo[MemberServiceProvider::TOURIST_GUIDE_CODE],
                'expirationDate' => $arrMemberInfo['expirationDate'],
                'cmtCccd' => $arrMemberInfo['cmtCccd'],
                'dateIssued' => $arrMemberInfo['dateIssued'],
                'issuedBy' => $arrMemberInfo['issuedBy'],
                'permanentAddress' => $arrMemberInfo['permanentAddress'],
                'address' => $arrMemberInfo['address'],
                'firstMobile' => $arrMemberInfo['firstMobile'],
                'secondMobile' => $arrMemberInfo['secondMobile'],
                'firstEmail' => $arrMemberInfo['firstEmail'],
                'secondEmail' => $arrMemberInfo['secondEmail'],
                MemberServiceProvider::TYPE_OF_TRAVEL_GUIDE => $arrMemberInfo[MemberServiceProvider::TYPE_OF_TRAVEL_GUIDE],
                'typeOfPlace' => $arrMemberInfo['typeOfPlace'],
                'experienceYear' => $arrMemberInfo['experienceYear'],
                'inboundOutbound' => $arrMemberInfo['inboundOutbound'],
                'experienceLevel' => $arrMemberInfo['experienceLevel'],
                'otherSkills' => $arrMemberInfo['otherSkills'],
                'otherInformation' => $arrMemberInfo['otherInformation'],
                'touristGuideLevel' => $arrMemberInfo['touristGuideLevel'],
                'achievements' => $arrMemberInfo['achievements'],
                MemberServiceProvider::PROVINCE_CODE => $arrMemberInfo[MemberServiceProvider::PROVINCE_CODE],
                'guideLanguage' => rtrim(implode(",", $arrMemberInfo['guideLanguage']),","),
                'forteTour' => rtrim(implode(",", $arrMemberInfo['forteTour']),","),
                'province' => $arrMemberInfo['province'],
                'province_code' => $arrMemberInfo['province_code'],
                'approved_by' => Auth::user()->id,
                'approved_at' => Carbon::now(),
                'createdAt' => $arrMemberInfoFull->createdAt
            ]);
            //save Educations
            isset($arrMemberInfo['educations'][0]->branchId) ? $branchId = $arrMemberInfo['educations'][0]->branchId : $branchId = '';
            isset($arrMemberInfo['educations'][0]->degreeId) ? $degreeId = $arrMemberInfo['educations'][0]->degreeId : $degreeId = '';
            self::updateEducationsTmp($arrMemberInfo['member_id'], $branchId, $degreeId);
            //save majorskill
            isset($arrMemberInfo['majorSkills']->ids) ? $major = $arrMemberInfo['majorSkills']->ids : $major = '';
            self::updateMajorSkillsTmp($arrMemberInfo['member_id'], $major);
            //save LanguageSkills
            isset($arrMemberInfo['languageSkills'][0]->languageId) ? $languageId = $arrMemberInfo['languageSkills'][0]->languageId : $languageId = '';
            isset($arrMemberInfo['languageSkills'][0]->levelId) ? $levelId = $arrMemberInfo['languageSkills'][0]->levelId : $levelId = '';
            self::updateLanguageSkillsTmp($arrMemberInfo['member_id'], $languageId, $levelId);
            //save history
            if(isset($arrMemberInfo['workHistory'][0])) self::updateWorkHistoryTmp($arrMemberInfo['member_id'], $arrMemberInfo['workHistory']);
            //save elements
            isset($arrMemberInfo['elements'][0]->groupSizeId) ? $groupSizeId = $arrMemberInfo['elements'][0]->groupSizeId : $groupSizeId = '';
            isset($arrMemberInfo['elements'][0]->typeGuideId) ? $typeGuideId = $arrMemberInfo['elements'][0]->typeGuideId : $typeGuideId = '';
            self::updateElementsTmp($arrMemberInfo['member_id'], $typeGuideId, $groupSizeId);
            self::onlyUpdateMemberIntoPdf($arrMemberInfo['member_id']);
            //delete member_tmp
            MemberTmp::where('member_id', '=', $arrMemberInfo['member_id'])->delete();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            LogsHelper::trackByFile('update_member_from_tmp_member_fail', 'Error when update new member (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r($arrMemberInfo, true) . ')');
            $objMember = null;
        }

        return $objMember;
    }

    private static function updateEducationsTmp($memberId, $branchId, $degreeId)
    {
        if($branchId > 0 && $degreeId > 0){
            Education::where(self::MEMBER_ID, $memberId)->update([
                'branchId' => $branchId,
                'degreeId' => $degreeId,
            ]);
        }
    }

    private static function updateMajorSkillsTmp($memberId, $majorId)
    {
        if($majorId > 0) {
            MajorSkill::where(self::MEMBER_ID, $memberId)->update([
                'majorId' => $majorId,
            ]);
        }
    }

    private static function updateLanguageSkillsTmp($memberId, $languageId, $levelId)
    {
        LanguageSkill::where(self::MEMBER_ID, $memberId)->update([
            'languageId' => $languageId,
            'levelId' => $levelId,
        ]);
    }

    private static function updateWorkHistoryTmp($memberId, $new_workHistory)
    {
        if(count($new_workHistory) > 0){
            //get data work -> member_id
            $workHistory =  WorkHistory::where('memberId', $memberId)->get()->toArray();
            if(count($workHistory) > 0){
                foreach($workHistory as $key => $value){
                    $objWork = Work::where('id', $value['elementId'])->first();
                    if (!empty($objWork)) {
                        //delete work
                        $objWork->delete();
                    }
                }
            }
            //delete workHistory
            WorkHistory::where('memberId', '=', $memberId)->delete();
            //save work, workhistory
            self::createWorkHistory($new_workHistory, $memberId);
        }

    }

    private static function updateElementsTmp($memberId, $typeGuideId, $groupSizeId)
    {
        Element::where(self::MEMBER_ID, $memberId)->update([
            'typeGuideId' => $typeGuideId,
            'groupSizeId' => $groupSizeId,
        ]);
    }

    private static function createMajorSkills($majorSkills, $memberId)
    {
        if (!empty($majorSkills['ids'])) {
            MajorSkill::updateOrCreate([MemberServiceProvider::MEMBER_ID => $memberId], [
                'majorId' => $majorSkills['ids'],
                MemberServiceProvider::MEMBER_ID => $memberId,
            ]);
        }

    }

    private static function createAchievementFiles($achievementFiles, $memberId)
    {
        if (!empty($achievementFiles)) {
            foreach ($achievementFiles as $achievementFile) {
                $storeFile = new StoreFile($achievementFile->getRealPath());
                $storeFile->moveToCollection();
                $achievementFileModel = File::create([
                    MemberServiceProvider::ORIGINAL_NAME => $achievementFile->getClientOriginalName(),
                    MemberServiceProvider::ABSOLUTE_PATH => $storeFile->getRealPath(),
                    MemberServiceProvider::RELATIVE_URL => $storeFile->getRelativeUrl(),
                    'mime' => $storeFile->getFileInfo()->getMimeType(),
                    'size' => $storeFile->getFileInfo()->getSize(),
                    MemberServiceProvider::STATUS => 1,
                ]);

                if (!empty($memberId) && !empty($achievementFileModel->id)) {
                    AchievementFile::create([
                        MemberServiceProvider::MEMBER_ID => $memberId,
                        MemberServiceProvider::FILE_ID => $achievementFileModel->id,
                    ]);
                }
            }
        }
    }

    private static function createEducations($educations, $memberId)
    {
        if (!empty($educations)) {
            foreach ($educations as $education) {
                $educationFileId = null;
                if (!empty($education['file'])) {
                    $file = $education['file'];
                    $storeFile = new StoreFile($file->getRealPath());
                    $storeFile->moveToCollection();
                    $educationFile = File::create([
                        MemberServiceProvider::ORIGINAL_NAME => $file->getClientOriginalName(),
                        MemberServiceProvider::ABSOLUTE_PATH => $storeFile->getRealPath(),
                        MemberServiceProvider::RELATIVE_URL => $storeFile->getRelativeUrl(),
                        'mime' => $storeFile->getFileInfo()->getMimeType(),
                        'size' => $storeFile->getFileInfo()->getSize(),
                        MemberServiceProvider::STATUS => 1,
                    ]);
                    $educationFileId = $educationFile->id;
                }

                if (!empty($education['branchId']) && !empty($education['degreeId'])) {
                    Education::create([
                        'branchId' => $education['branchId'],
                        'degreeId' => $education['degreeId'],
                        MemberServiceProvider::FILE_ID => $educationFileId,
                        MemberServiceProvider::MEMBER_ID => $memberId,
                    ]);
                }
            }
        }
    }

    private static function createLanguageSkills($languageSkills, $memberId)
    {
        if (!empty($languageSkills)) {
            foreach ($languageSkills as $languageSkill) {
                $langugeSkillFileId = null;
                if (!empty($languageSkill['file'])) {
                    $file = $languageSkill['file'];
                    $storeFile = new StoreFile($file->getRealPath());
                    $storeFile->moveToCollection();
                    $languageSkillFile = File::create([
                        MemberServiceProvider::ORIGINAL_NAME => $file->getClientOriginalName(),
                        MemberServiceProvider::ABSOLUTE_PATH => $storeFile->getRealPath(),
                        MemberServiceProvider::RELATIVE_URL => $storeFile->getRelativeUrl(),
                        'mime' => $storeFile->getFileInfo()->getMimeType(),
                        'size' => $storeFile->getFileInfo()->getSize(),
                        MemberServiceProvider::STATUS => 1,
                    ]);
                    $langugeSkillFileId = $languageSkillFile->id;
                }

                if (!empty($languageSkill['languageId']) && !empty($languageSkill['levelId'])) {
                    LanguageSkill::updateOrCreate([MemberServiceProvider::MEMBER_ID => $memberId], [
                        'languageId' => $languageSkill['languageId'],
                        'levelId' => $languageSkill['levelId'],
                        MemberServiceProvider::FILE_ID => $langugeSkillFileId,
                        MemberServiceProvider::MEMBER_ID => $memberId,
                    ]);
                }
            }
        }
    }

    private static function createElements($elements, $memberId)
    {
        if (!empty($elements)) {
            foreach ($elements as $element) {
                if (!empty($element['typeGuideId']) && !empty($element['groupSizeId'])) {
                    Element::create([
                        'typeGuideId' => $element['typeGuideId'],
                        'groupSizeId' => $element['groupSizeId'],
                        MemberServiceProvider::MEMBER_ID => $memberId,
                    ]);
                }
            }
        }
    }

    private static function createWorkHistory($workHistory, $memberId)
    {
        if (!empty($workHistory)) {
            $work_ids = [];
            foreach ($workHistory as $item) {
                $item = (array) $item;
                $work = Work::updateOrCreate([
                    'elementName' => $item['elementName'],
                    'typeOfContract' => $item['typeOfContract'],
                    'fromDate' => $item['fromDate'],
                    'toDate' => $item['toDate']
                ]);
                $work_ids[] = $work->id;
            }
            if (!empty($memberId)) {
                $member = Member::find($memberId);
                $member->works()->sync($work_ids);
            }
        }
    }

    public static function verifyEmailToken($memberId, $emailToken)
    {
        try {
            $member = Member::where('id', $memberId)
                ->where('emailToken', $emailToken)
                ->first();

            if (empty($member)) {
                return null;
            }

            $member->emailVerified = 1;
            $member->save();
            return $member;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('verify_email_token_fail', 'Error when verify email token (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId, $emailToken], true) . ')');
            return null;
        }
    }

    public static function verifyPhoneToken($memberId, $phoneToken)
    {
        try {
            $member = Member::where('id', $memberId)
                ->where('phoneToken', $phoneToken)
                ->first();

            if (empty($member)) {
                return null;
            }

            $member->phoneVerified = 1;
            $member->save();
            return $member;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('verify_phone_token_fail', 'Error when verify phone token (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId, $phoneToken], true) . ')');
            return null;
        }
    }

    public static function acceptTermsAndPolicies($memberId)
    {
        try {
            $member = Member::where('id', $memberId)->first();

            if (empty($member)) {
                return null;
            }

            $member->acceptTermsAndPolicies = 1;
            $member->createdAt = Carbon::now();
            $member->save();
            return $member;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('accept_terms_and_policies_fail', 'Error when accept terms and policies (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    /**
     *get phoneToken from memberId
     */
    public static function getPhoneToken($memberId)
    {
        $member = Member::where('id', $memberId)->first();
        return $member->phoneToken;
    }

    public static function sendMailToken($memberId)
    {
        $member = Member::where('id', $memberId)->first();
        $emailToken = $member->emailToken;
        $memberMail = $member->firstEmail;

        try {
            $to = [['address' => $memberMail, 'name' => $member->fullName]];
            $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
            $subject = 'Thông báo mã xác nhận địa chỉ email đăng ký hội viên Hội Hướng dẫn viên Du lịch Việt Nam';
            $data = ['mailToken'=> $emailToken];
            $template = 'member.register.sendmail';
            dispatch((new SendEmail($template, $data, $to, $bcc, $subject))->delay(5));
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('send_mail_verification_register_fail', 'Error when send mail verify register (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
        }
    }

    public static function sendWelcomeMail($memberId)
    {
        $objMember  = Member::where('id', $memberId)->first();
        $firstMail = $objMember->firstEmail;
        $secondMail = $objMember->secondEmail;

        self::_sendWelcomeMail($objMember, $firstMail);

        if (!empty($secondMail)) {
            self::_sendWelcomeMail($objMember, $secondMail);
        }
    }

    private static function _sendWelcomeMail ($objMember, $email)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = '';
                // $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL02')->first();
                $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL02')->where('branch_id', $objMember->province_code)->first();
                
                if (empty($getEmail)) {
                    $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL02')->where('branch_id', self::TOANQUOC)->first();
                }
                $attach = null;
                
                if (!empty($getEmail)) {
                    $subject = $getEmail->title;
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
    
                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }
                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
                $template = 'member.register.thankyou_email';
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject, $attach))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_welcome_register_fail', 'Error when send mail verify register (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember], true) . ')');
            }
        }
    }

    public static function sendInformFee($objMember)
    {
        $firstMail = $objMember->firstEmail;
        $secondMail = $objMember->secondEmail;

        $template = 'admin.member.mail.inform_fee';
        $subject = 'Thông báo nộp Hội phí Hội Hướng Dẫn Viên Du Lịch Việt Nam năm ' . config('vita.year_of_fee');
        self::_sendEmailWith($template, $objMember, $subject, $firstMail);

        if (!empty($secondMail)) {
            self::_sendEmailWith($template, $objMember, $subject, $secondMail);
        }
    }

    private static function _sendEmailWith($template, $objMember, $subject, $email)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $data = ['objMember'=> $objMember];
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile(str_replace('.', '_', $template), 'Error when send mail (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember], true) . ')');
            }
        }
    }

    /**
     * Send mail yêu cầu update hồ sơ
     *
     * @param $memberId
     */
    public static function sendRequiredUpdateBriefMail($memberId)
    {
        $objMember  = Member::where('id', $memberId)->first();
        $firstMail = $objMember->firstEmail;
        $secondMail = $objMember->secondEmail;
        self::_sendRequestUpdateMail($objMember, $firstMail);

        if (!empty($secondMail)) {
            self::_sendRequestUpdateMail($objMember, $secondMail);
        }

    }

    private static function _sendRequestUpdateMail($objMember, $email)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = 'Thông báo yêu cầu bổ sung hồ sơ đăng ký hội viên online';
                $data = ['objMember' => $objMember];
                $template = 'admin.member.mail.required_update_brief_mail';
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_required_update_brief_mail', 'Error when send mail required_update_brief_mail (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember->id], true) . ')');
            }
        }
    }

    public static function sendApprovedMail($memberId, $mailTemplate = 'hn_approved_mail')
    {
        $objMember  = Member::where('id', $memberId)->first();
        $firstMail = $objMember->firstEmail;
        $secondMail = $objMember->secondEmail;
        self::_sendApprovedMail($objMember, $firstMail, $mailTemplate);

        if (!empty($secondMail)) {
            self::_sendApprovedMail($objMember, $secondMail, $mailTemplate);
        }

    }

    private static function _sendApprovedMail($objMember, $email, $mailTemplate)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = 'Thông báo hồ sơ ' . (empty($objMember->file_code) ? '' : $objMember->file_code) .' đăng ký gia nhập Hội Du Lịch Việt Nam đã được phê duyệt';

                $act1 = action('MemberController@downloadContractFile', ['fileName' => md5($objMember->id) . '_detail.pdf', 'fileNameSaved' => 'Phiếu thông tin']);
                $phieuthongtinhoivien = '<a href="'.$act1.'">Phiếu thông tin hội viên</a>';
				$taiphieu = '<a href="'.$act1.'">Tải về</a>';

                $act2 = action('MemberController@downloadContractFile', ['fileName' => md5($objMember->id) . '.pdf', 'fileNameSaved' => 'Đơn đăng ký']);
                $dondangky = '<a href="'.$act2.'">Đơn đăng kí gia nhập Hội HDVDL VN</a>';
				$taidondk = '<a href="'.$act2.'">Tải về</a>';

                $getBranchParentId = Branches::select('parent_id')->where('id', $objMember->province_code)->first();
                if (!empty($getBranchParentId)) {
                    if ($getBranchParentId->parent_id == null) {
                        $getBranchArea = 'CLB';
                    }
                    else {
                        $getBranchArea = Branches::select('area')->where('id', $getBranchParentId->parent_id)->first();
                    }
                }

                // if (!empty($getBranchArea)) {
                //     if ($getBranchArea == 'CLB') {
                //         $getEmail = Emails::select('content', 'attach')->where('option_code', self::EMAIL01)->where('branch_id', self::HANOI)->first();
                //     }
                //     else {
                //         if ($getBranchArea->area == 'HN') {
                //             $getEmail = Emails::select('content', 'attach')->where('option_code', self::EMAIL01)->where('branch_id', self::HANOI)->first();
                //         }
                //         else if ($getBranchArea->area == 'DN')
                //         {
                //             $getEmail = Emails::select('content', 'attach')->where('option_code', self::EMAIL01)->where('branch_id', self::DANANG)->first();
                //         }
                //         else {
                //             $getEmail = Emails::select('content', 'attach')->where('option_code', self::EMAIL01)->where('branch_id', self::HCM)->first();
                //         }
                //     }
                // }
                $getEmail = Emails::select('id','content', 'attach')->where('option_code', self::EMAIL01)->where('branch_id', $objMember->province_code)->first();
                if (empty($getEmail)) {
                    $getEmail = Emails::select('id','content', 'attach')->where('option_code', self::EMAIL01)->where('branch_id', self::TOANQUOC)->first();
                }
                $feeMoney = MailHelper::getFeeMoney($getEmail->id);
                $attach = null;
                if (!empty($getEmail)) {
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_MAHOSO, empty($objMember->file_code) ? '' : $objMember->file_code, $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_URL_PHIEUTHONGTIN, $phieuthongtinhoivien, $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_URL_DONDANGKY, $dondangky, $getEmail->content);
					$getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TAI_DK, $taidondk, $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TAI_PHIEU, $taiphieu, $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_HOIPHI, $feeMoney, $getEmail->content);

                    if (auth()->check()) {
                        $branchId = auth()->user()->branch_id ?? null;
                        if (! $branchId) {
                            $branchObj = Branches::query()
                                ->select('name')
                                ->where('id', $branchId)
                                ->where('code', 'NOT LIKE', '%CLB%')
                                ->first();
                            if ($branchObj) {
                                $getEmail->content = str_replace('Hội Hướng dẫn viên Du lịch Việt Nam phê duyệt', 'Chi hội '. $branchObj->name . ' phê duyệt', $getEmail->content);
                            }
                        }
                    }


                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }

                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];

                $template = 'admin.member.mail.' . $mailTemplate;
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject, $attach))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_approved', 'Error when send mail approved (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember->id], true) . ')');
            }
        }
    }

    public static function sendRejectMail($memberId, $mailTemplate = 'hn_reject_mail')
    {
        $objMember  = Member::where('id', $memberId)->first();
        $firstEmail = $objMember->firstEmail;
        $secondMail = $objMember->secondEmail;
        self::_sendRejectMail($objMember, $firstEmail, $mailTemplate);

        if (!empty($secondMail)) {
            self::_sendRejectMail($objMember, $secondMail, $mailTemplate);
        }
    }

    private static function _sendRejectMail($objMember, $email, $mailTemplate)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = 'Thông báo hồ sơ đăng ký gia nhập Hội Du Lịch Việt Nam đã bị từ chối';
                $data = ['objMember' => $objMember];
                $template = 'admin.member.mail.' . $mailTemplate;
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_reject', 'Error when send mail approved (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember->id], true) . ')');
            }
        }
    }

    public static function sendUpdateInfoMail($memberId, $mailTemplate = 'hn_update_info_mail')
    {
        $objMember  = Member::where('id', $memberId)->first();
        $firstEmail = $objMember->firstEmail;
        $secondMail = $objMember->secondEmail;
        self::_sendUpdateInfoMail($objMember, $firstEmail, $mailTemplate);

        if (!empty($secondMail)) {
            self::_sendUpdateInfoMail($objMember, $secondMail, $mailTemplate);
        }
    }

    private static function _sendUpdateInfoMail($objMember, $email, $mailTemplate)
    {
        if (!empty($email)) {
            try {
                // Send approve mail
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = 'Thông báo hồ sơ đăng ký gia nhập Hội Du Lịch Việt Nam cần được bổ sung';
                $data = ['objMember' => $objMember];
                $template = 'admin.member.mail.' . $mailTemplate;
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_update_info', 'Error when send mail approved (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember->id], true) . ')');
            }
        }
    }

    public static function sendCreateMemberCardSuccessMail ($memberId,$password, $newUsername)
    {
        $objMember  = Member::where('id', $memberId)->first();
        $firstEmail = $objMember->firstEmail;
        $secondMail = $objMember->secondEmail;
        self::_sendCreateMemberCardSuccessMail($objMember, $firstEmail, $password, $newUsername);

        if (!empty($secondMail)) {
            self::_sendCreateMemberCardSuccessMail($objMember, $secondMail ,$password, $newUsername);
        }

    }

    private static function _sendCreateMemberCardSuccessMail($objMember, $email, $password, $newUsername)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = '';
                $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL10')->where('branch_id', $objMember->province_code)->first();
                if (empty($getEmail)) {
                    $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL10')->where('branch_id', self::TOANQUOC)->first();
                }
                $attach = null;
                if (!empty($getEmail)) {
                    $subject = $getEmail->title;
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_MAHOIVIEN, htmlentities($objMember->member_code), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_USERNAME, htmlentities($newUsername), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_PASSWORD, htmlentities($password), $getEmail->content);
    
                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }
                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
                $template = 'admin.member.mail.create_member_card_success';
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject, $attach))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_create_member_card_success', 'Error when send mail create member card success (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember->id], true) . ')');
            }
        }
    }

    public static function updateMemberStatus($memberId, $status)
    {
        try {
            $member = Member::where('id', $memberId)->first();

            if (empty($member) || empty($status)) {
                return null;
            }

            $member->status = $status;
            if ($status == MemberConstants::APPROVAL_WAITING) {
                $member->verified_by = Auth::user()->id;
                $member->verified_at = Carbon::now();
            } elseif ($status == MemberConstants::FEE_WAITING) {
                $arrMemberFileCode = self::genMemberFileCode();

                $member->approved_by = Auth::user()->id;
                $member->approved_at = Carbon::now();
                $member->file_code = $arrMemberFileCode['file_code'];

                // re update increase code
                CodeServiceProvider::updateOrCreate('C', $arrMemberFileCode['current_code']);
            }

            $member->save();
            return $member;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_status_fail', 'Error when update member status (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }


    public static function updateVerify($memberId) {
        try {
            $member = Member::where('id', $memberId)->first();

            if (empty($member)) {
                return null;
            }

            $member->verified_by = Auth::user()->id;
            $member->verified_at = Carbon::now();
            $member->save();
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_verify_at', 'Error when update member verify (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }

    }

    public static function updateApprove($memberId) {
        try {
            $member = Member::where('id', $memberId)->first();

            if (empty($member)) {
                return null;
            }

            $member->approved_by = Auth::user()->id;
            $member->approved_at = Carbon::now();
            $member->save();
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_approve', 'Error when update member approve (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }
    //binhnd update 25022018
    public static function updateMemberStatusNotCheck($memberId, $status)
    {

        if(empty($status))
            return null;

        $member = Member::where('id', $memberId)->first();

        if (empty($member))
            return null;

        $member = Member::where('id', $memberId)->first();

        try {
            $member->status = $status;
            $member->save();
            return $member;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_status_fail', 'Error when update member status (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    public static function updateMemberStatusByAccount($memberId, $status)
    {
        try {
            $member = Member::where('id', $memberId)->first();

            if (empty($member) || empty($status)) {
                return null;
            }

            $member->status = $status;
            $member->is_signed = NULL;
            if ($status == MemberConstants::FEE_WAITING) {
                $member->is_fee = null;
            } elseif ($status == MemberConstants::SIGNING_WAITING) {
                $member->is_fee = 1;
            }
            $member->save();
            return $member;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_status_fail', 'Error when update member status (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    public static function genMemberFileCode ($fileCodePrefix = 'C')
    {
        $objLastCode = CodeServiceProvider::getCurrentCodeByPrefix($fileCodePrefix);
        if (!empty($objLastCode)) {
            $lastFileCode = $objLastCode;
        }
        else {
            $lastFileCode = 0;
        }
        $lastFileCode = (int) $lastFileCode;
        $lastFileCode ++;
        $lastFileCode = (string) $lastFileCode;

        // Member file code has max 5 digits
        if (strlen($lastFileCode) > 6) {
            LogsHelper::trackByFile('create_member_file_code_fail', 'Error when create member file code, Data last member: ' . $objLastCode);
        }

        $fileCode = '00000';
        $fileCode = substr($fileCode, 0, strlen($fileCode) - strlen($lastFileCode));

        return [
            'file_code'    => $fileCodePrefix . $fileCode . (string) $lastFileCode,
            'current_code' => $lastFileCode
        ];
    }

    public static function updateMemberWhenDoCard ($memberId, $arrInfoUpdate)
    {
        try {
            LogsHelper::trackByFile('create_member_code_fail', 'Current Code 2: ' . $arrInfoUpdate['member_code']);

            $member = Member::where('id', $memberId)->first();

            if (empty($member) || empty($arrInfoUpdate)) {
                return null;
            }

            if (isset($arrInfoUpdate[MemberServiceProvider::IS_VERIFIED])) {
                $member->is_verified = $arrInfoUpdate[MemberServiceProvider::IS_VERIFIED];
            }

            if (isset($arrInfoUpdate[MemberServiceProvider::IS_FEE])) {
                $member->is_fee = $arrInfoUpdate[MemberServiceProvider::IS_FEE];
            }

            if (isset($arrInfoUpdate[MemberServiceProvider::IS_SIGNED])) {
                $member->is_signed = $arrInfoUpdate[MemberServiceProvider::IS_SIGNED];
            }

            if (isset($arrInfoUpdate[MemberServiceProvider::MEMBER_TYPE])) {
                $member->member_type = $arrInfoUpdate[MemberServiceProvider::MEMBER_TYPE];
            }

            if (isset($arrInfoUpdate['member_code'])) {
                $member->member_code = $arrInfoUpdate['member_code'];
            }

            if (isset($arrInfoUpdate['member_qr_id'])) {
                $member->member_qr_id = $arrInfoUpdate['member_qr_id'];
            }

            if (isset($arrInfoUpdate['member_from'])) {
                $member->member_from = $arrInfoUpdate['member_from'];
            }

            if (isset($arrInfoUpdate[self::STATUS])) {
                $member->status = $arrInfoUpdate[self::STATUS];
            }

            $member->save();
            return $member;
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_when_do_card_fail', 'Error when update member do card (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    /**
     * Create member code by last member code and typeOfTravelGuide code as member code prefix
     *
     * @param $memberPrefixType
     * @return array|null
     */
    public static function genMemberCode ($memberPrefixType)
    {
        if (empty($memberPrefixType)) {
            LogsHelper::trackByFile('create_member_code_fail', 'Error when create member code, empty typeOfTravelGuide');
            return null;
        }

        $objLastCode = CodeServiceProvider::getCurrentCodeByPrefix($memberPrefixType);

        if (!empty($objLastCode)) {
            $lastMemberCode = $objLastCode;
        } else {
            $lastMemberCode = 0;
        }

        $lastMemberCode = (int) $lastMemberCode;
        $lastMemberCode ++;
        $lastMemberCode = (string) $lastMemberCode;

        /*// Member file code has max 5 digits
        if (strlen($lastMemberCode) > 6) {
            LogsHelper::trackByFile('create_member_code_fail', 'Error when create member code, Data last member: ' . $objLastMember);
        }*/

        $memberCode = '00000';
        $memberCode = substr($memberCode, 0, strlen($memberCode) - strlen($lastMemberCode));
        LogsHelper::trackByFile('create_member_code_fail', 'Current Code 1: ' . $memberCode);
        return [
            'member_code'  => (string)$memberPrefixType . $memberCode . (string) $lastMemberCode,
            'current_code' => $lastMemberCode
        ];
    }

    public static function onlyUpdateMemberIntoPdf ($memberId)
    {
        $objMember = Member::where('id', $memberId)->first();
        try {

            $pdfFullMemberInfo  =  DomPDF::loadView('admin.member.pdf.member_info', compact('objMember'));
            $outputPdfFullMemberInfo  = $pdfFullMemberInfo->output();
            file_put_contents(storage_path('pdf/' . md5($memberId) .'_detail.pdf'), $outputPdfFullMemberInfo);
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('save_pdf_file_fail', 'Error when save data into pdf status (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    public static function saveDataMemberIntoPdf($memberId)
    {
        $objMember = Member::where('id', $memberId)->first();
        $typeOfTravelGuide  = empty($objMember->typeOfTravelGuide) ? '' : $objMember->typeOfTravelGuide;
        $typeOfPlace = empty($objMember->typeOfPlace) ? '' : $objMember->typeOfPlace;

        $objEducation = MemberServiceProvider::getEducationByMemberId($objMember->id);
        $objLanguageSkills = MemberServiceProvider::getLanguageAchievedByMemberId($objMember->id);
        // các bảng này quan hệ với member loại 1 - 1
        $objMajorSkills =  MajorSkill::where('memberId', $objMember->id)->first();
        if (!empty($objMajorSkills) ) {
            $objMajor = Major::where('id', $objMajorSkills->majorId)->first();
        }
        $workHistories = MemberServiceProvider::getWorkHistoryByMemberId($objMember->id);
        $objElement = MemberServiceProvider::getElementByMemeber($objMember);
        $paymentInfor = \App\Models\MemberPayments::where('memberId', $objMember->id)->first();

        $objBrand = empty($objEducation[CommontConstants::BRAND]) ? '' : $objEducation[CommontConstants::BRAND];
        $objDegree = empty($objEducation[CommontConstants::DEGREE]) ? '' : $objEducation[CommontConstants::DEGREE];
        $objMajor = empty($objMajor) ? '' : $objMajor;
        $objLanguage = empty($objLanguageSkills[CommontConstants::LANGUAGE]) ? '' : $objLanguageSkills[CommontConstants::LANGUAGE];
        $objLanguageLevel = empty($objLanguageSkills[CommontConstants::LANGUAGE_LEVEL]) ? '' : $objLanguageSkills[CommontConstants::LANGUAGE_LEVEL];
        $workHistories = empty($workHistories) ? '' : $workHistories;
        $typeGuideName = empty($objElement[CommontConstants::TYPE_GUIDE_NAME]) ? '' : $objElement[CommontConstants::TYPE_GUIDE_NAME];
        $groupSizeName = empty($objElement[CommontConstants::GROUP_SIZE_NAME]) ? '' : $objElement[CommontConstants::GROUP_SIZE_NAME];
        $guideLanguage = empty($objMember->guideLanguageName) ? '' : $objMember->guideLanguageName;
        $profileImg    =  empty($objMember->profileImg) ? '' : $objMember->profileImg;
        $objVerifiedBy  = \App\Providers\UserServiceProvider::getUserById($objMember->verified_by);
        $verifiedByName = !empty($objVerifiedBy) ? $objVerifiedBy->fullname : '';

        $objApprovedBy  = \App\Providers\UserServiceProvider::getUserById($objMember->approved_by);
        $approvedByName = !empty($objApprovedBy) ? $objApprovedBy->fullname : '';

        $provinceType = Auth::user()->province_type;
        if ($provinceType == BranchConstants::TYPE_OF_PLACE_HN) {
            $state = 'Hà Nội';
        } elseif ($provinceType == BranchConstants::TYPE_OF_PLACE_DN) {
            $state = 'Đà Nẵng';
        } else {
            $state = 'TP Hồ Chí Minh';
        }
        $languages = Language::getAllLanguages();
        $forteTours = ForteTour::getAllForteTour();
        $memberOfficial = MemberConstants::MEMBER_OFFICIAL_MEMBER;
        $otherConstant = $workConstant = $provincial = $chihoi = $clbthuochoi = [];
        $otherTmp = OtherConstant::where('status', 1)->orderBy('otherName', 'asc')->get();
        $workTmp = WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get();
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($otherTmp)){
            foreach($otherTmp as $key => $value){
                $otherConstant[$value->id] = $value->otherName;
            }
        }
        if(!empty($workTmp)){
            foreach($workTmp as $key => $value){
                $workConstant[$value->id] = $value->workName;
            }
        }
        if(!empty($optionTmp)){
            foreach($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if(!empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $branch_chihoi[$value->id] = $value->name;
            }
        }
        if(!empty($clbthuochoTmp)){
            foreach($clbthuochoTmp as $key => $value){
                $branch_clbthuochoi[$value->id] = $value->name;
            }
        }
        $isProfile = false;
        try {
            $isPdf = true;
            $pdfShortMemberInfo = DomPDF::loadView('admin.member.pdf.don_dang_ky_gia_nhap', compact('objMember', 'typeOfTravelGuide', 'typeOfPlace', 'provincial'));
            $pdfFullMemberInfo  = DomPDF::loadView('admin.member.pdf.member_info', compact('objMember', 'typeOfTravelGuide', 'typeOfPlace', 'objBrand', 'objDegree', 'objMajor', 'objLanguage', 'objLanguageLevel', 'workHistories', 'typeGuideName', 'groupSizeName', 'guideLanguage', 'profileImg', 'verifiedByName', 'approvedByName', 'state', 'provincial', 'workConstant', 'branch_clbthuochoi', 'branch_chihoi', 'otherConstant', 'languages', 'forteTours', 'memberOfficial', 'isProfile', 'isPdf'));
            $outputPdfShortMemberInfo = $pdfShortMemberInfo->output();
            $outputPdfFullMemberInfo  = $pdfFullMemberInfo->output();
            file_put_contents(storage_path('pdf/' . md5($memberId) .'.pdf'), $outputPdfShortMemberInfo);
            file_put_contents(storage_path('pdf/' . md5($memberId) .'_detail.pdf'), $outputPdfFullMemberInfo);
            LogServiceProvider::createSystemHistory(trans('history.fn_saveDataMemberIntoPdf'), ['file_name' => md5($memberId) . '.pdf' . ' ' . md5($memberId) . '_detail.pdf']);
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('save_pdf_file_fail', 'Error when save data into pdf status (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
    }

    public static function getMembersForCreatingCard($limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE)
    {
        // kế toán chỉ list ra những member có status là Chờ đóng lệ phí + hội phí <=> 4 update 20180119
        if (UserServiceProvider::isAccountant()) {
            $objMember = self::getAllMembersNotByPlaceTypeQuery()
                ->whereIn(self::STATUS, [MemberConstants::FEE_WAITING, MemberConstants::SIGNING_WAITING]);
        } elseif (UserServiceProvider::isMemberShipCardIssuer()) {
            // màn hình tạo card chỉ cho chuyên viên truy cập và list ra 2 trang thái member (5) Chờ ký quyết định hội viên.  (6) Chờ cấp mã hội viên update 20180119
            // update follow chỉ nhìn thấy status 6 và 9, 20180207
            $objMember = self::_getRuleActiveMember()
                ->whereIn(self::STATUS, [MemberConstants::CODE_PROVIDING_WAITING, MemberConstants::CARD_PRINTING_WAITING]);
        } else {
            return null;
        }

        return $objMember->paginate($limit);
    }

    public static function getAllMembersByPlaceType($limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE,$isCount = false)
    {
        if(!$isCount){
            $objMember = self::getAllMembersByPlaceTypeQuery()->paginate($limit);
        }else {
            $objMember = self::getAllMembersByPlaceTypeQuery()->get();
        }


        return $objMember;
    }

    public static function getAllMembersInPayment($limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE) {
        $query = Member::where(MemberServiceProvider::PHONE_VERIFIED, 1)
            ->where(MemberServiceProvider::ACCEPT_TERMS_AND_POLICIES, 1);
        $statusDict = MemberConstants::$file_accountant_const;

        // update logic 14062018
        $branches = new BranchesRepository(new Branches());
        $typeOfPlaces = $branches->getAllByParentId(null);

        // $typeOfPlaces = array_merge(BranchConstants::$hn_typeOfPlace, BranchConstants::$dn_typeOfPlace, BranchConstants::$hcm_typeOfPlace);
        return $query->whereIn(self::STATUS, self::extractStatusValues($statusDict))
            ->whereIn(self::PROVINCE_CODE, $typeOfPlaces)
            ->orderBy(MemberServiceProvider::CREATED_AT, 'des')
            ->paginate($limit);
    }

    private static function _getRuleActiveMember ()
    {
        return MemberPayments::rightJoin('members', 'members.id', '=', 'member_payments.memberId')
            ->where(MemberServiceProvider::PHONE_VERIFIED, 1)
            ->where(MemberServiceProvider::ACCEPT_TERMS_AND_POLICIES, 1);
    }

    private static function getAllMembersByPlaceTypeQuery()
    {
        $query = self::_getRuleActiveMember();

        $statusDict = [];
        // update logic code 20180614
        $branches = new BranchesRepository(new Branches());
        $typeOfPlaces = UserServiceProvider::getTypeOfPlace();
        if (UserServiceProvider::isExpertRole()) {
            $statusDict = MemberConstants::$file_expert_const;

            /*if (UserServiceProvider::isHnExpertUser()) {
                $typeOfPlaces = BranchConstants::$hn_typeOfPlace;
            } else if (UserServiceProvider::isDnExpertUser()) {
                $typeOfPlaces = BranchConstants::$dn_typeOfPlace;
            } else if (UserServiceProvider::isHcmExpertUser()) {
                $typeOfPlaces = BranchConstants::$hcm_typeOfPlace;
            }*/
        } elseif (UserServiceProvider::isLeaderRole()) {
            $statusDict = MemberConstants::$file_leader_const;

            /*if (UserServiceProvider::isHnLeaderUser()) {
                $typeOfPlaces = BranchConstants::$hn_typeOfPlace;
            } else if (UserServiceProvider::isDnLeaderUser()) {
                $typeOfPlaces = BranchConstants::$dn_typeOfPlace;
            } else if (UserServiceProvider::isHcmLeaderUser()) {
                $typeOfPlaces = BranchConstants::$hcm_typeOfPlace;
            }*/
        } elseif (UserServiceProvider::isAccountant()) {
            //$statusDict = MemberConstants::$file_accountant_const;

            // update logic code 20180614
            $typeOfPlaces = $branches->getAllByParentId(null);
            // $typeOfPlaces = array_merge(BranchConstants::$hn_typeOfPlace, BranchConstants::$dn_typeOfPlace, BranchConstants::$hcm_typeOfPlace);
        }elseif (UserServiceProvider::isMemberShipCardIssuer()) {
            $statusDict = MemberConstants::$file_membership_card_issuer_const;

            // update logic code 20180614
            $typeOfPlaces = $branches->getAllByParentId(null);
            // $typeOfPlaces = array_merge(BranchConstants::$hn_typeOfPlace, BranchConstants::$dn_typeOfPlace, BranchConstants::$hcm_typeOfPlace);
        }

        $query->whereIn(self::STATUS, self::extractStatusValues($statusDict))
            ->whereIn(self::PROVINCE_CODE, $typeOfPlaces)
            ->orderBy('members.' . MemberServiceProvider::CREATED_AT, 'des');
    }

    public static function getAllMembersNotByPlaceTypeQuery()
    {
        $query = Member::where(MemberServiceProvider::PHONE_VERIFIED, 1)
            ->where(MemberServiceProvider::ACCEPT_TERMS_AND_POLICIES, 1);

        if (UserServiceProvider::isAccountant()) {
            $statusDict = MemberConstants::$file_accountant_const;
        }
        else {
            $statusDict = [];
        }
        return $query->whereIn(self::STATUS, self::extractStatusValues($statusDict))
            ->orderBy(MemberServiceProvider::CREATED_AT, 'des');
    }

    public static function getSearchAllMemberForCreateCard()
    {
        $query = self::_getRuleActiveMember();
        $statusDict = MemberConstants::$file_membership_card_issuer_create_card_const;

        return $query->whereIn(self::STATUS, self::extractStatusValues($statusDict))
            ->orderBy(MemberServiceProvider::CREATED_AT, 'des');
    }

    public static function getSearchAllMemberForPrintCard()
    {
        $query = self::_getRuleActiveMember();
        $statusDict = MemberConstants::$file_membership_card_issuer_print_card_const;

        return $query->whereIn(self::STATUS, self::extractStatusValues($statusDict))
            ->orderBy(MemberServiceProvider::CREATED_AT, 'des');
    }


    private static function extractStatusValues($statusDict)
    {
        return collect($statusDict)->keys()->all();
    }

    /**
     * get member list that has created card
     *
     * @param $limit
     * @return mixed
     */
    public static function getMemberHasCard ($limit)
    {
        $objMember = self::_getRuleActiveMember()
            ->where('member_code', '<>', null)
            ->where(MemberServiceProvider::MEMBER_TYPE, '<>', null)
            ->whereIn('status', array(MemberConstants::CARD_PRINTING_WAITING, MemberConstants::MEMBER_OFFICIAL_MEMBER)) // chỉ list ra những user có status = 9 Chờ in thẻ (CV). update 20180119
            //->where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER) // chỉ list ra những user có status = 13 Chờ in thẻ (CV). update 20180119
            ->paginate($limit);

        return $objMember;
    }

    /**
     * @todo  get member by role of admin
     */
    public static function getMembersByRoleUserAdmin($limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE, $role = 2)
    {

        if($role == 2)
        {
            $objMember = self::getAllMembersByPlaceTypeQuery()
                    ->whereIn(MemberServiceProvider::STATUS, [MemberConstants::FEE_WAITING, MemberConstants::SIGNING_WAITING])
                    ->paginate($limit);

                return $objMember;
        }
    }

    public static function updateMultiMember ($arrMemberId, $arrUpdateValue)
    {
        DB::beginTransaction();
        try {
            $objMembers = Member::whereIn('id', $arrMemberId)->update($arrUpdateValue);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            LogsHelper::trackByFile('update_multi_members_fail', 'Error when update multi members (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data arrMemberIds: ' . print_r($arrMemberId, true) . ')');
            $objMembers = null;
        }

        return $objMembers;
    }

    public static function updateMemberWith($arrMemberInfo, $memberId)
    {

        $memberInfo = $arrMemberInfo;
        $memberInfo['profileImg'] = $arrMemberInfo['avatar'];
        $memberInfo['guideLanguage'] = rtrim(implode(",", $arrMemberInfo['guideLanguage']),",");
        $memberInfo['forteTour'] = rtrim(implode(",", $arrMemberInfo['forteTour']),",");

        unset($memberInfo[MemberServiceProvider::EMAIL_VERIFIED]);
        unset($memberInfo[MemberServiceProvider::PHONE_VERIFIED]);
        unset($memberInfo['emailToken']);
        unset($memberInfo['phoneToken']);
        unset($memberInfo['educations']);
        unset($memberInfo['majorSkills']);
        unset($memberInfo['languageSkills']);
        unset($memberInfo['workHistory']);
        unset($memberInfo['elements']);
        unset($memberInfo['achievementFiles']);

        $member = Member::find($memberId);

        // Official member have member code
        // if ($member->member_code)
        //     $memberInfo['status'] = MemberConstants::UPDATE_INFO_2;
        // else $memberInfo['status'] = MemberConstants::VERIFICATION_WAITING;

        DB::beginTransaction();
        try {
            $member->update($memberInfo);

            self::updateEducations($arrMemberInfo['educations'], $memberId);
            self::updateMajorSkills($arrMemberInfo['majorSkills'], $memberId);
            self::updateLanguageSkills($arrMemberInfo['languageSkills'], $memberId);
            self::updateWorkHistory($arrMemberInfo['workHistory'], $memberId);
            self::updateElements($arrMemberInfo['elements'], $memberId);
            self::updateAchievementFiles($arrMemberInfo['achievementFiles'], $memberId);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            LogsHelper::trackByFile('create_member_fail', 'Error when create new member (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r($arrMemberInfo, true) . ')');
            throw $exception;
        }

        return Member::find($memberId);
    }
    //binhnd update member tmp
    public static function updateMemberTmpWith($arrMemberInfo, $memberId)
    {
        $memberInfo = $memberTmp = $arrMemberInfo;
        $memberInfo['guideLanguage'] = rtrim(implode(",", $arrMemberInfo['guideLanguage']),",");
        $memberInfo['forteTour'] = rtrim(implode(",", $arrMemberInfo['forteTour']),",");

        unset($memberInfo[MemberServiceProvider::EMAIL_VERIFIED]);
        unset($memberInfo[MemberServiceProvider::PHONE_VERIFIED]);
        unset($memberInfo['emailToken']);
        unset($memberInfo['phoneToken']);
        unset($memberInfo['educations']);
        unset($memberInfo['majorSkills']);
        unset($memberInfo['languageSkills']);
        unset($memberInfo['workHistory']);
        unset($memberInfo['elements']);
        unset($memberInfo['achievementFiles']);

        DB::beginTransaction();
        $member = Member::find($memberId);
        //$memberUpdate['status'] = MemberConstants::UPDATE_INFO_2;
        try {
            //update member status 10
            $member->update($memberUpdate);
            $memberTmp['member_id'] = $memberId;
            //tạo member_tmp
            self::createMemberTmpWithUpdate($memberTmp);
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            LogsHelper::trackByFile('create_member_tmp_fail', 'Error when create new member (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r($arrMemberInfo, true) . ')');
            throw $exception;
        }

        return Member::find($memberId);
    }

    public static function updateMemberWithoutStatus($arrMemberInfo, $memberId)
    {
        $memberInfo = $arrMemberInfo;
        $memberInfo['profileImg'] = $arrMemberInfo['avatar'];
        $memberInfo['guideLanguage'] = rtrim(implode(",", $arrMemberInfo['guideLanguage']),",");
        $memberInfo['forteTour'] = rtrim(implode(",", $arrMemberInfo['forteTour']),",");
        unset($memberInfo[MemberServiceProvider::EMAIL_VERIFIED]);
        unset($memberInfo[MemberServiceProvider::PHONE_VERIFIED]);
        unset($memberInfo['emailToken']);
        unset($memberInfo['phoneToken']);
        unset($memberInfo['educations']);
        unset($memberInfo['majorSkills']);
        unset($memberInfo['languageSkills']);
        unset($memberInfo['workHistory']);
        unset($memberInfo['elements']);
        unset($memberInfo['achievementFiles']);
        unset($memberInfo['status']);

        $member = Member::find($memberId);

        // $memberInfo['status'] = 13;

        DB::beginTransaction();
        try {
            $member->update($memberInfo);

            self::updateEducations($arrMemberInfo['educations'], $memberId);
            self::updateMajorSkills($arrMemberInfo['majorSkills'], $memberId);
            self::updateLanguageSkills($arrMemberInfo['languageSkills'], $memberId);
            self::updateWorkHistory($arrMemberInfo['workHistory'], $memberId);
            self::updateElements($arrMemberInfo['elements'], $memberId);
            self::updateAchievementFiles($arrMemberInfo['achievementFiles'], $memberId);

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();

            LogsHelper::trackByFile('create_member_fail', 'Error when create new member (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r($arrMemberInfo, true) . ')');
            throw $exception;
        }

        return Member::find($memberId);
    }

    private static function createMemberTmpWithUpdate($arrMemberInfo)
    {
        try {
            $objMember = MemberTmp::Create([
                'member_id'     => $arrMemberInfo['member_id'],
                'content'       => json_encode($arrMemberInfo),
            ]);
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('create_memberTmp_fail', 'Error when create new member Tmp (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r($arrMemberInfo, true) . ')');
            $objMember = null;
        }
    }
    //
    private static function updateAvatarMemberTmp($file)
    {
        if (empty($file)) {
            return;
        }

        $storeFile = new StorePhoto($file->getRealPath());
        $storeFile->moveToCollection();
        $avatarFile = File::create([
            MemberServiceProvider::ORIGINAL_NAME => $file->getClientOriginalName(),
            MemberServiceProvider::ABSOLUTE_PATH => $storeFile->getRealPath(),
            MemberServiceProvider::RELATIVE_URL => $storeFile->getRelativeUrl(),
            'mime' => $storeFile->getFileInfo()->getMimeType(),
            'size' => $storeFile->getFileInfo()->getSize(),
            MemberServiceProvider::STATUS => 1,
        ]);
        return $avatarFile->id;
    }
    /**
     * @param UploadedFile $file
     * @param $memberId
     */
    private static function updateAvatar($file, $memberId)
    {
        if (empty($file)) {
            return;
        }

        $storeFile = new StorePhoto($file->getRealPath());
        $storeFile->moveToCollection();
        $avatarFile = File::updateOrCreate([
            MemberServiceProvider::ORIGINAL_NAME => $file->getClientOriginalName(),
            MemberServiceProvider::ABSOLUTE_PATH => $storeFile->getRealPath(),
            MemberServiceProvider::RELATIVE_URL => $storeFile->getRelativeUrl(),
            'mime' => $storeFile->getFileInfo()->getMimeType(),
            'size' => $storeFile->getFileInfo()->getSize(),
            MemberServiceProvider::STATUS => 1,
        ]);

        MemberAvatar::where(self::MEMBER_ID, $memberId)->update([
            MemberServiceProvider::FILE_ID => $avatarFile->id,
            'current' => 1,
        ]);
    }
    private static function updateMajorSkills($majorSkills, $memberId)
    {
        if (!empty($majorSkills['ids'])) {
            MajorSkill::where(self::MEMBER_ID, $memberId)->update([
                'majorId' => $majorSkills['ids'],
            ]);
        }
    }

    private static function updateAchievementFiles($achievementFiles, $memberId)
    {
        if (!empty($achievementFiles)) {
            foreach ($achievementFiles as $achievementFile) {
                $storeFile = new StoreFile($achievementFile->getRealPath());
                $storeFile->moveToCollection();
                $achievementFileModel = File::updateOrCreate([
                    MemberServiceProvider::ORIGINAL_NAME => $achievementFile->getClientOriginalName(),
                    MemberServiceProvider::ABSOLUTE_PATH => $storeFile->getRealPath(),
                    MemberServiceProvider::RELATIVE_URL => $storeFile->getRelativeUrl(),
                    'mime' => $storeFile->getFileInfo()->getMimeType(),
                    'size' => $storeFile->getFileInfo()->getSize(),
                    MemberServiceProvider::STATUS => 1,
                ]);

                if (!empty($memberId) && !empty($achievementFileModel->id)) {
                    AchievementFile::updateOrCreate([
                        MemberServiceProvider::MEMBER_ID => $memberId,
                        MemberServiceProvider::FILE_ID => $achievementFileModel->id,
                    ]);
                }
            }
        }
    }

    private static function updateEducations($educations, $memberId)
    {
        if (!empty($educations)) {
            foreach ($educations as $education) {
                $educationFileId = null;
                if (!empty($education['file'])) {
                    $file = $education['file'];
                    $storeFile = new StoreFile($file->getRealPath());
                    $storeFile->moveToCollection();
                    $educationFile = File::updateOrCreate([
                        MemberServiceProvider::ORIGINAL_NAME => $file->getClientOriginalName(),
                        MemberServiceProvider::ABSOLUTE_PATH => $storeFile->getRealPath(),
                        MemberServiceProvider::RELATIVE_URL => $storeFile->getRelativeUrl(),
                        'mime' => $storeFile->getFileInfo()->getMimeType(),
                        'size' => $storeFile->getFileInfo()->getSize(),
                        MemberServiceProvider::STATUS => 1,
                    ]);
                    $educationFileId = $educationFile->id;
                }

                if (!empty($education['branchId']) && !empty($education['degreeId'])) {
                    Education::where(self::MEMBER_ID, $memberId)->update([
                        'branchId' => $education['branchId'],
                        'degreeId' => $education['degreeId'],
                        MemberServiceProvider::FILE_ID => $educationFileId,
                    ]);
                }
            }
        }
    }

    private static function updateLanguageSkills($languageSkills, $memberId)
    {
        if (!empty($languageSkills)) {
            foreach ($languageSkills as $languageSkill) {
                $langugeSkillFileId = null;
                if (!empty($languageSkill['file'])) {
                    $file = $languageSkill['file'];
                    $storeFile = new StoreFile($file->getRealPath());
                    $storeFile->moveToCollection();
                    $languageSkillFile = File::updateOrCreate([
                        MemberServiceProvider::ORIGINAL_NAME => $file->getClientOriginalName(),
                        MemberServiceProvider::ABSOLUTE_PATH => $storeFile->getRealPath(),
                        MemberServiceProvider::RELATIVE_URL => $storeFile->getRelativeUrl(),
                        'mime' => $storeFile->getFileInfo()->getMimeType(),
                        'size' => $storeFile->getFileInfo()->getSize(),
                        MemberServiceProvider::STATUS => 1,
                    ]);
                    $langugeSkillFileId = $languageSkillFile->id;
                }

                if (!empty($languageSkill['languageId']) && !empty($languageSkill['levelId'])) {
                    LanguageSkill::where(self::MEMBER_ID, $memberId)->update([
                        'languageId' => $languageSkill['languageId'],
                        'levelId' => $languageSkill['levelId'],
                        MemberServiceProvider::FILE_ID => $langugeSkillFileId,
                    ]);
                }
            }
        }
    }

    private static function updateWorkHistory($workHistory, $memberId)
    {
        WorkHistory::where(self::MEMBER_ID, $memberId)->delete();
        if (!empty($workHistory)) {
            foreach ($workHistory as $item) {
                $work = Work::updateOrCreate([
                    'elementName' => $item['elementName'],
                    'typeOfContract' => $item['typeOfContract'],
                    'fromDate' => $item['fromDate'],
                    'toDate' => $item['toDate'],
                ]);

                if (!empty($work->id) && !empty($memberId)) {
                    WorkHistory::create([
                        'elementId' => $work->id,
                        MemberServiceProvider::MEMBER_ID => $memberId,
                    ]);
                }
            }
        }
    }

    private static function updateElements($elements, $memberId)
    {
        if (!empty($elements)) {
            foreach ($elements as $element) {
                if (!empty($element['typeGuideId']) && !empty($element['groupSizeId'])) {
                    Element::where(self::MEMBER_ID, $memberId)->update([
                        'typeGuideId' => $element['typeGuideId'],
                        'groupSizeId' => $element['groupSizeId'],
                    ]);
                }
            }
        }
    }

    public static function getMemberByFileCode ($fileCode)
    {
        if (empty($fileCode)) {
            return null;
        } else {
            return self::_getRuleActiveMember()
                ->where('file_code', $fileCode)
                ->first();
        }
    }

    public static function getMemberExistByFileCode($fileCode) {
        if (empty($fileCode)) {
            return null;
        } else {
            return Member::where('file_code', $fileCode)
                ->whereNull('is_delete')
                ->orWhere('is_delete', 0)
                ->first();
        }
    }

    public static function getMemberExistByMemberCode($memberCode) {
        if (empty($memberCode)) {
            return null;
        } else {
            return Member::where('member_code', $memberCode)
                ->whereNull('is_delete')
                ->orWhere('is_delete', 0)
                ->first();
        }
    }

    public static function updateStatusByFileCode ($fileCode, $status)
    {
        if (empty($fileCode) || empty($status)) {
            return null;
        } else {
            $objMember = Member::where(MemberServiceProvider::PHONE_VERIFIED, 1)
                ->where(MemberServiceProvider::ACCEPT_TERMS_AND_POLICIES, 1)
                ->where('file_code', $fileCode)->first();

            if (empty($objMember)) {
                return null;
            }

            $objMember->status = $status;

            DB::beginTransaction();
            try {
                $objMember->save();
                DB::commit();
            } catch (\Exception $exception) {
                DB::rollBack();

                LogsHelper::trackByFile('update_status_by_file_code_fail', 'Error when update status by file code (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$status, $fileCode], true) . ')');
                $objMember = null;
            }

            return $objMember;
        }
    }

    public static function isStatus($id, $status)
    {
        if (empty($id)) return false;
        if (empty($status)) return false;

        $member = Member::where('id', $id)
            ->where('status', $status)
            ->first();

        return $member || false;
    }

    public static function getEducationByMemberId($memberId)
    {
        $objEducation = \App\Models\Education::where('memberId', $memberId)->first();
        if (!empty($objEducation)) {
            $objBrand = self::getBranchById($objEducation->branchId);
            $objDegree = self::getDegreeById($objEducation->degreeId);
            //$objFile   = \App\Models\File::where('id', $objEducation->fileId)->first();
        } else {
            return array(self::BRAND => '', self::DEGREE => '');
        }

        return array(self::BRAND => $objBrand, self::DEGREE => $objDegree);
    }

    public static function getLanguageAchievedByMemberId($memberId)
    {
        $objLanguageSkills = \App\Models\LanguageSkill::where('memberId', $memberId)->first();
        if (!empty($objLanguageSkills)) {
            $objLanguage = \App\Models\Language::where('id', $objLanguageSkills->languageId)->first();
            $objLanguageLevel = \App\Models\LanguageLevel::where('id', $objLanguageSkills->levelId)->first();
        } else {
            return array(self::LANGUAGE => '', self::LANGUAGE_LEVEL => '');
        }

        return array(self::LANGUAGE => $objLanguage, self::LANGUAGE_LEVEL => $objLanguageLevel);
    }

    public static function getElementByMemeber($objMember)
    {
        $element = $objMember->getElementsAttribute();
        if (!empty($element)) {
            $typeGuideId = $element->typeGuideId;
            $groupSizeId = $element->groupSizeId;

            $objTypeGuide = \App\Models\TypeGuide::where('id', $typeGuideId)->first();
            $objGroupSize = \App\Models\GroupSize::where('id', $groupSizeId)->first();

            if ($objTypeGuide) {
                $typeGuideName = $objTypeGuide->typeName;
            }

            if ($objGroupSize) {
                $groupSizeName = $objGroupSize->groupSize;
            }
        } else {
            $typeGuideName = '';
            $groupSizeName = '';
        }

        return array(self::TYPE_GUIDE_NAME => $typeGuideName, self::GROUP_SIZE_NAME => $groupSizeName);
    }

    public static function getWorkHistoryByMemberId($memberId)
    {
        $query = WorkHistory::leftJoin('works', 'workHistory.elementId', '=', 'works.id')
            ->where('workHistory.memberId', '=', $memberId);
        $workHistory = $query->get();

        return $workHistory;
    }

    public static function getBranchById($brandId)
    {
        $objBrand  = \App\Models\EducationBranch::where('id', $brandId)->first();
        return $objBrand;
    }

    public static function getDegreeById($degreeId)
    {
        $objDegree = \App\Models\EducationDegree::where('id', $degreeId)->first();
        return $objDegree;
    }

    public static function createMemberRank($memberId, $attributes) {
        $getAllMemberRankExist = MemberRank::getRankByMemberIdAndRankId($memberId, $attributes['majors']);
        if (empty($getAllMemberRankExist)) {
            MemberRank::query()->create([
                'rank_id' => $attributes['majors'],
                'member_id' => $memberId,
                'is_deleted' => 0
            ]);
        }
        return;
    }

    public static function getRepresentativeOfficeID() {
        $representativeOfficeID = Branches::select('id', 'area')->whereIn('option_code', ['HEAD', 'OFFICES'])->where('status', 1)->get();
        return $representativeOfficeID;
    }

    public static function getMaxMemberCodeByType($type) {
        $maxTouristCode = Member::select('member_code')->where('typeOfTravelGuide', $type)->whereRaw('SUBSTRING(member_code, 1, 1) =' . $type)->orderBy('member_code', 'DESC')->first();
        if (empty($maxTouristCode)) {
            return $type . '00001';
        }
        return $maxTouristCode->member_code;
    }

    public static function updateMemberCodeFromTouristType($memberId, $data) {
        try {
            $date = $data['expirationDate'];
            $expiration = str_replace('/', '-', $date);
            Member::where('id', $memberId)->update([
                'touristGuideCode' => $data['touristGuideCode'],
                'typeOfTravelGuide' => $data['typeOfTravelGuide'],
                'expirationDate' => date('Y-m-d 00:00:00', strtotime($expiration))
            ]);
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_guide_travel_code_fail', 'Error when update code, Data code: ' . print_r(['memberId' => $memberId, 'touristGuideCode' => $data['touristGuideCode'], 'typeOfTravelGuide' => $data['typeOfTravelGuide'], 'expirationDate' => $data['expirationDate']] ,true));
        }
        return true;
    }

    public static function getMemberExistByTouristGuideCode($touristGuideCode) {
        if (empty($touristGuideCode)) {
            return null;
        } else {
            return Member::where('touristGuideCode', $touristGuideCode)
                ->whereNull('is_delete')
                ->orWhere('is_delete', 0)
                ->first();
        }
    }

    public static function updateMemberUpdatedAt($userId) {
        $objUser = User::select('memberId')->where('id', $userId)->whereNull('deleted_at')->first();
        if (empty($objUser) || is_null($objUser->memberId)) {
            return false;
        }
        DB::beginTransaction();
        try {
            Member::where('id', $objUser->memberId)->whereNull('is_delete')->update(['updatedAt' => date('Y-m-d H:i:s')]);
            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_updatedAt_fail', 'Error when update updatedAt user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . $objUser->memberId);
            return false;
        }
    }

    /**
     * @param array $attributes
     * @param string $action
     * @return bool
     */
    public static function handlePrintCardSelected(array $attributes, string $action): bool
    {
        if (! auth()->check()) {
            return false;
        }

        try {
            $userId = auth()->user()->id;
            foreach ($attributes['attributes'] as $data) {
                $type = self::$mappingPrintCardSelectedType[(int) $data['type']] ?? null;
                if (is_null($type)) {
                    break;
                }
                if ($action === 'add') {
                    MemberPrintCardSelected::query()->updateOrCreate(
                        [
                            'user_id' => $userId,
                            'member_id' => $data['member_id'],
                            'print_card_type' => $type
                        ],
                        []
                    );
                } else {
                    MemberPrintCardSelected::query()->where('user_id', $userId)
                        ->where('member_id', $data['member_id'])
                        ->where('print_card_type', $type)
                        ->delete();
                }
            }
            return true;
        } catch (\Exception $e) {
            LogsHelper::trackByFile('print_card_selected_fail', 'Error when ' .$action. ' record into DB (Exception: ' . $e->getMessage() . ' on Line ' . $e->getLine() . ' in File ' . $e->getFile() . ', Data: ' . print_r($attributes, true) . ')');
            return false;
        }
    }

    public static function fetchMemberIdExport($userId, $type = null): array
    {
        return MemberPrintCardSelected::query()
            ->where('user_id', $userId)
            ->when(! is_null($type), function ($query) use ($type) {
                $query->where('print_card_type', $type);
            })
            ->pluck('member_id')
            ->all();
    }

    /**
     * @param int $userId
     * @return array
     */
    public static function fetchPrintCardSelectedMemberId(int $userId): array
    {
        $arr = [];
        $fetchPrintCardSelectedAll = MemberPrintCardSelected::query()
            ->select('print_card_type', 'member_id')
            ->where('user_id', $userId)
            ->get();
        $arrMapping = array_flip(self::$mappingPrintCardSelectedType);
        foreach ($fetchPrintCardSelectedAll as $obj) {
            $arr[] = [
                'member_id' => $obj->member_id,
                'print_card_type' => $arrMapping[$obj->print_card_type]
            ];
        }

        return $arr;
    }

    /**
     * @param int $userId
     * @param int|null $type
     * @param $limit
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function fetchPrintCardSelected(int $userId, ?int $type, $limit)
    {
        $query = MemberPrintCardSelected::query()
            ->select('members.*', 'member_print_card_selected.id as mpcsid', 'member_print_card_selected.print_card_type')
            ->join('members', 'members.id', '=', 'member_print_card_selected.member_id')
            ->where('member_print_card_selected.user_id', $userId);

        $query->when(! is_null($type), function ($query) use ($type) {
            $query->where('member_print_card_selected.print_card_type', $type);
        });

        return $query->paginate($limit);
    }

    /**
     * @param int $id
     * @return bool
     */
    public static function deletePrintCardSelectedById(int $id): bool
    {
        try {
            MemberPrintCardSelected::query()->where('id', $id)->delete();
            return true;
        } catch (\Exception $e) {
            LogsHelper::trackByFile('delete_print_card_selected_fail', 'Error when delete record into DB (Exception: ' . $e->getMessage() . ' on Line ' . $e->getLine() . ' in File ' . $e->getFile() . ', Data: ' . print_r([$id], true) . ')');
            return false;
        }
    }

    public static function checkSelected($memberId, $type, $attributes)
    {
        $flg = false;
        foreach ($attributes as $att) {
            if ($memberId === $att['member_id'] && $type === $att['print_card_type']) {
                $flg = true;
                break;
            }
        }
        return $flg;
    }

    /**
     * @param $userId
     * @param $type
     */
    public static function deletePrintCardSelectedByUserIdAndType($userId, $type)
    {
        try {
            MemberPrintCardSelected::query()
                ->where('user_id', $userId)
                ->when(! is_null($type), function ($query) use ($type) {
                    $query->where('print_card_type', $type);
                })
                ->delete();
        } catch (\Exception $e) {
            LogsHelper::trackByFile('delete_print_card_selected_fail', 'Error when delete record into DB (Exception: ' . $e->getMessage() . ' on Line ' . $e->getLine() . ' in File ' . $e->getFile() . ', Data: ' . print_r([$userId, $type], true) . ')');
        }
    }
}
