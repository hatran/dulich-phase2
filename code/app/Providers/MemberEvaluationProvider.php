<?php

namespace App\Providers;

use App\Models\MemberEvaluation;
use App\Models\Options;
use Validator as V;
use App\Constants\RankConstants;

class MemberEvaluationProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    
    public static $rules = [
        'taxcode_company' => 'required',
        'member_id' => 'required|integer',
        'name' => 'required',
        'position' => 'required',
        'phone' => 'required|numeric',
        'tourist_code' => 'required',
        'forte_tour' => 'required',
        'number_of_tourist' => 'required|numeric',
        'from_date' => 'required',
        'to_date' => 'required',
        'criteria1' => 'required|integer|numeric',
        'criteria2' => 'required|integer|numeric',
        'criteria3' => 'required|integer|numeric',
        'criteria4' => 'required|integer|numeric',
        'criteria5' => 'required|integer|numeric',
        'criteria1_code' => 'required',
        'criteria2_code' => 'required',
        'criteria3_code' => 'required',
        'criteria4_code' => 'required',
        'criteria5_code' => 'required',
    ];

    public static $messages = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'integer' => ':attribute phải là số',
        'numeric' => ':attribute chỉ gồm các số từ 0 đến 9'
    ];
              
    public static $niceAttributeName = [
        'taxcode_company' => 'Mã số thuế',
        'member_id' => 'Hội viên',
        'name' => 'Tên người đánh giá',
        'position' => 'Chức danh người đánh giá',
        'phone' => 'Số điện thoại',
        'tourist_code' => 'Mã đoàn',
        'forte_tour' => 'Tour tuyến',
        'number_of_tourist' => 'Số lượng khách',
        'from_date' => 'Từ ngày',
        'to_date' => 'Đến ngày',
        'criteria1' => 'Tiêu chí 1',
        'criteria2' => 'Tiêu chí 2',
        'criteria3' => 'Tiêu chí 3',
        'criteria4' => 'Tiêu chí 4',
        'criteria5' => 'Tiêu chí 5',
        'criteria1_code' => 'Mã tiêu chí 1',
        'criteria2_code' => 'Mã tiêu chí 2',
        'criteria3_code' => 'Mã tiêu chí 3',
        'criteria4_code' => 'Mã tiêu chí 4',
        'criteria5_code' => 'Mã tiêu chí 5',
    ];
    
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;

    public static function getPosition()
    {
        $position = RankConstants::$position;
        return $position;
    }

    public static function isValid(array $attributes, array $rules = null, array $messages = null, array $niceAttributeName = null) {
        $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        if (!empty($niceAttributeName)) {
            $v->setAttributeNames($niceAttributeName);
        }
        if ($v->fails()) {
            return $v->messages();
        }

        return true;
    }
    
    public static function memberEvaluationSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];

        if (!empty($arrConditions['company_name'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['traveler_company_rank.company_name', 'like', '%' . $arrConditions['company_name'] . '%']]);
        }

        if (!empty($arrConditions['tax_code'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['traveler_company_rank.tax_code', '=', $arrConditions['tax_code']]]);
        }

        if (!empty($arrConditions['member_name'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['members.fullName', 'like', '%' . $arrConditions['member_name'] . '%']]);
        }

        if (!empty($arrConditions['tourist_guide_code'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['members.touristGuideCode', '=', $arrConditions['tourist_guide_code']]]);
        }

        if (!empty($arrConditions['member_code'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['members.member_code', '=', $arrConditions['member_code']]]);
        }

        if (!empty($arrConditions['tourist_code'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.tourist_code', '=', $arrConditions['tourist_code']]]);
        }

        if (!empty($arrConditions['forte_tour'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.forte_tour', '=', $arrConditions['forte_tour']]]);
        }

        if (!empty($arrConditions['from_date']) || !empty($arrConditions['to_date'])) {
            if (empty($arrConditions['from_date'])) {
                $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.to_date', '<=', $arrConditions['to_date'] . ' 23:59:59']]);
            } elseif (empty($arrConditions['to_date'])) {
                $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.from_date', '>=', $arrConditions['from_date'] . ' 00:00:00']]);
            } else {
                $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.from_date', '>=', $arrConditions['from_date'] . ' 00:00:00']]);
                $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.to_date', '<=', $arrConditions['to_date'] . ' 23:59:59']]);
            }
        }    

        if (empty($arrConditions['member_eval'])) {
            $auth_user = auth()->user();
            if (!empty($auth_user)) {
                if ($auth_user->is_traveler_company == 1) {
                    $taxCodeCompany = $auth_user->username;
                    $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.taxcode_company', '=', $taxCodeCompany]]);
                }
                else {
                    if ($auth_user->role == 1) {
                        $mem = $auth_user->memberId;
                        $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.member_id', '=', $mem]]);
                    }
                }
            }
        }
        else {
            $arrSearchCondition = array_merge($arrSearchCondition, [['member_evaluation.member_id', '=', $arrConditions['member_eval']]]);
        }

        $objEval = MemberEvaluation::query()->select('traveler_company_rank.tax_code', 'traveler_company_rank.company_name', 'members.touristGuideCode', 'members.member_code', 'members.fullName', 'member_evaluation.id', 'member_evaluation.tourist_code', 'member_evaluation.forte_tour', 'member_evaluation.created_at', 'member_evaluation.criteria1', 'member_evaluation.criteria2', 'member_evaluation.criteria3', 'member_evaluation.criteria4', 'member_evaluation.criteria5', 'member_evaluation.criteria1_code', 'member_evaluation.criteria2_code', 'member_evaluation.criteria3_code', 'member_evaluation.criteria4_code', 'member_evaluation.criteria5_code', 'member_evaluation.rank_id as rid')
            ->join('members', 'members.id', '=', 'member_evaluation.member_id')
            ->join('traveler_company_rank', 'traveler_company_rank.tax_code', '=', 'member_evaluation.taxcode_company')
            ->where($arrSearchCondition);

        if (!$isCount) {
            return $objEval->addSelect(['member_evaluation.name', 'member_evaluation.phone'])
                    ->orderBy('member_evaluation.created_at', 'des')
                    ->paginate($limit);
        }
        else {
            return $objEval->get();
        }
    }

    public static function findMemberEvaluation($id, $tourist_code) {
        return MemberEvaluation::where('member_evaluation.id', $id)->where('member_evaluation.tourist_code', '=', $tourist_code)->first();
    }
}
