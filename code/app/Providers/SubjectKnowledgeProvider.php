<?php

namespace App\Providers;

use App\Constants\MemberConstants;
use App\Libs\Helpers\LogsHelper;
use Illuminate\Support\ServiceProvider;
use Response;
use Validator;
use Carbon\Carbon;
use App\Libs\Helpers\Utils;
use App\Libs\Storage\StoreFile;
use App\Libs\Storage\StorePhoto;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Constants\BranchConstants;
use App\Constants\CommontConstants;
use App\Models\Knowledge;
use App\Models\Member;
use App\Models\SubjectKnowledge;

class SubjectKnowledgeProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const TITLE = 'title';
    const OPTION_CODE = 'option_code';
    const ID = 'id';
    const VERIFIED_AT = 'verified_at';
    const PROVINCE_CODE = 'province_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_VERIFIED = 'is_verified';
    const IS_DELETE = 'is_delete';
    const MEMBER_TYPE = 'member_type';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';


    public static function getAllKnowledge ()
    {
        $objSubject = Knowledge::getAllKnowledge();
        return $objSubject;
    }


    public static function subjectsSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];
      
        if (!empty($arrConditions['nameall'])) {
            $arrSearchCondition = [['name', 'like', '%' . $arrConditions['nameall'] . '%']];
        }

        if (!empty($arrConditions['id'])) {
            $arrSearchCondition = [['knowledge_id', '=', $arrConditions['id']]];
        }

        if (!empty($arrConditions['nameall']) && !empty($arrConditions['id'])) {
            $arrSearchCondition = array_merge($arrSearchCondition,[['knowledge_id', '=', $arrConditions['id']], ['name', 'like', '%' . $arrConditions['nameall'] . '%']]);
        }

        if (isset($arrConditions['status']) && $arrConditions['status'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', $arrConditions['status']]]);
        }
        
        if(!$isCount){
            $objNew = SubjectKnowledge::where($arrSearchCondition)->whereNull('is_deleted')->orderBy('created_at', 'des')->paginate($limit);
        }else{
            $objNew = SubjectKnowledge::where($arrSearchCondition)->whereNull('is_deleted')->get();
        }
        return $objNew;
    }

    public static function getDataById($id)
    {
        return SubjectKnowledge::where('id',$id)->first();
    }

    public static function getStatusZeroById($id)
    {
        return SubjectKnowledge::where('id', $id)->where('status', 0)->first();
    }

    public static function findSubjectKnowledge($id) {
        return SubjectKnowledge::find($id);
    }

    public static function getSubjectKnowledgeByKnowledge($knowledge_id)
    {
        return SubjectKnowledge::getSubjectKnowledgeNameIDByKnowledgeID($knowledge_id);
    }

    public static function getAllSubjectKnowledgeIDName() {
        return SubjectKnowledge::getAllSubjectKnowledgeIDName();
    }

}
