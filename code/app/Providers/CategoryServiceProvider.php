<?php

namespace App\Providers;

use App\Constants\MemberConstants;
use App\Libs\Helpers\LogsHelper;
use App\Models\File;
use App\Models\MemberDecision;
use App\Models\NewOption;
use Illuminate\Support\ServiceProvider;
use Response;
use Validator;
use Carbon\Carbon;
use App\Libs\Helpers\Utils;
use App\Libs\Storage\StoreFile;
use App\Libs\Storage\StorePhoto;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Constants\BranchConstants;
use App\Constants\CommontConstants;
use App\Models\Option;


use App\Models\EducationBranch;
use App\Models\EducationDegree;
use App\Models\GroupSize;
use App\Models\Language;
use App\Models\LanguageLevel;
use App\Models\Major;
use App\Models\MajorSkill;
use App\Models\TypeGuide;
use App\Models\Member;
use App\Models\LeaderSigningDecision;
use App\Models\OtherConstant;
use App\Models\WorkConstant;
use App\Models\ForteTour;
use App\Models\Rank;
use App\Models\Information;
use App\Models\Knowledge;

class CategoryServiceProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const TITLE = 'title';
    const OPTION_CODE = 'option_code';
    const ID = 'id';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_VERIFIED = 'is_verified';
    const IS_DELETE = 'is_delete';
    const MEMBER_TYPE = 'member_type';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';


    public static function getAllOption ($options)
    {
        return Option::query()->where('key', $options)
            ->where('status', 1)
            ->orderBy('value', 'asc')
            ->get();
    }

    public static function newsSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];
//      1 Danh mục chuyên ngành học vấn   EducationBranch
        if($arrConditions['option_code'] == 'CATEGORY_01'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['branchName', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = EducationBranch::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = EducationBranch::where($arrSearchCondition)->get();
            }
        }
//      2 Danh mục trình độ học vấn           EducationDegree
        if($arrConditions['option_code'] == 'CATEGORY_02'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['degree', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = EducationDegree::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = EducationDegree::where($arrSearchCondition)->get();
            }
        }
//      3 Danh mục chuyên ngành ngoại ngữ     Language
        if($arrConditions['option_code'] == 'CATEGORY_03'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['languageName', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = Language::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = Language::where($arrSearchCondition)->get();
            }
        }
//      4 Danh mục trình độ học ngoại ngữ      Language
        if($arrConditions['option_code'] == 'CATEGORY_04'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['levelName', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = LanguageLevel::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = LanguageLevel::where($arrSearchCondition)->get();
            }
        }
//      5 Danh mục hình thức lao động
        if($arrConditions['option_code'] == 'CATEGORY_05'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['workName', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
//            print_r($arrSearchCondition);die;
            if(!$isCount){
                $objNew = WorkConstant::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = WorkConstant::where($arrSearchCondition)->get();
            }
        }
//        6 Danh mục sở trường hướng dẫn        bao gom 7-8
//        7 Danh mục quy mô hướng dẫn           GroupSize
        if($arrConditions['option_code'] == 'CATEGORY_07'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['groupSize', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = GroupSize::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = GroupSize::where($arrSearchCondition)->get();
            }
        }
//        8 Danh mục loại hình hướng dẫn        TypeGuide
        if($arrConditions['option_code'] == 'CATEGORY_08'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['typeName', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = TypeGuide::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = TypeGuide::where($arrSearchCondition)->get();
            }
        }
//      9 Danh mục nghiệp vụ hướng dẫn        Major
        if($arrConditions['option_code'] == 'CATEGORY_09'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['majorName', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = Major::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = Major::where($arrSearchCondition)->get();
            }
        }
//      10 Danh mục ngôn ngữ hướng dẫn  Language Trung3
//      11 Danh mục số năm kinh nghiệm
        if($arrConditions['option_code'] == 'CATEGORY_11'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['otherName', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = OtherConstant::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = OtherConstant::where($arrSearchCondition)->get();
            }
        }
//      06 Danh mục tỉnh thành
        if($arrConditions['option_code'] == 'CATEGORY_06'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['value', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            $arrSearchCondition = array_merge($arrSearchCondition, [['key','=', 'provincial']]);
            if(!$isCount){
                $objNew = Option::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objNew = Option::where($arrSearchCondition)->get();
            }
        }
//      12 Danh mục chức vụ
        if($arrConditions['option_code'] == 'CATEGORY_12'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['value', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            $arrSearchCondition = array_merge($arrSearchCondition, [['key','=', 'position']]);
            if(!$isCount){
                $objNew = Option::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objNew = Option::where($arrSearchCondition)->get();
            }
        }
//      13 Danh mục Câu lạc bộ
        if($arrConditions['option_code'] == 'CATEGORY_13'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['value', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            $arrSearchCondition = array_merge($arrSearchCondition, [['key','=', 'club']]);
            if(!$isCount){
                $objNew = Option::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objNew = Option::where($arrSearchCondition)->get();
            }
        }
//        14 Danh mục Người ký quyết định        leader_signing_decision
        if($arrConditions['option_code'] == 'CATEGORY_14'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['fullname', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = LeaderSigningDecision::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = LeaderSigningDecision::where($arrSearchCondition)->get();
            }
        }

        if($arrConditions['option_code'] == 'CATEGORY_15'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['name', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1'], ['type', '=', '1']]);
            if(!$isCount){
                $objNew = ForteTour::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objNew = ForteTour::where($arrSearchCondition)->get();
            }
        }

        if($arrConditions['option_code'] == 'CATEGORY_16'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['name', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1'], ['type', '=', '2'], ['parent_id', '=', NULL]]);
            if(!$isCount){
                $objNew = ForteTour::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objNew = ForteTour::where($arrSearchCondition)->get();
            }
        }

        if($arrConditions['option_code'] == 'CATEGORY_17'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['name', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = Rank::where($arrSearchCondition)->orderBy('createdAt', 'des')->paginate($limit);
            }else{
                $objNew = Rank::where($arrSearchCondition)->get();
            }
        }

        if($arrConditions['option_code'] == 'CATEGORY_18'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['name', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1'], ['parent_id', '=', NULL]]);
            if(!$isCount){
                $objNew = Information::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objNew = Information::where($arrSearchCondition)->get();
            }
        }

        if($arrConditions['option_code'] == 'CATEGORY_19'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['name', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            if(!$isCount){
                $objNew = Knowledge::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objNew = Knowledge::where($arrSearchCondition)->get();
            }
        }

        if($arrConditions['option_code'] == 'CATEGORY_20'){
            if (!empty($arrConditions['nameall'])) {
                $arrSearchCondition = [['value', 'like', '%' . $arrConditions['nameall'] . '%']];
            }
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
            $arrSearchCondition = array_merge($arrSearchCondition, [['key','=', 'operatingArea']]);
            if(!$isCount){
                $objNew = Option::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objNew = Option::where($arrSearchCondition)->get();
            }
        }
//print_r(json_decode(json_encode($objNew)));die;
        return $objNew;
    }

    public static function getDataById($id, $option)
    {
        //EducationBranch
        if($option == 'CATEGORY_01'){
            return EducationBranch::where('id',$id)->where('status', 1)->first();
        }
        //EducationDegree
        if($option == 'CATEGORY_02'){
            return EducationDegree::where('id',$id)->where('status', 1)->first();
        }
        //Language
        if($option == 'CATEGORY_03'){
            return Language::where('id',$id)->where('status', 1)->first();
        }
        //LanguageLevel
        if($option == 'CATEGORY_04'){
            return LanguageLevel::where('id',$id)->where('status', 1)->first();
        }
        //WorkConstant
        if($option == 'CATEGORY_05'){
            return WorkConstant::where('id',$id)->where('status', 1)->first();
        }
        //GroupSize
        if($option == 'CATEGORY_07'){
            return GroupSize::where('id',$id)->where('status', 1)->first();
        }
        //TypeGuide
        if($option == 'CATEGORY_08'){
            return TypeGuide::where('id',$id)->where('status', 1)->first();
        }
        //Major
        if($option == 'CATEGORY_09'){
            return Major::where('id',$id)->where('status', 1)->first();
        }
        //OtherConstant
        if($option == 'CATEGORY_11'){
            return OtherConstant::where('id',$id)->where('status', 1)->first();
        }
        //Option provincial
        if($option == 'CATEGORY_06'){
            return Option::where('id',$id)->where('status', 1)->first();
        }
        //Option position
        if($option == 'CATEGORY_12'){
            return Option::where('id',$id)->where('status', 1)->first();
        }
        //Option club
        if($option == 'CATEGORY_13'){
            return Option::where('id',$id)->where('status', 1)->first();
        }
        //LeaderSigningDecision
        if($option == 'CATEGORY_14'){
            return LeaderSigningDecision::where('id',$id)->where('status', 1)->first();
        }
        if($option == 'CATEGORY_15'){
            return ForteTour::where('id',$id)->where('status', 1)->first();
        }
        if($option == 'CATEGORY_16'){
            return ForteTour::where('id',$id)->where('status', 1)->first();
        }
        if($option == 'CATEGORY_17'){
            return Rank::where('id',$id)->where('status', 1)->first();
        }
        if($option == 'CATEGORY_18'){
            return Information::where('id',$id)->where('status', 1)->first();
        }
        if($option == 'CATEGORY_19'){
            return Knowledge::where('id',$id)->where('status', 1)->first();
        }
        if($option == 'CATEGORY_20'){
            return Option::where('id',$id)->where('status', 1)->first();
        }
    }

    public static function getDataByCode($id, $option, $code)
    {
        //EducationBranch
        if($option == 'CATEGORY_01'){
            if($id != 'new'){
                return EducationBranch::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return EducationBranch::where('code',$code)->where('status', 1)->first();
            }
        }
        //EducationDegree
        if($option == 'CATEGORY_02'){
            if($id != 'new') {
                return EducationDegree::where('code', $code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return EducationDegree::where('code', $code)->where('status', 1)->first();
            }
        }
        //Language
        if($option == 'CATEGORY_03'){
            if($id != 'new') {
                return Language::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return Language::where('code', $code)->where('status', 1)->first();
            }
        }
        //LanguageLevel
        if($option == 'CATEGORY_04'){
            if($id != 'new') {
                return LanguageLevel::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return LanguageLevel::where('code', $code)->where('status', 1)->first();
            }
        }
        //WorkConstant
        if($option == 'CATEGORY_05'){
            if($id != 'new') {
                return WorkConstant::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return WorkConstant::where('code', $code)->where('status', 1)->first();
            }
        }
        //GroupSize
        if($option == 'CATEGORY_07'){
            if($id != 'new') {
                return GroupSize::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return GroupSize::where('code', $code)->where('status', 1)->first();
            }
        }
        //TypeGuide
        if($option == 'CATEGORY_08'){
            if($id != 'new') {
                return TypeGuide::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return TypeGuide::where('code', $code)->where('status', 1)->first();
            }
        }
        //Major
        if($option == 'CATEGORY_09'){
            if($id != 'new') {
                return Major::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return Major::where('code', $code)->where('status', 1)->first();
            }
        }
        //OtherConstant
        if($option == 'CATEGORY_11'){
            if($id != 'new') {
                return OtherConstant::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return OtherConstant::where('code', $code)->where('status', 1)->first();
            }
        }
        //Option provincial
        if($option == 'CATEGORY_06'){
            if($id != 'new') {
                return Option::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return Option::where('code', $code)->where('status', 1)->first();
            }
        }
        //Option position
        if($option == 'CATEGORY_12'){
            if($id != 'new') {
                return Option::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return Option::where('code', $code)->where('status', 1)->first();
            }
        }
        //Option club
        if($option == 'CATEGORY_13'){
            if($id != 'new') {
                return Option::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return Option::where('code', $code)->where('status', 1)->first();
            }
        }
        //LeaderSigningDecision
        if($option == 'CATEGORY_14'){
            if($id != 'new') {
                return LeaderSigningDecision::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return LeaderSigningDecision::where('code', $code)->where('status', 1)->first();
            }
        }

        if($option == 'CATEGORY_15'){
            if($id != 'new') {
                return ForteTour::where('code',$code)->where('status', 1)->where('type', 1)->where('id', '<>', $id)->first();
            }else{
                return ForteTour::where('code', $code)->where('status', 1)->where('type', 1)->first();
            }
        }

        if($option == 'CATEGORY_16'){
            if($id != 'new') {
                return ForteTour::where('code',$code)->where('status', 1)->where('type', 2)->where('id', '<>', $id)->first();
            }else{
                return ForteTour::where('code', $code)->where('status', 1)->where('type', 2)->first();
            }
        }

        if($option == 'CATEGORY_17'){
            if($id != 'new') {
                return Rank::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return Rank::where('code', $code)->where('status', 1)->first();
            }
        }

        if($option == 'CATEGORY_18'){
            if($id != 'new') {
                return ForteTour::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return ForteTour::where('code', $code)->where('status', 1)->first();
            }
        }

        if($option == 'CATEGORY_19'){
            if($id != 'new') {
                return Knowledge::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return Knowledge::where('code', $code)->where('status', 1)->first();
            }
        }

        if($option == 'CATEGORY_20'){
            if($id != 'new') {
                return Option::where('code',$code)->where('status', 1)->where('id', '<>', $id)->first();
            }else{
                return Option::where('code', $code)->where('status', 1)->first();
            }
        }
    }
    public static function getNewsByOption ($option_code)
    {
        $objNews = NewOption::where(
            [
                'option_code'       => $option_code,
                'status'            => 1,
            ]
        )
//            ->whereDate('end_time', '>=', date('Y-m-d H:i:s'))
            ->orderBy('created_at', 'des')
            ->get()
            ->toArray();
        $data = [];
        if(count($objNews) > 0){
            foreach ($objNews as $value) {

                if($value['start_time'] == '' && $value['end_time'] == ''){
                    $data = $value;
                    break;
                }

                if($value['start_time'] != '' && $value['end_time'] == ''){
                    if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s'))){
                        $data = $value;
                        break;
                    }
                }

                if($value['start_time'] == '' && $value['end_time'] != ''){
                    if(strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                        $data = $value;
                        break;
                    }
                }

                if($value['start_time'] != '' && $value['end_time'] != ''){
                    if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s')) && strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                        $data = $value;
                        break;
                    }
                }
            }
        }
        return $data;
    }



    public static function delete ($id)
    {
        $objNew = self::getNewById($id);
        if (empty($objNew)) {
            return false;
        }

        try {
            $objNew->delete();
//            DB::commit();
            return true;
        } catch (\Exception $exception) {
//            DB::rollBack();
            LogsHelper::trackByFile('delete_new_fail', 'Error when delete user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . $id);
            return false;
        }
    }



    public static function update ($arrUpdateData)
    {
        $id = $arrUpdateData['id'];
        if (empty($id)) {
            return false;
        }

        $objIntro = self::getNewById($id);

        if (!empty($arrUpdateData['option_code'])) {
            $objIntro->option_code = $arrUpdateData['option_code'];
        }

        if (!empty($arrUpdateData['title'])) {
            $objIntro->title = $arrUpdateData['title'];
        }

        if (!empty($arrUpdateData['start_time'])) {
            $objIntro->start_time = $arrUpdateData['start_time'];
        }

        if (isset($arrUpdateData['end_time']) && $arrUpdateData['end_time'] != '') {
            $objIntro->end_time = $arrUpdateData['end_time'];
        }

        if (isset($arrUpdateData['status'])) {
            $objIntro->status = $arrUpdateData['status'];
        }

        if (!empty($arrUpdateData['content'])) {
            $objIntro->content = $arrUpdateData['content'];
        }

        if (!empty($arrUpdateData['password'])) {
            $objIntro->password = $arrUpdateData['password'];
        }

        try {
            $objIntro->save();
            DB::commit();
            return $objIntro;
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_intro_fail', 'Error when update user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$arrUpdateData], true) . ')');
            return null;
        }
    }


}
