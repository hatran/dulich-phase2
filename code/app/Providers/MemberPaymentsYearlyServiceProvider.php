<?php

namespace App\Providers;

use App\Constants\CommontConstants;
use App\Models\Member;
use App\Models\Offices;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Libs\Helpers\LogsHelper;
use App\Models\MemberPaymentsYearly;
use App\Models\MemberPayments;
use Carbon\Carbon;
use Mail;
use App\Constants\MemberConstants;
use Auth;
use App\Providers\MemberPaymentsServiceProvider;
use App\Models\WaitingPrint;
use App\Models\Printed;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;

class MemberPaymentsYearlyServiceProvider extends ServiceProvider
{

    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const MEMBER_ID = 'memberId';
    const CREATED_AT = 'createdAt';
    const CURRENCY = 'currency';
    const CURRENCY_TYPE = 'currency_type';
    const PAYMENT_MENTHOD = 'payment_method';
    const PAYMENT_CONTENT = 'payment_content';
    const NOTE = 'note';
    const UPDATE_AT = 'updateAt';
    const D_M_Y_FORMAT = 'd/m/Y';

    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const IS_VERIFIED = 'is_verified';
    const IS_FEE = 'is_fee';
    const IS_SIGNED = 'is_signed';
    const MEMBER_TYPE = 'member_type';
    const ORIGINAL_NAME = 'originalName';
    const ABSOLUTE_PATH = 'absPath';
    const RELATIVE_URL = 'relativeUrl';
    const FILE_ID = 'fileId';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';
    const NUMBER_PAYMENT = 'number_payment';
    const DATE_PAYMENT = 'date_payment';
    const PROVINCE_TYPE = 'province_type';
    const FLAG = 'flag';

    /**
     *
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap the application services.
     *
     * @return $objectMemberPayment
     */
    public static function getMemberPaymentByMemberId($id)
    {

        if (empty($id)) {
            $objectMemberPayment = null;
        } else {
            $objectMemberPayment = MemberPaymentsYearly::find($id);
        }

        return $objectMemberPayment;
    }

    public static function getMemberPaymentYearlyByMemberId($memberId)
    {
        if (empty($memberId)) {
            $objectMemberPayment = null;
        } else {
            $objectMemberPayment = MemberPaymentsYearly::where('memberId', $memberId)->where('is_delete', 0)->where('flag', 0)->first();
        }
        return $objectMemberPayment;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getMemberPaymentById($id)
    {
        return MemberPaymentsYearly::find($id);
    }

    /**
     * @param $data
     * @param $memberId
     * @return $payment
     */
    public static function update($data, $id)
    {
        if (isset($data['old_number_payment'])) {
            $payment = MemberPaymentsYearly::where('number_payment', $data['old_number_payment'])->where('is_delete', 0)->first();
            $memberId = $payment->memberId;
            MemberPaymentsYearly::where('number_payment', $data['old_number_payment'])->where('is_delete', 0)->delete();
        }
        else {
            $payment = MemberPaymentsYearly::find($id);
            $memberId = $payment->memberId;
            MemberPaymentsYearly::where('number_payment', $payment->number_payment)->where('is_delete', 0)->delete();
        }

        $realCurrency = self::refomatCurrency($data['currency']);
        $isPayment = MemberPaymentsYearly::where('memberId', $memberId)->where('is_delete', 0)->first();
        $var = $data['date_expiration'];
        $expirationDate = str_replace('/', '-', $var);

        $arr[] = [
            'memberId' => $memberId,
            'number_payment' => $data['number_payment'],
            'date_payment' => date('Y-m-d H:i:s', strtotime(str_replace('/','-',$data['date_payment']))),
            'currency' => $realCurrency,
            'payment_content' => $data['content'],
            'note' => $data['note'],
            'currency_type' => 'vnd',
            'is_delete' => 0,
            'payment_method' => 'payment',
            'year' => Carbon::parse($expirationDate)->year,
            'flag' => empty($isPayment) ? 0 : 1,
            'created_at' => date('Y-m-d H:i:s'),
            'number_of_year' => $realCurrency/500000,
            'member_code_expiration' => date('Y-m-d 00:00:00', strtotime($expirationDate))
        ];

        DB::beginTransaction();
        try {
            // insert payment yearly
            $payment = MemberPaymentsYearly::insert($arr);
            // insert waiting print
            $objMemberStatus = Member::select('status')->where('id', $memberId)->first();
            if (!empty($objMemberStatus) && $objMemberStatus->status == MemberConstants::MEMBER_OFFICIAL_MEMBER) {
                WaitingPrint::where('memberId', $memberId)->delete();
                $waitingPrint = new WaitingPrint();
                $waitingPrint->memberId = $memberId;
                $waitingPrint->cardType = 1;
                $waitingPrint->created_at = date('Y-m-d H:i:s');
                $waitingPrint->save();
            }

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_member_payment_yearly_fail', 'Error when accept member payment yearly (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$data], true) . ')');
            $payment = null;
        }

        return $payment;
    }

    /**
     * @param $memberId
     * @return $memberPayment
     */

    public static function _getPaymentsByMemberId($memberId)
    {
        $memberPayment = MemberPaymentsYearly::where('memberId', $memberId)
            ->where('is_delete', 0)
            ->first();

        return empty($memberPayment) ? null : $memberPayment;
    }

    public static function deletePayment($id)
    {
        $payment = self::getMemberPaymentById($id);

        if (!(empty($payment))) {
            MemberPaymentsYearly::where('number_payment', $payment->number_payment)->update(['is_delete' => 1]);
            WaitingPrint::where('memberId', $payment->memberId)->where('cardType', 1)->delete();
        }
        return null;
    }

    /**
     * Create payment yearly
     *
     * @param $data
     * @param $memberId
     * @return MemberPaymentsYearly|null
     */
    public static function create($data, $memberId)
    {
        $isPayment = self::_getPaymentsByMemberId($memberId);
        $arr = [];
        $realCurrency = self::refomatCurrency($data['currency']);
        $var = $data['date_expiration'];
        $expirationDate = str_replace('/', '-', $var);
        $arr[] = [
            'memberId' => $memberId,
            'number_payment' => $data['number_payment'],
            'date_payment' => date('Y-m-d H:i:s', strtotime(str_replace('/','-',$data['date_payment']))),
            'currency' => $realCurrency,
            'payment_content' => $data['content'],
            'note' => $data['note'],
            'currency_type' => 'vnd',
            'is_delete' => 0,
            'payment_method' => 'payment',
            'year' => Carbon::parse($expirationDate)->year,
            'flag' => !empty($isPayment) ? 1 : 0,
            'created_at' => date('Y-m-d H:i:s'),
            'number_of_year' => $realCurrency/500000,
            'member_code_expiration' => date('Y-m-d 00:00:00', strtotime($expirationDate))
        ];

        DB::beginTransaction();
        try {
            // insert payment yearly
            $payment = MemberPaymentsYearly::insert($arr);
            // insert waitting print
            $objMemberStatus = Member::select('status')->where('id', $memberId)->first();
            if (!empty($objMemberStatus) && $objMemberStatus->status == MemberConstants::MEMBER_OFFICIAL_MEMBER) {
                WaitingPrint::where('memberId', $memberId)->delete();
                $waitingPrint = new WaitingPrint();
                $waitingPrint->memberId = $memberId;
                $waitingPrint->cardType = 1;
                $waitingPrint->created_at = date('Y-m-d H:i:s');
                $waitingPrint->save();
            }

            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::exceptionByFile('create_payment_yearly_fail', $exception, $data);
            $payment = null;
        }

        return $payment;
    }

    public static function searchMemberByConditionsPaymentFirst($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE, $type = "") {
        $query = Member::selectRaw('members.file_code, members.member_code, members.touristGuideCode, members.fullName, members.firstMobile, members.approved_at, members.province_code, members.status, members.id')
            ->where('members.phoneVerified', 1)
            ->where('members.acceptTermsAndPolicies', 1)
            ->whereNull('members.is_delete')
            ->where('members.status', 4)
            ->orderBy('members.id');

        self::searchInfoCommon($query, $arrConditions);

        if ($type === 'export') {
            return $query->get();
        }
        return $query->paginate($limit);
    }

    public static function searchMemberByConditionsPaymentYearly($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE, $type = "") {
        $fromYear = $arrConditions['from-year'];
        $toYear = $arrConditions['to-year'];
        $status = 1;

        $conditionYear = "";
        if (!empty($fromYear) && empty($toYear)) {
            $conditionYear = "where year = {$fromYear}";
        }

        if (!empty($toYear) && empty($fromYear)) {
            $conditionYear = "where year = {$toYear}";
        }

        if (!empty($toYear) && !empty($fromYear)) {
            $conditionYear = "where year between {$fromYear} and {$toYear}";
        }
        $subQuery = MemberPaymentsYearly::from('member_payments_yearly as tt')->select('tt.id', 'tt.number_payment', 'tt.date_payment', 'groupedtt.MaxYear as year', 'tt.currency', 'groupedtt.memberId', 'tt.is_delete', 'tt.created_at', 'tt.number_of_year', 'tt.flag')
            // ->leftJoin('members','tt.memberId', '=', 'members.id')     
            // ->leftJoin('branches','members.province_code', '=', 'branches.id')     
            ->join(DB::raw("(SELECT memberId, MAX(year) AS MaxYear FROM member_payments_yearly ".$conditionYear." GROUP BY memberId) groupedtt")
                        
                        , function ($query) {
                            $query->on('tt.memberId', '=', 'groupedtt.memberId')
                                  ->on('tt.year', '=', 'groupedtt.MaxYear');
                        })
                        ->groupBy('groupedtt.memberId')
                        ->groupBy('groupedtt.MaxYear')
                        ->orderBy('tt.created_at', 'DESC');

        if ($status == 1) {
            $query = Member::selectRaw('
                    members.file_code, 
                    members.member_code,
                    members.touristGuideCode,
                    members.fullName, 
                    members.firstMobile, 
                    members.approved_at, 
                    members.province_code, 
                    members.status, 
                    members.id as mid,
                    members.member_code_expiration,
                    payment_yearly.id as mpuid,
                    payment_yearly.number_payment,
                    payment_yearly.date_payment,
                    payment_yearly.year,
                    payment_yearly.currency,
                    payment_yearly.created_at,
                    payment_yearly.number_of_year,
                    payment_yearly.flag,
                    branches.name as province_name
                ')
            ->leftJoin(
                DB::raw("({$subQuery->toSql()}) AS payment_yearly"), 
                'members.id', 
                '=', 
                'payment_yearly.memberId')
            // ->where('payment_yearly.is_delete', 0)
            ->leftJoin('branches','members.province_code', '=', 'branches.id')  
            ->where('members.phoneVerified', 1)
            ->where('members.acceptTermsAndPolicies', 1)
            ->whereNull('members.is_delete')
            ->whereIn('members.status', [5, 6, 61, 13, 4]);
        }

        if ($status == 0) {
            $queryMemberPaymentYearly = $subQuery->get()->toArray();
            $memberIds = array_column($queryMemberPaymentYearly, 'memberId');
            $query = Member::selectRaw('members.file_code, members.member_code, members.touristGuideCode, members.fullName, members.firstMobile, members.approved_at, members.province_code, members.status, members.id as mid, members.member_code_expiration')
            ->where('members.phoneVerified', 1)
            ->where('members.acceptTermsAndPolicies', 1)
            ->whereNull('members.is_delete')
            ->whereIn('members.status', [5, 6, 61, 13])
            ->whereNotIn('members.id', $memberIds)
            ->orderBy('members.id');
          
        }
        
        self::searchMemberCode($query, $arrConditions['member_code']);

        if ($status == 1) {
            if(!empty($arrConditions[self::TO_DATE])) {
                $toDate = date('Y-m-d', strtotime(str_replace('/', '-', $arrConditions[self::TO_DATE])));
                $query->whereDate('payment_yearly.' . self::DATE_PAYMENT, '<=', $toDate . ' 23:59:59');
            }

            if(!empty($arrConditions[self::FROM_DATE])) {
                $fromDate = date('Y-m-d', strtotime(str_replace('/', '-', $arrConditions[self::FROM_DATE])));
                $query->whereDate('payment_yearly.' . self::DATE_PAYMENT, '>=', $fromDate . ' 00:00:00');
            }
        }

        self::searchInfoCommon($query, $arrConditions);

        if ($type === 'export') {
            return $query->get();
        }

        // dd($query->toSql());

        return $query->paginate($limit);
    }

    public static function searchMemberByConditionsPayment($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE)
    {
        $fromYear = $arrConditions['from-year'];
        $toYear = $arrConditions['to-year'];
        $memberCode = $arrConditions['member_code'];
        $fileCode = $arrConditions['file_code'];

        //trang thai trong (empty)
        if ($arrConditions[CommontConstants::STATUS] == 1) {
            if (!empty($fromYear) || !empty($toYear)) {
                if (!empty($fileCode) && empty($memberCode)) {
                    $listMember = Member::select(
                        DB::raw('members.id as mid, 
                        members.*, 
                        null as mpuid, 
                        null as number_payment, 
                        null as date_payment, 
                        null as year, null as memberId, 
                        null as currency, 
                        null as currency_type, 
                        null as payment_method, 
                        null as payment_content, 
                        null as note, 
                        null as flag, 
                        null as number_of_year, 
                        null as exp'))
                        ->where('members.status', '<' , 5)
                        ->where(['members.phoneVerified' => 1, 'members.acceptTermsAndPolicies' => 1])
                        ->whereNull('members.is_delete')
                        ->orderBy('members.createdAt' , 'desc');
                }

                if (!empty($memberCode)) {
                    $listMember = DB::table('members')->select(
                        DB::raw('members.id as mid, 
                            members.*, 
                            mp.id as mpuid, 
                            mp.number_payment, 
                            mp.date_payment, 
                            mp.year, 
                            mp.memberId, 
                            mp.currency, 
                            mp.currency_type, 
                            mp.payment_method, 
                            mp.payment_content, 
                            mp.note, 
                            mp.flag, 
                            mp.number_of_year, 
                            mp.member_code_expiration as exp'))
                        ->join('member_payments_yearly as mp',function ($join) {
                            $join->on('members.id', '=', 'mp.memberId')
                                ->where('mp.is_delete', 0);
                        })
                        ->whereNull('members.is_delete')
                        ->where('members.status', '>=', 5)
                        ->where(['members.phoneVerified' => 1, 'members.acceptTermsAndPolicies' => 1]);

                    self::searchMemberCode($listMember, $memberCode);

                    if (!empty($fromYear)) {
                        $listMember->where('mp.year', '>=', $fromYear);
                    }

                    if (!empty($toYear)) {
                        $listMember->where('mp.year', '<=', $toYear);
                    }
                }
            }
            else {
                // danh sach hoi vien cho dong hoi phi nam dau
                $listMember = Member::select(
                    DB::raw('members.id as mid, 
                    members.*, 
                    null as mpuid, 
                    null as number_payment, 
                    null as date_payment, 
                    null as year, null as memberId, 
                    null as currency, 
                    null as currency_type, 
                    null as payment_method, 
                    null as payment_content, 
                    null as note, 
                    null as flag, 
                    null as number_of_year, 
                    null as exp'))
                    ->where(['members.phoneVerified' => 1, 'members.acceptTermsAndPolicies' => 1, 'members.status' => 4])
                    ->whereNull('members.is_delete')
                    ->orderBy('members.createdAt' , 'desc');
                }
        }

        //chua nop hoi phi
        if($arrConditions[CommontConstants::STATUS] == 2){
            if (empty($memberCode) && empty($fromYear) && empty($toYear)) {
                // danh sach hoi vien chua dong hoi phi nam dau
                $listMember = Member::select(
                    DB::raw('members.id as mid, 
                    members.*, 
                    null as mpuid, 
                    null as number_payment, 
                    null as date_payment, 
                    null as year, null as memberId, 
                    null as currency, 
                    null as currency_type, 
                    null as payment_method, 
                    null as payment_content, 
                    null as note, 
                    null as flag, 
                    null as number_of_year, 
                    null as exp'))
                    ->where('members.status', '<' , 5)
                    ->where(['members.phoneVerified' => 1, 'members.acceptTermsAndPolicies' => 1])
                    ->whereNull('members.is_delete')
                    ->orderBy('members.createdAt' , 'desc');
            } else {
                // danh sach hoi vien chua dong hoi phi theo nam
                $listMember = DB::table('members')->select(
                    DB::raw('members.id as mid, 
                    members.*, 
                    mp.id as mpuid, 
                    mp.number_payment, 
                    mp.date_payment, 
                    mp.year, 
                    mp.memberId, 
                    mp.currency, 
                    mp.currency_type, 
                    mp.payment_method, 
                    mp.payment_content, 
                    mp.note, 
                    mp.flag, 
                    mp.number_of_year, 
                    mp.member_code_expiration as exp'))
                    ->join('member_payments_yearly as mp',function ($join) {
                        $join->on('members.id', '=', 'mp.memberId')
                            ->where('mp.is_delete', 0);
                    })
                    ->whereNull('members.is_delete')
                    ->where(['members.phoneVerified' => 1, 'members.acceptTermsAndPolicies' => 1]);

                if (!empty($fromYear)) {
                    if (!empty($toYear)) {
                        $listMember->where('mp.year', '<', $toYear);
                    }
                    else {
                        $listMember->where('mp.year', '<', $fromYear);
                    }
                }
                else {
                    if(!empty($toYear)) {
                        $listMember->where('mp.year', '<', $toYear);
                    }
                }

                self::searchMemberCode($listMember, $memberCode);
            }
        }

        //da nop hoi phi
        if($arrConditions[CommontConstants::STATUS] == 3){
            // list danh sach hoi vien da nop hoi phi
            $listMember = DB::table('members')->select(
                DB::raw('members.id as mid, 
                    members.*, 
                    mp.id as mpuid, 
                    mp.number_payment, 
                    mp.date_payment, 
                    mp.year, 
                    mp.memberId, 
                    mp.currency, 
                    mp.currency_type, 
                    mp.payment_method, 
                    mp.payment_content, 
                    mp.note, 
                    mp.flag, 
                    mp.number_of_year, 
                    mp.member_code_expiration as exp'))
                ->join('member_payments_yearly as mp',function ($join) {
                    $join->on('members.id', '=', 'mp.memberId')
                        ->where('mp.is_delete', 0);
                })
                ->whereNull('members.is_delete')
                ->where('members.status', '>=', 5)
                ->where(['members.phoneVerified' => 1, 'members.acceptTermsAndPolicies' => 1]);

            self::searchMemberCode($listMember, $memberCode);

            if (!empty($fromYear)) {
                $listMember->where('mp.year', '>=', $fromYear);
            }

            if (!empty($toYear)) {
                $listMember->where('mp.year', '<=', $toYear);
            }
        }

        self::searchInfoCommon($listMember, $arrConditions);

        return $listMember->paginate($limit);
    }

    public static function searchMemberCode(&$query, $memberCode) {
        if (!empty($memberCode)) {
            $query->where('members.member_code', 'like', '%' . $memberCode . '%');
        }
    }

    public static function searchInfoCommon(&$query, $arrConditions) {
        if (!empty($arrConditions['touristGuideCode'])) {
            $query->where('members.touristGuideCode', 'like', '%' . $arrConditions['touristGuideCode'] . '%');
        }

        if (!empty($arrConditions['file_code'])) {
            $query->where('members.file_code', 'like', '%' . $arrConditions['file_code'] . '%');
        }

        if (!empty($arrConditions['fullName'])) {
            $query->where('members.fullName', 'like', '%' . $arrConditions['fullName'] . '%');
        }

        //search by number_payment
        if (!empty($arrConditions['number_payment'])) {
            $query->where('payment_yearly.number_payment', 'like', '%' . $arrConditions['number_payment'] . '%');
        }

        //search by flag
        if (!empty($arrConditions['flag'])) {
            if($arrConditions['flag'] == 2) {
                $query->where('members.status', 4);
            } else {
                $query->where('members.status','<>', 4);
            }
            
        }

        //search by chi hội
        if(!empty($arrConditions[self::PROVINCE_CODE])) {  
            $query->where('members.' . self::PROVINCE_CODE, $arrConditions[self::PROVINCE_CODE]);
        }
    }

    public static function getMemberPaymentsByIds($ids ){
        $objMember = Member::whereIn('id', [1, 2, 3])->get();
    }

    public static function updateOrCreate ($memberId, array $arrUpdateData)
    {
        DB::beginTransaction();
        try {
            $objMemberPayment = MemberPaymentsYearly::updateOrCreate(
                ['memberId' => $memberId],
                $arrUpdateData
            );
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_or_create_member_payment_yearly_fail', 'Error when update member payment yearly (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId, $arrUpdateData], true) . ')');
            $objMemberPayment = null;
        }

        return $objMemberPayment;
    }

    public static function getMemberById($idMember)
    {
        return Member::whereNull('is_delete')->where('id', $idMember)->first();
    }

    public static function getByMemberIdAndYear($memberId, $year)
    {
        return MemberPaymentsYearly::where('is_delete', 0)
            ->where('memberId', $memberId)
            ->where('year', $year)
            ->first();
    }

    public static function getByMemberIdAndNumberOfYear($memberId, $year)
    {
        return MemberPaymentsYearly::where('is_delete', 0)
            ->where('memberId', $memberId)
            ->where('number_of_year', $year)
            ->first();
    }

    // Toàn quốc
    public static function getSumAllPaymentCreated ()
    {
        return MemberPaymentsYearly::where('is_delete', 0)
            ->whereNotNull('year')
            ->sum('currency');
    }

    // Chi hội, clb thuộc hội
    public static function getSumCustomPaymentCreated ($isChihoi)
    {
        // tim theo dieu kien chi hoi hoặc clb thuộc hội, lọc ra list member id rồi tìm payment
        $arrMemberId = Member::where('id', 10)->pluck('id');

        return MemberPaymentsYearly::where('is_delete', 0)
            ->whereNotNull('year')
            ->whereIn('memberId', $arrMemberId)
            ->sum('currency');
    }

    //Tổng tiền đã nộp
    public static function allCurrentByYear($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE)
    {
        $limit = 10000;
        $arrKeyConditions = array_keys($arrConditions);
        $year = $arrConditions['fromYear'];
        $arrStatus = [
            MemberConstants::MEMBER_OFFICIAL_MEMBER,//13 hoi vien chinh thuc
            MemberConstants::SIGNING_WAITING,//5 da dong le phi
            MemberConstants::CODE_PROVIDING_WAITING,//6 cho cap ma
			MemberConstants::CODE_PROVIDING_REWAITING// nâng cấp thẻ
//            MemberConstants::CARD_PRINTING_WAITING,//9 cho in the
        ];
        $query = Member::whereIn('status', $arrStatus)
            ->where('members.is_delete', null)
            ->rightJoin('member_payments_yearly as mp', function($join){
                $join->on('members.id', '=', 'mp.memberId')
                    ->where('mp.is_delete', 0)
                    ->orderBy('mp.date_payment' , 'desc');
            })->select('members.id AS mid', 'members.*', 'mp.id AS mpuid', 'mp.*')->whereNotNull('mp.flag');

        if(empty($arrConditions[self::FROM_DATE]) && empty($arrConditions[self::TO_DATE])) {
            if(!empty($arrConditions['from-year']) && !empty($arrConditions['to-year'])) {
                $query->where('mp.year', '>=', $arrConditions['from-year'])
                      ->where('mp.year', '<=', $arrConditions['to-year']);
            }
        }

        // search by văn phòng đại diện
        MemberServiceProvider::searchByVpd($arrConditions, $query);

        $getAllProvinceTypes = Offices::searchByAllOffices();
        $branchesId = array();
        foreach ($getAllProvinceTypes as $provinceTypes) {
            $branchesId[] = $provinceTypes->id;
        }

        if(!empty($arrConditions[self::PROVINCE_CODE])) {
            // search với chi hội
            MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
        }
        //search theo ngay thang
        if(in_array(self::TO_DATE, $arrKeyConditions) && !empty($arrConditions[self::TO_DATE])) {
            $query->whereDate('mp.' . self::DATE_PAYMENT, '<=', $arrConditions[self::TO_DATE] . ' 23:59:59');
        }

        if(in_array(self::FROM_DATE, $arrKeyConditions) && !empty($arrConditions[self::FROM_DATE])) {
            $query->whereDate('mp.' . self::DATE_PAYMENT, '>=', $arrConditions[self::FROM_DATE] . ' 00:00:00');
        }

        return $query->sum('mp.currency');
        /*$query = '';
        $query = Member::where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)
            ->rightJoin('member_payments_yearly as mp', function($join) use ($year){
                $join->on('members.id', '=', 'mp.memberId')
                    ->where('mp.is_delete', 0)
                    ->where('mp.year', $year);
            })
            ->sum('mp.currency');
        return $query;*/
    }

    /**
     * Bootstrap the application services.
     *
     * @return $objectMemberPayment
     */
    public static function getAllPaymentYearlyByMemberId($id)
    {

        if (empty($id)) {
            $objectMemberPaymentYearly = null;
        } else {
            $objectMemberPaymentYearly = self::_getAllPaymentYearlyByMemberId($id);
        }

        return $objectMemberPaymentYearly;
    }

    public static function _getAllPaymentYearlyByMemberId($memberId)
    {
        $memberPaymentsYearly = MemberPaymentsYearly::where('memberId', $memberId)
            ->where('is_delete', 0)
            ->orderBy('date_payment' , 'desc')
            ->get();

        return empty($memberPaymentsYearly) ? null : $memberPaymentsYearly;
    }

    public static function refomatCurrency($currency)
    {
        if (strpos($currency, ".") != false) {
            $leftNumber = substr($currency, 0, -3);
        }
        else {
            $leftNumber = $currency;
        }
        if (strpos($leftNumber, ",") != false) {
            $realCurrency = str_replace(",", "", $leftNumber);
        }
        else {
            $realCurrency = $currency;
        }
        return $realCurrency;
    }

    //Chưa nộp phí lần đầu
    public static function exportUnPaidByFirstYear($arrConditions)
    {
        // danh sach hoi vien cho dong hoi phi nam dau
        $listMember = Member::select('members.id AS mid', 'members.*')
            ->where(['members.phoneVerified' => 1, 'members.acceptTermsAndPolicies' => 1, 'members.status' => 4])
            ->whereNull('members.is_delete')
            ->orderBy('members.createdAt' , 'desc');

        self::searchInfoCommon($listMember, $arrConditions);

        $memberIdUnPaid = $listMember->get();
        return $memberIdUnPaid;
    }

    public static function deletePaymentYearlyNew($id, $status)
    {
        $payment = self::getMemberPaymentById($id);

        if (!(empty($payment))) {
            $objWaiting = WaitingPrint::where('memberId', $payment->memberId)->where('cardType', 1)->first();

            if ($status === 5 || ($status == 13 && !empty($objWaiting))) {
                MemberPaymentsYearly::where('number_payment', $payment->number_payment)->update(['is_delete' => 1]);
            }
            
            if ($status == 13) {
                WaitingPrint::where('memberId', $payment->memberId)->where('cardType', 1)->delete();
            } else if ($status == 5) {
                $member = Member::where('id', $payment->memberId)->first();
                if (empty($member)) {
                    return null;
                }

                $member->status = $status;
                $member->is_signed = NULL;

                if ($status == MemberConstants::FEE_WAITING) {
                    $member->is_fee = null;
                } 
                $member->save();

            } else {
                return null;
            }
            
        }
        return true;
    }
}

