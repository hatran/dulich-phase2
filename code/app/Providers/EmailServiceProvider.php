<?php

namespace App\Providers;

use App\Constants\MemberConstants;
use App\Libs\Helpers\LogsHelper;
use App\Models\Emails;
use Illuminate\Support\ServiceProvider;
use Response;
use Validator;
use Carbon\Carbon;
use App\Libs\Helpers\Utils;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Constants\BranchConstants;
use App\Constants\CommontConstants;
use App\Models\Option;
use App\Models\Offices;

class EmailServiceProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const TITLE = 'title';
    const OPTION_CODE = 'option_code';
    const ID = 'id';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_VERIFIED = 'is_verified';
    const IS_DELETE = 'is_delete';
    const MEMBER_TYPE = 'member_type';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';

    public static function getOffices ()
    {
        $office = Offices::whereIn('option_code', ['HEAD', 'OFFICES'])->whereNull('parent_id')->whereNull('deleted_at')->whereNotNull('status')->orderBy('created_at', 'des')->get();
        return $office;
    }

    public static function getAllOption ($options)
    {
        $objOption = Option::where('key', $options)->whereNull('deleted_at')
//            ->orderBy('id', 'des')
            ->get();

        return $objOption;
    }
    public static function getEmailsByOption ($option_code)
    {
        $objEmails = Emails::where(
            [
                'option_code'       => $option_code,
                'status'            => 1,
            ]
        )
//            ->whereDate('end_time', '>=', date('Y-m-d H:i:s'))
            ->orderBy('created_at', 'des')
            ->get()
            ->toArray();
        $data = [];
        if(count($objEmails) > 0){
            foreach ($objEmails as $value) {

                if($value['start_time'] == '' && $value['end_time'] == ''){
                    $data = $value;
                    break;
                }

                if($value['start_time'] != '' && $value['end_time'] == ''){
                    if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s'))){
                        $data = $value;
                        break;
                    }
                }

                if($value['start_time'] == '' && $value['end_time'] != ''){
                    if(strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                        $data = $value;
                        break;
                    }
                }

                if($value['start_time'] != '' && $value['end_time'] != ''){
                    if(strtotime($value['start_time']) <= strtotime(date('Y-m-d H:i:s')) && strtotime($value['end_time']) >= strtotime(date('Y-m-d'). ' 00:00:00')){
                        $data = $value;
                        break;
                    }
                }
            }
        }
        return $data;
    }

    public static function emailsSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];

        if (!empty($arrConditions['title'])) {
            $arrSearchCondition = [['title', 'like', '%' . $arrConditions['title'] . '%']];
        }

        if ($arrConditions['created_at'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['created_at', '<=', $arrConditions['created_at'].' 23:59:59']]);
            $arrSearchCondition = array_merge($arrSearchCondition, [['created_at', '>=', $arrConditions['created_at'].' 00:00:00']]);
        }

        if ($arrConditions['option_code'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['option_code', '=', $arrConditions['option_code']]]);
        }
        $objEmail = Emails::where($arrSearchCondition);

        if ($arrConditions['status'] != '') {
            $objEmail = $objEmail->where('status', $arrConditions['status']);
        }
        if(!empty($arrConditions['branch_id'])) {
            if(!$isCount){
                $objEmail = $objEmail->where('branch_id', $arrConditions['branch_id'])->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objEmail = $objEmail->where('branch_id', $arrConditions['branch_id'])->get();
            }
        } else {
            if(!$isCount){
                $objEmail = $objEmail->orderBy('created_at', 'des')->paginate($limit);
            }else{
                $objEmail = $objEmail->get();
            }
        }
        

//print_r(json_decode(json_encode($objNew)));die;
        return $objEmail;
    }

    public static function delete ($id)
    {
        $objEmail = self::getEmailById($id);
        if (empty($objEmail)) {
            return false;
        }

        try {
            $objEmail->delete();
//            DB::commit();
            return true;
        } catch (\Exception $exception) {
//            DB::rollBack();
            LogsHelper::trackByFile('delete_email_fail', 'Error when delete email (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . $id);
            return false;
        }
    }

    public static function getEmailById($id)
    {
        return Emails::find($id);
    }

    public static function getEmailByOptionCode($id, $status, $option_code)
    {

        //check add
        $arrSearchCondition = [];
        $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', $status]]);
        $arrSearchCondition = array_merge($arrSearchCondition, [['option_code','=', $option_code]]);
        if($id != 'null'){
            $arrSearchCondition = array_merge($arrSearchCondition, [['id','<>', $id]]);
        }
        $objEmail = Emails::where($arrSearchCondition)->get();
        return count($objEmail) > 0 ? false : true;

        //print_r(json_decode(json_encode($objNew)));die;
    }

    public static function update ($arrUpdateData)
    {
        $id = $arrUpdateData['id'];
        if (empty($id)) {
            return false;
        }

        $objEmail = self::getEmailById($id);

        if (!empty($arrUpdateData['option_code'])) {
            $objEmail->option_code = $arrUpdateData['option_code'];
        }

        if (!empty($arrUpdateData['title'])) {
            $objEmail->title = $arrUpdateData['title'];
        }

        if (!empty($arrUpdateData['start_time'])) {
            $objEmail->start_time = $arrUpdateData['start_time'];
        }
        else {
            $objEmail->start_time = null;
        }

        if (isset($arrUpdateData['end_time']) && $arrUpdateData['end_time'] != '') {
            $objEmail->end_time = $arrUpdateData['end_time'];
        }

        if (empty($arrUpdateData['end_time'])) {
            $objEmail->end_time = null;
        }

        if (isset($arrUpdateData['status'])) {
            $objEmail->status = $arrUpdateData['status'];
        }

        if (!empty($arrUpdateData['content'])) {
            $objEmail->content = $arrUpdateData['content'];
        }

        if (!empty($arrUpdateData['branch_id'])) {
            $objEmail->branch_id = $arrUpdateData['branch_id'];
        }

        if (!empty($arrUpdateData['auto_send_time'])) {
            $objEmail->auto_send_time = $arrUpdateData['auto_send_time'];
        }
        else {
            $objEmail->auto_send_time = null;
        }

        if (!empty($arrUpdateData['attach'])) {
            $objEmail->attach = $arrUpdateData['attach'];
        }

        try {
            $objEmail->save();
            DB::commit();
            return $objEmail;
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_email_fail', 'Error when update email (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$arrUpdateData], true) . ')');
            return null;
        }
    }


}
