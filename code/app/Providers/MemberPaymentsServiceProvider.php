<?php

namespace App\Providers;

use App\Models\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Libs\Helpers\LogsHelper;
use App\Models\MemberPayments;
use App\Models\MemberPaymentsYearly;
use App\Models\WaitingPrint;
use Carbon\Carbon;
use Mail;
use DomPDF;
use App\Constants\MemberConstants;
use Auth;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;

class MemberPaymentsServiceProvider extends ServiceProvider
{

    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const MEMBER_ID = 'memberId';
    const CREATED_AT = 'createdAt';
    const CURRENCY = 'currency';
    const CURRENCY_TYPE = 'currency_type';
    const PAYMENT_MENTHOD = 'payment_method';
    const PAYMENT_CONTENT = 'payment_content';
    const NOTE = 'note';
    const UPDATE_AT = 'updateAt';
    const D_M_Y_FORMAT = 'd/m/Y';

    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const STATUS = 'status';
    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const IS_VERIFIED = 'is_verified';
    const IS_FEE = 'is_fee';
    const IS_SIGNED = 'is_signed';
    const MEMBER_TYPE = 'member_type';
    const ORIGINAL_NAME = 'originalName';
    const ABSOLUTE_PATH = 'absPath';
    const RELATIVE_URL = 'relativeUrl';
    const FILE_ID = 'fileId';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';
    const NUMBER_PAYMENT = 'number_payment';
    const DATE_PAYMENT = 'date_payment';
    const PROVINCE_TYPE = 'province_type';

    /**
     *
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap the application services.
     *
     * @return $objectMemberPayment
     */
    public static function getMemberPaymentByMemberId($id)
    {

        if (empty($id)) {
            $objectMemberPayment = null;
        } else {
            $objectMemberPayment = self::_getPaymentsByMemberId($id);
        }

        return $objectMemberPayment;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getMemberPaymentById($id)
    {
        $objMember = MemberPayments::find($id);

        return $objMember;
    }

    /**
     * @param $data
     * @param $memberId
     * @return $payment
     */
    public static function updatePayment($data, $memberId)
    {
        try {
            $payment = self::_getPaymentsByMemberId($memberId);
            if (!(empty($payment))) {
                $payment->number_payment = $data['number_payment'];
                $payment->date_payment = Carbon::createFromFormat('d/m/Y', $data['date_payment'])->toDateString();
                $payment->currency = $data['currency'];
                $payment->payment_content = $data['content'];
                $payment->note = $data['note'];
                return $payment->save();

            }
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_payment_fail', 'Error when accept member payment_content (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId], true) . ')');
            return null;
        }
        return null;
    }

    /**
     * @param $memberId
     * @return $memberPayment
     */

    public static function _getPaymentsByMemberId($memberId){
        $memberPayment = MemberPayments::where('memberId', $memberId)
            ->where('is_delete', 0)
            ->first();

        return empty($memberPayment) ? null : $memberPayment;
    }

    /**
     * @param $memberId
     * @return $memberPayment
     */
    public static function _checkPaymentMemberExits($memberId)
    {
        $memberPayment = MemberPayments
            ::where('memberId', $memberId)
            ->first();

        return empty($memberPayment) ? null : $memberPayment;
    }

    /**
     * @param $memberId
     * @return $payment
     */

    public static function deletePayment($number_payment){
        $payment = MemberPayments::select('memberId')->where('number_payment', $number_payment)->first();

        if (!empty($payment)) {
            MemberPaymentsYearly::where('number_payment', $number_payment)->update(['is_delete' => 1]);
            MemberPayments::where('number_payment', $number_payment)->update(['is_delete' => 1]);
            WaitingPrint::where('memberId', $payment->memberId)->where('cardType', 1)->delete();
        }

        // $payment->is_delete = 1;
        // $payment->save();
        
        return null;
    }

    /**
     * @param $data
     * @param $memberId
     */
    public static function createPayment($data, $memberId)
    {
        $payment = self::_checkPaymentMemberExits($memberId);
        if (empty($payment)) {
            $payment = new MemberPayments();
            $payment->memberId = $memberId;
            $payment->number_payment = $data['number_payment'];
            $payment->date_payment = Carbon::createFromFormat('d/m/Y', $data['date_payment'])->toDateString();
            $payment->currency = $data['currency'];
            $payment->payment_content = $data['content'];
            $payment->note = $data['note'];
            $payment->currency_type = 'vnd';
            $payment->is_delete = '0';
            $payment->payment_method = 'payment';
        } else {
            $payment->number_payment = $data['number_payment'];
            $payment->date_payment = Carbon::createFromFormat('d/m/Y', $data['date_payment'])->toDateString();
            $payment->currency = $data['currency'];
            $payment->payment_content = $data['content'];
            $payment->note = $data['note'];
            $payment->currency_type = 'vnd';
            $payment->is_delete = '0';
            $payment->payment_method = 'payment';
        }
        $payment->save();
        return MemberPayments::created($payment);
    }

    private static function extractStatusValues($statusDict)
    {
        return collect($statusDict)->keys()->all();
    }

    public static function searchMemberByConditionsPayment($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE, $isCount = false)
    {
        
        $query = DB::table('member_payments')->select('members.file_code', 'members.id', 'members.fullName', 'members.verified_at', 'members.approved_at', 'members.status', 'members.touristGuideCode', 'member_payments.is_delete', 'member_payments.number_payment', 'member_payments.date_payment', 'member_payments.currency')
            ->rightJoin('members', 'members.id', '=', 'member_payments.memberId')
            ->where('members.phoneVerified', 1)
                 ->where('members.acceptTermsAndPolicies', 1);

        $arrKeyConditions = array_keys($arrConditions);
        if (in_array(self::STATUS, $arrKeyConditions) && !empty($arrConditions[self::STATUS])) {
            // search theo trạng thái được lựa chọn
            $query->where('members.status' , $arrConditions[self::STATUS]);
        } else {
            // list  all hồ sơ có status = 4 chờ cập nhật lệ phí + hội phí và status = 5 chờ cấp mã thẻ hội viên
            $query->whereIn('members.status' , self::extractStatusValues(MemberConstants::$file_accountant_const));
        }
        
        if(in_array(self::TO_DATE, $arrKeyConditions) && !empty($arrConditions[self::TO_DATE])) {
            $query->whereDate('member_payments.' . self::DATE_PAYMENT, '<=', $arrConditions[self::TO_DATE] . ' 23:59:59');
        }
        
        if(in_array(self::FROM_DATE, $arrKeyConditions) && !empty($arrConditions[self::FROM_DATE])) {
            $query->whereDate('member_payments.' . self::DATE_PAYMENT, '>=', $arrConditions[self::FROM_DATE] . ' 00:00:00');
        }

        if(in_array(self::NUMBER_PAYMENT, $arrKeyConditions) && !empty($arrConditions[self::NUMBER_PAYMENT])) {
            $query->where('member_payments.' . self::NUMBER_PAYMENT , 'like', '%' . addslashes($arrConditions[self::NUMBER_PAYMENT]) . '%');
        }

        if(!empty($arrConditions[self::PROVINCE_CODE])) {
            MemberServiceProvider::searchByProvinceCode($arrConditions, $query);
        } 
       
        if (in_array(self::PROVINCE_TYPE, array_keys($arrConditions)) && !empty($arrConditions[self::PROVINCE_TYPE])) {
            //$arrBranchesIds = Offices::getProvinceCodeByProvinceType($arrConditions[self::PROVINCE_TYPE]);
            $query->whereIn(self::PROVINCE_CODE, $arrConditions[self::PROVINCE_TYPE]);
        }   

        // search by common
        MemberServiceProvider::searchByCommon($arrConditions, $query);
        $query->where('members.is_delete' , null);
        $query->orderBy('members.file_code' , 'asc');

        if(!$isCount){
            $objPayment = $query->paginate($limit);
        }else {
            $objPayment = $query->get();
        }
        return $objPayment;
    }


    public static function getMemberPaymentsByIds($ids ){
        $objMember = Member::whereIn('id', [1, 2, 3])->get();
    }

    public static function updateOrCreate ($memberId, array $arrUpdateData)
    {
        DB::beginTransaction();
        try {
            $objMemberPayment = MemberPayments::updateOrCreate (
                ['memberId' => $memberId],
                $arrUpdateData
            );
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_or_create_member_payment_fail', 'Error when update member payment (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$memberId, $arrUpdateData], true) . ')');
            $objMemberPayment = null;
        }

        return $objMemberPayment;
    }
    
    public static function getMemberById($idMember) {
        return Member::whereNull('is_delete')->where('id', $idMember)->first();
    }

    public static function refomatCurrency($currency)
    {
        if (strpos($currency, ".") != false) {
            $curr = explode($currency, ".");
            $leftNumber = $curr[0];
        }
        else {
            $leftNumber = $currency;
        }
        if (strpos($leftNumber, ",") != false) {
            $realCurrency = str_replace(",", "", $leftNumber);
        }
        else {
            $realCurrency = $currency;
        }

        return $realCurrency;
    }
}
