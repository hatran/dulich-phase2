<?php

namespace App\Providers;

use App\Models\LeaderSigningDecision;
use Illuminate\Support\ServiceProvider;

class LeaderSigningDecisionProvider extends ServiceProvider
{

    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const FULL_NAME = 'fullname';
    const POSITION = 'position';
    const IS_DELETE = 'is_delete';
    const PAGINATOR = 'paginator';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    public static function getAllLeaderSignDecision()
    {

      $leaderObject = LeaderSigningDecision::where('status', '=' , 1)->get();
      return $leaderObject;
    }
    public static function getLeaderById($id)
    {

        if(empty($id))
            return null;

        $leaderObject =   LeaderSigningDecision::where('id',$id)
                                    ->where('is_delete','!=', 1)->get();
        return $leaderObject;
    }
    public static function getLeaderSignDecisionFirst()
    {
      $leaderObject = LeaderSigningDecision::select('id')->where('status', '=' , 1)->first();
      return $leaderObject;
    }
}
