<?php
namespace App\Providers;

use App\Constants\BranchConstants;
use App\Constants\UserConstants;
use App\Http\Controllers\Controller;
use App\Libs\Helpers\LogsHelper;
use App\Models\Branches;
use App\Models\Offices;
use App\Models\User;
use App\Models\FeePolicy;
use App\Models\Groups;
use App\Repositories\Branches\BranchesRepository;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendEmail;

class FeePolicyServiceProvider extends AppServiceProvider
{
    const DEFAULT_LIMIT_USER_PER_PAGE = 10;
    const BCC_EMAIL = 'hoihdv@gmail.com';

    public static function getAllUser ($limit = self::DEFAULT_LIMIT_USER_PER_PAGE)
    {
        try{
            $objFeePolicy = FeePolicy::orderBy('created_at', 'des')
            ->with(['fee_policy', 'branch'])
            ->paginate($limit);
        } catch (\Exception $ex) {
            dd($ex);
        }
        
            
            
        return $objFeePolicy;
    }


    public static function getUserById($id)
    {
        $objUser = User::find($id);

        return $objUser;
    }

    public static function getUserByMemberId ($memberId)
    {
        $objUser = User::where(User::MEMBER_ID, $memberId)->first();

        return $objUser;
    }

    public static function searchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_USER_PER_PAGE)
    {
        $arrSearchCondition = [];

        if (!empty($arrConditions['name'])) {
            $arrSearchCondition[] = ['name', 'like', '%' . $arrConditions['name'] . '%'];
        }

        if (!empty($arrConditions['code'])) {
            $arrSearchCondition[] = ['code', 'like', '%' . $arrConditions['code'] . '%'];
        }

        if (!empty($arrConditions['start_date'])) {
            $dateArr = explode ("/", $arrConditions['start_date']);
            $start_date = "".$dateArr[2]."-".$dateArr[1]."-".$dateArr[0]." 00:00:00";
            $arrSearchCondition[] = ['start_date', '>=', $start_date];
        }

        if (!empty($arrConditions['end_date'])) {
            $dateArr = explode ("/", $arrConditions['end_date']);
            $end_date = "".$dateArr[2]."-".$dateArr[1]."-" .$dateArr[0]." 23:59:59";
            $arrSearchCondition[] = ['end_date', '<=', $end_date];
        }

        // if (!empty($arrConditions['email'])) {
        //     $arrSearchCondition[] = ['email', 'like', '%' . $arrConditions['email'] . '%'];
        // }

        // if (!empty($arrConditions['role'])) {
        //     $arrSearchCondition[] = ['role', '=', $arrConditions['role']];
        // }

        // if (!empty($arrConditions['province_type'])) {
        //     $arrSearchCondition[] = ['province_type', '=', $arrConditions['province_type']];
        // }

        if (empty($arrSearchCondition)) {
            // $objUser = User::where('role', '<>', UserConstants::NORMAL_USER)
            //     ->orderBy('created_at', 'des')
            //     ->paginate($limit);

            $objUser = FeePolicy::orderBy('created_at', 'des')
                    ->paginate($limit);
        } else {
            // $objUser = User::where('role', '<>', UserConstants::NORMAL_USER)
            //     ->where($arrSearchCondition)
            //     ->orderBy('created_at', 'des')
            //     ->paginate($limit);

            $objUser = FeePolicy::where($arrSearchCondition)
                ->orderBy('created_at', 'des')
                ->paginate($limit);
        }

        return $objUser;
    }

    public static function searchByProvince($id)
    {
        $arrSearchCondition = [];


            $arrSearchCondition[] = ['province_type', '=', $id];

        if (empty($arrSearchCondition)) {
            // $objUser = User::where('role', '<>', UserConstants::NORMAL_USER)
            //     ->orderBy('created_at', 'des')
            //     ->paginate($limit);
            
            $objUser = FeePolicy::orderBy('created_at', 'des')
                    ->paginate(self::DEFAULT_LIMIT_USER_PER_PAGE);
        } else {
            // $objUser = User::where('role', '<>', UserConstants::NORMAL_USER)
            //     ->where($arrSearchCondition)
            //     ->orderBy('created_at', 'des')
            //     ->paginate($limit);
           // dd(FeePolicy::where($arrSearchCondition)->orderBy('created_at', 'des'));
           try{
            $objFeePolicy = User::where($arrSearchCondition) -> orderBy('created_at', 'des')
            ->with(['fee_policy', 'branch'])
            ->paginate(self::DEFAULT_LIMIT_USER_PER_PAGE);
        } catch (\Exception $ex) {
            dd($ex);
        }
            // $objUser = FeePolicy::where($arrSearchCondition)
            //     ->orderBy('created_at', 'des')
            //     ->paginate(self::DEFAULT_LIMIT_USER_PER_PAGE);
            //     dd(123);
                
        }

        return $objUser;
    }

    public static function update ($arrUpdateData)
    {
        $id = $arrUpdateData['id'];
        if (empty($id)) {
            return false;
        }

        $objUser = self::getUserById($id);

        if (!empty($arrUpdateData['fullname'])) {
            $objUser->fullname = $arrUpdateData['fullname'];
        }

        if (!empty($arrUpdateData['username'])) {
            $objUser->username = $arrUpdateData['username'];
        }

        if (!empty($arrUpdateData['email'])) {
            $objUser->email = $arrUpdateData['email'];
        }

        if (!empty($arrUpdateData['role'])) {
            $objUser->role = $arrUpdateData['role'];
        }

        if (isset($arrUpdateData['status']) && $arrUpdateData['status'] != '') {
            $objUser->status = $arrUpdateData['status'];
        }

        if (!empty($arrUpdateData['province_type'])) {
            if ($arrUpdateData['province_type'] == '-1') {
                $objUser->province_type = null;
            } else {
                $objUser->province_type = $arrUpdateData['province_type'];
            }
        }

        if (!empty($arrUpdateData['password'])) {
            $objUser->password = $arrUpdateData['password'];
        }

        if (!empty($arrUpdateData['branch_id'])) {
            $objUser->branch_id = $arrUpdateData['branch_id'];
        }
        else {
            $objUser->branch_id = NULL;
        }

        if (!empty($arrUpdateData['club_id'])) {
            $objUser->club_id = $arrUpdateData['club_id'];
        }
        else {
            $objUser->club_id = NULL;
        }
        if (!empty($arrUpdateData['role']) && $arrUpdateData['role'] != UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
            $objUser->branch_id = null;
            $objUser->club_id = null;
        }

        try {
            $objUser->save();
            //LogServiceProvider::createSystemHistory(trans('history.fn_update_user'), $objUser);
            DB::commit();
            return $objUser;
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('update_user_fail', 'Error when update user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$arrUpdateData], true) . ')');
            return null;
        }
    }

    public static function delete ($id)
    {
        $objUser = self::getUserById($id);
        if (empty($objUser)) {
            return false;
        }

        try {
            $objUser->delete();
            //LogServiceProvider::createSystemHistory(trans('history.fn_delete_user'), $objUser);
            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollBack();
            LogsHelper::trackByFile('delete_user_fail', 'Error when delete user (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . $id);
            return false;
        }
    }

    public static function isNormalUser ()
    {
        return (self::_getRoleId() == UserConstants::NORMAL_USER || auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isAccountant()
    {
        return (auth()->user()->hasPermissions('is_accountant') && auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isAdmin ()
    {
        return (self::_getRoleId() == UserConstants::ADMIN_USER || auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isContentManagerSubGroup ()
    {
        return (self::_getRoleId() == UserConstants::CONTENT_MANAGER_SUB_GROUP_USER || auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isContentManagerGroup ()
    {
        return (self::_getRoleId() == UserConstants::CONTENT_MANAGER_GROUP_USER || auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isHnExpertUser ()
    {
        return (auth()->user()->hasPermissions('is_expert_user') && self::inArrayProvinceByArea(Controller::AREA_HN) && auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isDnExpertUser ()
    {
        return (auth()->user()->hasPermissions('is_expert_user') && self::inArrayProvinceByArea(Controller::AREA_DN) && auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isHcmExpertUser ()
    {
        return (auth()->user()->hasPermissions('is_expert_user') && self::inArrayProvinceByArea(Controller::AREA_HCM) && auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isHnLeaderUser ()
    {
        return (auth()->user()->hasPermissions('is_leader_user') && self::inArrayProvinceByArea(Controller::AREA_HN) && auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isDnLeaderUser ()
    {
        return (auth()->user()->hasPermissions('is_leader_user') && self::inArrayProvinceByArea(Controller::AREA_DN) && auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isHcmLeaderUser ()
    {
        return (auth()->user()->hasPermissions('is_leader_user') && self::inArrayProvinceByArea(Controller::AREA_HCM) && auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isLeaderManager ()
    {
        return (auth()->user()->hasPermissions('is_leader_user') && auth()->user()->hasPermissions(request()->route()->getName()));
    }

    public static function isMemberShipCardIssuer ()
    {
        return (auth()->user()->hasPermissions('is_member_ship_card_issuer') && (auth()->user()->hasPermissions(request()->route()->getName())));
    }

    public static function _getRoleId ()
    {
        if (!empty(auth()->user()->role)) {
            return auth()->user()->role;
        } else {
            return UserConstants::NORMAL_USER;
        }
    }

    public static function isLeaderRole ()
    {
        return (self::isHnLeaderUser() || self::isDnLeaderUser() || self::isHcmLeaderUser());
    }

    public static function isExpertRole ()
    {
        return (self::isHnExpertUser() || self::isDnExpertUser() || self::isHcmExpertUser());
    }

    public static function sendPasswordResettingInfoMail($userId, $password, $mailTemplate = 'password_resetting_info_mail')
    {
        $objUser  = User::where('id', $userId)->first();
        $objUser->resetPassword = $password;
        $email = $objUser->email;
        self::_sendPasswordResettingInfoMail($objUser, $email, $mailTemplate);
    }

    private static function _sendPasswordResettingInfoMail($objUser, $email, $mailTemplate)
    {
        if (!empty($email)) {
            try {
                // Send approve mail
                $to = [['address' => $email, 'name' => $objUser->fullName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = 'Thông báo về việc reset mật khẩu ';
                $data = ['objUser' => $objUser];
                $template = 'admin.user.mail.' . $mailTemplate;
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_password_resetting_info', 'Error when send mail password resetting (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objUser->id], true) . ')');
            }
        }
    }

    public static function getTypeOfPlace ()
    {
        $vpddCode = auth()->user()->province_type;
        $branches = new BranchesRepository(new Branches());

        if (empty($vpddCode)) {
            return $branches->getAll(['id']);
        }

        $arrTypeOfPlaceBelongToThisUser = $branches->getAllByParentId($vpddCode);

        return $arrTypeOfPlaceBelongToThisUser;
    }

    private static function inArrayProvinceByArea ($area = Controller::AREA_HN)
    {
        if (in_array(auth()->user()->province_type, Offices::getBranchesByArea($area, ['id']))) {
            return true;
        }

        return false;
    }
}
