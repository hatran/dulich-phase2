<?php

namespace App\Providers;

use App\Constants\MemberConstants;
use App\Libs\Helpers\LogsHelper;
use Illuminate\Support\ServiceProvider;
use Response;
use Validator;
use Carbon\Carbon;
use App\Libs\Helpers\Utils;
use App\Libs\Storage\StoreFile;
use App\Libs\Storage\StorePhoto;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Constants\BranchConstants;
use App\Constants\CommontConstants;
use App\Models\ForteTour;
use App\Models\Member;

class ForteTourServiceProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const TITLE = 'title';
    const OPTION_CODE = 'option_code';
    const ID = 'id';
    const VERIFIED_AT = 'verified_at';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const ACCEPT_TERMS_AND_POLICIES = 'acceptTermsAndPolicies';
    const PROVINCE_CODE = 'province_code';
    const PROVINCE_TYPE = 'province_type';
    const IS_VERIFIED = 'is_verified';
    const IS_DELETE = 'is_delete';
    const MEMBER_TYPE = 'member_type';
    const FILE_CODE = 'file_code';
    const TYPE_OF_PLACE = 'typeOfPlace';


    public static function getAllParentForteTour ()
    {
        $objForte = ForteTour::whereNull('parent_id')
            ->orderBy('created_at', 'desc')
            ->get();

        return $objForte;
    }

    public static function getType($id) 
    {
        $objForte = ForteTour::select('type')
                    ->where('id', $id)
                    ->first();
        return $objForte;
    }

    public static function newsSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];
      
        if (!empty($arrConditions['nameall'])) {
            $arrSearchCondition = [['name', 'like', '%' . $arrConditions['nameall'] . '%']];
        }

        if (!empty($arrConditions['id'])) {
            $arrSearchCondition = [['parent_id', '=', $arrConditions['id']]];
        }

        if (!empty($arrConditions['nameall']) && !empty($arrConditions['id'])) {
            $arrSearchCondition = [['parent_id', '=', $arrConditions['id']], ['name', 'like', '%' . $arrConditions['nameall'] . '%']];
        }
        $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', '1']]);
        if(!$isCount){
            $objNew = ForteTour::where($arrSearchCondition)->orderBy('created_at', 'des')->paginate($limit);
        }else{
            $objNew = ForteTour::where($arrSearchCondition)->get();
        }
        return $objNew;
    }

    public static function getDataById($id)
    {
        return ForteTour::where('id',$id)->where('status', 1)->first();
    }
    
    public static function getMemberForteTourById($id)
    {
        return Member::where('guideLanguage', 'LIKE', '%'.$id.'%')->first();
    }
}
