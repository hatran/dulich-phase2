<?php

namespace App\Providers;

use App\Models\TravelerCompanyRank;
use App\Models\User;

class TravelerCompanyRankProvider extends AppServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;

    public static function travelersSearchByConditions($arrConditions, $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE ,$isCount = false)
    {
        $arrSearchCondition = [];
        if (!empty($arrConditions['company_name'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['company_name', 'like', '%' . $arrConditions['company_name'] . '%']]);
        }

        if (!empty($arrConditions['tax_code'])) {
            $arrSearchCondition = array_merge($arrSearchCondition, [['tax_code', 'like', '%' . $arrConditions['tax_code'] . '%']]);
        }

        if (isset($arrConditions['status']) && $arrConditions['status'] != '') {
            $arrSearchCondition = array_merge($arrSearchCondition, [['status','=', $arrConditions['status']]]);
        }

        $objTraveler = TravelerCompanyRank::query()->where($arrSearchCondition)->whereNull('is_deleted');
        if (!$isCount) {
            return $objTraveler->orderBy('created_at', 'des')->paginate($limit);
        }
        return $objTraveler->get();
    }

    public static function getDataById($id)
    {
        return TravelerCompanyRank::where('id',$id)->whereNull('is_deleted')->first();
    }

    public static function getStatusZeroById($id)
    {
        return TravelerCompanyRank::where('id', $id)->where('status', 0)->whereNull('is_deleted')->first();
    }

    public static function checkCode($code)
    {
        $getCodeExist = TravelerCompanyRank::where('tax_code', $code)->whereNull('is_deleted')->first();
        return $getCodeExist;
    }

    public static function checkCodeInUserTable($taxCode) {
        $getUser = User::where('username', $taxCode)->first();
        return $getUser;
    }

    public static function checkEmailInUserTable($email) {
        $getEmail = User::where(['email' => $email, 'is_traveler_company' => 1])->first();
        return $getEmail;
    }

}
