<?php

namespace App\Repositories\News;

use App\Models\News;
use App\Models\Information;

/**
 * Class NewsRepository.
 *
 * @author laven9696
 */
class NewsRepository {

    const PERPAGE = 10;
    const NEWS_BRANCH = 'NEWS02';
    const NEWS_CLUB = 'NEWS03';
    const STATUS_PUBLIC = 1;
    const OPTION_KEY = 'infomation';

    /**
     * @var
     */
    public $perPage;

    /**
     * @var \News
     */
    protected $news;

    /**
     * @param News $news
     */
    public function __construct(News $news) {
        $this->news = $news;
        $this->perPage = self::PERPAGE;
    }

    public function getListNewsPage($optionCode)
    {
        $listThongtin = $this->news
            ->whereNotNull('start_time')
            ->where('start_time', '<=', date('Y-m-d 23:59:59'))
            ->where(function($query) {
                $query->where(function($query) {
                    $query->whereNull('end_time');
                })->orWhere(function($query) {
                    $query->where('end_time', '>=', date('Y-m-d 00:00:00'));
                });
            })
            ->where('option_code', $optionCode)
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->paginate($this->perPage);
        return $listThongtin;
    }

    public function getListNews2($optionCode, $branch_id)
    {
        $listThongtin = $this->news
            ->whereNotNull('start_time')
            ->where('start_time', '<=', date('Y-m-d 23:59:59'))
            ->where(function($query) {
                $query->where(function($query) {
                    $query->whereNull('end_time');
                })->orWhere(function($query) {
                    $query->where('end_time', '>=', date('Y-m-d 00:00:00'));
                });
            })
            ->where('option_code', $optionCode)
            ->where('branch_id', $branch_id)
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->paginate($this->perPage);
        return $listThongtin;
    }

    public function getListNews($optionCode) {
        $listThongtin = $this->news
            ->whereNotNull('start_time')
            ->where('start_time', '<=', date('Y-m-d 23:59:59'))
            ->where(function($query) {
                $query->where(function($query) {
                    $query->whereNull('end_time');
                })->orWhere(function($query) {
                    $query->where('end_time', '>=', date('Y-m-d 00:00:00'));
                });
            })->where('option_code', $optionCode)
                ->where('status', 1)
                ->orderBy('id', 'desc')
                ->paginate($this->perPage);
        return $listThongtin;
    }

    public function find($id) {
        return $this->news->where('status', self::STATUS_PUBLIC)->where('id', $id)->first();
    }

    public function findRelatedNews($id, $branch_id) {
        $listNews = $this->news->whereNotNull('start_time')
            ->where('start_time', '<=', date('Y-m-d 23:59:59'))
            ->where(function($query) {
                $query->where(function($query) {
                    $query->whereNull('end_time');
                })->orWhere(function($query) {
                    $query->where('end_time', '>=', date('Y-m-d 00:00:00'));
                });
            })
            ->where('id', '!=', $id)
            ->where('branch_id', $branch_id)
            ->where('status', 1)
            ->whereNull('deleted_at')
            ->orderBy('id', 'desc')
            ->get();
        return $listNews;
    }

    public function getLastestNews($optionCode, $currentId) {
        $list = $this->news
            ->whereNotNull('start_time')
            ->where('start_time', '<=', date('Y-m-d 23:59:59'))
            ->where(function($query) {
                $query->where(function($query) {
                    $query->whereNull('end_time');
                })->orWhere(function($query) {
                    $query->where('end_time', '>=', date('Y-m-d 00:00:00'));
                });
            })->where('option_code', $optionCode)
                ->where('status', 1)
                ->where('id', '<>', $currentId)
                ->orderBy('id', 'desc')
                ->limit($this->perPage)
                ->get();

        return $list;
    }

    public function paginateNewsByBranchId($optionCode, $branchId) {
        $listThongtin = $this->news
            ->whereNotNull('start_time')
            ->where('start_time', '<=', date('Y-m-d 23:59:59'))
            ->where(function($query) {
                $query->where(function($query) {
                    $query->whereNull('end_time');
                })->orWhere(function($query) {
                    $query->where('end_time', '>=', date('Y-m-d 00:00:00'));
                });
            })->where('branch_id', $branchId)
                ->where('status', 1)
                ->where('option_code', $optionCode)
                ->orderBy('id', 'desc')
                ->paginate($this->perPage);

        return $listThongtin;
    }

    public function getLastestNewsHomePages($optionCode = 'NEWS01', $perPage = 5) {
        $list = $this->news
            ->whereNotNull('start_time')
            ->where('start_time', '<=', date('Y-m-d 23:59:59'))
            ->where(function($query) {
                $query->where(function($query) {
                    $query->whereNull('end_time');
                })->orWhere(function($query) {
                    $query->where('end_time', '>=', date('Y-m-d 00:00:00'));
                });
            })->where('option_code', $optionCode)
                ->where('status', 1)
                ->where('show_home_page', 1)
                //->where('id', '<>', $currentId)
                ->orderBy('id', 'desc')
                ->limit($perPage)
                ->get();

        return $list;
    }

    public function getParInformation($id) {
        $parinfo = Information::where('id', $id)->where('status', 1)->first();
        return $parinfo;
    }

    public function getSubInformation($id) {
        $subinfo = Information::where('parent_id', $id)->where('status', 1)->first();
        return $subinfo;
    }

    public function getSubInformation1($id, $subid) {
        $subinfo = Information::where('parent_id', $id)->where('id', $subid)->where('status', 1)->first();
        return $subinfo;
    }

    public function getInformationByPluck() {
        return Information::whereNull('parent_id')->where('status', 1)->pluck('name', 'id')->all();
    }

    public function getSubInfor($id) {
        return Information::where('parent_id', $id)->where('status', 1)->get();
    }
}
