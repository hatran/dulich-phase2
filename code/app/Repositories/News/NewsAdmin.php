<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\News;

use App\Constants\UserConstants;
use App\Models\News;
use App\Models\Offices;
use App\Models\Options;
use Validator as V;
use App\Models\Branches;
use App\Exceptions\Validation\ValidationException;
use App\Repositories\Branches\BranchesRepository;
use App\Repositories\ClubOfHead\ClubOfHeadRepository;
use Carbon\Carbon;
use App\Models\Information;
use App\Models\Member;
use Illuminate\Support\Str;

/**
 * Description of NewsRepositoryAdmin
 *
 * @author DuyDuc
 */
class NewsAdmin extends News {

    const PERPAGE = 10;
    const OPTION_KEY = 'infomation';
    const OPTION_KEY_NEWS = 'news';
    const STATUS_PUBLIC = 1;
    const STATUS_DRAFT = 0;
    const STATUS_ON_HOMEPAGE = 1;
    const STATUS_OFF_HOMEPAGE = 0;
    const OPTIONS_HOI = 'NEWS01';
    const OPTIONS_BRANCHES = 'NEWS02';
    const OPTIONS_CLUB = 'NEWS03';
    
    public static $statusOption = [
        self::STATUS_PUBLIC => 'Hoạt động',
        self::STATUS_DRAFT => 'Lưu nháp'
    ];
    
    protected $fillable = [
        'title',
        'thumnail_image',
        'short_description',
        'content',
        'status',
        'start_time',
        'end_time',
        'option_code',
        'show_home_page',
        'branch_id',
        'created_author',
        'updated_author',
        'email_author',
        'file_name'
    ];
    protected $guarded = [
        'id'
    ];
    
    protected static $rules = [
        'title' => 'required|max:500',
        'status' => 'required|integer|in:0,1',
        'start_time' => 'required|nullable|date|after:yesterday',
        'short_description' => 'required',
        'end_time' => 'nullable|date|after:yesterday|after_or_equal:start_time',
        'content' => 'required',
        'thumnail_image' => 'required'
    ];

    protected static $messages = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'integer' => ':attribute phải là số',
        'max' => 'Độ dài tối đa của :attribute là :max kí tự',
        'after_or_equal' => ':attribute phải lớn hơn :date',
        'in' => 'Dữ liệu không nằm trong dữ liệu cho phép',
        'mimes' => ':attribute phải là một tập tin có định dạng: :values.',
        'after' => ':attribute phải lớn hơn hoặc bằng ngày hiện tại',
    ];

    protected static $niceAttributeName = [
        'option_code' => 'Loại thông tin',
        'option_code_parent' => 'Loại thông tin',
        'title' => 'Tiêu đề',
        'status' => 'Trạng thái',
        'start_time' => 'Phát hành từ ngày',
        'end_time' => 'Đến ngày',
        'content' => 'Nội dung đầy đủ',
        'thumnail_image' => 'Hình ảnh tóm tắt',
        'short_description' => 'Nội dung tóm tắt',
    ];

    /**
     * @var
     */
    public $perPage;

    /**
     * @var
     */
    public $infomationCode = null;
    
    /**
     * @var
     */
    public $branchesNewsCode = null;

    /**
     * @var \News
     */
    protected $news;

    /**
     * @param News $news
     */
    
    public $infomationCodeParent = null;
    public function __construct() {
        $this->infomationCode = $this->getOptionInfomation();
        $this->infomationCodeParent = $this->getOptionInfomationParent();
        $this->branchesNewsCode = $this->getOptionNews();
        $this->perPage = self::PERPAGE;

        parent::__construct();
    }

    public function pagination($params = false) {
        if (!empty($params['search_start_time'])) {
            $params['search_start_time'] = Carbon::createFromFormat('d/m/Y', $params['search_start_time'])->format('Y-m-d 00:00:00');
        }
        if (!empty($params['search_end_time'])) {
            $params['search_end_time'] = Carbon::createFromFormat('d/m/Y', $params['search_end_time'])->format('Y-m-d 23:59:59');
        }
        if (!empty($params['search_created_at'])) {
            $params['search_created_at'] = Carbon::createFromFormat('d/m/Y', $params['search_created_at'])->format('Y-m-d');
        }

        $query = $this->whereIn('option_code', array_keys($this->infomationCode));
        if (!empty($params['search_option_code'])) {
            $query->where('option_code', $params['search_option_code']);
        }

        if (isset($params['search_option_code_parent'])) {
            if (!empty($params['search_option_code_parent'] && empty($params['search_option_code']))) {
                $optionCode = $this->getOptionInfomationByParentID($params['search_option_code_parent']);
                $arrOptionCode = array();
                foreach ($optionCode as $key => $value) {
                    $arrOptionCode[] = $key;
                }
                $arrOptionCode[] = $params['search_option_code_parent'];
                $query->whereIn('option_code', $arrOptionCode);
            }
        }
    
        if (isset($params['search_title']) && $params['search_title'] != '') {
            $query->where('title', 'like', '%' . trim($params['search_title']) . '%');
        }
        if (isset($params['search_status']) && $params['search_status'] != '') {
            $query->where('status', 'like', '%' . trim($params['search_status']) . '%');
        }
        if (!empty($params['search_start_time']) && !empty($params['search_end_time'])) {
            $query->where('start_time', '>=', $params['search_start_time']);
            $query->where('end_time', '<=', $params['search_end_time']);
        } elseif (!empty($params['search_start_time']) && empty($params['search_end_time'])) {
            $query->where('start_time', '>=', $params['search_start_time']);
        }elseif (empty($params['search_start_time']) && !empty($params['search_end_time'])) {
            $query->where('end_time', '<=', $params['search_end_time']);
        }
        if (!empty($params['search_created_at'])) {
            $query->whereRaw('DATE(created_at) = ? ', [$params['search_created_at']]);
        }
        $query->orderBy('id', 'desc');
       
        $listThongtin = $query->paginate($this->perPage);

        return $listThongtin;
    }

    public function create($attributes, $branchNewsFlag = false) {
        $attributes['start_time'] = !empty($attributes['start_time']) ? date('Y-m-d', strtotime(str_replace('/', '-', $attributes['start_time']))) : null;
        $attributes['end_time'] = !empty($attributes['end_time']) ? date('Y-m-d', strtotime(str_replace('/', '-', $attributes['end_time']))) : null;

        $rules = self::$rules;
        $niceAtt = self::$niceAttributeName;
        // $rules['option_code'] = 'required|in:' . implode(',', array_keys($this->infomationCode));
        $rules['option_code_parent'] = 'required';
        if ($branchNewsFlag) {
            $rules['option_code'] = 'required|in:' . implode(',', array_keys($this->branchesNewsCode));
            $niceAtt['option_code'] = 'Chi hội - CLB';
        }
        if (request()->hasFile('thumnail_image')) {
            $rules['thumnail_image'] = 'nullable|mimes:jpeg,jpg,png,gif|required|max:10000';
        }
        $validate = $this->isValid($attributes, $rules, self::$messages, $niceAtt);

        if ($validate === true) {
            if (request()->hasFile('thumnail_image')) {
                $file = $attributes['thumnail_image'];
                $filePath = public_path('/images/news');
                $getExt = $file->getClientOriginalExtension();
                $newNameFile = Str::random(40).'_'.strtotime('now').'.'.$getExt;
                $file->move($filePath, $newNameFile);
                $attributes['thumnail_image'] = $newNameFile;
            }
            $attributes['show_home_page'] = isset($attributes['show_home_page']) && $attributes['show_home_page'] == 1 ? 1 : null;
            if (!is_null($attributes['end_time']) && is_null($attributes['start_time'])) {
                $attributes['start_time'] = date('Y-m-d H:i:s');
            }
            $attributes['created_at'] = date('Y-m-d H:i:s');
            $attributes['updated_at'] = date('Y-m-d H:i:s');

            if (isset($attributes['created_author']) && isset($attributes['email_author'])) {
                $existMember = Member::where('fullname', $attributes['created_author'])->where(function ($query) use ($attributes) {
                    $query->where('firstEmail', $attributes['email_author'])
                            ->orWhere('secondEmail', $attributes['email_author']);
                })->first();
                if (empty($existMember)) {
                    $message = ['created_author' => ['Người gửi hoặc email không tồn tại hoặc đã bị xóa']];
                    throw new ValidationException('News validation failed', $message);
                }
            }

            if (empty($attributes['option_code'])) {
                $attributes['option_code'] = $attributes['option_code_parent'];
            }

            if (request()->hasFile('pdfAll_add')) {
                $file = $attributes['pdfAll_add'];
                $filePath = public_path('/pdf/information');
                $getExt = $file->getClientOriginalExtension();
                $newNameFile = Str::random(40).'_'.strtotime('now').'.'.$getExt;
                $file->move($filePath, $newNameFile);
                $attributes['file_name'] = $newNameFile;
            }
           
            $this->fill($attributes)->save();
            return true;
        }

        throw new ValidationException('News validation failed', $validate);
    }
    
    public function updateData($id, $attributes, $branchNewsFlag = false) {
        $detailModel = $this->find($id);
        $attributes['start_time'] = !empty($attributes['start_time']) ? date('Y-m-d', strtotime(str_replace('/', '-', $attributes['start_time']))) : null;
        $attributes['end_time'] = !empty($attributes['end_time']) ? date('Y-m-d', strtotime(str_replace('/', '-', $attributes['end_time']))) : null;
        $rules = self::$rules;
        $niceAtt = self::$niceAttributeName;
        // $rules['option_code'] = 'required|in:' . implode(',', array_keys($this->infomationCode));
        $rules['option_code_parent'] = 'required';
        $rules['start_time'] = 'nullable|date';
        $rules['end_time'] = 'nullable|date|after_or_equal:start_time';
        if (request()->hasFile('thumnail_image')) {
            $rules['thumnail_image'] = 'nullable|mimes:jpeg,jpg,png,gif|required|max:10000';
        }
        if ($branchNewsFlag) {
            $rules['option_code'] = 'required|in:' . implode(',', array_keys($this->branchesNewsCode));
            $niceAtt['option_code'] = 'Chi hội - CLB';
        }
        $validate = $this->isValid($attributes, $rules, self::$messages, $niceAtt);

        if ($validate === true) {
            if (request()->hasFile('thumnail_image')) {
                // unset old image
                $oldLink = realpath(public_path('images/news/') . $detailModel->thumnail_image);
                //unlink($oldLink);
                $file = $attributes['thumnail_image'];
                $filePath = public_path('/images/news');
                $getExt = $file->getClientOriginalExtension();
                $newNameFile = Str::random(40).'_'.strtotime('now').'.'.$getExt;
                $file->move($filePath, $newNameFile);
                $attributes['thumnail_image'] = $newNameFile;
            }
            $attributes['show_home_page'] = isset($attributes['show_home_page']) && $attributes['show_home_page'] == 1 ? 1 : null;
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            if (empty($attributes['option_code'])) {
                $attributes['option_code'] = $attributes['option_code_parent'];
            }

            if (request()->hasFile('pdfAll_edit')) {
                $file = $attributes['pdfAll_edit'];
                $filePath = public_path('/pdf/information');
                $getExt = $file->getClientOriginalExtension();
                $newNameFile = Str::random(40).'_'.strtotime('now').'.'.$getExt;
                $file->move($filePath, $newNameFile);
                $attributes['file_name'] = $newNameFile;
            }
            $detailModel->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('News validation failed', $validate);
    }
    
    /**
     *  Create News
     */
    public function createNews($attributes) {

        if (!empty($attributes['branch_id']) && !empty($attributes['option_code'])) {
            $branchDetail = Branches::find($attributes['branch_id']);
            if (empty($branchDetail)) {
                $name = $attributes['option_code'] == self::OPTIONS_BRANCHES ? 'Chi Hội' : 'CLB thuộc Hội';
                $message = ['branch_id' => [$name . ' không tồn tại hoặc đã bị xóa']];
                throw new ValidationException('News validation failed', $message);
            }
        }
        
        $attributes['start_time'] = !empty($attributes['start_time']) ? date('Y-m-d', strtotime(str_replace('/', '-', $attributes['start_time']))) : null;
        $attributes['end_time'] = !empty($attributes['end_time']) ? date('Y-m-d', strtotime(str_replace('/', '-', $attributes['end_time']))) : null;

        $rules = self::$rules;
        $niceAtt = self::$niceAttributeName;
        $rules['option_code'] = 'required|in:' . implode(',', array_keys($this->branchesNewsCode));
        if (in_array($attributes['option_code'], [self::OPTIONS_BRANCHES, self::OPTIONS_CLUB])) {
            $rules['branch_id'] = 'required';
            $niceAtt['branch_id'] = $attributes['option_code'] == self::OPTIONS_BRANCHES ? 'Chi Hội' : 'CLB thuộc Hội';
        }

        if (request()->hasFile('thumnail_image')) {
            $rules['thumnail_image'] = 'nullable|mimes:jpeg,jpg,png,gif|required|max:10000';
        }
        $validate = $this->isValid($attributes, $rules, self::$messages, $niceAtt);

        if ($validate === true) {
            if (request()->hasFile('thumnail_image')) {
                $file = $attributes['thumnail_image'];
                $filePath = public_path('/images/news');
                $file->move($filePath, $file->getClientOriginalName());
                $attributes['thumnail_image'] = $file->getClientOriginalName();
            }
            $attributes['show_home_page'] = isset($attributes['show_home_page']) && $attributes['show_home_page'] == 1 ? 1 : null;
            if (!is_null($attributes['end_time']) && is_null($attributes['start_time'])) {
                $attributes['start_time'] = date('Y-m-d H:i:s');
            }

            $attributes['created_at'] = date('Y-m-d H:i:s');
            $attributes['created_author'] = auth()->user()->username;
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            if (!isset($attributes['branch_id'])) {
                $attributes['branch_id'] = auth()->user()->province_type;
            }
            
            $this->fill($attributes)->save();
            return true;
        }

        throw new ValidationException('News validation failed', $validate);
    }
    
    /**
     *  Update News
     */
    public function updateNews($id, $attributes) {
        if (!empty($attributes['branch_id']) && !empty($attributes['option_code'])) {
            $branchDetail = Branches::find($attributes['branch_id']);
            if (empty($branchDetail)) {
                $name = $attributes['option_code'] == self::OPTIONS_BRANCHES ? 'Chi Hội' : 'CLB thuộc Hội';
                $message = ['branch_id' => [$name . ' không tồn tại hoặc đã bị xóa']];
                throw new ValidationException('News validation failed', $message);
            }
        }
        
        $detailModel = $this->find($id);
        $attributes['start_time'] = !empty($attributes['start_time']) ? date('Y-m-d', strtotime(str_replace('/', '-', $attributes['start_time']))) : null;
        $attributes['end_time'] = !empty($attributes['end_time']) ? date('Y-m-d', strtotime(str_replace('/', '-', $attributes['end_time']))) : null;
        
        $rules = self::$rules;
        $niceAtt = self::$niceAttributeName;
        $rules['option_code'] = 'required|in:' . implode(',', array_keys($this->infomationCode));
        if (in_array($attributes['option_code'], [self::OPTIONS_BRANCHES, self::OPTIONS_CLUB])) {
            $rules['branch_id'] = 'required';
            $niceAtt['branch_id'] = $attributes['option_code'] == self::OPTIONS_BRANCHES ? 'Chi Hội' : 'CLB thuộc Hội';
        }
        if (request()->hasFile('thumnail_image')) {
            $rules['thumnail_image'] = 'nullable|mimes:jpeg,jpg,png,gif|required|max:10000';
        }
        $rules['option_code'] = 'required|in:' . implode(',', array_keys($this->branchesNewsCode));
        $validate = $this->isValid($attributes, $rules, self::$messages, $niceAtt);

        if ($validate === true) {
            if (request()->hasFile('thumnail_image')) {
                // unset old image
                $oldLink = realpath(public_path('images/news/') . $detailModel->thumnail_image);
                //unlink($oldLink);
                $file = $attributes['thumnail_image'];
                $filePath = public_path('/images/news');
                $file->move($filePath, $file->getClientOriginalName());
                $attributes['thumnail_image'] = $file->getClientOriginalName();
            }
            $attributes['show_home_page'] = isset($attributes['show_home_page']) && $attributes['show_home_page'] == 1 ? 1 : null;
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $attributes['updated_author'] = auth()->user()->username;
            if (!isset($attributes['branch_id'])) {
                $attributes['branch_id'] = auth()->user()->province_type;
            }
            $detailModel->fill($attributes)->save();
            return true;
        }

        throw new ValidationException('News validation failed', $validate);
    }

    /**
     * Tin tức Hội
     *
     * @return Response
     */
    public function paginationNewsHoi($params = false) {
        $user = auth()->user();
        $userBranchID = $user->province_type;

        if (!empty($params['search_start_time'])) {
            $params['search_start_time'] = Carbon::createFromFormat('d/m/Y', $params['search_start_time'])->format('Y-m-d 00:00:00');
        }
        if (!empty($params['search_end_time'])) {
            $params['search_end_time'] = Carbon::createFromFormat('d/m/Y', $params['search_end_time'])->format('Y-m-d 23:59:59');
        }
        if (!empty($params['search_created_at'])) {
            $params['search_created_at'] = Carbon::createFromFormat('d/m/Y', $params['search_created_at'])->format('Y-m-d');
        }
        $query = $this->where('option_code', self::OPTIONS_HOI);
        if (!empty($params['search_option_code'])) {
            $query->where('option_code', $params['search_option_code']);
        }
        if (isset($params['search_title']) && $params['search_title'] != '') {
            $query->where('title', 'like', '%' . trim($params['search_title']) . '%');
        }
        if (isset($params['search_status']) && $params['search_status'] != '') {
            $query->where('status', 'like', '%' . trim($params['search_status']) . '%');
        }
        if (!empty($params['search_start_time']) && !empty($params['search_end_time'])) {
            $query->where('start_time', '>=', $params['search_start_time']);
            $query->where('end_time', '<=', $params['search_end_time']);
        } elseif (!empty($params['search_start_time']) && empty($params['search_end_time'])) {
            $query->where('start_time', '>=', $params['search_start_time']);
        }elseif (empty($params['search_start_time']) && !empty($params['search_end_time'])) {
            $query->where('end_time', '<=', $params['search_end_time']);
        }
        if (!empty($params['search_created_at'])) {
            $query->whereRaw('DATE(created_at) = ? ', [$params['search_created_at']]);
        }

        if ($userBranchID != null) {
            $getBranches = Branches::select('parent_id')->where('id' , $userBranchID)->where('status', 1)->first();
            if ($getBranches->parent_id == null) {
                $getBranchesParentID = Branches::where('parent_id', $userBranchID)->where('status', 1)->get();
                $arr = array();
                foreach ($getBranchesParentID as $branchParentID) {
                    $arr[] = $branchParentID->id;
                }
                $arr[] = $userBranchID;
                $query->whereIn('branch_id', $arr);
            }
            else {
                $query->where('branch_id', $userBranchID);
            }
        }
        $query->orderBy('id', 'desc');
        $listThongtin = $query->paginate($this->perPage);
        return $listThongtin;
    }
    
    /**
     * Tin tức Chi hội
     *
     * @return Response
     */
    public function paginationNewBranches($params = false) {
        $query = $this->where('option_code', self::OPTIONS_BRANCHES);
        // Kiểm tra quyền biên tập viên
        $auth = auth()->user();
        if ($auth->role == UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
            if (empty($auth->branch_id)) {
                return array();
            }
            $query->whereIn('branch_id', explode(',', $auth->branch_id));
        }
        if (!empty($params['search_start_time'])) {
            $params['search_start_time'] = Carbon::createFromFormat('d/m/Y', $params['search_start_time'])->format('Y-m-d 00:00:00');
        }
        if (!empty($params['search_end_time'])) {
            $params['search_end_time'] = Carbon::createFromFormat('d/m/Y', $params['search_end_time'])->format('Y-m-d 23:59:59');
        }
        if (!empty($params['search_created_at'])) {
            $params['search_created_at'] = Carbon::createFromFormat('d/m/Y', $params['search_created_at'])->format('Y-m-d');
        }
        if (!empty($params['search_branch_id'])) {
            $query->where('branch_id', $params['search_branch_id']);
        }
        if (!empty($params['search_option_code'])) {
            $query->where('option_code', $params['search_option_code']);
        }
        if (isset($params['search_title']) && $params['search_title'] != '') {
            $query->where('title', 'like', '%' . trim($params['search_title']) . '%');
        }
        if (isset($params['search_status']) && $params['search_status'] != '') {
            $query->where('status', 'like', '%' . trim($params['search_status']) . '%');
        }
        if (!empty($params['search_start_time']) && !empty($params['search_end_time'])) {
            $query->where('start_time', '>=', $params['search_start_time']);
            $query->where('end_time', '<=', $params['search_end_time']);
        } elseif (!empty($params['search_start_time']) && empty($params['search_end_time'])) {
            $query->where('start_time', '>=', $params['search_start_time']);
        }elseif (empty($params['search_start_time']) && !empty($params['search_end_time'])) {
            $query->where('end_time', '<=', $params['search_end_time']);
        }
        if (!empty($params['search_created_at'])) {
            $query->whereRaw('DATE(created_at) = ? ', [$params['search_created_at']]);
        }

        if (auth()->check()) {
            $user = auth()->user();
            $branchIds = $user->branch_id ? [$user->branch_id] : [];
            if (empty($branchIds)) {
                $provinceType = $user->province_type ?? null;
                if ($provinceType) {
                    $getProvinceCode = Offices::getProvinceCodeByProvinceType($provinceType);
                    foreach ($getProvinceCode as $provinceCode) {
                        $branchIds[] = $provinceCode['id'];
                    }
                }
            }

            if (count($branchIds)) {
                $query->whereIn('branch_id', $branchIds);
            }
        }

        $query->orderBy('id', 'desc');

        return $query->paginate($this->perPage);
    }
    
    /**
     * Tin tức CLB
     *
     * @return Response
     */
    public function paginationNewClub ($params = false) {
        $query = $this->where('option_code', self::OPTIONS_CLUB);
        $auth = auth()->user();
        if ($auth->role == UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
            if (empty($auth->club_id)) {
                return array();
            }
            $query->whereIn('branch_id', explode(',', $auth->club_id));
        }
        if (!empty($params['search_start_time'])) {
            $params['search_start_time'] = Carbon::createFromFormat('d/m/Y', $params['search_start_time'])->format('Y-m-d 00:00:00');
        }
        if (!empty($params['search_end_time'])) {
            $params['search_end_time'] = Carbon::createFromFormat('d/m/Y', $params['search_end_time'])->format('Y-m-d 23:59:59');
        }
        if (!empty($params['search_created_at'])) {
            $params['search_created_at'] = Carbon::createFromFormat('d/m/Y', $params['search_created_at'])->format('Y-m-d');
        }

        if (!empty($params['search_option_code'])) {
            $query->where('option_code', $params['search_option_code']);
        }

        if (!empty($params['search_branch_id'])) {
            $query->where('branch_id', $params['search_branch_id']);
        }
        if (isset($params['search_title']) && $params['search_title'] != '') {
            $query->where('title', 'like', '%' . trim($params['search_title']) . '%');
        }
        if (isset($params['search_status']) && $params['search_status'] != '') {
            $query->where('status', 'like', '%' . trim($params['search_status']) . '%');
        }
        if (!empty($params['search_start_time']) && !empty($params['search_end_time'])) {
            $query->where('start_time', '>=', $params['search_start_time']);
            $query->where('end_time', '<=', $params['search_end_time']);
        } elseif (!empty($params['search_start_time']) && empty($params['search_end_time'])) {
            $query->where('start_time', '>=', $params['search_start_time']);
        }elseif (empty($params['search_start_time']) && !empty($params['search_end_time'])) {
            $query->where('end_time', '<=', $params['search_end_time']);
        }
        if (!empty($params['search_created_at'])) {
            $query->whereRaw('DATE(created_at) = ? ', [$params['search_created_at']]);
        }
        $query->orderBy('id', 'desc');

        $listThongtin = $query->paginate($this->perPage);

        return $listThongtin;
    }
    
    public function getOptionInfomation() {
        // return Options::where('key', self::OPTION_KEY)->pluck('value', 'code')->all();
        return Information::where('status', 1)->pluck('name', 'id')->all();
    }

    public function getOptionInfomationParent() {
        // return Options::where('key', self::OPTION_KEY)->pluck('value', 'code')->all();
        return Information::whereNull('parent_id')->where('status', 1)->pluck('name', 'id')->all();
    }

    public function getOptionInfomationByParentID($id) {
        // return Options::where('key', self::OPTION_KEY)->pluck('value', 'code')->all();
        return Information::where('parent_id', $id)->where('status', 1)->pluck('name', 'id')->all();
    }

     public function getOptionInfomationParentByID($id) {
        return Information::where('id', $id)->whereNotNull('parent_id')->where('status', 1)->first();
    }
    
    public function getOptionNews() {
        return Options::where('key', self::OPTION_KEY_NEWS)->pluck('value', 'code')->all();
    }

    public function getStarttimeAttribute($value) {
        $date = date('Y-m-d', strtotime($value));
        $tempDate = explode('-', $date);
        if (checkdate($tempDate[1], $tempDate[2], $tempDate[0]) && $date != '1970-01-01') {
            return date('d/m/Y', strtotime($value));
        }

        return $value;
    }

    public function getEndtimeAttribute($value) {
        $date = date('Y-m-d', strtotime($value));
        $tempDate = explode('-', $date);
        if (checkdate($tempDate[1], $tempDate[2], $tempDate[0]) && $date != '1970-01-01') {
            return date('d/m/Y', strtotime($value));
        }

        return $value;
    }

    public function mappingOptionCode($optionCode) {
        return !empty($this->infomationCode[$optionCode]) ? $this->infomationCode[$optionCode] : '';
    }

    public static function mappingStatus($status) {
        return isset(self::$statusOption[$status]) ? self::$statusOption[$status] : self::$statusOption[0];
    }
    
    public function getListBranches() {
        $query = \App\Models\Branches::where('option_code', BranchesRepository::BRANCHESCODE);
        $auth = auth()->user();
        if ($auth->role == UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
            if (empty($auth->branch_id)) {
                return array();
            }
            $query = $query->whereIn('id', explode(',', $auth->branch_id));
        }

        return $query->pluck('name', 'id')->all();
    }
    
    public function getListClub() {
        $query = \App\Models\Branches::where('option_code', ClubOfHeadRepository::BRANCHES_CLUBOFHEAD);
        $auth = auth()->user();
        if ($auth->role == UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
            if (empty($auth->club_id)) {
                return array();
            }
            $query->whereIn('id', explode(',', $auth->club_id));
        }

        return $query->pluck('name', 'id')->all();
    }

    public function isValid(array $attributes, array $rules = null, array $messages = null, array $niceAttributeName = null) {
        if (empty($messages)) {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules);
        } else {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        }
        $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        if (!empty($niceAttributeName)) {
            $v->setAttributeNames($niceAttributeName);
        }
        if ($v->fails()) {
            return $v->messages();
        }

        return true;
    }

}
