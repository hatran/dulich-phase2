<?php

namespace App\Repositories\ClubOfHead;

use Config;
use App\Models\Branches;
use App\Repositories\RepositoryAbstract;
use App\Repositories\CrudableInterface;
use App\Exceptions\Validation\ValidationException;
use App\Constants\MemberConstants;
use App\Models\Member;
use App\Libs\Helpers\Utils;
use App\Constants\BranchConstants;
use App\Models\EmployeesPosition;
use App\Models\Employees;
use App\Models\Options;
use App\Models\MemberPosition;
use App\Constants\UserConstants;
use DB;

/**
 * Class BranchesRepository.
 *
 * @author laven9696
 */
class ClubOfHeadRepository extends RepositoryAbstract implements ClubOfHeadInterface, CrudableInterface {

    const INVALID_ATTR = ':attribute không hợp lệ';
    const BRANCHES_CLUB = 'club';
    const BRANCHES_CLUBOFHEAD = 'BRANCHES03';

    /**
     * @array
     */
    protected $crudTableAction = ['save', 'update'];
    protected $detailBranch = null;
    protected $employees = null;
    protected $options = null;

    /**
     * @string
     */
    protected $crudTableDelAction = 'del';

    /**
     * @var
     */
    public $perPage;

    /**
     * @var \Branches
     */
    protected $branches;

    /**
     * Rules.
     *
     * @var array
     */
    protected static $rules = [
        'code' => 'required',
        'address' => 'max:350',
        'phone' => 'nullable|numeric|digits_between:1,30',
        'email' => 'nullable|email|max:100',
        'note' => 'max:500',
    ];
    protected static $messages = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'alpha_num' => ':attribute chỉ được gồm chữ và số',
        'integer' => ':attribute phải là số',
        'max' => 'Độ dài tối đa của :attribute là :max kí tự',
        'min' => 'Độ dài tối thiểu của :attribute là :min kí tự',
        'unique' => ':attribute đã được sử dụng',
        'email' => self::INVALID_ATTR,
        'in' => self::INVALID_ATTR,
    ];
    protected static $niceAttributeName = [
        'code' => 'CLB',
        'address' => 'Địa chỉ',
        'phone' => 'Số điện thoại',
        'email' => 'Email',
        'note' => 'Ghi chú',
    ];

    /**
     * @param Branches $branches
     */
    public function __construct(Branches $branches) {
        $this->branches = $branches;
        $this->perPage = $this->branches->perPage;
    }

    /**
     * @return mixed
     */
    public function all() {
        return $this->branches->get();
    }

    /**
     * @param int  $page
     * @param int  $limit
     * @param bool $all
     *
     * @return mixed|\StdClass
     */
    public function paginate($page = 1, $limit = 15, $params = false) {
        $result = new \StdClass();
        $result->page = $page;
        $result->limit = $limit;
        $result->totalItems = 0;
        $result->items = array();

        $query = $this->branches->select('branches.*', 'options.status as abc')->join('options', 'branches.option_code', '=', 'options.code')
                ->orderBy('branches.id', 'desc')
                ->where('branches.option_code', self::BRANCHES_CLUBOFHEAD)
                ->where('options.status', 1);
       
        $auth = auth()->user();
        if ($auth->role == UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
            if (empty($auth->club_id)) {
                return null;
            }
            $query = $query->whereIn('id', explode(',', $auth->club_id));
        }
        // search
        if (isset($params['search_name']) && $params['search_name'] != '') {
            $query->where('branches.name', 'like', '%' . trim($params['search_name']) . '%');
        }
        if (isset($params['search_address']) && $params['search_address'] != '') {
            $query->where('branches.address', 'like', '%' . trim($params['search_address']) . '%');
        }
        if (!empty($params['search_option_code'])) {
            $query->where('branches.code', trim($params['search_option_code']));
        }
        if (isset($params['search_phone']) && $params['search_phone'] != '') {
            $query->where('branches.phone', 'like', '%' . trim($params['search_phone']) . '%');
        }
        if (isset($params['search_status']) && $params['search_status'] != '') {
            $query->where('branches.status', trim($params['search_status']));
        }
        if (isset($params['search_email']) && $params['search_email'] != '') {
            $query->where('email', 'like', '%' . trim($params['search_email']) . '%');
        }

        $count = $query->count();
        $lists = $query->skip($limit * ($page - 1))->take($limit)->get();

        $result->totalItems = $count;
        $result->items = $lists->all();

        return $result;
    }
    
    public function getListClubOfHead() {
        return $this->branches->select('branches.*')
                ->orderBy('branches.id', 'desc')
                ->where('branches.option_code', self::BRANCHES_CLUBOFHEAD)
                ->pluck('name', 'id')
                ->all();
    }

    /**
     * @return mixed
     */
    public function lists() {
        
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id) {
        return $this->branches->with(['MemberPosition'])->findOrFail($id);
    }

    /**
     * @param $attributes
     *
     * @return bool|mixed
     *
     * @throws \App\Exceptions\Validation\ValidationException
     */
    public function create($attributes) {
        if ($this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName)) {
            $detailOption = Options::getCodeByCondititon(['code' => $attributes['code']]);
            if (empty($detailOption)) {
                throw new ValidationException('CLB validation failed', ['reload' => true, 'option_code' => ['CLB không tồn tại hoặc đã xóa']]);
            }
            $attributes['name'] = $detailOption->value;
            $attributes['created_at'] = date('Y-m-d H:i:s');
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $attributes['code'] = $detailOption->code;
            $attributes['option_code'] = self::BRANCHES_CLUBOFHEAD;
            $this->branches->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('Branches validation failed', $this->getErrors());
    }

    /**
     * @param $id
     * @param $attributes
     *
     * @return bool|mixed
     *
     * @throws \App\Exceptions\Validation\ValidationException
     */
    public function update($id, $attributes) {
        $this->branches = $this->find($id);
        if (empty($this->branches)) {
            throw new ValidationException('CLB validation failed', ['reload' => true, 'nullBranch' => ['CLB không tồn tại hoặc đã xóa']]);
        }
        if ($this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName)) {
            if (isset($attributes['status']) && $attributes['status'] == 0) {
                $validateData = $this->validateDeleteOrDisableBranch($id, true);
                if ($validateData !== true) {
                    throw new ValidationException('Branches validation failed', ['updated' => false, 'reload' => false, 'errorsUpdateDisable' => $validateData]);
                }
            }
            $detailOption = Options::getCodeByCondititon(['code' => $attributes['code']]);
            if (empty($detailOption)) {
                throw new ValidationException('CLB validation failed', ['reload' => true, 'option_code' => ['CLB không tồn tại hoặc đã xóa']]);
            }
            $attributes['name'] = $detailOption->value;
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $this->branches->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('CLB thuộc Hội validation failed', $this->getErrors());
    }

    /**
     * @param $id
     *
     * @return mixed|void
     */
    public function delete($id) {
        if ($this->validateDeleteOrDisableBranch($id) !== true) {
            return $this->validateDeleteOrDisableBranch($id);
        }
        $branch = $this->branches->find($id);
        if (!empty($branch) && $branch->delete()) {
            return ['deleted' => true, 'messages' => 'Xóa CLB thành công'];
        }

        return ['deleted' => false, 'messages' => 'CLB không tồn tại hoặc đã xóa'];
    }

    public function validateDeleteOrDisableBranch($id, $update = false) {
        $countMember = $this->getMemberIdMemberPositionByBranchId($id);

        // Có hội viên không được xóa
        if (!empty($countMember)) {
            if ($update) {
                return 'CLB có hội viên không thể chuyển sang Không hoạt động';
            }

            return ['deleted' => false, 'messages' => 'CLB có hội viên không xóa được'];
        }
        $countManager = $this->getEmployeesPositionByBranchId($id);
        if ($countManager->count()) {
            if ($update) {
                return 'CLB có lãnh đạo không thể chuyển sang Không hoạt động';
            }
            
            return ['deleted' => false, 'messages' => 'CLB có lãnh đạo không xóa được'];
        }
        
        return true;
    }

    public function getMemberIdMemberPositionByBranchId($branchId) {
        return MemberPosition::where('branch_id', $branchId)
                        ->pluck('member_id')->all();
    }

    public function getEmployeesPositionByBranchId($id) {
        return EmployeesPosition::where('branch_id', $id)->with(['employees'])->orderBy('created_at', 'asc')->get();
    }

    public function saveEmployeesPosition($attributes) {
        // case Create/Update
        if (in_array($attributes['action'], $this->crudTableAction)) {
            if ($this->validateMemberPosition($attributes) !== true) {
                return $this->validateMemberPosition($attributes);
            }

            if ($attributes['action'] == 'save') {
                $employees = new Employees();
                $employees->fullname = $attributes['l_fullname'];
                if (isset($attributes['l_birthday'])) {
                    $employees->birthday = $attributes['l_birthday'];
                }
                if (isset($attributes['l_phone'])) {
                    $employees->phone = $attributes['l_phone'];
                }
                if (isset($attributes['l_email'])) {
                    $employees->email = $attributes['l_email'];
                }
                $employees->option_code = $attributes['l_position'];
                $employees->created_at = date('Y-m-d H:i:s');
                $employees->updated_at = date('Y-m-d H:i:s');
                if ($employees->save()) {
                    $employeesPosition = new EmployeesPosition();
                    $employeesPosition->branch_id = $attributes['branchesId'];
                    $employeesPosition->employee_id = $employees->id;
                    $employeesPosition->option_code = $attributes['l_position'];
                    $employeesPosition->created_at = date('Y-m-d H:i:s');
                    $employeesPosition->updated_at = date('Y-m-d H:i:s');
                    $employeesPosition->save();
                }
            } elseif ($attributes['action'] == 'update') {
                $employeesPosition = EmployeesPosition::find($attributes['rid']);
                if (empty($employeesPosition)) {
                    return ['error' => $attributes['l_fullname'] . ' không tồn tại hoặc bị xóa'];
                }
                $employeesPosition->option_code = $attributes['l_position'];
                $employeesPosition->updated_at = date('Y-m-d H:i:s');
                $employeesPosition->save();

                $employees = Employees::find($employeesPosition->employee_id);
                if (empty($employees)) {
                    return ['error' => $attributes['l_fullname'] . ' không tồn tại hoặc bị xóa'];
                }
                $arrayUpdated = [
                    'fullname' => $attributes['l_fullname'],
                    'option_code' => $attributes['l_position'],
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $arrayUpdated['birthday'] = isset($attributes['l_birthday']) ? $attributes['l_birthday'] : null;
                $arrayUpdated['phone'] = isset($attributes['l_phone']) ? $attributes['l_phone'] : null;
                $arrayUpdated['email'] = isset($attributes['l_email']) ? $attributes['l_email'] : null;
                $employees = Employees::updateOrCreate([
                            'id' => $employeesPosition->employee_id
                                ], $arrayUpdated);
            }
            $responses['success'] = 1;
            $responses['l_fullname'] = htmlentities($employees->fullname);
            $responses['l_email'] = !empty($employees->email) ? $employees->email : '';
            $responses['l_phone'] = !empty($employees->phone) ? $employees->phone : '';
            $responses['l_birthday'] = !empty($employees->birthday) ? $employees->birthday : '';
            $responses['l_position'] = $this->options->value;
            $responses['id'] = $employeesPosition->id;

            return $responses;
        } elseif ($attributes['action'] == $this->crudTableDelAction) {
            $employeesPosition = EmployeesPosition::where('id', $attributes['rid'])->with(['employees'])->first();
            // check case delete
            if (empty($employeesPosition)) {
                return ['error' => 'Lỗi dữ liệu!!!!'];
            }
            if ($employeesPosition->employees !== "") {
                $employeesPosition->employees->delete();
                if ($employeesPosition->delete()) {
                    $responses['success'] = 1;
                    $responses['id'] = $employeesPosition->id;

                    return $responses;
                }
            }
        }
        // check case delete

        return null;
    }

    public function validateMemberPosition($attributes) {
        if (!isset($attributes['branchesId'])) {
            return ['error' => 'Lỗi dữ liệu'];
        }

        if (!empty($attributes['branchesId'])) {
            $this->detailBranch = $this->branches->find($attributes['branchesId']);
            if (empty($this->detailBranch)) {
                return ['error' => 'Chi hội không tồn tại hoặc đã xóa'];
            }
        }

        if (!empty($attributes['l_position'])) {
            $this->options = Options::where('code', $attributes['l_position'])->where('key', 'position')->first();
            if (empty($this->options)) {
                return ['error' => 'Chức vụ không tồn tại hoặc đã xóa'];
            }
        }

        return true;
    }

    public function getOptionsByKey($key) {
        if (empty($key)) {
            return [];
        }

        return Options::where('key', $key)->select('value', 'code')->pluck('value', 'code')->all();
    }

    public function getOptionsClub($unuse = false, $search = false) {

        $query = Options::select('value', 'code')->where('options.key', self::BRANCHES_CLUB)->where('options.status', 1);
        if ($unuse) {
            $query->whereRaw('options.code not in(SELECT code FROM branches WHERE deleted_at is null and option_code="' . self::BRANCHES_CLUBOFHEAD . '")');
        }

        if (!empty($search)) {
            $query->where('options.value', 'like', '%' . $search . '%');
        }

        return $query->orderBy('options.value', 'ASC')->pluck('value', 'code')->all();
    }

    public function getMemberByCondition($condition) {
        return Member::where($condition)->get();
    }

    // ========================= FRONTEND ===========================

    public function getListClub() {
        return $this->branches
                        ->select('name', 'id')
                        ->orderBy('name', 'asc')
                        ->where('option_code', self::BRANCHES_CLUBOFHEAD)
                        ->where('status', '<>', 0)
                        ->pluck('name', 'id')->all();
    }

    public function paginateManager($id, $limit = 12) {
        $result = EmployeesPosition::where('branch_id', $id)
                ->with(['employees'])->orderBy('id', 'desc')
                ->paginate($limit);

        return $result;
    }

    public function paginateMember($id, $limit = 12, $params = false) {
        $clause = [
                ['status', MemberConstants::MEMBER_OFFICIAL_MEMBER],
                ['is_delete', null],
                ['typeOfPlace', \App\Constants\BranchConstants::TYPE_OF_PLACE_CLB_THUOC_HOI],
                ['province_code', $id]
        ];

        $query = Member::where($clause);
        // search
        if (isset($params['search_member_name']) && $params['search_member_name'] != '') {
            $query->where('fullName', 'like', '%' . trim($params['search_member_name']) . '%');
        }
        if (isset($params['search_touristGuideCode']) && $params['search_touristGuideCode'] != '') {
            $query->where('touristGuideCode', 'like', '%' . trim($params['search_touristGuideCode']) . '%');
        }
        if (isset($params['search_member_code']) && $params['search_member_code'] != '') {
            $query->where('member_code', 'like', '%' . trim($params['search_member_code']) . '%');
        }
        if (isset($params['search_guideLanguage']) && $params['search_guideLanguage'] != '') {
            $query->where('guideLanguage', 'like', '%' . trim($params['search_guideLanguage'] . '%'));
        }

        return $query->paginate($limit);
    }

}
