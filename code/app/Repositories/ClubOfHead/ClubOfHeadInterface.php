<?php

namespace App\Repositories\ClubOfHead;

use App\Repositories\RepositoryInterface;

/**
 * Interface BranchesInterface.
 *
 * @author laven9696
 */
interface ClubOfHeadInterface extends RepositoryInterface {
    
}
