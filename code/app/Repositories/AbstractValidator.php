<?php

namespace App\Repositories;

use Validator as V;

/**
 * Class AbstractValidator.
 *
 * @author laven9696
 */
abstract class AbstractValidator {

    /**
     * @var
     */
    protected $errors;

    /**
     * Check valid attributes.
     *
     * @param array $attributes
     * @param array $rules
     *
     * @return bool
     */
    public function isValid(array $attributes, array $rules = null, array $messages = null, array $niceAttributeName = null) {
        if (empty($messages)) {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules);
        } else {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        }
        $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        if (!empty($niceAttributeName)) {
            $v->setAttributeNames($niceAttributeName);
        }
        if ($v->fails()) {
            $this->setErrors($v->messages());

            return false;
        }

        return true;
    }

    /**
     * Get validation error messages.
     *
     * @return mixed
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * Set validation error messages.
     *
     * @param $error
     */
    public function setErrors($error) {
        $this->errors = $error;
    }

}
