<?php

namespace App\Repositories\ClubOfBranch;

use App\Repositories\RepositoryInterface;

/**
 * Interface BranchesInterface.
 *
 * @author laven9696
 */
interface ClubOfBranchInterface extends RepositoryInterface {
    
}
