<?php

namespace App\Repositories\ClubOfBranch;

use Config;
use App\Models\Branches;
use App\Repositories\RepositoryAbstract;
use App\Repositories\CrudableInterface;
use App\Exceptions\Validation\ValidationException;
use App\Constants\MemberConstants;
use App\Models\Member;
use App\Constants\BranchConstants;
use App\Models\EmployeesPosition;
use App\Models\Employees;
use App\Libs\Helpers\Utils;
use App\Models\Options;
use App\Models\MemberPosition;
use DB;

/**
 * Class BranchesRepository.
 *
 * @author laven9696
 */
class ClubOfBranchRepository extends RepositoryAbstract implements ClubOfBranchInterface, CrudableInterface {

    const INVALID_ATTR = ':attribute không hợp lệ';
    const BRANCHESCODE = 'BRANCHES02';
    // code chi hội
    const BRANCHESCODECH = 'BRANCHES01';
    const BRANCHES_CLUB = 'club';

    public $memberLimit = 10;

    /**
     * @array
     */
    protected $crudTableAction = ['save', 'update'];
    protected $detailBranch = null;
    protected $employees = null;
    protected $options = null;
    public $branchCode = null;

    /**
     * @string
     */
    protected $crudTableDelAction = 'del';

    /**
     * @var
     */
    public $perPage;

    /**
     * @var \Branches
     */
    protected $branches;

    /**
     * Rules.
     *
     * @var array
     */
    protected static $rules = [
        'code' => 'required',
        'parent_id' => 'required|integer',
        'address' => 'max:350',
        'phone' => 'nullable|numeric|digits_between:1,30',
        'email' => 'nullable|email|max:100',
        'note' => 'max:500',
    ];

    /**
     * Rules message.
     *
     * @var array
     */
    protected static $messages = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'alpha_num' => ':attribute chỉ được gồm chữ và số',
        'integer' => ':attribute phải là số',
        'max' => 'Độ dài tối đa của :attribute là :max kí tự',
        'min' => 'Độ dài tối thiểu của :attribute là :min kí tự',
        'unique' => ':attribute đã được sử dụng',
        'email' => self::INVALID_ATTR,
        'in' => self::INVALID_ATTR,
    ];

    /**
     * AttributeName.
     *
     * @var array
     */
    protected static $niceAttributeName = [
        'code' => 'Tên CLB thuộc Chi Hội',
        'parent_id' => 'Chi Hội',
        'address' => 'Địa chỉ',
        'phone' => 'Số điện thoại',
        'email' => 'Email',
        'note' => 'Ghi chú',
    ];

    /**
     * @param Branches $branches
     */
    public function __construct(Branches $branches) {
        $this->branches = $branches;
        $this->perPage = $this->branches->perPage;
        $this->branchCode = self::BRANCHESCODE;
    }

    /**
     * @return mixed
     */
    public function all() {
        return $this->branches->get();
    }

    /**
     * @param int  $page
     * @param int  $limit
     * @param bool $all
     *
     * @return mixed|\StdClass
     */
    public function paginate($page = 1, $limit = 15, $params = false) {
        $result = new \StdClass();
        $result->page = $page;
        $result->limit = $limit;
        $result->totalItems = 0;
        $result->items = array();

        $query = $this->branches->orderBy('id', 'desc');
        if ($this->branchCode == null) {
            return null;
        }

        $query->where('option_code', $this->branchCode);
        // search
        if (isset($params['search_name']) && $params['search_name'] != '') {
            $query->where('name', 'like', '%' . trim($params['search_name']) . '%');
        }
        if (isset($params['search_address']) && $params['search_address'] != '') {
            $query->where('address', 'like', '%' . trim($params['search_address']) . '%');
        }
        if (isset($params['search_parent_id']) && $params['search_parent_id'] != '') {
            $query->where('parent_id', trim($params['search_parent_id']));
        }
        if (isset($params['search_phone']) && $params['search_phone'] != '') {
            $query->where('phone', 'like', '%' . trim($params['search_phone']) . '%');
        }
        if (isset($params['search_status']) && $params['search_status'] != '') {
            $query->where('status', trim($params['search_status']));
        }
        if (isset($params['search_email']) && $params['search_email'] != '') {
            $query->where('email', 'like', '%' . trim($params['search_email']) . '%');
        }
        
        $count = $query->count();
        $lists = $query->skip($limit * ($page - 1))->take($limit)->get();

        $result->totalItems = $count;
        $result->items = $lists->all();

        return $result;
    }

    /**
     * @return mixed
     */
    public function lists() {
        
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id) {
        return $this->branches->with(['MemberPosition'])->find($id);
    }

    /**
     * @param $attributes
     *
     * @return bool|mixed
     *
     * @throws \App\Exceptions\Validation\ValidationException
     */
    public function create($attributes) {
        if ($this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName)) {
            // check chi hoi da xoa hay chua
            $branchDetail = $this->find($attributes['parent_id']);
            if (empty($branchDetail)) {
                throw new ValidationException('CLB thuộc chi hội validation failed', ['reload' => true, 'parent_id' => ['Chi hội đã bị xóa']]);
            }
            $detailOption = Options::getCodeByCondititon(['code' => $attributes['code']]);
            if (empty($detailOption)) {
                throw new ValidationException('Options validation failed', ['reload' => true, 'option_code' => ['CLB không tồn tại hoặc đã xóa']]);
            }
            $attributes['name'] = $detailOption->value;
            $attributes['created_at'] = date('Y-m-d H:i:s');
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $attributes['code'] = $detailOption->code;
            $attributes['option_code'] = self::BRANCHESCODE;

            $this->branches->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('CLB thuộc chi hội validation failed', $this->getErrors());
    }

    /**
     * @param $id
     * @param $attributes
     *
     * @return bool|mixed
     *
     * @throws \App\Exceptions\Validation\ValidationException
     */
    public function update($id, $attributes) {
        $this->branches = $this->find($id);
        if (empty($this->branches)) {
            throw new ValidationException('CLB thuộc chi hội validation failed', ['reload' => true, 'updated' => false, 'nullBranch' => ['CLB không tồn tại hoặc đã xóa']]);
        }
        if ($this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName)) {
            $branchDetail = $this->find($attributes['parent_id']);
            if (empty($branchDetail)) {
                throw new ValidationException('CLB thuộc chi hội validation failed', ['reload' => true, 'updated' => false, 'parent_id' => ['Chi hội đã bị xóa']]);
            }
            if (isset($attributes['status']) && $attributes['status'] == 0) {
                $validateData = $this->validateDeleteOrDisableBranch($id, true);
                if ($validateData !== true) {
                    throw new ValidationException('Branches validation failed', ['updated' => false, 'reload' => false, 'errorsUpdateDisable' => $validateData]);
                }
            }
            $detailOption = Options::getCodeByCondititon(['code' => $attributes['code']]);
            if (empty($detailOption)) {
                throw new ValidationException('Options validation failed', ['reload' => true, 'option_code' => ['CLB không tồn tại hoặc đã xóa']]);
            }
            $duplicateRow = $this->branches->where('parent_id', $attributes['parent_id'])
                ->where('code', $detailOption->code)
                ->where('id', '<>', $id)->first();
            if (!empty($duplicateRow)) {
                throw new ValidationException('Options validation failed', ['reload' => false, 'code' => ['CLB thuộc Chi Hội đã tồn tại.']]);
            }
            $attributes['code'] = $detailOption->code;
            $attributes['name'] = $detailOption->value;
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $attributes['option_code'] = self::BRANCHESCODE;
            $this->branches->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('Branches validation failed', $this->getErrors());
    }

    /**
     * @param $id
     *
     * @return mixed|void
     */
    public function delete($id) {
        if ($this->validateDeleteOrDisableBranch($id) !== true) {
            return $this->validateDeleteOrDisableBranch($id);
        }

        $branch = $this->branches->find($id);
        if (!empty($branch) && $branch->delete()) {
            return ['deleted' => true, 'messages' => 'Xóa CLB thành công'];
        }

        return ['deleted' => false, 'messages' => 'CLB không tồn tại hoặc đã xóa'];
    }

    public function validateDeleteOrDisableBranch($id, $update = false) {
        $countMember = $this->getMemberIdMemberPositionByBranchId($id);

        // Có hội viên không được xóa
        if (!empty($countMember)) {
            if ($update) {
                return 'CLB có hội viên không thể chuyển sang Không hoạt động';
            }

            return ['deleted' => false, 'messages' => 'CLB có hội viên không xóa được'];
        }
        $countManager = $this->getEmployeesPositionByBranchId($id);
        if ($countManager->count()) {
            if ($update) {
                return 'CLB có lãnh đạo không thể chuyển sang Không hoạt động';
            }

            return ['deleted' => false, 'messages' => 'CLB có lãnh đạo không xóa được'];
        }

        return true;
    }

    /**
     * get list EmployeesPosition by id.
     *
     * @var array
     */
    public function getEmployeesPositionByBranchId($id) {
        return EmployeesPosition::where('branch_id', $id)->with(['employees'])->orderBy('created_at', 'asc')->get();
    }

    /**
     * save saveEmployeesPosition
     *
     * @var array
     */
    public function saveEmployeesPosition($attributes) {
        // case Create/Update
        if (in_array($attributes['action'], $this->crudTableAction)) {
            if ($this->validateMemberPosition($attributes) !== true) {
                return $this->validateMemberPosition($attributes);
            }

            if ($attributes['action'] == 'save') {
                $employees = new Employees();
                $employees->fullname = $attributes['l_fullname'];
                if (isset($attributes['l_birthday'])) {
                    $employees->birthday = $attributes['l_birthday'];
                }
                if (isset($attributes['l_phone'])) {
                    $employees->phone = $attributes['l_phone'];
                }
                if (isset($attributes['l_email'])) {
                    $employees->email = $attributes['l_email'];
                }
                $employees->option_code = $attributes['l_position'];
                $employees->created_at = date('Y-m-d H:i:s');
                $employees->updated_at = date('Y-m-d H:i:s');
                if ($employees->save()) {
                    $employeesPosition = new EmployeesPosition();
                    $employeesPosition->branch_id = $attributes['branchesId'];
                    $employeesPosition->employee_id = $employees->id;
                    $employeesPosition->option_code = $attributes['l_position'];
                    $employeesPosition->created_at = date('Y-m-d H:i:s');
                    $employeesPosition->updated_at = date('Y-m-d H:i:s');
                    $employeesPosition->save();
                }
            } elseif ($attributes['action'] == 'update') {
                $employeesPosition = EmployeesPosition::find($attributes['rid']);
                if (empty($employeesPosition)) {
                    return ['error' => $attributes['l_fullname'] . ' không tồn tại hoặc bị xóa'];
                }
                $employeesPosition->option_code = $attributes['l_position'];
                $employeesPosition->updated_at = date('Y-m-d H:i:s');
                $employeesPosition->save();
                $employees = Employees::find($employeesPosition->employee_id);
                if (empty($employees)) {
                    return ['error' => $attributes['l_fullname'] . ' không tồn tại hoặc bị xóa'];
                }
                $arrayUpdated = [
                    'fullname' => $attributes['l_fullname'],
                    'option_code' => $attributes['l_position'],
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $arrayUpdated['birthday'] = isset($attributes['l_birthday']) ? $attributes['l_birthday'] : null;
                $arrayUpdated['phone'] = isset($attributes['l_phone']) ? $attributes['l_phone'] : null;
                $arrayUpdated['email'] = isset($attributes['l_email']) ? $attributes['l_email'] : null;
                $employees = Employees::updateOrCreate([
                            'id' => $employeesPosition->employee_id
                                ], $arrayUpdated);
            }
            $responses['success'] = 1;
            $responses['l_fullname'] = htmlentities($employees->fullname);
            $responses['l_email'] = !empty($employees->email) ? $employees->email : '';
            $responses['l_phone'] = !empty($employees->phone) ? $employees->phone : '';
            $responses['l_birthday'] = !empty($employees->birthday) ? $employees->birthday : '';
            $responses['l_position'] = $this->options->value;
            $responses['id'] = $employeesPosition->id;

            return $responses;
        } elseif ($attributes['action'] == $this->crudTableDelAction) {
            $employeesPosition = EmployeesPosition::where('id', $attributes['rid'])->with(['employees'])->first();
            // check case delete
            if (empty($employeesPosition)) {
                return ['error' => 'Lỗi dữ liệu!!!!'];
            }
            if ($employeesPosition->employees !== "") {
                $employeesPosition->employees->delete();
                if ($employeesPosition->delete()) {
                    $responses['success'] = 1;
                    $responses['id'] = $employeesPosition->id;

                    return $responses;
                }
            }
        }
        // check case delete

        return null;
    }

    /**
     * save validateMemberPosition
     *
     * @var array
     */
    public function validateMemberPosition($attributes) {
        if (!isset($attributes['branchesId'])) {
            return ['error' => 'Lỗi dữ liệu'];
        }

        if (!empty($attributes['branchesId'])) {
            $this->detailBranch = $this->branches->find($attributes['branchesId']);
            if (empty($this->detailBranch)) {
                return ['error' => 'Chi hội không tồn tại hoặc đã xóa'];
            }
        }

        if (!empty($attributes['l_position'])) {
            $this->options = Options::where('code', $attributes['l_position'])->where('key', 'position')->first();
            if (empty($this->options)) {
                return ['error' => 'Chức vụ không tồn tại hoặc đã xóa'];
            }
        }

        return true;
    }

    /**
     * save getOptionsByKey
     *
     * @var array
     */
    public function getOptionsByKey($key) {
        if (empty($key)) {
            return [];
        }

        return Options::where('key', $key)->select('value', 'code')->pluck('value', 'code')->all();
    }

    public function getOptionsClub($unuse = false, $search = false, $searchParentId = null) {
        $query = Options::where('options.key', self::BRANCHES_CLUB);
        if ($unuse) {
            //$query->whereRaw('options.code not in(SELECT option_code FROM branches WHERE parent_id > 1)');
            if (!empty($searchParentId)) {
                $query->whereRaw("options.code not in(SELECT code FROM branches WHERE deleted_at is null AND option_code='".self::BRANCHESCODE."' AND parent_id=".(int)$searchParentId.")");
            }
        }
        if (!empty($search)) {
            $query->where('options.value', 'like', '%' . $search . '%');
        }
        return $query->orderBy('options.value', 'ASC')->pluck('value', 'code')->all();
    }

    public function getOptionDetailByName($name) {
        return Options::where('key', self::BRANCHES_CLUB)->where('value', $name)->first();
    }

    public function getMemberByCondition($condition) {
        return Member::where($condition)->get();
    }

    public function ajaxSearchMemberByCondition($attributes) {
        $keyword = $attributes['search'];
        $branchId = $attributes['branchId'];
        $listMemberIdFromMemberPosition = $this->getMemberIdMemberPositionByBranchId($branchId);

        return Member::select('members.*', DB::raw('group_concat(languages.languageName) as guideLanguage'))
                        ->leftjoin("languages",\DB::raw("FIND_IN_SET(languages.id, members.guideLanguage)"),">",\DB::raw("'0'"))
                        ->whereNotIn('members.id', $listMemberIdFromMemberPosition)
                        ->where('members.status', MemberConstants::MEMBER_OFFICIAL_MEMBER)
                        ->whereNull('is_delete')
                        ->where('members.fullName', 'like', '%' . $keyword . '%')->groupBy('members.id')->get();
    }

    public function getListBranch() {
        return $this->branches
                        ->select('name', 'id')
                        ->where('option_code', self::BRANCHESCODECH)
                        ->where('status', 1)
                        ->orderBy('name', 'asc')
                        ->pluck('name', 'id')->all();
    }

    public function getMemberIdMemberPositionByBranchId($branchId) {
        return MemberPosition::where('branch_id', $branchId)
                        ->pluck('member_id')->all();
    }

    public function getListMemberExportExcel($branchId) {
        $listId = $this->getMemberIdMemberPositionByBranchId($branchId);

        return Member::where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)
                        ->whereIn('id', $listId)
                        ->get();
    }

    public function paginateMemberByCondition($id, $page = 1, $limit = 15, $params = false) {
        $result = new \StdClass();
        $result->page = $page;
        $result->limit = $limit;
        $result->totalItems = 0;
        $result->items = array();

        // filter member
        $clause = [
                ['status', MemberConstants::MEMBER_OFFICIAL_MEMBER],
                ['is_delete', null],
        ];
        $query = Member::where($clause)
                        ->join('member_position', 'member_position.member_id', '=', 'members.id')
                        ->where('member_position.branch_id', $id)->whereNull('member_position.deleted_at');

        if (isset($params['search_member_name']) && $params['search_member_name'] != '') {
            $query->where('members.fullName', 'like', '%' . trim($params['search_member_name']) . '%');
        }
        if (isset($params['search_member_code']) && $params['search_member_code'] != '') {
            $query->where('members.member_code', 'like', '%' . trim($params['search_member_code']) . '%');
        }
        if (isset($params['search_touristGuideCode']) && $params['search_touristGuideCode'] != '') {
            $query->where('members.touristGuideCode', 'like', '%' . trim($params['search_touristGuideCode']) . '%');
        }
        $query->orderBy('member_position.id', 'desc');

        $count = $query->count();
        $lists = $query->skip($limit * ($page - 1))->take($limit)->get();

        $result->totalItems = $count;
        $result->items = $lists->all();

        return $result;
    }

    public function deleteMember($id) {
        $memberPosition = MemberPosition::findOrFail($id);

        return $memberPosition->delete();
    }

    public function createMember($id, $attributes) {
        $rules = ['member_id' => 'required'];
        $messages = ['required' => ':attribute là không được bỏ trống'];
        $niceAttributeName = ['member_id' => 'Họ tên'];
        if ($this->isValid($attributes, $rules, $messages, $niceAttributeName)) {
            $member = Member::where('id', $attributes['member_id'])
                    ->whereNull('is_delete')
                    ->where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)
                    ->first();
            if (empty($member)) {
                throw new ValidationException('Member validation failed', ['reload' => true, 'member_id' => ['Hội viên không tồn tại hoặc đã bị xóa']]);
            }
            $memberPosition = new MemberPosition();
            $memberPosition->branch_id = $id;
            $memberPosition->member_id = $attributes['member_id'];
            $memberPosition->created_at = date('Y-m-d H:i:s');
            $memberPosition->updated_at = date('Y-m-d H:i:s');
            $memberPosition->save();

            return true;
        }

        throw new ValidationException('CLB thuộc chi hội validation failed', $this->getErrors());
    }

    // ========================== Frontend ==========================

    public function getClbByBranchesId($branchId) {
        return $this->branches->select('name', 'id')->where('parent_id', $branchId)->pluck('name', 'id')->all();
    }

    public function paginateManager($id, $limit = 12) {
        $result = EmployeesPosition::where('branch_id', $id)
                ->with(['employees'])->orderBy('id', 'desc')
                ->paginate($limit);

        return $result;
    }

    public function paginateMember($id, $limit = 12, $params = false) {
        $clause = [
                ['status', MemberConstants::MEMBER_OFFICIAL_MEMBER], ['is_delete', null],
        ];
        $query = Member::where($clause)
                        ->join('member_position', 'member_position.member_id', '=', 'members.id')
                        ->where('member_position.branch_id', $id)->whereNull('member_position.deleted_at');
        // search
        if (isset($params['search_member_name']) && $params['search_member_name'] != '') {
            $query->where('fullName', 'like', '%' . trim($params['search_member_name']) . '%');
        }
        if (isset($params['search_touristGuideCode']) && $params['search_touristGuideCode'] != '') {
            $query->where('touristGuideCode', 'like', '%' . trim($params['search_touristGuideCode']) . '%');
        }
        if (isset($params['search_member_code']) && $params['search_member_code'] != '') {
            $query->where('member_code', 'like', '%' . trim($params['search_member_code']) . '%');
        }
        if (isset($params['search_guideLanguage']) && $params['search_guideLanguage'] != '') {
            $query->where('guideLanguage', 'like', '%' . trim($params['search_guideLanguage'] . '%'));
        }
        $query->orderBy('member_position.id', 'desc');

        return $query->paginate($limit);
    }

    public function getListMemberClubOfBranchByBranchId($id) {
        $listClubOfBranchId = $this->branches->where('option_code', $this->branchCode)
            ->where('parent_id', $id)->pluck('id')->all();
        $clause = [
            ['status', MemberConstants::MEMBER_OFFICIAL_MEMBER],
            ['is_delete', null],
        ];
        return Member::select('members.id')->where($clause)
            ->join('member_position', 'member_position.member_id', '=', 'members.id')
            ->whereIn('member_position.branch_id', $listClubOfBranchId)->whereNull('member_position.deleted_at')->pluck('id')->all();
    }
}
