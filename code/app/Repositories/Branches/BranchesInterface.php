<?php

namespace App\Repositories\Branches;

use App\Repositories\RepositoryInterface;

/**
 * Interface BranchesInterface.
 *
 * @author laven9696
 */
interface BranchesInterface extends RepositoryInterface {
    
}
