<?php

namespace App\Repositories\Branches;

use App\Models\Offices;
use Config;
use App\Models\Branches;
use App\Repositories\RepositoryAbstract;
use App\Repositories\CrudableInterface;
use App\Exceptions\Validation\ValidationException;
use App\Constants\MemberConstants;
use App\Models\Member;
use App\Models\EmployeesPosition;
use App\Models\Employees;
use App\Models\Options;
use App\Constants\UserConstants;
use DB;

/**
 * Class BranchesRepository.
 *
 * @author laven9696
 */
class BranchesRepository extends RepositoryAbstract implements BranchesInterface, CrudableInterface {

    const INVALID_ATTR = ':attribute không hợp lệ';
    const BRANCHESCODE = 'BRANCHES01';
    const VPDD_CODE = 'OFFICES';
    const HOISO_CODE = 'HEAD';

    /**
     * @array
     */
    protected $crudTableAction = ['save', 'update'];
    protected $detailBranch = null;
    protected $employees = null;
    protected $options = null;
    protected $branchCode = null;

    /**
     * @string
     */
    protected $crudTableDelAction = 'del';

    /**
     * @var
     */
    public $perPage;

    /**
     * @var \Branches
     */
    protected $branches;

    /**
     * Rules.
     *
     * @var array
     */
    protected static $rules = [
        'code' => 'required|alpha_num|max:10|unique:branches',
        'name' => 'required|max:50',
        'parent_id' => 'required|integer',
        'address' => 'max:350',
        'phone' => 'nullable|numeric|digits_between:1,30',
        'email' => 'nullable|email|max:100',
        'note' => 'max:500',
    ];
    protected static $messages = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'alpha_num' => ':attribute chỉ được gồm chữ và số',
        'integer' => ':attribute phải là số',
        'max' => 'Độ dài tối đa của :attribute là :max kí tự',
        'min' => 'Độ dài tối thiểu của :attribute là :min kí tự',
        'unique' => ':attribute đã được sử dụng',
        'email' => self::INVALID_ATTR,
        'in' => self::INVALID_ATTR,
    ];
    protected static $niceAttributeName = [
        'code' => 'Mã Chi Hội',
        'name' => 'Tên Chi Hội',
        'parent_id' => 'VPĐD',
        'address' => 'Địa chỉ',
        'phone' => 'Số điện thoại',
        'email' => 'Email',
        'note' => 'Ghi chú',
    ];

    /**
     * @param Branches $branches
     */
    public function __construct(Branches $branches) {
        $this->branches = $branches;
        $this->perPage = $this->branches->perPage;
        $this->branchCode = self::BRANCHESCODE;
    }

    /**
     * @return mixed
     */
    public function all() {
        return $this->branches->get();
    }

    /**
     * @param int  $page
     * @param int  $limit
     * @param bool $all
     *
     * @return mixed|\StdClass
     */
    public function paginate($page = 1, $limit = 15, $params = false) {
        $result = new \StdClass();
        $result->page = $page;
        $result->limit = $limit;
        $result->totalItems = 0;
        $result->items = array();

        $query = $this->branches->orderBy('id', 'desc');
        $auth = auth()->user();
        if ($auth->role == UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
            if (empty($auth->branch_id)) {
                return null;
            }
            $query = $query->whereIn('id', explode(',', $auth->branch_id));
        }
        if ($this->branchCode == null) {
            return null;
        }

        $query->where('option_code', $this->branchCode);

        // search
        if (isset($params['search_code']) && $params['search_code'] != '') {
            $query->where('code', 'like', '%' . trim($params['search_code']) . '%');
        }
        if (isset($params['search_name']) && $params['search_name'] != '') {
            $query->where('name', 'like', '%' . trim($params['search_name']) . '%');
        }
        if (isset($params['search_address']) && $params['search_address'] != '') {
            $query->where('address', 'like', '%' . trim($params['search_address']) . '%');
        }
        if (isset($params['search_parent_id']) && $params['search_parent_id'] != '') {
            $query->where('parent_id', trim($params['search_parent_id']));
        }
        if (isset($params['search_status']) && $params['search_status'] != '') {
            $query->where('status', trim($params['search_status']));
        }
        if (isset($params['search_phone']) && $params['search_phone'] != '') {
            $query->where('phone', 'like', '%' . trim($params['search_phone']) . '%');
        }
        if (isset($params['search_email']) && $params['search_email'] != '') {
            $query->where('email', 'like', '%' . trim($params['search_email']) . '%');
        }

        if (auth()->check()) {
            $user = auth()->user();
            $branchIds = $user->branch_id ? [$user->branch_id] : [];
            if (empty($branchIds)) {
                $provinceType = $user->province_type ?? null;
                if ($provinceType) {
                    $getProvinceCode = Offices::getProvinceCodeByProvinceType($provinceType);
                    foreach ($getProvinceCode as $provinceCode) {
                        $branchIds[] = $provinceCode['id'];
                    }
                }
            }

            if (count($branchIds)) {
                $query->whereIn('id', $branchIds);
            }
        }

        $count = $query->count();
        $lists = $query->skip($limit * ($page - 1))->take($limit)->get();

        $result->totalItems = $count;
        $result->items = $lists->all();

        return $result;
    }

    /**
     * @param $parentId
     * @param array $pluckBy
     * @return mixed
     */
    public function getAllByParentId ($parentId)
    {
        $getProvinceCode = Offices::whereNotNull('parent_id')
            ->where('status',1)
            ->where('parent_id', $parentId)
            ->whereNull('deleted_at')
            ->pluck('id')
            ->all();

        $getBranch = Offices::where('status',1)
            ->where('id', $parentId)
            ->whereNull('deleted_at')
            ->first();

        if (!empty($getBranch) && $getBranch->area != null) {
            if ($getBranch->area = 'HN') {
                $getClub = Offices::whereNull('parent_id')
                    ->where('status',1)
                    ->where('option_code', 'BRANCHES03')
                    ->whereNull('deleted_at')
                    ->pluck('id')
                    ->all();
                if (!empty($getClub)) {
                    $getProvinceCode = array_merge($getProvinceCode, $getClub);
                }
            }
        }

        return $getProvinceCode;


        /*$objBranches = $this->branches->where('id', $id)
            ->where('status', 1)
            ->whereNull('deleted_at');

        if (!empty($pluckBy) && is_array($pluckBy)) {
            $pluckBy = implode(',', $pluckBy);
            $objBranches = $objBranches->pluck($pluckBy)->all();
        } else {
            $objBranches = $objBranches->get();
        }

        return $objBranches;*/
    }

    public function getAll (array $pluckBy = [])
    {
        $objBranches = $this->branches->where('status', 1);

        if (!empty($pluckBy) && is_array($pluckBy)) {
            $pluckBy = implode(',', $pluckBy);
            $objBranches = $objBranches->pluck($pluckBy)->all();
        } else {
            $objBranches = $objBranches->get();
        }

        return $objBranches;
    }

    /**
     * Lấy ra với parentId khác với parentId nhập vào
     *
     * @param $parentId
     * @param array $pluckBy
     * @return mixed
     */
    public function getAllByNotNullNotEqualParentId ($parentId, array $pluckBy = [])
    {
        $getProvinceCode = Offices::whereNotNull('parent_id')
            ->where('status',1)
            ->where('parent_id', '<>', $parentId)
            ->whereNull('deleted_at')
            ->pluck('id')
            ->all();

        $getBranch = Offices::where('status',1)
            ->where('id', $parentId)
            ->whereNull('deleted_at')
            ->first();

        if ($getBranch->area != null) {
            if ($getBranch->area = 'HN') {
                $getClub = Offices::whereNull('parent_id')
                    ->where('status',1)
                    ->where('option_code', 'BRANCHES03')
                    ->whereNull('deleted_at')
                    ->pluck('id')
                    ->all();
                if (!empty($getClub)) {
                    $getProvinceCode = array_merge($getProvinceCode, $getClub);
                }
            }
        }

        return $getProvinceCode;

        /*$objBranches = $this->branches->whereNotNull('parent_id')
            ->where('parent_id', '<>', $parentId)
            ->where('status', 1)
            ->whereNull('deleted_at');

        if (!empty($pluckBy) && is_array($pluckBy)) {
            $pluckBy = implode(',', $pluckBy);

            $objBranches = $objBranches->pluck($pluckBy)->all();
        } else {
            $objBranches = $objBranches->get();
        }

        return $objBranches;*/
    }

    /**
     * @return mixed
     */
    public function lists() {
        
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id) {
        return $this->branches->with(['MemberPosition'])->findOrFail($id);
    }

    /**
     * @param $attributes
     *
     * @return bool|mixed
     *
     * @throws \App\Exceptions\Validation\ValidationException
     */
    public function create($attributes) {
        // check mã code hoa thường
        $attributes['code2'] = $attributes['code'];
        $attributes['code'] = strtolower($attributes['code']);
        
        if ($this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName)) {
            
            $parents = $this->branches->where('id', $attributes['parent_id'])->first();
            if (empty($parents)) {
                throw new ValidationException('Branches validation failed', ['updated' => false,'reload' => true, 'nullBranch' => ['Không thêm được chi hội cho VPĐD đại diện đã xóa']]);
            }
            
            $attributes['created_at'] = date('Y-m-d H:i:s');
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $attributes['option_code'] = $this->branchCode;
            $attributes['code'] = $attributes['code2'];

            $this->branches->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('Branches validation failed', $this->getErrors());
    }

    /**
     * @param $id
     * @param $attributes
     *
     * @return bool|mixed
     *
     * @throws \App\Exceptions\Validation\ValidationException
     */
    public function update($id, $attributes) {
        $this->branches = $this->find($id);
        if (empty($this->branches)) {
            throw new ValidationException('Chi hội validation failed', ['reload' => true, 'updated' => false, 'nullBranch' => ['Chi hội tồn tại hoặc đã xóa']]);
        }

        self::$rules['code'] = 'required|alpha_num|max:20|unique:branches,code,' . $this->branches->id;
        if ($this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName)) {
            
            $parents = $this->branches->where('id', $attributes['parent_id'])->first();
            if (empty($parents)) {
                throw new ValidationException('Branches validation failed', ['updated' => false, 'reload' => true, 'nullBranch' => ['Không thêm được chi hội cho VPĐD đại diện đã xóa']]);
            }
            if (isset($attributes['status']) && $attributes['status'] == 0) {
                $validateData = $this->validateDeleteOrDisableBranch($id, true);
                if ($validateData !== true) {
                    throw new ValidationException('Branches validation failed', ['updated' => false, 'reload' => false, 'errorsUpdateDisable' => $validateData]);
                }
            }
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $this->branches->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('Branches validation failed', $this->getErrors());
    }

    /**
     * @param $id
     *
     * @return mixed|void
     */
    public function delete($id) {
        if ($this->validateDeleteOrDisableBranch($id) !== true) {
            return $this->validateDeleteOrDisableBranch($id);
        }
        $branch = $this->branches->find($id);
        if (!empty($branch) && $branch->delete()) {
            return ['deleted' => true, 'messages' => 'Xóa Chi hội thành công'];
        }

        return ['deleted' => false, 'messages' => 'Chi hội không tồn tại hoặc đã xóa'];
    }
    
    public function validateDeleteOrDisableBranch($id, $update = false) {
        $countMember = Member::where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)
                        ->where('province_code', $id)->count();
        
        // Có hội viên không được xóa
        if ($countMember) {
            if ($update) {
                return 'Chi Hội có hội viên không thể chuyển sang Không hoạt động';
            }

            return ['deleted' => false, 'messages' => 'Chi Hội có hội viên không xóa được'];
        }
        $countManager = $this->getEmployeesPositionByBranchId($id);
        if ($countManager->count()) {
            if ($update) {
                return 'Chi Hội có lãnh đạo không thể chuyển sang Không hoạt động';
            }

            return ['deleted' => false, 'messages' => 'Chi Hội có lãnh đạo không xóa được'];
        }

        return true;
    }

    /**
     * @param $id
     *
     * @description lấy danh sách Employees theo chi hội
     */
    public function getEmployeesPositionByBranchId($id) {
        return EmployeesPosition::where('branch_id', $id)->with(['employees'])->orderBy('created_at', 'asc')->get();
    }

    /**
     * @param $id
     *
     * @description lưu ban lãnh đạo
     */
    public function saveEmployeesPosition($attributes) {
        // case Create/Update
        if (in_array($attributes['action'], $this->crudTableAction)) {
            if ($this->validateMemberPosition($attributes) !== true) {
                return $this->validateMemberPosition($attributes);
            }

            // case add
            if ($attributes['action'] == 'save') {
                // lưu thông tin người dùng
                $employees = new Employees();
                $employees->fullname = $attributes['l_fullname'];
                if (isset($attributes['l_birthday'])) {
                    $employees->birthday = $attributes['l_birthday'];
                }
                if (isset($attributes['l_phone'])) {
                    $employees->phone = $attributes['l_phone'];
                }
                if (isset($attributes['l_email'])) {
                    $employees->email = $attributes['l_email'];
                }
                $employees->option_code = $attributes['l_position'];
                $employees->created_at = date('Y-m-d H:i:s');
                $employees->updated_at = date('Y-m-d H:i:s');
                if ($employees->save()) {
                    // lưu chức vụ
                    $employeesPosition = new EmployeesPosition();
                    $employeesPosition->branch_id = $attributes['branchesId'];
                    $employeesPosition->employee_id = $employees->id;
                    $employeesPosition->option_code = $attributes['l_position'];
                    $employeesPosition->created_at = date('Y-m-d H:i:s');
                    $employeesPosition->updated_at = date('Y-m-d H:i:s');
                    $employeesPosition->save();
                }
            } elseif ($attributes['action'] == 'update') {
                $employeesPosition = EmployeesPosition::find($attributes['rid']);
                if (empty($employeesPosition)) {
                    return ['error' => $attributes['l_fullname'] . ' không tồn tại hoặc bị xóa'];
                }
                $employeesPosition->option_code = $attributes['l_position'];
                $employeesPosition->updated_at = date('Y-m-d H:i:s');
                $employeesPosition->save();

                $employees = Employees::find($employeesPosition->employee_id);
                if (empty($employees)) {
                    return ['error' => $attributes['l_fullname'] . ' không tồn tại hoặc bị xóa'];
                }
                $arrayUpdated = [
                    'fullname' => $attributes['l_fullname'],
                    'option_code' => $attributes['l_position'],
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $arrayUpdated['birthday'] = isset($attributes['l_birthday']) ? $attributes['l_birthday'] : null;
                $arrayUpdated['phone'] = isset($attributes['l_phone']) ? $attributes['l_phone'] : null;
                $arrayUpdated['email'] = isset($attributes['l_email']) ? $attributes['l_email'] : null;

                $employees = Employees::updateOrCreate([
                            'id' => $employeesPosition->employee_id
                                ], $arrayUpdated);
            }
            // array response
            $responses['success'] = 1;
            $responses['l_fullname'] = htmlentities($employees->fullname);
            $responses['l_email'] = !empty($employees->email) ? $employees->email : '';
            $responses['l_phone'] = !empty($employees->phone) ? $employees->phone : '';
            $responses['l_birthday'] = !empty($employees->birthday) ? $employees->birthday : '';
            $responses['l_position'] = $this->options->value;
            $responses['id'] = $employeesPosition->id;

            return $responses;
        } elseif ($attributes['action'] == $this->crudTableDelAction) {
            $employeesPosition = EmployeesPosition::where('id', $attributes['rid'])->with(['employees'])->first();
            // check case delete
            if (empty($employeesPosition)) {
                return ['error' => 'Lỗi dữ liệu!!!!'];
            }
            if ($employeesPosition->employees !== "") {
                $employeesPosition->employees->delete();
                if ($employeesPosition->delete()) {
                    $responses['success'] = 1;
                    $responses['id'] = $employeesPosition->id;

                    return $responses;
                }
            }
        }
        // check case delete

        return null;
    }

    public function validateMemberPosition($attributes) {
        if (!isset($attributes['branchesId'])) {
            return ['error' => 'Lỗi dữ liệu'];
        }

        if (!empty($attributes['branchesId'])) {
            $this->detailBranch = $this->branches->find($attributes['branchesId']);
            if (empty($this->detailBranch)) {
                return ['error' => 'Chi hội không tồn tại hoặc đã xóa'];
            }
        }

        if (!empty($attributes['l_position'])) {
            $this->options = Options::where('code', $attributes['l_position'])->where('key', 'position')->first();
            if (empty($this->options)) {
                return ['error' => 'Chức vụ không tồn tại hoặc đã xóa'];
            }
        }

        return true;
    }

    public function getOptionsByKey($key) {
        if (empty($key)) {
            return [];
        }

        return Options::where('key', $key)->select('value', 'code')->pluck('value', 'code')->all();
    }

    public function getMemberByCondition($condition, $listMemberByClubOfBranchId) {
        return Member::select('firstMobile', 'firstEmail', 'secondMobile', 'secondEmail','fullName', 'birthday', 'touristGuideCode', 'member_code', 'guideLanguage', 'member_from', 'address')
            ->whereIn('id', $listMemberByClubOfBranchId)->orWhere($condition)->get();
    }

    public function getVPDD() {
        return $this->branches
                ->select('name', 'id')
                ->where(function($query) {
                    $query->where(function($query) {
                        $query->where('option_code', self::VPDD_CODE);
                    })->orWhere(function($query) {
                        $query->where('option_code', self::HOISO_CODE);
                    });
                })
                ->where('status', 1)
                ->orderBy('name', 'asc')
                ->pluck('name', 'id')
                ->all();
    }

    // ============================== FrontEnd ============================
    public function listAllBranches() {
        $query = $this->branches->select('name', 'id')->orderBy('name', 'asc')->where('status', '<>', 0);

        if ($this->branchCode == null) {
            return null;
        }

        return $query->where('option_code', $this->branchCode)->pluck('name', 'id')->all();
    }
    
    public function paginateManager($id, $limit = 12) {
        $result = EmployeesPosition::where('branch_id', $id)
                ->with(['employees'])->orderBy('created_at', 'asc')
                ->paginate($limit);

        return $result;
    }
    
    public function paginateMember($id, $limit = 12, $params = false) {
        $clause = [
                ['status', MemberConstants::MEMBER_OFFICIAL_MEMBER],
                ['is_delete', null],
                ['typeOfPlace', \App\Constants\BranchConstants::TYPE_OF_PLACE_CHI_HOI],
                ['province_code', $id]
        ];

        $query = Member::where($clause);
        // search
        if (isset($params['search_member_name']) && $params['search_member_name'] != '') {
            $query->where('fullName', 'like', '%' . trim($params['search_member_name']) . '%');
        }
        if (isset($params['search_touristGuideCode']) && $params['search_touristGuideCode'] != '') {
            $query->where('touristGuideCode', 'like', '%' . trim($params['search_touristGuideCode']) . '%');
        }
        if (isset($params['search_member_code']) && $params['search_member_code'] != '') {
            $query->where('member_code', 'like', '%' . trim($params['search_member_code']) . '%');
        }
        if (isset($params['search_guideLanguage']) && $params['search_guideLanguage'] != '') {
            $query->where('guideLanguage', 'like', '%' . trim($params['search_guideLanguage'] . '%'));
        }

        return $query->paginate($limit);
    }
}
