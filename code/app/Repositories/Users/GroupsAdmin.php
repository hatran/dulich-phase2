<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\Users;

use App\Models\Groups;
use App\Models\Options;
use Validator as V;
use App\Models\Permission;
use App\Models\GroupsRoles;
use App\Exceptions\Validation\ValidationException;


/**
 * Description of GroupsAdmin
 *
 * @author DuyDuc
 */
class GroupsAdmin extends Groups {

    const PERPAGE = 10;
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public static $statusLabel = [
        self::STATUS_ENABLE => 'Hoạt động',
        self::STATUS_DISABLE => 'Bị khóa'
    ];

    protected static $rules = [
        'name' => 'required|max:100',
        'description' => 'required|max:400',
        'group_role_id' => 'required',
    ];
    protected static $messages = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'max' => 'Độ dài tối đa của :attribute là :max kí tự',
        'unique' => ':attribute đã được sử dụng',
        'alpha_num' => ':attribute chỉ có thể chứa chữ cái và số.',
    ];
    protected static $niceAttributeName = [
        'name' => 'Tên',
        'group_role_id' => 'Nhóm vai trò',
        'description' => 'Mô tả'
    ];

    /**
     * @var
     */
    public $perPage;

    /**
     * __construct
     */
    public function __construct() {
        $this->perPage = self::PERPAGE;
        parent::__construct();
    }
    
    public function pagination($params = false) {
        $query = $this;
        if (isset($params['search_code']) && $params['search_code'] != '') {
            $query = $query->where('code', 'like', '%' . trim($params['search_code']) . '%');
        }
        if (isset($params['search_name']) && $params['search_name'] != '') {
            $query = $query->where('name', 'like', '%' . trim($params['search_name']) . '%');
        }
        if (isset($params['search_description']) && $params['search_description'] != '') {
            $query = $query->where('description', 'like', '%' . trim($params['search_description']) . '%');
        }
        if (isset($params['search_status']) && $params['search_status'] != '') {
            $query = $query->where('status', $params['search_status']);
        }
        $listThongtin = $query->orderBy('id', 'desc')->paginate($this->perPage);

        return $listThongtin;
    }

    public function getListAllPermissions() {
        return Permission::select('id', 'parent', 'name', 'route_name', 'description')->orderByRaw('ISNULL(position), position ASC')->get();
    }

    public function getListGroupRole() {
        return GroupsRoles::pluck('name', 'id')->all();
    }
    
    public function getListAll() {
        return $this->pluck('name', 'code')->all();
    }

    public function create($attributes) {
        $validate = $this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName);

        if ($validate === true) {
            $attributes['group_role_id'] = json_encode($attributes['group_role_id']);
            $attributes['created_at'] = date('Y-m-d H:i:s');
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $this->fill($attributes)->save();
            return true;
        }

        throw new ValidationException('Groups validation failed', $validate);
    }
    
    public function updateData($id, $attributes) {
        $groups = $this->find($id);
        if (empty($groups)) {
            throw new ValidationException('Groups validation failed', ['updated' => false, 'nullBranch' => ['Quyền không tồn tại hoặc đã xóa']]);
        }
        $rules = self::$rules;
        $validate = $this->isValid($attributes, $rules, self::$messages, self::$niceAttributeName);

        if ($validate === true) {
            $attributes['group_role_id'] = json_encode($attributes['group_role_id']);
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $groups->fill($attributes)->save();
            return true;
        }

        throw new ValidationException('Groups validation failed', $validate);
    }

    public function isValid(array $attributes, array $rules = null, array $messages = null, array $niceAttributeName = null) {
        if (empty($messages)) {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules);
        } else {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        }
        // $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        if (!empty($niceAttributeName)) {
            $v->setAttributeNames($niceAttributeName);
        }
        if ($v->fails()) {
            return $v->messages();
        }

        return true;
    }

    public function mappingStatus() {
        return isset(self::$statusLabel[$this->status]) ? self::$statusLabel[$this->status] : 'Bị khóa';
    }

}
