<?php
/**
 * Created by PhpStorm.
 * User: DuyDuc
 * Date: 5/9/2018
 * Time: 10:48 PM
 */

namespace App\Repositories\Users;


use App\Models\GroupsRoles;
use App\Models\Permission;
use Validator as V;
use App\Exceptions\Validation\ValidationException;
use DB;

class GroupsRolesAdmin extends GroupsRoles
{
    const PERPAGE = 10;
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public static $statusLabel = [
        self::STATUS_ENABLE => 'Hoạt động',
        self::STATUS_DISABLE => 'Bị khóa'
    ];

    protected static $rules = [
        'name' => 'required|max:100',
        'description' => 'required|max:500',
        'permissions' => 'required'
    ];
    protected static $messages = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'max' => 'Độ dài tối đa của :attribute là :max kí tự',
    ];
    protected static $niceAttributeName = [
        'name' => 'Tên',
        'description' => 'Mô tả',
        'permissions' => 'Quyền'
    ];

    /**
     * @var
     */
    public $perPage;

    /**
     * __construct
     */
    public function __construct() {
        $this->perPage = self::PERPAGE;
        parent::__construct();
    }

    public function pagination($params = false) {
        $query = $this->select('id', 'name', 'description', 'created_at');
        if (isset($params['search_name']) && $params['search_name'] != '') {
            $query = $query->where('name', 'like', '%' . trim($params['search_name']) . '%');
        }
        if (isset($params['search_description']) && $params['search_description'] != '') {
            $query = $query->where('description', 'like', '%' . trim($params['search_description']) . '%');
        }
        $listGroupRole = $query->orderBy('id', 'desc')->paginate($this->perPage);

        return $listGroupRole;
    }

    public function getListPermissionGroup() {
        return Permission::select('id', 'name')
            ->whereRaw('id in (select DISTINCT parent from permission WHERE parent <>0)')
            ->orderBy('name', 'DESC')
            ->pluck('name', 'id')
            ->all();
    }

    public function create($attributes) {
        $validate = $this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName);

        if ($validate === true) {
            $attributes['permissions'] = json_encode($attributes['permissions']);
            $attributes['created_at'] = date('Y-m-d H:i:s');
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $this->fill($attributes)->save();
            return true;
        }

        throw new ValidationException('Groups validation failed', $validate);
    }

    public function updateData($id, $attributes) {
        $groups = $this->find($id);
        if (empty($groups)) {
            throw new ValidationException('Groups validation failed', ['updated' => false, 'nullBranch' => ['Quyền không tồn tại hoặc đã xóa']]);
        }
        $rules = self::$rules;
        $validate = $this->isValid($attributes, $rules, self::$messages, self::$niceAttributeName);

        if ($validate === true) {
            $attributes['permissions'] = json_encode($attributes['permissions']);
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $groups->fill($attributes)->save();
            return true;
        }

        throw new ValidationException('Groups validation failed', $validate);
    }

    public function isValid(array $attributes, array $rules = null, array $messages = null, array $niceAttributeName = null) {
        if (empty($messages)) {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules);
        } else {
            $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        }
        // $v = V::make($attributes, ($rules) ? $rules : static::$rules, $messages);
        if (!empty($niceAttributeName)) {
            $v->setAttributeNames($niceAttributeName);
        }
        if ($v->fails()) {
            return $v->messages();
        }

        return true;
    }
}