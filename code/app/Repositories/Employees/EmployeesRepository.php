<?php

namespace App\Repositories\Employees;

use App\Repositories\RepositoryAbstract;
use App\Repositories\CrudableInterface;
use App\Exceptions\Validation\ValidationException;
use App\Models\Employees;
use DB;

/**
 * Class EmployeesRepository.
 *
 * @author laven9696
 */
class EmployeesRepository extends RepositoryAbstract implements CrudableInterface {

    const INVALID_ATTR = ':attribute không hợp lệ';

    /**
     * @var
     */
    public $perPage;

    /**
     * @var \Employees
     */
    protected $employees;

    /**
     * @param Employees $employees
     */
    public function __construct(Employees $employees) {
        $this->employees = $employees;
        $this->perPage = $this->employees->perPage;
    }

    protected static $rules = [];

    protected static $messages = [];
    
    protected static $niceAttributeName = [];

    /**
     * @return mixed
     */
    public function all() {
        return $this->employees->get();
    }

    /**
     * @param int  $page
     * @param int  $limit
     * @param bool $all
     *
     * @return mixed|\StdClass
     */
    public function paginate($page = 1, $limit = 15, $params = false) {}

    /**
     * @return mixed
     */
    public function lists() {
        
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id) {
        return $this->employees->findOrFail($id);
    }

    /**
     * @param $attributes
     *
     * @return bool|mixed
     *
     * @throws \App\Exceptions\Validation\ValidationException
     */
    public function create($attributes) {
        if ($this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName)) {
            $attributes['created_at'] = date('Y-m-d H:i:s');
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $this->employees->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('Employees validation failed', $this->getErrors());
    }

    /**
     * @param $id
     * @param $attributes
     *
     * @return bool|mixed
     *
     * @throws \App\Exceptions\Validation\ValidationException
     */
    public function update($id, $attributes) {
        $this->employees = $this->find($id);
        if ($this->isValid($attributes, self::$rules, self::$messages, self::$niceAttributeName)) {
            $attributes['updated_at'] = date('Y-m-d H:i:s');
            $this->employees->fill($attributes)->save();

            return true;
        }

        throw new ValidationException('Employees validation failed', $this->getErrors());
    }

    /**
     * @param $id
     *
     * @return mixed|void
     */
    public function delete($id) {
        
        $employees = $this->employees->findOrFail($id);
        $employees->delete();
    }

    /**
     * Get total count.
     *
     * @return mixed
     */
    protected function total() {
        return $this->employees->count();
    }
    
    public function getListEmployeesByOptionId($optionId, $searchs = false) {
        $query = $this->employees->select('id', 'fullname', 'email', 'phone', DB::raw("DATE_FORMAT(birthday, '%d/%m/%Y') as birthday"));
        if ($searchs) {
            $query->where('fullname', $searchs);
        }
        return $query->get();
    }
}
