<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RequiredExpireDate implements Rule
{
    protected $request;
    protected $parameters;

    /**
     * Create a new rule instance.
     *
     * @param $request
     * @param $parameters
     * @return void
     */
    public function __construct($request, $parameters)
    {
        //
        $this->request = $request;
        $this->parameters = $parameters;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
        return $this->request[$this->parameters['field']] != $this->parameters['value'] || !empty($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Trường bắt buộc.';
    }
}
