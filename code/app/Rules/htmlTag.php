<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class htmlTag implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        preg_match_all("/(<([\w]+)[^>]*>)(.*?)(<\/\\2>)/", $value, $matches, PREG_SET_ORDER);
        return !empty($matches) ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute chỉ được nhập các ký tự a-z A-Z 0-9 - . /';
    }
}
