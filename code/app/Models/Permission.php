<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{

    use SoftDeletes;

    protected $table = 'permission';

    public $perPage = 10;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'route_name',
        'name',
        'description'
    ];
    protected $guarded = [
        'id'
    ];
    
    /**
     * A permission will have many users.
     */
    public function users()
    {
        return $this->hasMany('\App\Models\User')->withTimestamps();   
    }
}
