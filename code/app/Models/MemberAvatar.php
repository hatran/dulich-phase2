<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberAvatar extends Model
{
    const CURRENT = 1;

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'memberAvatars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'fileId',
        'current',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'memberId', 'id');
    }

    public function file()
    {
        return $this->belongsTo(File::class, 'fileId', 'id');
    }
}
