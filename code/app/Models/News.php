<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model {

    use SoftDeletes;

    protected $table = 'news';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một tin tức', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một tin tức', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Đánh dấu xóa một tin tức', $record);
        });
    }
}
