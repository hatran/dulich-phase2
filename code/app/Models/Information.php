<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'information';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
        'status',
        'code'
    ];

    public function parent()
    {
        return $this->belongsTo(Information::class, 'parent_id', 'id');
    }

    public static function getAllInformation() {
        return self::where('status', 1)->pluck('name', 'id')->all();
    }

    public static function getAllInformationWithParentNull() {
        return self::where('status', 1)->whereNull('parent_id')->get();
    }
}
