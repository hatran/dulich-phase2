<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = 'notes';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một ghi chú', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một ghi chú', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Đánh dấu xóa một ghi chú', $record);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'user_id',
        'note',
        'function_type',
        'action_type',
        'content',
		'value_old',
		'value_new'
    ];

    public function member()
    {
    	return $this->belongsTo('App\Models\Member');
    }

}
