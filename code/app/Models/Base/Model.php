<?php

namespace App\Models\Base;

class Model extends \Illuminate\Database\Eloquent\Model
{

    protected $table = '';
    public $perPage = 15;
    protected $fillable = [];
    protected $guarded = [];

    public function save(array $options = [])
    {
        return parent::save($options);
    }


}
