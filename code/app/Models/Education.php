<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'educations';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới Education', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật Education', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa Education', $record);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'branchId',
        'degreeId',
        'fileId',
    ];

    public function file()
    {
        return $this->belongsTo(File::class, 'fileId', 'id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'memberId', 'id');
    }

    public function educationBranch()
    {
        return $this->belongsTo(EducationBranch::class, 'branchId', 'id');
    }

    public function educationDegree()
    {
        return $this->belongsTo(EducationDegree::class, 'degreeId', 'id');
    }
}
