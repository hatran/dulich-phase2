<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'works';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'elementName',
        'typeOfContract',
        'fromDate',
        'toDate',
        'status',
    ];

    protected $dates = ['fromDate', 'toDate', 'createdAt', 'updatedAt'];
}
