<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branches extends Model
{

    use SoftDeletes;

    const BRANCHES_ACTIVE = 1;
    const BRANCHES_DEACTIVE = 0;

    public static $statusLabel = [
        self::BRANCHES_ACTIVE => 'Hoạt động',
        self::BRANCHES_DEACTIVE => 'Không hoạt đông'
    ];

    protected $table = 'branches';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới branch', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật branch', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa branch', $record);
        });
    }

    public $perPage = 10;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'address',
        'phone',
        'email',
        'note',
        'parent_id',
        'option_code',
        'status',
        'images',
    ];
    protected $guarded = [
        'id'
    ];
    public function MemberPosition()
    {
        return $this->hasMany('App\Models\MemberPosition', 'branch_id', 'id');
    }
    public function employees()
    {
        return $this->belongsToMany('App\Models\Employees', 'employee_position', 'branch_id', 'employee_id');
    }

    public function options() {
        return $this->belongsToMany('App\Models\Option', 'employee_position', 'branch_id', 'options_code');
    }

    public function getStatusLabel() {
        return isset(self::$statusLabel[$this->status]) ? self::$statusLabel[$this->status] : 'Không hoạt động';
    }

    public static function getBranches03() {
        $branches03 = array();
        $getBranches03 = self::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($getBranches03)){
            foreach($getBranches03 as $key => $value){
                $branches03[$value->id] = $value->name;
            }
        }
        return $branches03;
    }

    public static function getBranches01() {
        $branches01 = array();
        $getBranches01 = self::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($getBranches01)){
            foreach($getBranches01 as $key => $value){
                $branches01[$value->id] = $value->name;
            }
        }
        return $branches01;
    }

    public static function getBranches01Province($id) {
        $branches01 = "";
        $getBranches01 = self::where(function($query) {
            $query->where(function($query) {
                $query->where('option_code', 'BRANCHES01');
            })->orWhere(function($query) {
                $query->where('option_code', 'BRANCHES03');
            });
        })->where('status', 1)->where('id', $id)->pluck('name', 'option_code');
        if(!empty($getBranches01)){
            return $getBranches01;
        }
        return $branches01;
    }

    public static function getBranchesNameByUserProvinceType($province_type) {
        return self::where('id', $province_type)->pluck('name')->all();
    }

    public static  function getBranchesNameById($branch_id){
        return self::where(id, $branch_id)->pluck('name')->first();
    }
}
