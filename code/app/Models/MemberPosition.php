<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberPosition extends Model
{

    use SoftDeletes;
    
    protected $table = 'member_position';

    public $perPage = 15;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id',
        'member_id',
        'deleted_at',
        'created_at',
        'updated_at',
    ];
    protected $guarded = [
        'id'
    ];
}
