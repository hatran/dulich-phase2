<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberRanked extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'member_ranked';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'rank_id',
    ];
}
