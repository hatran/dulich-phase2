<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{

    protected $table = 'options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
    ];
    protected $guarded = [
        'id'
    ];
    
    public static function getCodeByCondititon($condition) {
        return self::where($condition)->first();
    }
}
