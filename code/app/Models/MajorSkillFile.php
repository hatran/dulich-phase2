<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MajorSkillFile extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'majorSkillFiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'fileId',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'memberId', 'id');
    }

    public function file()
    {
        return $this->belongsTo(File::class, 'fileId', 'id');
    }
}
