<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubjectKnowledge extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'subject_knowledge';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
        'code',
        'created_author',
        'knowledge_id',
        'file_name',
        'is_deleted',
        'area'
    ];

    public static function getAllSubjectKnowledge() {
        return self::where('status', 1)->whereNull('is_deleted')->get();
    }

    public static function getAllSubjectKnowledgeIDName() {
        return self::where('status', 1)->whereNull('is_deleted')->pluck('name', 'id')->all();
    }

    public static function getSubjectKnowledgeByKnowledgeID($knowledge_id)
    {
        return self::where('knowledge_id', $knowledge_id)->where('status', 1)->whereNull('is_deleted')->get();
    }

    public static function getSubjectKnowledgeNameIDByKnowledgeID($knowledge_id)
    {
        return self::where('knowledge_id', $knowledge_id)->where('status', 1)->whereNull('is_deleted')->pluck('name', 'id')->all();
    }

    public static function getAllSubjectKnowledgeName() {
        return self::where('status', 1)->pluck('id', 'name')->all();
    }

    public static function checkName($name) {
        $name = trim($name);
        return self::where('name', $name)->where('status', 1)->first();
    }
}
