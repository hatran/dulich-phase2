<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'question';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject_knowledge_id',
        'question_rank',
        'code',
        'content',
        'answer1',
        'answer2',
        'answer3',
        'answer4',
        'answer_correct',
        'status',
        'is_deleted',
        'created_author'
    ];

    public static function getAllQuestion() {
        return self::where('status', 1)->whereNull('is_deleted')->get();
    }

    public static function getQuestionBySubjectKnowledgeID($subject_knowledge_id)
    {
        return self::where('subject_knowledge_id', $subject_knowledge_id)->where('status', 1)->whereNull('is_deleted')->get();
    }
}
