<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class MemberPaymentsYearly extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một hội phí thường niên trong hệ thống', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một hội phí thường niên trong hệ thống', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một hội phí thường niên trong hệ thống', $record);
        });
    }

    protected $table = 'member_payments_yearly';

    protected $fillable = [
        'number_payment',
        'date_payment',
        'is_delete',
        'memberId',
        'currency',
        'currency_type',
        'payment_method',
        'payment_content',
        'note',
        'year',
        'flag',
        'number_of_year',
        'member_code_expiration'
    ];

    protected $dates = [ 'created_at', 'updated_at', 'deleted_at', 'date_payment', 'member_code_expiration'];

    public function getDatePaymentAttribute($value) {
        if (empty($value)) {
            return '';
        } else {
            return date('d/m/Y', strtotime($value));
        }
    }

    /*public function getCurrencyAttribute($value) {
        if (empty($value)) {
            return '';
        } elseif (! is_numeric($value)) {
            return $value;
        } else {
            return number_format($value, 0, ',', '.');
        }
    }*/

    /*public function getFormattedCurrencyAttribute() {
        if (empty($this->currency)) {
            return '';
        } elseif (!is_numeric($this->currency)) {
            return $this->currency;
        } else {
            return number_format($this->currency, 0, '.', ',');
        }
    }*/

    public function decision ()
    {
        return $this->hasMany('App\Models\MemberDecision', 'memberId', 'memberId');
    }

    public function member ()
    {
        return $this->hasOne(Member::class, 'id', 'memberId');
    }
}
