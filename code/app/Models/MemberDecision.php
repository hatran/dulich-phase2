<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class MemberDecision extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETE_AT = 'deleteAt';

    protected $table = 'member_decision_file';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một quyết định', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một quyết định', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một quyết định', $record);
        });
    }

    protected $fillable = [
        'memberId',
        'number_decisive',
        'leaderId',
        'sign_date',
        'fileId',
        'is_signed',
        'is_delete',
        'is_print',
    ];

    protected $dates = [ 'createdAt', 'updatedAt', 'deleteAt', 'date_payment'];

    public function file ()
    {
        return $this->hasMany('App\Models\File', 'id', 'fileId');
    }
}
