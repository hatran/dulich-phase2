<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class WorkConstant extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'workConstants';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một Danh mục hình thức lao động trong hệ thống', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một Danh mục hình thức lao động trong hệ thống', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một Danh mục hình thức lao động trong hệ thống', $record);
        });
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workName',
        'status',
        'code',
    ];

    protected $dates = ['createdAt', 'updatedAt'];
}
