<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'rank';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
        'code'
    ];

    public function members()
    {
        return $this->belongsToMany(Member::class);
    }

    public static function getAllRanks() {
        return self::query()->where('status', 1)->pluck('name', 'id')->all();
    }

    public static function getAllRanksByCode($code) {
        return self::query()->where('status', 1)
            ->where('code', 'like', '%'.$code.'%')
            ->pluck('name', 'id')
            ->all();
    }

    public static function getRanksExceptZero() {
        return self::query()->where('status', 1)->where('code', '<>', 'HDVZ0')->pluck('name', 'id')->all();
    }

    public static function getAllRanksForNotMember() {
        return self::query()->where('status', 1)
            ->where('code', 'like', '%HDVQT%')
            ->orWhere('code', 'like', '%HDVND%')
            ->pluck('name', 'id')
            ->all();
    }
}
