<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable {

    use Notifiable;
    use SoftDeletes;

    const FULLNAME = 'fullname';
    const USERNAME = 'username';
    const EMAIL = 'email';
    const PWD_INPUT = 'password';
    const STATUS = 'status';
    const ROLE = 'role';
    const PROVINCE_TYPE = 'province_type';
    const MEMBER_ID = 'memberId';
    const INVALID_ATTR = ':attribute không hợp lệ';

    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một người dùng', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một người dùng', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một người dùng', $record);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FULLNAME, self::USERNAME, self::EMAIL, self::PWD_INPUT, self::ROLE, self::STATUS, self::PROVINCE_TYPE, self::MEMBER_ID, 'branch_id', 'is_traveler_company'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PWD_INPUT, 'remember_token',
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    public static $rule = [
        self::FULLNAME => 'required|string|max:255',
        self::USERNAME => 'required|string|max:255|unique:users,username,NULL,id,deleted_at,NULL',
        self::EMAIL => 'required|string|email|max:255',
        // 'role' => 'required|in:1,2,3,4,5,61,62,63,71,72,73,8,9',
        self::STATUS => 'required|integer|in:0,1',
        self::PWD_INPUT => 'required|string|min:8|regex:/^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8,}$/',
		// 'password_confirmation' => 'required|string|min:8|same:' . self::PWD_INPUT,
        'password_confirmation' => 'required|same:' . self::PWD_INPUT,
        // self::PROVINCE_TYPE => 'integer|in:1,2,3,-1',
        'role' => 'required'
    ];
    public static $message = [
        'required' => ':attribute là trường bắt buộc phải điền',
        'role.required' => 'Vai trò là trường bắt buộc phải điền',
        'string' => ':attribute phải là một đoạn văn bản',
        'branch_id.required' => 'Bạn phải chọn Chi Hội hoặc CLB Thuộc Hội',
        self::PWD_INPUT . '.string' => ':attribute quá yếu, mật khẩu phải theo chuẩn sau: Mật khẩu có độ dài tối thiểu 8 ký tự bao gồm ít nhất 2 ký tự viết hoa, 1 ký tự đặc biệt, 2 chữ số và 3 ký tự thường',
        self::PWD_INPUT. '.min' => ':attribute quá yếu, mật khẩu phải theo chuẩn sau: Mật khẩu có độ dài tối thiểu 8 ký tự bao gồm ít nhất 2 ký tự viết hoa, 1 ký tự đặc biệt, 2 chữ số và 3 ký tự thường',
        // 'password_confirmation.string' => ':attribute quá yếu, mật khẩu phải theo chuẩn sau: Mật khẩu có độ dài tối thiểu 8 ký tự bao gồm ít nhất 2 ký tự viết hoa, 1 ký tự đặc biệt, 2 chữ số và 3 ký tự thường',
        // 'password_confirmation.min' => ':attribute quá yếu, mật khẩu phải theo chuẩn sau: Mật khẩu có độ dài tối thiểu 8 ký tự bao gồm ít nhất 2 ký tự viết hoa, 1 ký tự đặc biệt, 2 chữ số và 3 ký tự thường',
        'max' => 'Độ dài tối đa của :attribute là :max kí tự',
        'min' => 'Độ dài tối thiểu của :attribute là :min kí tự',
        'unique' => ':attribute đã được sử dụng',
        'regex' => ':attribute quá yếu, mật khẩu phải theo chuẩn sau: Mật khẩu có độ dài tối thiểu 8 ký tự bao gồm ít nhất 2 ký tự viết hoa, 1 ký tự đặc biệt, 2 chữ số và 3 ký tự thường',
        self::EMAIL => self::INVALID_ATTR,
        'in' => self::INVALID_ATTR,
        'integer' => self::INVALID_ATTR,
        //'confirmed' => 'Xác nhận mật khẩu phải giống với Mật Khẩu đã nhập',
        'same' => 'Xác nhận mật khẩu phải giống với Mật Khẩu đã nhập',
    ];
    public static $niceAttributeName = [
        self::FULLNAME => 'Họ và tên',
        self::USERNAME => 'Tên đăng nhập',
        self::EMAIL => 'Email',
        'role' => 'Quyền truy cập',
        self::PWD_INPUT => 'Mật khẩu',
        self::PROVINCE_TYPE => 'Hội - VPĐD',
        self::STATUS => 'Trạng thái người dùng',
        'branch_id' => 'Chi Hội',
        // 'club_id' => 'CLB Thuộc Hội',
		'password_confirmation' => 'Xác nhận mật khẩu',
        'old_password' => 'Mật khẩu cũ'
    ];

    public function hasPermissions($permissions)
    {
        try {
            // group
            $groupsDetail = Groups::select('group_role_id')->where('code', $this->role)->first();
            if (empty($groupsDetail) || empty($groupsDetail->group_role_id)){
                return false;
            }
            $userPermissions = GroupsRoles::whereIn('id', json_decode($groupsDetail->group_role_id))->pluck('permissions')->all();
            $userPermissions = array_map('json_decode', $userPermissions);
            $userRoles = [];
            foreach ($userPermissions as $val) {
                $userRoles = array_merge($userRoles, $val);
            }
            if (empty($userRoles)) {
                $userRoles = [];
            }
            // custom permission
            /*$customPermission = PermissionUser::where('user_id', $this->id)->pluck('permission_route_name')->all();
            if (!empty($customPermission)) {
                $userPermissions = array_merge($userRoles, $customPermission);
            }*/
            // If all are not required, check that the user has at least 1.
            return in_array($permissions, $userRoles);
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function canSee ($arrPermissions)
    {
        foreach ($arrPermissions as $permissions) {
            if ($this->hasPermissions($permissions)) {
                return true;
            }
        }

        return false;
    }

    public function getFirstRouteAccess ()
    {
        /*$firstRoute = '';
        // group
        $groupsDetail = Groups::select('group_role_id')->where('code', $this->role)->first();
        if (empty($groupsDetail) || empty($groupsDetail->group_role_id)){
            return false;
        }
        $userPermissions = GroupsRoles::whereIn('id', json_decode($groupsDetail->group_role_id))->pluck('permissions')->all();
        $userPermissions = array_map('json_decode', $userPermissions);
        $userRoles = [];
        foreach ($userPermissions as $val) {
            $userRoles = array_merge($userRoles, $val);
        }
        if (empty($userRoles)) {
            $userRoles = [];
        }*/
        // custom permission
        /*$customPermission = PermissionUser::where('user_id', $this->id)->pluck('permission_route_name')->all();
        if (!empty($customPermission)) {
            $userPermissions = array_merge($userRoles, $customPermission);
        }*/

        /*if (!empty($userRoles)) {
            $firstRoute = array_get($userRoles, 0, '');
        }

        return $firstRoute;*/

        return 'admin_user_profile';
    }

    public function group ()
    {
        return $this->hasOne(Groups::class, 'code', 'role');
    }

    public function branch ()
    {
        return $this->hasOne(Branches::class, 'code', self::PROVINCE_TYPE)->whereNull('parent_id');
    }

    public function sendPasswordResetNotification($token) {
        $this->notify(new ResetPasswordNotification($token));
    }
}
