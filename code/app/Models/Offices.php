<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class Offices extends Model
{
    protected $table = 'branches';
//    protected $table = 'offices';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory(trans('history.fn_create_new_employee'), $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory(trans('history.fn_update_employee'), $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory(trans('history.fn_delete_employee'), $record);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'address',
        'phone',
        'email',
        'payment_info',
        'note',
        'status',
        'area'
    ];

    public static function searchByAllOffices()
    {
        $getAllOffices = self::whereIn('option_code',['HEAD','OFFICES'])->where('status',1)->whereNull('parent_id')->whereNull('deleted_at')->get();
        return $getAllOffices;
    }

    public static function getProvinceCodeByProvinceType($parent_id, $orderby='name')
    {
        if ($parent_id == null)
        {
            $getProvinceCode = self::where('status',1)->whereNull('deleted_at')->where(function ($query) {
                    $query->where('option_code','BRANCHES01')
                        ->orWhere('option_code', 'BRANCHES03');
                })->orderBy($orderby)->get();
        }
        else
        {
            $getProvinceCode = self::where('status',1)->where('parent_id', $parent_id)->whereNull('deleted_at')->get(); 
        }
        return $getProvinceCode;
    }

    public static function getProvinceCodeByArea($parent_id)
    {
        $getProvinceCode = self::whereNotNull('parent_id')->where('status',1)->where('parent_id', $parent_id)->whereNull('deleted_at')->get();
        $getBranch = self::where('status',1)->where('id', $parent_id)->whereNull('deleted_at')->first();
        if ($getBranch->area != null) {
            if ($getBranch->area = 'HN') {
                $getClub = self::whereNull('parent_id')->where('status',1)->where('option_code', 'BRANCHES03')->whereNull('deleted_at')->get();
                if (!empty($getProvinceCode) && !empty($getClub)) {
                    $getProvinceCode = array_merge($getProvinceCode->toArray(), $getClub->toArray());
                }
            }
        }
        return $getProvinceCode; 
    }

    public static function getBranchesByArea($area, array $pluckBy = [])
    {
        $getBranch = self::where('area', $area)->where('status', 1)->whereNull('deleted_at')->where(function ($query) {
                $query->where('option_code', 'OFFICES')
                    ->orWhere('option_code', 'HEAD');
            });

        if (!empty($pluckBy) && is_array($pluckBy)) {
            $pluckBy = implode(',', $pluckBy);
            $getBranch = $getBranch->pluck($pluckBy)->all();
        } else {
            $getBranch = $getBranch->get();
        }

        return $getBranch;
    }

    public static function getProvinceNameByProvinceCode($code) {
        $province = self::select('name')->where('id', $code)->first();
        if (empty($province)) {
            return null;
        }
        return $province->name;
    }
}
