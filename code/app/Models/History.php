<?php

namespace App\Models;

use App\Models\Base\Model;

class History extends Model
{

    protected $table = 'history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'user_id',
        'ip',
        'description'
    ];


    protected $guarded = [
        'id'
    ];
}
