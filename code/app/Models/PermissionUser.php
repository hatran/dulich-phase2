<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermissionUser extends Model
{

    use SoftDeletes;

    protected $table = 'permission_user';

    public $perPage = 10;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'permission_route_name',
        'user_id',
    ];
    protected $guarded = [
        'id'
    ];

    /**
     * A permission will have many users.
     */
    public function users()
    {
        return $this->belongsToMany('\App\Models\User');
    }
}
