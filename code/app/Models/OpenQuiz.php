<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OpenQuiz extends Model
{
    protected $table = 'open_quiz';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id', 'open',
    ];

    public $timestamps = false;
}
