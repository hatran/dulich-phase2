<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberPrintCardSelected extends Model
{
    protected $table = 'member_print_card_selected';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'member_id',
        'print_card_type'
    ];
}
