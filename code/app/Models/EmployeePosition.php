<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class EmployeePosition extends Model
{
    protected $table = 'employee_position';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới employee_position', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật employee_position', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa employee_position', $record);
        });
    }

    protected $fillable = [
        'id',
        'branch_id',
        'employee_id',
        'option_code',
        'deleted_at',
        'created_at',
        'updated_at'
    ];


}
