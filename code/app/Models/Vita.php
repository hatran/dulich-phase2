<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vita extends Model
{
    protected $table = 'vita';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'type',
        'name',
        'position',
        'company',
        'address',
        'department',
    ];
}
