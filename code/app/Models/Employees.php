<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employees extends Model
{

    use SoftDeletes;

    protected $table = 'employees';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới Ban chấp hành VPĐD', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật Ban chấp hành VPĐD', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa Ban chấp hành VPĐD', $record);
        });
    }

    public $perPage = 15;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname',
        'profile_image',
        'birthday',
        'gender',
        'option_code',
        'email',
        'phone',
        'address',
        'company',
        'deleted_at',
        'status'
    ];
    protected $guarded = [
        'id'
    ];

    public function getBirthdayAttribute($value)
    {
        $date = date('Y-m-d', strtotime($value));
        $tempDate = explode('-', $date);
        if(checkdate($tempDate[1], $tempDate[2], $tempDate[0]) && $date != '1970-01-01') {
            return date('d/m/Y', strtotime($value));
        }
        
        return $value;
    }
    public function branches() {
        return $this->belongsToMany('App\Models\Branches', 'employee_position', 'employee_id', 'branch_id');
    }

    public function options() {
        return $this->belongsToMany('App\Models\Option', 'employee_position', 'employee_id', 'options_code');
    }
}
