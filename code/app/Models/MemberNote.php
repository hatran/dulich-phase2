<?php
namespace App\Models;
                                                                                                            
use Illuminate\Database\Eloquent\Model;
use App\Models\Rank;

class MemberNote extends Model
{

    protected $primaryKey = 'id';

    protected $table = 'member_note';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'author_id',
        'content',
    ];

    public static function getAuthorName($author_id)
    {
        $user = User::where('id', '=', $author_id)
            ->first();
        return $user->fullname ?? null;
    }
}
