<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberTmp extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const GUIDE_LANGUAGE = 'guideLanguage';

    protected $table = 'members_tmp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'content',
    ];

    public static $guideLanguage = self::GUIDE_LANGUAGE;

    protected $dates = ['birthday', 'dateIssued', 'expirationDate',
        'createdAt', 'updatedAt', 'member_from', 'verified_at', 'approved_at', 'sms_exprired_at','deleteAt'];

    public function getElementsAttribute()
    {
        $memberElements = Element::where('memberId', $this->id)->first();

        return empty($memberElements) ? null : $memberElements;
    }
}
