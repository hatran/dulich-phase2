<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'options';


    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            if(isset($record->key) && $record->key == 'provincial'){
                LogServiceProvider::createSystemHistory('Tạo mới một Danh mục tỉnh thành trong hệ thống', $record);
            }elseif(isset($record->key) && $record->key == 'position'){
                LogServiceProvider::createSystemHistory('Tạo mới một Danh mục chức vụ trong hệ thống', $record);
            }elseif(isset($record->key) && $record->key == 'club'){
                LogServiceProvider::createSystemHistory('Tạo mới một Danh mục câu lạc bộ trong hệ thống', $record);
            }
        });

        static::updating(function($record) {
            if(isset($record->key) && $record->key == 'provincial'){
                LogServiceProvider::createSystemHistory('Cập nhật một Danh mục tỉnh thành trong hệ thống', $record);
            }elseif(isset($record->key) && $record->key == 'position'){
                LogServiceProvider::createSystemHistory('Cập nhật một Danh mục chức vụ trong hệ thống', $record);
            }elseif(isset($record->key) && $record->key == 'club'){
                LogServiceProvider::createSystemHistory('Cập nhật một Danh mục câu lạc bộ trong hệ thống', $record);
            }
        });

        static::deleting(function($record) {
            if(isset($record->key) && $record->key == 'provincial'){
                LogServiceProvider::createSystemHistory('Xóa một Danh mục tỉnh thành trong hệ thống', $record);
            }elseif(isset($record->key) && $record->key == 'position'){
                LogServiceProvider::createSystemHistory('Xóa một Danh mục chức vụ trong hệ thống', $record);
            }elseif(isset($record->key) && $record->key == 'club'){
                LogServiceProvider::createSystemHistory('Xóa một Danh mục câu lạc bộ trong hệ thống', $record);
            }
        });
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'status',
        'code',
        'value',
    ];

    protected $dates = ['created_at', 'updated_at'];
}
