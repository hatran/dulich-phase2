<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelerCompanyRank extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'traveler_company_rank';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tax_code',
        'company_name',
        'address',
        'email',
        'phone',
        'business_certificate',
        'created_author',
        'is_deleted',
        'status'
    ];

    public static function getAllTravelerCompanyRank() {
        return self::where('status', 1)->whereNull('is_deleted')->get();
    }
}
