<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    use Notifiable;
    use SoftDeletes;

    const TITLE         = 'title';
    const OPTION_CODE   = 'option_code';
    const CONTENT       = 'content';
    const STATUS        = 'status';
    const INVALID_ATTR  = ':attribute không hợp lệ';
    const ATTACH        = 'attach';

    protected $table = 'emails';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps  = true;
    protected $fillable = [
        'title',
        'content',
        'status',
        'option_code',
        'branch_id',
        'end_time',
        'start_time',
        'auto_send_time',
        'attach'
    ];

    protected $dates = ['start_time', 'end_time', 'created_at', 'updated_at', 'deleted_at'];

    public static $rule = [
        self::TITLE         => 'required|max:500',
        self::OPTION_CODE   => 'required|max:255',
        self::CONTENT       => 'required',
        self::STATUS        => 'required|integer|in:0,1',
        self::ATTACH        => 'mimes:pdf'
    ];

    public static $message = [
        'required'  => ':attribute là trường bắt buộc phải điền',
        'string'    => ':attribute phải là một đoạn văn bản',
        'max'       => 'Độ dài tối đa của :attribute là :max kí tự',
        'min'       => 'Độ dài tối thiểu của :attribute là :min kí tự',
        'unique'    => ':attribute đã được sử dụng',
        'regex'     => ':attribute quá yếu, mật khẩu phải theo chuẩn sau: Mật khẩu có độ dài tối thiểu 8 ký tự bao gồm ít nhất 2 ký tự viết hoa, 1 ký tự đặc biệt, 2 chữ số và 3 ký tự thường',
        'in'        => self::INVALID_ATTR,
        'integer'   => self::INVALID_ATTR,
        'mimes'     => 'Yêu cầu nhập :attribute có định dạng là pdf'
    ];

    public static $niceAttributeName = [
        self::TITLE         => 'Tiêu đề',
        self::OPTION_CODE   => 'Loại email',
        self::CONTENT       => 'Nội dung',
        self::STATUS        => 'Trạng thái',
        self::ATTACH        => 'File đính kèm'
    ];
}
