<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'elements';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới elements', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật elements', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa elements', $record);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'typeGuideId',
        'groupSizeId',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'memberId', 'id');
    }

    public function typeGuide()
    {
        return $this->belongsTo(TypeGuide::class, 'typeGuideId', 'id');
    }

    public function groupSize()
    {
        return $this->belongsTo(GroupSize::class, 'groupSizeId', 'id');
    }
}
