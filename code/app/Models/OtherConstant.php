<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class OtherConstant extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'otherConstants';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một Danh mục số năm kinh nghiệm trong hệ thống', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một Danh mục số năm kinh nghiệm trong hệ thống', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một Danh mục số năm kinh nghiệm trong hệ thống', $record);
        });
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'otherName',
        'status',
        'code',
    ];

    protected $dates = ['createdAt', 'updatedAt'];
}
