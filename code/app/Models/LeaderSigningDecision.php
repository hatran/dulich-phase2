<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class LeaderSigningDecision extends Model
{
    //
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETE_AT = 'deleteAt';

    protected $table = 'leader_signing_decision';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một Danh mục người ký quyết định trong hệ thống', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một Danh mục người ký quyết định trong hệ thống', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một Danh mục người ký quyết định trong hệ thống', $record);
        });
    }


    protected $fillable = [
        'fullname',
        'status',
        'position',
        'is_delete',
        'code',
    ];
    protected $dates = [ 'createdAt', 'updatedAt', 'deleteAt', 'date_payment'];

}
