<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeesPosition extends Model
{

    use SoftDeletes;

    protected $table = 'employee_position';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới employee_position', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật employee_position', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa employee_position', $record);
        });
    }

    public $perPage = 15;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id',
        'branch',
        'employee_id',
        'option_code',
        'deleted_at',
        'created_at',
        'updated_at',
    ];
    protected $guarded = [
        'id'
    ];

    protected $dates = ['deleted_at'];

    public function employees() {
        return $this->hasOne('App\Models\Employees', 'id', 'employee_id');
    }
}
