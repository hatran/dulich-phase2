<?php

namespace App\Models;

use App\Models\Base\Model;

class HistoryEmailSMS extends Model
{

    protected $table = 'history_email_sms';

    const EMAIL_LOG_TYPE = 1;
    const SMS_LOG_TYPE   = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'email_sms_to',
        'email_sms_name',
        'subject',
        'content',
        'user_id',
        'ip',
    ];


    protected $guarded = [
        'id'
    ];
}
