<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Printed extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'printed';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'cardType',
    ];
}
