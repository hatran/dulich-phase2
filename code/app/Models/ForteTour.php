<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForteTour extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'forte_tour';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
        'type',
        'code',
        'status'
    ];

    public function parent()
    {
        return $this->belongsTo(ForteTour::class, 'parent_id', 'id');
    }

    public static function getAllForteTour() {
        return self::where('status', 1)->pluck('name', 'id')->all();
    }

    public static function getForteTourName($forteTour) {
        $forteTourName = "";
        $forteTours = self::getAllForteTour();
        foreach (explode(",", $forteTour) as $key => $lang) {
            foreach ($forteTours as $data => $val) {
                if ($data == $lang) {
                    $forteTourName .= $val .",";
                }
            }
        }
        return rtrim($forteTourName, ",");
    }
}
