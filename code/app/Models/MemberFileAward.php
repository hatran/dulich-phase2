<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberFileAward extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'member_file_award';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'rank_id',
        'score_file',
        'score_award',
    ];
}
