<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'languages';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một Danh mục chuyên ngành ngoại ngữ trong hệ thống', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một Danh mục chuyên ngành ngoại ngữ trong hệ thống', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một Danh mục chuyên ngành ngoại ngữ trong hệ thống', $record);
        });
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'languageName',
        'status',
        'position',
        'code',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'id', 'guideLanguage');
    }

    public static function getAllLanguages() {
        return self::where('status', 1)->pluck('languageName', 'id')->all();
    }

    public static function getLanguageName($language) {
        $languageName = "";
        $languages = self::getAllLanguages();
        foreach (explode(",", $language) as $key => $lang) {
            foreach ($languages as $data => $val) {
                if ($data == $lang) {
                    $languageName .= $val .",";
                }
            }
        }
        return rtrim($languageName, ",");
        // return self::where('id', $id)->first();
    }
}
