<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    protected $table = 'pages';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới page', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật page', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa page', $record);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
}
