<?php
namespace App\Models;
                                                                                                            
use Illuminate\Database\Eloquent\Model;
use App\Models\Rank;

class MemberRank extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $primaryKey = 'id';

    protected $table = 'member_rank';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'rank_id',
        'base_question',
        'subject_question',
        'score',
        'join_date',
        'is_deleted'
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id', 'id');
    }

    public function rank()
    {
        return $this->belongsTo(Rank::class, 'rank_id', 'id');
    } 

    public static function getRankByMemberId($id)
    {
        return self::query()->select('id', 'rank_id', 'member_id')->where('member_id', $id)->where('is_deleted', 0)->orderBy(self::CREATED_AT, 'DESC')->first();
    }

    public static function getRankByMemberIdAndRankId($memberId, $rankId)
    {
        return self::query()
            ->where('member_id', $memberId)
            ->where('rank_id', $rankId)
            ->where('is_deleted', 0)
            ->orderBy(self::CREATED_AT, 'DESC')
            ->exists();
    }

    public static function getAllRanks()
    {
        return self::query()->where('is_deleted', 0)->orderBy(self::CREATED_AT, 'DESC')->pluck('rank_id', 'member_id')->all();
    }

    public static function getLatestRankByMemberId($memberId)
    {
        return self::query()
            ->select('rank_id', 'member_id')
            ->where('member_id', $memberId)
            ->where('is_deleted', 0)
            ->whereNotNull('score')
            ->orderByDesc(self::CREATED_AT)
            ->first();
    }

    public static function getRankNameByMemberId($id)
    {
        $rank = self::query()->select('rank.name')
            ->join('rank', 'member_rank.rank_id', '=', 'rank.id')
            ->where('member_rank.member_id', $id)
            ->where('member_rank.is_deleted', 0)
            ->orderByDesc('member_rank.'.self::CREATED_AT)
            ->first();

        return $rank ? $rank->name : '';
    }

    public static function getRankIdAndNameByMemberId($id)
    {
        return self::query()
            ->select('rank.name', 'rank.id')
            ->join('rank', 'member_rank.rank_id', '=', 'rank.id')
            ->where('member_rank.member_id', $id)
            ->where('member_rank.is_deleted', 0)
            ->orderBy('member_rank.'.self::CREATED_AT, 'DESC')
            ->first();
    }

    public static function getRankIDByMemberId($id) {
        return self::query()->select('rank_id')->where('member_id', $id)->where('is_deleted', 0)->orderBy(self::CREATED_AT, 'DESC')->first();
    }

    public static function getInfoRankByMemberId($id) {
        return self::query()->where('member_id', $id)->where('is_deleted', 0)->orderBy(self::CREATED_AT, 'DESC')->first();
    }

    public static function getAllByJoinDate($id, $rankId) {
        return self::query()
            ->where('member_id', $id)
            ->where('rank_id', $rankId)
            ->whereNotNull('join_date')
            ->where('is_deleted', 0)
            ->get();
    }

    public static function deleleJoinDateNull($id) {
        self::query()->where('member_id', $id)->whereNull('join_date')->where('is_deleted', 0)->delete();
    }

    public static function fetchInfoRankByMemberId($id) {
        return self::query()
            ->select('member_rank.*', 'rank.name', 'member_file_award.score_file', 'member_file_award.score_award')
            ->join('rank', 'member_rank.rank_id', '=', 'rank.id')
            ->leftJoin('member_file_award', function ($join) {
                $join->on('member_rank.member_id', '=', 'member_file_award.member_id')
                    ->on('member_rank.rank_id', '=', 'member_file_award.rank_id');
            })
            ->where('member_rank.member_id', $id)
            ->where('member_rank.is_deleted', 0)
            ->orderBy('member_rank.'.self::CREATED_AT, 'DESC')
            ->first();
    }
}
