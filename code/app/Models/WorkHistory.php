<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkHistory extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'workHistory';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'elementId',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'memberId', 'id');
    }

    public function work()
    {
        return $this->belongsTo(Work::class, 'elementId', 'id');
    }
}
