<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerTmp extends Model
{
    protected $table = 'answer_tmp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id',
        'member_id',
        'answer',
    ];

    public $timestamps = false;

    public static function getAllAnswerByMemberID($id) {
        return self::where('member_id', $id)->get();
    }

    public static function deleteAllAnswerByMemberID($id)
    {
        return self::where('member_id', $id)->delete();
    }
}
