<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'files';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Một file mới được tải lên hệ thống', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Một file được cập nhật trên hệ thống', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Một file được xóa trên hệ thống', $record);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'originalName',
        'absPath',
        'relativeUrl',
        'mime',
        'size',
        'status',
    ];
}
