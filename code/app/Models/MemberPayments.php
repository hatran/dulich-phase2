<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class MemberPayments extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const DELETE_AT  = 'deleteAt';

    protected $table = 'member_payments';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một hội phí trong hệ thống', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một hội phí trong hệ thống', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một hội phí trong hệ thống', $record);
        });
    }

    protected $fillable = [
        'number_payment',
        'date_payment',
        'is_delete',
        'memberId',
        'currency',
        'currency_type',
        'payment_method',
        'payment_content',
        'note',
    ];

    protected $dates = [ 'createdAt', 'updatedAt', 'deleteAt', 'date_payment'];

    public function getDatePaymentAttribute($value) {
    	return date('d/m/Y', strtotime($value));
    }

    public function getFormattedCurrencyAttribute() {
        if (strpos($this->currency, '.') !== false || strpos($this->currency, ',') !== false) 
        {
            return $this->currency;
        }
    	return number_format($this->currency, 0, '.', ',');
    }

    public function decision ()
    {
        return $this->hasMany('App\Models\MemberDecision', 'memberId', 'memberId');
    }
}
