<?php

namespace App\Models;

use App\Constants\MemberConstants;
use App\Providers\LogServiceProvider;
use App\Scopes\FilterBranchScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Member extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    const GUIDE_LANGUAGE = 'guideLanguage';

    protected $table = 'members';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới một member trong hệ thống', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật một member trong hệ thống', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa một member trong hệ thống', $record);
        });

        static::addGlobalScope(new FilterBranchScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullName',
        'profileImg',
        'birthday',
        'gender',
        'touristGuideCode',
        'expirationDate',
        'cmtCccd',
        'dateIssued',
        'issuedBy',
        'permanentAddress',
        'address',
        'firstMobile',
        'secondMobile',
        'firstEmail',
        'secondEmail',
        'typeOfTravelGuide',
        'typeOfPlace',
        'experienceYear',
        'experienceLevel',
        'otherSkills',
        'otherInformation',
        'touristGuideLevel',
        'achievements',
        'emailVerified',
        'emailToken',
        'phoneVerified',
        'phoneToken',
        'acceptTermsAndPolicies',
        'status',
        'province_code',
        'is_verified',
        'is_fee',
        'is_signed',
        'member_type',
        'member_code',
        'member_qr_id',
        'member_from',
        self::GUIDE_LANGUAGE,
        'verified_by',
        'verified_at',
        'approved_by',
        'approved_at',
        'file_code',
        'inboundOutbound',
        'sms_exprired_at',
        'is_delete',
        'initial_stock',
        'province',
        'pending_status',
        'forteTour',
        'flg_status',
        'member_code_expiration'
    ];

    public static $guideLanguage = self::GUIDE_LANGUAGE;

    protected $dates = ['birthday', 'dateIssued', 'expirationDate',
        'createdAt', 'updatedAt', 'member_from', 'verified_at', 'approved_at', 'sms_exprired_at','deleteAt'];

    public function getAvatarAttribute()
    {
        $memberAvatar = MemberAvatar::where('memberId', $this->id)
            ->where('current', MemberAvatar::CURRENT)
            ->orderBy('createdAt', 'desc')
            ->first();

        return $memberAvatar->file ?? null;
    }

    public function getFormattedBirthdayAttribute() {
        if (empty($this->birthday)) {
            return '';
        }
        return date('d/m/Y', strtotime($this->birthday));
    }

    public function getGuideLanguageNameAttribute()
    {
        $memberGuideLanguage = Language::where('id', $this->attributes[$this::GUIDE_LANGUAGE])->first();

        return $memberGuideLanguage->languageName ?? null;
    }

    public function getElementsAttribute()
    {
        $memberElements = Element::where('memberId', $this->id)->first();

        return empty($memberElements) ? null : $memberElements;
    }

    public function getEducationBranchIdAttribute()
    {
        $memberEducations = Education::where('memberId', $this->id)->first();

        return $memberEducations->branchId ?? null;
    }

    public function getEducationDegreeIdAttribute()
    {
        $memberEducations = Education::where('memberId', $this->id)->first();

        return $memberEducations->degreeId ?? null;
    }

    public function getMajorIdAttribute ()
    {
        $memberMajorSkill = MajorSkill::where('memberId', $this->id)->first();

        return $memberMajorSkill->majorId ?? null;
    }

    public function getLanguageIdAttribute ()
    {
        $memberLanguageSkill = LanguageSkill::where('memberId', $this->id)->first();

        return $memberLanguageSkill->languageId ?? null;
    }

    public function getLevelIdAttribute ()
    {
        $memberLanguageSkill = LanguageSkill::where('memberId', $this->id)->first();

        return $memberLanguageSkill->levelId ?? null;
    }

    public function getGroupSizeIdAttribute ()
    {
        $memberElement = Element::where('memberId', $this->id)->first();

        return $memberElement->groupSizeId ?? null;
    }

    public function getTypeGuideIdAttribute ()
    {
        $memberElement = Element::where('memberId', $this->id)->first();

        return $memberElement->typeGuideId ?? null;
    }

    public function getWorksAttribute ()
    {
        $memberWorkHistory = WorkHistory::where('memberId', $this->id)->get();

        $arrMemberWorks = [];

        foreach ($memberWorkHistory as $memberWorks) {
            if (empty($memberWorks->elementId)) {
                $arrMemberWorks[] = null;
            } else {
                $getWork = Work::where('id', $memberWorks->elementId)->first();
                if ($getWork) {
                    $arrMemberWorks[] = Work::where('id', $memberWorks->elementId)->first();
                }
            }
        }
        
        return $arrMemberWorks;
    }

    public function getRanksAttribute()
    {
        $memberRank = MemberRank::where('member_id', $this->id)->where('is_deleted', 0)->get();

        $arrMemberRank = [];

        foreach ($memberRank as $memRank) {
            if (empty($memRank->rank_id)) {
                $arrMemberRank[] = null;
            } else {
                $getRank = Rank::where('id', $memRank->rank_id)->first();
                if ($getRank) {
                    $arrMemberRank[] = Rank::where('id', $memRank->rank_id)->first();
                }
            }
        }

        return $arrMemberRank;
    }
    
    public function getMemberFromAttribute($value)
    {
        if(!is_null($value)) {
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }
    }

    public function notes()
    {
        return $this->hasMany('App\Models\Note');
    }

    public function decision ()
    {
        return $this->hasOne('App\Models\MemberDecision');
    }

    public function works()
    {
        return $this->belongsToMany('App\Models\Work', 'workHistory', 'memberId', 'elementId');
    }

    public function member_payments_yearly()
    {
        return $this->hasMany(MemberPaymentsYearly::class, 'memberId', 'id');
    }

    public function ranks()
    {
        return $this->belongsToMany(Rank::class, 'member_rank', 'member_id', 'rank_id');
    }

    public static function checkRankConditionMember($memberId, $rankId) {
        return self::query()
            ->whereExists(function ($query) use ($memberId, $rankId) {
            $query->select(DB::raw(1))
                ->from('member_evaluation')
                ->where('member_evaluation.member_id', '=', $memberId)
                ->where('member_evaluation.rank_id', '=', $rankId);
            })
            ->whereNull('is_delete')
            ->where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)
            ->where('rank_status', MemberConstants::RANK_RANKED)
            ->where('id', $memberId)
            ->exists();
    }
}
