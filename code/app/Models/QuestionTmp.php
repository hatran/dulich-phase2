<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionTmp extends Model
{
    protected $table = 'question_tmp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id',
        'subject_knowledge_id',
        'member_id',
        'content',
        'answer1',
        'answer2',
        'answer3',
        'answer4',
        'answer_correct',
        'created_at'
    ];

    public $timestamps = false;

    public static function getAllQuestionByMemberID($id) {
        return self::where('member_id', $id)->get();
    }

    public static function deleteAllQuestionByMemberID($id)
    {
        return self::where('member_id', $id)->delete();
    }
}
