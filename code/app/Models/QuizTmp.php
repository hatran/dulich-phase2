<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizTmp extends Model
{
    protected $table = 'quiz_tmp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'start_time',
        'join_time',
        'current_page'
    ];

    public $timestamps = false;

    public static function getTimeByMemberID($id) {
        return self::where('member_id', $id)->first();
    }

    public static function deleteTimeByMemberID($id)
    {
        return self::where('member_id', $id)->delete();
    }
}
