<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Knowledge extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const STATUS_VALID = 1;

    protected $table = 'knowledge';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'status',
        'code'
    ];

    private static function queryStatusValid() {
        return self::query()->where('status', self::STATUS_VALID);
    }

    public static function getAllKnowledge() {
        return self::queryStatusValid()->pluck('name', 'id')->all();
    }

    public static function getAllKnowledgeName() {
        return self::queryStatusValid()->pluck('id', 'name')->all();
    }

    public static function checkName($name) {
        return self::query()->where('name', trim($name))->where('status', 1)->first();
    }

    public static function getSubjectKnowledge() {
        return self::query()
            ->select('subject_knowledge.*', 'knowledge.name as parent_name')
            ->join('subject_knowledge', 'knowledge.id', '=', 'subject_knowledge.knowledge_id')
            ->where('knowledge.status', self::STATUS_VALID)
            ->where('subject_knowledge.status', self::STATUS_VALID)
            ->whereNull('subject_knowledge.is_deleted')
            ->get();
    }
}
