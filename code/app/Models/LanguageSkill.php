<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;

class LanguageSkill extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'languageSkills';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới language skill', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật language skill', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa language skill', $record);
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'languageId',
        'levelId',
        'fileId',
    ];

    public function file()
    {
        return $this->belongsTo(File::class, 'fileId', 'id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'memberId', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'languageId', 'id');
    }

    public function languageLevel()
    {
        return $this->belongsTo(LanguageLevel::class, 'levelId', 'id');
    }
}
