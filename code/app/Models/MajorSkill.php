<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MajorSkill extends Model
{
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $table = 'majorSkills';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'majorId',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'memberId', 'id');
    }

    public function major()
    {
        return $this->belongsTo(Major::class, 'majorId', 'id');
    }
}
