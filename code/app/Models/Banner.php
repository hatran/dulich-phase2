<?php

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{

    use SoftDeletes;

    protected $table = 'banners';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới banner', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật banner', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa banner', $record);
        });
    }

    public $perPage = 15;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'profile_image',
        'page_code',
        'content',
        'content2',
        'deleted_at',
        'start_time',
        'end_time',
        'status'
    ];
    protected $dates = ['start_time', 'end_time', 'created_at', 'updated_at', 'deleted_at'];
}
