<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EvaluationCriteria extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'evaluation_criteria';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'percentage',
        'created_author',
        'is_deleted',
        'status'
    ];

    public static function getAllEvaluationCriteria() {
        return self::where('status', 1)->whereNull('is_deleted')->get();
    }
}
