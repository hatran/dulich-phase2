<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaitingPrint extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'waiting_print';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'memberId',
        'cardType',
    ];
}
