<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberEvaluation extends Model
{
    protected $table = 'member_evaluation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'taxcode_company',
        'member_id',
        'name',
        'position',
        'phone',
        'tourist_code',
        'forte_tour',
        'number_of_tourist',
        'from_date',
        'to_date',
        'criteria1',
        'criteria2',
        'criteria3',
        'criteria4',
        'criteria5',
        'criteria1_code',
        'criteria2_code',
        'criteria3_code',
        'criteria4_code',
        'criteria5_code',
        'rank_id',
    ];

    protected $dates = [
        'from_date', 'to_date', 'created_at', 'updated_at'
    ];

    public $timestamps = false;

    public static function fetchCompanyEvaluation($rankId, $memberId)
    {
        return self::query()->select('member_evaluation.*', 'traveler_company_rank.company_name')
            ->join('traveler_company_rank', 'member_evaluation.taxcode_company', '=', 'traveler_company_rank.tax_code')
            ->where('member_id', $memberId)
            ->where('rank_id', $rankId)
            ->get();
    }
}
