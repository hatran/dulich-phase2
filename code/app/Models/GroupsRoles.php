<?php
/**
 * Created by PhpStorm.
 * User: DuyDuc
 * Date: 5/9/2018
 * Time: 10:26 PM
 */

namespace App\Models;

use App\Providers\LogServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupsRoles extends Model
{
    use SoftDeletes;

    protected $table = 'group_role';

    protected static function boot()
    {
        parent::boot();

        static::creating(function($record) {
            LogServiceProvider::createSystemHistory('Tạo mới nhóm quyền', $record);
        });

        static::updating(function($record) {
            LogServiceProvider::createSystemHistory('Cập nhật nhóm quyền', $record);
        });

        static::deleting(function($record) {
            LogServiceProvider::createSystemHistory('Xóa quyền nhóm quyền', $record);
        });
    }

    public $perPage = 10;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'permissions'
    ];
    protected $guarded = [
        'id'
    ];
}