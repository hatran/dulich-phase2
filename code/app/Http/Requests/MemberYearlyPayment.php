<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class MemberYearlyPayment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $routeName = Route::currentRouteName();
        $unique_rule = 'unique:member_payments_yearly,number_payment';
        if ($routeName === 'payment_yearly_update') {
            $number_payment = session('number_payment', '');
            if ($number_payment === $this->input('number_payment')) {
                $unique_rule = null;
            }
        }
        
        return [
            'number_payment' => 'required|max:20|'.$unique_rule,
            'date_payment'   => 'required',
            'currency'       => 'required|numeric',
            'note'           => 'nullable|max:200',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute là trường bắt buộc phải điền.',
            'unique' => ':attribute đã tồn tại.'
        ];
    }

    public function attributes()
    {
        return [
            'number_payment' => 'Số chứng từ',
            'date_payment'   => 'Ngày chứng từ',
            'currency'       => 'Số tiền',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(array('errors' => array("uniqueNumberPayment" => $validator->errors()->first()))));
    }
}
