<?php

namespace App\Http\Middleware;

use App\Providers\LogServiceProvider;
use Closure;

class LogHistory
{
    const LOG_EXCLUDES = ['ajax'];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        LogServiceProvider::createSystemHistory();
        return $next($request);
    }
}
