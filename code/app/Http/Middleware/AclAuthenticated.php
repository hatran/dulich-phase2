<?php

namespace App\Http\Middleware;

use App\Providers\UserServiceProvider;
use Closure;

class AclAuthenticated
{
    /**
     * Handle an incoming request with acl
     *
     * @param $request
     * @param Closure $next
     * @param array $arrRoles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$arrRoles)
    {
        if (empty($arrRoles)) {
            return $next($request);
        }

        $roleFlagDeny = true;

        if (count($arrRoles) < 2) {
            // get role if has single rule in route
            $roleFlagDeny = $this->_doAcl($arrRoles[0]);
        } else {
            foreach ($arrRoles as $role) {
                // get role from role list
                $roleFlagDeny = $this->_doAcl($role);

                // if has role
                if (!$roleFlagDeny) {
                    break;
                }
            }
        }

        if ($roleFlagDeny) {
            abort(404);
        }

        return $next($request);
    }

    private function _doAcl ($role)
    {
        $roleFlagDeny = true;

        if ($role == 'accountant' && UserServiceProvider::isAccountant()) {
            $roleFlagDeny = false;
        }

        if ($role == 'admin' && UserServiceProvider::isAdmin() || auth()->user()->id == 24) {
            $roleFlagDeny = false;
        }

        if ($role == 'contentMSG' && UserServiceProvider::isContentManagerSubGroup()) {
            $roleFlagDeny = false;
        }

        if ($role == 'contentMG' && UserServiceProvider::isContentManagerGroup()) {
            $roleFlagDeny = false;
        }

        if ($role == 'hnExpert' && UserServiceProvider::isHnExpertUser()) {
            $roleFlagDeny = false;
        }

        if ($role == 'dnExpert' && UserServiceProvider::isDnExpertUser()) {
            $roleFlagDeny = false;
        }

        if ($role == 'hcmExpert' && UserServiceProvider::isHcmExpertUser()) {
            $roleFlagDeny = false;
        }

        if ($role == 'hnLeader' && UserServiceProvider::isHnLeaderUser()) {
            $roleFlagDeny = false;
        }

        if ($role == 'dnLeader' && UserServiceProvider::isDnLeaderUser()) {
            $roleFlagDeny = false;
        }

        if ($role == 'hcmLeader' && UserServiceProvider::isHcmLeaderUser()) {
            $roleFlagDeny = false;
        }

        if ($role == 'leaderManager' && UserServiceProvider::isLeaderManager()) {
            $roleFlagDeny = false;
        }

        if ($role == 'cardIssuer' && UserServiceProvider::isMemberShipCardIssuer()) {
            $roleFlagDeny = false;
        }

        return $roleFlagDeny;
    }
}
