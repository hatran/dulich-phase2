<?php

namespace App\Http\Middleware;

use App\Providers\UserServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class ProfileAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $authenticatedUser = UserServiceProvider::getUserById(Auth::id());
			if (empty($authenticatedUser->memberId)) {
				return redirect()->route('admin_user_profile');
			}
            $request->route()->setParameter('id', $authenticatedUser->memberId);
        }

        return $next($request);
    }
}
