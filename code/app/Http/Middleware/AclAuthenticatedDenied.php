<?php

namespace App\Http\Middleware;

use App\Providers\UserServiceProvider;
use Closure;

class AclAuthenticatedDenied
{
    /**
     * Handle an incoming request with acl
     *
     * @param $request
     * @param Closure $next
     * @param array $arrRoles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$arrRoles)
    {
        if (empty($arrRoles)) {
            return $next($request);
        }

        $roleFlagDeny = true;

        if (count($arrRoles) < 2) {
            // get role if has single rule in route
            $roleFlagDeny = $this->_doAcl($arrRoles[0]);
        } else {
            foreach ($arrRoles as $role) {
                // get role from role list
                $roleFlagDeny = $this->_doAcl($role);

                // if has role
                if (!$roleFlagDeny) {
                    break;
                }
            }
        }

        if ($roleFlagDeny) {
            abort(404);
        }

        return $next($request);
    }

    private function _doAcl ($role)
    {
        $roleFlagDeny = false;

        if ($role == 'normal' && UserServiceProvider::isNormalUser()) {
            $roleFlagDeny = true;
        }

        if ($role == 'accountant' && UserServiceProvider::isAccountant()) {
            $roleFlagDeny = true;
        }

        if ($role == 'admin' && UserServiceProvider::isAdmin()) {
            $roleFlagDeny = true;
        }

        if ($role == 'contentMSG' && UserServiceProvider::isContentManagerSubGroup()) {
            $roleFlagDeny = true;
        }

        if ($role == 'contentMG' && UserServiceProvider::isContentManagerGroup()) {
            $roleFlagDeny = true;
        }

        if ($role == 'hnExpert' && UserServiceProvider::isHnExpertUser()) {
            $roleFlagDeny = true;
        }

        if ($role == 'dnExpert' && UserServiceProvider::isDnExpertUser()) {
            $roleFlagDeny = true;
        }

        if ($role == 'hcmExpert' && UserServiceProvider::isHcmExpertUser()) {
            $roleFlagDeny = true;
        }

        if ($role == 'hnLeader' && UserServiceProvider::isHnLeaderUser()) {
            $roleFlagDeny = true;
        }

        if ($role == 'dnLeader' && UserServiceProvider::isDnLeaderUser()) {
            $roleFlagDeny = true;
        }

        if ($role == 'hcmLeader' && UserServiceProvider::isHcmLeaderUser()) {
            $roleFlagDeny = true;
        }

        if ($role == 'leaderManager' && UserServiceProvider::isLeaderManager()) {
            $roleFlagDeny = true;
        }

        if ($role == 'cardIssuer' && UserServiceProvider::isMemberShipCardIssuer()) {
            $roleFlagDeny = true;
        }

        return $roleFlagDeny;
    }
}
