<?php

namespace App\Http\Middleware;

use App\Providers\UserServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (UserServiceProvider::isNormalUser()) {
                return redirect('/');
            } else {
                if (auth()->user()->hasPermissions('admin_member_list_view')) {
                    return redirect()->route('admin_member_list_view');
                }

                if ($firstRoute = auth()->user()->getFirstRouteAccess()) {
                    return redirect()->route($firstRoute);
                }

                abort(404);
                /*if (UserServiceProvider::isAccountant()) {
                    return redirect('/officesys/payment');
                } elseif (UserServiceProvider::isMemberShipCardIssuer()) {
                    return redirect('/officesys/decision');
                } elseif (UserServiceProvider::isExpertRole() || UserServiceProvider::isLeaderRole()){
                    return redirect('/officesys/list_members');
                }
                else {
                    return redirect('/officesys/list_members');
                }*/
            }
        }

        return $next($request);
    }
}
