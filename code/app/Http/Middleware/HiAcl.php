<?php

namespace App\Http\Middleware;

use Closure;

class HiAcl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->hasPermissions(request()->route()->getName())) {
            abort(404);
        }

        return $next($request);
    }
}
