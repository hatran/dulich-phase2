<?php

namespace App\Http\Middleware;

use Closure;

class QuizAuthentication
{
    const MESS_2 = 'Vui lòng đăng nhập để sử dụng chức năng này';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            return $next($request);
        }
        $routeName = $request->route()->getName();
        switch ($routeName) {
            case 'front_end_infoquestion':
                return response()->view('frontend.newdesign.survey.unauthenticated', [
                    'data' => self::MESS_2
                ]);
            default:
                abort(404);
        }
    }
}
