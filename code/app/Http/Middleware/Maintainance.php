<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\View;

class Maintainance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isDisplayMaintenance = !config('maintainance.OPEN_SITE') && !$this->isAccessPermitIP(config('maintainance.PERMIT_ACCESS_IP'));
        if ($isDisplayMaintenance) {
            View::share('status', 503);
            return abort('503');
        }
        return $next($request);
    }

    private function isAccessPermitIP($permitIPAddresses)
    {
        if ($permitIPAddresses == "") {
            return false;
        }

        if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ipAddress = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ipAddress = $_SERVER["REMOTE_ADDR"];
        }

        $ipAddressArray = explode(',', $ipAddress);

        $permitIPList = explode(',', $permitIPAddresses);

        foreach ($permitIPList as $permitIP) {
            if (preg_match('/\//', $permitIP)) {
                if ($this->inCIDR($ipAddress, $permitIP)) {
                    return true;
                }
            } else if (in_array($permitIP, $ipAddressArray, false)) {
                return true;
            }
        }
        return false;
    }

    protected function inCIDR($ip, $cidr)
    {
        list($network, $mask_bit_len) = explode('/', $cidr);
        $host = 32 - $mask_bit_len;
        $net = ip2long($network) >> $host << $host;
        $ip_net = ip2long($ip) >> $host << $host;
        return $net === $ip_net;
    }
}
