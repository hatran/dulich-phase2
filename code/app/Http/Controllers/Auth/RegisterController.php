<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Providers\LogServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/officesys/users';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        abort(404);
        $this->middleware('user');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, User::$rule, User::$message);

        $validator->setAttributeNames(User::$niceAttributeName);

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        //LogServiceProvider::createSystemHistory(trans('history.fn_create_user'), $data);
        return User::create([
            'fullname'      => $data['fullname'],
            'username'      => $data['username'],
            'email'         => $data['email'],
            'role'          => $data['role'],
            'status'        => $data['status'],
            'province_type' => $data['province_type'],
            'password'      => bcrypt($data['password']),
        ]);
    }
}
