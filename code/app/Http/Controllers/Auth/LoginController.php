<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/officesys/list_members';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        if ($request->input('login_homepage') != 1) {
            $this->redirectTo = '/redirect';
            app()->setLocale('vi');
            $this->middleware('guest')->except('logout');
        }
    }

    public function login(Request $request)
    {
        if ($request->input('login_homepage') != 1) {
            return $this->loginPost($request);
        } else {
            return $this->loginAjaxPost($request);
        }
    }

    public function loginAjaxPost(Request $request)
    {
        if (!env('DISABLE_LOGIN_CAPTCHA')) {
            if (!$this->captchaCheck($request)) {
                return view('frontend.newdesign.login.home')
                    ->with('request', $request->input())
                    ->withErrors(['captcha' => trans('auth.failed')]);
            }
        }

        if ($request->remember) {
            $remember = true;
        } else {
            $remember = false;
        }

        if (Auth::attempt(['username' => $request->username, 'password' => $request->password], $remember))
            return 'true';

        return view('frontend.newdesign.login.home')
            ->with('request', $request->input())
            ->withErrors(['username' => trans('auth.failed')]);
    }

    public function loginPost(Request $request)
    {
        $urlFallback = '/login';
        $ruleValidate = [
            'username' => 'required',
            'password' => 'required',
//            'g-recaptcha-response' => 'required',
        ];

        if (env('DISABLE_LOGIN_CAPTCHA')) {
            unset($ruleValidate['captcha']);
            unset($ruleValidate['g-recaptcha-response']);
        }

        $this->validate($request, $ruleValidate, [
            'failed' => ':attribute không tồn tại',
            'required' => ':attribute là trường bắt buộc phải điền',
            //'captcha' => 'Mã xác nhận không hợp lệ',
//            'recaptcha' => 'Vui lòng nhập đúng mã xác thực'
        ], [
            'username' => 'Tên đăng nhập',
            'password' => 'Mật khẩu',
            'captcha' => 'Mã xác nhận',
//            'g-recaptcha-response' => 'Mã xác nhận'
        ]);

        if ($request->input('remember')) {
            $remember = true;
        } else {
            $remember = false;
        }

        if (Auth::attempt(['username' => $request->username, 'password' => $request->password], $remember)) {
            return redirect('/redirect');
        }

        return redirect($urlFallback)->withErrors(['username' => trans('auth.failed')]);
    }

    function captchaCheck(Request $request)
    {
        $members = $request->input();
        $captcha = '';
        if (!empty($members['captcha'])) $captcha = $members['captcha'];
        $rules = ['captcha' => 'required|captcha'];
        $captcha = array("captcha" => $captcha, "check" => '');
        $validator = Validator::make($captcha, $rules);
        if ($validator->fails()) return false;
        return true;
    }
}
