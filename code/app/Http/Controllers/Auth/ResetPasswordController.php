<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public static $niceAttributeName = [
        'email' => 'Email',
        'password' => 'Mật khẩu mới',
    ];

    public function rules() {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|string|min:8|regex:/^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8,}$/|confirmed',
        ];
    }
    public function validationErrorMessages()
    {
        return [
            'required' => ':attribute là trường bắt buộc phải điền',
            'string' => ':attribute phải là một đoạn văn bản',
            'max' => 'Độ dài tối đa của :attribute là :max kí tự',
            'min' => 'Độ dài tối thiểu của :attribute là :min kí tự',
            'unique' => ':attribute đã được sử dụng',
            'regex' => ':attribute quá yếu, mật khẩu phải theo chuẩn sau: Mật khẩu có độ dài tối thiểu 8 ký tự bao gồm ít nhất 2 ký tự viết hoa, 1 ký tự đặc biệt, 2 chữ số và 3 ký tự thường',
            'confirmed' => 'Xác nhận mật khẩu phải giống với Mật Khẩu đã nhập',
        ];
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(),
            $this->validationErrorMessages(), self::$niceAttributeName);

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
        }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($response)
            : $this->sendResetFailedResponse($request, $response);
    }
}
