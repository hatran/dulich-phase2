<?php

namespace App\Http\Controllers\Admin;

use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Http\Controllers\Admin\Base\BaseController;
use App\Jobs\SendEmail;
use App\Jobs\SendSms;
use App\Libs\Helpers\LogsHelper;
use App\Models\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\HistoryEmailSMS;
use App\Constants\CommontConstants;

class HistoryEmailSMSController extends BaseController
{

    // Path
    const AJAX_PATH = 'officesys/history/email/ajax';
    const SEND_PATH = 'officesys/history/email/send';
    const SEND_SMS_PATH = 'officesys/history/sms/send';

    protected $_request = '';
    protected $_totalCollection = 0;

    // Fliter
    const HISTORY_EMAILSMS_SEARCH_TYPE = 'type';
    const HISTORY_EMAIL_SMS_SEARCH_EMAIL_TO = 'email_sms_to';
    const HISTORY_EMAILSMS_SEARCH_STARTDATE = 'start_date';
    const HISTORY_EMAILSMS_SEARCH_ENDDATE = 'end_date';

    protected $_type = '';
    protected $_email_sms_to = '';
    protected $_startdate = '';
    protected $_enddate = '';

    const BCC_EMAIL = 'hoihdv@gmail.com';

    public function __construct(Request $request)
    {
        // FILTER
        $this->_type = $request->input(self::HISTORY_EMAILSMS_SEARCH_TYPE);
        $this->_email_sms_to = $request->input(self::HISTORY_EMAIL_SMS_SEARCH_EMAIL_TO);
        $this->_startdate = $request->input(self::HISTORY_EMAILSMS_SEARCH_STARTDATE);
        $this->_enddate = $request->input(self::HISTORY_EMAILSMS_SEARCH_ENDDATE);
        // Path
        $this->_path = self::AJAX_PATH;
        $this->getCollection($hasAuth = false);

        parent::__construct($request);
    }

    public function getCollection($hasAuth = true)
    {
        $collection = $this->fliterBySearch($hasAuth);
        $this->_collection = $collection->toArray();
        $this->reformatCollection($hasAuth);
        $this->addShortField('content',40);
        parent::getCollection($hasAuth);
    }

    public function reformatCollection($hasAuth) {
        $result = [];
        foreach($this->_collection as $_item) {
            $_item['type'] = ($_item['type'] == 1)? 'Email' : 'SMS' ;
            $_item['action'] = $this->actionHtml($_item['id'], $_item['type']);
            $_item['content'] = htmlspecialchars_decode($_item['content']);
            if ($hasAuth && (int) auth()->user()->role !== UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
                $_item['download'] = $this->renderDownloadPdfHtml($_item['type'], $_item['email_sms_to']);
            }
            $result[] = $_item;
        }
        $this->_collection = $result;
    }

    public function actionHtml($id, $type){

        return view('admin.historyemailsms.button.send')->with([
            'url' => $this->getResendMailUrl($id, $type)
        ])->render();
    }

    public function renderDownloadPdfHtml($type, $emailSmsTo)
    {
        $query = Member::query();
        if ($type === 'Email') {
            $query->where('firstEmail', 'like', '%'.$emailSmsTo.'%')
                ->orWhere('secondEmail', 'like', '%'.$emailSmsTo.'%');
        } else {
            $emailSmsTo = '0'. substr($emailSmsTo, 2);
            $query->where('firstMobile', 'like', '%'.$emailSmsTo.'%')
                ->orWhere('secondMobile', 'like', '%'.$emailSmsTo.'%');
        }

        $memberObj = $query->select('id', 'file_code')->first();
        if (! $memberObj || array_key_exists($memberObj->status, MemberConstants::$file_download_const)) {
            $memberId = 0;
            $memberFileCode = 0;
        } else {
            $memberId = $memberObj->id;
            $memberFileCode = $memberObj->file_code;
        }

        return view('admin.historyemailsms.button.download')->with([
            'memberId' => $memberId,
            'fileCode' => $memberFileCode
        ])->render();
    }

    public function getResendMailUrl($id, $type)
    {
        return ($type == 'Email')?  url(self::SEND_PATH) . '/'.$id :  url(self::SEND_SMS_PATH) . '/'.$id;
    }

    public function fliterBySearch($hasAuth)
    {
        $collection = HistoryEmailSMS::orderBy('history_email_sms.id', 'desc');
        if (!empty($this->_type)) $collection->where('type', 'like', '%' . $this->_type . '%');
        if (!empty($this->_email_sms_to)) $collection->where('email_sms_to', 'like', '%' . $this->_email_sms_to . '%');
        if (!empty($this->_startdate) && !empty($this->_enddate)) {
            $start_time = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $this->_startdate)->toDateString();
            $end_time = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $this->_enddate)->toDateString();
            $collection->where('created_at', '>=' , $start_time.' 00:00:01')->where('created_at', '<=', $end_time.' 23:59:59');
        }

        if (!empty($this->_startdate) && empty($this->_enddate)) {
            $start_time = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $this->_startdate)->toDateString();
            $collection->where('created_at', '>=' , $start_time.' 00:00:01');
        }

        if (!empty($this->_enddate) && empty($this->_startdate)) {
            $end_time = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $this->_enddate)->toDateString();
            $collection->where('created_at', '<=', $end_time.' 23:59:59');
        }

        $cloneCollection = clone $collection;
        $this->_totalCollection = $cloneCollection->count();
        $collection = $collection->take($this->_limit)->skip($this->_offset);
        return $collection->get();
    }

    public function getAjaxJson()
    {
        $this->getCollection($hasAuth = true);
        $response = array(
            "rows" => $this->_collection,
            "total" => $this->_totalCollection
        );
        return $response;
    }

    private function _reSendMail ($id)
    {
        $objHistoryEmailSms = HistoryEmailSMS::find($id);
        if (!empty($objHistoryEmailSms->email_sms_to)) {
            try {
                $to = [['address' => $objHistoryEmailSms->email_sms_to, 'name' => $objHistoryEmailSms->email_sms_to]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = $objHistoryEmailSms->subject;
                $content = html_entity_decode($objHistoryEmailSms->content);
                dispatch((new SendEmail('admin.historyemailsms.mail_resend', ['content' => $content], $to, $bcc, $subject))->delay(5));

                /*$content = $objHistoryEmailSms->content;
                if (!empty($content) && !empty($objHistoryEmailSms->email_sms_to)) {
                    Mail::send('admin.historyemailsms.mail_resend', ['content'=> $content], function ($message) use ($objHistoryEmailSms) {
                        $message->to($objHistoryEmailSms->email_sms_to, $objHistoryEmailSms->subject)
                            ->subject($objHistoryEmailSms->subject);
                    });
                }*/

                return true;
            } catch (\Exception $exception) {
                LogsHelper::exceptionByFile('resend_mail_in_log_fail', $exception, $objHistoryEmailSms);
                return false;
            }
        }
    }



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.historyemailsms.index')->with(
            array(
                'response_view' => $this->_request,
                'ajaxFliterUrl' => $this->getAjaxFilterUrl(),
                'total'         => $this->_totalCollection
            ));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function ajax()
    {
        return response()->json($this->getAjaxJson());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function reSendEmail($id)
    {
        $resendEmail = $this->_reSendMail($id);

        if ($resendEmail) {
            return redirect()->route('admin_history_emailsms_view')->with(
                array(
                    'response_view' => $this->_request,
                    'ajaxFliterUrl' => $this->getAjaxFilterUrl(),
                    'success_message' => "Gửi lại email thành công",
                    'total'         => $this->_totalCollection
                ));
        } else {
            return redirect()->route('admin_history_emailsms_view')->with(
                array(
                    'response_view' => $this->_request,
                    'ajaxFliterUrl' => $this->getAjaxFilterUrl(),
                    'success_message' => "Gửi lại email thất bại",
                    'total'         => $this->_totalCollection
                ));
        }
    }

    public function reSendSms($id)
    {
        $objHistoryEmailSms = HistoryEmailSMS::find($id);
        if (!empty($objHistoryEmailSms->email_sms_to)) {
            dispatch((new SendSms($objHistoryEmailSms->email_sms_to, $objHistoryEmailSms->content))->delay(Carbon::now()->addSeconds(2)));
        }
        return redirect()->route('admin_history_emailsms_view')->with(
            array(
                'response_view' => $this->_request,
                'ajaxFliterUrl' => $this->getAjaxFilterUrl(),
                'success_message' => "Gửi lại sms thành công sendBulkSms",
                'total'         => $this->_totalCollection
            ));
    }
}
