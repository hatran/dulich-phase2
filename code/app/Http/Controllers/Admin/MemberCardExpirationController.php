<?php

namespace App\Http\Controllers\Admin;

use App\Constants\CommontConstants;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Libs\Helpers\LogsHelper;
use App\Models\Emails;
use App\Providers\MemberServiceProvider;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MemberCardExpirationController extends Controller {

    public function sendExpirationEmail (Request $request)
    {
        $strMemberIdList = $request->input('list_id');
        $arrResendNotificationMemberId = explode(',', $strMemberIdList);

        foreach ($arrResendNotificationMemberId as $key => $memberId) {
            $objMember = MemberServiceProvider::getMemberById($memberId);
            if (!empty($objMember->firstEmail)) {
                $this->sendMail($objMember->firstEmail, $objMember, $key);
            }

            if (!empty($objMember->secondEmail)) {
                $this->sendMail($objMember->secondEmail, $objMember, $key);
            }
        }

        return back()->withInput()->with('successes', ['Gửi thông báo sắp hết hạn hội viên thành công']);
    }

    private function sendMail ($email, $objMember, $key)
    {
        try {
            $to = [['address' => $email, 'name' => $objMember->fullName]];
            $subject = '';
            $getEmail = Emails::select('content', 'title')->where('option_code', 'EMAIL15')->first();
            $attach = null;

            if (!empty($getEmail)) {
                $subject = $getEmail->title;
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_MAHOIVIEN, htmlentities($objMember->member_code), $getEmail->content);
            }

            $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
            $template = 'admin.member_card_expiration.email';
            dispatch((new SendEmail($template, $data, $to, '', $subject, $attach))->delay(Carbon::now()->addSeconds($key + 5)));
        } catch (\Exception $exception) {
            LogsHelper::exceptionByFile('member_card_expiration_fail', $exception, $objMember);
        }
    }
}
