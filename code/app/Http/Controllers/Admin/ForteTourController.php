<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\ForteTourServiceProvider;
use App\Providers\LogServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\UserConstants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Constants\MemberConstants;
use App\Libs\Helpers\Utils;
use App\Models\ForteTour;


class ForteTourController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const SEARCH_INPUT = 'search';
    const OBJ_NEWS = 'objNews';
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';

    const TITLE = 'nameall';
    const ID = 'id';
    const CONTENT = 'content';
    const STATUS = 'status';
    const START_TIME = 'start_time';
    const END_TIME = 'end_time';
    const COUNT_NEW= 'count_news';
    const CURRENT_PAGINATOR = 'current_paginator';
    const OPTIONS = 'options';

    const FORTE_INDEX = 'fortetour.index';
    const FORTE_SHOW = 'fortetour.show';
    const SUCCESSES = 'successes';

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objNews = $this->_searchByCondition($request, $limit,false);
        $sumObjNews = $this->_searchByCondition($request, $limit,true);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }
        $options = ForteTourServiceProvider::getAllParentForteTour();
      
//        print_r(json_decode(json_encode($objNews)));
        return view('admin.fortetour.index', [
            self::OBJ_NEWS          => $objNews,
            'admin_user'            => UserConstants::ADMIN_USER,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_NEW         => count($sumObjNews),
            self::OPTIONS           => $options,
            self::PAGINATOR         => (count($sumObjNews) > 0) ? $objNews->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(self::TITLE))) {
            $title = $request->input(self::TITLE);
        } else {
            $title = '';
        }

        if (!empty($request->input(self::ID))) {
            $id = $request->input(self::ID);
        } else {
            $id = '';
        }

        $arrConditions = [
            self::TITLE  => $title,
            self::ID   => $id,
        ];

        return ForteTourServiceProvider::newsSearchByConditions($arrConditions, $limit,$isCount);
    }

    public function saveForte (Request $request)
    {
        if(!empty($request->input('id_edit'))){
            $checkData = ForteTourServiceProvider::getDataById($request->input('id_edit'));
            if(empty($checkData->id)){
                return redirect()->route($this::FORTE_INDEX)->withErrors(['Tuyến không tồn tại hoặc đã xóa khỏi hệ thống']);
            }
        }

        $objData = [];
        //add danh muc
        if(empty($request->input('id_edit'))){
            $option = $request->input('optionAll_add');
            $nameAll = $request->input('nameAll_add');
        }else{
            $id = $request->input('id_edit');
            $option = $request->input('option_edit');
            $nameAll = $request->input('nameAll_edit');
        }
        
        $objData['name']           = $nameAll;
        $objData['status']         = 1;
        $objData['parent_id']      = $option;
        $objType = ForteTourServiceProvider::getType($option);
        if (empty($objType)) {
            return redirect()->route($this::FORTE_INDEX, ['id' => $request->input('option_edit')])->withErrors(['Tuyến không tồn tại hoặc đã xóa khỏi hệ thống']);
        }
        else {
            $objData['type'] = $objType->type;
        }
        if(!empty($request->input('id_edit'))){
            $objSave = ForteTour::where('id', $id)->update($objData);
        }else {
            $objSave = ForteTour::create($objData);
        }
        
        
        if (empty($objSave)) {
            if(!empty($request->input('id_edit'))){
                return redirect()->route($this::FORTE_INDEX, ['id' => $option])->withErrors(['Cập nhật tuyến thất bại']);
            }else{
                return redirect()->route($this::FORTE_INDEX, ['id' => $option])->withErrors(['Thêm mới tuyến thất bại']);
            }
        }
     
        if(!empty($request->input('id_edit'))){
            return redirect()->route($this::FORTE_INDEX, ['id' => $option])->with($this::SUCCESSES, ['Cập nhật tuyến thành công']);
        }else{
            return redirect()->route($this::FORTE_INDEX, ['id' => $option])->with($this::SUCCESSES, ['Thêm mới tuyến thành công']);
        }

    }
    
    public function showEdit ($id, $option, Request $request)
    {

        if (empty($id) || empty($option)) {
            $objData = null;
        } else {
            $objData = ForteTourServiceProvider::getDataById($id);
        }

        if (empty($objData)) {
            $request->session()->flash('error_ajax', ['Tuyến không tồn tại hoặc đã xóa khỏi hệ thống']);
            return response()->json(['deleted' => 'error']);
        }
      
        $objData['title'] = $objData['name'];
        $objOptions = ForteTourServiceProvider::getAllParentForteTour();
        return response()->json(['objData' => $objData, 'objOptions' => $objOptions]);
    }

    public function deleteOption ($id, $option)
    {
        $objIntro = ForteTourServiceProvider::getDataById($id);
        if (empty($objIntro)) {
            return redirect()->route($this::FORTE_INDEX, ['id' => $option])->withErrors(['Tên tuyến không tồn tại hoặc đã xóa khỏi hệ thống']);
        }

        $objData['status'] = 0;
        $objMemberForteTour = ForteTourServiceProvider::getMemberForteTourById($id);
        if(empty($objMemberForteTour)) {
            $objSave = ForteTour::where('id', $id)->update($objData);
        }else{
            return redirect()->route($this::FORTE_INDEX, ['id' => $option])->withErrors(['Tên tuyến đang được sử dụng, ko được xóa']);
        }
        if(empty($objSave)){
            return redirect()->route($this::FORTE_INDEX, ['id' => $option])->withErrors(['Xóa tuyến thất bại']);
        } else {
            return redirect()->route($this::FORTE_INDEX, ['id' => $option])->with($this::SUCCESSES, ['Xóa tuyến thành công']);
        }
    }

    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }
}
