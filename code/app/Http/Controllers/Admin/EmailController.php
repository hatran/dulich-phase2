<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FeePolicy;
use App\Models\Option;
use App\Models\User;
use App\Models\Emails;
use App\Providers\EmailServiceProvider;
use App\Providers\LogServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\UserConstants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Offices;

class EmailController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const SEARCH_INPUT = 'search';
    const OBJ_EMAILS = 'objEmails';
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';

    const TITLE = 'title';
    const OPTION_CODE = 'option_code';
    const BRANCH_ID = 'branch_id';
    const CONTENT = 'content';
    const STATUS = 'status';
    const START_TIME = 'start_time';
    const END_TIME = 'end_time';
    const COUNT_EMAIL= 'count_emails';
    const HOIPHI= 'hoiphi';
    const CURRENT_PAGINATOR = 'current_paginator';
    const OPTIONS = 'options';
    const STATUS_PUBLIC = 1;
    const STATUS_DRAFT = 0;

    const EMAIL_INDEX = 'emails.index';
    const EMAIL_SHOW = 'emails.show';
    const SUCCESSES = 'successes';
    public static $statusOption = [
        self::STATUS_PUBLIC => 'Hoạt động',
        self::STATUS_DRAFT => 'Khóa'
    ];

    private $destinationPath = 'uploads\pdf';

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objEmails = $this->_searchByCondition($request, $limit,false);
        $sumObjEmails = $this->_searchByCondition($request, $limit,true);
        $arrOptions = EmailServiceProvider::getAllOption(CommontConstants::EMAIL);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }

        $branches = EmailServiceProvider::getOffices();
        $options = EmailServiceProvider::getAllOption(CommontConstants::EMAIL);
        $offices = Offices::getProvinceCodeByProvinceType("");

        return view('admin.email.index', [
            self::OBJ_EMAILS          => $objEmails,
            'admin_user'            => UserConstants::ADMIN_USER,
            'statusOption' => self::$statusOption,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_EMAIL         => count($sumObjEmails),
            self::OPTIONS           => $options,
            self::PAGINATOR         => (count($sumObjEmails) > 0) ? $objEmails->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
            'branches'              => $branches,
            'offices'       => $offices,
            'arrOptions'   => $arrOptions
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(self::TITLE))) {
            $title = $request->input(self::TITLE);
        } else {
            $title = '';
        }

        if (!empty($request->input(self::BRANCH_ID)) && (empty($request->input("typeOp")) || empty($request->input("typeOp")) != "1")) {
            $branch_id = $request->input(self::BRANCH_ID);
        } else {
            // $options = EmailServiceProvider::getOffices();
            // $branch_id = [];
            // foreach ($options as $key => $value) {
            //     $branch_id[] = $value->id;
            // }
            $branch_id = null;
        }

        if ($request->input('search_status') != '') {
            $status = $request->input('search_status');
        } else {
            $status = '';
        }

        if (!empty($request->input('created_at'))) {
            $created_at = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input('created_at', ''))->toDateString();
        } else {
            $created_at = '';
        }

        $arrConditions = [
            self::TITLE         => $title,
            self::BRANCH_ID   => $branch_id,
            'status'            => $status,
            'created_at'        => $created_at,
            "option_code"       => $request->input("option_code")
        ];

        return EmailServiceProvider::emailsSearchByConditions($arrConditions, $limit,$isCount);
    }
    public function show ($id)
    {

        if (empty($id)) {
            $objEmail = null;
        } else {
            $objEmail = EmailServiceProvider::getEmailById($id);
        }
        if (empty($objEmail)) {
            abort(404);
        }
        $arrOptions = EmailServiceProvider::getAllOption(CommontConstants::EMAIL);
        $arrBranches = EmailServiceProvider::getOffices();
        $offices = Offices::getProvinceCodeByProvinceType("", 'id');
        $branch_id = $objEmail->branch_id;
        $list_fees = FeePolicy::where(function ($query) use ($branch_id) {
            $query->where('list_id', '=', $branch_id)
                ->orWhereNull('list_id');
        })->whereNull('deleted_at')->orderBy('list_id', 'desc')->get();
        $key_email_fee = 'key_email_fee_'.$objEmail->id;
        $fee_selected = Option::where('code', '=', 'EMAIL_HOIPHI')->where('key', '=', $key_email_fee)->first();
        if ($fee_selected) {
            $fee = $fee_selected->value;
        }else {
            $fee = 1;
        }
        return view('admin.email.edit', [
            'objEmail' => $objEmail,
            'arrOptions' => $arrOptions,
            'arrBranches' => $arrBranches,
            'offices'       => $offices,
            'list_fees'    => $list_fees,
            'fee_selected' => $fee
        ]);
    }

    public function detail ($id)
    {
        if (empty($id)) {
            $objEmail = null;
        } else {
            $objEmail = EmailServiceProvider::getEmailById($id);
        }

        if (empty($objEmail)) {
            return redirect()->route($this::EMAIL_INDEX)->withErrors(['Email không tồn tại hoặc đã xóa khỏi hệ thống']);
        }
        $arrOptions = EmailServiceProvider::getAllOption(CommontConstants::EMAIL);
        $arrBranches = EmailServiceProvider::getOffices();
        return view('admin.email.detail', [
            'objEmail' => $objEmail,
            'arrOptions' => $arrOptions,
            'arrBranches' => $arrBranches
        ]);
    }
    public function destroy ($id)
    {
        if (empty($id)) {
            abort(404);
        }
        $objEmail = EmailServiceProvider::getEmailById($id);
        if (empty($objEmail)) {
            return redirect()->route($this::EMAIL_INDEX)->withErrors(['Email không tồn tại hoặc đã xóa khỏi hệ thống']);
        }else{
            if($objEmail->status == 1){
                return redirect()->route($this::EMAIL_INDEX)->withErrors(['Bạn không được phép xóa email đang hoạt động']);
            }else{
                $objEmail = EmailServiceProvider::delete($id);

                if (empty($objEmail)) {
                    return redirect()->route($this::EMAIL_INDEX)->withErrors(['Xóa email thất bại']);
                } else {
                    // LogServiceProvider::createSystemHistory(trans('history.fn_delete_introduction'), [
                    //     'introductionId' => $id,
                    //     'introduction' => $objNew
                    // ]);
                    return redirect()->route($this::EMAIL_INDEX)->with($this::SUCCESSES, ['Xóa email thành công']);
                }
            }
        }
    }
    public function create ()
    {
        $arrOptions = EmailServiceProvider::getAllOption(CommontConstants::EMAIL);
        $arrBranches = EmailServiceProvider::getOffices();
        $offices = Offices::getProvinceCodeByProvinceType("", 'id');
        $list_fees = FeePolicy::where(function ($query) {
            $query->where('list_id', '=', 1)
                ->orWhereNull('list_id');
        })->whereNull('deleted_at')->orderBy('list_id', 'desc')->get();
        return view('admin.email.register', [
            'arrOptions' => $arrOptions,
            'arrBranches' => $arrBranches,
            'offices'       => $offices,
            'list_fees'    => $list_fees
        ]);
    }

    public function store (Request $request)
    {
        $validator = Validator::make($request->all(), Emails::$rule, Emails::$message);

        $validator->setAttributeNames(Emails::$niceAttributeName);

        if ($validator->fails()) {
            return redirect()
                ->route('emails.create')
                ->withErrors($validator)
                ->withInput();
        }

        $arrData = [
            $this::OPTION_CODE      => $request->input($this::OPTION_CODE) ,
            $this::TITLE            => $request->input($this::TITLE),
            $this::STATUS           => $request->input($this::STATUS),
            $this::CONTENT          => $request->input($this::CONTENT),
            $this::BRANCH_ID      => $request->input("typeOp") == "0" ? $request->input($this::BRANCH_ID) : "0",
        ];
        $arrData[$this::START_TIME]     = !empty($request->input($this::START_TIME)) ? date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input($this::START_TIME)))) : date('Y-m-d H:i:s');
        $arrData[$this::END_TIME]       = !empty($request->input($this::END_TIME)) ? date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input($this::END_TIME)))) : null;

        if ($request->hasFile('attach')) {
            $upload = $request->file('attach');
            $arrData['attach'] = public_path($this->destinationPath . '\\' . $upload->getClientOriginalName());
            
            //Move Uploaded File
            $upload->move($this->destinationPath, $upload->getClientOriginalName());
        }

        $objEmailOption = Emails::create($arrData);

        if (empty($objEmailOption)) {
            return redirect()->route('emails.create')->withErrors(['Tạo mới email thất bại']);
        } else {
            $option = [
                'status' => 1,
                'key'   => 'key_email_fee_'.$objEmailOption->id,
                'value' => $request->input($this::HOIPHI) || 1,
                'code'  => 'EMAIL_HOIPHI'
            ];
            Option::create($option);

            return redirect()->route($this::EMAIL_INDEX)->with($this::SUCCESSES, ['Tạo mới email thành công']);
        }
    }

    public function getListFee ($id, Request $request)
    {
        $list_fees = FeePolicy::where(function ($query) use ($id) {
            $query->where('list_id', '=', $id)
                ->orWhereNull('list_id');
        })->whereNull('deleted_at')->orderBy('list_id', 'desc')->get()->toArray();
        return response()->json([
            'list_fees' => $list_fees
        ]);
    }

    public function checkStatus ($id, $status, $option_code, Request $request)
    {
        $data = EmailServiceProvider::getEmailByOptionCode($id, $status, $option_code);

        if ($data == false) {
            return response()->json(['error' => 'error', 'content' => 'Chuyên mục này có email đang hoạt động. Vui lòng khóa email đó trước.']);
        }
        return response()->json(['error' => '']);
    }

    public function update (Request $request, $id)
    {

        if (empty($id)) {
            abort(404);
        }

        $objEmail = EmailServiceProvider::getEmailById($id);
        if (empty($objEmail)) {
            return redirect()->route($this::EMAIL_INDEX)->withErrors(['Email không tồn tại hoặc đã xóa khỏi hệ thống']);
        }

        $validator = Validator::make($request->all(), Emails::$rule, Emails::$message);
        $validator->setAttributeNames(Emails::$niceAttributeName);

        if ($validator->fails()) {
            return redirect()
                ->route($this::EMAIL_SHOW, [$id])
                ->withErrors($validator)
                ->withInput();
        }
        $arrData = [
            'id'                    => $id,
            $this::BRANCH_ID      => $request->input("typeOp") == "0" ? $request->input($this::BRANCH_ID) : "0",
            $this::OPTION_CODE      => $request->input($this::OPTION_CODE),
            $this::TITLE            => $request->input($this::TITLE),
            $this::STATUS           => $request->input($this::STATUS),
            $this::CONTENT          => $request->input($this::CONTENT),
        ];
        if(!empty($request->input($this::START_TIME))) $arrData[$this::START_TIME]     =  date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input($this::START_TIME))));
        $arrData[$this::END_TIME]       = !empty($request->input($this::END_TIME)) ? date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input($this::END_TIME)))) : null;
        
        if ($request->hasFile('attach')) {
            $upload = $request->file('attach');
            $arrData['attach'] = public_path($this->destinationPath . '\\' . $upload->getClientOriginalName());
            if (file_exists($objEmail->attach)) {
                unlink($objEmail->attach);
            }
            //Move Uploaded File
            $upload->move($this->destinationPath, $upload->getClientOriginalName());
        }
 
        $objEmail = EmailServiceProvider::update($arrData);
        
        if (empty($objEmail)) {
            return redirect()->route($this::EMAIL_INDEX)->withErrors(['Cập nhật email thất bại']);
        } else {
            // LogServiceProvider::createSystemHistory(trans('history.fn_save_introduction'), [
            //     'introduction' => $objNew
            // ]);
            $key_email_fee = 'key_email_fee_'.$objEmail->id;
            $fee_selected = Option::where('code', '=', 'EMAIL_HOIPHI')->where('key', '=', $key_email_fee)->first();
            if ($fee_selected) {
                $fee_selected->value = $request->input($this::HOIPHI) || 1;
                $fee_selected->save();
            }else {
                $option = [
                    'status' => 1,
                    'key'   => 'key_email_fee_'.$objEmail->id,
                    'value' => $request->input($this::HOIPHI) || 1,
                    'code'  => 'EMAIL_HOIPHI'
                ];
                Option::create($option);
            }
            return redirect()->route($this::EMAIL_INDEX)->with($this::SUCCESSES, ['Cập nhật email thành công']);
        }
    }



    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }


}
