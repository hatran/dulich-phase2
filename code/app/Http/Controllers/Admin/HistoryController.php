<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Base\BaseController;
use Illuminate\Http\Request;
use App\Models\History;

class HistoryController extends BaseController
{

    // Path
    const HISTORY_AJAX_PATH = 'officesys/history/ajax';

    protected $_request = '';
    protected $_totalCollection = 0;

    // Fliter
    const HISTORY_SEARCH_NAME = 'fullname';
    const HISTORY_SEARCH_FUNCTION = 'title';
    const HISTORY_SEARCH_STARTDATE = 'start_date';
    const HISTORY_SEARCH_ENDDATE = 'end_date';

    protected $_fullname = '';
    protected $_router = '';
    protected $_startdate = '';
    protected $_enddate = '';

    public function __construct(Request $request)
    {
        // FILTER
        $this->_fullname = $request->input(self::HISTORY_SEARCH_NAME);
        $this->_router = $request->input(self::HISTORY_SEARCH_FUNCTION);
        $this->_startdate = $request->input(self::HISTORY_SEARCH_STARTDATE);
        $this->_enddate = $request->input(self::HISTORY_SEARCH_ENDDATE);

        // Path
        $this->_path = self::HISTORY_AJAX_PATH;
        $this->getCollection($hasAuth = false);
        return parent::__construct($request);
    }

    public function getCollection($hasAuth = true)
    {
        $collection = $this->fliterBySearch();
        $this->_collection = $collection->toArray();
        $this->reformatCollection();
        $this->addShortField('description',40);
        parent::getCollection($hasAuth);
    }

    public function reformatCollection(){
        $result = [];
        foreach($this->_collection as $_item){
            if(!empty($_item['function_name'])) $_item['name'] = $_item['function_name'];
            $result[] = $_item;
        }
        $this->_collection = $result;
    }

    public function fliterBySearch()
    {
        $collection = History::join('users', 'users.id', '=', 'history.user_id')
            ->leftJoin('permission','permission.id','=','history.permission_id')
            ->orderBy('history.id', 'desc');
        if (!empty($this->_fullname)) $collection->where('users.fullname', 'like', '%' . $this->_fullname . '%');
        if (!empty($this->_router)) {
            $collection->where(function ($query) {
                $query->where('permission.name', 'like', '%' . $this->_router . '%')
                    ->orWhere('function_name', 'like', '%' . $this->_router . '%');
            });
        }
        if (!empty($this->_startdate)) $collection->where('history.created_at', '>=', date('Y-m-d',strtotime(str_replace('/', '-', $this->_startdate))) . ' 00:00:00');
        if (!empty($this->_enddate)) $collection->where('history.created_at', '<=', date('Y-m-d',strtotime(str_replace('/', '-', $this->_enddate))) . ' 23:59:59');
        $this->_totalCollection = $collection->count();
        $collection = $collection->take($this->_limit)->skip($this->_offset);
        return $collection->get(array('history.*', 'users.fullname', 'users.username', 'permission.name as name'));
    }

    public function getAjaxJson()
    {
        $this->getCollection($hasAuth = true);
        $response = array(
            "rows" => $this->_collection,
            "total" => $this->_totalCollection
        );
        return $response;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $currentDate = strtotime(date("Y-m-d"));
        $startDate = strtotime(str_replace('/', '-', $this->_startdate));
        if($startDate > $currentDate) return $this->wrongStartDate();
        return view('admin.history.index')->with(
            array(
                'response_view' => $this->_request,
                'ajaxFliterUrl' => $this->getAjaxFilterUrl(),
                'total'         => $this->_totalCollection
            ));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function ajax()
    {
        return response()->json($this->getAjaxJson());
    }

    public function wrongStartDate(){
        return redirect()->route('admin_history_view')->with('error', 'Tìm kiếm từ ngày không được lớn hơn ngày hiện tại');
    }

}