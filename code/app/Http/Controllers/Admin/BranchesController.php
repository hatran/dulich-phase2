<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ClubOfBranch\ClubOfBranchRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\Pagination;
use App\Constants\MemberConstants;
use App\Repositories\Branches\BranchesInterface;
use App\Exceptions\Validation\ValidationException;
use App\Repositories\Employees\EmployeesRepository;
use App\Libs\Helpers\LogsHelper;
use App\Models\Branches;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Input;
use App\Providers\LogServiceProvider;
use App\Libs\Helpers\Utils;

class BranchesController extends Controller {

    const PAGINATOR = 'paginator';
    const RESPONSE_VIEW = 'response_view';
    const POSITION_KEY = 'position';
    const STATUS_PUBLIC = 1;
    const STATUS_DRAFT = 0;

    protected $branches;
    public static $statusOption = [
        self::STATUS_PUBLIC => 'Hoạt động',
        self::STATUS_DRAFT => 'Không hoạt động'
    ];

    public function __construct(BranchesInterface $branches) {
        $this->branches = $branches;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        // list and search
        $page = $request->get('page', 1);
        $pagiData = $this->branches->paginate($page, $this->branches->perPage, $request->all());
        if (empty($pagiData)) {
            abort(404);
        }
        $branches = Pagination::makeLengthAware($pagiData->items, $pagiData->totalItems, $this->branches->perPage);

        // get VPĐD
        $representative_office = $this->branches->getVPDD();

        return view('admin.branches.index', [
            'branches' => $branches,
            'totalItems' => $pagiData->totalItems,
            'offsets' => ($page - 1) * $this->branches->perPage,
            'representative_office' => $representative_office,
            'statusOption' => self::$statusOption,
            self::RESPONSE_VIEW => $request->all(),
            self::PAGINATOR => $branches->count() ? $branches->appends($request->all()) : null
        ]);
    }

    /**
     * Show the form for creating a branches resource.
     *
     * @return Response
     */
    public function create() {
        $representative_office = $this->branches->getVPDD();

        return view('admin.branches.create', ['representative_office' => $representative_office]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        try {
            $this->branches->create($request->all());
            //LogServiceProvider::createSystemHistory(trans('history.fn_create_branche'), $request->all());
            return response()->json(['savedData' => true, 'message' => 'Thành công', 'type' => 'success']);
        } catch (ValidationException $e) {
            return response()->json(['savedData' => false, 'errorMessages' => $e->getErrors()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->branches->find($id);
        $representative_office = $this->branches->getVPDD();

        return view('admin.branches.edit', ['detail' => $detail, 'representative_office' => $representative_office]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->branches->find($id);
        if (empty($detail)) {
            abort(404);
        }
        $representative_office = $this->branches->getVPDD();

        return view('admin.branches.detail', [
            'detail' => $detail,
            'representative_office' => $representative_office
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        if (empty($id)) {
            abort(404);
        }
        try {
            $this->branches->update($id, $request->all());
            //LogServiceProvider::createSystemHistory(trans('history.fn_save_branche'), $request->all());
            return response()->json(['savedData' => true, 'message' => 'Thành công', 'type' => 'success']);
        } catch (ValidationException $e) {
            return response()->json(['savedData' => false, 'errorMessages' => $e->getErrors()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (empty($id)) {
            abort(404);
        }

        $deleted = $this->branches->delete($id);
        if ($deleted['deleted']) {
            //LogServiceProvider::createSystemHistory(trans('history.fn_delete_branche'), $deleted);
            return redirect()->route('branches.index')->with('successes', [$deleted['messages']]);
        }

        return redirect()->route('branches.index')->withErrors($deleted['messages']);
    }

    public function branchManager($id, Request $request) {
        if (empty($id)) {
            abort(404);
        }
        $detailBranch = $this->branches->find($id);
        $status = true;
        if (empty($detailBranch) || $detailBranch->status != \App\Models\Branches::BRANCHES_ACTIVE) {
            $status = false;
        }
        if (!$status && $request->ajax()){
            return response()->json(['status' => $status]);
        } elseif (!$status && !$request->ajax()) {
            abort(404);
        }
        $listManager = $this->branches->getEmployeesPositionByBranchId($id);
        $listOption = $this->branches->getOptionsByKey(self::POSITION_KEY);
        return view('admin.branches.manager', [
            'detailBranch' => $detailBranch,
            'listManager' => $listManager,
            'listOption' => $listOption
        ]);
    }

    public function ajaxFindMember(Request $request, EmployeesRepository $employees) {
        $employees = $employees->getListEmployeesByOptionId(1, $request->get('query', ''));
        return response()->json($employees);
    }

    public function ajaxSaveMember(Request $request) {
        try {
            $saved = $this->branches->saveEmployeesPosition($request->all());
            if (!empty($saved)) {
                // LogServiceProvider::createSystemHistory(trans('history.fn_save_employee_branche'), $request->all());
                return response()->json($saved);
            }

            //return response()->json($saved);
        } catch (\Exception $e) {
            LogsHelper::trackByFile('ajaxSaveMemberFail', 'Error when update member payment(Exception: ' . $e->getMessage() . ' on Line ' . $e->getLine() . ' in File ' . $e->getFile() . ')');
        }
    }

    public function exportExcel($id) {
        if (empty($id)) {
            abort(404);
        }
        $detailBranch = $this->branches->find($id);
        if (empty($detailBranch)) {
            return response()->json(['error' => 'Chi hội không tồn tại hoặc bị xóa.']);
        }
        $listManager = $this->branches->getEmployeesPositionByBranchId($id);
        $clubOfBranch = new ClubOfBranchRepository(new Branches());
        $listMemberByClubOfBranchId = $clubOfBranch->getListMemberClubOfBranchByBranchId($id);
        $clause = [
                ['status', MemberConstants::MEMBER_OFFICIAL_MEMBER],
                ['is_delete', null],
                ['typeOfPlace', \App\Constants\BranchConstants::TYPE_OF_PLACE_CHI_HOI],
                ['province_code', $id]
        ];

        $listMember = $this->branches->getMemberByCondition($clause, $listMemberByClubOfBranchId);
        $listOption = $this->branches->getOptionsByKey(self::POSITION_KEY);
        $listLanguage = \App\Models\Language::getAllLanguages();       
        $fileName = 'Danh_Sach_hoi_vien_Chi_Hoi_ [' . str_replace(' ', '_', Utils::stripUnicode($detailBranch->name)) . ']' . date('d/m/Y');

        Excel::create($fileName, function($excel) use ($detailBranch, $listManager, $listMember, $listOption, $listLanguage) {
            $excel->setTitle('HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM');
            $excel->sheet('First sheet', function($sheet) use ($detailBranch, $listManager, $listMember, $listOption, $listLanguage) {
                $sheet->loadView('admin.branches.export_excel', [
                    'listManager' => $listManager,
                    'listMember' => $listMember,
                    'detailBranch' => $detailBranch,
                    'listOption' => $listOption,
                    'listLanguage' => $listLanguage
                ]);
            });
        })->download('xlsx');
    }

}
