<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libs\Helpers\Utils;
use App\Models\Branches;
use App\Models\MemberEvaluation;
use App\Models\MemberFileAward;
use App\Models\Option;
use App\Providers\EvaluationCriteriaProvider;
use App\Providers\LogServiceProvider;
use DomPDF;
use Illuminate\Http\Request;
use App\Models\MemberRank;
use App\Providers\MemberServiceProvider;
use App\Models\Emails;
use App\Constants\CommontConstants;
use App\Jobs\SendEmail;
use App\Libs\Helpers\LogsHelper;
use App\Constants\MemberConstants;
use DB;
use Illuminate\Support\Facades\Auth;

class MemberRankController extends Controller
{
    const BCC_EMAIL = 'hoihdv@gmail.com';
    const EMAIL06 = 'EMAIL06';

    public function updateScoreFileAndAward(Request $request) {
        try {
            $attributes = $request->all();
            MemberFileAward::query()->updateOrCreate(
                [
                    'member_id' => $attributes['member_id_edit'],
                    'rank_id' => $attributes['rank_id_edit'],
                ],
                [
                    'score_file' => $attributes['scorefile_edit'],
                    'score_award' => $attributes['scoreaward_edit'],
                ]
            );
            return back()->with('successes', ['Đã cập nhật điểm hồ sơ, điểm giải thưởng thành công']);
        } catch (\Exception $e) {
            return back()->withErrors(['Cập nhật điểm hồ sơ, điểm giải thưởng thất bại']);
        }

    }

    public function deleteMemberRank($id) {
        $member_rank = MemberRank::query()->where('id', $id)->first();
        if (empty($member_rank)) {
            return back()->withErrors(['Không tồn tại hạng HDV ở hồ sơ này']);
        }

        $member = MemberServiceProvider::getMemberById($member_rank->member_id);
        if (empty($member)) {
            return back()->withErrors(['Không tồn tại hồ sơ này']);
        }

        if ($member->rank_status == MemberConstants::RANK_RANKED) {
            return back()->withErrors(['Không được xóa HDV đã được xếp hạng']);
        }

        try {
            DB::beginTransaction();
            MemberRank::query()
                ->where('member_id', $member_rank->member_id)
                ->where('rank_id', $member_rank->rank_id)
                ->delete();

            MemberEvaluation::query()
                ->where('rank_id', $member_rank->rank_id)
                ->where('member_id', $member_rank->member_id)
                ->delete();

            MemberFileAward::query()
                ->where('rank_id', $member_rank->rank_id)
                ->where('member_id', $member_rank->member_id)
                ->delete();

            $memberRankObj = MemberRank::query()
                ->select('member_rank.*', 'member_ranked.id as rank_registered')
                ->leftJoin('member_ranked', function ($join) {
                    $join->on('member_rank.member_id', '=', 'member_ranked.member_id')
                        ->on('member_rank.rank_id', '=', 'member_ranked.rank_id');
                })
                ->where('member_rank.is_deleted', 0)
                ->where('member_rank.member_id', $member_rank->member_id)
                ->groupBy(['member_rank.rank_id'])
                ->orderBy('member_rank.rank_id', 'asc')
                ->first();

            if ($memberRankObj) {
                if ($memberRankObj->rank_registered) {
                    $member->rank_status = MemberConstants::RANK_RANKED;
                } else {
                    $member->rank_status = MemberConstants::RANK_REGISTER;
                }
            } else {
                $member->rank_status = null;
            }

            $member->save();
            DB::commit();

            return back()->with('successes', ['Xóa đăng ký xếp hạng HDV thành công']);
        } catch (\Exception $e) {
            DB::rollback();
            return back()->withErrors(['Không xóa được hạng HDV này']);
        }
    }

    public function updateMemberRankFile($id) {
        $member_rank = MemberRank::where('id', $id)->first();
        if (empty($member_rank)) {
            return back()->withErrors(['Không tồn tại hạng HDV ở hồ sơ này']);
        }

        $member = MemberServiceProvider::getMemberById($member_rank->member_id);
        if (empty($member)) {
            return back()->withErrors(['Không tồn tại hồ sơ này']);
        }

        $member->rank_status = MemberConstants::RANK_UPDATE_FILE;
        $member->save();
        $mailTemplate = 'rank_file';
        self::_sendApprovedMail($member->fullName, $member->firstEmail, $mailTemplate);

        return back()->with('successes', ['Cập nhật trạng thái HDV đủ điều kiện ham gia xếp hạng HDV thành công']);
        
    }

    private static function _sendApprovedMail($name, $email, $mailTemplate)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $name]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = 'Thông báo đã nhận được hồ sơ đăng ký xếp hạng';
                $getEmail = Emails::select('content', 'attach')->where('option_code', self::EMAIL06)->first();
                $attach = null;
                if (!empty($getEmail)) {
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($name), $getEmail->content);
                    
                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }

                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
               
                $template = 'admin.member_rank.email.' . $mailTemplate;
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject, $attach))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_rank_register_file', 'Error when send rank register file mail(Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$name], true) . ')');
            }
        }
    }

    public function downloadfile(Request $request)
    {
        if (! Auth::check()) {
            return 'Đã có lỗi xảy ra';
        }
        $attribute = $request->all();
        $memberId = $attribute['memberId'] ?? 0;
        $member = MemberServiceProvider::getMemberById($memberId);
        if (empty($member) || ! $member->rank_status) {
            return 'Đã có lỗi xảy ra';
        }

        $provincial = Option::query()
            ->where('status', 1)
            ->where('key', 'provincial')
            ->pluck('value', 'id')
            ->all();
        $branch_chihoi = Branches::query()
            ->where('option_code', 'BRANCHES01')
            ->where('status', 1)
            ->pluck('name', 'id')
            ->all();
        $branch_clbthuochoi = Branches::query()
            ->where('option_code', 'BRANCHES03')
            ->where('status', 1)
            ->pluck('name', 'id')
            ->all();
        $member_typeOfTravelGuide = MemberConstants::$member_typeOfTravelGuide;

        $member_rank = MemberRank::fetchInfoRankByMemberId($memberId);
        if (empty($member_rank)) {
            return 'Đã có lỗi xảy ra';
        }

        list($base, $sub) = Utils::getBaseAndSubjectKnowledge($member_rank->rank_id);
        $percentage = EvaluationCriteriaProvider::getPercentage();
        $criteria = EvaluationCriteriaProvider::getEvaluationCriteria();
        $companyEvaluationList = MemberEvaluation::fetchCompanyEvaluation($member_rank->rank_id, $memberId);
        $query = " 
            (
                select 
                    count(id) as total,
                    count(DISTINCT taxcode_company) as cnt,
                    ROUND(SUM(criteria1) / count(id)) as ct1,
                    ROUND(SUM(criteria2) / count(id)) as ct2,
                    ROUND(SUM(criteria3) / count(id)) as ct3,
                    ROUND(SUM(criteria4) / count(id)) as ct4,
                    ROUND(SUM(criteria5) / count(id)) as ct5,
                    criteria1_code, 
                    criteria2_code,
                    criteria3_code,
                    criteria4_code,
                    criteria5_code
                from
                    member_evaluation
                where 
                    (select count(*) from member_evaluation m1 where m1.member_id = member_evaluation.member_id and m1.rank_id <= member_evaluation.rank_id and m1.id >= member_evaluation.id order by m1.rank_id asc) <= 30
                and    
                    member_id = $memberId
                and
                    rank_id = $member_rank->rank_id
                group by criteria1_code, 
                    criteria2_code,
                    criteria3_code,
                    criteria4_code,
                    criteria5_code
            )
        ";
        $total = DB::select($query);
        $profileImg = empty($member->profileImg) ? '' : $member->profileImg;
        $folder = 'files';
        $spl = explode($folder, $profileImg);
        if (count($spl) > 1) {
            $profileImg = public_path($folder.$spl[1]);
        }
        
        $dataView =  [
            'total' => $total,
            'member_rank' => $member_rank,
            'percentage' => $percentage,
            'criteria' => $criteria,
            'member' => $member,
            'sub' => $sub,
            'base' => $base,
            'provincial' => $provincial,
            'branch_chihoi' => $branch_chihoi,
            'branch_clbthuochoi' => $branch_clbthuochoi,
            CommontConstants::TYPE_OF_PLACE => empty($member->typeOfPlace) ? '' : $member->typeOfPlace,
            CommontConstants::TYPE_OF_TRAVEL_GUIDE => empty($member->typeOfTravelGuide) ? '' : $member->typeOfTravelGuide,
            'member_typeOfTravelGuide' => $member_typeOfTravelGuide,
            CommontConstants::PROFILE_IMG => $profileImg,
            'isProfile' => false,
            'isAdmin' => isset($attribute['memberId']) ? $attribute['memberId'] : '',
            'companyEvaluationList' => $companyEvaluationList,
        ];

        try {
            $pdfDownloadMemberRankInfo = DomPDF::loadView('admin.member_rank.pdf.info', $dataView);
            LogServiceProvider::createSystemHistory(trans('history.fn_download_rank_file'), ['memberId' => $member->id]);
            return $pdfDownloadMemberRankInfo->download($member->touristGuideCode. '_' . date('Ymd').'.pdf');
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('download_pdf_file_fail', 'Error when download pdf status (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$member->id], true) . ')');
            return 'Đã có lỗi xảy ra';
        }
    }
}
