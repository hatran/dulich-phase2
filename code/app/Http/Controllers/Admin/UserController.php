<?php
namespace App\Http\Controllers\Admin;

use App\Constants\BranchConstants;
use App\Constants\UserConstants;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\UserServiceProvider;
use App\Providers\MemberServiceProvider;
use App\Repositories\Branches\BranchesRepository;
use App\Repositories\Users\GroupsAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ClubOfBranch\ClubOfBranchRepository;
use App\Models\Branches;
use App\Repositories\ClubOfHead\ClubOfHeadRepository;
use Hash;
use App\Models\Member;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    const DEFAULT_LIMIT_USER_PER_PAGE = 10;
    const FULL_NAME = 'fullname';
    const STATUS = 'status';
    const PROVINCE_TYPE = 'province_type';
    const USERNAME = 'username';
    const EMAIL = 'email';
    const PWD_INPUT = 'password';
    const USERS_INDEX = 'users.index';
    const USERS_SHOW = 'users.show';
    const SUCCESSES = 'successes';
    const ROLE = 'role';

    public function index(Request $request)
    {
        $limit = $this::DEFAULT_LIMIT_USER_PER_PAGE;

        $groupAdmin = new GroupsAdmin();
        $groups = $groupAdmin->getListAll();

        $branches = new BranchesRepository(new Branches());
        $branchesVpdd = $branches->getVPDD();

        if (!empty($request->input('search'))) {
            $arrConditions = [
                $this::FULL_NAME     => $request->input($this::FULL_NAME, ''),
                $this::STATUS        => $request->input($this::STATUS, ''),
                $this::EMAIL         => $request->input($this::EMAIL, ''),
                $this::ROLE          => $request->input($this::ROLE, ''),
                $this::USERNAME      => $request->input($this::USERNAME, ''),
                $this::PROVINCE_TYPE => $request->input($this::PROVINCE_TYPE, ''),
            ];


            $objUsers = UserServiceProvider::searchByConditions($arrConditions, $limit);
        } else {
            $objUsers = UserServiceProvider::getAllUser($limit);
        }
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }

        return view('admin.user.index', [
            'groups'         => $groups,
            'objUsers'       => $objUsers,
            'response_view'  => empty($request) ? array() : $request,
            'paginator'      => (count($objUsers) > 0) ? $objUsers->appends(request()->except('page')) : null,
            'current_cursor' => $this->getCurrentCursor($limit, $request),
            'branchesVpdd'   => $branchesVpdd,
            'branch_chihoi' => $chihoi
        ]);
    }

    public function show ($id)
    {
        if (empty($id)) {
            $objUser = null;
        } else {
            $objUser = UserServiceProvider::getUserById($id);
        }

        if (empty($objUser)) {
            abort(404);
        }
        
        $groupAdmin = new GroupsAdmin();
        $groups = $groupAdmin->getListAll();

        $branches = new BranchesRepository(new Branches());
        $branchesVpdd = $branches->getVPDD();

        $branchesRepo = new ClubOfBranchRepository(new Branches());
        $clubOfHeadRepo = new ClubOfHeadRepository(new Branches());
        $listBranches = $branchesRepo->getListBranch();
        $listClub = $clubOfHeadRepo->getListClubOfHead();
        $oldRole = old('role') ?: $objUser->role;
        $oldClubId = old('club_id') ?: $objUser->club_id;
        $oldBranchId = old('branch_id') ?: $objUser->branch_id;
        $oldProvinceType = old('province_type') ?: $objUser->province_type;
        return view('admin.user.detail', [
            'objUser' => $objUser,
            'groups' => $groups,
            'listBranches' => $listBranches,
            'listClub' => $listClub,
            'oldRole' => $oldRole,
            'oldBranchId' => $oldBranchId,
            'oldClubId' => $oldClubId,
            'branchesVpdd' => $branchesVpdd,
            'oldProvinceType' => $oldProvinceType
        ]);
    }

    public function create ()
    {
        $groupAdmin = new GroupsAdmin();
        $groups = $groupAdmin->getListAll();

        $branches = new BranchesRepository(new Branches());
        $branchesVpdd = $branches->getVPDD();
        $branchesRepo = new ClubOfBranchRepository(new Branches());
        $clubOfHeadRepo = new ClubOfHeadRepository(new Branches());
        $listBranches = $branchesRepo->getListBranch();
        $listClub = $clubOfHeadRepo->getListClubOfHead();

        return view('admin.user.register', [
            'listBranches' => $listBranches,
            'listClub' => $listClub,
            'groups' => $groups,
            'branchesVpdd' => $branchesVpdd
        ]);
    }

    public function store (Request $request)
    {
        $groupAdmin = new GroupsAdmin();
        $groups = $groupAdmin->getListAll();

        $rules = User::$rule;
        $rules['role'] = 'required|in:' . implode(',', array_keys($groups));
        $rules['branch_id'] = 'required_if:province_type,66,67,68';
        $rules[User::PROVINCE_TYPE] = 'required';
        $validator = Validator::make($request->all(), $rules, User::$message);

        $validator->setAttributeNames(User::$niceAttributeName);

        if ($validator->fails()) {
            return redirect()
                ->route('users.create')
                ->withErrors($validator)
                ->withInput();
        }

       
        $checkDuplicate = User::where($this::USERNAME, $request->input($this::USERNAME))->onlyTrashed()->first();

        if($checkDuplicate != null) {
           return redirect()
                ->route('users.create')
                ->withErrors(['Tên đăng nhập  đã tồn tại'])
                ->withInput();
        }
        $is_traveler_company = null;
        if ($request->input('role') == 100054) {
            $is_traveler_company = 1;
        }

        $objUser = User::create([
            $this::FULL_NAME     => $request->input($this::FULL_NAME),
            $this::USERNAME      => $request->input($this::USERNAME),
            $this::EMAIL         => $request->input($this::EMAIL),
            'role'               => $request->input('role'),
            $this::STATUS        => $request->input($this::STATUS),
            $this::PROVINCE_TYPE => $request->input($this::PROVINCE_TYPE) === "all" ? null : $request->input($this::PROVINCE_TYPE),
            $this::PWD_INPUT     => bcrypt($request->input($this::PWD_INPUT)),
            'branch_id'          => $request->input('branch_id'),
            'club_id'            => $request->input('club_id'),
            'is_traveler_company'=> $is_traveler_company
        ]);

        if (empty($objUser)) {
            return redirect()->route('users.create')->withErrors(['Tạo mới người dùng thất bại']);
        }

        return redirect()->route($this::USERS_INDEX)->with($this::SUCCESSES, ['Tạo mới người dùng thành công']);
    }

    public function update (Request $request, $userId)
    {
        if (empty($userId)) {
            abort(404);
        }

        $objUser = UserServiceProvider::getUserById($userId);
        if (empty($objUser)) {
            abort(404);
        }

        $rule = User::$rule;

        if ($objUser->username == $request->input('username')) {
            unset($rule['username']);
            $request->merge(['username' => null]);
        }

        if ($objUser->email == $request->input('email')) {
            unset($rule['email']);
            $request->merge(['email' => null]);
        }

        if (empty($request->input('password')) && !empty($objUser->password)) {
            unset($rule['password'], $rule['password_confirmation']);
            $request->merge(['password' => null]);
            $request->merge(['password_confirmation' => null]);
        }

        $groupAdmin = new GroupsAdmin();
        $groups = $groupAdmin->getListAll();
        if (!empty($request->get('role'))) {
            $rule['role'] = 'required|in:' . implode(',', array_keys($groups));
        }
        $rules['branch_id'] = 'required_if:province_type,66,67,68';
        $rules[User::PROVINCE_TYPE] = 'required';

        $validator = Validator::make($request->all(), $rule, User::$message);
        $validator->setAttributeNames(User::$niceAttributeName);

        if ($validator->fails()) {
            return redirect()
                ->route($this::USERS_SHOW, [$userId])
                ->withErrors($validator)
                ->withInput();
        }

        $arrUpdateData = [
            'id'                 => $userId,
            $this::FULL_NAME     => $request->input($this::FULL_NAME),
            $this::USERNAME      => $request->input($this::USERNAME),
            $this::EMAIL         => $request->input($this::EMAIL),
            'role'               => $request->input('role'),
            $this::STATUS        => $request->input($this::STATUS),
            $this::PROVINCE_TYPE => $request->input($this::PROVINCE_TYPE) === 'all' ? null : $request->input($this::PROVINCE_TYPE),
            $this::PWD_INPUT     => empty($request->input($this::PWD_INPUT)) ? null : bcrypt($request->input($this::PWD_INPUT)),
            'branch_id'          => $request->input('branch_id'),
            'club_id'            => $request->input('club_id'),
        ];

        $objUser = UserServiceProvider::update($arrUpdateData);

        if (empty($objUser)) {
            return redirect()->route($this::USERS_INDEX, [$userId])->withErrors(['Cập nhật người dùng thất bại']);
        }

        MemberServiceProvider::updateMemberUpdatedAt($userId);
        return redirect()->route($this::USERS_INDEX, [$userId])->with($this::SUCCESSES, ['Cập nhật người dùng thành công']);
    }

    public function destroy ($userId)
    {
        if (empty($userId)) {
            abort(404);
        }

        $objUser = UserServiceProvider::delete($userId);

        if (empty($objUser)) {
            return redirect()->route($this::USERS_INDEX)->withErrors(['Xóa người dùng thất bại']);
        } else {
            return redirect()->route($this::USERS_INDEX)->with($this::SUCCESSES, ['Xóa người dùng thành công']);
        }
    }

    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }

    public function showChangePasswordForm(Request $request){
        if ($request->isMethod('post')){

            $rule = [
                'old_password' => 'required|pwdvalidation',
                'password' => User::$rule['password'] . '|different:old_password',
                'password_confirmation' => User::$rule['password_confirmation'],
            ];
            Validator::extend('pwdvalidation', function($field, $value, $parameters)
            {
                return Hash::check($value, auth()->user()->password);
            });
            $messages = User::$message;
            $messages['pwdvalidation'] = 'Mật khẩu cũ không chính xác';
            $messages['different'] = 'Mật khẩu mới phải không trùng với mật khẩu cũ';
            $validator = Validator::make($request->all(), $rule, $messages);
            $validator->setAttributeNames(User::$niceAttributeName);

            if ($validator->fails()) {
                return redirect(url()->current())
                    ->withErrors($validator)
                    ->withInput();
            }

            $objUser = UserServiceProvider::update([
                'id' => auth()->user()->id,
                'password' => bcrypt($request->get('password'))
            ]);
            if (empty($objUser)) {
                return redirect(url()->current())->withErrors(['Thay đổi mật khẩu thất bại.']);
            } else {
                MemberServiceProvider::updateMemberUpdatedAt(auth()->user()->id);
                return redirect(url()->current())->with
                ($this::SUCCESSES, ['Thay đổi mật khẩu thành công.']);
            }
        }

        return view('admin.user.changepassword');
    }

    public function showProfile() {
        return view('admin.user.profile', ['user' => auth()->user()]);
    }

    public function resetPassword($id, Request $request) {
        $objUser = UserServiceProvider::update([
            'id' => $id,
            'password' => bcrypt($request->input('input-password'))
        ]);
        if (empty($objUser)) {
            return redirect()->route($this::USERS_INDEX)->withErrors(['Reset Password cho người dùng: '.$objUser->fullnameate.' thất bại']);
        } else {
            MemberServiceProvider::updateMemberUpdatedAt($id);
			UserServiceProvider::sendPasswordResettingInfoMail($id, $request->input('input-password'), 'password_resetting_info_mail');
            return redirect()->route($this::USERS_INDEX)->with($this::SUCCESSES, ['Reset Password cho người dùng: '.$objUser->fullname .' thành công']);
        }
    }

    public function createNotMember()
    {
        $groupAdmin = new GroupsAdmin();
        $groups = $groupAdmin->getListAll();

        $branches = new BranchesRepository(new Branches());
        $branchesVpdd = $branches->getVPDD();
        $clubOfHeadRepo = new ClubOfHeadRepository(new Branches());
        $listBranches = [
            2 => BranchConstants::$name[2],
            4 => BranchConstants::$name[4]
        ];
        $listClub = $clubOfHeadRepo->getListClubOfHead();

        return view('admin.user.registernotmember', [
            'listBranches' => $listBranches,
            'listClub' => $listClub,
            'groups' => $groups,
            'branchesVpdd' => $branchesVpdd
        ]);
    }

    public function storeNotMember (Request $request)
    {
        $rules = User::$rule;
        $rules['branch_id'] = 'required';
        $rules['first_mobile'] = 'required';
        unset($rules['password_confirmation']);
        unset($rules['password']);
        unset($rules['role']);
        unset($rules['status']);
        $rules['password'] = 'required';
        $messages = User::$message;
        $messages['branch_id.required'] = 'Bạn phải chọn Chi Hội';
        $messages['first_mobile.required'] = 'Số điện thoại là trường bắt buộc';
        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames(User::$niceAttributeName);
        if ($validator->fails()) {
            return redirect()
                ->route('users_create_notmember')
                ->withErrors($validator)
                ->withInput();
        }

        $inputEmail = $request->input($this::EMAIL);
        $emailUser = User::where('email', $inputEmail)->whereNull('deleted_at')->first();
        if (!empty($emailUser)) {
            return redirect()->route('users_create_notmember')->withErrors(['Email đã tồn tại. Vui lòng chọn email khác']);
        }
        $fullName = $request->input($this::FULL_NAME);
        $userName = $request->input($this::USERNAME);
        try {
            DB::beginTransaction();
            $emailMember = Member::query()->where(function ($query) use ($inputEmail)  {
                $query->where('firstEmail', $inputEmail)
                    ->orWhere('secondEmail', $inputEmail);
            })->whereNull('is_delete')->first();
            if (!empty($emailMember)) {
                return back()->withInput()->withErrors(['Email đã tồn tại. Vui lòng chọn email khác']);
            }
            $objMem = new Member();
            $objMem->fullName = $fullName;
            $objMem->firstEmail = $inputEmail;
            $objMem->firstMobile = $request->input('first_mobile');
            $objMem->status = -1;
            $objMem->save();

            User::query()->create([
                $this::FULL_NAME     => $fullName,
                $this::USERNAME      => $userName,
                $this::EMAIL         => $inputEmail,
                'role'               => 1,
                $this::STATUS        => 1,
                $this::PWD_INPUT     => bcrypt($request->input($this::PWD_INPUT)),
                'branch_id'          => $request->input('branch_id'),
                'memberId'           => $objMem->id,
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('users_create_notmember')->withErrors(['Tạo mới người dùng không phải hội viên thất bại']);
        }
        return redirect()->route($this::USERS_INDEX)->with($this::SUCCESSES, ['Tạo mới người dùng không phải hội viên thành công']);
    }
}
