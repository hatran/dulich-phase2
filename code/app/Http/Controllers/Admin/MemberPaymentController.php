<?php

namespace App\Http\Controllers\Admin;

use App\Constants\PaymentConstants;
use App\Http\Requests\ImportExcelRequest;
use App\Libs\Helpers\LogsHelper;
use App\Libs\Mail\MailHelper;
use App\Providers\LogServiceProvider;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory as ReaderFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Constants\MemberConstants;
use App\Http\Controllers\Controller;
use App\Providers\MemberPaymentsServiceProvider;
use App\Providers\MemberPaymentsYearlyServiceProvider;
use App\Providers\MemberServiceProvider;
use App\Providers\NoteServiceProvider;
use App\Libs\Helpers\Utils;
use Validator;
use Response;
use Carbon\Carbon;
use App\Constants\CommontConstants;
use App\Models\Offices;
use App\Providers\ListMemberServiceProvider;
use App\Jobs\SendEmail;
use App\Jobs\SendSms;
use App\Models\Emails;
use App\Models\MemberPayments;

class MemberPaymentController extends Controller
{
    const NUMBER_PAYMENT = 'number_payment';

    const NUMBER_PAYMENT_MAX_LENGTH = 20;
    const PAYMENT_CONTENT_MAX_LENGTH = 500;
    const PAYMENT_NOTE_MAX_LENGTH = 500;
    const PAYMENT_NUMBER_MAX_LENGTH = 10;
    const TOANQUOC = 0;

    protected $rules =
    [
        'number_payment' => 'required|max:20',
        'date_payment' => 'required',
        'currency' => 'required|numeric',
        //'content' => 'required|max:500',
        'note' => 'nullable|max:500',
    ];

    protected $messages = [
        'required' => ':attribute là trường bắt buộc phải điền.',
    ];

    protected $attributes = [
        'number_payment'                => 'Số chứng từ',
        'date_payment'                  => 'Ngày chứng từ',
        'currency'                      => 'Số tiền ',
        //'content'                       => 'Nội dung nộp tiền'
    ];
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->input(CommontConstants::STATUS, '');
        if($status && !in_array($status, [MemberConstants::FEE_WAITING, MemberConstants::SIGNING_WAITING])) {
            abort(404);
        }

        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $allMembers = $this->_searchByCondition($request, $limit, True);
        $objMembers = $this->_searchByCondition($request, $limit);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }

        $searchCommon = $this->getStatusByStep();
        //$searchByRoleOrArea = $this->getSearchByRoleOrArea($request);

        return view('admin.payment.index', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            // CommontConstants::AREA_OR_ROLE => $searchByRoleOrArea,
            // CommontConstants::HN_SEARCH => $this->searchByHnArea(),
            // CommontConstants::DN_SEARCH => $this->searchByDnArea(),
            // CommontConstants::HCM_SEARCH => $this->searchByHcmArea(),
            CommontConstants::ALL_SEARCH => $this->searchByAllArea(),
            CommontConstants::COUNT_MEMBER => count($allMembers),
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ? $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'offices' => Offices::searchByAllOffices()
        ]);
    }


    private function _searchByCondition ($request, $limit,$isCount = false)
    {
        if (!empty($request->input(CommontConstants::FROM_DATE))) {
            $fromDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::FROM_DATE, ''))->toDateString();
        } else {
            $fromDate = '';
        }

        if (!empty($request->input(CommontConstants::TO_DATE))) {
            $toDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::TO_DATE, ''))->toDateString();
        } else {
            $toDate = '';
        }

        if (!empty($request->input(CommontConstants::PROVINCE_TYPE))) {
            $getProvinceType = $this->searchByProvinceType($request->input(CommontConstants::PROVINCE_TYPE));
        }
        else {
            $getProvinceType = '';
        }

        $arrConditions = [
            self::NUMBER_PAYMENT => $request->input(self::NUMBER_PAYMENT, ''),
            CommontConstants::FULL_NAME => $request->input(CommontConstants::FULL_NAME),
            CommontConstants::TOURIST_GUIDE_CODE => $request->input(CommontConstants::TOURIST_GUIDE_CODE),
            CommontConstants::FILE_CODE => $request->input(CommontConstants::FILE_CODE),
            CommontConstants::STATUS => $request->input(CommontConstants::STATUS),
            CommontConstants::PROVINCE_CODE => $request->input(CommontConstants::PROVINCE_CODE),
            CommontConstants::PROVINCE_TYPE => $getProvinceType,
            CommontConstants::FROM_DATE => $fromDate,
            CommontConstants::TO_DATE => $toDate,

        ];
        
        return MemberPaymentsServiceProvider::searchMemberByConditionsPayment($arrConditions, $limit,$isCount);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        if (empty($id)) {
            $objMember = null;
            $objMemberPayment =  null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
            $objMemberPayment = MemberPaymentsServiceProvider::getMemberPaymentByMemberId($id);

            if (!is_null($objMember)) {
                $objMember['formattedBirthday'] = $objMember->formatted_birthday;
            }
            
            if (!is_null($objMemberPayment)) {
                $objMemberPayment['formattedCurrency'] = $objMemberPayment->formatted_currency;
            }
        }

        if (empty($objMember)) {
            abort(404);
        }
        
        if ($objMember->is_delete) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã bị xóa.']]);
        }

        return response()->json(['objMember' => $objMember, 'objMemberPayment' => $objMemberPayment]);
    }

     /* Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
        }

        if (empty($objMember)) {
            abort(404);
        }

        return view('admin.payment.create_edit', [
            'objMember' => $objMember
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (empty($id)) {
            $objMember = null;
            $objMemberPayment =  null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
            $objMemberPayment = MemberPaymentsServiceProvider::getMemberPaymentByMemberId($id);
        }
        if (empty($objMember)) {
            abort(404);
        }

        return view('admin.payment.update', [
            'objMember' => $objMember,
            'objMemberPayment'=>$objMemberPayment,
        ]);
    }

    public function update($id, Request $request)
    {
        // check status trước khi update.
        if (empty($id) || $request->input('status') != MemberConstants::SIGNING_WAITING) {
            return Response::json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này")));
        }
        $objMember = MemberServiceProvider::getMemberById($id);

        if (empty($objMember)) {
           return Response::json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này")));
        }
        
        if ($objMember->is_delete) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã bị xóa.']]);
        }
        $isStatus = MemberServiceProvider::isStatus($id, MemberConstants::SIGNING_WAITING);

        if(!$isStatus) {
            return response()->json(['errors' => ['memberInvalid' => 'Hồ sơ này đã được hoàn tất xử lý trước đó.']]);
        }

        $validator = Validator::make(Input::all(), $this->rules, $this->messages, $this->attributes);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            // $data = [];
            // foreach ($this->rules as $key => $value) {
            //     $data[$key] = $request->input($key);
            // }
            $data = $request->all();
			$objNumberPayment = MemberPaymentsServiceProvider::_getPaymentsByMemberId($id);
            $data['old_number_payment'] = $objNumberPayment->number_payment;
            $objPayment = MemberPaymentsServiceProvider::updatePayment($data, $id);
            MemberPaymentsYearlyServiceProvider::update($data, $id);
            $countDataYear = count($data['years']);
            //update member_code_expiration into members table
            $objMember->member_code_expiration = $data['years'][$countDataYear - 1].'-12-31 00:00:00';
            $objMember->save();
            NoteServiceProvider::createNote($id, $data['note'], MemberConstants::IS_PAID_ACTION, MemberConstants::SIGNING_WAITING);
			
            if (empty($objPayment)) {
                return response()->json(['errors' => ['memberInvalid' => 'Cập nhật thất bại.']]);
            } else {
                return response()->json(['success' => 'Cập nhật thành công']);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        if (empty($id) || $request->input('status') != MemberConstants::FEE_WAITING) {
            return Response::json(array('errors' => array("memberInvalid" =>"không được nộp phí cho hồ sơ có trạng thái chưa hợp lệ.")));
        }
        $objMember = MemberServiceProvider::getMemberById($id);
        
        if (empty($objMember)) {
           return Response::json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này . ")));
        }

        if ($objMember->is_delete) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã bị xóa.']]);
        }
        $isStatus = MemberServiceProvider::isStatus($id, MemberConstants::FEE_WAITING);

        if(!$isStatus) {
            return response()->json(['errors' => ['memberInvalid' => 'Hồ sơ này đã được hoàn tất xử lý trước đó.']]);
        }

        $validator = Validator::make(Input::all(), $this->rules, $this->messages, $this->attributes);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        
        //$data = [];
        // foreach ($this->rules as $key => $value) {
        //     $data[$key] = $request->input($key);
        // }
        $data = $request->all();
  
        MemberPaymentsServiceProvider::createPayment($data, $id);
        MemberPaymentsYearlyServiceProvider::create($data, $id);
        $countDataYear = count($data['years']);
        //update member_code_expiration into members table
        $objMember->member_code_expiration = $data['years'][$countDataYear - 1].'-12-31 00:00:00';
        $objMember->save();
        
        
        NoteServiceProvider::createNote($id, $data['note'], MemberConstants::IS_PAID_ACTION, MemberConstants::SIGNING_WAITING);

        $firstMail = $objMember->firstEmail;
        $secondMail = $objMember->secondEmail;

        self::_sendPaymentMail($objMember, $firstMail, $data["currency"]);

        if (!empty($secondMail)) {
            self::_sendPaymentMail($objMember, $secondMail, $data["currency"]);
        }
     
        MemberServiceProvider::updateMemberStatus($id, MemberConstants::SIGNING_WAITING);
     
        return response()->json(['success' => 'Tạo mới thành công']);
    }

    private static function _sendPaymentMail ($objMember, $email, $feeMoney = 0)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $subject = '';
                $getEmail = Emails::select('id','content', 'title', 'attach')->where('option_code', 'EMAIL11')->where('branch_id', $objMember->province_code)->first();
                if (empty($getEmail)) {
                    $getEmail = Emails::select('id','content', 'title', 'attach')->where('option_code', 'EMAIL11')->where('branch_id', self::TOANQUOC)->first();
                }
                $attach = null;
                $feeMoney = MailHelper::currency_format($feeMoney);
                if (!empty($getEmail)) {
                    $subject = $getEmail->title;
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_HOIPHI_DANOP, $feeMoney, $getEmail->content);

                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }
                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
                $template = 'admin.payment.payment_email';
                dispatch((new SendEmail($template, $data, $to, '', $subject, $attach))->delay(0));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_payment_fail', 'Error when send mail payment (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember], true) . ')');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($numberpayment)
    {
        // if (empty($numberpayment)) {
        //     $objMember = null;
        //     $objMemberPayment = null;
        // } else {
        //     $objMember = MemberServiceProvider::getMemberById($id);
        //     $objMemberPayment = MemberPaymentsServiceProvider::getMemberPaymentByMemberId($id);
        // }

        $objMemberPayment = MemberPayments::where('number_payment', $numberpayment)->where('is_delete', 0)->first();

        if (empty($objMemberPayment)) {
            abort(404);
        }

        MemberPaymentsServiceProvider::deletePayment($numberpayment);
        // LogServiceProvider::createSystemHistory(trans('history.fn_delete_payment'), [
        //     'memberId' => $id,
        //     'payment' => $objMemberPayment
        // ]);
        MemberServiceProvider::updateMemberStatusByAccount($objMemberPayment->memberId,MemberConstants::FEE_WAITING);
        // LogServiceProvider::createSystemHistory(trans('history.fn_revert_status_payment'), [
        //     'memberId' => $id,
        //     'status' => MemberConstants::FEE_WAITING
        // ]);

        return response()->json(['success' => 'Xoá thành công']);
    }

    public function sendNotification(Request $request)
    {
        $idArr = $request->input('member-ids');
        if (!is_array($idArr)) {
            return response()->json(['errors' => 'Vui lòng chọn ít nhất 1 thành viên.']);
        }

        $ids = implode(',', $idArr);
        $objMembers = \App\Models\Member::find($idArr);

        if (is_null($objMembers)) {
            return response()->json(['errors' => 'Không tìm thấy thành viên nào.']);
        }

        $failedNames = [];
        foreach($objMembers as $objMember) {
            try {
                MemberServiceProvider::sendInformFee($objMember);
                // send sms
                $this->sendSms($objMember);
            } catch(\Exception $e) {
                array_push($failedNames, $objMember->fullName);
                //return response()->json(['errors' => $e->getMessage()]);
            }
        }

        $numberOfFailed = count($failedNames);
        if ($numberOfFailed == 0) {
            return response()->json(['success' => 'Đã gửi thông báo']);
        } elseif ($numberOfFailed == 1) {
            return response()->json(['errors' => 'Có lỗi xảy ra trong quá trình gửi thông báo cho thành viên <br>' . implode(', ', $failedNames) .  '<br> Vui lòng thử lại sau.']);
        } else {
            return response()->json(['errors' => 'Có lỗi xảy ra trong quá trình gửi thông báo cho các thành viên <br>' . implode(', ', $failedNames) .  '<br> Vui lòng thử lại sau.']);
        }
    }

    private function sendSms($objMember)
    {
        $view = View::make('admin.member.sms.inform_fee', ['objMember' => $objMember]);
        $smsContent = (string) $view;
        $firstMobile = $objMember->firstMobile;
        $secondMobile = $objMember->secondMobile;
        Utils::sendSms($firstMobile, $smsContent);
        if (!empty($secondMobile)) {
            Utils::sendSms($secondMobile, $smsContent);
        }
    }

    public function importExcel (Request $request)
    {
        $file = $request->file('fileToUpload');

        if(!$file) {
            return redirect()->route('admin_member_list_payment_view')->with('error_message', 'Không có file!');
        }

        $fileMine = $file->getClientMimeType();
        $fileType = $file->getClientOriginalExtension();
        $fileSize = $file->getClientSize();

        if ($fileSize > 10000000) {
            return redirect()->route('admin_member_list_payment_view')->withErrors('File quá lớn, dung lượng file cho phép tối đa là 10Mb!');
        }

        $mineRules = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel');

        if(!in_array($fileMine, $mineRules) || $fileType != Type::XLSX) {
            return redirect()->route('admin_member_list_payment_view')->withErrors('File không đúng định dạng!');
        }

        try {
            $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
            $reader->open($file);
        } catch (\Exception $exception) {
                LogsHelper::trackByFile('update_member_payment_fail', 'Error when update member payment(Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ')');
            return redirect()->route('admin_member_list_payment_view',['status' => MemberConstants::FEE_WAITING])->withErrors(['error_message' => 'Không thể đọc file.']);
        }

        $sheetList = $reader->getSheetIterator();

        if (empty($sheetList)) {
            return redirect()->route('admin_payment_fee_import_view');
        }

        DB::beginTransaction();
        foreach ($sheetList as $key => $sheet) {
            if ($key > 1) {
                break;
            }

            $recordValid = 0;
            $rowCount = 0;
            $errors = array();
            $arrFileCodesErr = [];

            foreach ($sheet->getRowIterator() as $keyRow => $arrRow) {
                if ($this->emptyArrayElement($arrRow)) continue;
                $rowCount++;
                if($keyRow == 3){
                    if($arrRow != ['TT', 'Mã Hồ sơ', 'Số chứng từ', 'Ngày chứng từ', 'Số tiền', 'Nội dung nộp tiền', 'Ghi chú', '']){
                        return redirect()->route('admin_member_list_payment_view')->withErrors('File không đúng định dạng!');
                    }
                }
                // do stuff with the row
                if ($keyRow > 3) {
                    // $arrRow[1] => file_code
                    if (!empty($arrRow[1])) {
                        // do update in members table
                        $objMember = MemberServiceProvider::getMemberExistByFileCode($arrRow[1]);

                        // file code is exist
                        if (empty($objMember) || empty($memberId = $objMember->id)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 2 - Không tìm thấy thành viên trong cơ sở dữ liệu.';

                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        if (!empty($objMember) && !empty($objMember->status)) {
                            // Chỉ status = 4 mới được nộp lệ phí
                            if ($objMember->status != MemberConstants::FEE_WAITING) {
                                // add file code in the row has wrong data type to $arrFileCodesErr
                                $arrFileCodesErr[] = $arrRow[1];
                                continue;
                            }
                        }

                        // number payment is exist and content length <= 20
                        if(strlen($arrRow[2]) > self::NUMBER_PAYMENT_MAX_LENGTH) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 3 - Số chứng từ không được lớn hơn 20 kí tự.';

                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        // payment date is required and format as d/m/y
                        $datePayment = $arrRow[3];

                        if (is_object($datePayment)) {
                            $datePayment = $datePayment->format(CommontConstants::D_M_Y_FORMAT);
                            $today = Carbon::today()->format(CommontConstants::D_M_Y_FORMAT);
                            $approvedAt  = $objMember->approved_at->format(CommontConstants::D_M_Y_FORMAT);
                        }

                        $datePaymentMysql = \DateTime::createFromFormat(CommontConstants::D_M_Y_FORMAT, $datePayment);
                        $datePaymentMysql = date_format($datePaymentMysql, 'Y-m-d H:i:s');

                        // if(empty($datePayment) || empty(Utils::validateDateFormat($datePayment,CommontConstants::D_M_Y_FORMAT)) ||
                        //     (!empty($approvedAt) && ($approvedAt <= $datePayment || ($today >= $datePayment)))) {
                        if(empty($datePayment)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 4 - Ngày chứng từ : ' . $datePayment .' sai định dạng. dd/mm/yyyy';
                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        // payment number int, max 10, > 0, not null
                        $paymentNumber = str_replace('.', '', $arrRow[4]);
                        $paymentNumber = str_replace(',', '', $paymentNumber);
                        $paymentNumber = str_replace(' ', '', $paymentNumber);

                        if(empty($paymentNumber) || !($paymentNumber > 0) || strlen($paymentNumber) > self::PAYMENT_NUMBER_MAX_LENGTH || !is_numeric($paymentNumber)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 5 - Số tiền phải chỉ được tối đa 10 số và là số lớn hơn 0.';
                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        //  payment content is required and content length >= 200
                        if(strlen($arrRow[5]) > self::PAYMENT_CONTENT_MAX_LENGTH) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 6 - Nội dung chứng từ không được lớn hơn 200 kí tự.';

                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }
                        // payment note content length >= 200
                        if(array_key_exists(6, $arrRow) && strlen($arrRow[6]) > self::PAYMENT_NOTE_MAX_LENGTH) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột : 7 - Ghi chú chứng từ không được lớn hơn 200 kí tự.';

                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        MemberServiceProvider::updateStatusByFileCode($arrRow[1], MemberConstants::SIGNING_WAITING);

                        try {
                            $realCurrency = MemberPaymentsServiceProvider::refomatCurrency($arrRow[4]);
                            MemberPaymentsServiceProvider::updateOrCreate($memberId, [
                                'number_payment'  => $arrRow[2], // số chứng từ, number_payment
                                'date_payment'    => $datePaymentMysql, // ngày chứng từ, date_payment
                                'is_delete'       => 0, // ngày chứng từ, date_payment
                                'memberId'        => $memberId,
                                'currency'        => $realCurrency, // số tiền, currency
                                'currency_type'   => 'vnd', // số tiền, currency_type
                                'payment_content' => $arrRow[5], // nội dung nộp tiền, payment_content
                                'payment_method'  => PaymentConstants::METHOD_VIETCOMBANK,
                                'note'            => $arrRow[6], // ghi chú, note
                            ]);
                            $firstMail = $objMember->firstEmail;
                            $secondMail = $objMember->secondEmail;

                            self::_sendPaymentMail($objMember, $firstMail, $arrRow[4]);

                            if (!empty($secondMail)) {
                                self::_sendPaymentMail($objMember, $secondMail, $arrRow[4]);
                            }
                            $recordValid++;
                        } catch (\Exception $exception) {
                            $recordValid--;
                            DB::rollBack();
                            LogsHelper::trackByFile('update_member_payment_fail', 'Error when update member payment(Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r($arrRow, true) . ')');
                        }
                    }
                }
            }

            /*if(!empty($errors)) {
                DB::rollback();
                return redirect()->route('admin_member_list_payment_view',['status' => MemberConstants::FEE_WAITING])->withErrors(['error_message' => $errors]);
            }*/

            if(!empty($arrFileCodesErr)) {
                DB::rollback();
                return redirect()->route('admin_member_list_payment_view', ['status' => MemberConstants::FEE_WAITING])
                    ->withErrors([
                        'Dữ liệu hồ sơ: ' . implode($arrFileCodesErr, ', ') . ' không hợp lệ.' . implode($errors, ', ')
                    ]);
            }

            if($recordValid === ($rowCount - 3)) {
                DB::commit();
                // LogsHelper::trackByFile('update_member_payment_success', $recordValid . 'inserted successfully.');
                return redirect()->route('admin_member_list_payment_view', ['status' => MemberConstants::FEE_WAITING])
                    ->with(['successes' => ['Cập nhật dữ liệu thành công.']]);
            } else {
                DB::rollback();
                return redirect()->route('admin_member_list_payment_view', ['status' => MemberConstants::FEE_WAITING])
                    ->withErrors('Không có bản ghi nào được cập nhật.');
            }
        }

        $reader->close();

        return redirect()->route('admin_member_list_payment_view');
    }

    public function emptyArrayElement($array = array()){
        $check  = 0;
        $count = count($array);
        foreach ($array as $cell) {
            if(empty($cell))
                $check++;
        }
        return $check == $count;
    }

    public function sendNotificationEmailRemindCompletingFee(Request $request)
    {
        $strMemberIdList = $request->input('list_id');
        $arrResendNotificationMemberId = explode(',', $strMemberIdList);
        $member = ListMemberServiceProvider::getMembersByIds($arrResendNotificationMemberId);
        foreach ($member as $key => $objMember) {
            if (!empty($objMember->firstEmail)) {
                $this->_sendRemindCompletingFeeMail($objMember, $objMember->firstEmail, 'remind_completing_fee', $key);
            }

            if (!empty($objMember->secondEmail)) {
                $this->_sendRemindCompletingFeeMail($objMember, $objMember->secondEmail, 'remind_completing_fee', $key);
            }
        }

        return back()->withInput()->with('successes', ['Gửi email nhắc nhở hội viên đã đăng ký hoàn thành hội phí lệ phí gia nhập']);
    }

    public function sendNotificationSmsRemindCompletingFee(Request $request)
    {
        $strMemberIdList = $request->input('list_id');
        $arrResendNotificationMemberId = explode(',', $strMemberIdList);
        $member = ListMemberServiceProvider::getMembersByIds($arrResendNotificationMemberId);
        foreach ($member as $key => $objMember) {
            $smsContent = 'De nghi ong/ba gui ho so ban cung gom don dang ky gia nhap, phieu thong tin hoi vien, the HDVDL(photo) CMTND(photo) ve tang 5, so 211, Giang Vo, Cat Linh, Ha Noi. Dong 1.000.000 vnd(500.000 vnd le phi gia nhap va 500.000 vnd hoi phi nam '. date("Y") .') ve tk Hoi Huong Dan Vien Viet Nam, stk: 0011004369919, SGD-Vietcombank, ND: le phi gia nhap, hoi phi so ho so ' . $objMember->file_code .' Tran trong.';

            if (!empty($objMember->firstMobile)) {
                dispatch((new SendSms($objMember->firstMobile, $smsContent))->delay(Carbon::now()->addSeconds($key + 5)));
            }

            if (!empty($objMember->secondMobile)) {
                dispatch((new SendSms($objMember->secondMobile, $smsContent))->delay(Carbon::now()->addSeconds($key + 5)));
            }
        }

        return back()->withInput()->with('successes', ['Gửi sms nhắc nhở hội viên đã đăng ký hoàn thành hội phí lệ phí gia nhập']);
    }

    private static function _sendRemindCompletingFeeMail($objMember, $email, $mailTemplate, $key)
    {
        if (!empty($email)) {
            try {
                $bcc_email = 'hoihdv@gmail.com';
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                //$bcc = [['address' => $bcc_email, 'name' => $bcc_email]];
                $subject = '';

                $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL08')->where('branch_id', $objMember->province_code)->first();
                if (empty($getEmail)) {
                    $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL08')->where('branch_id', self::TOANQUOC)->first();
                }
				$act1 = action('MemberController@downloadContractFile', ['fileName' => md5($objMember->id) . '_detail.pdf', 'fileNameSaved' => 'Phiếu thông tin']);
                $phieuthongtinhoivien = '<a href="'.$act1.'">Phiếu thông tin hội viên</a>';
				$taiphieu = '<a href="'.$act1.'">Tải về</a>';
				

                $act2 = action('MemberController@downloadContractFile', ['fileName' => md5($objMember->id) . '.pdf', 'fileNameSaved' => 'Đơn đăng ký']);
                $dondangky = '<a href="'.$act2.'">Đơn đăng kí gia nhập Hội HDVDL VN</a>';
				$taidondk = '<a href="'.$act2.'">Tải về</a>';
    
                $attach = null;

                if (!empty($getEmail)) {
                    $subject = $getEmail->title;
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
					$getEmail->content = str_replace(CommontConstants::PLACEHOLDER_MAHOSO, empty($objMember->file_code) ? '' : $objMember->file_code, $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_URL_PHIEUTHONGTIN, $phieuthongtinhoivien, $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_URL_DONDANGKY, $dondangky, $getEmail->content);
					$getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TAI_DK, $taidondk, $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TAI_PHIEU, $taiphieu, $getEmail->content);
    
                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }

                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
               
                $template = 'admin.member.mail.' . $mailTemplate;
                // dispatch((new SendEmail($template, $data, $to, $bcc, $subject))->delay(5));
                dispatch((new SendEmail($template, $data, $to, '', $subject, $attach))->delay(Carbon::now()->addSeconds($key + 5)));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_remindcompletingfee_joining', 'Error when send mail reminding completing fee (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember->id], true) . ')');
            }
        }
    }
}
