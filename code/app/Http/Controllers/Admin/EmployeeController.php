<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Employees;
use App\Models\EmployeesPosition;
use App\Models\Offices;
use App\Providers\LogServiceProvider;
use DB;
use Dompdf\Exception;
use Illuminate\Support\Facades\Validator;
use Lang;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const ARR_OFFICE = 'arrayEmployees';
    const PAGINATOR = 'paginator';
    const CURRENT_PAGINATOR = 'current_paginator';
    const STATUS_PUBLIC = 1;
    const STATUS_DRAFT = 0;
    public static $statusOption = [
        self::STATUS_PUBLIC => 'Hoạt động',
        self::STATUS_DRAFT => 'Không hoạt động'
    ];

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objEmployees = DB::table('employees')
            ->join('employee_position', 'employee_position.employee_id', '=', 'employees.id')
            ->whereNull('employees.deleted_at')->whereNotNull('employee_position.branch');
        if (!empty($request->input())) $objEmployees = $this->_objByFliter($request->input(),$objEmployees);

        $objEmployees = $objEmployees->orderBy('employees.created_at', 'asc')->get();
        $lastResult = array();
        $i = 1;
        foreach ($objEmployees as $key => $employee) {
            $employee->action = $this->_actionButton($key,$employee->employee_id);
            if(!empty($employee->employee_id)) $employee->id = $employee->employee_id;

            $employee->fullname = htmlspecialchars($employee->fullname, ENT_QUOTES, 'UTF-8');
            // Short Company
            $short_company = $this->shorten((string)$employee->company, 25);
            $str_company = "<span class=\"tooltip\" title=\"$employee->company\">".htmlentities($short_company)."</span>".htmlentities($short_company);
            $employee->company2 = $str_company;

            // Short Address
            $short_addree = $this->shorten((string)$employee->address, 25);
            $str_ddree = "<span class=\"tooltip\" title=\"$employee->address\">".htmlentities($short_addree)."</span>".htmlentities($short_addree);
            $employee->address2 = $str_ddree;

            // Add position and department
            $position = $this->getOption('position', $employee->option_code);
            $department = $this->getOption('department', $employee->branch);
            $employee->position = $position;
            $employee->department = $department;

            // Status Render
            $employee->status_render = ($employee->status == 1) ? Lang::get('offices.active') : Lang::get('offices.deactive');

            // Fliter by position or department
            // if (!empty($request->input('position')) && $position != $request->input('position')) continue;
            // if (!empty($request->input('department')) && $department != $request->input('department')) continue;

            $employee->stt = $i;
            $i++;

            $lastResult[] = $employee;  
        }

        return view('admin.employee.index', [
            'response_view' => empty($request) ? array() : $request,
            self::ARR_OFFICE => json_encode($lastResult),
            self::PAGINATOR => $limit,
            'statusOption' => self::$statusOption,
            'total' => count($lastResult),
            'positions' => $this->getAllPosition(),
            'departments' => $this->getAllDepartment()
        ]);
    }

    function show($id){
        $model = DB::table('employees')
            ->join('employee_position', 'employee_position.employee_id', '=', 'employees.id')
            ->where('employees.id',$id)->first();
        return view('admin.employee.view', [
            'model' => $model,
            'position' => $this->getOption('position', $model->option_code),
            'department' => $this->getOption('department', $model->branch)
        ]);
    }

    function shorten($str, $maxlen)
    {
        if (strlen($str) <= $maxlen - 3) {
            return $str;
        } else {
            return mb_substr($str,0,$maxlen - 3, "utf-8") . '...';
        }
    }

    public function getOption($key, $code)
    {
        $options = DB::table('options')->where('options.code', '=', $code)->get();
        foreach ($options as $option) {
            if ($option->key == $key) return $option->value;
        }
        return null;
    }

    public function getAllPosition()
    {
        $options = DB::table('options')->where('options.key', '=', 'position')->where('options.status', '=', 1)->get();
        $result = array();
        foreach ($options as $option) {
            if (!in_array($option->value, $result)) {
                $result[] = $option;
            }
        }
        return $result;
    }

    public function getAllDepartment()
    {
        $options = DB::table('options')->where('options.key', '=', 'department')->get();
        $result = array();
        foreach ($options as $option) {
            if (!in_array($option->value, $result)) {
                $result[] = $option;
            }
        }
        return $result;
    }

    public function _objByFliter($params,$obj)
    {
        $fullname = $params['fullname'];
        $phone = $params['phone'];
        $email = $params['email'];
        $orderBy = 'fullname';
        $status = $params['search_status'];
        $branch = $params['department'];
        $position = $params['position'];
        if ($fullname == '' && $phone == '' && $email == '') {
            $orderBy = 'id';
            $obj = $obj->where([
                    ['fullname', 'like', '%' . $fullname . '%'],
                    ['status', 'like', '%' . $status . '%']
                ])->where(function ($query) {
                        $query->where('email', 'like', '%' . '' . '%')
                              ->orWhereNull('email');
                })->where(function ($query) {
                        $query->where('phone', 'like', '%' . '' . '%')
                              ->orWhereNull('phone');
                });
        }
        if ($fullname != '') {
            $obj = $obj->where('fullname', 'like', '%' . $fullname . '%');
        }
        if ($phone != '') {
            $obj = $obj->where('phone', 'like', '%' . $phone . '%');
        }
        if ($email != '') {
            $obj = $obj->where('email', 'like', '%' . $email . '%');
        }
        if ($status != '') {
            $obj = $obj->where('status', 'like', '%' . $status . '%');
        }
        if ($branch != '') {
            $obj = $obj->where('employee_position.branch', $branch);
        }
        if ($position != '') {
           $obj = $obj->where('employee_position.option_code', $position);
        }
        return $obj;
    }

    public function _actionButton($key,$id = null)
    {
        $url = "'".'./employee/view/'.$id."'";
        $action = '<a title="Xem thông tin thành viên" alt="Xem thông tin thành viên" onclick="viewmodal('.$url.')">
                                <i class="fa fa-eye" aria-hidden="true" style="font-size: 18px;"></i>
                            </a>';
        $action .= " <a class=\"btn-function edit-modal\" 
                        data-toggle=\"tooltip\" 
                        data-placement=\"left\" 
                        title=\"Sửa thông tin thành viên\" 
                        atl=\"Sửa thông tin thành viên\"
                        onclick=\"editOffices($key)\"
                        >
                                <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\" style=\"font-size: 18px; color: #ff6600;\"></i>
                            </a>
                            ";
        $action .= " <a class=\"btn-function\" 
                        data-toggle=\"tooltip\" 
                        data-placement=\"left\" 
                        title=\"Xóa thành viên\" 
                        atl=\"Xóa thành viên\"
                        onclick=\"deleteOffices($key)\"
                        >
                                <i class=\"fa fa-trash-o\" aria-hidden=\"true\" style=\"font-size: 18px;\"></i>
                            </a>
                            ";
        return $action;
    }

    public function update(Request $request)
    {
        $params = $request->input();
        $id = $params['id'];
        unset($params['id']);
        $mesSuccess = 'Cập nhật thành viên thành công';
        $errorMsg = 'Cập nhật thành viên thất bại';
        if ($id == -1) {
            $objOffices = new Offices();
            $mesSuccess = "Thêm mới thành viên thành công";
            $errorMsg = "Thêm mới thất bại";
        } else $objOffices = Offices::find($id);
        $objOffices->status = 1;
        $objOffices->fill($params);
        try {
            if (!empty($_FILES["profile_image"]) && $images = $this->_uploadImages()) {
                $objOffices->images = $images;
                $objOffices->save();
                /*if ($id == -1) {
                    LogServiceProvider::createSystemHistory(trans('history.fn_create_new_employee'), [
                        'object' => $objOffices,
                    ]);
                } else {
                    LogServiceProvider::createSystemHistory(trans('history.fn_update_employee'), [
                        'object' => $objOffices,
                    ]);
                }*/
                return redirect()->route('admin_employee_view')->with('success_message', $mesSuccess);
            } else {
                return redirect()->route('admin_employee_view')->with('error', $errorMsg);
            }
        } catch (\Exception $e) {
            return redirect()->route('admin_employee_view')->with('error', 'Cập nhật thật bại');
        }
    }

    public function _uploadImages()
    {
        $errors = array();
        $uploadedFiles = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        $bytes = 1024;
        $KB = 1024;
        $totalBytes = 100 * $bytes * $KB;
        $dirName = date('YmdHis', time());
        $UploadFolder = public_path('files');
        if (!file_exists($UploadFolder)) {
            mkdir($UploadFolder, 0755, true);
        }
        $counter = 0;
        $temp = $_FILES["profile_image"]["tmp_name"];
        $name = $_FILES["profile_image"]["name"];
        $UploadOk = true;
        if ($_FILES["profile_image"]["size"] > $totalBytes) {
            $UploadOk = false;
            array_push($errors, $name . " file size is larger than the 1 MB.");
        }
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        if (in_array(strtolower($ext), $extension) == false) {
            $UploadOk = false;
            array_push($errors, $name . " is invalid file type.");
        }
        if (file_exists($UploadFolder . "/" . $name) == true) {
            $UploadOk = false;
            array_push($errors, $name . " file is already exist.");
        }
        $nameS = "/employees_" . $dirName . $name;
        if ($UploadOk == true) {
            move_uploaded_file($temp, $UploadFolder . $nameS);
        }
        if ($counter > 0) {
            if (count($errors) > 0) {
                return false;
            }
            return $nameS;
        } else {
            return $nameS;
        }
    }

    public function destroy(Request $request)
    {
        $params = $request->input();
        $id = $params['id'];
        if (empty($id)) {
            abort(404);
        }
        $objOffice = Employees::where('id', $id)->first(); // File::find($id)
        $objEmployees = EmployeesPosition::where('employee_id', $id)->first();
        if($objEmployees->status == 1) return redirect()->route('admin_employee_view')->with('error', Lang::get('offices.undeletecompletewithstatusenable'));

        try {
            $date = date("Y-m-d");
            $objOffice->deleted_at = $date;
            $objOffice->save();

            if (!empty($objEmployees)) {
                $objEmployees->deleted_at = $date;
                $objEmployees->save();
            }
            /*LogServiceProvider::createSystemHistory(trans('history.fn_delete_employee'), [
                'employee' => $objEmployees
            ]);*/
            return redirect()->route('admin_employee_view')->with('success_message', 'Xóa thành viên thành công');
        } catch (\Exception $e) {
            return redirect()->route('admin_employee_view')->with('error', 'Không thể xóa được thành viên');
        }

    }

    public function doCreate(Request $request)
    {
        if ($request->isMethod('post')) {
            $result = [
                'message' => '',
                'code' => true
            ];

            $data = $request->all();
          

            if (!isset($data['profile_image'])) {
                $data['profile_image'] = '';
            }
            if (!isset($data['gender'])) {
                $data['profile_image'] = '';
            }
            if (!isset($data['option'])) {
                $data['option'] = '';
            }
            if (!isset($data['company'])) {
                $data['company'] = '';
            }

            if (isset($data['branch'])) {
                $data['option_code'] = $data['branch'];
            }

            $id = $data['id'];
            $mes = 'Cập nhật thông tin thành viên thành công';
            if ($id == 0) {
                $employee = new Employees;
                $mes = 'Thêm mới thành viên thành công';
            } else $employee = Employees::find($id);
            $employee->fill($data);
            if (isset($data['hidden_image'])) {
                $employee->profile_image  = $data['hidden_image'];
            }

            if (!empty($_FILES["profile_image"]["name"]) && $images = $this->_uploadImages()) $employee->profile_image = $images;
            try {

                $employee->save();

                // create data to employee position
                $employeePositionData = [
                    'employee_id' => $employee->id,
                ];
                if (!empty($data['option'])) $employeePositionData['option_code'] = $data['option'];
                if (!empty($data['branch'])) $employeePositionData['branch'] = $data['branch'];

                $employeePositionFirst = EmployeesPosition::where('employee_id', (int)$employee->id)->first();
                $employeePositionId = null;
                if ($employeePositionFirst) $employeePositionId = $employeePositionFirst->id;

                $employeePosition = EmployeesPosition::updateOrCreate(
                    ['id' => $employeePositionId],
                    $employeePositionData
                );
                if (empty($employeePosition->branch_id)) $employeePosition->branch_id = 0;
                $employeePosition->save();
                return redirect()->route('admin_employee_view')->with('success_message', $mes);
            } catch (Exception $e) {
                $result['code'] = false;
                $result['message'] = $e->getMessage();
                return redirect()->route('admin_employee_view')->with('error', 'Cập nhật thật bại');
            }
        }
    }
}
