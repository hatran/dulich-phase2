<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\SubjectKnowledgeProvider;
use App\Providers\LogServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\UserConstants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Constants\MemberConstants;
use App\Libs\Helpers\Utils;
use App\Models\SubjectKnowledge;
use Illuminate\Support\Str;


class SubjectKnowledgeController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const SEARCH_INPUT = 'search';
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';
    const OBJ_SUBJECTS = 'objSubjects';

    const TITLE = 'nameall';
    const ID = 'id';
    const STATUS = 'status';
    const START_TIME = 'start_time';
    const END_TIME = 'end_time';
    const CURRENT_PAGINATOR = 'current_paginator';
    const COUNT_SUBJECT= 'count_subjects';

    const SUBJECT_INDEX = 'subjectknowledge.index';
    const SUBJECT_SHOW = 'subjectknowledge.show';
    const SUCCESSES = 'successes';
    const OPTIONS = 'options';

    const INTERNATIONAL_SUBJECT = 1;
    const INTERNATIONAL_KNOWLEDGE = 2;

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objSubjects = $this->_searchByCondition($request, $limit,false);
        $sumObjSubjects = $this->_searchByCondition($request, $limit,true);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }
        $options = SubjectKnowledgeProvider::getAllKnowledge();
        
//        print_r(json_decode(json_encode($objNews)));
        return view('admin.subject_knowledge.index', [
            self::OBJ_SUBJECTS          => $objSubjects,
            'admin_user'            => UserConstants::ADMIN_USER,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_SUBJECT         => count($sumObjSubjects),
            self::OPTIONS           => $options,
            self::PAGINATOR         => (count($sumObjSubjects) > 0) ? $objSubjects->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
            'area'                  => self::INTERNATIONAL_SUBJECT,
            'knowledge'             => self::INTERNATIONAL_KNOWLEDGE
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(self::TITLE))) {
            $title = $request->input(self::TITLE);
        } else {
            $title = '';
        }

        if (!empty($request->input(self::ID))) {
            $id = $request->input(self::ID);
        } else {
            $id = '';
        }

        if ($request->input(self::STATUS) != "") {
            $status = $request->input(self::STATUS);
        } else {
            $status = '';
        }
        
        $arrConditions = [
            self::TITLE  => $title,
            self::ID   => $id,
            self::STATUS => $status
        ];

        return SubjectKnowledgeProvider::subjectsSearchByConditions($arrConditions, $limit,$isCount);
    }

    public function saveSubject (Request $request)
    {
        if(!empty($request->input('id_edit'))){
            $checkData = SubjectKnowledgeProvider::getDataById($request->input('id_edit'));
            if(empty($checkData->id)){
                return redirect()->route($this::SUBJECT_INDEX)->withErrors(['Danh mục câu hỏi kiến thức không tồn tại hoặc đã xóa khỏi hệ thống']);
            }
        }

        $objData = [];
        $area = null;
        //add danh muc
        if(empty($request->input('id_edit'))){
            $option = $request->input('optionAll_add');
            $nameAll = $request->input('nameAll_add');
            $status = $request->input('statusAll_add');
            $areaLocalAll = $request->input('areaLocalAll_add');
            $areaInternationalAll = $request->input('areaInternationalAll_add');
            if (isset($areaLocalAll) && isset($areaInternationalAll)) {
                $area = 3;
            }

            if (!isset($areaLocalAll) && !isset($areaInternationalAll)) {
                $area = 3;
            }

            if (isset($areaLocalAll) && !isset($areaInternationalAll)) {
                $area = $areaInternationalAll;
            }

            if (!isset($areaLocalAll) && isset($areaInternationalAll)) {
                $area = $areaLocalAll;
            }

            if (request()->hasFile('pdfAll_add')) {
                $file = $request->pdfAll_add;
                $filePath = public_path('/pdf/subject_knowledge');
                $getExt = $file->getClientOriginalExtension();
                $newNameFile = Str::random(40).'_'.strtotime('now').'.'.$getExt;
                $file->move($filePath, $newNameFile);
                $objData['file_name'] = $newNameFile;
            }
        }else{
            $id = $request->input('id_edit');
            $option = $request->input('optionAll_edit');
            $nameAll = $request->input('nameAll_edit');
            $status = $request->input('statusAll_edit');
            $areaLocalAll = $request->input('areaLocalAll_edit');
            $areaInternationalAll = $request->input('areaInternationalAll_edit');
            if (isset($areaLocalAll) && isset($areaInternationalAll)) {
                $area = 3;
            }

            if (!isset($areaLocalAll) && !isset($areaInternationalAll)) {
                $area = 3;
            }

            if (isset($areaLocalAll) && !isset($areaInternationalAll)) {
                $area = $areaLocalAll;
            }

            if (!isset($areaLocalAll) && isset($areaInternationalAll)) {
                $area = $areaInternationalAll;
            }
           
            if (request()->hasFile('pdfAll_edit')) {
                $file = $request->pdfAll_edit;
                $filePath = public_path('/pdf/subject_knowledge');
                $getExt = $file->getClientOriginalExtension();
                $newNameFile = Str::random(40).'_'.strtotime('now').'.'.$getExt;
                $file->move($filePath, $newNameFile);
                $objData['file_name'] = $newNameFile;
            }
        }
        
        $objData['name']           = $nameAll;
        $objData['status']         = $status;
        $objData['knowledge_id']      = $option;
        $objData['area']           = $area;
        $objData['created_author'] = auth()->user()->username;
       
        if(!empty($request->input('id_edit'))){
            $objSave = SubjectKnowledge::where('id', $id)->update($objData);
        }else {
            $objSave = SubjectKnowledge::create($objData);
        }
        
        
        if (empty($objSave)) {
            if(!empty($request->input('id_edit'))){
                return redirect()->route($this::SUBJECT_INDEX)->withErrors(['Cập nhật danh mục câu hỏi kiến thức thất bại']);
            }else{
                return redirect()->route($this::SUBJECT_INDEX)->withErrors(['Thêm mới danh mục câu hỏi kiến thức thất bại']);
            }
        }
     
        if(!empty($request->input('id_edit'))){
            return redirect()->route($this::SUBJECT_INDEX)->with($this::SUCCESSES, ['Cập nhật danh mục câu hỏi kiến thức thành công']);
        }else{
            return redirect()->route($this::SUBJECT_INDEX)->with($this::SUCCESSES, ['Thêm mới danh mục câu hỏi kiến thức thành công']);
        }

    }

    public function showEdit ($id, Request $request)
    {

        if (empty($id)) {
            $objData = null;
        } else {
            $objData = SubjectKnowledgeProvider::getDataById($id);
        }

        if (empty($objData)) {
            $request->session()->flash('error_ajax', ['Danh mục câu hỏi kiến thức không tồn tại hoặc đã xóa khỏi hệ thống']);
            return response()->json(['deleted' => 'error']);
        }
      
        $objData['title'] = $objData['name'];
        $objOptions = SubjectKnowledgeProvider::getAllKnowledge();

        return response()->json(['objData' => $objData, 'objOptions' => $objOptions]);
    }

    public function deleteOption ($id)
    {
        $objIntro = SubjectKnowledgeProvider::getDataById($id);
        if (empty($objIntro)) {
            return redirect()->route($this::SUBJECT_INDEX)->withErrors(['Danh mục câu hỏi kiến thức không tồn tại hoặc đã xóa khỏi hệ thống']);
        }

        $objData['is_deleted'] = 1;
        $objMemberSubjectKnowledge = SubjectKnowledgeProvider::getStatusZeroById($id);
        if(!empty($objMemberSubjectKnowledge)) {
            $objSave = SubjectKnowledge::where('id', $id)->update($objData);
        }else{
            return redirect()->route($this::SUBJECT_INDEX)->withErrors(['Danh mục câu hỏi kiến thức đang được sử dụng, ko được xóa']);
        }
        if(empty($objSave)){
            return redirect()->route($this::SUBJECT_INDEX)->withErrors(['Xóa Danh mục câu hỏi kiến thức thất bại']);
        } else {
            return redirect()->route($this::SUBJECT_INDEX)->with($this::SUCCESSES, ['Xóa Danh mục câu hỏi kiến thức thành công']);
        }
    }

    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }
}
