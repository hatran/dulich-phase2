<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\QuestionProvider;
use App\Providers\SubjectKnowledgeProvider;
use App\Providers\OpenQuizProvider;
use App\Providers\LogServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\UserConstants;
use App\Constants\QuizConstants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Constants\MemberConstants;
use App\Libs\Helpers\Utils;
use App\Models\SubjectKnowledge;
use App\Models\Knowledge;
use Illuminate\Support\Str;
use App\Constants\RankConstants;
use App\Models\Question;
use App\Models\QuizTmp;
use App\Models\AnswerTmp;
use App\Models\QuestionTmp;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory as ReaderFactory;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel as Excel;

class QuestionController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';
    const OBJ_QUESTIONS = 'objQuestions';

    const CONTENT = 'contentall';
    const QUESTION_RANK = 'question_rank';
    const SUBJECT_KNOWLEDGE_ID = 'subject_knowledge_id';
    const CURRENT_PAGINATOR = 'current_paginator';
    const COUNT_QUESTION= 'count_questions';

    const QUESTION_INDEX = 'question.index';
    const SUCCESSES = 'successes';

    public function index(Request $request)
    {
        $attribute = $request->all();
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objQuestions = $this->_searchByCondition($request, $limit,false);
        $sumObjQuestions = $this->_searchByCondition($request, $limit,true);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }
        $knowledge = SubjectKnowledgeProvider::getAllKnowledge();
        $subjectKnowledge = SubjectKnowledgeProvider::getAllSubjectKnowledgeIDName();
        $rank_type = RankConstants::$rank;
        $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';

        return view('admin.question.index', [
            self::OBJ_QUESTIONS     => $objQuestions,
            'admin_user'            => UserConstants::ADMIN_USER,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_QUESTION    => count($sumObjQuestions),
            'knowledge'             => $knowledge,
            'subjectKnowledge'      => $subjectKnowledge,
            self::PAGINATOR         => (count($sumObjQuestions) > 0) ? $objQuestions->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
            'rank_type'             => $rank_type,
            'subject_knowledge_id'  => $request->input('subject_knowledge_id'),
            'query_string'          => $query_string
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {

        if (!empty($request->input(self::CONTENT))) {
            $content = $request->input(self::CONTENT);
        } else {
            $content = '';
        }

        if (!empty($request->input(self::QUESTION_RANK))) {
            $question_rank = $request->input(self::QUESTION_RANK);
        } else {
            $question_rank = '';
        }

        if ($request->input(self::SUBJECT_KNOWLEDGE_ID) != "") {
            $subject_knowledge_id = $request->input(self::SUBJECT_KNOWLEDGE_ID);
        } else {
            $subject_knowledge_id = '';
        }

        if ($request->input('knowledge_id') != "") {
            $knowledge_id = $request->input('knowledge_id');
        } else {
            $knowledge_id = '';
        }

        $arrConditions = [
            self::CONTENT  => $content,
            self::QUESTION_RANK   => $question_rank,
            self::SUBJECT_KNOWLEDGE_ID => $subject_knowledge_id,
            'knowledge_id' => $knowledge_id
        ];

        return QuestionProvider::questionsSearchByConditions($arrConditions, $limit,$isCount);
    }

    public function saveQuestion (Request $request)
    {
        if(!empty($request->input('id_edit'))){
            $checkData = QuestionProvider::getDataById($request->input('id_edit'));
            if(empty($checkData->id)){
                return redirect()->route($this::QUESTION_INDEX)->withErrors(['Câu hỏi kiến thức không tồn tại hoặc đã xóa khỏi hệ thống']);
            }
        }

        $objData = [];
        $answer1 = null;
        $answer2 = null;
        $answer3 = null;
        $answer4 = null;
        //add danh muc
        if(empty($request->input('id_edit'))){
            $codeAll = $request->input('codeAll_add');
            $getCodeExist = QuestionProvider::checkCode($codeAll);
            if (!empty($getCodeExist)) {
                return redirect()->route($this::QUESTION_INDEX)->withErrors(['Mã câu hỏi kiến thức này đã tồn tại']);
            }
            $subjectKnowledgeIDAll = $request->input('subjectKnowledgeIDAll_add');
            $contentAll = $request->input('contentAll_add');
            $questionRankAll = $request->input('questionRankAll_add');
            $status = $request->input('statusAll_add');

            $answer1All = $request->input('answer1All_add');
            $answer2All = $request->input('answer2All_add');
            $answer3All = $request->input('answer3All_add');
            $answer4All = $request->input('answer4All_add');
           
            if (isset($answer1All)) {
                $answer1 = $request->input('answer1All_add');
            }
            if (isset($answer2All)) {
                $answer2 = $request->input('answer2All_add');
            }
            if (isset($answer3All)) {
                $answer3 = $request->input('answer3All_add');
            }
            if (isset($answer1All)) {
                $answer4 = $request->input('answer4All_add');
            }

            $correctAll = $request->input('answerCorrectAll_add');

        }else{
            $id = $request->input('id_edit');
            $codeAll = $request->input('codeAll_edit');
            $subjectKnowledgeIDAll = $request->input('subjectKnowledgeIDAll_edit');
            $contentAll = $request->input('contentAll_edit');
            $questionRankAll = $request->input('questionRankAll_edit');
            $status = $request->input('statusAll_edit');

            $answer1All = $request->input('answer1All_edit');
            $answer2All = $request->input('answer2All_edit');
            $answer3All = $request->input('answer3All_edit');
            $answer4All = $request->input('answer4All_edit');
           
            if (isset($answer1All)) {
                $answer1 = $request->input('answer1All_edit');
            }
            if (isset($answer2All)) {
                $answer2 = $request->input('answer2All_edit');
            }
            if (isset($answer3All)) {
                $answer3 = $request->input('answer3All_edit');
            }
            if (isset($answer1All)) {
                $answer4 = $request->input('answer4All_edit');
            }

            $correctAll = $request->input('answerCorrectAll_edit');
        }
        
        $objData['code']           = $codeAll;
        $objData['content']         = $contentAll;
        $objData['subject_knowledge_id']      = $subjectKnowledgeIDAll;
        $objData['question_rank']           = $questionRankAll;
        $objData['answer1']           = $answer1;
        $objData['answer2']           = $answer2;
        $objData['answer3']           = $answer3;
        $objData['answer4']           = $answer4;
        $objData['answer_correct']           = $correctAll;
        $objData['status']           = $status;
        $objData['created_author'] = auth()->user()->username;

        $validate = QuestionProvider::isValid($objData, QuestionProvider::$rules, QuestionProvider::$messages, QuestionProvider::$niceAttributeName);
        if ($validate === true) {
            if(!empty($request->input('id_edit'))){
                $objSave = Question::where('id', $id)->update($objData);
            }else {
                $objSave = Question::create($objData);
            }
            
            
            if (empty($objSave)) {
                if(!empty($request->input('id_edit'))){
                    return redirect()->route('question.edit', ['id' => $id])->withErrors(['Cập nhật câu hỏi kiến thức thất bại']);
                }else{
                    return redirect()->route('question.create')->withErrors(['Thêm mới câu hỏi kiến thức thất bại']);
                }
            }
         
            if(!empty($request->input('id_edit'))){
                return redirect()->route('question.edit', ['id' => $id])->with($this::SUCCESSES, ['Cập nhật câu hỏi kiến thức thành công']);
            }else{
                return redirect()->route('question.create')->with($this::SUCCESSES, ['Thêm mới câu hỏi kiến thức thành công']);
            }
        }
        else {
            if(!empty($request->input('id_edit'))){
                return redirect()->route('question.edit', ['id' => $id])->withInput()->withErrors($validate);
            }
            else {
                return redirect()->route('question.create')->withInput()->withErrors($validate);
            }
        }
    }

    public function ajxCheckCode (Request $request)
    {
        $codeAll = $request->input('code');
        $code = QuestionProvider::checkCode($codeAll);
        $arr = [];
        if (!empty(code)) {
            $status = 1;
        }
        else {
            $status = 0;
        }
        $arr = [
            'status' => 'OK',
            'data' => $status
        ];
        return $arr;
    }

    public function deleteOption ($id)
    {
        $objQuestion = QuestionProvider::getDataById($id);
        if (empty($objQuestion)) {
            return redirect()->route($this::QUESTION_INDEX)->withErrors(['Câu hỏi kiến thức không tồn tại hoặc đã xóa khỏi hệ thống']);
        }

        $objData['is_deleted'] = 1;
        $objMemberQuestion = QuestionProvider::getStatusZeroById($id);
        if(!empty($objMemberQuestion)) {
            $objSave = Question::where('id', $id)->update($objData);
        }else{
            return redirect()->route($this::QUESTION_INDEX)->withErrors(['Câu hỏi kiến thức đang được sử dụng, ko được xóa']);
        }
        if(empty($objSave)){
            return redirect()->route($this::QUESTION_INDEX)->withErrors(['Xóa câu hỏi kiến thức thất bại']);
        } else {
            return redirect()->route($this::QUESTION_INDEX)->with($this::SUCCESSES, ['Xóa câu hỏi kiến thức thành công']);
        }
    }

    public function create() {
        $knowledge = SubjectKnowledgeProvider::getAllKnowledge();
        $statusDefault = 1;
        $rank_type = RankConstants::$rank;
        if (old('status') != '') {
            $statusDefault = old('status') == 1 ? 1 : 0;
        }
        return view('admin.question.create', ['statusDefault' => $statusDefault, 'knowledge' => $knowledge, 'rank_type' => $rank_type]);
    }

    public function edit($id) {
        if (empty($id)) {
            abort(404);
        }
        $question = QuestionProvider::findQuestion($id);
        if (empty($question)) {
            abort(404);
        }
        $subjectKnowledge = SubjectKnowledgeProvider::findSubjectKnowledge($question->subject_knowledge_id);
        if (empty($subjectKnowledge)) {
            abort(404);
        }
        $knowledge = SubjectKnowledgeProvider::getAllKnowledge();
        $rank_type = RankConstants::$rank;
        return view('admin.question.edit', [
            'question' => $question,
            'subjectKnowledge' => $subjectKnowledge,
            'knowledge' => $knowledge,
            'rank_type'             => $rank_type,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        if (empty($id)) {
            abort(404);
        }
        $question = QuestionProvider::findQuestion($id);
        if (empty($question)) {
            abort(404);
        }
        $subjectKnowledge = SubjectKnowledgeProvider::findSubjectKnowledge($question->subject_knowledge_id);
        if (empty($subjectKnowledge)) {
            abort(404);
        }
        $subjectKnowledgeName = SubjectKnowledgeProvider::getAllSubjectKnowledgeIDName();
        $knowledge = SubjectKnowledgeProvider::getAllKnowledge();
        $rank_type = RankConstants::$rank;
        return view('admin.question.detail', [
            'question' => $question,
            'subjectKnowledge' => $subjectKnowledge,
            'subjectKnowledgeName' => $subjectKnowledgeName,
            'knowledge'  => $knowledge,
            'rank_type' => $rank_type
        ]);
    }

    public function ajaxAddSubjectKnowledge(Request $request) {
        $attribute = $request->all();
        $id = $attribute['id'];
        $subInfo = SubjectKnowledgeProvider::getSubjectKnowledgeByKnowledge($id);
        $data = [
            'data' => $subInfo,
            'status' => 'OK',
        ];
        return $data;
    }

    public function openQuizStatus(Request $request) {
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;

        $arrConditions = [
            CommontConstants::FULL_NAME => $request->input(CommontConstants::FULL_NAME, ''),
            CommontConstants::STATUS => $request->input(CommontConstants::STATUS, ''),
            CommontConstants::TOURIST_GUIDE_CODE => $request->input(CommontConstants::TOURIST_GUIDE_CODE, ''),
            CommontConstants::FILE_CODE => $request->input(CommontConstants::FILE_CODE),
            CommontConstants::MEMBER_CODE => $request->input(CommontConstants::MEMBER_CODE)
        ];

        $objMembers = OpenQuizProvider::searchMembers($arrConditions, $limit, false);
        $sumObjMembers = $objMembers->total();
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <= 1){
            $current_page = 0;
        } else {
            $current_page = ($current_page - 1) * 10;
        }

        return view('admin.question.openquiz', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::COUNT_MEMBER => $sumObjMembers,
            CommontConstants::PAGINATOR => ($sumObjMembers > 0) ? $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'status' => QuizConstants::OPEN_QUIZ_STATUS,
        ]);
    }

    // Đóng/mở thi cho member
    public function updateQuizStatus(Request $request) {
        $strMemberIdList = $request->input('list_id');
        $arrMemberId = explode(',', $strMemberIdList);
        $status = $request->input('status');
        $msg = 'Đóng thi';

        OpenQuizProvider::deleteMembers($arrMemberId);

        if ($status == 1) {
            $msg = 'Mở thi';
            $arrInsert = [];
            foreach ($arrMemberId as $memberId) {
                $arrInsert[] = [
                    'member_id' => $memberId,
                    'open' => 1,
                    'created_at' => date('Y-m-d H:i:s')
                ];
            }
            OpenQuizProvider::insertMembers($arrInsert);
        }
        
        return back()->with($this::SUCCESSES, [$msg .' thành công!']);
    }

    public function importExcel (Request $request)
    {
        $file = $request->file('fileToUpload');

        if(!$file) {
            return redirect()->route('question.index')->with('error_message', 'Không có file!');
        }

        $fileMine = $file->getClientMimeType();
        $fileType = $file->getClientOriginalExtension();
        $fileSize = $file->getClientSize();

        if ($fileSize > 10000000) {
            return redirect()->route('question.index')->withErrors('File quá lớn, dung lượng file cho phép tối đa là 10Mb!');
        }

        if ($this->getNumRows($file) == 3) {
            return redirect()->route('question.index')->withErrors('File không có dữ liệu để cập nhật!');
        }

        $mineRules = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel');

        if(!in_array($fileMine, $mineRules) || $fileType != Type::XLSX) {
            return redirect()->route('question.index')->withErrors('File không đúng định dạng!');
        }

        try {
            $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
            $reader->open($file);
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_question_fail', 'Error when update question(Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ')');
            return redirect()->route('question.index')->withErrors(['error_message' => 'Không thể đọc file.']);
        }

        $sheetList = $reader->getSheetIterator();
    
        if (empty($sheetList)) {
            return redirect()->route('question.index');
        }

        DB::beginTransaction();
        foreach ($sheetList as $key => $sheet) {
            if ($key > 1) {
                break;
            }

            $recordValid = 0;
            $rowCount = 0;
            $errors = array();
            $arrFileCodesErr = [];
            $done = [];
            $mss = [];
            // $donemss = [];
            // $arrayMssMerge = [];
            foreach ($sheet->getRowIterator() as $keyRow => $arrRow) {
                if ($this->emptyArrayElement($arrRow)) continue;
                $rowCount++;
                if($keyRow == 3){
                    if($arrRow != ['TT', 'Loại kiến thức', 'Danh mục câu hỏi', 'Loại câu hỏi', 'Mã câu hỏi', 'Nội dung câu hỏi', 'Đáp án 1', 'Đáp án 2', 'Đáp án 3', 'Đáp án 4', 'Đáp án đúng']){
                        return redirect()->route('question.index')->withErrors('File không đúng định dạng!');
                    }
                }

                if ($keyRow > 3) {

                    if (!empty($arrRow[4])) {
                        $checkKnowledge = Knowledge::checkName($arrRow[1]);
                        
                        if (empty($checkKnowledge)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 2 - Không tìm thấy loại kiến thức trong cơ sở dữ liệu.';

                            $arrFileCodesErr[] = $arrRow[4];

                            continue;
                        }

                        $checkSubjectKnowledge = SubjectKnowledge::checkName($arrRow[2]);

                        if (empty($checkSubjectKnowledge)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 3 - Không tìm thấy danh mục câu hỏi trong cơ sở dữ liệu.';

                            $arrFileCodesErr[] = $arrRow[4];

                            continue;
                        }

                        $check_question_rank = RankConstants::$question_rank;

                        if (!in_array($arrRow[3], $check_question_rank)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 4 - Không tìm thấy loại câu hỏi.';

                            $arrFileCodesErr[] = $arrRow[4];

                            continue;
                        }

                        $code = QuestionProvider::checkCode($arrRow[4]);

                        if (!empty($code)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 5 - Mã câu hỏi đã bị trùng.';

                            $arrFileCodesErr[] = $arrRow[4];

                            continue;
                        }

                        $check_answer = RankConstants::$answer;
                        if (!in_array($arrRow[10], $check_answer)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 11 - Đáp án chỉ là một trong bốn số 1 2 3 4.';

                            $arrFileCodesErr[] = $arrRow[4];

                            continue;
                        }

                        $subjectKnow = SubjectKnowledge::getAllSubjectKnowledgeName();
                        $rank_name_arr = RankConstants::$rank_name_id;

                        $objData = [];

                        $objData['code']                      = $arrRow[4];
                        $objData['content']                   = $arrRow[5];
                        $objData['subject_knowledge_id']      = $subjectKnow[$arrRow[2]];
                        $objData['question_rank']             = $rank_name_arr[$arrRow[3]];
                        $objData['answer1']           =  $arrRow[6];
                        $objData['answer2']           =  $arrRow[7];
                        $objData['answer3']           =  $arrRow[8];
                        $objData['answer4']           =  $arrRow[9];
                        $objData['answer_correct']    = $arrRow[10];
                        $objData['status']            = 1;
                        $objData['created_author']    = auth()->user()->username;

                        $findQuestion = Question::where('code', $arrRow[4])->where('is_deleted', 0)->where('status', 1)->get();

                        try {
                            $objSave = Question::create($objData);
                            $done[] = 'Mã câu hỏi : ' . $arrRow[4] . ' đã được thêm.';
                        } catch (\Exception $exception) {
                            DB::rollBack();
                            LogsHelper::trackByFile('create_question_fail', 'Error when create question(Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r($arrRow, true) . ')');
                        }
                    }
                }
            }
    
            if(!empty($arrFileCodesErr)) {
                // DB::rollback();
                $mss[] = 'Dữ liệu mã câu hỏi: ' . implode($arrFileCodesErr, ', ') . ' không hợp lệ.';
                if (!empty($errors)) {
                    $mss[] = implode($errors, ', ');
                }

                $arrayMssMerge[] = $mss;
            }

            if (count($done) != 0) {
                DB::commit();
                $mss[] = implode($done, ', ');
                return redirect()->route('question.index')->with($this::SUCCESSES, $mss);
            }

            return redirect()->route('question.index')->withErrors($mss);
        }
        $reader->close();

        return redirect()->route('question.index');
    }

    public function getNumRows($xlsxPath) {
        $numRows = 0;

        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($xlsxPath);

        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                $numRows++;
            }
        }

        $reader->close();

        return $numRows;
    }

    public function emptyArrayElement($array = array()){
        $check  = 0;
        $count = count($array);
        foreach ($array as $cell) {
            if(empty($cell))
                $check++;
        }
        return $check == $count;
    }

    public function exportExcel (Request $request)
    {
        $rank_type = RankConstants::$rank;

        if (!empty($request->input(self::CONTENT))) {
            $content = $request->input(self::CONTENT);
        } else {
            $content = '';
        }

        if (!empty($request->input(self::QUESTION_RANK))) {
            $question_rank = $request->input(self::QUESTION_RANK);
        } else {
            $question_rank = '';
        }

        if ($request->input(self::SUBJECT_KNOWLEDGE_ID) != "") {
            $subject_knowledge_id = $request->input(self::SUBJECT_KNOWLEDGE_ID);
        } else {
            $subject_knowledge_id = '';
        }

        if ($request->input('knowledge_id') != "") {
            $knowledge_id = $request->input('knowledge_id');
        } else {
            $knowledge_id = '';
        }

        $arrSearchCondition = [];
      
        if (!empty($content)) {
            $arrSearchCondition = array_merge($arrSearchCondition,[['question.content', 'like', '%' . $content . '%']]);
        }

        if (!empty($question_rank)) {
            $arrSearchCondition = array_merge($arrSearchCondition,[['question.question_rank', '=', $question_rank]]);
        }

        $arrSub = array();
        if (!empty($subject_knowledge_id)) {
            $arrSearchCondition = array_merge($arrSearchCondition,[['question.subject_knowledge_id', '=', $subject_knowledge_id]]);
        }
        else {
            if (!empty($knowledge_id)) {
                $sub = SubjectKnowledge::getSubjectKnowledgeByKnowledgeID($knowledge_id);
                
                if (count($sub) != 0) {
                    foreach ($sub as $s) {
                        $arrSub[] = $s->id;
                    }
                }
            }
        }       

        if (count($arrSub) != 0) {
            $objQuestion = Question::select('question.*','subject_knowledge.name as skname', 'knowledge.name as kname')
                ->join('subject_knowledge', 'question.subject_knowledge_id', '=', 'subject_knowledge.id')
                ->join('knowledge', 'subject_knowledge.knowledge_id', '=', 'knowledge.id')
                ->where($arrSearchCondition)
                ->whereIn('question.subject_knowledge_id', $arrSub)
                ->where(function($q)  {
                    $q->where('question.is_deleted', 0)
                      ->orWhereNull('question.is_deleted');
            })->get();
        }
        else {
            $objQuestion = Question::select('question.*','subject_knowledge.name as skname', 'knowledge.name as kname')
                ->join('subject_knowledge', 'question.subject_knowledge_id', '=', 'subject_knowledge.id')
                ->join('knowledge', 'subject_knowledge.knowledge_id', '=', 'knowledge.id')
                ->where($arrSearchCondition)
                ->where(function($q)  {
                    $q->where('question.is_deleted', 0)
                      ->orWhereNull('question.is_deleted');
            })->get();
        }   

        $fileName = 'DanhSachCauHoi_' . date('YmdHis') . '-' . Utils::randomString(6);

        Excel::create($fileName, function($excel) use ($objQuestion, $rank_type) {
            $excel->setTitle('DANH SÁCH CÂU HỎI KIẾN THỨC');
            $excel->sheet('First sheet', function($sheet) use ($objQuestion, $rank_type) {
                $sheet->setWidth(array(
                    'A'     =>  5,
                    'B'     =>  15,
                    'C'     =>  50,
                    'D'     =>  50,
                    'E'     =>  20,
                    'F'     =>  20,
                    'G'     =>  50,
                    'H'     =>  50,
                    'I'     =>  50,
                    'J'     =>  50,
                    'K'     =>  5,
                    'L'     =>  15,
                    'M'     =>  15,
                    'N'     =>  15,
                ));
                $sheet->getStyle('C')->getAlignment()->setWrapText(true);
                $sheet->getStyle('D')->getAlignment()->setWrapText(true);
                $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                $sheet->getStyle('H')->getAlignment()->setWrapText(true);
                $sheet->getStyle('I')->getAlignment()->setWrapText(true);
                $sheet->getStyle('J')->getAlignment()->setWrapText(true);

                $sheet->loadView('admin.question.export_excel', [
                    'objQuestions'          => $objQuestion,
                    'rank_type'             => $rank_type,
                ]);
            });
        })->download('xlsx');
    }
}
