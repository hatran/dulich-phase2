<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\UserServiceProvider;
use Illuminate\Http\Request;
use App\Repositories\News\NewsAdmin;
use App\Providers\LogServiceProvider;
use App\Exceptions\Validation\ValidationException;
use Input;
use App\Constants\UserConstants;
use Illuminate\Support\Facades\Route;

class NewsController extends Controller {

    protected $news = null;

    public function __construct() {
        $this->news = new NewsAdmin();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {

        // list and search
        $page = $request->get('page', 1);
        $listData = $this->news->paginationNewsHoi($request->all());
        $user = auth()->user();
       
        return view('admin.news.index', [
            'listData' => $listData,
            'statusOption' => NewsAdmin::$statusOption,
            'offsets' => ($page - 1) * $this->news->perPage,
            'paginator' => !empty($listData) ? $listData->appends($request->all()) : null,
            'user' => $user,
            'leaderManagerUser' => UserConstants::LEADER_MANAGER_USER,
        ]);
    }

    /**
     * Show the form for creating a branches resource.
     *
     * @return Response
     */
    public function create() {
        $optionCode = NewsAdmin::OPTIONS_HOI;
        $statusDefault = NewsAdmin::STATUS_PUBLIC;
        if (old('status') != '') {
            $statusDefault = old('status') == NewsAdmin::STATUS_PUBLIC ? NewsAdmin::STATUS_PUBLIC : NewsAdmin::STATUS_DRAFT;
        }
        
        return view('admin.news.create', ['optionCode' => $optionCode, 'statusDefault' => $statusDefault]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        try {
            $this->news->createNews($request->all(), NewsAdmin::OPTIONS_HOI);
			//LogServiceProvider::createSystemHistory(trans('history.fn_create_news'), $request->all());
            return redirect()->route('admin.news_ch_clb.index')->with('successes', ['Thêm mới tin tức thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('admin.news_ch_clb.create')->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            abort(404);
        }
        $optionCode = NewsAdmin::OPTIONS_HOI;
        
        return view('admin.news.edit', [
            'detail' => $detail,
            'optionCode' => $optionCode
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            abort(404);
        }

        return view('admin.news.show', [
            'detail' => $detail,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            return redirect()->route('admin.news_ch_clb.index')->withInput()->withErrors(['Tin tức không tồn tại hoặc đã bị xóa']);
        }
        try {
            $this->news->updateNews($id, $request->all());
            //LogServiceProvider::createSystemHistory(trans('history.fn_save_news'), $request->all());
            return redirect()->route('admin.news_ch_clb.index')->with('successes', ['Sửa tin tức thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('admin.news_ch_clb.update', ['id' => $id])->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (empty($id)) {
            abort(404);
        }
        try {
            $model = $this->news->find($id);
            if (!empty($model)) {
                $model->delete();
                //LogServiceProvider::createSystemHistory(trans('history.fn_delete_new'), $model);
                return redirect()->route('admin.news_ch_clb.index')->with('successes', ['Xóa tin tức thành công']);
            }
            return redirect()->route('admin.news_ch_clb.index')->withErrors('Tin tức không tồn tại hoặc đã xóa');
        } catch (Exception $ex) {
            return redirect()->route('admin.news_ch_clb.index')->withErrors('Tin tức không tồn tại hoặc đã xóa');
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexBranches(Request $request) {

        // list and search
        $page = $request->get('page', 1);
        $listData = $this->news->paginationNewBranches($request->all());
        $listBranches = $this->news->getListBranches();
        $user = auth()->user();

        return view('admin.news.branches.index', [
            'listData' => $listData,
            'listBranches' => $listBranches,
            'statusOption' => NewsAdmin::$statusOption,
            'offsets' => ($page - 1) * $this->news->perPage,
            'paginator' => !empty($listData) ? $listData->appends($request->all()) : null,
            'user' => $user,
            'leaderManagerUser' => UserConstants::LEADER_MANAGER_USER,
        ]);
    }
    
    /**
     * Show the form for creating a branches resource.
     *
     * @return Response
     */
    public function createBranches() {
        $optionCode = NewsAdmin::OPTIONS_BRANCHES;
        $listBranches = $this->news->getListBranches();
        $statusDefault = NewsAdmin::STATUS_PUBLIC;
        if (old('status') != '') {
            $statusDefault = old('status') == NewsAdmin::STATUS_PUBLIC ? NewsAdmin::STATUS_PUBLIC : NewsAdmin::STATUS_DRAFT;
        }
        
        return view('admin.news.branches.create', [
            'optionCode' => $optionCode,
            'listBranches' => $listBranches,
            'statusDefault' => $statusDefault
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeBranches(Request $request) {
        try {
            $this->news->createNews($request->all(), NewsAdmin::OPTIONS_BRANCHES);
            return redirect()->route('admin.news_ch.index')->with('successes', ['Thêm mới tin tức thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('admin.news_ch.create')->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function editBranches($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            abort(404);
        }
        $optionCode = NewsAdmin::OPTIONS_BRANCHES;
        $listBranches = $this->news->getListBranches();
        
        return view('admin.news.branches.edit', [
            'detail' => $detail,
            'optionCode' => $optionCode,
            'listBranches' => $listBranches
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showBranches($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        $listBranches = $this->news->getListBranches();
        if (empty($detail)) {
            abort(404);
        }

        return view('admin.news.branches.show', [
            'detail' => $detail,
            'listBranches' => $listBranches
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateBranches(Request $request, $id) {

        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            return redirect()->route('admin.news_ch.index')->withInput()->withErrors(['Tin tức không tồn tại hoặc đã bị xóa']);
        }
        try {
            $this->news->updateNews($id, $request->all());
            return redirect()->route('admin.news_ch.index')->with('successes', ['Sửa tin tức thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('admin.news_ch.update', ['id' => $id])->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroyBranches($id) {
        if (empty($id)) {
            abort(404);
        }
        try {
            $model = $this->news->find($id);
            if (!empty($model)) {
                $model->delete();
                return redirect()->route('admin.news_ch.index')->with('successes', ['Xóa tin tức thành công']);
            }
            return redirect()->route('admin.news_ch.index')->withErrors('Tin tức không tồn tại hoặc đã xóa');
        } catch (Exception $ex) {
            return redirect()->route('admin.news_ch.index')->withErrors('Tin tức không tồn tại hoặc đã xóa');
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function indexClub(Request $request) {

        // list and search
        $page = $request->get('page', 1);
        $listData = $this->news->paginationNewClub($request->all());
        $listClub = $this->news->getListClub();

        return view('admin.news.club.index', [
            'listData' => $listData,
            'listClub' => $listClub,
            'statusOption' => NewsAdmin::$statusOption,
            'offsets' => ($page - 1) * $this->news->perPage,
            'paginator' => !empty($listData) ? $listData->appends($request->all()) : null
        ]);
    }
    
    /**
     * Show the form for creating a branches resource.
     *
     * @return Response
     */
    public function createClub() {
        $optionCode = NewsAdmin::OPTIONS_CLUB;
        $listClub = $this->news->getListClub();
        $statusDefault = NewsAdmin::STATUS_PUBLIC;
        if (old('status') != '') {
            $statusDefault = old('status') == NewsAdmin::STATUS_PUBLIC ? NewsAdmin::STATUS_PUBLIC : NewsAdmin::STATUS_DRAFT;
        }

        return view('admin.news.club.create', [
            'optionCode' => $optionCode,
            'listClub' => $listClub,
            'statusDefault' => $statusDefault
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function storeClub(Request $request) {
        try {
            $this->news->createNews($request->all(), NewsAdmin::OPTIONS_CLUB);
            return redirect()->route('admin.news_clb.index')->with('successes', ['Thêm mới tin tức thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('admin.news_clb.create')->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function editClub($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            abort(404);
        }
        $optionCode = NewsAdmin::OPTIONS_CLUB;
        $listClub = $this->news->getListClub();
        
        return view('admin.news.club.edit', [
            'detail' => $detail,
            'optionCode' => $optionCode,
            'listClub' => $listClub
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showClub($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        $listClub = $this->news->getListClub();
        if (empty($detail)) {
            abort(404);
        }

        return view('admin.news.club.show', [
            'detail' => $detail,
            'listClub' => $listClub
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateClub(Request $request, $id) {

        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            return redirect()->route('admin.news_clb.index')->withInput()->withErrors(['Tin tức không tồn tại hoặc đã bị xóa']);
        }
        try {
            $this->news->updateNews($id, $request->all());
            return redirect()->route('admin.news_clb.index')->with('successes', ['Sửa Tin tức thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('admin.news_clb.update', ['id' => $id])->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroyClub($id) {
        if (empty($id)) {
            abort(404);
        }
        try {
            $model = $this->news->find($id);
            if (!empty($model)) {
                $model->delete();
                return redirect()->route('admin.news_clb.index')->with('successes', ['Xóa tin tức thành công']);
            }

            return redirect()->route('admin.news_clb.index')->withErrors('Tin tức không tồn tại hoặc đã xóa');
        } catch (Exception $ex) {
            return redirect()->route('admin.news_clb.index')->withErrors('Tin tức không tồn tại hoặc đã xóa');
        }
    }
    
}
