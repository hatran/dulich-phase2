<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\EvaluationCriteriaProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Libs\Helpers\Utils;
use App\Models\EvaluationCriteria;
use Illuminate\Support\Str;
use App\Constants\UserConstants;

class EvaluationCriteriaController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const SEARCH_INPUT = 'search';
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';
    const OBJ_EVALUATIONS = 'objEvaluations';

    const NAME = 'name';
    const STATUS = 'status';
    const CURRENT_PAGINATOR = 'current_paginator';
    const COUNT_EVALUATION = 'count_evaluations';

    const EVALUATION_INDEX = 'evaluationcriteria.index';
    const EVALUATION_SHOW = 'evaluationcriteria.show';
    const SUCCESSES = 'successes';

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objEvaluations = $this->_searchByCondition($request, $limit,false);
        $sumObjEvaluations = $this->_searchByCondition($request, $limit,true);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }
        
//        print_r(json_decode(json_encode($objNews)));
        return view('admin.evaluation_criteria.index', [
            self::OBJ_EVALUATIONS          => $objEvaluations,
            'admin_user'            => UserConstants::ADMIN_USER,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_EVALUATION         => count($sumObjEvaluations),
            self::PAGINATOR         => (count($sumObjEvaluations) > 0) ? $objEvaluations->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(self::NAME))) {
            $name = $request->input(self::NAME);
        } else {
            $name = '';
        }

        if ($request->input(self::STATUS) != "") {
            $status = $request->input(self::STATUS);
        } else {
            $status = '';
        }
        
        $arrConditions = [
            self::NAME  => $name,
            self::STATUS => $status
        ];

        return EvaluationCriteriaProvider::evaluationsSearchByConditions($arrConditions, $limit,$isCount);
    }

    public function saveEvaluations (Request $request)
    {
        if(!empty($request->input('id_edit'))){
            $checkData = EvaluationCriteriaProvider::getDataById($request->input('id_edit'));
            if(empty($checkData->id)){
                return redirect()->route($this::EVALUATION_INDEX)->withErrors(['Tiêu chí đánh giá này không tồn tại hoặc đã xóa khỏi hệ thống']);
            }
        }

        $objData = [];
        //add danh muc
        if(empty($request->input('id_edit'))){
            $nameAll = $request->input('nameAll_add');
            $status = $request->input('statusAll_add');
            $codeAll = $request->input('codeAll_add');
            $percentageAll = $request->input('percentageAll_add');
           
         
        }else{
            $id = $request->input('id_edit');
            $nameAll = $request->input('nameAll_edit');
            $status = $request->input('statusAll_edit');
            $codeAll = $request->input('codeAll_edit');
            $percentageAll = $request->input('percentageAll_edit');
        }
        
        $objData['name']           = $nameAll;
        $objData['status']         = $status;
        $objData['code']      = $codeAll;
        $objData['percentage']           = $percentageAll;
        $objData['created_author'] = auth()->user()->username;
       
        if(!empty($request->input('id_edit'))){
            $objSave = EvaluationCriteria::where('id', $id)->update($objData);
        }else {
            $objSave = EvaluationCriteria::create($objData);
        }
        
        
        if (empty($objSave)) {
            if(!empty($request->input('id_edit'))){
                return redirect()->route($this::EVALUATION_INDEX)->withErrors(['Cập nhật tiêu chí đánh giá thất bại']);
            }else{
                return redirect()->route($this::EVALUATION_INDEX)->withErrors(['Thêm mới tiêu chí đánh giá thất bại']);
            }
        }
     
        if(!empty($request->input('id_edit'))){
            return redirect()->route($this::EVALUATION_INDEX)->with($this::SUCCESSES, ['Cập nhật tiêu chí đánh giá thành công']);
        }else{
            return redirect()->route($this::EVALUATION_INDEX)->with($this::SUCCESSES, ['Thêm mới tiêu chí đánh giá thành công']);
        }

    }

    public function showEdit ($id, Request $request)
    {

        if (empty($id)) {
            $objData = null;
        } else {
            $objData = EvaluationCriteriaProvider::getDataById($id);
        }

        if (empty($objData)) {
            $request->session()->flash('error_ajax', ['Tiêu chí đánh giá không tồn tại hoặc đã xóa khỏi hệ thống']);
            return response()->json(['deleted' => 'error']);
        }
        return response()->json(['objData' => $objData]);
    }

    public function deleteOption ($id)
    {
        $objEvaluation = EvaluationCriteriaProvider::getDataById($id);
        if (empty($objEvaluation)) {
            return redirect()->route($this::EVALUATION_INDEX)->withErrors(['Tiêu chí đánh giá không tồn tại hoặc đã xóa khỏi hệ thống']);
        }

        $objData['is_deleted'] = 1;
        $objMemberEvaluationCriteria = EvaluationCriteriaProvider::getStatusZeroById($id);
        if(!empty($objMemberEvaluationCriteria)) {
            $objSave = EvaluationCriteria::where('id', $id)->update($objData);
        }else{
            return redirect()->route($this::EVALUATION_INDEX)->withErrors(['Không được xóa tiêu chí đánh giá đang hoạt động']);
        }
        if(empty($objSave)){
            return redirect()->route($this::EVALUATION_INDEX)->withErrors(['Xóa tiêu chí đánh giá thất bại']);
        } else {
            return redirect()->route($this::EVALUATION_INDEX)->with($this::SUCCESSES, ['Xóa tiêu chí đánh giá thành công']);
        }
    }

    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }
}
