<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\MemberFileAward;
use App\Models\MemberNote;
use App\Providers\ListMemberServiceProvider;
use App\Providers\MemberServiceProvider;
use App\Providers\UserServiceProvider;
use App\Providers\LogServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\MemberConstants;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Major;
use App\Models\Member;
use App\Models\MajorSkill;
use App\Models\Offices;
use App\Models\OtherConstant;
use App\Models\WorkConstant;
use App\Models\Option;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Input;
use App\Models\Branches;
use App\Providers\MemberPaymentsYearlyServiceProvider;
use App\Models\MemberRank;
use App\Models\Emails;
use App\Providers\EvaluationCriteriaProvider;
use App\Models\Rank;
use App\Providers\NoteServiceProvider;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Libs\Helpers\LogsHelper;
use App\Jobs\SendEmail;
use App\Jobs\SendSms;
use App\Models\HistoryEmailSMS;
use App\Models\WaitingPrint;
use App\Libs\Helpers\Utils;
use App\Models\Note;

class ListMemberController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const MEMBER_ID = 'memberId';
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const GENDER = 'gender';
    const BIRTHDAY = 'birthday';
    const DOB = 'bod';
    const BIRTH_YEAR = 'birthYear';
    const BIRTH_MONTH = 'birthMonth';
    const BIRTH_DATE = 'birthDate';
    const CMT_CCCD = 'cmtCccd';
    const ISSUED_BY = 'issuedBy';
    const PERMANENT_ADDRESS = 'permanentAddress';
    const ADDRESS = 'address';
    const FIRST_MOBILE = 'firstMobile';
    const SECOND_MOBILE = 'secondMobile';
    const FIRST_EMAIL = 'firstEmail';
    const SECOND_EMAIL = 'secondEmail';
    const EXPERIENCE_YEAR = 'experienceYear';
    const INBOUND_OUTBOUND = 'inboundOutbound';
    const EXPERIENCE_LEVEL = 'experienceLevel';
    const OTHER_SKILLS = 'otherSkills';
    const OTHER_INFORMATION = 'otherInformation';
    const TOURIST_GUIDE_LEVEL = 'touristGuideLevel';
    const ACHIEVEMENTS = 'achievements';
    const STATUS = 'status';
    const PROVINCE_CODE = 'province_code';
    const IS_VERIFIED = 'is_verified';
    const IS_FEE = 'is_fee';
    const IS_SIGNED = 'is_signed';
    const MEMBER_TYPE = 'member_type';
    const GUIDE_LANGUAGE = 'guideLanguage';
    const EDUCATIONS = 'educations';
    const BRANCH_ID = 'branchId';
    const EDUCATION_BRANCHES = 'educationBranches';
    const DEGREE_ID = 'degreeId';
    const EDUCATION_DEGREES = 'educationDegrees';
    const FILE = 'file';
    const EDUCATION_FILES = 'educationFiles';
    const LANGUAGE = 'language';
    const LEVEL2 = 'level2';
    const LANGUAGE_ID = 'languageId';
    const LEVEL_ID = 'levelId';
    const MAJOR_SKILLS = 'majorSkills';
    const IDS = 'ids';
    const MAJORS = 'majors';
    const FILES = 'files';
    const MAJOR_FILES = 'majorFiles';
    const LANGUAGE_SKILLS = 'languageSkills';
    const LANGUAGES = 'languages';
    const LANGUAGE_LEVELS = 'languageLevels';
    const ELEMENTS = 'elements';
    const WORK_HISTORY = 'workHistory';
    const WORK_ELEMENT_NAMES = 'workElementNames';
    const WORK_TYPE_OF_CONTRACTS = 'workTypeOfContracts';
    const WORK_FROM_DATES = 'workFromDates';
    const WORK_TO_DATES = 'workToDates';
    const ELEMENT_NAME = 'elementName';
    const TYPE_OF_CONTRACT = 'typeOfContract';
    const FROM_DATE = 'fromDate';
    const TO_DATE = 'toDate';
    const GROUP_SIZE_ID = 'groupSizeId';
    const GROUP_SIZES = 'groupSizes';
    const TYPE_GUIDE_ID = 'typeGuideId';
    const TYPE_GUIDES = 'typeGuides';
    const AVATAR = 'avatar';
    const USER_PHOTO = 'userPhoto';
    const FILE_NAME = 'fileName';
    const ACHIEVEMENT_FILES = 'achievementFiles';
    const TYPE_OF_PLACE = 'typeOfPlace';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EXPIRATION_DATE = 'expirationDate';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const DATE_ISSUED = 'dateIssued';
    const COOKIE_REGISTER_TOKEN = 'regis_token';
    const CLUB = 'club';
    const OBJ_MEMBER = 'objMember';
    const OBJ_MEM = 'objMem';
    const MEMBER_BELONGS_TO_CHI_HOI = 1;
    const MEMBER_BELONGS_TO_HOI = 2;
    const LOCAL_TRAVEL_GUIDE = 3;
    const PROVINCE = 'province';
    const ACCEPTED = 'acceptTermsAndPolicies';
    const DELETED = 'is_delete';
    const SUCCESSES = 'successes';
    const MAJORS_SKILLS = 'objMajorSkills';
    const OBJ_MAJORS = 'objMajor';
    const OBJ_LANGUAGE = 'objLanguage';
    const OBJ_LANGUAGE_LEVEL = 'objLanguageLevel';
    const WORKHISTORY = 'workHistories';
    const TYPE_GUIDE_NAME = 'typeGuideName';
    const GROUP_SIZE_NAME = 'groupSizeName';
    const BRAND = 'objBrand';
    const DEGREE = 'objDegree';
    const OBJ_PAYMENT = 'paymentInfor';
    const OBJ_NOTES = 'notes';
    const POSITION = 'position';
    const PROFILE_IMG = 'profileImg';
    const FORTE_TOUR = 'forteTour';
    const GENERAL_REPORT = 'generalReport';

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objMembers = $this->_searchByCondition($request, $limit, false);
        $current_page = empty($request['page']) ? 0 : $request['page'];
        if ($current_page <= 1){
            $current_page = 0;
        } else {
            $current_page = ($current_page - 1) * 10;
        }
        $searchCommon = $this->getStatusByStep();
        unset($searchCommon['arrFile'][9]);

        $searchCommon['arrFile'][61] = 'Chờ cấp lại thẻ';
        $searchCommon['print_status'][90] = 'Chờ in thẻ lần đầu';
        $searchCommon['print_status'][91] = 'Chờ in thẻ gia hạn';
        $searchCommon['print_status'][92] = 'Chờ in thẻ cấp lại';
        $searchCommon['print_status'][130] = 'Đã in thẻ lần đầu';
        $searchCommon['print_status'][131] = 'Đã in thẻ gia hạn';
        $searchCommon['print_status'][132] = 'Đã in thẻ cấp lại';
        $guideLanguage = \App\Models\Language::getAllLanguages();
        $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
        $tourist_guide_type = MemberConstants::$member_typeOfTravelGuide;

        return view('admin.list.index', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            CommontConstants::COUNT_MEMBER => $objMembers->total(),
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ?  $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'query_string' => $query_string,
            'offices' => Offices::searchByAllOffices(),
            'status' => !empty($request->input('status')) ? $request->input('status') : '',
            'tourist_guide_type' => $tourist_guide_type,
            'print_status' => $searchCommon['print_status'],
            'guideLanguage' => $guideLanguage
        ]);
    }
    public function listExport(Request $request)
    {

        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objMembers = $this->_searchByCondition($request, $limit,false);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }
        $searchCommon = $this->getStatusByStep();
        $searchByRoleOrArea = $this->getSearchByRoleOrArea($request);
        $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
		$tourist_guide_type = MemberConstants::$member_typeOfTravelGuide;
		
        return view('admin.list.listexport', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            CommontConstants::COUNT_MEMBER => $objMembers->total(),
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ?  $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'query_string' => $query_string,
			'tourist_guide_type' => $tourist_guide_type
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount, $code = null)
    {
        if (!empty($request->input(CommontConstants::FROM_DATE))) {
            $fromDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::FROM_DATE, ''))->toDateString();
        } else {
            $fromDate = '';
        }

        if (!empty($request->input(CommontConstants::TO_DATE))) {
            $toDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::TO_DATE, ''))->toDateString();
        } else {
            $toDate = '';
        }

        if (!empty($request->input(CommontConstants::PROVINCE_TYPE))) {
            $getProvinceType = $this->searchByProvinceType($request->input(CommontConstants::PROVINCE_TYPE));
        }
        else {
            $getProvinceType = '';
        }

        $arrConditions = [
            CommontConstants::FILE_CODE => $request->input(CommontConstants::FILE_CODE, ''),
            CommontConstants::TOURIST_GUIDE_CODE => $request->input(CommontConstants::TOURIST_GUIDE_CODE, ''),
            CommontConstants::FULL_NAME => $request->input(CommontConstants::FULL_NAME, ''),
            CommontConstants::FROM_DATE => $fromDate,
            CommontConstants::TO_DATE => $toDate,
            CommontConstants::STATUS => $request->input(CommontConstants::STATUS, ''),
            CommontConstants::PROVINCE_CODE => $request->input(CommontConstants::PROVINCE_CODE, ''),
            CommontConstants::MEMBER_CODE => $request->input(CommontConstants::MEMBER_CODE, ''),
            CommontConstants::PROVINCE_TYPE => $getProvinceType,
            'rank_status' => $request->input('rank_status', ''),
            'tourist_guide_type' => $request->input('tourist_guide_type', ''),
            'member_status' => $request->input('member_status', ''),
            'print_status' => $request->input('print_status', ''),
            'member_from' => $request->input(CommontConstants::MEMBER_FROM) ?? '',
            'tourist_expiration_year' => $request->input('tourist_expiration_year') ?? '',
            'guideLanguage' => $request->input('guideLanguage') ?? ''
        ];

        if (!empty($code) && ($code == self::GENERAL_REPORT)) {
            return ListMemberServiceProvider::generalReportSearchByConditions($arrConditions, $limit, $isCount);
        }
        
        return ListMemberServiceProvider::listSearchByConditions($arrConditions, $limit, $isCount);
    }

    /**
     * Test function
     *
     * @param $id
     * @return string
     */
    public function printFile($id)
    {
        if (empty($id)) {
            return 'Bạn chưa nhập Id';
        } else {
            $objMember = ListMemberServiceProvider::getMemberById($id);
        }

        return view('admin.list.pdf.member_info', ['objMember' => $objMember]);

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {

        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = ListMemberServiceProvider::getMemberById($id);
        }

        $languages = ListMemberServiceProvider::getAllLanguages();
        $forteTours = ListMemberServiceProvider::getAllForteTours();

        if (empty($objMember)) {
            abort(404);
        }
        if($objMember->is_delete) {

            return redirect()->route('admin_member_list_view')->withErrors(['error_message' => 'Hồ sơ này đã bị xóa khỏi hệ thống']);
        }

        // thay đổi logic bởi duong 20180614
        /*$typeOfPlaces = array();
        if (UserServiceProvider::isHnLeaderUser() || UserServiceProvider::isHnExpertUser()) {
            $typeOfPlaces = BranchConstants::$hn_typeOfPlace;
        } else if (UserServiceProvider::isDnLeaderUser() || UserServiceProvider::isDnExpertUser()) {
            $typeOfPlaces = BranchConstants::$dn_typeOfPlace;
        } else if (UserServiceProvider::isHcmLeaderUser() || UserServiceProvider::isHcmExpertUser()) {
            $typeOfPlaces = BranchConstants::$hcm_typeOfPlace;
        }*/

        $typeOfPlaces = UserServiceProvider::getTypeOfPlace();
        // if (!empty($typeOfPlaces) && !in_array($objMember->province_code, $typeOfPlaces)) {
        //     return redirect()->route('admin_member_list_view')->withErrors(['Bạn không có quyền truy cập hồ sơ này']);
        // }

        $objEducation = MemberServiceProvider::getEducationByMemberId($objMember->id);
        $objLanguageSkills = MemberServiceProvider::getLanguageAchievedByMemberId($objMember->id);

        $objMajorSkills =  MajorSkill::where('memberId', $objMember->id)->first();

        if (empty($objMajorSkills)) {
            $objMajor = null;
        } else {
            $objMajor = Major::where('id', $objMajorSkills->majorId)->first();
        }

        $workHistories = MemberServiceProvider::getWorkHistoryByMemberId($objMember->id);
        $objElement = MemberServiceProvider::getElementByMemeber($objMember);
        $paymentInfor = \App\Models\MemberPayments::where('memberId', $objMember->id)->first();
        $notes = \App\Providers\MemberServiceProvider::getDetailMemberById($objMember->id);
        $paymentYearly = MemberPaymentsYearlyServiceProvider::getAllPaymentYearlyByMemberId($objMember->id);

        $otherConstant = $workConstant = $provincial = $chihoi = $clbthuochoi = [];
        $otherTmp = OtherConstant::where('status', 1)->orderBy('otherName', 'asc')->get();
        $workTmp = WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get();
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();

        if(!empty($otherTmp)){
            foreach($otherTmp as $key => $value){
                $otherConstant[$value->id] = $value->otherName;
            }
        }
        if(!empty($workTmp)){
            foreach($workTmp as $key => $value){
                $workConstant[$value->id] = $value->workName;
            }
        }
        if(!empty($optionTmp)){
            foreach($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if(!empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }
        if(!empty($clbthuochoTmp)){
            foreach($clbthuochoTmp as $key => $value){
                $clbthuochoi[$value->id] = $value->name;
            }
        }

        if ($objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER) {
            $memberRank = MemberRank::getRankByMemberId($id);
            $ranks = Rank::getAllRanks();
        }
        $memberNotes = MemberNote::where("member_id", "=", $id)->orderBy('id', 'desc')->paginate(10);

        return view('admin.list.detail', [
            CommontConstants::OBJ_MEMBER => $objMember,
            CommontConstants::TYPE_OF_TRAVEL_GUIDE => empty($objMember->typeOfTravelGuide) ? '' : $objMember->typeOfTravelGuide,
            CommontConstants::TYPE_OF_PLACE => empty($objMember->typeOfPlace) ? '' : $objMember->typeOfPlace,
            CommontConstants::BRAND => empty($objEducation[CommontConstants::BRAND]) ? '' : $objEducation[CommontConstants::BRAND],
            CommontConstants::DEGREE => empty($objEducation[CommontConstants::DEGREE]) ? '' : $objEducation[CommontConstants::DEGREE],
            CommontConstants::MAJORS => empty($objMajor) ? '' : $objMajor,
            CommontConstants::LANGUAGE => empty($objLanguageSkills[CommontConstants::LANGUAGE]) ? '' : $objLanguageSkills[CommontConstants::LANGUAGE],
            CommontConstants::LANGUAGE_LEVEL => empty($objLanguageSkills[CommontConstants::LANGUAGE_LEVEL]) ? '' : $objLanguageSkills[CommontConstants::LANGUAGE_LEVEL],
            CommontConstants::WORKHISTORY => empty($workHistories) ? '' : $workHistories,
            CommontConstants::TYPE_GUIDE_NAME =>empty($objElement[CommontConstants::TYPE_GUIDE_NAME]) ? '' : $objElement[CommontConstants::TYPE_GUIDE_NAME],
            CommontConstants::GROUP_SIZE_NAME =>empty($objElement[CommontConstants::GROUP_SIZE_NAME]) ? '' : $objElement[CommontConstants::GROUP_SIZE_NAME],
            CommontConstants::OBJ_PAYMENT => empty($paymentInfor) ? '' : $paymentInfor,
            CommontConstants::OBJ_NOTES => empty($notes) ? '' : $notes,
            CommontConstants::GUIDE_LANGUAGE => empty($objMember->guideLanguage) ? '' : $objMember->guideLanguage,
            CommontConstants::PROFILE_IMG => empty($objMember->profileImg) ? '' : $objMember->profileImg,
            'otherConstant' => $otherConstant,
            'workConstant' => $workConstant,
            'provincial' => $provincial,
            'branch_chihoi' => $chihoi,
            'branch_clbthuochoi' => $clbthuochoi,
            'paymentYearly' => $paymentYearly,
            'isProfile' => false,
            'languages' => $languages,
            'forteTours' => $forteTours,
            'memberOfficial' => MemberConstants::MEMBER_OFFICIAL_MEMBER,
            'memberRank' => !empty($memberRank) ? $memberRank : '',
            'ranks' => !empty($ranks) ? $ranks : '',
            'rank_status' => MemberConstants::$rank_status,
            'memberNotes' => $memberNotes
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($id)) {
            return null;
        }

        $objMember = ListMemberServiceProvider::getMemberById($id);

        if (empty($objMember)) {
            abort(404);
        }

        if($objMember->is_delete) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã bị xóa.']]);
        }
        if ($request->input('type') == 1 && ($objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER || $objMember->status == MemberConstants::UPDATE_INFO_2)) {
            return response()->json(['errors' => ['memberInvalid' => 'Không thể lưu kho hội viên chính thức']]);

        }
        if ($request->input('type') == 2 && $objMember->status != MemberConstants::MEMBER_STORED) {
            return response()->json(['errors' => ['memberInvalid' => 'Hồ sơ đã được khôi phục.']]);
        }
        $objmember = ListMemberServiceProvider::updateMemberStatus($request, $id);
        if ($request->input('type') == 2) {
            LogServiceProvider::createSystemHistory(trans('history.fn_restock_save'), [
                'memberId' => $id,
                'status' => $objmember->initial_stock
            ]);
        } else {
            LogServiceProvider::createSystemHistory(trans('history.fn_put_store'), [
                'memberId' => $id,
                'status' => MemberConstants::MEMBER_STORED
            ]);
        }

        return response()->json(['member' => $objmember]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        if (empty($id))
            return null;
        $objMember = ListMemberServiceProvider::getMemberById($id);

        if (empty($objMember)) {
            abort(404);
        }
        if($objMember->is_delete) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã được xóa rồi.']]);
        }
        if($objMember->status ==  MemberConstants::MEMBER_OFFICIAL_MEMBER || $objMember->status ==  MemberConstants::UPDATE_INFO_2) {
            return response()->json(['errors' => ['memberInvalid' => 'Không được xóa hội viên chính thức.']]);
        }

        $objMember = ListMemberServiceProvider::softDelete($id);
        LogServiceProvider::createSystemHistory(trans('history.fn_delete_member'), [
            'memberId' => $id,
            'member' => $objMember
        ]);
        return response()->json($objMember);
    }

    /**
     * Note the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function saveNoteMember(Request $request,$id)
    {
        try {
            if (empty($id))
                return null;
            $objMember = ListMemberServiceProvider::getMemberById($id);
            $note = Note::where('member_id','=',$id)->where('function_type','=',5)->where('action_type','=',1)->get()->first();
            if(empty($note))
            $note = new Note();
            
            $note->member_id = $id;
            $note->note = $request->_note;
            $note->function_type = 5;
            $note->action_type = 1;
            $note->save();

 
            return response()->json($note);
        } catch (\Exception $e) {
            return response()->json($e);
        }   
    }

    public function getNoteMember(Request $request,$id)
    {
        try {
            if (empty($id))
                return null;
            $note = Note::where('member_id','=',$id)->where('function_type','=',5)->where('action_type','=',1)->get()->first();
          

 
            return response()->json( $note);
        } catch (\Exception $e) {
            return response()->json($e);
        }   
    }


    public function exportExcel(Request $request) {
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objMembers = $this->_searchByCondition($request, $limit,true);
        $tourist_guide_type = MemberConstants::$member_typeOfTravelGuide;
        $offices = Offices::searchByAllOffices();
        $status = !empty($request->input('status')) ? $request->input('status') : '';
        $print_status = !empty($request->input('print_status')) ? $request->input('print_status') : '';
        $fileName = 'DanhSachHS_' . date('Y-m-d') . '-' . Utils::randomString(6);
        Excel::create($fileName, function($excel) use ($objMembers, $tourist_guide_type, $offices, $status, $print_status) {
            $excel->setTitle('HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM');
            $excel->sheet('First sheet', function($sheet) use ($objMembers, $tourist_guide_type, $offices, $status, $print_status) {
                $sheet->loadView('admin.list.export_excel', compact('objMembers', 'tourist_guide_type', 'offices', 'status', 'print_status'));
            });
        })->download('xlsx');
    }

    public function exportExcelCode(Request $request) {

        $limit = 100000;
        /*
        hs dang ky moi   				status 1
        hs duoc phe duyet  				status 4
        hs yeu cau bo sung thong tin  	status 22 and 2
        hs tu choi phe duyet  			status 8
        hs hoi vien chinh thuc   		status 13 */
        $branchTmp = \App\Models\Offices::whereNotNull('parent_id')->get();
        $branchList = [];
        foreach($branchTmp as $value){
            $branchList[$value['id']] = $value['name'];
        }
        $province_code  = $request->input('province_code', '');

        if (!empty($request->input(CommontConstants::FROM_DATE))) {
            $fromDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::FROM_DATE, ''))->toDateString();
        } else {
            $fromDate = '';
        }

        $branchTmp2 = \App\Models\Offices::where('option_code', 'BRANCHES03')->get();

        if(!empty($branchTmp2)){
            foreach($branchTmp2 as $value){
                $branchList[$value['id']] = $value['name'];
            }
        }

        if (!empty($request->input(CommontConstants::TO_DATE))) {
            $toDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::TO_DATE, ''))->toDateString();
        } else {
            $toDate = '';
        }

        $arrData = [];

        $dangkymoi = ListMemberServiceProvider::countMemberByStatus(['1'], $branchList, $province_code, $fromDate, $toDate);
        $dangkymoi['name'] = 'Sồ hồ sơ đăng ký mới';
        $arrData[] = $dangkymoi;
        $duocpheduyet = ListMemberServiceProvider::countMemberByStatus(['4'], $branchList, $province_code, $fromDate, $toDate);
        $duocpheduyet['name'] = 'Sồ hồ sơ được phê duyệt';
        $arrData[] = $duocpheduyet;
        $bosung = ListMemberServiceProvider::countMemberByStatus(['2','22'], $branchList, $province_code, $fromDate, $toDate);
        $bosung['name'] = 'Sồ hồ sơ yêu cầu bổ sung thông tin';
        $arrData[] = $bosung;
        $tuchoi = ListMemberServiceProvider::countMemberByStatus(['8'], $branchList, $province_code, $fromDate, $toDate);
        $tuchoi['name'] = 'Sồ hồ sơ từ chối phê duyệt';
        $arrData[] = $tuchoi;
        $chinhthuc = ListMemberServiceProvider::countMemberByStatus(['13'], $branchList, $province_code, $fromDate, $toDate);
        $chinhthuc['name'] = 'Số hội viên chính thức';
        $arrData[] = $chinhthuc;
        $status = empty($province_code) ? 'all' : $branchList[$province_code];
        $fileName = 'Thong_Ke_So_Luong_HS_' . date('Y-m-d') . '-' . Utils::randomString(6);
        //print_r($arrData);
        if($status == 'all'){
            $merge0A = $merge0B = $merge0C = '';
            $merge1A = $merge1B = $merge1C = '';
            $merge2A = $merge2B = $merge2C = '';
            $merge3A = $merge3B = $merge3C = '';
            $merge4A = $merge4B = $merge4C = '';
            $merge5 = 'D5:E5';
            if(!empty($arrData)){
                foreach($arrData as $key => $value){
                    if ($key == 0) {
                        $tmp0 = count($value['all']) > 0 ? ((count($value['all'])-1)+6) : 6 ;
                        $merge0A = 'A6:A'.$tmp0;
                        $merge0B = 'B6:B'.$tmp0;
                        $merge0C = 'C6:C'.$tmp0;
                    }
                    if ($key == 1) {
                        $tmp1 = ($tmp0+1)+(count($value['all']) > 0 ? (count($value['all'])-1) : 0 );
                        $merge1A = 'A'.($tmp0+1).':A'.$tmp1;
                        $merge1B = 'B'.($tmp0+1).':B'.$tmp1;
                        $merge1C = 'C'.($tmp0+1).':C'.$tmp1;
                    }
                    if ($key == 2) {
                        $tmp2 = ($tmp1+1)+(count($value['all']) > 0 ? (count($value['all'])-1) : 0 );
                        $merge2A = 'A'.($tmp1+1).':A'.$tmp2;
                        $merge2B = 'B'.($tmp1+1).':B'.$tmp2;
                        $merge2C = 'C'.($tmp1+1).':C'.$tmp2;
                    }
                    if ($key == 3) {
                        $tmp3 = ($tmp2+1)+(count($value['all']) > 0 ? (count($value['all'])-1) : 0);
                        $merge3A = 'A'.($tmp2+1).':A'.$tmp3;
                        $merge3B = 'B'.($tmp2+1).':B'.$tmp3;
                        $merge3C = 'C'.($tmp2+1).':C'.$tmp3;
                    }
                    if ($key == 4) {
                        $tmp4 = ($tmp3+1)+(count($value['all']) > 0 ? (count($value['all'])-1) : 0 );
                        $merge4A = 'A'.($tmp3+1).':A'.$tmp4;
                        $merge4B = 'B'.($tmp3+1).':B'.$tmp4;
                        $merge4C = 'C'.($tmp3+1).':C'.$tmp4;
                    }
                }
            }
//            echo $merge0A.'---'.$merge1A.'---'.$merge2A.'---'.$merge3A.'---'.$merge4A;die;
            Excel::create($fileName, function($excel) use ($arrData, $status, $fromDate, $toDate, $merge0A, $merge0B, $merge0C, $merge1A, $merge1B, $merge1C, $merge2A, $merge2B, $merge2C, $merge3A, $merge3B, $merge3C, $merge4A, $merge4B, $merge4C, $merge5 ) {
                $excel->setTitle('HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM');
                $excel->sheet('First sheet', function($sheet) use ($arrData, $status, $fromDate, $toDate, $merge0A, $merge0B, $merge0C, $merge1A, $merge1B, $merge1C, $merge2A, $merge2B, $merge2C, $merge3A, $merge3B, $merge3C, $merge4A, $merge4B, $merge4C, $merge5) {
                    $sheet->loadView('admin.list.export_excel_code', [
                        'arrData'   => $arrData,
                        'status'    => $status,
                        'fromDate'  => $fromDate,
                        'toDate'    => $toDate,

                    ])
                        ->mergeCells($merge0A)
                        ->mergeCells($merge0B)
                        ->mergeCells($merge0C)
                        ->mergeCells($merge1A)
                        ->mergeCells($merge1B)
                        ->mergeCells($merge1C)
                        ->mergeCells($merge2A)
                        ->mergeCells($merge2B)
                        ->mergeCells($merge2C)
                        ->mergeCells($merge3A)
                        ->mergeCells($merge3B)
                        ->mergeCells($merge3C)
                        ->mergeCells($merge4A)
                        ->mergeCells($merge4B)
                        ->mergeCells($merge4C)
                        ->mergeCells($merge5)
                    ;
                });
            })->download('xlsx');
        }else{
            Excel::create($fileName, function($excel) use ($arrData, $status, $fromDate, $toDate) {
                $excel->setTitle('HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM');
                $excel->sheet('First sheet', function($sheet) use ($arrData, $status, $fromDate, $toDate) {
                    $sheet->loadView('admin.list.export_excel_code', [
                        'arrData' => $arrData,
                        'status' => $status,
                        'fromDate'  => $fromDate,
                        'toDate'    => $toDate,
                    ]);
                });
            })->download('xlsx');
        }

    }

    public function penddingOfficalMember($memberId) {
        if (empty($memberId))
            return null;
        $objMember = ListMemberServiceProvider::getMemberById($memberId);

        if (empty($objMember)) {
            abort(404);
        }
        if($objMember->is_delete) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên này đã được xóa.']]);
        }
        $validStatus = array(MemberConstants::MEMBER_OFFICIAL_MEMBER,
                            MemberConstants::UPDATE_INFO_2,
                            MemberConstants::MEMBER_PENDDING, MemberConstants::MEMBER_DISTROY);
        if(!in_array($objMember->status, $validStatus)) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên này chưa phải hội viên chính thức.']]);
        }

        $objMember = ListMemberServiceProvider::softDelete($memberId);
        LogServiceProvider::createSystemHistory(trans('history.fn_delete_member'), [
            'memberId' => $memberId,
            'member' => $objMember
        ]);
        return response()->json($objMember);
    }

    public function distroyOfficalMember() {

    }

    public function revertOfficalMember() {

    }

    public function indexRank(Request $request)
    {
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objMembers = $this->_searchByConditionRank($request, $limit,false);
        $current_page = $request['page'] ?? 0;
        if ($current_page <= 1){
            $current_page = 0;
        } else {
            $current_page = ($current_page - 1) * 10;
        }
        $searchCommon = $this->getStatusByStep();
        $rankStatus = MemberConstants::$rank_status;
        $criteria = EvaluationCriteriaProvider::getPercentage();
        $query_string = $_SERVER['QUERY_STRING'] ?? '';

        return view('admin.list.indexrank', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            CommontConstants::COUNT_MEMBER => $objMembers->total(),
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ?  $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'query_string' => $query_string,
            'offices' => Offices::searchByAllOffices(),
            'status' => $request->input('status') ?? '',
            'rankStatus' => $rankStatus,
            'criteria' => $criteria,
            'ranksName' => Rank::getAllRanks(),
        ]);
    }

    private function _searchByConditionRank($request, $limit,$isCount)
    {
        if (!empty($request->input(CommontConstants::PROVINCE_TYPE))) {
            $getProvinceType = $this->searchByProvinceType($request->input(CommontConstants::PROVINCE_TYPE));
        }
        else {
            $getProvinceType = '';
        }

        $arrConditions = [
            CommontConstants::FILE_CODE => $request->input(CommontConstants::FILE_CODE, ''),
            CommontConstants::TOURIST_GUIDE_CODE => $request->input(CommontConstants::TOURIST_GUIDE_CODE, ''),
            CommontConstants::FULL_NAME => $request->input(CommontConstants::FULL_NAME, ''),
            CommontConstants::STATUS => $request->input(CommontConstants::STATUS, ''),
            CommontConstants::PROVINCE_CODE => $request->input(CommontConstants::PROVINCE_CODE, ''),
            CommontConstants::MEMBER_CODE => $request->input(CommontConstants::MEMBER_CODE, ''),
            CommontConstants::PROVINCE_TYPE => $getProvinceType,
            'rank_status' => $request->input('rank_status', ''),
        ];

        return ListMemberServiceProvider::listSearchByConditionsRank($arrConditions, $limit, $isCount);
    }

    public function getScoreFileAward($member_id, $rank_id) {
        $memberFileAward = Member::query()
            ->select('members.fullName', 'member_file_award.score_file', 'member_file_award.score_award')
            ->leftJoin('member_file_award', function ($join) use ($rank_id) {
               $join->on('member_file_award.member_id', '=', 'members.id')
                   ->where('member_file_award.rank_id', $rank_id);
            })
            ->where('members.id', $member_id)
            ->first();
        return response()->json([
            'score_file' => $memberFileAward->score_file ?? 0,
            'score_award' => $memberFileAward->score_award ?? 0,
            'member_name' => $memberFileAward->fullName ?? '',
        ]);
    }

    public function listMemberByEditor(Request $request)
    {
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objMembers = $this->_searchByEditorCondition($request, $limit,false);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }
        $searchCommon = $this->getStatusByStep();
        unset($searchCommon['arrFile'][9]);
        unset($searchCommon['arrFile'][13]);

        $searchCommon['arrFile'][61] = 'Chờ cấp lại thẻ';
        $searchCommon['arrFile'][90] = 'Chờ in thẻ lần đầu';
        $searchCommon['arrFile'][91] = 'Chờ in thẻ gia hạn';
        $searchCommon['arrFile'][92] = 'Chờ in thẻ cấp lại';
        $searchCommon['arrFile'][130] = 'Hội viên chính thức';
        $searchCommon['arrFile'][131] = 'Đã in thẻ gia hạn';
        $searchCommon['arrFile'][132] = 'Đã in thẻ cấp lại';

        $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
        $tourist_guide_type = MemberConstants::$member_typeOfTravelGuide;

        return view('admin.list.index', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            CommontConstants::COUNT_MEMBER => $objMembers->total(),
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ?  $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'query_string' => $query_string,
            'offices' => Offices::searchByAllOffices(),
            'status' => !empty($request->input('status')) ? $request->input('status') : '',
            'tourist_guide_type' => $tourist_guide_type
        ]);
    }

    private function _searchByEditorCondition($request, $limit,$isCount)
    {
        if (!empty($request->input(CommontConstants::FROM_DATE))) {
            $fromDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::FROM_DATE, ''))->toDateString();
        } else {
            $fromDate = '';
        }

        if (!empty($request->input(CommontConstants::TO_DATE))) {
            $toDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::TO_DATE, ''))->toDateString();
        } else {
            $toDate = '';
        }

        $arrConditions = [
            CommontConstants::FILE_CODE => $request->input(CommontConstants::FILE_CODE, ''),
            CommontConstants::TOURIST_GUIDE_CODE => $request->input(CommontConstants::TOURIST_GUIDE_CODE, ''),
            CommontConstants::FULL_NAME => $request->input(CommontConstants::FULL_NAME, ''),
            CommontConstants::FROM_DATE => $fromDate,
            CommontConstants::TO_DATE => $toDate,
            CommontConstants::STATUS => $request->input(CommontConstants::STATUS, ''),
            CommontConstants::MEMBER_CODE => $request->input(CommontConstants::MEMBER_CODE, ''),
        ];

        return ListMemberServiceProvider::listMemberByRoleEditor($arrConditions, $limit,$isCount);
    }

    public function getMemberForList($memberId) {
        $member = MemberServiceProvider::getMemberById($memberId);
        if (empty($member)) {
            return response()->json([
                'data' => 'no found',
                'status' => 200
            ]);
        }
        else {
            $member->typeOfTravelGuideName = MemberConstants::$member_typeOfTravelGuide;
            $member->expirationDateFormat = !empty($member->expirationDate) ? date('d/m/Y', strtotime($member->expirationDate)) : null;
            return response()->json([
                'data' => $member,
                'status' => 200
            ]);
        }
    }

    public function updateTouristCode($id, Request $request) {
        $data = $request->all();
        $member = MemberServiceProvider::getMemberById($id);
        $tourist_code_old = $member->touristGuideCode;
        $expiration_old = $member->expirationDate;
        $rules = [
            'touristGuideCode' => [
                Rule::unique('members')->ignore($member->id),
            ],
        ];

        $messages = [
            'touristGuideCode.unique'  => 'Số thẻ HDV du lịch đã tồn tại'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => $errors->first('touristGuideCode'),
                'status' => 422
            ]);
        }

        $date = $data['expirationDate'];
        $expiration = str_replace('/', '-', $date);
        $content = "Cập nhật số thẻ HDV";

        $isUpdated = MemberServiceProvider::updateMemberCodeFromTouristType($id, $data);
        $att = [
            'touristGuideCode_old' => $tourist_code_old,
            'touristGuideCode_new' => $data['touristGuideCode'],
            'expiration_old'       => $expiration_old,
            'expiration_new'       => date('Y-m-d 00:00:00', strtotime($expiration))
        ];
        NoteServiceProvider::createNoteWithMemberCode($id, $content , MemberConstants::IS_PROVIDING_WAITTING_VAL_ACTION, MemberConstants::CODE_PROVIDING_REWAITING, $att, $tourist_code_old, $data['touristGuideCode']);

        if ($data['typeOfTravelGuide'] != $member->typeOfTravelGuide) {
            $member->status = 61;
            $member->save();

            WaitingPrint::where('memberId', $id)->delete();
        }

        if (!$isUpdated) {
            return response()->json([
                'error' => 'Đã có lỗi trong quá trình cập nhật',
                'status' => 200
            ]);
        }
        return response()->json([
            'success' => 'Cập nhật số thẻ HDV thành công!',
            'status' => 200
        ]);
    }

    private function ignoreSomeAttributes(Request $request, &$attributes)
    {
        // We don't need update avatar in case of no change
        $fileName = $request->input(self::FILE_NAME);
        $file = $request->file(self::USER_PHOTO, null);
        if (!empty($fileName) && is_null($file)) {
            unset($attributes[self::AVATAR]);
        }
    }


    private function normalizedTouristGuideCodeFrom(Request $request)
    {
        $this->rules[self::TOURIST_GUIDE_CODE] = [
            'required',
            'digits:9',
            Rule::unique('members')->where(function ($query) {
                return $query->where(self::PHONE_VERIFIED, 1)->where(self::ACCEPTED, 1)->whereNull(self::DELETED);
            })
        ];

        $touristGuideCode = $request->input(self::TOURIST_GUIDE_CODE);
        $touristGuideCode = trim($touristGuideCode);
        $touristGuideCode = str_replace(' ', '', $touristGuideCode);
        return [self::TOURIST_GUIDE_CODE => $touristGuideCode];
    }

    private function getEducationsFrom(Request $request)
    {
        return [
            self::BRANCH_ID => $request->input(self::EDUCATION_BRANCHES, 1),
            self::DEGREE_ID => $request->input(self::EDUCATION_DEGREES, 1),
            self::FILE => $request->file(self::EDUCATION_FILES, 0)
        ];
    }

    private function getLanguageSkillsFrom(Request $request)
    {
        if (empty($request->input(self::LANGUAGE)) || empty($request->input(self::LEVEL2))) {
            $languageSkills[] = [];
        } else {
            $languageSkills[] = [
                self::LANGUAGE_ID => $request->input(self::LANGUAGE),
                self::LEVEL_ID => $request->input(self::LEVEL2),
                self::FILE => null,
            ];
        }

        return $languageSkills;
    }

    private function getWorkHistoryFrom(Request $request)
    {
        $workHistory = [];
        $workElementNames = $request->input(self::WORK_ELEMENT_NAMES, []);
        $workTypeOfContracts = $request->input(self::WORK_TYPE_OF_CONTRACTS, []);
        $workFromDates = $request->input(self::WORK_FROM_DATES, []);
        $workToDates = $request->input(self::WORK_TO_DATES, []);
        foreach ($workElementNames as $key => $workElementName) {
            $workHistory[] = [
                self::ELEMENT_NAME => $workElementName,
                self::TYPE_OF_CONTRACT => $workTypeOfContracts[$key],
                self::FROM_DATE => isset($workFromDates[$key]) ? $workFromDates[$key] . '-01-01 00:00:00' : null,
                self::TO_DATE => isset($workToDates[$key]) ? $workToDates[$key] . '-01-01 00:00:00' : null,
            ];
        }

        return $workHistory;
    }

    private function getElementsFrom(Request $request)
    {
        return [
            self::GROUP_SIZE_ID => $request->input(self::GROUP_SIZES, 1),
            self::TYPE_GUIDE_ID => $request->input(self::TYPE_GUIDES, 1),
        ];
    }

    private function getMemberAttrsFrom(Request $request)
    {
        $educations[] = $this->getEducationsFrom($request);
        $languageSkills = $this->getLanguageSkillsFrom($request);
        $workHistory = $this->getWorkHistoryFrom($request);
        $elements[] = $this->getElementsFrom($request);

        if (empty($request->input(self::TYPE_OF_PLACE)) || $request->input(self::TYPE_OF_PLACE) == self::MEMBER_BELONGS_TO_CHI_HOI) {
            $request->merge([self::TYPE_OF_PLACE => self::MEMBER_BELONGS_TO_CHI_HOI]);
            $provinceCode = $request->input(self::PROVINCE_CODE);
        } else {
            $request->merge([self::TYPE_OF_PLACE => self::MEMBER_BELONGS_TO_HOI]);
            $provinceCode = $request->input(self::CLUB);
        }

        return [
            self::FULL_NAME => $request->input(self::FULL_NAME),
            self::GENDER => $request->input(self::GENDER),
            self::BIRTHDAY => $request->input(self::DOB),
            self::TOURIST_GUIDE_CODE => $request->input(self::TOURIST_GUIDE_CODE),
            self::EXPIRATION_DATE => empty($request->input(self::EXPIRATION_DATE)) ? null : \DateTime::createFromFormat('d/m/Y', $request->input(self::EXPIRATION_DATE))->format('Y-m-d'),
            self::CMT_CCCD => $request->input(self::CMT_CCCD),
            self::DATE_ISSUED => empty($request->input(self::DATE_ISSUED)) ? null : \DateTime::createFromFormat('d/m/Y', $request->input(self::DATE_ISSUED))->format('Y-m-d'),
            self::ISSUED_BY => $request->input(self::ISSUED_BY),
            self::PERMANENT_ADDRESS => $request->input(self::PERMANENT_ADDRESS),
            self::ADDRESS => $request->input(self::ADDRESS),
            self::FIRST_MOBILE => $request->input(self::FIRST_MOBILE),
            self::SECOND_MOBILE => $request->input(self::SECOND_MOBILE),
            self::FIRST_EMAIL => $request->input(self::FIRST_EMAIL),
            self::SECOND_EMAIL => $request->input(self::SECOND_EMAIL),
            self::TYPE_OF_TRAVEL_GUIDE => $request->input(self::TYPE_OF_TRAVEL_GUIDE),
            self::TYPE_OF_PLACE => $request->input(self::TYPE_OF_PLACE),
            self::EXPERIENCE_YEAR => $request->input(self::EXPERIENCE_YEAR),
            self::INBOUND_OUTBOUND => $request->input(self::INBOUND_OUTBOUND),
            self::EXPERIENCE_LEVEL => $request->input(self::EXPERIENCE_LEVEL),
            self::OTHER_SKILLS => $request->input(self::OTHER_SKILLS),
            self::OTHER_INFORMATION => $request->input(self::OTHER_INFORMATION),
            self::TOURIST_GUIDE_LEVEL => $request->input(self::TOURIST_GUIDE_LEVEL),
            self::ACHIEVEMENTS => $request->input(self::ACHIEVEMENTS),
            self::STATUS => MemberConstants::VERIFICATION_WAITING,
            self::PROVINCE_CODE => $provinceCode,
            self::IS_VERIFIED => null,
            self::IS_FEE => null,
            self::IS_SIGNED => null,
            self::MEMBER_TYPE => null,
            self::GUIDE_LANGUAGE => $request->input(self::GUIDE_LANGUAGE),
            self::FORTE_TOUR => $request->input(self::FORTE_TOUR),
            self::EDUCATIONS => $educations,
            self::MAJOR_SKILLS => [
                self::IDS => $request->input(self::MAJORS, null),
                self::FILES => $request->file(self::MAJOR_FILES, []),
            ],
            self::LANGUAGE_SKILLS => $languageSkills,
            self::ELEMENTS => $elements,
            self::WORK_HISTORY => $workHistory,
            self::AVATAR => $request->input(self::AVATAR),
            self::ACHIEVEMENT_FILES => $request->file(self::ACHIEVEMENT_FILES, []),
            self::PROVINCE => $request->input(self::PROVINCE),
        ];
    }

    private function combineDobFrom(Request $request)
    {
        return [self::DOB => $request->input(self::BIRTH_YEAR) . '-' . $request->input(self::BIRTH_MONTH) . '-' . $request->input(self::BIRTH_DATE)];
    }

    public function updateMemberFileInfo(Request $request, $id)
    {
        $request->merge($this->normalizedTouristGuideCodeFrom($request));
        $request->merge($this->combineDobFrom($request));

        $attributes = $this->getMemberAttrsFrom($request);
        $this->ignoreSomeAttributes($request, $attributes);

        $objMember = null;
        try {
            //update email and phone into history email sms
            $objMemberOld = Member::select('firstEmail', 'firstMobile')->where('id', $id)->first();
            if (!empty($objMemberOld)) {
                $old_email = $objMemberOld->firstEmail;
                $old_mobile = $objMemberOld->firstMobile;
                $updateEmailHistory = HistoryEmailSMS::where('email_sms_to', $old_email)->update([
                    'email_sms_to' => $request->input('firstEmail')
                ]);

                $updateSmsHistory = HistoryEmailSMS::where('email_sms_to', $old_mobile)->update([
                    'email_sms_to' => $request->input('firstMobile')
                ]);
            }
            $objMember = MemberServiceProvider::updateMemberWithoutStatus($attributes, $id);

            //update member phan bo sung thong tin thanh cho tham dinh
            if ($objMember->status == MemberConstants::UPDATE_INFO_1) {
                $objMember->status = MemberConstants::VERIFICATION_WAITING;
                $objMember->save();
            }
            MemberServiceProvider::saveDataMemberIntoPdf($id);
        } catch(\Exception $e) {

            return redirect()->route('admin_member_list_view')
                ->withErrors(['error_message' => 'Lỗi khi thực hiện cập nhật member, vui lòng thử lại'])
                ->withInput();
        }
        if (empty($objMember) || empty($objMember->id)) {
            return redirect()->route('admin_member_list_view')->withErrors(['error_message' => 'Lỗi khi thực hiện cập nhật member, vui lòng thử lại'])
                ->withInput();
        }

        return redirect()->route('admin_member_list_view')->with('success_message', 'Cập nhật hồ sơ thành công');
    }

    public function listNotOfficialMember(Request $request)
    {
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        if (!empty($request->input('birthday'))) {
            $birthday = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input('birthday', ''))->toDateString();
        } else {
            $birthday = '';
        }

        $arr = [
            'birthday' => $birthday,
            'fullName' => !empty($request->input('fullName')) ? $request->input('fullName') : '',
            'firstMobile' => !empty($request->input('firstMobile')) ? $request->input('firstMobile') : '',
            'firstEmail' => !empty($request->input('firstEmail')) ? $request->input('firstEmail') : ''
        ];

        $objMembers = ListMemberServiceProvider::getListNotMember($arr, $limit, false);
        $countMember = count(ListMemberServiceProvider::getListNotMember($arr, $limit, true));

        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }

        $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';

        return view('admin.list.notmember', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::COUNT_MEMBER => $countMember,
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ?  $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'query_string' => $query_string
        ]);
    }

    public function sendNotificationEmailToNotMember(Request $request)
    {
        $strMemberIdList = $request->input('list_id');
        $arrResendNotificationMemberId = explode(',', $strMemberIdList);
        $member = ListMemberServiceProvider::getMembersByIds($arrResendNotificationMemberId);
        foreach ($member as $key => $objMember) {
            if (!empty($objMember->firstEmail)) {
                $this->_sendJoiningMail($objMember, $objMember->firstEmail, 'joining', $key);
            }

            if (!empty($objMember->secondEmail)) {
                $this->_sendJoiningMail($objMember, $objMember->secondEmail, 'joining', $key);
            }
        }

        return redirect()->route('admin_member_list_not_official')->with('successes', ['Gửi thư mời email gia nhập hội thành công']);
    }

    public function sendNotificationSmsToNotMember(Request $request)
    {
        $strMemberIdList = $request->input('list_id');
        $arrResendNotificationMemberId = explode(',', $strMemberIdList);
        $member = ListMemberServiceProvider::getMembersByIds($arrResendNotificationMemberId);
        foreach ($member as $key => $objMember) {
            $smsContent = 'Kinh Moi gia nhap Hoi Huong Dan Vien Du Lich Viet Nam de du dieu kien hanh nghe HDV theo nghi dinh 45/2019/NĐ-CP va tham gia san giao dich viec lam cua hoi. Chi tiet truy cap http://hoihuongdanvien.vn hoac LH 024.37835120, 37835122. Tran trong';

            if (!empty($objMember->firstMobile)) {
                dispatch((new SendSms($objMember->firstMobile, $smsContent))->delay(Carbon::now()->addSeconds($key + 5)));
            }

            if (!empty($objMember->secondMobile)) {
                dispatch((new SendSms($objMember->secondMobile, $smsContent))->delay(Carbon::now()->addSeconds($key + 5)));
            }
        }

        return redirect()->route('admin_member_list_not_official')->with('successes', ['Gửi thư mời sms gia nhập hội thành công']);
    }

    private static function _sendJoiningMail($objMember, $email, $mailTemplate, $key)
    {
        if (!empty($email)) {
            try {
                $bcc_email = 'hoihdv@gmail.com';
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                //$bcc = [['address' => $bcc_email, 'name' => $bcc_email]];
                $subject = '';

                $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL07')->first();
                $attach = null;
                
                if (!empty($getEmail)) {
                    $subject = $getEmail->title;
    
                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }

                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];

                $template = 'admin.member.mail.' . $mailTemplate;
                // dispatch((new SendEmail($template, $data, $to, $bcc, $subject))->delay(5));
                dispatch((new SendEmail($template, $data, $to, '', $subject, $attach))->delay(Carbon::now()->addSeconds($key + 5)));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_joining', 'Error when send mail joining (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember->id], true) . ')');
            }
        }
    }

    // bao cao tong hop
    public function generalReport(Request $request)
    {
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objMembers = $this->_searchByCondition($request, $limit,false, self::GENERAL_REPORT);
        $countMember = $objMembers->total();

        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }

        $searchCommon = $this->getStatusByStep();
        $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
        $member_status = MemberConstants::$member_status_in_general_report;

        return view('admin.list.general_report', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            CommontConstants::COUNT_MEMBER => $countMember,
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ?  $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'query_string' => $query_string,
            'member_status' => $member_status
        ]);
    }

    // xuat excel bao cao tong hop
    public function exportGeneralReport(Request $request) {
        $limit = 100000;
        $fileName = 'BaoCaoTongHop' . date('YmdHis') . '-' . Utils::randomString(6);
        $objMembers = $this->_searchByCondition($request, $limit,false, self::GENERAL_REPORT);

        //chi hoi HDV
        $branchTmp = Offices::get();
        $branchList = [];
        foreach($branchTmp as $value){
            $branchList[$value['id']] = $value['name'];
        }

        $branchTmp2 = Offices::where('option_code', 'BRANCHES03')->get();
        if(!empty($branchTmp2)){
            foreach($branchTmp2 as $value){
                $branchList[$value['id']] = $value['name'];
            }
        }

        $province_code = $request->input('province_code', '');

        $check = 0;
        $provinceCode = '';
        if ($province_code != '') {
            $check = 1;
            $provinceCode = $branchList[$province_code];
        }

        $ranks = Rank::getAllRanks();
        $memberRank = MemberRank::getAllRanks();
        Excel::create($fileName, function($excel) use ($objMembers, $check, $provinceCode, $request, $ranks, $memberRank) {
            $excel->setTitle('HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM');
            $excel->sheet('First sheet', function($sheet) use ($objMembers, $check, $provinceCode, $request, $ranks, $memberRank) {
                $sheet->setWidth(array(
                    'A'     =>  5,
                    'B'     =>  20,
                    'C'     =>  20,
                    'D'     =>  20,
                    'E'     =>  20,
                    'F'     =>  20,
                    'G'     =>  20,
                    'H'     =>  30,
                    'I'     =>  30,
                    'J'     =>  45,
                    'K'     =>  25,
                    'L'     =>  25,
                    'M'     =>  30
                ));

                $sheet->loadView('admin.list.export_general_report', [
                    'objMembers' => $objMembers,
                    'check' => $check,
                    'provinceCode' => $provinceCode,
                    'arr_status' => MemberConstants::$file_const_display_status,
                    CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
                    'ranks' => $ranks,
                    'memberRank' => $memberRank,
                ]);
            });
        })->download('xlsx');
    }

    public function storeMemberNote(Request $request, $memberId) {
        $memberNote = [
            'content' => $request->input('content'),
            'author_id' => auth()->user()->id,
            'member_id' => $memberId
        ];
        $isCreate = MemberNote::create($memberNote);
        if (empty($isCreate)) {
            return redirect()->route('admin_list_member_detail_view', [$memberId, 'is_note' => 1])->withErrors(['Tạo mới ghi chú thất bại']);
        } else {
            return redirect()->route('admin_list_member_detail_view', [$memberId, 'is_note' => 1])->with($this::SUCCESSES, ['Tạo mới ghi chú thành công']);
        }
    }
}
