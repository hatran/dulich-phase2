<?php

namespace App\Http\Controllers\Admin;

use App\Constants\BranchConstants;
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\CommontConstants;
use App\Http\Controllers\Controller;
use App\Libs\Helpers\Utils;
use App\Models\GroupSize;
use App\Models\Language;
use App\Models\LanguageLevel;
use App\Models\MemberPayments;
use App\Models\MemberRanked;
use App\Models\TypeGuide;
use App\Models\User;
use App\Models\MemberTmp;
use App\Models\Member;
use App\Models\Offices;
use App\Models\MajorSkill;
use App\Models\Major;
use App\Libs\Helpers\BarcodeHelpers;
use App\Providers\CodeServiceProvider;
use App\Providers\LogServiceProvider;
use App\Providers\MemberServiceProvider;
use App\Providers\MemberPaymentsServiceProvider;
use App\Providers\MemberPaymentsYearlyServiceProvider;
use App\Providers\NoteServiceProvider;
use App\Providers\UserServiceProvider;
use App\Libs\Helpers\LogsHelper;
use App\Repositories\Branches\BranchesRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\OtherConstant;
use App\Models\WorkConstant;
use App\Models\Option;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Models\EducationBranch;
use App\Models\EducationDegree;
use App\Models\Branches;
use App\Models\ForteTour;
use App\Models\Rank;
use App\Models\MemberRank;
use App\Jobs\SendEmail;
use App\Jobs\SendSms;
use App\Models\Emails;
use App\Models\WaitingPrint;
use App\Models\Printed;
use ZipArchive;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    const LAST_NOTE = 'last_note';
    const ACTION_TYPE = 'typeOfAction';
    const EDUCATION_BRANCHES = 'educationBranches';
    const EDUCATION_DEGREES = 'educationDegrees';
    const MAJORS = 'majors';
    const LANGUAGES = 'languages';
    const LANGUAGE_LEVELS = 'languageLevels';
    const GROUP_SIZES = 'groupSizes';
    const TYPE_GUIDES = 'typeGuides';
    const OBJ_MEMBER = 'objMember';
    const OBJ_MEM = 'objMem';
    const POSITION = 'position';
    const FORTETOUR = 'forteTour';

    public function index(Request $request)
    {
        $status = $request->input(CommontConstants::STATUS, '');
        if ($status) {
            if (! UserServiceProvider::isAdmin()) {
                if (! (auth()->user()->hasPermissions('is_expert_user') && auth()->user()->hasPermissions('is_leader_user'))) {
                    if (UserServiceProvider::isHnLeaderUser() || UserServiceProvider::isDnLeaderUser() || UserServiceProvider::isHcmLeaderUser()) {
                        if(!in_array($status, [MemberConstants::APPROVAL_WAITING, MemberConstants::APPROVAL_REJECTED, MemberConstants::APPROVE_WAITING_UPDATE])) {
                            abort(404);
                        }
                    }

                    if (UserServiceProvider::isHnExpertUser() || UserServiceProvider::isDnExpertUser() || UserServiceProvider::isHcmExpertUser()) {
                        if(!in_array($status, [MemberConstants::VERIFICATION_WAITING, MemberConstants::VERIFICATION_REJECTED, MemberConstants::UPDATE_INFO_1])) {
                            abort(404);
                        }
                    }
                }
            }
        }

        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objMembers = $this->_searchByCondition($request, $limit, false);
        $countMember = count($this->_searchByCondition($request, $limit, true));

        $current_page =  empty($request['page'])? 0 :$request['page'];
        if ($current_page <= 1){
            $current_page = 0;
        } else {
            $current_page = ($current_page -1 ) * 10;
        }

        $searchCommon = $this->getStatusByStep($status);
        return view('admin.member.index', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            CommontConstants::COUNT_MEMBER => $countMember,
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ? $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'offices' => Offices::searchByAllOffices(),
            'status' => !empty($request->input('status')) ? $request->input('status') : '',
        ]);
    }
    private function _searchByConditionPrintCard($request, $limit, $isCount)
    {
        $arrConditions = $this->createSearchCondition($request);
        if (!empty($request->input(CommontConstants::PROVINCE_TYPE))) {
            $getProvinceType = $this->searchByProvinceType($request->input(CommontConstants::PROVINCE_TYPE));
        }
        else {
            $getProvinceType = '';
        }
        $arrConditions[CommontConstants::PROVINCE_TYPE] = $getProvinceType;
        return MemberServiceProvider::searchByConditionsForPrintCard($arrConditions, $limit, $isCount);
    }

    private function _searchByCondition ($request, $limit, $isCount)
    {
        $arrConditions = $this->createSearchCondition($request);
        if (!empty($request->input(CommontConstants::PROVINCE_TYPE))) {
            $getProvinceType = $this->searchByProvinceType($request->input(CommontConstants::PROVINCE_TYPE));
        }
        else {
            $getProvinceType = '';
        }
        $arrConditions[CommontConstants::PROVINCE_TYPE] = $getProvinceType;
        return MemberServiceProvider::searchByConditions($arrConditions, $limit, $isCount);
    }

    private function _searchByConditionForCreateCard ($request, $limit, $isCount)
    {
        $arrConditions = $this->createSearchCondition($request);
        if (!empty($request->input(CommontConstants::PROVINCE_TYPE))) {
            $getProvinceType = $this->searchByProvinceType($request->input(CommontConstants::PROVINCE_TYPE));
        }
        else {
            $getProvinceType = '';
        }
        $arrConditions[CommontConstants::PROVINCE_TYPE] = $getProvinceType;
        return MemberServiceProvider::searchByConditionsForCreateCard($arrConditions, $limit, $isCount);
    }

    private function createSearchCondition($request) {
        if (!empty($request->input(CommontConstants::FROM_DATE))) {
            $fromDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::FROM_DATE, ''))->toDateString();
        } else {
            $fromDate = '';
        }

        if (!empty($request->input(CommontConstants::TO_DATE))) {
            $toDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::TO_DATE, ''))->toDateString();
        } else {
            $toDate = '';
        }

        if (!empty($request->input('from_date_expiration'))) {
            $fromDateExpiration = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input('from_date_expiration', ''))->toDateString();
        } else {
            $fromDateExpiration = '';
        }

        if (!empty($request->input('to_date_expiration'))) {
            $toDateExpiration = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input('to_date_expiration', ''))->toDateString();
        } else {
            $toDateExpiration = '';
        }

        $arrConditions = [
            CommontConstants::FILE_CODE          => $request->input(CommontConstants::FILE_CODE, ''),
            CommontConstants::TOURIST_GUIDE_CODE => $request->input(CommontConstants::TOURIST_GUIDE_CODE, ''),
            CommontConstants::FULL_NAME          => $request->input(CommontConstants::FULL_NAME, ''),
            CommontConstants::FROM_DATE          => $fromDate,
            CommontConstants::TO_DATE            => $toDate,
            CommontConstants::STATUS             => $request->input(CommontConstants::STATUS, ''),
            CommontConstants::PROVINCE_CODE      => $request->input(CommontConstants::PROVINCE_CODE, ''),
            CommontConstants::PROVINCE_TYPE      => $request->input(CommontConstants::PROVINCE_TYPE, ''),
            CommontConstants::IS_PRINTING        => $request->input(CommontConstants::IS_PRINTING, ''),
            CommontConstants::MEMBER_CODE        => $request->input(CommontConstants::MEMBER_CODE, ''),
            'from_date_expiration'               => $fromDateExpiration,
            'to_date_expiration'                 => $toDateExpiration
        ];

        return $arrConditions;
    }

    public function show($id)
    {
        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
        }

        if (empty($objMember)) {
            abort(404);
        }
        if($objMember->is_delete) {
            return redirect()->route('admin_member_list_view')->withErrors(['Hồ sơ này đã bị xóa khỏi hệ thống']);
        }
        /*$typeOfPlaces = array();
        if (UserServiceProvider::isHnLeaderUser() || UserServiceProvider::isHnExpertUser()) {
            $typeOfPlaces = BranchConstants::$hn_typeOfPlace;
        } else if (UserServiceProvider::isDnLeaderUser() || UserServiceProvider::isDnExpertUser()) {
            $typeOfPlaces = BranchConstants::$dn_typeOfPlace;
        } else if (UserServiceProvider::isHcmLeaderUser() || UserServiceProvider::isHcmExpertUser()) {
            $typeOfPlaces = BranchConstants::$hcm_typeOfPlace;
        }*/
        // if (!in_array($objMember->province_code, $typeOfPlaces)) {
        //     return redirect()->route('admin_member')->withErrors(['Bạn không có quyền truy cập hồ sơ này']);
        // }

        $objEducation = MemberServiceProvider::getEducationByMemberId($objMember->id);
        $objLanguageSkills = MemberServiceProvider::getLanguageAchievedByMemberId($objMember->id);
        // các bảng này quan hệ với member loại 1 - 1
        $objMajorSkills =  MajorSkill::where('memberId', $objMember->id)->first();
        if (!empty($objMajorSkills) ) {
            $objMajor = Major::where('id', $objMajorSkills->majorId)->first();
        }

        $workHistories = MemberServiceProvider::getWorkHistoryByMemberId($objMember->id);
        $objElement = MemberServiceProvider::getElementByMemeber($objMember);
        $paymentInfor = MemberPayments::where('memberId', $objMember->id)->first();
        $notes = MemberServiceProvider::getDetailMemberById($objMember->id);

        $otherConstant = $workConstant = $provincial = $chihoi = $clbthuochoi = [];
        $otherTmp = OtherConstant::where('status', 1)->orderBy('id', 'asc')->get();
        $workTmp = WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get();
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($otherTmp)){
            foreach($otherTmp as $key => $value){
                $otherConstant[$value->id] = $value->otherName;
            }
        }
        if(!empty($workTmp)){
            foreach($workTmp as $key => $value){
                $workConstant[$value->id] = $value->workName;
            }
        }
        if(!empty($optionTmp)){
            foreach($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if(!empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }
        if(!empty($clbthuochoTmp)){
            foreach($clbthuochoTmp as $key => $value){
                $clbthuochoi[$value->id] = $value->name;
            }
        }

        if ($objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER) {
            $ranks = Rank::getAllRanks();
            $memberRank = MemberRank::getRankByMemberId($id);
        }

        return view('admin.member.detail', [
            CommontConstants::OBJ_MEMBER => $objMember,
            CommontConstants::TYPE_OF_TRAVEL_GUIDE => empty($objMember->typeOfTravelGuide) ? '' : $objMember->typeOfTravelGuide,
            CommontConstants::TYPE_OF_PLACE => empty($objMember->typeOfPlace) ? '' : $objMember->typeOfPlace,
            CommontConstants::BRAND => empty($objEducation[CommontConstants::BRAND]) ? '' : $objEducation[CommontConstants::BRAND],
            CommontConstants::DEGREE => empty($objEducation[CommontConstants::DEGREE]) ? '' : $objEducation[CommontConstants::DEGREE],
            CommontConstants::MAJORS => empty($objMajor) ? '' : $objMajor,
            CommontConstants::LANGUAGE => empty($objLanguageSkills[CommontConstants::LANGUAGE]) ? '' : $objLanguageSkills[CommontConstants::LANGUAGE],
            CommontConstants::LANGUAGE_LEVEL => empty($objLanguageSkills[CommontConstants::LANGUAGE_LEVEL]) ? '' : $objLanguageSkills[CommontConstants::LANGUAGE_LEVEL],
            CommontConstants::WORKHISTORY => empty($workHistories) ? '' : $workHistories,
            CommontConstants::TYPE_GUIDE_NAME =>empty($objElement[CommontConstants::TYPE_GUIDE_NAME]) ? '' : $objElement[CommontConstants::TYPE_GUIDE_NAME],
            CommontConstants::GROUP_SIZE_NAME =>empty($objElement[CommontConstants::GROUP_SIZE_NAME]) ? '' : $objElement[CommontConstants::GROUP_SIZE_NAME],
            CommontConstants::OBJ_PAYMENT => empty($paymentInfor) ? '' : $paymentInfor,
            CommontConstants::OBJ_NOTES => empty($notes) ? '' : $notes,
            self::LAST_NOTE => $this->getLastNote($objMember),
            self::ACTION_TYPE => $this->getCurrentAction($objMember),
            CommontConstants::GUIDE_LANGUAGE => empty($objMember->guideLanguage) ? '' : $objMember->guideLanguage,
            CommontConstants::PROFILE_IMG => empty($objMember->profileImg) ? '' : $objMember->profileImg,
            'otherConstant' => $otherConstant,
            'workConstant' => $workConstant,
            'provincial' => $provincial,
            'branch_chihoi' => $chihoi,
            'branch_clbthuochoi' => $clbthuochoi,
            'isProfile' => false,
            'languages' => Language::getAllLanguages(),
            'forteTours' => ForteTour::getAllForteTour(),
            'ranks' => !empty($ranks) ? $ranks : '',
            'memberRank' => !empty($memberRank) ? $memberRank : '',
            'memberOfficial' => MemberConstants::MEMBER_OFFICIAL_MEMBER,
            'rank_status' => MemberConstants::$rank_status
        ]);
    }

    public function showIndexRank($id)
    {
        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
        }

        if (empty($objMember)) {
            abort(404);
        }
        if($objMember->is_delete) {
            return redirect()->route('admin_member_list_view')->withErrors(['Hồ sơ này đã bị xóa khỏi hệ thống']);
        }

        $objEducation = MemberServiceProvider::getEducationByMemberId($objMember->id);
        $objLanguageSkills = MemberServiceProvider::getLanguageAchievedByMemberId($objMember->id);
        $objMajorSkills =  MajorSkill::where('memberId', $objMember->id)->first();
        if (!empty($objMajorSkills) ) {
            $objMajor = Major::where('id', $objMajorSkills->majorId)->first();
        }

        $workHistories = MemberServiceProvider::getWorkHistoryByMemberId($objMember->id);
        $objElement = MemberServiceProvider::getElementByMemeber($objMember);
        $paymentInfor = MemberPayments::where('memberId', $objMember->id)->first();
        $notes = MemberServiceProvider::getDetailMemberById($objMember->id);

        $otherConstant = $workConstant = $provincial = $chihoi = $clbthuochoi = [];
        $otherTmp = OtherConstant::where('status', 1)->orderBy('id', 'asc')->get();
        $workTmp = WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get();
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($otherTmp)){
            foreach($otherTmp as $key => $value){
                $otherConstant[$value->id] = $value->otherName;
            }
        }
        if(!empty($workTmp)){
            foreach($workTmp as $key => $value){
                $workConstant[$value->id] = $value->workName;
            }
        }
        if(!empty($optionTmp)){
            foreach($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if(!empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }
        if(!empty($clbthuochoTmp)){
            foreach($clbthuochoTmp as $key => $value){
                $clbthuochoi[$value->id] = $value->name;
            }
        }

        if ($objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER) {
            $ranks = Rank::getAllRanks();
            $memberRank = MemberRank::getRankByMemberId($id);
        }

        return view('admin.list.indexrank_detail', [
            CommontConstants::OBJ_MEMBER => $objMember,
            CommontConstants::TYPE_OF_TRAVEL_GUIDE => empty($objMember->typeOfTravelGuide) ? '' : $objMember->typeOfTravelGuide,
            CommontConstants::TYPE_OF_PLACE => empty($objMember->typeOfPlace) ? '' : $objMember->typeOfPlace,
            CommontConstants::BRAND => empty($objEducation[CommontConstants::BRAND]) ? '' : $objEducation[CommontConstants::BRAND],
            CommontConstants::DEGREE => empty($objEducation[CommontConstants::DEGREE]) ? '' : $objEducation[CommontConstants::DEGREE],
            CommontConstants::MAJORS => empty($objMajor) ? '' : $objMajor,
            CommontConstants::LANGUAGE => empty($objLanguageSkills[CommontConstants::LANGUAGE]) ? '' : $objLanguageSkills[CommontConstants::LANGUAGE],
            CommontConstants::LANGUAGE_LEVEL => empty($objLanguageSkills[CommontConstants::LANGUAGE_LEVEL]) ? '' : $objLanguageSkills[CommontConstants::LANGUAGE_LEVEL],
            CommontConstants::WORKHISTORY => empty($workHistories) ? '' : $workHistories,
            CommontConstants::TYPE_GUIDE_NAME =>empty($objElement[CommontConstants::TYPE_GUIDE_NAME]) ? '' : $objElement[CommontConstants::TYPE_GUIDE_NAME],
            CommontConstants::GROUP_SIZE_NAME =>empty($objElement[CommontConstants::GROUP_SIZE_NAME]) ? '' : $objElement[CommontConstants::GROUP_SIZE_NAME],
            CommontConstants::OBJ_PAYMENT => empty($paymentInfor) ? '' : $paymentInfor,
            CommontConstants::OBJ_NOTES => empty($notes) ? '' : $notes,
            self::LAST_NOTE => $this->getLastNote($objMember),
            self::ACTION_TYPE => $this->getCurrentAction($objMember),
            CommontConstants::GUIDE_LANGUAGE => empty($objMember->guideLanguage) ? '' : $objMember->guideLanguage,
            CommontConstants::PROFILE_IMG => empty($objMember->profileImg) ? '' : $objMember->profileImg,
            'otherConstant' => $otherConstant,
            'workConstant' => $workConstant,
            'provincial' => $provincial,
            'branch_chihoi' => $chihoi,
            'branch_clbthuochoi' => $clbthuochoi,
            'isProfile' => false,
            'languages' => Language::getAllLanguages(),
            'forteTours' => ForteTour::getAllForteTour(),
            'ranks' => !empty($ranks) ? $ranks : '',
            'memberRank' => !empty($memberRank) ? $memberRank : '',
            'memberOfficial' => MemberConstants::MEMBER_OFFICIAL_MEMBER
        ]);
    }

    private function getLastNote($objMember)
    {
        foreach ($objMember->notes as $note) {
            $last_note = $note->note;
        }
    }

    private function getCurrentAction($objMember)
    {
        switch ($objMember->status) {
            case MemberConstants::VERIFICATION_REJECTED:
                 $typeOfAction = 'submit_verification_reject';
                break;
            case MemberConstants::UPDATE_INFO_1:
                if (UserServiceProvider::isLeaderRole()) {
                    $typeOfAction = 'leader_submit_verification_update';
                } else {
                    $typeOfAction = 'submit_verification_update';
                }
                break;
            case MemberConstants::VERIFICATION_WAITING:
                 $typeOfAction = 'submit_verification_success';
                break;
            case MemberConstants::APPROVAL_REJECTED:
                 $typeOfAction = 'leader_submit_verification_reject';
                break;
            case MemberConstants::APPROVAL_WAITING:
                 $typeOfAction = 'leader_submit_verification_success';
                break;
            case MemberConstants::MEMBER_OFFICIAL_MEMBER;
                $typeOfAction = 'member_official_rank';
                break;
            default:
                $typeOfAction = '';
                break;
        }
        return $typeOfAction;
    }

    public function showtmp($id)
    {
        if (empty($id)) {
            return abort(404);
        }

        $objMemberTmp = MemberTmp::where('member_id', $id)->firstOrFail();
        if (empty($objMemberTmp)) {
            abort(404);
        }

        // thông tin mới
        $objMemberTmp = json_decode($objMemberTmp->content);
        //thông tin cũ
        $objEducation = MemberServiceProvider::getEducationByMemberId($objMemberTmp->member_id);
        $objLanguageSkills = MemberServiceProvider::getLanguageAchievedByMemberId($objMemberTmp->member_id);
        // các bảng này quan hệ với member loại 1 - 1
        $workHistories = MemberServiceProvider::getWorkHistoryByMemberId($objMemberTmp->member_id);
        $objMajorSkills =  MajorSkill::where('memberId', $objMemberTmp->member_id)->first();
        $objMember = MemberServiceProvider::getMemberById($objMemberTmp->member_id);
        $objElement = MemberServiceProvider::getElementByMemeber($objMember);

        if (empty($objMemberTmp->educations[0]->branchId)) {
            $objBrand = $objEducation[CommontConstants::BRAND];
            $objDegree = $objEducation[CommontConstants::DEGREE];
        } else {
            $objBrand = MemberServiceProvider::getBranchById($objMemberTmp->educations[0]->branchId);
            $objDegree = MemberServiceProvider::getDegreeById($objMemberTmp->educations[0]->degreeId);
        }


        if (empty($objMemberTmp->majorSkills->ids)) {
            $objMajor = Major::where('id', $objMajorSkills->majorId)->first();
        } else {
            $objMajor = Major::where('id', $objMemberTmp->majorSkills->ids)->first();
        }

        if (!empty($objMemberTmp->languageSkills[0]->languageId)) {
            $objLanguage = Language::where('id', $objMemberTmp->languageSkills[0]->languageId)->first();
        }
        else {
            $objLanguage = $objLanguageSkills[CommontConstants::LANGUAGE];
        }
        if (!empty($objMemberTmp->languageSkills[0]->levelId)) {
            $objLanguageLevel = LanguageLevel::where('id', $objMemberTmp->languageSkills[0]->levelId)->first();
        } else {
            $objLanguageLevel = $objLanguageSkills[CommontConstants::LANGUAGE_LEVEL];
        }

        if (!empty($objMemberTmp->elements[0]->groupSizeId)) {
            $objGroupSize = GroupSize::where('id', $objMemberTmp->elements[0]->groupSizeId)->first();
            if ($objGroupSize) {
                $groupSizeName = $objGroupSize->groupSize;
            }
        } else {
            $groupSizeName = $objElement[CommontConstants::GROUP_SIZE_NAME];
        }

        if (!empty($objMemberTmp->elements[0]->typeGuideId)) {
            $objTypeGuide = TypeGuide::where('id', $objMemberTmp->elements[0]->typeGuideId)->first();
            if ($objTypeGuide) {
                $typeGuideName = $objTypeGuide->typeName;
            }
        }else{
            $typeGuideName = $objElement[CommontConstants::TYPE_GUIDE_NAME];
        }

        if(!empty($objMemberTmp->typeOfTravelGuide)) {
            $typeOfTravelGuide = $objMemberTmp->typeOfTravelGuide;
        } else {
            $typeOfTravelGuide = $objMember->typeOfTravelGuide;
        }

        if(!empty($objMemberTmp->typeOfPlace)) {
            $typeOfPlace = $objMemberTmp->typeOfPlace;
        } else {
            $typeOfPlace = $objMember->typeOfPlace;
        }
        if (is_array($objMemberTmp->guideLanguage)) {
            $objMemberTmp->guideLanguage = implode(",", $objMemberTmp->guideLanguage);
        }

        if (is_array($objMemberTmp->forteTour)) {
            $objMemberTmp->forteTour = implode(",", $objMemberTmp->forteTour);
        }

        $tmpLanguage = Language::getLanguageName($objMemberTmp->guideLanguage);
        // $listMember[$key]->languageName = !empty($tmpLanguage->languageName) ? $tmpLanguage->languageName : '';
        //$listMember[$key]->languageName = !empty($tmpLanguage) ? $tmpLanguage : '';

        // if($objMemberTmp->guideLanguage > 0){
        //     $objLanguageGuide = Language::where('id', $objMemberTmp->guideLanguage)->first();
        //     if (!empty($objLanguageGuide)) {
        //         $guideLanguage = $objLanguageGuide->languageName;
        //     }
        // } else {
        //     $guideLanguage = $objMember->guideLanguageName;
        // }

        if(!empty($objMemberTmp->guideLanguage)) {
            $objLanguageGuide = Language::getLanguageName($objMemberTmp->guideLanguage);
            if (!empty($objLanguageGuide)) {
                $guideLanguage = $objLanguageGuide;
            }
        } else {
            $objLanguageGuide = Language::getLanguageName($objMemberTmp->guideLanguage);
            if (!empty($objLanguageGuide)) {
                $guideLanguage = $objLanguageGuide;
            }
        }

        if (!empty($objMember->avatar)) {
            $img = $objMember->avatar;
        } else {
            $img = $objMember->profileImg;
        }

        $otherConstant = $workConstant = $provincial = $chihoi = $clbthuochoi = [];
        $otherTmp = OtherConstant::where('status', 1)->orderBy('otherName', 'asc')->get();
        $workTmp = WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get();
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($otherTmp)){
            foreach($otherTmp as $key => $value){
                $otherConstant[$value->id] = $value->otherName;
            }
        }
        if(!empty($workTmp)){
            foreach($workTmp as $key => $value){
                $workConstant[$value->id] = $value->workName;
            }
        }
        if(!empty($optionTmp)){
            foreach($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if(!empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }
        if(!empty($clbthuochoTmp)){
            foreach($clbthuochoTmp as $key => $value){
                $clbthuochoi[$value->id] = $value->name;
            }
        }

        return view('admin.member.detail_tmp', [
            CommontConstants::OBJ_MEMBER => $objMemberTmp,
            CommontConstants::TYPE_OF_TRAVEL_GUIDE => empty($typeOfTravelGuide) ? '' : $typeOfTravelGuide,
            CommontConstants::TYPE_OF_PLACE => empty($typeOfPlace) ? '' : $typeOfPlace,
            CommontConstants::GUIDE_LANGUAGE => empty($guideLanguage) ? '' : $guideLanguage,
            CommontConstants::PROFILE_IMG => empty($img) ? '' : $img,
            CommontConstants::BRAND => empty($objBrand) ? '' : $objBrand,
            CommontConstants::DEGREE => empty($objDegree) ? '' : $objDegree,
            CommontConstants::MAJORS => empty($objMajor) ? '' : $objMajor,
            CommontConstants::LANGUAGE => empty($objLanguage) ? '' : $objLanguage,
            CommontConstants::LANGUAGE_LEVEL => empty($objLanguageLevel) ? '' : $objLanguageLevel,
            CommontConstants::WORKHISTORY => empty($workHistories) ? '' : $workHistories,
            CommontConstants::TYPE_GUIDE_NAME =>empty($typeGuideName) ? '' : $typeGuideName,
            CommontConstants::GROUP_SIZE_NAME =>empty($groupSizeName) ? '' : $groupSizeName,
            CommontConstants::OBJ_PAYMENT => '',
            CommontConstants::OBJ_NOTES => '',
            self::LAST_NOTE => '',
            self::ACTION_TYPE => $this->getCurrentAction($objMember),
            'otherConstant' => $otherConstant,
            'workConstant' => $workConstant,
            'provincial' => $provincial,
            'branch_chihoi' => $chihoi,
            'branch_clbthuochoi' => $clbthuochoi,
            'isProfile' => false,
            'languages' => Language::getAllLanguages(),
            'forteTours' => ForteTour::getAllForteTour(),
            'memberOfficial' => MemberConstants::MEMBER_OFFICIAL_MEMBER
        ]);
    }
    //check hồ sơ dã được xử lý chưa
    public function checkMember (Request $request)
    {
        $status_old = $request->input('status_old');
        $id = $request->input('id');
        $result['status'] = 0;
        $objMember = MemberServiceProvider::getMemberById($id);
        if($status_old != $objMember->status){
            $result['status'] = 1;
        }
        return response()->json($result);
    }

    private function isInvalidApproveStatus($objMember)
    {
        if(!in_array($objMember->status, [MemberConstants::APPROVAL_WAITING, MemberConstants::APPROVE_WAITING_UPDATE, MemberConstants::APPROVAL_REJECTED])) {
            return true;
        }
        return false;
    }

    private function isInvalidVerifyStatus($objMember)
    {
        if(!in_array($objMember->status, [MemberConstants::VERIFICATION_WAITING, MemberConstants::VERIFICATION_REJECTED, MemberConstants::UPDATE_INFO_1])) {
            return true;
        }

        return false;
    }

    private function getMemberProvinceType($objMember) {
        $branches = new BranchesRepository(new Branches());
        $objBranch = $branches->find($objMember->typeOfPlace);
        if (empty($objBranch) || empty($objBranch->parent_id)) {
            $province_type = null;
        } else {
            $province_type = $objBranch->parent_id;
        }

        return $province_type;
    }

    public function update ($id, Request $request)
    {
        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
        }

        if (empty($objMember)) {
            abort(404);
        }

        if($objMember->is_delete == 1) {
            return redirect()->route('admin_member')->withErrors(['Hồ sơ đã xóa khỏi hệ thống.']);
        }

        if($objMember->status == MemberConstants::MEMBER_STORED) {
            return redirect()->route('admin_member')->withErrors(['Hồ sơ đã lưu kho, không thể thực hiện thao tác này.']);
        }

        if((UserServiceProvider::isLeaderRole() || UserServiceProvider::isExpertRole()) && !MemberServiceProvider::isStatus($id, $request->input('status_old'))){
            return redirect()->route('admin_member')->withErrors(['Hồ sơ đã hoàn thành xử lý trước đây.']);
        }

        $formType = $request->input('type_of_action');
        $noteContent = trim($request->input('idea'));
        $province_type = $this->getMemberProvinceType($objMember);
        $officeIDs = MemberServiceProvider::getRepresentativeOfficeID();
        if ($formType == 'submit_verification_reject') {
            if($this->isInvalidVerifyStatus($objMember)) {
                return back()->with('memberInvalid', 'Hồ sơ này đã được hoàn tất xử lý trước đó.');
            }
            MemberServiceProvider::updateMemberStatus($id, MemberConstants::VERIFICATION_REJECTED);
            if (!empty($noteContent)) {
                NoteServiceProvider::createNote($id, $noteContent, MemberConstants::IS_VERIFIED_ACTION, MemberConstants::VERIFICATION_REJECTED);
            }
            MemberServiceProvider::updateVerify($id);
            LogServiceProvider::createSystemHistory(trans('history.fn_verify_reject_member'), [
                'memberId' => $id,
                'status' => MemberConstants::VERIFICATION_REJECTED
            ]);
            // Gửi mail thông báo bao gồm Mã hồ sơ + link download (mail template 2)
            $mailTemplate = 'hn_reject_mail';

            // if ($province_type == null) {
            //     $mailTemplate = 'hn_reject_mail';
            // }
            // else {
            //     foreach ($officeIDs as $officeKey => $officeValue) {
            //         if ($province_type == $officeValue->id) {
            //             if ($officeValue->area == 'HN') {
            //                 $mailTemplate = 'hn_reject_mail';
            //             }
            //             else if ($officeValue->area == 'DN') {
            //                 $mailTemplate = 'dn_reject_mail';
            //             }
            //             else if ($officeValue->area == 'HCM') {
            //                 $mailTemplate = 'hcm_reject_mail';
            //             }
            //             else {
            //                 $mailTemplate = 'hn_reject_mail';
            //             }
            //         }
            //     }
            // }

            // Send approve mail
            MemberServiceProvider::sendRejectMail($id, $mailTemplate);
            return redirect()->route('admin_member', ['status'=>MemberConstants::VERIFICATION_REJECTED])->with(CommontConstants::SUCCESSES, 'Từ chối thẩm định hồ sơ thành công');
        } elseif ($formType == 'submit_verification_success') {
            if($this->isInvalidVerifyStatus($objMember)) {
                return back()->with('memberInvalid', 'Hồ sơ này đã được hoàn tất xử lý trước đó.');
            }

            // update to APPROVAL_WAITING if valid, chờ phê duyệt
            MemberServiceProvider::updateMemberStatus($id, MemberConstants::APPROVAL_WAITING);

            LogServiceProvider::createSystemHistory(trans('history.fn_verify_member'), [
                'memberId' => $id,
                'status' => MemberConstants::APPROVAL_WAITING
            ]);

            NoteServiceProvider::createNote($id, $noteContent, MemberConstants::IS_VERIFIED_ACTION, MemberConstants::APPROVAL_WAITING);
            return redirect()->route('admin_member', ['status'=>MemberConstants::VERIFICATION_WAITING])->with(CommontConstants::SUCCESSES, 'Thẩm định hồ sơ thành công');

        } elseif ($formType == 'leader_submit_verification_success') {
            if($this->isInvalidApproveStatus($objMember)) {
                return back()->with('memberInvalid', 'Hồ sơ này đã được hoàn tất xử lý trước đó.');
            }
            // Chuyển sang trạng thái đã phê duyệt (được phê duyệt)
            MemberServiceProvider::updateMemberStatus($id, MemberConstants::FEE_WAITING);

            LogServiceProvider::createSystemHistory(trans('history.fn_approve_member'), [
                'memberId' => $id,
                'status' => MemberConstants::FEE_WAITING
            ]);

            // Gửi mail thông báo bao gồm Mã hồ sơ + link download (mail template 2)
            $mailTemplate = 'hn_approved_mail';

            // if ($province_type == null) {
                // $mailTemplate = 'hn_approved_mail';
            // }
            // else {
            //     foreach ($officeIDs as $officeKey => $officeValue) {
            //         if ($province_type == $officeValue->id) {
            //             if ($officeValue->area == 'HN') {
            //                 $mailTemplate = 'hn_approved_mail';
            //             }
            //             else if ($officeValue->area == 'DN') {
            //                 $mailTemplate = 'dn_approved_mail';
            //             }
            //             else if ($officeValue->area == 'HCM') {
            //                 $mailTemplate = 'hcm_approved_mail';
            //             }
            //             else {
            //                 $mailTemplate = 'hn_approved_mail';
            //             }
            //         }
            //     }
            // }

            NoteServiceProvider::createNote($id, $noteContent, MemberConstants::IS_APPROVED_ACTION, MemberConstants::FEE_WAITING);
            // Create member data pdf
            MemberServiceProvider::saveDataMemberIntoPdf($id);

            // Send approve mail
            MemberServiceProvider::sendApprovedMail($id, $mailTemplate);
            return redirect()->route('admin_member', ['status'=>MemberConstants::APPROVAL_WAITING])->with(CommontConstants::SUCCESSES, 'Phê duyệt hồ sơ thành công');
        } elseif ($formType == 'submit_verification_update') {
            if($this->isInvalidVerifyStatus($objMember)) {
                return back()->with('memberInvalid', 'Hồ sơ này đã được hoàn tất xử lý trước đó.');
            }
            MemberServiceProvider::updateMemberStatus($id, MemberConstants::UPDATE_INFO_1);

            if (!empty($noteContent)) {
                NoteServiceProvider::createNote($id, $noteContent, MemberConstants::IS_VERIFIED_ACTION, MemberConstants::UPDATE_INFO_1);
            }
            MemberServiceProvider::updateVerify($id);

            LogServiceProvider::createSystemHistory(trans('history.fn_verify_update_member'), [
                'memberId' => $id,
                'old_status' => $objMember->status,
                'status' => MemberConstants::UPDATE_INFO_1
            ]);
            // send mail require to update, yêu cầu update lại hồ sơ
            // Gửi mail thông báo bao gồm Mã hồ sơ + link download (mail template 2)
            $mailTemplate = 'hn_update_info_mail';

            // if ($province_type == null) {
            //     $mailTemplate = 'hn_update_info_mail';
            // }
            // else {
            //     foreach ($officeIDs as $officeKey => $officeValue) {
            //         if ($province_type == $officeValue->id) {
            //             if ($officeValue->area == 'HN') {
            //                 $mailTemplate = 'hn_update_info_mail';
            //             }
            //             else if ($officeValue->area == 'DN') {
            //                 $mailTemplate = 'dn_update_info_mail';
            //             }
            //             else if ($officeValue->area == 'HCM') {
            //                 $mailTemplate = 'hcm_update_info_mail';
            //             }
            //             else {
            //                 $mailTemplate = 'hn_update_info_mail';
            //             }
            //         }
            //     }
            // }

            // Send approve mail
            MemberServiceProvider::sendUpdateInfoMail($id, $mailTemplate);
            return redirect()->route('admin_member', ['status'=>MemberConstants::UPDATE_INFO_1])->with(CommontConstants::SUCCESSES, 'Thẩm định yêu cầu bổ sung hồ sơ thành công');
        } elseif ($formType == 'leader_submit_verification_reject') {
            if($this->isInvalidApproveStatus($objMember)) {
                return back()->with('memberInvalid', 'Hồ sơ này đã được hoàn tất xử lý trước đó.');
            }
            MemberServiceProvider::updateMemberStatus($id, MemberConstants::APPROVAL_REJECTED);
            if (!empty($noteContent)) {
                NoteServiceProvider::createNote($id, $noteContent, MemberConstants::IS_APPROVED_ACTION, MemberConstants::APPROVAL_REJECTED);
            }
            MemberServiceProvider::updateApprove($id);

            LogServiceProvider::createSystemHistory(trans('history.fn_approve_reject_member'), [
                'memberId' => $id,
                'old_status' => $objMember->status,
                'status' => MemberConstants::APPROVAL_REJECTED
            ]);

            // Gửi mail thông báo bao gồm Mã hồ sơ + link download (mail template 2)
            $mailTemplate = 'leader_reject_mail';
            // Send approve mail
            MemberServiceProvider::sendRejectMail($id, $mailTemplate);
            return redirect()->route('admin_member', ['status'=>MemberConstants::APPROVAL_REJECTED])->with(CommontConstants::SUCCESSES, 'Từ chối phê duyệt hồ sơ thành công');
        } elseif ($formType == 'leader_submit_verification_update') {
            if($this->isInvalidApproveStatus($objMember)) {
                return back()->with('memberInvalid', 'Hồ sơ này đã được hoàn tất xử lý trước đó.');
            }
            MemberServiceProvider::updateMemberStatus($id, MemberConstants::APPROVE_WAITING_UPDATE);

            LogServiceProvider::createSystemHistory(trans('history.fn_approve_update_member'), [
                'memberId' => $id,
                'old_status' => $objMember->status,
                'status' => MemberConstants::APPROVE_WAITING_UPDATE
            ]);

            if (!empty($noteContent)) {
                NoteServiceProvider::createNote($id, $noteContent, MemberConstants::IS_APPROVED_ACTION, MemberConstants::APPROVE_WAITING_UPDATE);
            }
            MemberServiceProvider::updateApprove($id);
            // send mail require to update, yêu cầu update lại hồ sơ
            // Gửi mail thông báo bao gồm Mã hồ sơ + link download (mail template 2)
            $mailTemplate = 'hn_update_info_mail';


            // if ($province_type == null) {
            //     $mailTemplate = 'hn_update_info_mail';
            // }
            // else {
            //     foreach ($officeIDs as $officeKey => $officeValue) {
            //         if ($province_type == $officeValue->id) {
            //             if ($officeValue->area == 'HN') {
            //                 $mailTemplate = 'hn_update_info_mail';
            //             }
            //             else if ($officeValue->area == 'DN') {
            //                 $mailTemplate = 'dn_update_info_mail';
            //             }
            //             else if ($officeValue->area == 'HCM') {
            //                 $mailTemplate = 'hcm_update_info_mail';
            //             }
            //             else {
            //                 $mailTemplate = 'hn_update_info_mail';
            //             }
            //         }
            //     }
            // }

            // Send approve mail
            MemberServiceProvider::sendUpdateInfoMail($id, $mailTemplate);
            return redirect()->route('admin_member', ['status'=>MemberConstants::APPROVE_WAITING_UPDATE])->with(CommontConstants::SUCCESSES, 'Phê duyệt yêu cầu bổ sung hồ sơ thành công');
        }
        elseif ($formType == 'manager_success') {
            // lay member_tmp luu lai thang member
            $objOldTmpMember = MemberTmp::where('member_id', $id)->first();
            if (empty($objOldTmpMember)) {
                abort(404);
                $arrOldTmpMember = [];
            } else {
                $arrOldTmpMember = $objOldTmpMember;
            }
            MemberServiceProvider::updateMemberFromMemberTmp($arrOldTmpMember);

            LogServiceProvider::createSystemHistory(trans('history.fn_manager_success'), $arrOldTmpMember);

            NoteServiceProvider::createNote($id, "Đồng ý Phê duyệt cập nhật hồ sơ", MemberConstants::IS_APPROVED_ACTION, MemberConstants::APPROVE_WAITING_UPDATE);
            return redirect()->route('admin_member_list_view')->with(CommontConstants::SUCCESSES_MSG, 'Phê duyệt cập nhật hồ sơ thành công');
        }
        elseif ($formType == 'manager_reject') {
            // từ chối thì update lại status = 13 và không lưu dữ liệu update của user
            // lay member_tmp luu lai thang member
            $objOldTmpMember = Member::where('id', $id)->first();
            if (!empty($objOldTmpMember)) {
                MemberServiceProvider::updateMemberStatusNotCheck($id, MemberConstants::MEMBER_OFFICIAL_MEMBER);

                LogServiceProvider::createSystemHistory(trans('history.fn_manager_reject'), [
                    'memberId' => $id,
                    'old_status' => $objMember->status,
                    'status' => MemberConstants::MEMBER_OFFICIAL_MEMBER
                ]);
            }
            //delete member_tmp
            $objOldTmpMember = MemberTmp::where('member_id', $id)->first();
            if (!empty($objOldTmpMember)) {
                $objOldTmpMember->delete();
            }
            NoteServiceProvider::createNote($id, "Từ chối Phê duyệt cập nhật hồ sơ", MemberConstants::IS_APPROVED_ACTION, MemberConstants::APPROVAL_REJECTED);
            return redirect()->route('admin_member_list_view')->with(CommontConstants::SUCCESSES_MSG, 'Từ chối thành công');
        }
        elseif ($formType == 'member_official_rank') {
            $att = $request->all();
            if (isset($att['rank']) && $att['rank'] != MemberConstants::UNRANK) {
                $objMember->rank_status = MemberConstants::RANK_RANKED;
                $objMember->save();
                MemberRanked::query()->updateOrCreate(['member_id' => $id, 'rank_id' => $att['rank']], ['member_id' => $id, 'rank_id' => $att['rank']]);
            } else {
                return redirect()->route('admin_member_detail_view', ['id' => $id])->withErrors(['Chưa chọn hạng HDV']);
            }

            LogServiceProvider::createSystemHistory(trans('history.fn_member_official_rank'), $att);

            NoteServiceProvider::createNote($id, "Xếp hạng HDV", MemberConstants::MEMBER_OFFICIAL_MEMBER, MemberConstants::MEMBER_OFFICIAL_MEMBER);
            return redirect()->route('admin_member_rank_view', ['status' => MemberConstants::MEMBER_OFFICIAL_MEMBER, 'rank_status' => MemberConstants::RANK_RANKED])
                ->with(CommontConstants::SUCCESSES_MSG, 'Xếp hạng HDV hồ sơ thành công');
        }
    }

    public function createMemberCard (Request $request)
    {
        $status = $request->input(CommontConstants::STATUS, '');
        if($status && !in_array($status, [MemberConstants::CODE_PROVIDING_WAITING, 61, 90, 91, 92])) {
            abort(404);
        }

        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;

        $objMembers = $this->_searchByConditionForCreateCard($request, $limit, false);
        $total = count($this->_searchByConditionForCreateCard($request, $limit, true));
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * $limit;
        }
        // $searchCommon = $this->getStatusByStep();
        $searchCommon['step'] = 5;
        $searchCommon['arrFile'][6] = 'Chờ cấp mã hội viên';
        $searchCommon['arrFile'][61] = 'Chờ cấp lại thẻ';
        $searchCommon['arrFile'][90] = 'Chờ in thẻ lần đầu' ;
        $searchCommon['arrFile'][91] = 'Chờ in thẻ gia hạn';
        $searchCommon['arrFile'][92] = 'Chờ in thẻ cấp lại';
        // $searchByRoleOrArea = $this->getSearchByRoleOrArea($request);

        return view('admin.member.create_card', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            // CommontConstants::AREA_OR_ROLE => $searchByRoleOrArea,
            // CommontConstants::HN_SEARCH => $this->searchByHnArea(),
            // CommontConstants::DN_SEARCH => $this->searchByDnArea(),
            // CommontConstants::HCM_SEARCH => $this->searchByHcmArea(),
            CommontConstants::ALL_SEARCH => $this->searchByAllArea(),
            CommontConstants::PAGINATOR => (count($objMembers) > 0) ? $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            CommontConstants::COUNT_MEMBER => $total,
            'offices' => Offices::searchByAllOffices()
        ]);
    }

    public function apiDetailUser (Request $request)
    {
        $id = $request->input('id');
        $listMember = MemberServiceProvider::getMemberById($id);
        return response()->json($listMember);
    }

    public function apiChangeStatus (Request $request)
    {
        $id = $request->input('id');

        // check member delete
        $member = Member::whereNull('is_delete')->where('id', $id)->first();
        if (empty($member)) {
            return response()->json(['status' => 0, 'errors' => 'Hồ sơ không tồn tại trong hệ thống']);
        }

        if ($member->status != MemberConstants::CODE_PROVIDING_WAITING && $member->status != 61) {
            return response()->json(['status' => 0, 'errors' => 'Hồ sơ không hợp lệ để cấp mã thẻ hội viên hoặc cấp lại thẻ']);
        }

        // do create card
        $result = $this->_createCard($id, $member->status);
        if(!empty($result['errors'])) {
            return response()->json(['status' => 0, 'errors' => $result['errors']]);
        }

        $dataReload = MemberServiceProvider::updateMemberStatus($id, 13);
        //print_r($dataReload);die;
        $dataReload = array('status' => '1', 'errors' => '');
        return response()->json($dataReload);
    }

    private function _createCard ($id, $status)
    {
        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
        }
		if (empty($status)) { $status = $objMember->status; }
        if ($status == MemberConstants::CODE_PROVIDING_WAITING) {
            $objUser = $this->providingCodeCommon($objMember, $id);
            if (empty($objUser)) {
                return array('errors' => 'Tạo mới người dùng thất bại');
            }

            WaitingPrint::updateOrCreate(
                ['memberId' => $objMember->id],
                ['cardType' => 0, 'created_at' => date('Y-m-d H:i:s')]
            );
        }
        else if ($status == MemberConstants::CODE_PROVIDING_REWAITING) {
            $member_code_old = $objMember->member_code;
            $arrMemberCode = MemberServiceProvider::genMemberCode($objMember->typeOfTravelGuide);

            MemberServiceProvider::updateMemberWhenDoCard($id, [
                'member_type'  => MemberConstants::OFFICIAL_MEMBER,
                'member_code'  => $arrMemberCode['member_code'],
                'member_qr_id' => uniqid(15),
                'member_from'  => Carbon::now(),
                CommontConstants::STATUS    => MemberConstants::MEMBER_OFFICIAL_MEMBER
            ]);

            // update code
            CodeServiceProvider::updateOrCreate($objMember->typeOfTravelGuide, $arrMemberCode['current_code']);
            // update username
            $userObj = User::where('memberId', $id)->first();
            if ($userObj) {
                $userObj->username = $arrMemberCode['member_code'];
                $userObj->save();
            }

            $username_old = $userObj->username ?? '';
            $username_new = $arrMemberCode['member_code'];

            $att = [
                'memberCode_old'       => $member_code_old,
                'memberCode_new'       => $arrMemberCode['member_code'],
                'username_old'         => $username_old,
                'username_new'         => $username_new
            ];

            WaitingPrint::where('memberId', $objMember->id)->delete();
			// cấp lại thẻ mới cho hội viên chính thức khi thay đổi số thẻ hdv
            $waitingPrint = new WaitingPrint();
            $waitingPrint->memberId = $objMember->id;
            $waitingPrint->cardType = 2; // cấp lại thẻ
            $waitingPrint->created_at = date('Y-m-d H:i:s');
            $waitingPrint->save();
            $content = "Cập nhật lại mã hội viên mới";
            NoteServiceProvider::createNoteWithMemberCode($objMember->id, $content , MemberConstants::IS_PROVIDING_WAITTING_VAL_ACTION, MemberConstants::CODE_PROVIDING_REWAITING, $att);
        }

        return array('errors' => '');
    }

    public function doCreateMemberCard ($id, Request $request)
    {
        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
        }

        if (empty($objMember)) {
            abort(404);
        }

        if ($request->input('is_signed') == 1 && UserServiceProvider::isMemberShipCardIssuer() && $objMember->status == MemberConstants::SIGNING_WAITING) {
            // Ký quyết định hồ sơ
            $arrUpdateData = [
                'is_signed'   => MemberConstants::IS_SIGNED,
                CommontConstants::STATUS => MemberConstants::CODE_PROVIDING_WAITING
            ];

            MemberServiceProvider::updateMemberWhenDoCard($id, $arrUpdateData);
        }

        return redirect()->route('admin_member_create_card_view');
    }

    public function exportExcelCardList(Request $request)
    {
        $status = $request->input(CommontConstants::STATUS, '');
        if ($status && !in_array($status, [90, 91, 92, 130, 131, 132])) {
            abort(404);
        }

        $isPrint = $request->input('print');
        if ($isPrint) {
            $userId = auth()->user()->id ?? 0;
            $printCardType = $request->input('print_card_type');
            $arrPrintMemberId = MemberServiceProvider::fetchMemberIdExport($userId, $printCardType);
            $arrMemberInfo = [];
            $arrOfffice = [];
            foreach ($arrPrintMemberId as $memberId) {
                $objMember = MemberServiceProvider::getMemberById($memberId);
                if (empty($objMember) || !in_array($objMember->status, array(MemberConstants::CARD_PRINTING_WAITING, MemberConstants::MEMBER_OFFICIAL_MEMBER, MemberConstants::UPDATE_INFO_2))) {
                    continue;
                }
                $arrOfffice[] = $memberId;
                $arrMemberInfo[] = $objMember;
                // begin gen QRCode
                if ($objMember->typeOfPlace == 1) {
                    $typeOfPlaceName = 'Chi hội ' . array_get(Branches::getBranches01(), $objMember->province_code, '');
                } elseif ($objMember->typeOfPlace == 2) {
                    $typeOfPlaceName = 'CLB thuộc hội ' . array_get(Branches::getBranches03(), $objMember->province_code, '');
                } else {
                    $typeOfPlaceName = '';
                }
                //$member_from = empty($objMember->member_from)  ? '' : date('d/m/Y', strtotime($objMember->member_from));
                $member_from = empty($objMember->member_from)  ? '' : $objMember->member_from;

                $strBarCode = 'Số thẻ Hội Viên: ' . $objMember->member_code . "\n" .
                    'Họ và Tên: ' . $objMember->fullName . "\n" .
                    'Ngày Sinh: ' . $objMember->birthday->format('d/m/Y') . "\n" .
                    'Hội Viên VTGA từ: ' . $member_from . "\n" .
                    'Nơi Sinh Hoạt: ' . $typeOfPlaceName . "\n" .
                    'Hạn Sử Dụng Thẻ: ' . date('d/m/Y', strtotime($objMember->member_code_expiration));
					
				$trans2enQr = BarcodeHelpers::convertVn2En($objMember->fullName);				
				$correctNameQr = BarcodeHelpers::convert2LowerUc($trans2enQr);
				$removespaceNameQr = preg_replace('/\s+/', '', $correctNameQr);

                BarcodeHelpers::genBarcode($strBarCode, $objMember->member_code.'-'.$removespaceNameQr);
            }

            $waitingPrintObj = WaitingPrint::select('memberId', 'cardType')->whereIn('memberId', $arrOfffice)->pluck('cardType', 'memberId')->all();

            $arrPrinted = [];
            foreach ($waitingPrintObj as $key => $val) {
                $arrMemberId[] = $key;
                $arrPrinted[] = [
                    'memberId' => $key,
                    'cardType' => $val,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => NULL
                ];
            }

            WaitingPrint::query()->whereIn('memberId', $arrOfffice)->delete();
            Printed::query()->insert($arrPrinted);

            try {
                $filesPrint = Storage::disk('public')->allFiles('printcard');
                Storage::disk('public')->delete($filesPrint);

                //Initializing PHP class
                $zip = new ZipArchive();
                // Add File in ZipArchive
                $fileImageZip = 'Ảnh ngày '. date('Y') .' '. date('m');
                $fileZip = 'In thẻ ngày '. date('Y') .' '. date('m'). ' ' . date('d');
                $zip_path = storage_path($fileImageZip.'.zip');
                $zip->open($zip_path, ZipArchive::CREATE | ZipArchive::OVERWRITE);
                foreach ($arrMemberInfo as $key => $memberImage) {
                    if (str_contains($memberImage->profileImg, 'http://hoihuongdanvien.vn/')) {
                        $relatedPath = ltrim($memberImage->profileImg, 'http://hoihuongdanvien.vn/');
                    }
                    else if (str_contains($memberImage->profileImg, 'http://www.hoihuongdanvien.vn/')) {
                        $relatedPath = ltrim($memberImage->profileImg, 'http://www.hoihuongdanvien.vn/');
                    }

                    if (!empty($relatedPath) && file_exists(public_path($relatedPath))) {
                        $relatedPathPublic = public_path($relatedPath);
                    }

                    $oldPath = $relatedPath;
                    $pathInfo = pathinfo($oldPath);
                    //$fileExtension = $pathInfo['extension'];
					$fileExtension = 'jpg';
					$memberName = preg_replace('/\s+/', '', $memberImage->fullName);
                    //$newName = $memberImage->member_code.'-'.ucwords($memberName).'.'.$fileExtension;
					$newName = $memberImage->member_code.'.'.$fileExtension;
                    $newPathWithName = 'official/'.$newName;
                    if (file_exists($oldPath) && ! file_exists(public_path($newPathWithName))) {
                        Storage::copy($oldPath , $newPathWithName);
                        $zip->addFile(public_path($newPathWithName), $fileImageZip . '/' . $newName);
                    }
                }
                $zip->close();

                $zip = new ZipArchive();
                // Add File in ZipArchive
                $fileQrZip = 'QR ngày '. date('Y') .' '. date('m');
                $qr_zip_path = storage_path($fileQrZip.'.zip');
                $zip->open($qr_zip_path, ZipArchive::CREATE | ZipArchive::OVERWRITE);
                foreach ($arrMemberInfo as $key => $memberQr) {
                    /*$fileExtension = 'png';
                    $memberName = preg_replace('/\s+/', '', $memberQr->fullName);
					$newNameQr = $memberQr->member_code.'-'.ucwords($memberName).'-qr'.$fileExtension;
                    $newPathWithName = 'qr_code/'.$newNameQr;
                    $zip->addFile(storage_path($newPathWithName), $fileQrZip.'/'.$newNameQr);*/
					$fileExtension = 'png';
										
					$trans2en = BarcodeHelpers::convertVn2En($memberQr->fullName);
					$correctName = BarcodeHelpers::convert2LowerUc($trans2en);
					$removespaceName = preg_replace('/\s+/', '', $correctName);
					
                    $newName = $memberQr->member_code.'-'.$removespaceName.'.'.$fileExtension;
                    $newPathWithName = 'qr_code/'.$newName;
                    $zip->addFile(storage_path($newPathWithName), $fileQrZip.'/'.$newName);

                }
                $zip->close();

                $fileName = 'Danh_sach_in_the_ngay_' .date('d').'_'.date('m').'_'.date('Y');

                Excel::create($fileName, function($excel) use ($arrMemberInfo) {
                    $excel->setTitle('HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM');
                    $excel->sheet('First sheet', function($sheet) use ($arrMemberInfo) {
                        // Set width for multiple cells
                        $sheet->setWidth(array(
                            'A'     =>  5,
                            'B'     =>  15,
                            'C'     =>  40,
                            'D'     =>  15,
                            'E'     =>  15,
                            'F'     =>  40,
                            'G'     =>  15
                        ));
                        $sheet->loadView('admin.member.export_card_list_excel_template', ['arrMemberInfo' => $arrMemberInfo]);
                    });
                })->store('xlsx', storage_path());

                // Initializing PHP class
                $zip = new ZipArchive();
                // Add File in ZipArchive
                $zip_path = storage_path('printcard/'.$fileZip.'.zip');
                $zip->open($zip_path, ZipArchive::CREATE | ZipArchive::OVERWRITE);

                $zip->addFile(storage_path($fileImageZip.'.zip'), $fileImageZip.'.zip');
                $zip->addFile(storage_path($fileQrZip.'.zip'), $fileQrZip.'.zip');
                $zip->addFile(storage_path($fileName.'.xlsx'), $fileName.'.xlsx');
                $zip->close();

                $filesImg = Storage::allFiles('official');
                Storage::delete($filesImg);

                Storage::disk('public')->delete($fileImageZip.'.zip');
                Storage::disk('public')->delete($fileQrZip.'.zip');
                Storage::disk('public')->delete($fileName.'.xlsx');

                MemberServiceProvider::deletePrintCardSelectedByUserIdAndType($userId, $printCardType);

                return response()->download(storage_path('printcard/'.$fileZip.'.zip'));
            } catch(\Exception $e) {
                LogsHelper::exceptionByFile('print_card_fail', $e);
            }
        } else {
            $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE_PRINT;
            // list ra những hội viên có status = 9 chờ in thẻ và status = 13 đã là hội viên chính thức (đã in thẻ)
            $objMembers = $this->_searchByConditionPrintCard($request, $limit, false);
            $total = ! empty($objMembers) ? $objMembers->total() : 0;
            //update STT 11022018 binhnd
            $current_page = empty($request['page']) ? 0 : $request['page'];
            if ($current_page <= 1) {
                $current_page = 0;
            } else {
                $current_page = ($current_page - 1) * $limit;
            }

            $listPrintCardSelected = [];
            if (auth()->check()) {
                $listPrintCardSelected = MemberServiceProvider::fetchPrintCardSelectedMemberId(
                    auth()->user()->id
                );
            }

            $searchCommon['step'] = 6;
            $searchCommon['arrFile'][998] = 'Chờ in thẻ tổng hợp';
            $searchCommon['arrFile'][90] = 'Chờ in thẻ lần đầu';
            $searchCommon['arrFile'][91] = 'Chờ in thẻ gia hạn';
            $searchCommon['arrFile'][92] = 'Chờ in thẻ cấp lại';
            $searchCommon['arrFile'][999] = 'Đã in thẻ tổng hợp';
            $searchCommon['arrFile'][130] = 'Đã in thẻ';
            $searchCommon['arrFile'][131] = 'Đã in thẻ gia hạn';
            $searchCommon['arrFile'][132] = 'Đã in thẻ cấp lại';

            return view('admin.member.export_card_list', [
                CommontConstants::OBJ_MEMBERS => $objMembers,
                CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
                CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
                CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
                CommontConstants::ALL_SEARCH => $this->searchByAllArea(),
                CommontConstants::PAGINATOR => (count($objMembers) > 0) ? $objMembers->appends(request()->except('page')) : null,
                CommontConstants::CURRENT_PAGINATOR => $current_page,
                CommontConstants::COUNT_MEMBER => $total,
                'offices' => Offices::searchByAllOffices(),
                'listPrintCardSelected' => $listPrintCardSelected,
                'arrPrintCardType' => MemberServiceProvider::$mappingPrintCardSelectedType,
            ]);
        }
    }

    /**
     * Test function
     *
     * @param $id
     * @return string
     */
    public function genPdf ($id)
    {
        if (empty($id)) {
            return 'Bạn chưa nhập Id';
        }
        MemberServiceProvider::saveDataMemberIntoPdf($id);
        //http://hoihuongdanvien.vn/officesys/member/gen_pdf/1
    }

    public function fakeSendMailApprove ($id)
    {
        if (empty($id)) {
            return 'Bạn chưa nhập Id';
        }
        // Send approve mail
        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
        }

        if (empty($objMember)) {
            abort(404);
        }

        $mailTemplate = '';

        $province_type = $this->getMemberProvinceType($objMember);
        $officeIDs = MemberServiceProvider::getRepresentativeOfficeID();
        if ($province_type == null) {
            $mailTemplate = 'hn_approved_mail';
        }
        else {
            foreach ($officeIDs as $officeKey => $officeValue) {
                if ($province_type == $officeValue->id) {
                    if ($officeValue->area == 'HN') {
                        $mailTemplate = 'hn_approved_mail';
                    }
                    else if ($officeValue->area == 'DN') {
                        $mailTemplate = 'dn_approved_mail';
                    }
                    else if ($officeValue->area == 'HCM') {
                        $mailTemplate = 'hcm_approved_mail';
                    }
                    else {
                        $mailTemplate = 'hn_approved_mail';
                    }
                }
            }
        }

        MemberServiceProvider::sendApprovedMail($id, $mailTemplate);
		return response()->json(['member' => true]);
    }

    private function _extractArrayKey ($arr)
    {
        return collect($arr)->keys()->all();
    }

    public function showEdit($id)
    {
        $objMember = Member::where('id', $id)->firstOrFail();
        $objMem = MemberServiceProvider::getMemberForteTourAndGuideLanguageById($id);
        $enable_edit = true;

        $objMajors = Major::where('status', 1)->orderBy('majorName', 'asc')->get();
        $position  = count($objMajors) + 29;

//        print_r(json_decode(json_encode($objMember)));
        return view('admin.member.profile_edit', [
            self::EDUCATION_BRANCHES => EducationBranch::where('status', 1)->orderBy('branchName', 'asc')->get(),
            self::EDUCATION_DEGREES => EducationDegree::where('status', 1)->orderBy('degree', 'asc')->get(),
            self::MAJORS => $objMajors,
            self::LANGUAGES => Language::where('status', 1)->orderBy('position', 'asc')->get(),
            self::LANGUAGE_LEVELS => LanguageLevel::where('status', 1)->orderBy('levelName', 'asc')->get(),
            self::GROUP_SIZES => GroupSize::where('status', 1)->orderBy('id', 'asc')->get(),
            self::TYPE_GUIDES => TypeGuide::where('status', 1)->orderBy('typeName', 'asc')->get(),
            self::OBJ_MEMBER => empty($objMember) ? null : $objMember,
            self::OBJ_MEM => empty($objMem) ? null : $objMem,
            'enable_edit' => $enable_edit,
            self::POSITION => $position,
            'otherConstant' => OtherConstant::where('status', 1)->orderBy('otherName', 'asc')->get(),
            'workConstant' => WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get(),
            'provincial' => Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get(),
            'branch_chihoi' => Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get(),
            'branch_clbthuochoi' => Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get(),
            self::FORTETOUR => ForteTour::where('status', 1)->get(),
        ]);
    }

    private function providingCodeCommon($objMember, $id) {
        $arrMemberCode = MemberServiceProvider::genMemberCode($objMember->typeOfTravelGuide);
        MemberServiceProvider::updateMemberWhenDoCard($objMember->id, [
            'member_type'  => MemberConstants::OFFICIAL_MEMBER,
            'member_code'  => $arrMemberCode['member_code'],
            'member_qr_id' => uniqid(15),
            'member_from'  => Carbon::now(),
            CommontConstants::STATUS    => MemberConstants::MEMBER_OFFICIAL_MEMBER
        ]);

        // update code
        CodeServiceProvider::updateOrCreate($objMember->typeOfTravelGuide, $arrMemberCode['current_code']);

        // Thay đổi logic ngày 14062018 bởi duong

        $province_type = $this->getMemberProvinceType($objMember);
        // gen password
        $password = Utils::randomString(8);

        // chuyen username = mã HDV
        $username = $arrMemberCode['member_code'];

        $arrNewUser = [
            User::FULLNAME       => $objMember->fullName,
            User::USERNAME       => $username,
            User::EMAIL          => $objMember->firstEmail,
            User::ROLE           => UserConstants::NORMAL_USER,
            User::STATUS         => 1,
            User::PROVINCE_TYPE  => $province_type,
            User::PWD_INPUT      => bcrypt($password),
            User::MEMBER_ID      => $objMember->id
        ];

        User::where('memberId', $objMember->id)->whereNull('deleted_at')->whereNull('is_traveler_company')->delete();
        $objUser = User::create($arrNewUser);
        LogServiceProvider::createSystemHistory(trans('history.fn_create_user'), $arrNewUser);
        // Send mail
        MemberServiceProvider::sendCreateMemberCardSuccessMail($id, $password, $username);
        return $objUser;
    }

    public function providingCodeMany(Request $request) {
        $strMemberIdList = $request->input('list_id');
        $arrMemberId = explode(',', $strMemberIdList);
        foreach ($arrMemberId as $id) {
            $objMember = Member::whereNull('is_delete')->where('id', $id)->first();
            $this->providingCodeCommon($objMember, $id);
            // cấp thẻ mới
            WaitingPrint::updateOrCreate(
                ['memberId' => $id],
                ['cardType' => 0, 'created_at' => date('Y-m-d H:i:s')]
            );
        }

        Member::whereIn('id', $arrMemberId)->whereNull('is_delete')->update([
            'status' => 13
        ]);

        return redirect()->route('admin_member_create_card_view', ['status' => MemberConstants::CODE_PROVIDING_WAITING])->with('successes', ['Cấp mã thẻ hội viên thành công']);
    }

    public function sendNotificationToMember() {
        return view('admin.member.send_notification_to_member', [CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request, 'offices' => Offices::searchByAllOffices()]);
    }

    public function sendNotificationToMemberPost(Request $request) {
        $text = $request->input('text');
        $type = $request->input('type');
        $province = $request->input('province_type');
        $provinceCode = $request->input('province_code');
		$subject = $request->input('subject');
        $countText = $request->input('count');
        $words = Utils::count_words($countText);
        $attach = null;

        $validator = Validator::make($request->all(), [
            'upload' => 'mimes:pdf',
            'text'   => 'required',
            'subject'   => 'required',
        ], [
            'upload.mimes' => 'Yêu cầu định dạng của file đính kèm là pdf',
            'text.required' => 'Yêu cầu nhập nội dung',
            'subject.required' => 'Yêu cầu nhập tiêu đề'
        ]);
            
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        if ($request->hasFile('upload')) {
            $upload = $request->file('upload');
            $attach = [
                'realpath' => $upload->getRealPath(),
                'as' => $upload->getClientOriginalName(),
                'mime' => $upload->getClientMimeType()
            ];
        }
        
        if ($type == 2 && $words > 150) {
            return back()->withInput()->withErrors(['content' => 'Nội dung Sms vượt quá giới hạn 150 chữ!']);
        }

        if ($type == 2 && $attach) {
            return back()->withInput()->withErrors(['alert' => 'File đính kèm chỉ ở phần Email']);
        }
        
		if($type == 1) {
			// send email
			$this->sendNotificationByEmail($provinceCode, $province, $text, $subject, $attach);
		} else {
			// send sms
			$this->sendNotificationBySms($provinceCode, $province, $text);
		}
        return redirect()->route('admin.sendnoti')->with('successes', ['Gửi thông báo thành công!']);
    }

	private function sendNotificationByEmail($proviceCode = "", $province = "", $content, $subject, $attach) {
		$query = Member::select('fullName', 'firstEmail')->whereNull('is_delete');
        $query->where('status', '<>' , -1);

        if (empty($provinceCode)) {
            if(!empty($province)) {
                // chỉ gửi hội viên của VPDD
                $branchIds = array();
                $branches = Offices::getProvinceCodeByArea($province);
                foreach($branches as $key => $branch) {
                    $branchIds[] = $branch['id'];
                }

                // add condition
                $query->whereIn('province', $branchIds);
            }
        } else {
            $query->where('province', $provinceCode);
        }
		
		// gửi tất cả hội viên toàn quốc
		$objMemberChunk = $query->orderBy('id', 'ASC')->get();
		$data = ['email' => $content];
		$template = 'admin.member.mail_send_noti';
		foreach ($objMemberChunk->chunk(100) as $objMember) {
			foreach ($objMember as $objMem) {
				try {
					$to = [['address' => $objMem->firstEmail, 'name' => $objMem->fullName]];
					dispatch((new SendEmail($template, $data, $to, '', $subject, $attach))->delay(Carbon::now()->addSeconds(1)));
				} catch (\Exception $exception) {
					LogsHelper::exceptionByFile('mail_send_noti_fail', $exception, $objMember);
				}
			}
		}
	}

	private function sendNotificationBySms($proviceCode = "", $province = "", $content) {
		$query = Member::select('fullName', 'firstMobile', 'secondMobile')->whereNull('is_delete');
		$query->where('status', '<>' , -1);
        if (empty($provinceCode)) {
    		if(!empty($province)) {
    			// chỉ gửi hội viên của VPDD
    			$branchIds = array();
    			$branches = Offices::getProvinceCodeByArea($province);
    			foreach($branches as $key => $branch) {
    				$branchIds[] = $branch['id'];
    			}

    			// add condition
    			$query->whereIn('province', $branchIds);
    		}
        } else {
            $query->where('province', $provinceCode);
        }
		$objMemberChunk = $query->get();
		foreach ($objMemberChunk->chunk(100) as $objMember) {
			foreach ($objMember as $objMem) {
				try {
					if (!empty($objMem->firstMobile)) {
						dispatch((new SendSms($objMem->firstMobile, $content))->delay(Carbon::now()->addSeconds(1)));
					}

					if (!empty($objMem->secondMobile)) {
						dispatch((new SendSms($objMem->secondMobile, $content))->delay(Carbon::now()->addSeconds(1)));
					}
				} catch (\Exception $exception) {
					LogsHelper::exceptionByFile('sms_send_noti_fail', $exception, $objMember);
				}
			}
		}
	}

	public function printCardSelected(Request $request)
    {
        if (! auth()->check()) {
            abort(404);
        }
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE_PRINT;
        $objMembers = MemberServiceProvider::fetchPrintCardSelected(
            auth()->user()->id,
            $request->input('print_card_type'),
            $limit
        );

        $current_page = empty($request['page']) ? 0 : $request['page'];
        if ($current_page <= 1) {
            $current_page = 0;
        } else {
            $current_page = ($current_page - 1) * $limit;
        }

        return view('admin.member.print_card_selected_list', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => $request,
            CommontConstants::PAGINATOR => ! empty($objMembers) ? $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            CommontConstants::COUNT_MEMBER => ! empty($objMembers) ? $objMembers->total() : 0,
            'arrPrintCardTypeName' => MemberServiceProvider::$mappingPrintCardSelectedName,
            'arrPrintCardSelectedType' => array_flip(MemberServiceProvider::$mappingPrintCardSelectedType),
        ]);
    }

    public function addPrintCardSelected(Request $request)
    {
        if ($request->ajax()) {
            if (MemberServiceProvider::handlePrintCardSelected($request->all(), 'add')) {
                return response()->json([
                   'status' => 200,
                   'message' => 'OK'
                ]);
            }
        }
        return response()->json([
            'status' => 500,
            'message' => 'ERROR'
        ]);
    }

    public function deletePrintCardSelected(Request $request)
    {
        if ($request->ajax()) {
            if (MemberServiceProvider::handlePrintCardSelected($request->all(), 'delete')) {
                return response()->json([
                    'status' => 200,
                    'message' => 'OK'
                ]);
            }
        }

        if ($request->isMethod('post')) {
            if (! auth()->check()) {
                abort(404);
            }

            if (MemberServiceProvider::deletePrintCardSelectedById($request->input('id') ?? 0)) {
                return redirect()->back()->withInput()->with('success', 'Xóa thành công');
            }

            return redirect()->back()->withInput()->with('error', 'Xóa thất bại');
        }
        return response()->json([
            'status' => 500,
            'message' => 'ERROR'
        ]);
    }
}
