<?php

namespace App\Http\Controllers\Admin\Base;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Session;

abstract class BaseController extends Controller
{
    protected $_request = '';
    public $_collection = [];
    public $_path = '';

    // Page Pagination
    protected $_order = 'asc';
    protected $_offset = 0;
    protected $_limit = 10;

    public function __construct(Request $request)
    {
        // REQUEST INIT
        $this->_request = $request->input();

        // PAGE INIT
        $this->_order = $request->input('order');
        $this->_offset = $request->input('offset');
        $this->_limit = $request->input('limit');
    }

    public function getAjaxFilterUrl()
    {
        return url($this->_path) . '?' . http_build_query($this->_request);
    }

    public function getCollection($hasAuth = true)
    {
        $this->addOrderNumber();
    }

    public function addShortField($field, $length)
    {
        $result = [];
        foreach ($this->_collection as $_item) {
            $full = htmlspecialchars($_item[$field]);
            $short = htmlspecialchars($this->shorten((string)($_item[$field]), $length));
            $fullWithShort = "<span class=\"tooltip\" title=\"$full\">$short</span>$short";
            $_item[$field . '_short'] = $fullWithShort;
            $result[] = $_item;
        }
        $this->_collection = $result;
    }

    public function addOrderNumber()
    {
        $result = [];
        $i = 1 + $this->_offset;
        foreach ($this->_collection as $_item) {
            $_item['order_number'] = $i;
            $i++;
            $result[] = $_item;
        }
        $this->_collection = $result;
    }

    function shorten($str, $maxlen)
    {
        if (strlen($str) <= $maxlen - 3) {
            return $str;
        } else {
            return mb_substr($str, 0, $maxlen - 3, "utf-8") . '...';
        }
    }

}