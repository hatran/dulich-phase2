<?php
namespace App\Http\Controllers\Admin;

use App\Constants\BranchConstants;
use App\Constants\UserConstants;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\FeePolicy;
use App\Models\FeePolicyRepository;
use App\Models\FeePolicyMapRepository;
use App\Providers\FeePolicyServiceProvider;
use App\Providers\UserServiceProvider;
use App\Providers\MemberServiceProvider;
use App\Repositories\Branches\BranchesRepository;
use App\Repositories\Users\GroupsAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ClubOfBranch\ClubOfBranchRepository;
use App\Models\Branches;
use App\Repositories\ClubOfHead\ClubOfHeadRepository;
use Hash;
use App\Models\Member;
use Illuminate\Support\Facades\DB;
use App\Models\Offices;
use App\Models\Option;

class FeePolicyController extends Controller
{
    const DEFAULT_LIMIT_USER_PER_PAGE = 10;
    const CODE = 'code';
    const NAME = 'name';
    const START_DATE = 'start_date';
    const END_DATE = 'end_date';
    const MONEY = 'money';
    const NOTE = 'note';
    const TYPEOP = 'typeOp';
    const LIST_ID = 'list_id';
    const LIST_NAME = 'list_name';
    const PROVINCE_ID = 'province_id';
    const PERFECT_CODE = 'perfect_fee_policy_code';
    const PERFECT_NAME = 'perfect_fee_policy_name';
    const STATUS = 'status';
    const USERS_INDEX = 'fee_policy.index';
    const USERS_SHOW = 'fee_policy.show';
    const SUCCESSES = 'successes';
    // const ROLE = 'role';

    public function index(Request $request)
    {

        $limit = $this::DEFAULT_LIMIT_USER_PER_PAGE;

        $groupAdmin = new GroupsAdmin();
        $groups = $groupAdmin->getListAll();

        $branches = new BranchesRepository(new Branches());
        $branchesVpdd = $branches->getVPDD();

        $offices = Offices::getProvinceCodeByProvinceType("");
        $option = Option::where("key","fee-policy")->get();

        if (!empty($request->input('search'))) {

            $arrConditions = [
                $this::NAME          => $request->input($this::NAME, ''),
                $this::CODE          => $request->input($this::CODE, ''),
                $this::START_DATE    => $request->input($this::START_DATE, ''),
                $this::END_DATE      => $request->input($this::END_DATE, ''),
                // $this::USERNAME      => $request->input($this::USERNAME, ''),
                // $this::PROVINCE_TYPE => $request->input($this::PROVINCE_TYPE, ''),
            ];

            $objFeePolicy = FeePolicyServiceProvider::searchByConditions($arrConditions, $limit);
        } else {
            $objFeePolicy = FeePolicyServiceProvider::getAllUser($limit);

        }
        // $objUsers = Users::getAllUser($limit);
        // $user = User::get();

        return view('admin.fee-policy.index', [
            'groups'         => $groups,
            'offices'       => $offices,
            'option'       => $option,
            'objFeePolicy'       => $objFeePolicy,
            'response_view'  => empty($request) ? array() : $request,
            'paginator'      => (count($objFeePolicy) > 0) ? $objFeePolicy->appends(request()->except('page')) : null,
            'current_cursor' => $this->getCurrentCursor($limit, $request),
            'branchesVpdd'   => $branchesVpdd
        ]);
    }

    public function show ($id)
    {
        if (empty($id)) {
            $objFeePolicy = null;
        } else {
            $objFeePolicy = new FeePolicyRepository();
            $objFeePolicy = $objFeePolicy->find($id);
        }
        if (empty($objFeePolicy)) {
            abort(404);
        }

        $offices = Offices::getProvinceCodeByProvinceType("");
        $option = Option::where("key","fee-policy")->get();

        return view('admin.fee-policy.detail', [
            'objFeePolicy' => $objFeePolicy,
            'option' => $option,
            'offices' => $offices,
        ]);
    }

    public function create ()
    {
        $offices = Offices::getProvinceCodeByProvinceType("");
        $option = Option::where("key","fee-policy")->get();

        return view('admin.fee-policy.register', [
            'offices' => $offices,
            'option' => $option
        ]);
    }

    public function store (Request $request)
    {
        $start_date = "";
        $end_date = "";
        //Convert Start Date
        $date = $request->input($this::START_DATE);
        if($date != null && $date != "") {
            $dateArr = explode ("/", $date);
            $start_date = "".$dateArr[2]."-".$dateArr[1]."-".$dateArr[0]." 00:00:00";
        }
        //Convert End Date
        $date = $request->input($this::END_DATE);
        if($date != null && $date != "") {
            $dateArr = explode ("/", $date);
            $end_date = "".$dateArr[2]."-".$dateArr[1]."-" .$dateArr[0]." 23:59:59";
        }

        $objParam = [
            $this::CODE     => $request->input($this::CODE),
            $this::NAME     => $request->input($this::NAME),
            $this::START_DATE      => $start_date,
            $this::END_DATE         => $end_date,
            $this::MONEY        => $request->input($this::MONEY),
            $this::NOTE => $request->input($this::NOTE),
            $this::STATUS => $request->input($this::STATUS),
            $this::LIST_ID => $request->input($this::LIST_ID),
            $this::LIST_NAME => $request->input($this::LIST_NAME),
            $this::TYPEOP => $request->input($this::TYPEOP),
            $this::PERFECT_CODE => $request->input($this::PERFECT_CODE),
            $this::PERFECT_NAME => $request->input($this::PERFECT_NAME),
        ];

        $objFeePolicy = new FeePolicyRepository();
        $objFeePolicy->fill($objParam);
        $objFeePolicy->save();


        if (empty($objFeePolicy)) {
            return redirect()->route('fee_policy.create')->withErrors(['Tạo hội phí thất bại']);
        } else {
            return redirect()->route($this::USERS_INDEX)->with($this::SUCCESSES, ['Tạo mới hội phí thành công']);
        }

        if (empty($objFeePolicy)) {
            return redirect()->route('fee_policy.create')->withErrors(['Tạo mới người dùng thất bại']);
        } else {
            return redirect()->route($this::USERS_INDEX)->with($this::SUCCESSES, ['Tạo mới người dùng thành công']);
        }
    }

    public function update (Request $request, $id)
    {
        if (empty($id)) {
            abort(404);
        }

        $objFeePolicy = new FeePolicyRepository();
        $objFeePolicy = $objFeePolicy->find($id);
        if (empty($objFeePolicy)) {
            abort(404);
        }
        $start_date = "";
        $end_date = "";
        //Convert Start Date
        $date = $request->input($this::START_DATE);
        if($date != null && $date != "") {
            $dateArr = explode ("/", $date);
            $start_date = "".$dateArr[2]."-".$dateArr[1]."-".$dateArr[0]." 00:00:00";
        }
        //Convert End Date
        $date = $request->input($this::END_DATE);
        if($date != null && $date != "") {
            $dateArr = explode ("/", $date);
            $end_date = "".$dateArr[2]."-".$dateArr[1]."-" .$dateArr[0]." 23:59:59";
        }
        $objParam = [
            $this::CODE     => $request->input($this::CODE),
            $this::NAME     => $request->input($this::NAME),
            $this::START_DATE      => $start_date,
            $this::END_DATE         => $end_date,
            $this::MONEY        => $request->input($this::MONEY),
            $this::NOTE => $request->input($this::NOTE),
            $this::STATUS => $request->input($this::STATUS),
            $this::LIST_ID => $request->input($this::LIST_ID),
            $this::LIST_NAME => $request->input($this::LIST_NAME),
            $this::TYPEOP => $request->input($this::TYPEOP),
            $this::PERFECT_CODE => $request->input($this::PERFECT_CODE),
            $this::PERFECT_NAME => $request->input($this::PERFECT_NAME),
        ];

        $objFeePolicy = $objFeePolicy->update($objParam);

        if (empty($objFeePolicy)) {
            return redirect()->route($this::USERS_INDEX, [$id])->withErrors(['Cập nhật hội phí thất bại']);
        } else {
            // MemberServiceProvider::updateMemberUpdatedAt($id);
            return redirect()->route($this::USERS_INDEX, [$id])->with($this::SUCCESSES, ['Cập nhật hội phí thành công']);
        }
    }

    public function destroy ($id)
    {
        if (empty($id)) {
            abort(404);
        }

        $objFeePolicy = new FeePolicyRepository();
        $objFeePolicy = $objFeePolicy->find($id);
        $objFeePolicy = $objFeePolicy->delete($id);
        if (empty($objFeePolicy)) {
            return redirect()->route($this::USERS_INDEX)->withErrors(['Xóa hội phí thất bại']);
        } else {
            return redirect()->route($this::USERS_INDEX)->with($this::SUCCESSES, ['Xóa hội phí thành công']);
        }
    }

    public function checkCode ($code)
    {
            
        $objFeePolicy = FeePolicyRepository::where("code",$code)->get();
        $data['list'] = $objFeePolicy;
        return response()->json($data);
    }

    public function storeUpdate (Request $request)
    {
        try{

            $id = $request->input("id_update");
            $objFeePolicy = new FeePolicyRepository();
            $objFeePolicy = $objFeePolicy->find($id);
            if (empty($objFeePolicy)) {
                abort(404);
            }

            if($request->input($this::TYPEOP) == 0) {
                $objParam = [
                    $this::TYPEOP        => $request->input($this::TYPEOP),
                    $this::LIST_ID        => $request->input($this::LIST_ID  ),
                    $this::LIST_NAME        => $request->input($this::LIST_NAME  ),
                    $this::PERFECT_CODE => $request->input($this::PERFECT_CODE),
                    $this::PERFECT_NAME => $request->input($this::PERFECT_NAME),
                ];
            } else {
                $objParam = [
                    $this::TYPEOP        => $request->input($this::TYPEOP),
                    $this::PERFECT_CODE => $request->input($this::PERFECT_CODE),
                    $this::PERFECT_NAME => $request->input($this::PERFECT_NAME),
                ];
            }

            $objFeePolicy = $objFeePolicy->update($objParam);

            if (empty($objFeePolicy)) {
                return redirect()->route($this::USERS_INDEX, [$id])->withErrors(['Cập nhật hội phí thất bại']);
            } else {
                // MemberServiceProvider::updateMemberUpdatedAt($id);
                return redirect()->route($this::USERS_INDEX, [$id])->with($this::SUCCESSES, ['Cập nhật hội phí thành công']);
            }
        } catch (\Exception $ex) {
            dd($ex);
        }


        $rules = User::$rule;
        $rules['branch_id'] = 'required';
        $rules['first_mobile'] = 'required';
        unset($rules['password_confirmation']);
        unset($rules['password']);
        unset($rules['role']);
        unset($rules['status']);
        $rules['password'] = 'required';
        $messages = User::$message;
        $messages['branch_id.required'] = 'Bạn phải chọn Chi Hội';
        $messages['first_mobile.required'] = 'Số điện thoại là trường bắt buộc';
        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames(User::$niceAttributeName);
        if ($validator->fails()) {
            return redirect()
                ->route('users_create_notmember')
                ->withErrors($validator)
                ->withInput();
        }

        $inputEmail = $request->input($this::EMAIL);
        $emailUser = User::where('email', $inputEmail)->whereNull('deleted_at')->first();
        if (!empty($emailUser)) {
            return redirect()->route('users_create_notmember')->withErrors(['Email đã tồn tại. Vui lòng chọn email khác']);
        }
        $fullName = $request->input($this::FULL_NAME);
        $userName = $request->input($this::USERNAME);
        try {
            DB::beginTransaction();
            $emailMember = Member::query()->where(function ($query) use ($inputEmail)  {
                $query->where('firstEmail', $inputEmail)
                    ->orWhere('secondEmail', $inputEmail);
            })->whereNull('is_delete')->first();
            if (!empty($emailMember)) {
                return back()->withInput()->withErrors(['Email đã tồn tại. Vui lòng chọn email khác']);
            }
            $objMem = new Member();
            $objMem->fullName = $fullName;
            $objMem->firstEmail = $inputEmail;
            $objMem->firstMobile = $request->input('first_mobile');
            $objMem->status = -1;
            $objMem->save();

            User::query()->create([
                $this::FULL_NAME     => $fullName,
                $this::USERNAME      => $userName,
                $this::EMAIL         => $inputEmail,
                'role'               => 1,
                $this::STATUS        => 1,
                $this::PWD_INPUT     => bcrypt($request->input($this::PWD_INPUT)),
                'branch_id'          => $request->input('branch_id'),
                'memberId'           => $objMem->id,
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('users_create_notmember')->withErrors(['Tạo mới người dùng không phải hội viên thất bại']);
        }
        return redirect()->route($this::USERS_INDEX)->with($this::SUCCESSES, ['Tạo mới người dùng không phải hội viên thành công']);
    }
}
