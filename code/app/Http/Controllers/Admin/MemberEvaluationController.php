<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libs\Helpers\Utils;
use App\Models\Rank;
use App\Providers\MemberEvaluationProvider;
use App\Providers\TravelerCompanyRankProvider;
use App\Providers\EvaluationCriteriaProvider;
use App\Providers\MemberServiceProvider;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\Request;
use App\Models\MemberEvaluation;
use App\Models\Option;
use App\Models\Branches;
use App\Constants\CommontConstants;
use App\Constants\MemberConstants;
use App\Constants\QuizConstants;
use App\Providers\UserServiceProvider;
use Carbon\Carbon;
use App\Models\MemberRank;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MemberEvaluationController extends Controller
{
	const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const RESPONSE_VIEW = 'response_view';
    const OBJ_SUBJECTS = 'objEvaluations';

    const COMPANY_NAME = 'company_name';
    const TAX_CODE = 'tax_code';
    const MEMBER_NAME = 'member_name'; //ho ten HDV
    const TOURIRST_GUIDE_CODE = 'tourist_guide_code'; //so the HDV
    const MEMBER_CODE = 'member_code'; //so the hoi vien
    const TOURIST_CODE = 'tourist_code'; //ma doan
    const FORTE_TOUR = 'forte_tour'; //tour tuyen
    const COUNT_SUBJECT= 'count_evaluations';

    const SUBJECT_INDEX = 'member_evaluation.index';
    const SUBJECT_SHOW = 'member_evaluation.show';
    const SUCCESSES = 'successes';
    const OPTIONS = 'options';
    const CURRENT_PAGINATOR = 'current_paginator';
    const PAGINATOR = 'paginator';

    public function createEvaluation($member_id)
    {
        if (auth()->check() && auth()->user()->is_traveler_company == 1) {
        	$taxCode = auth()->user()->username;
            $travelerCompany = TravelerCompanyRankProvider::checkCode($taxCode);
            $position = MemberEvaluationProvider::getPosition();
            $criteria = EvaluationCriteriaProvider::getEvaluationCriteria();

            $member = MemberServiceProvider::getMemberById($member_id);
            if (empty($member)) {
                abort(404);
            }
            $provincial = $chihoi = $clbthuochoi = [];
            $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
            $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
            $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
            $member_typeOfTravelGuide = MemberConstants::$member_typeOfTravelGuide;
            if (!empty($optionTmp)){
                foreach($optionTmp as $key => $value){
                    $provincial[$value->id] = $value->value;
                }
            }
            if (!empty($chihoiTmp)){
                foreach($chihoiTmp as $key => $value){
                    $chihoi[$value->id] = $value->name;
                }
            }
            if (!empty($clbthuochoTmp)){
                foreach($clbthuochoTmp as $key => $value){
                    $clbthuochoi[$value->id] = $value->name;
                }
            }

            return view('admin.member_evaluation.create', [
                'member_id' => $member_id, 
                'travelerCompany' => $travelerCompany, 
                'position' => $position, 
                'criteria' => $criteria,
                'provincial' => $provincial,
                'branch_chihoi' => $chihoi,
                'branch_clbthuochoi' => $clbthuochoi,
                CommontConstants::TYPE_OF_PLACE => empty($member->typeOfPlace) ? '' : $member->typeOfPlace,
                CommontConstants::TYPE_OF_TRAVEL_GUIDE => empty($member->typeOfTravelGuide) ? '' : $member->typeOfTravelGuide,
                CommontConstants::TYPE_OF_PLACE => empty($member->typeOfPlace) ? '' : $member->typeOfPlace,
                'member_typeOfTravelGuide' => $member_typeOfTravelGuide,
                CommontConstants::PROFILE_IMG => empty($member->profileImg) ? '' : $member->profileImg,
                'isProfile' => false,
                'member' => $member
            ]);
        }
        else {
            abort(404);
        }
    }

    public function store(Request $request)
    {
        $objData = [];
        $taxcode_company = $request->input('taxcode_company');
        $member_id = $request->input('member_id');
        $name = $request->input('name');
        $position = $request->input('position');
        $phone = $request->input('phone');
        $tourist_code = $request->input('tourist_code');
        $forte_tour = $request->input('forte_tour');
        $number_of_tourist = $request->input('number_of_tourist');
        $from_date = $request->input('from_date'); 
        $to_date = $request->input('to_date');
        $criteria1 = $request->input('criteria1');
        $criteria2 = $request->input('criteria2');
        $criteria3 = $request->input('criteria3');
        $criteria4 = $request->input('criteria4');
        $criteria5 = $request->input('criteria5');
        $criteria1_code = $request->input('criteria1_code');
        $criteria2_code = $request->input('criteria2_code');
        $criteria3_code = $request->input('criteria3_code');
        $criteria4_code = $request->input('criteria4_code');
        $criteria5_code = $request->input('criteria5_code');

        if (!empty($from_date)) {
            $from_date = str_replace('/', '-', $from_date);
            $from_date = date('Y-m-d', strtotime($from_date));
        }
        
        if (!empty($to_date)) {
    		$to_date = str_replace('/', '-', $to_date);
    		$to_date = date('Y-m-d', strtotime($to_date));
        }

        $objData['taxcode_company']     = $taxcode_company;
        $objData['member_id']           = $member_id;
        $objData['name']                = $name;
        $objData['position']            = $position;
        $objData['phone']               = $phone;
        $objData['tourist_code']        = $tourist_code;
        $objData['forte_tour']          = $forte_tour;
        $objData['number_of_tourist']   = $number_of_tourist;
        $objData['from_date']           = $from_date;
        $objData['to_date']             = $to_date;
        $objData['criteria1']           = $criteria1;
        $objData['criteria2']           = $criteria2;
        $objData['criteria3']           = $criteria3;
        $objData['criteria4']           = $criteria4;
        $objData['criteria5']           = $criteria5;
        $objData['criteria1_code']      = $criteria1_code;
        $objData['criteria2_code']      = $criteria2_code;
        $objData['criteria3_code']      = $criteria3_code;
        $objData['criteria4_code']      = $criteria4_code;
        $objData['criteria5_code']      = $criteria5_code;

        $memberRank = MemberRank::getRankIDByMemberId($member_id);
        if (empty($memberRank)) {
            return redirect()->route('company_create_evaluation', ['id' => $member_id])->withErrors(['Chưa đăng ký hạng HDV để đánh giá']);
        }
        $objData['rank_id'] = $memberRank->rank_id;
        $validated = MemberEvaluationProvider::isValid($objData, MemberEvaluationProvider::$rules, MemberEvaluationProvider::$messages, MemberEvaluationProvider::$niceAttributeName);
        if ($validated instanceof MessageBag) {
            return redirect()->route('company_create_evaluation', ['id' => $member_id])->withInput()->withErrors($validated);
        }

        try {
            MemberEvaluation::query()->updateOrCreate(
                ['tourist_code' => $objData['tourist_code'], 'member_id' => $member_id, 'rank_id' => $memberRank->rank_id],
                $objData
            );
            return redirect()->route('company_create_evaluation', ['id' => $member_id])->with($this::SUCCESSES, ['Thêm mới đánh giá HDV thành công']);
        } catch (\Exception $e) {
            return redirect()->route('company_create_evaluation', ['id' => $member_id])->withInput()->withErrors(['Thêm mới đánh giá HDV thất bại']);
        }
    }

    public function list(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objSubjects = $this->_searchByCondition($request, $limit,false);
        $sumObjSubjects = $objSubjects->total();
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if ($current_page <= 1){
            $current_page = 0;
        } else {
            $current_page = ($current_page -1 ) * 10;
        }
        $criteria = EvaluationCriteriaProvider::getPercentage();
        return view('admin.member_evaluation.index', [
            self::OBJ_SUBJECTS      => $objSubjects,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_SUBJECT     => $sumObjSubjects,
            self::PAGINATOR         => ($sumObjSubjects > 0) ? $objSubjects->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
            'criteria'              => $criteria
        ]);
    }

    public function listFrontend(Request $request)
    {
        if (! auth()->check()) {
            abort(404);
        }
        $objSubjects = $this->_searchByConditionFrontend($request, self::DEFAULT_LIMIT_MEMBER_PER_PAGE,false);
        $sumObjSubjects = $objSubjects->total();
        $current_page = $request['page'] ?? 0;
        if ($current_page >= 1){
            $current_page = ($current_page - 1) * 10;
        }
        $criteria = EvaluationCriteriaProvider::getPercentage();
        $ranks = Rank::getAllRanks();
        $viewData = [
            self::OBJ_SUBJECTS      => $objSubjects,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_SUBJECT     => $sumObjSubjects,
            self::PAGINATOR         => ($sumObjSubjects > 0) ? $objSubjects->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
            'criteria'              => $criteria,
            'ranks'                 => $ranks,
        ];
        $viewTemplate = auth()->user()->is_traveler_company == 1 ? 'frontend.newdesign.member_evaluation.index' : 'frontend.newdesign.member_evaluation.member';
        return view($viewTemplate, $viewData);
    }

    public function listFrontendMember($id, Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $request->request->add(['member_eval' => $id]);
        $objSubjects = $this->_searchByConditionFrontend($request, $limit,false);
        $sumObjSubjects = $objSubjects->total();
        $current_page = $request['page'] ?? 0;
        if ($current_page >= 1){
            $current_page = ($current_page - 1) * 10;
        }
        $criteria = EvaluationCriteriaProvider::getPercentage();
        $ranks = Rank::getAllRanks();
        return view('frontend.newdesign.member_evaluation.member', [
            self::OBJ_SUBJECTS      => $objSubjects,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_SUBJECT     => $sumObjSubjects,
            self::PAGINATOR         => ($sumObjSubjects > 0) ? $objSubjects->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
            'criteria'              => $criteria,
            'ranks'                 => $ranks,
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(self::COMPANY_NAME))) {
            $company_name = $request->input(self::COMPANY_NAME);
        } else {
            $company_name = '';
        }

        if (!empty($request->input(self::TAX_CODE))) {
            $tax_code = $request->input(self::TAX_CODE);
        } else {
            $tax_code = '';
        }

        if ($request->input(self::MEMBER_NAME) != "") {
            $member_name = $request->input(self::MEMBER_NAME);
        } else {
            $member_name = '';
        }

        if ($request->input(self::TOURIRST_GUIDE_CODE) != "") {
            $tourist_guide_code = $request->input(self::TOURIRST_GUIDE_CODE);
        } else {
            $tourist_guide_code = '';
        }

        if ($request->input(self::MEMBER_CODE) != "") {
            $member_code = $request->input(self::MEMBER_CODE);
        } else {
            $member_code = '';
        }

        if ($request->input(self::TOURIST_CODE) != "") {
            $tourist_code = $request->input(self::TOURIST_CODE);
        } else {
            $tourist_code = '';
        }

        if ($request->input(self::FORTE_TOUR) != "") {
            $forte_tour = $request->input(self::FORTE_TOUR);
        } else {
            $forte_tour = '';
        }
        
        $arrConditions = [
            self::COMPANY_NAME  => $company_name,
            self::TAX_CODE   => $tax_code,
            self::MEMBER_NAME => $member_name,
            self::TOURIRST_GUIDE_CODE  => $tourist_guide_code,
            self::MEMBER_CODE   => $member_code,
            self::TOURIST_CODE => $tourist_code,
            self::FORTE_TOUR  => $forte_tour,
        ];

        return MemberEvaluationProvider::memberEvaluationSearchByConditions($arrConditions, $limit,$isCount);
    }

    private function _searchByConditionFrontend($request, $limit, $isCount)
    {
        if (!empty($request->input(CommontConstants::FROM_DATE))) {
            $fromDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::FROM_DATE, ''))->toDateString();
        } else {
            $fromDate = '';
        }

        if (!empty($request->input(CommontConstants::TO_DATE))) {
            $toDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::TO_DATE, ''))->toDateString();
        } else {
            $toDate = '';
        }

        if ($request->input(self::MEMBER_NAME) != "") {
            $member_name = $request->input(self::MEMBER_NAME);
        } else {
            $member_name = '';
        }

        if ($request->input(self::TOURIRST_GUIDE_CODE) != "") {
            $tourist_guide_code = $request->input(self::TOURIRST_GUIDE_CODE);
        } else {
            $tourist_guide_code = '';
        }

        if ($request->input(self::MEMBER_CODE) != "") {
            $member_code = $request->input(self::MEMBER_CODE);
        } else {
            $member_code = '';
        }

        if ($request->input(self::TOURIST_CODE) != "") {
            $tourist_code = $request->input(self::TOURIST_CODE);
        } else {
            $tourist_code = '';
        }

        if ($request->input(self::FORTE_TOUR) != "") {
            $forte_tour = $request->input(self::FORTE_TOUR);
        } else {
            $forte_tour = '';
        }

        if (!empty($request->input(self::COMPANY_NAME))) {
            $company_name = $request->input(self::COMPANY_NAME);
        } else {
            $company_name = '';
        }

        if (!empty($request->input(self::TAX_CODE))) {
            $tax_code = $request->input(self::TAX_CODE);
        } else {
            $tax_code = '';
        }
        $checkMember = $request->input('member_eval');
        if(isset($checkMember)) {
            $member_eval = $request->input('member_eval');
        }
        else {
            $member_eval = '';
        }
       
        $arrConditions = [
            'from_date'  => $fromDate,
            'to_date'   => $toDate,
            self::MEMBER_NAME => $member_name,
            self::TOURIRST_GUIDE_CODE  => $tourist_guide_code,
            self::MEMBER_CODE   => $member_code,
            self::TOURIST_CODE => $tourist_code,
            self::FORTE_TOUR  => $forte_tour,
            self::COMPANY_NAME  => $company_name,
            self::TAX_CODE   => $tax_code,
            'member_eval' => $member_eval
        ];

        return MemberEvaluationProvider::memberEvaluationSearchByConditions($arrConditions, $limit,$isCount);
    }

    public function show($id, $tourist_code) {
        if (empty($id) || ! auth()->check()) {
            abort(404);
        }
        $memberEvaluation = MemberEvaluationProvider::findMemberEvaluation($id, $tourist_code);

        if (empty($memberEvaluation->member_id)) {
            abort(404);
        }
        $objMember = MemberServiceProvider::getMemberById($memberEvaluation->member_id);

        $percentage = EvaluationCriteriaProvider::getPercentage();
        $travelerCompany = TravelerCompanyRankProvider::checkCode($memberEvaluation->taxcode_company);
        $criteria = EvaluationCriteriaProvider::getEvaluationCriteria();

        $provincial = $chihoi = $clbthuochoi = [];
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();

        $member_typeOfTravelGuide = MemberConstants::$member_typeOfTravelGuide;
        if ( !empty($optionTmp)){
            foreach($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if ( !empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }
        if ( !empty($clbthuochoTmp)){
            foreach($clbthuochoTmp as $key => $value){
                $clbthuochoi[$value->id] = $value->name;
            }
        }

        $viewData = [
            'memberEvaluation' => $memberEvaluation,
            'objMember' => $objMember,
            'percentage' => $percentage,
            'travelerCompany' => $travelerCompany,
            'criteria' => $criteria,
            'provincial' => $provincial,
            'branch_chihoi' => $chihoi,
            'branch_clbthuochoi' => $clbthuochoi,
            CommontConstants::TYPE_OF_PLACE => empty($objMember->typeOfPlace) ? '' : $objMember->typeOfPlace,
            CommontConstants::TYPE_OF_TRAVEL_GUIDE => empty($objMember->typeOfTravelGuide) ? '' : $objMember->typeOfTravelGuide,
            'member_typeOfTravelGuide' => $member_typeOfTravelGuide,
            CommontConstants::PROFILE_IMG => empty($objMember->profileImg) ? '' : $objMember->profileImg,
            'isProfile' => false,
        ];

        if (Auth::user()->is_traveler_company == 1) {
            return view('frontend.newdesign.member_evaluation.detail', $viewData);
        }

        return view('admin.member_evaluation.detail', $viewData);
    }

    public function rankInfo(Request $request) {
        if (! Auth::check()) {
            return redirect('/');
        }
        $attribute = $request->all();
        $memberId = $attribute['memberId'] ?? Auth::user()->memberId ?? 0;
        $member = MemberServiceProvider::getMemberById($memberId);
        if (empty($member) || ! $member->rank_status) {
            abort(404);
        }

        $provincial = $chihoi = $clbthuochoi = [];
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
        $member_typeOfTravelGuide = MemberConstants::$member_typeOfTravelGuide;

        if(! empty($optionTmp)){
            foreach ($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if(! empty($chihoiTmp)){
            foreach ($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }
        if(! empty($clbthuochoTmp)){
            foreach ($clbthuochoTmp as $key => $value){
                $clbthuochoi[$value->id] = $value->name;
            }
        }

        $member_rank = MemberRank::fetchInfoRankByMemberId($memberId);
        if (empty($member_rank)) {
            abort(404);
        }

        list($base, $sub) = Utils::getBaseAndSubjectKnowledge($member_rank->rank_id);
        $percentage = EvaluationCriteriaProvider::getPercentage();
        $criteria = EvaluationCriteriaProvider::getEvaluationCriteria();
        $companyEvaluationList = MemberEvaluation::fetchCompanyEvaluation($member_rank->rank_id, $memberId);
        $query = " 
            (
                select 
                    count(id) as total,
                    count(DISTINCT taxcode_company) as cnt,
                    ROUND(SUM(criteria1) / count(id)) as ct1,
                    ROUND(SUM(criteria2) / count(id)) as ct2,
                    ROUND(SUM(criteria3) / count(id)) as ct3,
                    ROUND(SUM(criteria4) / count(id)) as ct4,
                    ROUND(SUM(criteria5) / count(id)) as ct5,
                    criteria1_code, 
                    criteria2_code,
                    criteria3_code,
                    criteria4_code,
                    criteria5_code
                from
                    member_evaluation
                where 
                    (select count(*) from member_evaluation m1 where m1.member_id = member_evaluation.member_id and m1.rank_id <= member_evaluation.rank_id and m1.id >= member_evaluation.id order by m1.rank_id asc) <= 30
                and    
                    member_id = $memberId
                and
                    rank_id = $member_rank->rank_id
                group by criteria1_code, 
                    criteria2_code,
                    criteria3_code,
                    criteria4_code,
                    criteria5_code
            )
        ";
        $total = DB::select($query);
        return view('frontend.newdesign.survey.inforank', [
            'total' => $total, 
            'member_rank' => $member_rank,
            'percentage' => $percentage,
            'criteria' => $criteria, 
            'member' => $member, 
            'sub' => $sub, 
            'base' => $base,
            'provincial' => $provincial,
            'branch_chihoi' => $chihoi,
            'branch_clbthuochoi' => $clbthuochoi,
            CommontConstants::TYPE_OF_PLACE => empty($member->typeOfPlace) ? '' : $member->typeOfPlace,
            CommontConstants::TYPE_OF_TRAVEL_GUIDE => empty($member->typeOfTravelGuide) ? '' : $member->typeOfTravelGuide,
            'member_typeOfTravelGuide' => $member_typeOfTravelGuide,
            CommontConstants::PROFILE_IMG => empty($member->profileImg) ? '' : $member->profileImg,
            'isProfile' => false,
            'isAdmin' => isset($attribute['memberId']) ? $attribute['memberId'] : '',
            'companyEvaluationList' => $companyEvaluationList,
        ]);
    }
}
