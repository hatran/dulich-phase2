<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\NewOption;
use App\Providers\CategoryServiceProvider;
use App\Providers\LogServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\UserConstants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Option;
use App\Constants\MemberConstants;
use App\Libs\Helpers\Utils;
use App\Models\Education;
use App\Models\EducationBranch;
use App\Models\EducationDegree;
use App\Models\GroupSize;
use App\Models\Language;
use App\Models\LanguageLevel;
use App\Models\LanguageSkill;
use App\Models\Major;
use App\Models\MajorSkill;
use App\Models\TypeGuide;
use App\Models\Member;
use App\Models\LeaderSigningDecision;
use App\Models\OtherConstant;
use App\Models\WorkConstant;
use App\Models\Work;
use App\Models\Branches;
use App\Models\Element;
use App\Models\Options;
use App\Models\ForteTour;
use App\Models\MemberForteTour;
use App\Models\Rank;
use App\Models\MemberRank;
use App\Models\Information;
use App\Models\Knowledge;

class CategoryController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const SEARCH_INPUT = 'search';
    const OBJ_NEWS = 'objNews';
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';

    const TITLE = 'nameall';
    const OPTION_CODE = 'option_code';
    const CONTENT = 'content';
    const STATUS = 'status';
    const START_TIME = 'start_time';
    const END_TIME = 'end_time';
    const COUNT_NEW= 'count_news';
    const CURRENT_PAGINATOR = 'current_paginator';
    const OPTIONS = 'options';

    const CATEGORY_INDEX = 'categories.index';
    const CATEGORY_SHOW = 'categories.show';
    const SUCCESSES = 'successes';

    public function index(Request $request)
    {
        /*
        1 Danh mục chuyên ngành học vấn       EducationBranch
        2 Danh mục trình độ học vấn           EducationDegree
        3 Danh mục chuyên ngành ngoại ngữ     Language--trùng 10
        4 Danh mục trình độ học ngoại ngữ     LanguageLevel
        5 Danh mục hình thức lao động         WorkConstant::$typeOfContracts confirm--- Bang
        ---------6 Danh mục sở trường hướng dẫn        TypeGuide       ---gom 2 cai duoi
        7 Danh mục quy mô hướng dẫn           GroupSize
        8 Danh mục loại hình hướng dẫn        TypeGuide
        9 Danh mục nghiệp vụ hướng dẫn        Major
        --------10 Danh mục ngôn ngữ hướng dẫn         Language--trùng bo
        11 Danh mục số năm kinh nghiệm         OtherConstant::$experienceLevel confirm---
        12 Danh mục chức vụ                    options ???
        13 Danh mục Câu lạc bộ                 options ???
        14 Danh mục Người ký quyết định        leader_signing_decision
        6 tỉnh thành
        */
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objNews = $this->_searchByCondition($request, $limit,false);
        $sumObjNews = $this->_searchByCondition($request, $limit,true);
        $current_page = $request['page'] ?? 0;
        if ($current_page >= 1) {
            $current_page = ($current_page - 1) * 10;
        }
        $options = CategoryServiceProvider::getAllOption(CommontConstants::CATEGORY);
        foreach ($objNews as $key => $value) {
            if ($request['option_code'] == 'CATEGORY_01') $objNews[$key]['title'] = $objNews[$key]['branchName']; //EducationBranch
            if ($request['option_code'] == 'CATEGORY_02') $objNews[$key]['title'] = $objNews[$key]['degree'];    //EducationDegree
            if ($request['option_code'] == 'CATEGORY_03') $objNews[$key]['title'] = $objNews[$key]['languageName']; //Language
            if ($request['option_code'] == 'CATEGORY_04') $objNews[$key]['title'] = $objNews[$key]['levelName'];     //LanguageLevel
            if ($request['option_code'] == 'CATEGORY_05') $objNews[$key]['title'] = $objNews[$key]['workName'];   //WorkConstant
            if ($request['option_code'] == 'CATEGORY_06') $objNews[$key]['title'] = $objNews[$key]['value']; //options provincial
            if ($request['option_code'] == 'CATEGORY_07') $objNews[$key]['title'] = $objNews[$key]['groupSize']; //GroupSize
            if ($request['option_code'] == 'CATEGORY_08') $objNews[$key]['title'] = $objNews[$key]['typeName']; //TypeGuide
            if ($request['option_code'] == 'CATEGORY_09') $objNews[$key]['title'] = $objNews[$key]['majorName']; //Major
//                if($request['option_code'] == 'CATEGORY_10') $objNews[$key]['title'] = $objNews[$key]['languageName']; //Language Trung 3
            if ($request['option_code'] == 'CATEGORY_11') $objNews[$key]['title'] = $objNews[$key]['otherName']; //OtherConstant
            if ($request['option_code'] == 'CATEGORY_12') $objNews[$key]['title'] = $objNews[$key]['value']; //Option chức vụ position
            if ($request['option_code'] == 'CATEGORY_13') $objNews[$key]['title'] = $objNews[$key]['value']; //Option câu lạc bộ club
            if ($request['option_code'] == 'CATEGORY_14') $objNews[$key]['title'] = $objNews[$key]['fullname']; //LeaderSigningDecision
            if ($request['option_code'] == 'CATEGORY_15') $objNews[$key]['title'] = $objNews[$key]['name'];
            if ($request['option_code'] == 'CATEGORY_16') $objNews[$key]['title'] = $objNews[$key]['name'];
            if ($request['option_code'] == 'CATEGORY_17') $objNews[$key]['title'] = $objNews[$key]['name'];
            if ($request['option_code'] == 'CATEGORY_18') $objNews[$key]['title'] = $objNews[$key]['name'];
            if ($request['option_code'] == 'CATEGORY_19') $objNews[$key]['title'] = $objNews[$key]['name'];
            if ($request['option_code'] == 'CATEGORY_20') $objNews[$key]['title'] = $objNews[$key]['value'];
        }

        return view('admin.category.index', [
            self::OBJ_NEWS          => $objNews,
            'admin_user'            => UserConstants::ADMIN_USER,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_NEW         => count($sumObjNews),
            self::OPTIONS           => $options,
            self::PAGINATOR         => (count($sumObjNews) > 0) ? $objNews->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(self::TITLE))) {
            $title = $request->input(self::TITLE);
        } else {
            $title = '';
        }

        if (!empty($request->input(self::OPTION_CODE))) {
            $option_code = $request->input(self::OPTION_CODE);
        } else {
            $option_code = '';
        }

        $arrConditions = [
            self::TITLE         => $title,
            self::OPTION_CODE   => $option_code,
        ];

        return CategoryServiceProvider::newsSearchByConditions($arrConditions, $limit,$isCount);
    }

    public function saveCategory (Request $request)
    {
        if(!empty($request->input('id_edit'))){
            $checkData = CategoryServiceProvider::getDataById($request->input('id_edit'), $request->input('option_edit'));
            if(empty($checkData->id)){
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $request->input('option_edit')])->withErrors(['Danh mục không tồn tại hoặc đã xóa khỏi hệ thống']);
            }
        }

        $objData = [];
        //add danh muc
        if(empty($request->input('id_edit'))){
            $option = $request->input('optionAll_add');
            $nameAll = $request->input('nameAll_add');
            $codeAll = $request->input('code_add');
        }else{
            $id = $request->input('id_edit');
            $option = $request->input('option_edit');
            $nameAll = $request->input('nameAll_edit');
            $codeAll = $request->input('code_edit');
        }
        if($option == 'CATEGORY_01') {
            $objData['branchName']          = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = EducationBranch::where('id', $id)->update($objData);
            }else {
                $objSave = EducationBranch::create($objData);
            }
        } //EducationBranch
        if($option == 'CATEGORY_02') {
            $objData['degree']             = $nameAll;
            $objData['status']             = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = EducationDegree::where('id', $id)->update($objData);
            }else {
                $objSave = EducationDegree::create($objData);
            }
        }    //EducationDegree
        if($option == 'CATEGORY_03') {
            $objData['languageName']        = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Language::where('id', $id)->update($objData);
            }else {
                $objSave = Language::create($objData);
            }
        } //Language
        if($option == 'CATEGORY_04') {
            $objData['levelName']           = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = LanguageLevel::where('id', $id)->update($objData);
            }else {
                $objSave = LanguageLevel::create($objData);
            }
        }     //LanguageLevel
        if($option == 'CATEGORY_05') {
            $objData['workName']            = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = WorkConstant::where('id', $id)->update($objData);
            }else {
                $objSave = WorkConstant::create($objData);
            }
        }   //WorkConstant
//                if($option == 'CATEGORY_06') $objData['title']        = $objData['typeName']; //gom 7-8
        if($option == 'CATEGORY_07') {
            $objData['groupSize']           = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = GroupSize::where('id', $id)->update($objData);
            }else {
                $objSave = GroupSize::create($objData);
            }
        } //GroupSize
        if($option == 'CATEGORY_08') {
            $objData['typeName']            = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = TypeGuide::where('id', $id)->update($objData);
            }else {
                $objSave = TypeGuide::create($objData);
            }
        } //TypeGuide
        if($option == 'CATEGORY_09') {
            $objData['majorName']           = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Major::where('id', $id)->update($objData);
            }else {
                $objSave = Major::create($objData);
            }
        } //Major
//                if($option == 'CATEGORY_10') $objData['title']        = $objData['languageName']; //Language Trung 3
        if($option == 'CATEGORY_11') {
            $objData['otherName']           = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = OtherConstant::where('id', $id)->update($objData);
            }else {
                $objSave = OtherConstant::create($objData);
            }
        } //OtherConstant
        if($option == 'CATEGORY_06') {
            $objData['value']               = $nameAll;
            $objData['key']                 = 'provincial';
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Option::where('id', $id)->update($objData);
            }else {
                $objData['status']           = 1;
                $objSave = Option::create($objData);
            }
        } //Option chức vụ position
        if($option == 'CATEGORY_12') {
            $objData['value']               = $nameAll;
            $objData['key']                 = 'position';
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Option::where('id', $id)->update($objData);
            }else {
                $objData['status']           = 1;
                $objSave = Option::create($objData);
            }
        } //Option chức vụ position
        if($option == 'CATEGORY_13') {
            $objData['value']               = $nameAll;
            $objData['key']                 = 'club';
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Option::where('id', $id)->update($objData);
            }else {
                $objData['status']          = 1;
                $objSave = Option::create($objData);
            }
        } //Option câu lạc bộ club
        if($option == 'CATEGORY_14') {
            $objData['fullname']            = $nameAll;
            $objData['status']              = 1;
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = LeaderSigningDecision::where('id', $id)->update($objData);
            }else {
                $objSave = LeaderSigningDecision::create($objData);
            }
        } //LeaderSigningDecision
        if($option == 'CATEGORY_15') {
            $objData['name']           = $nameAll;
            $objData['status']         = 1;
            $objData['code']           = $codeAll;
            $objData['type']           = 1;
            if(!empty($request->input('id_edit'))){
                $objSave = ForteTour::where('id', $id)->update($objData);
            }else {
                $objSave = ForteTour::create($objData);
            }
        } 
        if($option == 'CATEGORY_16') {
            $objData['name']           = $nameAll;
            $objData['status']         = 1;
            $objData['code']           = $codeAll;
            $objData['type']           = 2;
            if(!empty($request->input('id_edit'))){
                $objSave = ForteTour::where('id', $id)->update($objData);
            }else {
                $objSave = ForteTour::create($objData);
            }
        } 
        if($option == 'CATEGORY_17') {
            $objData['name']           = $nameAll;
            $objData['status']         = 1;
            $objData['code']           = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Rank::where('id', $id)->update($objData);
            }else {
                $objSave = Rank::create($objData);
            }
        } 
        if($option == 'CATEGORY_18') {
            $objData['name']           = $nameAll;
            $objData['status']         = 1;
            $objData['code']           = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Information::where('id', $id)->update($objData);
            }else {
                $objSave = Information::create($objData);
            }
        } 
        if($option == 'CATEGORY_19') {
            $objData['name']           = $nameAll;
            $objData['status']         = 1;
            $objData['code']           = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Knowledge::where('id', $id)->update($objData);
            }else {
                $objSave = Knowledge::create($objData);
            }
        } 
        if($option == 'CATEGORY_20') {
            $objData['value']               = $nameAll;
            $objData['key']                 = 'operatingArea';
            $objData['code']                = $codeAll;
            if(!empty($request->input('id_edit'))){
                $objSave = Option::where('id', $id)->update($objData);
            }else {
                $objData['status']           = 1;
                $objSave = Option::create($objData);
            }
        } //Option chức vụ position

        if (empty($objSave)) {
            if(!empty($request->input('id_edit'))){
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Cập nhật danh mục thất bại']);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Thêm mới danh mục thất bại']);
            }
        }
        $objOptions = CategoryServiceProvider::getAllOption(CommontConstants::CATEGORY);
        if(!empty($request->input('id_edit'))){
            return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->with($this::SUCCESSES, ['Cập nhật danh mục thành công']);
        }else{
            return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->with($this::SUCCESSES, ['Thêm mới danh mục thành công']);
        }

    }

    public function showEdit ($id, $option, Request $request)
    {
        if (empty($id) || empty($option)) {
            $objData = null;
        } else {
            $objData = CategoryServiceProvider::getDataById($id, $option);
        }

        if (empty($objData)) {
            $request->session()->flash('error_ajax', ['Danh mục không tồn tại hoặc đã xóa khỏi hệ thống']);
            return response()->json(['deleted' => 'error']);
        }
        if($option == 'CATEGORY_01') $objData['title'] = $objData['branchName']; //EducationBranch
        if($option == 'CATEGORY_02') $objData['title'] = $objData['degree'];    //EducationDegree
        if($option == 'CATEGORY_03') $objData['title'] = $objData['languageName']; //Language
        if($option == 'CATEGORY_04') $objData['title'] = $objData['levelName'];     //LanguageLevel
        if($option == 'CATEGORY_05') $objData['title'] = $objData['workName'];   //WorkConstant
        if($option == 'CATEGORY_06') $objData['title'] = $objData['value']; //
        if($option == 'CATEGORY_07') $objData['title'] = $objData['groupSize']; //GroupSize
        if($option == 'CATEGORY_08') $objData['title'] = $objData['typeName']; //TypeGuide
        if($option == 'CATEGORY_09') $objData['title'] = $objData['majorName']; //Major
//                if($option == 'CATEGORY_10') $objData['title'] = $objData['languageName']; //Language Trung 3
        if($option == 'CATEGORY_11') $objData['title'] = $objData['otherName']; //OtherConstant
        if($option == 'CATEGORY_12') $objData['title'] = $objData['value']; //Option chức vụ position
        if($option == 'CATEGORY_13') $objData['title'] = $objData['value']; //Option câu lạc bộ club
        if($option == 'CATEGORY_14') $objData['title'] = $objData['fullname']; //LeaderSigningDecision
        if($option == 'CATEGORY_15') $objData['title'] = $objData['name'];
        if($option == 'CATEGORY_16') $objData['title'] = $objData['name'];
        if($option == 'CATEGORY_17') $objData['title'] = $objData['name'];
        if($option == 'CATEGORY_18') $objData['title'] = $objData['name'];
        if($option == 'CATEGORY_19') $objData['title'] = $objData['name'];
        if($option == 'CATEGORY_20') $objData['title'] = $objData['value'];
        $objOptions = CategoryServiceProvider::getAllOption(CommontConstants::CATEGORY);
        return response()->json(['objData' => $objData, 'objOptions' => $objOptions]);
    }
    public function checkCode ($id, $option, $code, Request $request)
    {
        $result['error'] = '';
        $objData = [];
        if (empty($id) || empty($option) || empty($code)) {
            $objData = null;
        } else {
            if ($id != 'new') {
                $objData = CategoryServiceProvider::getDataById($id, $option);
            }
        }
        if (empty($objData) && $id != 'new') {
            $request->session()->flash('error_ajax', ['Danh mục không tồn tại hoặc đã xóa khỏi hệ thống']);
            return response()->json(['deleted' => 'error']);
        }else{
            $objCode = CategoryServiceProvider::getDataByCode($id, $option, $code);
            if(!empty($objCode)){
                return response()->json(['error' => 'error']);
            }
        }

        return response()->json(['error' => '']);
    }

    public function deleteOption ($id, $option)
    {

        $objIntro = CategoryServiceProvider::getDataById($id, $option);
        if (empty($objIntro)) {
            return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục không tồn tại hoặc đã xóa khỏi hệ thống']);
        }

        $objData['status'] = 0;
        if($option == 'CATEGORY_01') {
            $objEducation = Education::where('branchId', $id)->first();
            if(empty($objEducation)){
                $objSave = EducationBranch::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        } //EducationBranch
        if($option == 'CATEGORY_02') {
            $objEducation = Education::where('degreeId', $id)->first();
            if(empty($objEducation)){
                $objSave = EducationDegree::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        }    //EducationDegree
        if($option == 'CATEGORY_03') {
            $objLanguageSkill = LanguageSkill::where('languageId', $id)->first();
            if(empty($objLanguageSkill)) {
                $objSave = Language::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        } //Language
        if($option == 'CATEGORY_04') {
            $objLanguageSkill = LanguageSkill::where('levelId', $id)->first();
            if(empty($objLanguageSkill)) {
                $objSave = LanguageLevel::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        }     //LanguageLevel
        if($option == 'CATEGORY_05') {
            $objtypeOfContract = Work::where('typeOfContract', $id)->first();
            if(empty($objtypeOfContract)) {
                $objSave = WorkConstant::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        }   //WorkConstant
        if($option == 'CATEGORY_06') {
            $objProvincial = Member::where('province', $id)->first();
            if(empty($objProvincial)) {
                $objSave = Options::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        }
        //$objData['title']        = $objData['typeName']; //gom 7-8
        if($option == 'CATEGORY_07') {
            $objElement = Element::where('groupSizeId', $id)->first();
            if(empty($objElement)) {
                $objSave = GroupSize::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        } //GroupSize
        if($option == 'CATEGORY_08') {
            $objElement = Element::where('typeGuideId', $id)->first();
            if(empty($objElement)) {
                $objSave = TypeGuide::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        } //TypeGuide
        if($option == 'CATEGORY_09') {
            $objMajorSkill = MajorSkill::where('majorId', $id)->first();
            if(empty($objMajorSkill)) {
                $objSave = Major::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        } //Major
//      if($option == 'CATEGORY_10') $objData['title']        = $objData['languageName']; //Language Trung 3
        if($option == 'CATEGORY_11') {
            $objexperienceLevel = Member::where('experienceLevel', $id)->first();
            if(empty($objexperienceLevel)) {
                $objSave = OtherConstant::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        } //OtherConstant
        if($option == 'CATEGORY_12') {
            $objSave = Option::where('id', $id)->update($objData);
        } //Option chức vụ position
        if($option == 'CATEGORY_13') {
            $objSave = Option::where('id', $id)->update($objData);
        } //Option câu lạc bộ club
        if($option == 'CATEGORY_14') {
            $objSave = LeaderSigningDecision::where('id', $id)->update($objData);
        } //LeaderSigningDecision
        
        if($option == 'CATEGORY_15' || $option == 'CATEGORY_16') {
            $objMemberForteTour = Member::where('guideLanguage', 'LIKE', '%'.$id.'%')->first();
            if(empty($objMemberForteTour)) {
                $objSave = ForteTour::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        }
        if($option == 'CATEGORY_17') {
            $objMemberRank = MemberRank::where('rank_id', $id)->where('is_deleted', 0)->first();
            if(empty($objMemberRank)) {
                $objSave = Rank::where('id', $id)->update($objData);
            }else{
                return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Danh mục đang được sử dụng, ko được xóa']);
            }
        }

        if($option == 'CATEGORY_18') {
            $objSave = Information::where('id', $id)->update($objData);
        }

        if($option == 'CATEGORY_19') {
            $objSave = Knowledge::where('id', $id)->update($objData);
        }

        if($option == 'CATEGORY_20') {
            $objSave = Options::where('id', $id)->update($objData);
        }

        if(empty($objSave)){
            return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->withErrors(['Xóa danh mục thất bại']);
        } else {
            return redirect()->route($this::CATEGORY_INDEX, ['option_code' => $option])->with($this::SUCCESSES, ['Xóa danh mục thành công']);
        }
    }

    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }
}
