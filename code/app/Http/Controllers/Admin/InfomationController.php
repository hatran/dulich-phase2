<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libs\Helpers\LogsHelper;
use App\Providers\LogServiceProvider;
use App\Repositories\News\NewsAdmin;
use App\Exceptions\Validation\ValidationException;
use Input;
use Illuminate\Support\Facades\Route;

class InfomationController extends Controller {

    protected $news = null;

    public function __construct() {
        $this->news = new NewsAdmin();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        // list and search
        $page = $request->get('page', 1);
        $listInfomationParent = $this->news->infomationCodeParent;
        $listInfomation = $this->news->infomationCode;
        $listData = $this->news->pagination($request->all());

        return view('admin.infomation.index', [
            'listData' => $listData,
            'listInfomation' => $listInfomation,
            'listInfomationParent' => $listInfomationParent,
            'statusOption' => NewsAdmin::$statusOption,
            'offsets' => ($page - 1) * $this->news->perPage,
            'paginator' => $listData->count() ? $listData->appends($request->all()) : null
        ]);
    }

    /**
     * Show the form for creating a branches resource.
     *
     * @return Response
     */
    public function create(Request $request) {
        $listInfomation = $this->news->infomationCode;
        $listInfomationParent = $this->news->infomationCodeParent;
        $statusDefault = NewsAdmin::STATUS_PUBLIC;
        if (old('status') != '') {
            $statusDefault = old('status') == NewsAdmin::STATUS_PUBLIC ? NewsAdmin::STATUS_PUBLIC : NewsAdmin::STATUS_DRAFT;
        }
        return view('admin.infomation.create', ['listInfomation' => $listInfomation, 'listInfomationParent' => $listInfomationParent, 'statusDefault' => $statusDefault]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        try {
            $request->merge(array_map('trim', $request->all()));
            $this->news->create($request->all());
            return redirect()->route('infomation.index')->with('successes', ['Thêm mới thông tin thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('infomation.create')->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            abort(404);
        }
        $optionCodeParent = $this->news->getOptionInfomationParentByID($detail->option_code);
        // if (empty($optionCodeParent)) {
        //     abort(404);
        // }
        $listInfomation = $this->news->infomationCode;
        $listInfomationParent = $this->news->infomationCodeParent;
        return view('admin.infomation.edit', [
            'detail' => $detail,
            'listInfomation' => $listInfomation,
            'listInfomationParent' => $listInfomationParent,
            'optionCodeParent' => $optionCodeParent
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->news->find($id);
        if (empty($detail)) {
            abort(404);
        }
        $listInfomation = $this->news->infomationCode;

        return view('admin.infomation.show', [
            'detail' => $detail,
            'listInfomation' => $listInfomation
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        if (empty($id)) {
            abort(404);
        }
        try {
            $this->news->updateData($id, $request->all());
            return redirect()->route('infomation.index')->with('successes', ['Sửa thông tin thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('infomation.update', ['id' => $id])->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (empty($id)) {
            abort(404);
        }
        try {
            $model = $this->news->find($id);
            if (!empty($model)) {
                $model->delete();
                // LogServiceProvider::createSystemHistory(trans('history.fn_delete_information'), [
                //     'information' => $model
                // ]);
                return redirect()->route('infomation.index')->with('successes', ['Xóa thông tin thành công']);
            }
            return redirect()->route('infomation.index')->withErrors('Thông tin không tồn tại hoặc đã xóa');
        } catch (Exception $ex) {
            return redirect()->route('infomation.index')->withErrors('Xóa thất bại');
        }
    }

    public function ajaxAddSubInfo(Request $request) {
        $attribute = $request->all();
        $id = $attribute['id'];
        $subInfo = $this->news->getOptionInfomationByParentID($id);
        $data = [
            'data' => $subInfo,
            'status' => 'OK',
        ];
        return $data;
    }
}
