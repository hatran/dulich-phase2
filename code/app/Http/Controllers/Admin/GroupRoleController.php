<?php

namespace App\Http\Controllers\Admin;

use App\Models\Groups;
use App\Models\Permission;
use App\Repositories\Users\GroupsAdmin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exceptions\Validation\ValidationException;
use Input;
use App\Repositories\Users\GroupsRolesAdmin;
use App\Providers\LogServiceProvider;

class GroupRoleController {

    protected $groups = null;
    protected $listTree = [];

    public function __construct() {
        $this->groups = new GroupsRolesAdmin();
    }

    public function index(Request $request) {
        $groups = $this->groups->pagination($request->all());
        $page = $request->get('page', 1);

        return view('admin.group_role.index', [
            'groups' => $groups,
            'offsets' => ($page - 1) * $this->groups->perPage,
            'paginator' => $groups->count() ? $groups->appends($request->all()) : null
        ]);
    }

    public function create() {
        $groupsAdmin = new GroupsAdmin();
        $listPermission = $groupsAdmin->getListAllPermissions();
        $this->showTree($listPermission);
        $oldPermission = old('permissions');

        return view('admin.group_role.create', [
            'listTree' => $this->listTree,
            'oldPermission' => $oldPermission,
        ]);
    }

    /**
     * Show the form for add the specified resource.
     *
     * @return Response
     */
    public function store(Request $request) {
        $request->merge(['code' => uniqid(6)]);
        try {
            $this->groups->create($request->all());
            return redirect()->route('group_role.index')->with('successes', ['Thêm mới thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('group_role.create')->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->groups->find($id);
        if (empty($detail)) {
            return redirect()->route('groups.index')->withErrors(['Quyền không tồn tại hoặc bị xóa']);
        }
        $groupsAdmin = new GroupsAdmin();
        $listPermission = $groupsAdmin->getListAllPermissions();
        $this->showTree($listPermission);
        $oldPermission = !empty(old('permissions')) ? old('permissions') : json_decode($detail->permissions);

        return view('admin.group_role.edit', [
            'detail' => $detail,
            'listTree' => $this->listTree,
            'oldPermission' => $oldPermission,
        ]);
    }

    public function updated($id, Request $request) {
        try {
            $this->groups->updateData($id, $request->all());
            return redirect()->route('group_role.index')->with('successes', ['Sửa quyền thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('group_role.edit', ['id' => $id])->withInput()->withErrors($e->getErrors());
        }
    }

    public function delete($id) {
        if (empty($id)) {
            abort(404);
        }
        try {
            $detail = $this->groups->find($id);
            $group = Groups::where('group_role_id', 'like', '%"' . $id . '"%')->get();
            if ($group->count()) {
                return redirect()->route('group_role.index')->withErrors(['Quyền đã được sử dụng không được xóa']);
            }
            if (empty($detail)) {
                return redirect()->route('group_role.index')->withErrors(['Quyền không tồn tại hoặc bị xóa']);
            }
            if ($detail->delete()) {
                return redirect()->route('group_role.index')->with('successes', ['Xóa quyền thành công']);
            }
        } catch (ValidationException $e) {
            return redirect()->route('group_role.index')->withErrors(['Quyền không tồn tại hoặc bị xóa']);
        }
    }

    public function genRouterHistory() {
        $app = app();
        $routes = $app->routes->getRoutes();

        foreach ($routes as $route) {
            if (!empty($route->getName()) && !str_is('front/*', $route->getName())) {
                $arrUpdateData = [
                    'route_name' => $route->getName(),
                    'name' => $route->getName(),
                    'description' => $route->uri . ' ' . $route->getName() . ' ' . $route->getPrefix() . ' ' . $route->getActionMethod(),
                ];

                if ($route->getPrefix() == 'officesys' || $route->getPrefix() == 'member') {
                    Permission::firstOrCreate(
                            ['route_name' => $route->getName()], $arrUpdateData
                    );
                }
            }
        }
    }

    public function showTree($list, $parent_id = 0)
    {
        foreach ($list as $key => $item)
        {
            if ($item->parent == $parent_id)
            {
                $this->listTree[$parent_id][$item->id] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'parent' => $parent_id,
                    'route_name' => $item->route_name
                ];
                unset($list[$key]);
                $this->showTree($list, $item->id);
            }
        }
        return true;
    }
}
