<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Pagination;
use App\Constants\MemberConstants;
use App\Repositories\ClubOfHead\ClubOfHeadInterface;
use App\Exceptions\Validation\ValidationException;
use App\Repositories\Employees\EmployeesRepository;
use App\Libs\Helpers\LogsHelper;
use App\Providers\LogServiceProvider;
use Maatwebsite\Excel\Facades\Excel as Excel;
use Input;
use App\Libs\Helpers\Utils;

class ClubOfHeadController extends Controller {

    const PAGINATOR = 'paginator';
    const RESPONSE_VIEW = 'response_view';
    const POSITION_KEY = 'position';
    const STATUS_PUBLIC = 1;
    const STATUS_DRAFT = 0;

    protected $repository;

    public static $statusOption = [
        self::STATUS_PUBLIC => 'Hoạt động',
        self::STATUS_DRAFT => 'Không hoạt động'
    ];

    public function __construct(ClubOfHeadInterface $branches) {
        $this->repository = $branches;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        // list and search
        $page = $request->get('page', 1);
        $pagiData = $this->repository->paginate($page, $this->repository->perPage, $request->all());
        $branches = Pagination::makeLengthAware($pagiData->items, $pagiData->totalItems, $this->repository->perPage);

        $listCLB = $this->repository->getOptionsClub();

        return view('admin.club_of_head.index', [
            'branches' => $branches,
            'offsets' => ($page - 1) * $this->repository->perPage,
            'listCLB' => $listCLB,
            'totalItems' => $pagiData->totalItems,
            'statusOption' => self::$statusOption,
            self::RESPONSE_VIEW => $request->all(),
            self::PAGINATOR => $branches->count() ? $branches->appends($request->all()) : null
        ]);
    }

    /**
     * Show the form for creating a branches resource.
     *
     * @return Response
     */
    public function create() {
        $listCLB = $this->repository->getOptionsClub(true);

        return view('admin.club_of_head.create', ['listCLB' => $listCLB]);
    }

    public function ajaxGetCLB(Request $request) {
        $search = $request->get('search', '');
        $unuse = $request->get('unuse', false);
        return $this->repository->getOptionsClub($unuse, $search);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        try {
            $this->repository->create($request->all());
            //LogServiceProvider::createSystemHistory(trans('history.fn_create_clb_of_head'), $request->all());
            return response()->json(['savedData' => true, 'message' => 'Thành công', 'type' => 'success']);
        } catch (ValidationException $e) {
            return response()->json(['savedData' => false, 'errorMessages' => $e->getErrors()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->repository->find($id);
        if (empty($detail)) {
            abort(404);
        }

        $listCLB = $this->repository->getOptionsClub();

        return view('admin.club_of_head.edit', ['detail' => $detail, 'listCLB' => $listCLB]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->repository->find($id);
        if (empty($detail)) {
            abort(404);
        }

        return view('admin.club_of_head.show', [
            'detail' => $detail,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (empty($id)) {
            abort(404);
        }
        try {
            $this->repository->update($id, $request->all());
            //LogServiceProvider::createSystemHistory(trans('history.fn_save_clb_of_head'), $request->all());
            return response()->json(['savedData' => true, 'message' => 'Thành công', 'type' => 'success']);
        } catch (ValidationException $e) {
            return response()->json(['savedData' => false, 'errorMessages' => $e->getErrors()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (empty($id)) {
            abort(404);
        }

        $deleted = $this->repository->delete($id);
        if ($deleted['deleted']) {
            //LogServiceProvider::createSystemHistory(trans('history.fn_delete_clb_of_head'), $deleted);
            return redirect()->route('club_of_head_index')->with('successes', [$deleted['messages']]);
        }

        return redirect()->route('club_of_head_index')->withErrors($deleted['messages']);
    }

    public function branchManager($id, Request $request) {
        if (empty($id)) {
            abort(404);
        }
        $detailBranch = $this->repository->find($id);
        $status = true;
        if (empty($detailBranch) || $detailBranch->status != \App\Models\Branches::BRANCHES_ACTIVE) {
            $status = false;
        }
        if (!$status && $request->ajax()){
            return response()->json(['status' => $status]);
        } elseif (!$status && !$request->ajax()) {
            abort(404);
        }
        $listManager = $this->repository->getEmployeesPositionByBranchId($id);
        $listOption = $this->repository->getOptionsByKey(self::POSITION_KEY);
        return view('admin.club_of_head.manager', [
            'detailBranch' => $detailBranch,
            'listManager' => $listManager,
            'listOption' => $listOption
        ]);
    }

    public function ajaxFindMember(Request $request, EmployeesRepository $employees) {
        $employees = $employees->getListEmployeesByOptionId(1, $request->get('query', ''));
        return response()->json($employees);
    }

    public function ajaxSaveMember(Request $request) {
        try {
            $saved = $this->repository->saveEmployeesPosition($request->all());
            if (!empty($saved)) {
                //LogServiceProvider::createSystemHistory(trans('history.fn_save_lead_clb_of_head'), $request->all());
                return response()->json($saved);
            }

            //return response()->json($saved);
        } catch (\Exception $e) {
            LogsHelper::trackByFile('ajaxSaveMemberFail', 'Error when update member payment(Exception: ' . $e->getMessage() . ' on Line ' . $e->getLine() . ' in File ' . $e->getFile() . ')');
        }
    }

    public function exportExcel($id) {
        if (empty($id)) {
            abort(404);
        }
        $detailBranch = $this->repository->find($id);
        if (empty($detailBranch)) {
            return response()->json(['error' => 'CLB không tồn tại hoặc bị xóa.']);
        }
        $listManager = $this->repository->getEmployeesPositionByBranchId($id);
        $clause = [
                ['status', MemberConstants::MEMBER_OFFICIAL_MEMBER],
                ['is_delete', null],
                ['typeOfPlace', \App\Constants\BranchConstants::TYPE_OF_PLACE_CLB_THUOC_HOI],
                ['province_code', $id]
        ];
        $listMember = $this->repository->getMemberByCondition($clause);
        $listOption = $this->repository->getOptionsByKey(self::POSITION_KEY);
        $listLanguage = \App\Models\Language::getAllLanguages();

        $fileName = 'Danh_sach_hoi_vien_chi_hoi_ [' . str_replace(' ', '_', Utils::stripUnicode($detailBranch->name)) . ']' . date('d/m/Y');

        Excel::create($fileName, function($excel) use ($detailBranch, $listManager, $listMember, $listOption, $listLanguage) {
            $excel->setTitle('HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM');
            $excel->sheet('First sheet', function($sheet) use ($detailBranch, $listManager, $listMember, $listOption, $listLanguage) {
                $sheet->loadView('admin.club_of_head.export_excel', [
                    'listManager' => $listManager,
                    'listMember' => $listMember,
                    'detailBranch' => $detailBranch,
                    'listOption' => $listOption,
                    'listLanguage' => $listLanguage
                ]);
            });
        })->download('xlsx');
    }

}
