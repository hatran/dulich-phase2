<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exceptions\Validation\ValidationException;
use Input;
use App\Repositories\Users\GroupsAdmin;
use App\Providers\LogServiceProvider;
use App\Libs\Helpers\Utils;

class GroupsController {

    protected $groups = null;
    protected $listTree = [];

    public function __construct() {
        $this->groups = new GroupsAdmin();
    }

    public function index(Request $request) {
        $groups = $this->groups->pagination($request->all());
        $page = $request->get('page', 1);
        $statusList = GroupsAdmin::$statusLabel;

        return view('admin.groups.index', [
            'groups' => $groups,
            'offsets' => ($page - 1) * $this->groups->perPage,
            'statusList' => $statusList,
            'paginator' => $groups->count() ? $groups->appends($request->all()) : null
        ]);
    }

    public function create() {
        $listGroupRole = $this->groups->getListGroupRole();
        $statusDefault = GroupsAdmin::STATUS_ENABLE;
        if (old('status') != '') {
            $statusDefault = old('status') == GroupsAdmin::STATUS_ENABLE ? GroupsAdmin::STATUS_ENABLE : GroupsAdmin::STATUS_DISABLE;
        }
        $oldGroupRole  = old('group_role_id');
        return view('admin.groups.create', [
            'listGroupRole' => $listGroupRole,
            'oldGroupRole' => $oldGroupRole,
            'statusDefault' => $statusDefault,
        ]);
    }

    public function showTree($list, $parent_id = 0)
    {
        foreach ($list as $key => $item)
        {
            if ($item->parent == $parent_id)
            {
                $this->listTree[$parent_id][$item->id] = [
                    'id' => $item->id,
                    'name' => $item->name,
                    'parent' => $parent_id,
                    'route_name' => $item->route_name
                ];
                unset($list[$key]);
                $this->showTree($list, $item->id);
            }
        }
        return true;
    }

    /**
     * Show the form for add the specified resource.
     *
     * @return Response
     */
    public function store(Request $request) {
        $request->merge(['code' => Utils::randomString(6, false)]);
        try {
            $this->groups->create($request->all());
            return redirect()->route('groups.index')->with('successes', ['Thêm mới thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('groups.create')->withInput()->withErrors($e->getErrors());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->groups->find($id);
        if (empty($detail)) {
            return redirect()->route('groups.index')->withErrors(['Vai trò không tồn tại hoặc bị xóa']);
        }
        $listGroupRole = $this->groups->getListGroupRole();
        $oldGroupRole = !empty(old('group_role_id')) ? old('group_role_id') : json_decode($detail->group_role_id);

        return view('admin.groups.edit', [
            'listGroupRole' => $listGroupRole,
            'oldGroupRole' => $oldGroupRole,
            'detail' => $detail,
        ]);
    }

    public function show($id) {
        if (empty($id)) {
            abort(404);
        }
        $detail = $this->groups->find($id);
        if (empty($detail)) {
            return redirect()->route('groups.index')->withErrors(['Vai trò không tồn tại hoặc bị xóa']);
        }
        $listGroupRole = $this->groups->getListGroupRole();
        $oldGroupRole = !empty(old('group_role_id')) ? old('group_role_id') : json_decode($detail->group_role_id);

        return view('admin.groups.show', [
            'listGroupRole' => $listGroupRole,
            'oldGroupRole' => $oldGroupRole,
            'detail' => $detail,
        ]);
    }

    public function updated($id, Request $request) {
        try {
            $this->groups->updateData($id, $request->all());
            return redirect()->route('groups.edit', ['id' => $id])->with('successes', ['Sửa vai trò thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('groups.edit', ['id' => $id])->withInput()->withErrors($e->getErrors());
        }
    }

    public function delete($id) {
        if (empty($id)) {
            abort(404);
        }
        try {
            $detail = $this->groups->find($id);
            $userGr = \App\Models\User::where('role', $detail->code)->get();
            if ($userGr->count()) {
                return redirect()->route('groups.index')->withErrors(['Vai trò có tài khoản không được xóa']);
            }
            if (empty($detail)) {
                return redirect()->route('groups.index')->withErrors(['Vai trò không tồn tại hoặc bị xóa']);
            }
            if ($detail->delete()) {
                return redirect()->route('groups.index')->with('successes', ['Xóa vai trò thành công']);
            }
        } catch (ValidationException $e) {
            return redirect()->route('groups.index')->withErrors(['Vai trò không tồn tại hoặc bị xóa']);
        }
    }

    public function genRouterHistory() {
        $app = app();
        $routes = $app->routes->getRoutes();

        foreach ($routes as $route) {
            if (!empty($route->getName()) && !str_is('front/*', $route->getName())) {
                $arrUpdateData = [
                    'route_name' => $route->getName(),
                    'name' => $route->getName(),
                    'description' => $route->uri . ' ' . $route->getName() . ' ' . $route->getPrefix() . ' ' . $route->getActionMethod(),
                ];

                if ($route->getPrefix() == 'officesys' || $route->getPrefix() == 'member') {
                    Permission::firstOrCreate(
                            ['route_name' => $route->getName()], $arrUpdateData
                    );
                }
            }
        }
    }

}
