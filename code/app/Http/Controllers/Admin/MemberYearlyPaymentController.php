<?php

namespace App\Http\Controllers\Admin;

use App\Constants\PaymentConstants;
use App\Http\Requests\ImportExcelRequest;
use App\Jobs\SendEmail;
use App\Jobs\SendSms;
use App\Libs\Helpers\LogsHelper;
use App\Libs\Mail\MailHelper;
use App\Providers\LogServiceProvider;
use App\Providers\MemberPaymentsYearlyServiceProvider;
use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory as ReaderFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Constants\MemberConstants;
use App\Http\Controllers\Controller;
use App\Providers\MemberServiceProvider;
use App\Providers\NoteServiceProvider;
use App\Libs\Helpers\Utils;
use Validator;
use Response;
use Carbon\Carbon;
use App\Constants\CommontConstants;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Models\Offices;
use App\Models\MemberPaymentsYearly;
use App\Models\MemberPayments;
use App\Models\Emails;
use App\Models\Language;
use App\Http\Requests\MemberYearlyPayment;
use App\Models\FeePolicyRepository;

class MemberYearlyPaymentController extends Controller
{
    const NUMBER_PAYMENT = 'number_payment';

    const NUMBER_PAYMENT_MAX_LENGTH  = 20;
    const PAYMENT_CONTENT_MAX_LENGTH = 200;
    const PAYMENT_NOTE_MAX_LENGTH    = 200;
    const PAYMENT_NUMBER_MAX_LENGTH  = 10;
    const FROM_YEAR = 'from-year';
    const TO_YEAR = 'to-year';
    const STATUS_DUES = [
        1 => '',
        2 => 'Chưa Nộp Hội Phí',
        3 => 'Đã Nộp Hội Phí'
    ];

    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const STATUS = 'status';
    const PAYMENT_YEARLY_POINT = '2019-08-12 00:00:00';
    const TOANQUOC = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->input(CommontConstants::STATUS, '');
        $fromYearSearch = $request->input(self::FROM_YEAR, '');
        $toYearSearch = $request->input(self::TO_YEAR, '');
        $year = Utils::getYears();
        $request->merge([CommontConstants::STATUS => $status]);

       if($status && !in_array($status, array_keys(self::STATUS_DUES))) {
           abort(404);
       }

        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $searchCommon = [
            'step' => 0,
            'arrFile' => self::STATUS_DUES
        ];

        $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
        $currentYear = date('Y');
        $objMembers = $this->_searchByCondition($request, $limit);

        return view('admin.payment_yearly.index', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            CommontConstants::ALL_SEARCH => $this->searchByAllArea(),
            CommontConstants::COUNT_MEMBER => $objMembers->total(),
            CommontConstants::PAGINATOR => !empty($objMembers) ? $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $this->getCurrentCursor($limit, $request),
            'offices' => Offices::searchByAllOffices(),
            'year' => $year,
            'fromYearSearch' => $fromYearSearch,
            'toYearSearch' => $toYearSearch,
            'query_string' => $query_string,
            'status' => $status,
            'currentYear' => $currentYear
        ]);
    }


    private function _searchByCondition ($request, $limit)
    {
        $arrConditions = [
            self::NUMBER_PAYMENT                    => $request->input(self::NUMBER_PAYMENT, ''),
            CommontConstants::FULL_NAME             => $request->input(CommontConstants::FULL_NAME),
            CommontConstants::TOURIST_GUIDE_CODE    => $request->input(CommontConstants::TOURIST_GUIDE_CODE),
            CommontConstants::MEMBER_CODE           => $request->input(CommontConstants::MEMBER_CODE),
            CommontConstants::STATUS                => $request->input(CommontConstants::STATUS),
            CommontConstants::PROVINCE_CODE         => $request->input(CommontConstants::PROVINCE_CODE),
            self::FROM_YEAR                         => $request->input(self::FROM_YEAR, ''),
            self::TO_YEAR                           => $request->input(self::TO_YEAR, ''),
            CommontConstants::FILE_CODE             => $request->input(CommontConstants::FILE_CODE)
        ];

        return MemberPaymentsYearlyServiceProvider::searchMemberByConditionsPayment($arrConditions, $limit);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, Request $request)
    {
        if ($request->session()->has('number_payment')) {
            $request->session()->forget('number_payment');
        }
        if (empty($id)) {
            $objMember = null;
            $objMemberPayment =  null;
            $objMemberPaymentAll = null;
            $lastYear = null;
        } else {
            $lastYear = null;
            if ($request->mpuid == 'null') {
                $memberId = $id;
                $objMember = MemberServiceProvider::getMemberById($memberId);
                $objMemberPayment = null;
                $objMemberPaymentAll = MemberPaymentsYearlyServiceProvider::getAllPaymentYearlyByMemberId($memberId);
            } else {
                if ($request->payment == 'null') {
                    $objMemberPayment = MemberPaymentsYearlyServiceProvider::getMemberPaymentYearlyByMemberId($id);
                }
                else {
                    $objMemberPayment = MemberPaymentsYearlyServiceProvider::getMemberPaymentByMemberId($id);
                }

                $objMemberPaymentAll = MemberPaymentsYearlyServiceProvider::getAllPaymentYearlyByMemberId($objMemberPayment->memberId);
                if (!empty($objMemberPayment) && !empty($objMemberPayment->memberId)) {
                    $objMember = MemberServiceProvider::getMemberById($objMemberPayment->memberId);
                } else {
                    $objMember = null;
                }
            }

            foreach ($objMemberPaymentAll as $key => $eachPayment) {
                if ($key == 0) {
                    $lastYear = $eachPayment->year;
                }
                else {
                    if ($eachPayment->year > $lastYear) {
                        $lastYear = $eachPayment->year;
                    }
                }
            }

            if (!is_null($objMember)) {
                $objMember['formattedBirthday'] = $objMember->formatted_birthday;
                $objMember['provinceName'] = Offices::getProvinceNameByProvinceCode($objMember->province_code);
            }

            if (!is_null($objMemberPayment)) {
                $objMemberPayment['formattedCurrency'] = $objMemberPayment->formatted_currency;
            }
        }

        $objFeePolicy[] = FeePolicyRepository::searchByListId();

        if (empty($objMember)) {
            abort(404);
        }

        if ($objMember->is_delete) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã bị xóa.']]);
        }

        if ($objMemberPayment) {
            $request->session()->put('number_payment', $objMemberPayment->number_payment);
        }

        return response()->json(['objMember' => $objMember, 'objMemberPayment' => $objMemberPaymentAll, 'objMemberPaymentEdit' => $objMemberPayment, 'lastYear' => $lastYear, 'objFeePolicy' => $objFeePolicy]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {
        $objMemberPayment = MemberPaymentsYearlyServiceProvider::getMemberPaymentByMemberId($id);
        if (!empty($objMemberPayment) && !empty($objMemberPayment->memberId)) {
            $objMember = MemberServiceProvider::getMemberById($objMemberPayment->memberId);
        } else {
            $objMember = null;
        }

        if (empty($objMember)) {
            abort(404);
        }

        return view('admin.payment_yearly.create_edit', [
            'objMember' => $objMember,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if (empty($id)) {
            $objMember = null;
            $objMemberPayment =  null;
        } else {
            $objMemberPayment = MemberPaymentsYearlyServiceProvider::getMemberPaymentByMemberId($id);
            if (!empty($objMemberPayment) && !empty($objMemberPayment->memberId)) {
                $objMember = MemberServiceProvider::getMemberById($objMemberPayment->memberId);
            } else {
                $objMember = null;
            }
        }
        if (empty($objMember)) {
            abort(404);
        }

        return view('admin.payment_yearly.update', [
            'objMember' => $objMember,
        ]);
    }

    public function update($id, MemberYearlyPayment $request)
    {
        $request->session()->forget('number_payment');

        if (empty($id)) {
            return Response::json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này")));
        }

        $objMemberPaymentYearly = MemberPaymentsYearlyServiceProvider::getMemberPaymentById($id);
        if (!empty($objMemberPaymentYearly) && !empty($objMemberPaymentYearly->memberId)) {
            $objMember = MemberServiceProvider::getMemberById($objMemberPaymentYearly->memberId);
        }

        if (empty($objMember)) {
           return Response::json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này")));
        }

        $objMemberPaymentYearly = MemberPaymentsYearlyServiceProvider::getByMemberIdAndNumberOfYear($objMember->id, $request->year);

        if ($objMember->is_delete) {
            return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã bị xóa.']]);
        }
        
        $dataReq = $request->all();
        $isUpdated = MemberPaymentsYearlyServiceProvider::update($dataReq, $id);
        if (empty($isUpdated)) {
            return response()->json(['errors' => ['memberInvalid' => 'Cập nhật thất bại.']]);
        } else {
            $var = $dataReq['date_expiration'];
            $date = str_replace('/', '-', $var);
            $objMember->member_code_expiration = date('Y-m-d 00:00:00', strtotime($date));
            $objMember->save();
            $noteContent = $dataReq['note'];
            if ($objMember->status == 13) {
                $actionStatus = MemberConstants::WAITTING_PRINT_ACTION_TYPE;
            } else {
                $actionStatus = MemberConstants::CODE_PROVIDING_WAITING;
            }

            NoteServiceProvider::createNote($id, 'Đã cập nhật hội phí thường niên', MemberConstants::IS_PAID_ACTION, $actionStatus);
            return response()->json(['success' => 'Cập nhật thành công']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, MemberYearlyPayment $request)
    {
        if ($request->input('status') == 4) {
            if (empty($id) || $request->input('status') != MemberConstants::FEE_WAITING) {
                return response()->json(array('errors' => array("memberInvalid" =>"không được nộp phí cho hồ sơ có trạng thái chưa hợp lệ.")));
            }
            $objMember = MemberServiceProvider::getMemberById($id);

            if (empty($objMember)) {
               return response()->json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này . ")));
            }

            if ($objMember->is_delete) {
                return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã bị xóa.']]);
            }
            $isStatus = MemberServiceProvider::isStatus($id, MemberConstants::FEE_WAITING);

            if(!$isStatus) {
                return response()->json(['errors' => ['memberInvalid' => 'Hồ sơ này đã được hoàn tất xử lý trước đó.']]);
            }

            $data = $request->all();
            $isCreated = MemberPaymentsYearlyServiceProvider::create($data, $id);
            if ($isCreated) {
                $var = $data['date_expiration'];
                $date = str_replace('/', '-', $var);
                $objMember->member_code_expiration = date('Y-m-d 00:00:00', strtotime($date));
                $objMember->save();

                NoteServiceProvider::createNote($id, 'Đã thêm mới hội phí lần đầu', MemberConstants::IS_PAID_ACTION, MemberConstants::CODE_PROVIDING_WAITING);

                $firstMail = $objMember->firstEmail;
                $secondMail = $objMember->secondEmail;

                self::_sendPaymentMail($objMember, $firstMail, $data["currency"]);

                if (!empty($secondMail)) {
                    self::_sendPaymentMail($objMember, $secondMail, $data["currency"]);
                }

                MemberServiceProvider::updateMemberStatus($id, MemberConstants::CODE_PROVIDING_WAITING);

                return response()->json(['success' => 'Tạo mới thành công']);
            }
        }
        else {
            if (empty($id)) {
                return response()->json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này.")));
            }

            if ($request->mpuid == 'null') {
                $objMember = MemberServiceProvider::getMemberById($id);
            } else {
                $objMemberPaymentYearly = MemberPaymentsYearlyServiceProvider::getMemberPaymentById($id);
                if (!empty($objMemberPaymentYearly) && !empty($objMemberPaymentYearly->memberId)) {
                    $objMember = MemberServiceProvider::getMemberById($objMemberPaymentYearly->memberId);
                }
            }

            if (empty($objMember)) {
               return response()->json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này.")));
            }

            if ($request->mpuid == 'null') {
                $objMemberPaymentYearly = MemberPaymentsYearlyServiceProvider::getByMemberIdAndYear($id, $request->year);
            } else {
                $objMemberPaymentYearly = MemberPaymentsYearlyServiceProvider::getByMemberIdAndYear($objMember->id, $request->year);
            }
            if (!empty($objMemberPaymentYearly)) {
                return response()->json(array('errors' => array("memberInvalid" =>"Không thực hiện được đóng hội phí, thành viên $objMember->fullname đã đóng hội phí cho năm $request->year trước đó.")));
            }

            if (empty($objMember)) {
                return response()->json(array('errors' => array("memberInvalid" =>"Không có dữ liệu hồ sơ này.")));
            }

            if ($objMember->is_delete) {
                return response()->json(['errors' => ['memberInvalid' => 'Hội viên đã bị xóa.']]);
            }

            if ($request->mpuid == 'null') {
                $dataReq = $request->all();
                $noteContent = $dataReq['note'];
                $isCreated = MemberPaymentsYearlyServiceProvider::create($dataReq, $id);
                
                if ($isCreated) {
                    $firstMail = $objMember->firstEmail;
                    $secondMail = $objMember->secondEmail;

                    //update member_code_expiration into members table
//                    $var = $dataReq['date_expiration'];
//                    $date = str_replace('/', '-', $var);
//                    $objMember->member_code_expiration = date('Y-m-d 00:00:00', strtotime($date));
                    $objMember->member_code_expiration = Carbon::createFromFormat('d/m/Y', $dataReq['date_expiration'])->format("Y-m-d");
                    $objMember->save();

                    self::_sendPaymentYearlyMail($objMember, $firstMail, $dataReq["currency"]);

                    if (!empty($secondMail)) {
                        self::_sendPaymentYearlyMail($objMember, $secondMail, $dataReq["currency"]);
                    }

                    NoteServiceProvider::createNote($id, 'Đã thêm mới hội phí thường niên', MemberConstants::IS_PAID_ACTION, MemberConstants::WAITTING_PRINT_ACTION_TYPE);

                    return response()->json(['success' => 'Thêm mới hội phí thường niên thành công']);
                }
            }
        }
        return response()->json(['errors' => ['memberInvalid' => 'Đã có lỗi xảy ra...']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($id)) {
            $objMember = null;
            $objMemberPaymentYearly = null;
        } else {
            $objMemberPaymentYearly = MemberPaymentsYearlyServiceProvider::getMemberPaymentById($id);
            if (!empty($objMemberPaymentYearly) && !empty($objMemberPaymentYearly->memberId)) {
                $objMember = MemberServiceProvider::getMemberById($objMemberPaymentYearly->memberId);
            }
        }

        if (empty($objMember) && empty($objMemberPaymentYearly)) {
            abort(404);
        }

        MemberPaymentsYearlyServiceProvider::deletePayment($id);
        $isPayment = MemberPayments::where('memberId', $objMember->id)->where('is_delete', 0)->first();
        if (empty($isPayment)) {
            MemberServiceProvider::updateMemberStatusByAccount($objMember->id,MemberConstants::FEE_WAITING);
        }
        // LogServiceProvider::createSystemHistory(trans('history.fn_delete_payment'), [
        //     'memberId' => $id,
        //     'payment' => $objMemberPaymentYearly
        // ]);
        // MemberServiceProvider::updateMemberStatusByAccount($id,MemberConstants::FEE_WAITING);
        /*LogServiceProvider::createSystemHistory(trans('history.fn_revert_status_payment'), [
            'memberId' => $id,
            'status' => MemberConstants::FEE_WAITING
        ]);*/

        return response()->json(['success' => 'Xoá hội phí thường niên thành công']);
    }

    public function sendNotification(Request $request)
    {
        $idArr = $request->input('member-ids');
        if (!is_array($idArr)) {
            return response()->json(['errors' => 'Vui lòng chọn ít nhất 1 thành viên.']);
        }

        $ids = implode(',', $idArr);
        $objMembers = \App\Models\Member::find($idArr);

        if (is_null($objMembers)) {
            return response()->json(['errors' => 'Không tìm thấy thành viên nào.']);
        }

        $failedNames = [];
        foreach($objMembers as $objMember) {
            try {
                MemberServiceProvider::sendInformFee($objMember);
                // send sms
                $this->sendSms($objMember);
            } catch(\Exception $e) {
                array_push($failedNames, $objMember->fullName);
                //return response()->json(['errors' => $e->getMessage()]);
            }
        }

        $numberOfFailed = count($failedNames);
        if ($numberOfFailed == 0) {
            return response()->json(['success' => 'Đã gửi thông báo']);
        } elseif ($numberOfFailed == 1) {
            return response()->json(['errors' => 'Có lỗi xảy ra trong quá trình gửi thông báo cho thành viên <br>' . implode(', ', $failedNames) .  '<br> Vui lòng thử lại sau.']);
        } else {
            return response()->json(['errors' => 'Có lỗi xảy ra trong quá trình gửi thông báo cho các thành viên <br>' . implode(', ', $failedNames) .  '<br> Vui lòng thử lại sau.']);
        }
    }

    private function sendSms($objMember)
    {
        $view = View::make('admin.member.sms.inform_fee', ['objMember' => $objMember]);
        $smsContent = (string) $view;
        $firstMobile = $objMember->firstMobile;
        $secondMobile = $objMember->secondMobile;
        Utils::sendSms($firstMobile, $smsContent);
        if (!empty($secondMobile)) {
            Utils::sendSms($secondMobile, $smsContent);
        }
    }

    public function importExcel (Request $request)
    {
        $file = $request->file('fileToUpload');
        if(!$file) {
            return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'from-year' => date('Y'), 'to-year' => date('Y') + 4])->with('error_message', 'Không có file!');
        }

        $fileMine = $file->getClientMimeType();
        $fileType = $file->getClientOriginalExtension();
        $fileSize = $file->getClientSize();

        if ($fileSize > 10000000) {
            return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'from-year' => date('Y'), 'to-year' => date('Y') + 4])->withErrors('File quá lớn, dung lượng file cho phép tối đa là 10Mb!');
        }

        if ($this->getNumRows($file) == 3) {
            return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'from-year' => date('Y'), 'to-year' => date('Y') + 4])->withErrors('File không có dữ liệu để cập nhật!');
        }

        $mineRules = array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel');
        if(!in_array($fileMine, $mineRules) || $fileType != Type::XLSX) {
            return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'from-year' => date('Y'), 'to-year' => date('Y') + 4])->withErrors('File không đúng định dạng!');
        }

        try {
            $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
            $reader->open($file);
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('update_member_payment_fail', 'Error when update member payment(Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ')');
            return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'from-year' => date('Y'), 'to-year' => date('Y') + 4])->withErrors(['error_message' => 'Không thể đọc file.']);
        }

        $sheetList = $reader->getSheetIterator();

        if (empty($sheetList)) {
            return redirect()->route('admin_payment_fee_import_view');
        }

        DB::beginTransaction();
        foreach ($sheetList as $key => $sheet) {
            if ($key > 1) {
                break;
            }

            $recordValid = 0;
            $rowCount = 0;
            $errors = array();
            $arrFileCodesErr = [];
            $already = [];
            $done = [];
            $mss = [];
            // $donemss = [];
            // $arrayMssMerge = [];
            foreach ($sheet->getRowIterator() as $keyRow => $arrRow) {
                if ($this->emptyArrayElement($arrRow)) continue;
                $rowCount++;
                if($keyRow == 3){
                    if($arrRow != ['TT', 'Số thẻ HDV', 'Số chứng từ', 'Ngày chứng từ', 'Số tiền', 'Nội dung nộp tiền', 'Ghi chú', 'Hội phí năm']){
                        return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'from-year' => date('Y'), 'to-year' => date('Y') + 4])->withErrors('File không đúng định dạng!');
                    }
                }

                // do stuff with the row
                if ($keyRow > 3) {

                    // $arrRow[1] => file_code
                    if (!empty($arrRow[1])) {
                        // do update in members table
                        // $objMember = MemberServiceProvider::getMemberExistByMemberCode($arrRow[1]);
                        $objMember = MemberServiceProvider::getMemberExistByTouristGuideCode($arrRow[1]);
                        // file code is exist
                        if (empty($objMember) || empty($memberId = $objMember->id)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 2 - Không tìm thấy thành viên trong cơ sở dữ liệu.';

                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        // if (!empty($objMember) && !empty($objMember->status)) {
                        //     // Chỉ status = 13 mới được nộp hội phí năm
                        //     if ($objMember->status != MemberConstants::MEMBER_OFFICIAL_MEMBER) {
                        //         // add file code in the row has wrong data type to $arrFileCodesErr
                        //         $arrFileCodesErr[] = $arrRow[1];
                        //         continue;
                        //     }
                        // }

                        // number payment is exist and content length <= 20
                        if(strlen($arrRow[2]) > self::NUMBER_PAYMENT_MAX_LENGTH) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 3 - Số chứng từ không được lớn hơn 20 kí tự.';

                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        // payment date is required and format as d/m/y
                        $datePayment = $arrRow[3];

                        if (is_object($datePayment)) {
                            $datePayment = $datePayment->format(CommontConstants::D_M_Y_FORMAT);
                            $today = Carbon::today()->format(CommontConstants::D_M_Y_FORMAT);
                            $approvedAt  = $objMember->approved_at->format(CommontConstants::D_M_Y_FORMAT);
                        }

                        $datePaymentMysql = \DateTime::createFromFormat(CommontConstants::D_M_Y_FORMAT, $datePayment);
                        $datePaymentMysql = date_format($datePaymentMysql, 'Y-m-d H:i:s');

                        // if(empty($datePayment) || empty(Utils::validateDateFormat($datePayment,CommontConstants::D_M_Y_FORMAT)) ||
                        //     (!empty($approvedAt) && ($approvedAt <= $datePayment || ($today >= $datePayment)))) {
                        if (empty($datePayment)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 4 - Ngày chứng từ sai định dạng. dd/mm/yyyy';
                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        // payment number int, max 10, > 0, not null
                        $paymentNumber = str_replace('.', '', $arrRow[4]);
                        $paymentNumber = str_replace(',', '', $paymentNumber);
                        $paymentNumber = str_replace(' ', '', $paymentNumber);

                        if(empty($paymentNumber) || !($paymentNumber > 0) || strlen($paymentNumber) > self::PAYMENT_NUMBER_MAX_LENGTH || !is_numeric($paymentNumber)) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 5 - Số tiền phải chỉ được tối đa 10 số và là số lớn hơn 0.';
                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        //  payment content is required and content length >= 200
                        if(strlen($arrRow[5]) > self::PAYMENT_CONTENT_MAX_LENGTH) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột: 6 - Nội dung chứng từ không được lớn hơn 200 kí tự.';

                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }
                        // payment note content length >= 200
                        if(array_key_exists(6, $arrRow) && strlen($arrRow[6]) > self::PAYMENT_NOTE_MAX_LENGTH) {
                            $errors[] = 'Hàng: ' . $keyRow . ' - Cột : 7 - Ghi chú chứng từ không được lớn hơn 200 kí tự.';

                            // add file code in the row has wrong data type to $arrFileCodesErr
                            $arrFileCodesErr[] = $arrRow[1];

                            continue;
                        }

                        if(array_key_exists(7, $arrRow)) {
                            $years = explode(",", $arrRow[7]);
                            $checkError = false;
                            foreach ($years as $eachYear) {
                                if ($arrRow[7] < date('Y')) {
                                    $checkError = true;
                                }
                            }
                            if ($checkError) {
                                $errors[] = 'Hàng: ' . $keyRow . ' - Cột : 8 - Hội phí năm phải là năm lớn hơn bằng năm hiện tại.';

                                // add file code in the row has wrong data type to $arrFileCodesErr
                                $arrFileCodesErr[] = $arrRow[1];

                                continue;
                            }
                        }


                        $years = explode(",", $arrRow[7]);
                        $data['number_payment'] = $arrRow[2];
                        $data['date_payment'] = $datePaymentMysql;
                        $data['currency'] = $arrRow[4];
                        $data['content'] = $arrRow[5];
                        $data['note'] = $arrRow[6];
                        $yearsExist = [];
                        foreach ($years as $eachYear) {
                            $findMember = MemberPaymentsYearly::where('memberId', $memberId)->where('year', $eachYear)->where('is_delete', 0)->get();
                            if (count($findMember) != 0) {
                                $yearsExist[] = $eachYear;
                                $already[] = 'Hồ sơ : ' . $arrRow[1] . ' đã nộp hội phí năm ' . $eachYear;
                                $recordValid++;
                            }
                        }
                        $years_not_exist = array_diff($years, $yearsExist);
                        if (count($years_not_exist) != 0) {
                            $data['years'] = $years_not_exist;
                            if ($objMember->status == 4) {
                                MemberPaymentsYearlyServiceProvider::create($data, $objMember->id);
                                $countDataYear = count($data['years']);
                                //update member_code_expiration into members table
                                $objMember->member_code_expiration = $data['years'][$countDataYear - 1].'-12-31 00:00:00';
                                $objMember->save();


                                NoteServiceProvider::createNote($objMember->id, $data['note'], MemberConstants::IS_PAID_ACTION, MemberConstants::CODE_PROVIDING_WAITING);

                                $firstMail = $objMember->firstEmail;
                                $secondMail = $objMember->secondEmail;

                                self::_sendPaymentMail($objMember, $firstMail, $data["currency"]);

                                if (!empty($secondMail)) {
                                    self::_sendPaymentMail($objMember, $secondMail, $data["currency"]);
                                }

                                MemberServiceProvider::updateMemberStatus($objMember->id, MemberConstants::CODE_PROVIDING_WAITING);
                            } else {
                                MemberPaymentsYearlyServiceProvider::create($data, $objMember->id);

                                $firstMail = $objMember->firstEmail;
                                $secondMail = $objMember->secondEmail;

                                //update member_code_expiration into members table
                                $countDataYear = count($data['years']);
                                $objMember->member_code_expiration = $data['years'][$countDataYear - 1].'-12-31 00:00:00';
                                $objMember->save();

                                self::_sendPaymentYearlyMail($objMember, $firstMail, $data["currency"]);

                                if (!empty($secondMail)) {
                                    self::_sendPaymentYearlyMail($objMember, $secondMail, $data["currency"]);
                                }
                            }
                            $done[] = 'Hồ sơ : ' . $arrRow[1] . ' năm ' . implode(",", $years_not_exist) . ' đã được cập nhật.';
                        }
                    }
                }
            }
            /*if(!empty($errors)) {
                DB::rollback();
                return redirect()->route('admin_member_list_payment_yearly_view',['status' => MemberConstants::FEE_WAITING])->withErrors(['error_message' => $errors]);
            }*/
            if(!empty($arrFileCodesErr)) {
                // DB::rollback();
                $mss[] = 'Dữ liệu hồ sơ: ' . implode($arrFileCodesErr, ', ') . ' không hợp lệ.';
                if (!empty($errors)) {
                    $mss[] = implode($errors, ', ');
                }

                // if (!empty($already)) {
                //     $mss[] = implode($already, ', ');
                // }
                $arrayMssMerge[] = $mss;
                // return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'year' => date('Y')])
                //     ->withErrors($mss);
            }

            // if($recordValid === ($rowCount - 3)) {
            if (count($done) != 0) {
                DB::commit();
                $mss[] = implode($done, ', ');
            }

                // LogsHelper::trackByFile('update_member_payment_success', $recordValid . 'inserted successfully.');
                // return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'year' => date('Y')])
                //     ->with(['successes' => ['Cập nhật dữ liệu thành công.']]);
            // } else {
                // DB::rollback();

                // return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'year' => date('Y')])
                //     ->withErrors($mss);
            // }

            if (!empty($already)) {
                $mss[] = implode($already, ', ');
            }


            return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'from-year' => date('Y'), 'to-year' => date('Y') + 4])->withErrors($mss);
        }
        $reader->close();

        return redirect()->route('admin_member_list_payment_yearly_view',['status' => 1, 'from-year' => date('Y'), 'to-year' => date('Y') + 4]);
    }

    public function getNumRows($xlsxPath) {
        $numRows = 0;

        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($xlsxPath);

        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                $numRows++;
            }
        }

        $reader->close();

        return $numRows;
    }

    public function emptyArrayElement($array = array()){
        $check  = 0;
        $count = count($array);
        foreach ($array as $cell) {
            if(empty($cell))
                $check++;
        }
        return $check == $count;
    }

    public function resendNotification (Request $request)
    {
        $strMemberIdList = $request->input('list_id');
        $arrResendNotificationMemberId = explode(',', $strMemberIdList);

        foreach ($arrResendNotificationMemberId as $key => $memberId) {
            $objMember = MemberServiceProvider::getMemberById($memberId);

            if (!empty($objMember->member_code)) {
                $memberCode = $objMember->member_code;
            } else {
                $memberCode = '';
            }

            $smsContent = 'De nghi quy hoi vien dong 500.000 vnd hoi phi HHDV Du lich Viet Nam nam ' . date('Y') . ' ve tai khoan: Hiep hoi Du lich Viet Nam: so TK: 0011004369919, So Giao dich Vietcombank. Noi dung: Dong Hoi phi HHDV nam ' . date('Y') .', So the hoi vien: ' . $memberCode;

            if (!empty($objMember->firstMobile)) {
                dispatch((new SendSms($objMember->firstMobile, $smsContent))->delay(Carbon::now()->addSeconds($key + 5)));
            }

            if (!empty($objMember->secondMobile)) {
                dispatch((new SendSms($objMember->secondMobile, $smsContent))->delay(Carbon::now()->addSeconds($key + 5)));
            }
        }

        return back()->withInput()->with('successes', ['Gửi lại thông báo nộp hội phí thường niên thành công']);
    }

    public function resendNotificationEmail (Request $request)
    {
        $strMemberIdList = $request->input('list_id');
        $arrResendNotificationMemberId = explode(',', $strMemberIdList);

        foreach ($arrResendNotificationMemberId as $key => $memberId) {
            $objMember = MemberServiceProvider::getMemberById($memberId);
            if (!empty($objMember->firstEmail)) {
                $this->sendMail($objMember->firstEmail, $objMember, $key);
            }

            if (!empty($objMember->secondEmail)) {
                $this->sendMail($objMember->secondEmail, $objMember, $key);
            }
        }

        return back()->withInput()->with('successes', ['Gửi lại thông báo nộp hội phí thường niên thành công']);
    }

    private function sendMail ($email, $objMember, $key)
    {
        try {
            $to = [['address' => $email, 'name' => $objMember->fullName]];
            $subject = '';
            $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL13')->where('branch_id', $objMember->province_code)->first();
            if (empty($getEmail)) {
                $getEmail = Emails::select('content', 'title', 'attach')->where('option_code', 'EMAIL13')->where('branch_id', self::TOANQUOC)->first();
            }

            $attach = null;
            
            if (!empty($getEmail)) {
                $subject = $getEmail->title;
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_MAHOIVIEN, htmlentities($objMember->member_code), $getEmail->content);
    
                if (!empty($getEmail->attach)) {
                    $attach = [
                        'realpath' => $getEmail->attach,
                        'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                        'mime' => 'pdf'
                    ];
                }
            }
           
            $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
            $template = 'admin.payment_yearly.mail_notify_payment';
            dispatch((new SendEmail($template, $data, $to, '', $subject, $attach))->delay(Carbon::now()->addSeconds($key + 5)));
        } catch (\Exception $exception) {
            LogsHelper::exceptionByFile('resend_mail_payments_yearly_fail', $exception, $objMember);
        }
    }

    /*
     * type = 1
     * type = 2 Hội viên chưa nộp hội phí
     * type = 3 Hội viên đã nộp hội phí
     */
    public function exportExcel(Request $request) {

        //chi hoi HDV
        $branchTmp = Offices::get();
        $branchList = [];
        foreach($branchTmp as $value){
            $branchList[$value['id']] = $value['name'];
        }

        $branchTmp2 = Offices::where('option_code', 'BRANCHES03')->get();
        if(!empty($branchTmp2)){
            foreach($branchTmp2 as $value){
                $branchList[$value['id']] = $value['name'];
            }
        }

        $type = $request->input('type', '');
        $fromYear = $request->input('from-year', '');
        $toYear = $request->input('to-year', '');

        //Tên tieu de danh sach da nop phi

        $province_code = $request->input('province_code', '');
        $check = 0;
        $provinCode = '';
        if ($province_code != '') {
            $check = 1;
            $provinCode = $branchList[$province_code];
        }

        //ngon ngu huong dan
        $listLanguage = Language::getAllLanguages();

        $arrConditions = [
            self::NUMBER_PAYMENT                    => $request->input(self::NUMBER_PAYMENT, ''),
            CommontConstants::FULL_NAME             => $request->input(CommontConstants::FULL_NAME),
            CommontConstants::TOURIST_GUIDE_CODE    => $request->input(CommontConstants::TOURIST_GUIDE_CODE),
            CommontConstants::MEMBER_CODE           => $request->input(CommontConstants::MEMBER_CODE),
            CommontConstants::STATUS                => $request->input(CommontConstants::STATUS),
            CommontConstants::PROVINCE_CODE         => $request->input(CommontConstants::PROVINCE_CODE),
            self::FROM_YEAR                         => $request->input(self::FROM_YEAR, ''),
            self::TO_YEAR                           => $request->input(self::TO_YEAR, ''),
            self::TO_DATE                           => $request->input(self::TO_DATE, ''),
            self::FROM_DATE                         => $request->input(self::FROM_DATE, ''),
            self::STATUS                            => $request->input(self::STATUS)
        ];

        $widthB = 15;
        $widthG = 30;
        $widthC = 30;
        $total = 0;

        //chua nop phi lan dau
        if($type == 1){
            $fileName = 'DanhSachHoiVienChuaNopLanDau_' . date('YmdHis') . '-' . Utils::randomString(6);
            $widthB = 30;
            $widthC = 10;
            $widthG = 20;
            $listMember = MemberPaymentsYearlyServiceProvider::searchMemberByConditionsPaymentFirst($arrConditions, null, 'export');
        }

        //chưa nộp phí thường niên
        if($type == 2){
            $listMember = MemberPaymentsYearlyServiceProvider::searchMemberByConditionsPaymentYearly($arrConditions, null, 'export');
            $fileName = 'DanhSachHoiVienChuaNopPhiThuongNien' . date('YmdHis') . '-' . Utils::randomString(6);
            $widthB = 15;
            $widthG = 30;
            $widthC = 30;
        }

        //đã nộp phí thường niên
        if($type == 3){
            $listMember = MemberPaymentsYearlyServiceProvider::searchMemberByConditionsPaymentYearly($arrConditions, null, 'export');
            $fileName = 'DanhSachHoiVienDaNopPhiThuongnien_' . date('YmdHis') . '-' . Utils::randomString(6);
            $widthB = 30;
            $widthC = 10;
            $widthG = 20;
           
            foreach ($listMember as $key => $item) {
                $money = 0;
                if (!empty($item->date_payment)) {
                    if (strtotime($item->date_payment) >= strtotime(self::PAYMENT_YEARLY_POINT)) {
                        $money = $item->currency / $item->number_of_year;
                    }
                    else {
                        if ($item->flag == 1) {
                            $money = $item->currency / $item->number_of_year;
                        }
                        else {
                            $money = ($item->currency - 500000) / $item->number_of_year;
                        }
                    }

                    if ($money != 0) {
                        $total += $money;
                    }
                }
            }
        }

        Excel::create($fileName, function($excel) use ($type, $fromYear, $toYear, $check,  $provinCode, $listLanguage, $branchList, $listMember, $widthB, $widthG, $widthC, $total) {
            $excel->setTitle('HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM');
            $excel->sheet('First sheet', function($sheet) use ($type, $fromYear, $toYear, $check, $provinCode, $listLanguage, $branchList, $listMember, $widthB, $widthG, $widthC, $total) {
                $sheet->setWidth(array(
                        'A'     =>  5,
                        'B'     =>  $widthB,
                        'C'     =>  $widthC,
                        'D'     =>  20,
                        'E'     =>  20,
                        'F'     =>  20,
                        'G'     =>  $widthG,
                        'H'     =>  30,
                        'I'     =>  30,
                        'J'     =>  25,
                    ));

                $sheet->loadView('admin.payment_yearly.export_excel', [
                    'type'              => $type,
                    'fromYear'          => $fromYear,
                    'toYear'            => $toYear,
                    'check'             => $check,
                    'provinCode'        => $provinCode,
                    'listLanguage'      => $listLanguage,
                    'branchList'        => $branchList,
                    'listMember'        => $listMember,
                    'total'             => $total,
                    'paymentYearlyPoint' => self::PAYMENT_YEARLY_POINT
                ]);
            });
        })->download('xlsx');
    }

    private static function _sendPaymentYearlyMail ($objMember, $email, $feeMoney = 0)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $subject = '';

                $getEmail = Emails::select('id','content', 'title', 'attach')->where('option_code', 'EMAIL12')->where('branch_id', $objMember->province_code)->first();
				LogsHelper::trackByFile('send_mail_payment_yearly: ', $objMember->province_code);
                if (empty($getEmail)) {
                    $getEmail = Emails::select('id','content', 'title', 'attach')->where('option_code', 'EMAIL12')->where('branch_id', self::TOANQUOC)->first();
					LogsHelper::trackByFile('send_mail_payment_yearly: ', self::TOANQUOC);
                }

                $attach = null;
                $feeMoney = MailHelper::currency_format($feeMoney);
                if (!empty($getEmail)) {
                    $subject = $getEmail->title;
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_HOIPHI_DANOP, $feeMoney, $getEmail->content);

                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }
                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
                $template = 'admin.payment_yearly.payment_yearly_email';
                dispatch(new SendEmail($template, $data, $to, '', $subject, $attach))->delay(0);
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_payment_yearly_fail', 'Error when send mail payment yearly (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember], true) . ')');
            }
        }
    }

    private static function _sendPaymentMail ($objMember, $email, $feeMoney = 0)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $objMember->fullName]];
                $subject = '';
                $getEmail = Emails::select('id','content', 'title', 'attach')->where('option_code', 'EMAIL11')->where('branch_id', $objMember->province_code)->first();
                LogsHelper::trackByFile('send_mail_payment_yearly: ', $objMember->province_code);
				if (empty($getEmail)) {
                    $getEmail = Emails::select('id','content', 'title', 'attach')->where('option_code', 'EMAIL11')->first();
					LogsHelper::trackByFile('send_mail_payment_yearly: ', self::TOANQUOC);
                }
                $feeMoney = MailHelper::currency_format($feeMoney);
                $attach = null;
                if (!empty($getEmail)) {
                    $subject = $getEmail->title;
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objMember->fullName), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_HOIPHI_DANOP, $feeMoney, $getEmail->content);

                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }
                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
                $template = 'admin.payment.payment_email';
                dispatch((new SendEmail($template, $data, $to, '', $subject, $attach))->delay(0));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_payment_fail', 'Error when send mail payment (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objMember], true) . ')');
            }
        }
    }
}
