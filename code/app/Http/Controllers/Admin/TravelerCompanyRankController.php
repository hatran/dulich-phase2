<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Rank;
use App\Providers\TravelerCompanyRankProvider;
use App\Providers\LogServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\UserConstants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Constants\MemberConstants;
use App\Libs\Helpers\Utils;
use App\Models\TravelerCompanyRank;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Emails;
use App\Libs\Helpers\LogsHelper;
use App\Jobs\SendEmail;

use App\Models\Language;
use App\Models\Member;
use App\Models\Offices;

class TravelerCompanyRankController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const SEARCH_INPUT = 'search';
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';
    const OBJ_TRAVELERS = 'objTravelers';

    const COMPANY_NAME = 'company_name';
    const TAX_CODE = 'tax_code';
    const STATUS = 'status';
    const CURRENT_PAGINATOR = 'current_paginator';
    const COUNT_TRAVELER = 'count_travelers';

    const TRAVELER_INDEX = 'travelercompanyrank.index';
    const TRAVELER_SHOW = 'travelercompanyrank.show';
    const SUCCESSES = 'successes';
    const BCC_EMAIL = 'hoihdv@gmail.com';
    const EMAIL04 = 'EMAIL04';

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objTravelers = $this->_searchByCondition($request, $limit,false);
        $sumObjTravelers = $objTravelers->total();
        $current_page = $request['page'] ?? 0;
        if ($current_page >= 1){
            $current_page = ($current_page - 1) * 10;
        }

        return view('admin.traveler_company_rank.index', [
            self::OBJ_TRAVELERS     => $objTravelers,
            'admin_user'            => UserConstants::ADMIN_USER,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_TRAVELER    => $sumObjTravelers,
            self::PAGINATOR         => ($sumObjTravelers > 0) ? $objTravelers->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(self::COMPANY_NAME))) {
            $company_name = $request->input(self::COMPANY_NAME);
        } else {
            $company_name = '';
        }

        if (!empty($request->input(self::TAX_CODE))) {
            $tax_code = $request->input(self::TAX_CODE);
        } else {
            $tax_code = '';
        }

        if ($request->input(self::STATUS) != "") {
            $status = $request->input(self::STATUS);
        } else {
            $status = '';
        }
        
        $arrConditions = [
            self::COMPANY_NAME  => $company_name,
            self::TAX_CODE   => $tax_code,
            self::STATUS => $status
        ];

        return TravelerCompanyRankProvider::travelersSearchByConditions($arrConditions, $limit,$isCount);
    }

    public function saveTravelers (Request $request)
    {
        if(!empty($request->input('id_edit'))){
            $checkData = TravelerCompanyRankProvider::getDataById($request->input('id_edit'));
            if(empty($checkData->id)){
                return redirect()->route($this::TRAVELER_INDEX)->withErrors(['Công ty lữ hành này không tồn tại hoặc đã xóa khỏi hệ thống']);
            }
        }

        $objData = [];
        $businessCertificateAll = null;
        //add danh muc
        if(empty($request->input('id_edit'))){
            $companyNameAll = $request->input('companyNameAll_add');
            $status = $request->input('statusAll_add');
            $taxCodeAll = $request->input('taxCodeAll_add');
            $checkTaxCode = TravelerCompanyRankProvider::checkCode($taxCodeAll);
            if (!empty($checkTaxCode)) {
                return redirect()->route($this::TRAVELER_INDEX)->withErrors(['Mã số thuế vừa nhập đã tồn tại']);
            }
            $addressAll = $request->input('addressAll_add');
            $emailAll = $request->input('emailAll_add');
            $checkEmail = TravelerCompanyRankProvider::checkEmailInUserTable($emailAll);
            if (!empty($checkEmail)) {
                return redirect()->route($this::TRAVELER_INDEX)->withErrors(['Email này đã tồn tại']);
            }
            $phoneAll = $request->input('phoneAll_add');
            $businessCertificate = $request->input('businessCertificateAll_add');
            if (isset($businessCertificate)) {
                $businessCertificateAll = $businessCertificate;
            }
         
        }else{
            $id = $request->input('id_edit');
            $companyNameAll = $request->input('companyNameAll_edit');
            $status = $request->input('statusAll_edit');
            $taxCodeAll = $request->input('taxCodeAll_edit');
            $addressAll = $request->input('addressAll_edit');
            $emailAll = $request->input('emailAll_edit');
            $phoneAll = $request->input('phoneAll_edit');
            $businessCertificate = $request->input('businessCertificateAll_edit');
            if (isset($businessCertificate)) {
                $businessCertificateAll = $businessCertificate;
            }
        }
        
        $objData['company_name']           = $companyNameAll;
        $objData['status']         = $status;
        $objData['tax_code']      = $taxCodeAll;
        $objData['address']           = $addressAll;
        $objData['email']          = $emailAll;
        $objData['phone']          = $phoneAll;
        $objData['business_certificate'] = $businessCertificateAll;
        $objData['created_author'] = auth()->user()->username;

        DB::beginTransaction();
        try {
            if(!empty($request->input('id_edit'))){
                $objSave = TravelerCompanyRank::where('id', $id)->update($objData);
            }else {
                $objSave = TravelerCompanyRank::create($objData);
            }

            $checkUser = TravelerCompanyRankProvider::checkCodeInUserTable($objData['tax_code']);
            $password = str_random(8);
            if (empty($checkUser)) {
                $user = new User();
                $user->fullname = $objData['tax_code'];
                $user->username = $objData['tax_code'];
                $user->role = 1;
                $user->status = 1;
                $user->email = $objData['email'];
                $user->password = Hash::make($password);
                if ($objData['status'] == 0) {
                    $user->deleted_at = date('Y-m-d H:i:s');
                }
                else {
                    $user->deleted_at = null;
                }
                $user->is_traveler_company = $objData['status'] == 1 ? 1 : NULL;
                $user->save();
            }
            else {
                $checkUser->fullname = $objData['tax_code'];
                $checkUser->username = $objData['tax_code'];
                $checkUser->role = 1;
                $checkUser->status = 1;
                $checkUser->email = $objData['email'];
                $checkUser->password = Hash::make($password);
                if ($objData['status'] == 0) {
                    $checkUser->deleted_at = date('Y-m-d H:i:s');
                }
                else {
                    $checkUser->deleted_at = null;
                }
                $checkUser->is_traveler_company = $objData['status'] == 1 ? 1 : NULL;
                $checkUser->save();
            }
            if ($objData['status'] == 1) {
                $mailTemplate = 'email';
                self::_sendApprovedMail($objData['company_name'], $password, $objData['tax_code'], $objData['email'], $mailTemplate);
            }
            
            DB::commit();
            
        } catch (\Exception $e) {
            DB::rollback();
        }

        if (empty($objSave)) {
            if(!empty($request->input('id_edit'))){
                return redirect()->route($this::TRAVELER_INDEX)->withErrors(['Cập nhật công ty lữ hành thất bại']);
            }else{
                return redirect()->route($this::TRAVELER_INDEX)->withErrors(['Thêm mới công ty lữ hành thất bại']);
            }
        }
     
        if(!empty($request->input('id_edit'))){
            return redirect()->route($this::TRAVELER_INDEX)->with($this::SUCCESSES, ['Cập nhật công ty lữ hành thành công']);
        }else{
            return redirect()->route($this::TRAVELER_INDEX)->with($this::SUCCESSES, ['Thêm mới công ty lữ hành thành công']);
        }

    }


    private static function _sendApprovedMail($companyName, $password, $tax_code, $email, $mailTemplate)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $companyName]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $subject = 'Thông báo cho công ty lữ hành';
                $getEmail = Emails::select('content', 'attach')->where('option_code', self::EMAIL04)->first();
                $attach = null;
                
                if (!empty($getEmail)) {
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($companyName), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_USERNAME, htmlentities($tax_code), $getEmail->content);
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_PASSWORD, htmlentities($password), $getEmail->content);
    
                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }

                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
               
                $template = 'admin.traveler_company_rank.' . $mailTemplate;
                dispatch((new SendEmail($template, $data, $to, $bcc, $subject, $attach))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_traveler_rank_company', 'Error when send mail traveler rank company (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$companyName], true) . ')');
            }
        }
    }

    public function showEdit ($id, Request $request)
    {

        if (empty($id)) {
            $objData = null;
        } else {
            $objData = TravelerCompanyRankProvider::getDataById($id);
        }

        if (empty($objData)) {
            $request->session()->flash('error_ajax', ['Công ty lữ hành không tồn tại hoặc đã xóa khỏi hệ thống']);
            return response()->json(['deleted' => 'error']);
        }
        return response()->json(['objData' => $objData]);
    }

    public function deleteOption ($id)
    {
        $objTraveler = TravelerCompanyRankProvider::getDataById($id);
        if (empty($objTraveler)) {
            return redirect()->route($this::TRAVELER_INDEX)->withErrors(['Công ty lữ hành không tồn tại hoặc đã xóa khỏi hệ thống']);
        }

        $objData['is_deleted'] = 1;
        $objMemberTravelerCompanyRank = TravelerCompanyRankProvider::getStatusZeroById($id);
        if(!empty($objMemberTravelerCompanyRank)) {
            $objSave = TravelerCompanyRank::where('id', $id)->update($objData);
            $checkUser = TravelerCompanyRankProvider::checkCodeInUserTable($objTraveler->tax_code);
            if (!empty($checkUser)) {
                $checkUser->is_traveler_company = NULL;
                $checkUser->save();
            }
        }else{
            return redirect()->route($this::TRAVELER_INDEX)->withErrors(['Không được xóa công ty lữ hành đang hoạt động']);
        }
        if(empty($objSave)){
            return redirect()->route($this::TRAVELER_INDEX)->withErrors(['Xóa công ty lữ hành thất bại']);
        } else {
            return redirect()->route($this::TRAVELER_INDEX)->with($this::SUCCESSES, ['Xóa công ty lữ hành thành công']);
        }
    }

    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }

    public function evaluate(Request $_request)
    {
        if (auth()->check() && auth()->user()->is_traveler_company == 1) {
            $languages = $this->getLanguage();
            $data = $_request->input();
            $members = $this->_objByFliter($data);
            $objClub = $this->getOptionsClub();
            $objBranche = $this->getBraches();
            $request = $_request->input();
            unset($request['check']);
            unset($request['_token']);
            unset($request['captcha']);

            if (empty($request)) {
                $members = "";
            }

            $total = $this->_count;
            $ranks = Rank::getAllRanks();
            if ($_request->input('check')) {
                return view('frontend.ajax.evaluate', compact('members', 'languages', 'objClub', 'objBranche', 'request', 'total', 'ranks'));
            } else {
                if ($_request->input('tmp')) {
                    return view('frontend.ajax.evaluate', compact('members', 'languages', 'objClub', 'objBranche', 'request', 'total', 'ranks'));
                }
                return view('frontend.newdesign.evaluate', compact('members', 'languages', 'objClub', 'objBranche', 'request', 'total', 'ranks'));
            }
    
        }
        else {
            abort(404);
        }
        
    }

    //hoivien list user, search
    public function _objByFliter($members)
    {
        $captcha = !empty($members['captcha']) ? $members['captcha'] : '';
        $token = !empty($members['_token']) ? $members['_token'] : '';
        $name = !empty($members['name']) ? $members['name'] : '';
        $id2 = !empty($members['id2']) ? $members['id2'] : '';
        $id3 = !empty($members['id3']) ? $members['id3'] : '';
        $language = !empty($members['language']) ? $members['language'] : '';
        $province_code = $members['active'] ?? $members['type'] ?? '';

        $rules = [];
        if (!empty($members['check'])) {
            $captcha = array("_token" => $token, "captcha" => $captcha, "check" => '');
            $validator = Validator::make($captcha, $rules);
            if ($validator->fails()) {
                $this->_count = 0;
                return '';
            }
        }

        $obj = DB::table('members')
            ->whereIn('status', [MemberConstants::MEMBER_OFFICIAL_MEMBER, -1])
            ->whereNull('deleteAt')
            ->whereNull('is_delete');
        if (! empty($name)) {
            $obj->where('fullName', 'like', '%' . $name . '%');
        }

        if (! empty($id2)) {
            $obj->where('member_code', 'like', '%' . $id2 . '%');
        }

        if (! empty($id3)) {
            $obj->where('touristGuideCode', 'like', '%' . $id3 . '%');
        }

        if (! empty($province_code)) {
            $obj->where('province_code', $province_code);
        }

        if (! empty($language)) {
            $obj->where('guideLanguage', 'like', '%' . $language . '%');
        }

        $obj = $obj->paginate(4);
        $this->_count = $obj->total();
        return $obj;
    }

    public function getLanguage()
    {
        $languages = Language::where('status', 1)->get();
        return $languages;
    }

    public function getOptionsClub()
    {
        $objOffices = Offices::where('option_code', 'BRANCHES03')->where('status', 1)->whereNull('deleted_at')->orderBy('name', 'asc')->get();
        $arrayOffices = $objOffices->toArray();
        // html entity
        $resultArray = array();
        foreach ($arrayOffices as $objOffices) {
            $resultItem = array();
            foreach ($objOffices as $key => $item) {
                $resultItem[$key] = e($item);
            }
            $resultArray[] = $resultItem;
        }
        $lastResult = array();
        foreach ($resultArray as $key => $office) {
            $lastResult[] = $office;
        }
        return $lastResult;

    }

    public function getBraches()
    {
        $objOffices = Offices::where('option_code', 'BRANCHES01')->where('status', 1)->whereNull('deleted_at')->orderBy('name', 'asc')->get();
        $arrayOffices = $objOffices->toArray();
        // html entity
        $resultArray = array();
        foreach ($arrayOffices as $objOffices) {
            $resultItem = array();
            foreach ($objOffices as $key => $item) {
                $resultItem[$key] = e($item);
            }
            $resultArray[] = $resultItem;
        }
        $lastResult = array();
        foreach ($resultArray as $key => $office) {
            $lastResult[] = $office;
        }
        return $lastResult;

    }
}
