<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Offices;
use App\Models\User;
use Lang;
use Illuminate\Http\Request;
use App\Models\Options;
use App\Models\Employees;
use App\Models\EmployeesPosition;
use App\Libs\Helpers\LogsHelper;
use App\Providers\LogServiceProvider;
use DB;

class RepresentativeOfficeController extends Controller
{

    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const ARR_OFFICE = 'arrayOffices';
    const PAGINATOR = 'paginator';
    const CURRENT_PAGINATOR = 'current_paginator';
    const POSITION_KEY = 'position';
    const OFFICECODE = 'OFFICES';
    const STATUS_PUBLIC = 1;
    const STATUS_DRAFT = 0;

    protected $areas = ['HN', 'DN', 'HCM'];
    protected $crudTableAction = ['save', 'update'];
    protected $crudTableDelAction = 'del';
    protected $detailBranch = null;
    protected $options = null;
    public static $statusOption = [
        self::STATUS_PUBLIC => 'Hoạt động',
        self::STATUS_DRAFT => 'Không hoạt động'
    ];

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objOffices = Offices::whereIn('option_code', ['HEAD', 'OFFICES'])->whereNull('parent_id')->whereNull('deleted_at')->whereNotNull('status');
        if (!empty($request->input()))
            $objOffices = $this->_objByFliter($request->input(), $objOffices);
        $objOffices = $objOffices->orderBy('created_at', 'des')->get();
        $arrayOffices = $objOffices->toArray();

        // html entity
        $resultArray = array();
        foreach ($arrayOffices as $objOffices) {
            $resultItem = array();
            foreach ($objOffices as $key => $item) {
                $resultItem[$key] = e($item);
            }
            $resultArray[] = $resultItem;
        }

        $i = 1;
        $lastResult = array();
        foreach ($resultArray as $key => $office) {
            if (!empty($office['parent_id']))
                continue;

            // Short Address
            $short_addree = $this->shorten((string)$office['address'], 25);
            $address = $office['address'];
            $str_ddree = '<span class="tooltip" title="' . $address . '">' . $short_addree . '</span>' . $short_addree;
            $office['address2'] = $str_ddree;

            // Short Paymentinfo
            $short_payment_info = $this->shorten((string)$office['payment_info'], 30);
            $payment_info = $office['payment_info'];
            $str_paymentinfo = "<span class=\"tooltip\" title=\"$payment_info\">$short_payment_info</span>$short_payment_info";
            $office['payment_info2'] = $str_paymentinfo;

            // Short Notes
            $short_note = $this->shorten((string)$office['note'], 30);
            $payment_info = $office['note'];
            $str_note = "<span class=\"tooltip\" title=\"$payment_info\">$short_note</span>$short_note";
            $office['note2'] = $str_note;

            $office['action'] = $this->_actionButton($key, $office['id']);
            $office['status2'] = ($office['status'] == 1) ? Lang::get('offices.active') : Lang::get('offices.deactive');
            $office['employee'] = ($office['status'] == 1) ? '<i title="Ban chấp hành VPĐD" data-href="' . url()->route('admin_representative_office_manager', ['id' => $office['id']]) . '" class="openPopupIframe fa fa-plus fa-2x"></i>' : '';
            $office['area2'] = (!empty($office['area'])) ? $this->getOption('area', $office['area']) : '';
            $office['stt'] = $i;
            $i++;

            $lastResult[$key] = $office;

        }

        return view('admin.offices.index', [
            self::ARR_OFFICE => json_encode($lastResult),
            self::PAGINATOR => $limit,
            'statusOption' => self::$statusOption,
            'total' => count($lastResult),
            'name' => (!empty($request->input('name'))) ? $request->input('name') : '',
            'code' => (!empty($request->input('code'))) ? $request->input('code') : '',
            'phone' => (!empty($request->input('phone'))) ? $request->input('phone') : '',
            'email' => (!empty($request->input('email'))) ? $request->input('email') : '',
            'lanhdao' => $this->lanhdao(),
            'area' => $this->getAreasArray()
        ]);
    }

    function getAreasArray()
    {
        $result = [];
        foreach ($this->areas as $area) {
            $result[$area] = $this->getOption('area', $area);
        }
        return $result;
    }

    public function getOption($key, $code)
    {
        $options = DB::table('options')->where('options.code', '=', $code)->get();
        foreach ($options as $option) {
            if ($option->key == $key) return $option->value;
        }
        return null;
    }

    function shorten($str, $maxlen)
    {
        if (strlen($str) <= $maxlen - 3) {
            return $str;
        } else {
            return mb_substr($str, 0, $maxlen - 3, "utf-8") . '...';
        }
    }

    public function manager($id, Request $request)
    {
        if (empty($id)) {
            abort(404);
        }
        $detailBranch = Offices::find($id);
        $status = true;
        if (empty($detailBranch) || $detailBranch->status != \App\Models\Branches::BRANCHES_ACTIVE) {
            $status = false;
        }
        if (!$status && $request->ajax()) {
            return response()->json(['status' => $status]);
        } elseif (!$status && !$request->ajax()) {
            abort(404);
        }
        $listManager = $this->getEmployeesPositionByBranchId($id);
        $listOption = $this->getOptionsByKey(self::POSITION_KEY);
        return view('admin.offices.manager', [
            'detailBranch' => $detailBranch,
            'listManager' => $listManager,
            'listOption' => $listOption
        ]);
    }

    function show($id)
    {
        $model = Offices::find($id);
        $area = $this->getOption('area', $model->area);
        return view('admin.offices.view', [
            'model' => $model,
            'area' => isset($area) ? $area : '',
        ]);
    }

    public function getEmployeesPositionByBranchId($id)
    {
        return EmployeesPosition::where('branch_id', $id)->with(['employees'])->get();
    }

    public function getOptionsByKey($key)
    {
        if (empty($key)) {
            return [];
        }

        return Options::where('key', $key)->pluck('value', 'code')->all();
    }

    public function ajaxSaveMember(Request $request)
    {
        try {
            $saved = $this->saveEmployeesPosition($request->all());
            if (!empty($saved)) {
                // LogServiceProvider::createSystemHistory(trans('history.fn_lead_of_vpdd'), $request->all());
                return response()->json($saved);
            }

            //return response()->json($saved);
        } catch (\Exception $e) {
            LogsHelper::trackByFile('ajaxSaveMemberFail', 'Error when update member payment(Exception: ' . $e->getMessage() . ' on Line ' . $e->getLine() . ' in File ' . $e->getFile() . ')');
        }
    }

    /**
     * @param $id
     *
     * @description lưu ban lãnh đạo
     */
    public function saveEmployeesPosition($attributes)
    {
        // case Create/Update
        if (in_array($attributes['action'], $this->crudTableAction)) {
            if ($this->validateMemberPosition($attributes) !== true) {
                return $this->validateMemberPosition($attributes);
            }

            // case add
            if ($attributes['action'] == 'save') {
                // lưu thông tin người dùng
                $employees = new Employees();
                $employees->fullname = $attributes['l_fullname'];
                if (isset($attributes['l_birthday'])) {
                    $employees->birthday = $attributes['l_birthday'];
                }
                if (isset($attributes['l_phone'])) {
                    $employees->phone = $attributes['l_phone'];
                }
                if (isset($attributes['l_email'])) {
                    $employees->email = $attributes['l_email'];
                }
                $employees->option_code = $attributes['l_position'];
                $employees->created_at = date('Y-m-d H:i:s');
                $employees->updated_at = date('Y-m-d H:i:s');
                if ($employees->save()) {
                    // lưu chức vụ
                    $employeesPosition = new EmployeesPosition();
                    $employeesPosition->branch_id = $attributes['branchesId'];
                    $employeesPosition->employee_id = $employees->id;
                    $employeesPosition->option_code = $attributes['l_position'];
                    $employeesPosition->created_at = date('Y-m-d H:i:s');
                    $employeesPosition->updated_at = date('Y-m-d H:i:s');
                    $employeesPosition->save();
                }
            } elseif ($attributes['action'] == 'update') {
                $employeesPosition = EmployeesPosition::find($attributes['rid']);
                if (empty($employeesPosition)) {
                    return ['error' => $attributes['l_fullname'] . ' không tồn tại hoặc bị xóa'];
                }
                $employeesPosition->option_code = $attributes['l_position'];
                $employeesPosition->updated_at = date('Y-m-d H:i:s');
                $employeesPosition->save();

                $employees = Employees::find($employeesPosition->employee_id);
                if (empty($employees)) {
                    return ['error' => $attributes['l_fullname'] . ' không tồn tại hoặc bị xóa'];
                }
                $arrayUpdated = [
                    'fullname' => $attributes['l_fullname'],
                    'option_code' => $attributes['l_position'],
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $arrayUpdated['birthday'] = isset($attributes['l_birthday']) ? $attributes['l_birthday'] : null;
                $arrayUpdated['phone'] = isset($attributes['l_phone']) ? $attributes['l_phone'] : null;
                $arrayUpdated['email'] = isset($attributes['l_email']) ? $attributes['l_email'] : null;
                $employees = Employees::updateOrCreate([
                    'id' => $employeesPosition->employee_id
                ], $arrayUpdated);
            }
            // array response
            $responses['success'] = 1;
            $responses['l_fullname'] = htmlentities($employees->fullname);
            $responses['l_email'] = !empty($employees->email) ? $employees->email : '';
            $responses['l_phone'] = !empty($employees->phone) ? $employees->phone : '';
            $responses['l_birthday'] = !empty($employees->birthday) ? $employees->birthday : '';
            $responses['l_position'] = $this->options->value;
            $responses['id'] = $employeesPosition->id;
            //LogServiceProvider::createSystemHistory(trans('history.fn_lead_position_of_vpdd'), $responses);
            return $responses;
        } elseif ($attributes['action'] == $this->crudTableDelAction) {
            $employeesPosition = EmployeesPosition::where('id', $attributes['rid'])->with(['employees'])->first();
            // check case delete
            if (empty($employeesPosition)) {
                return ['error' => 'Lỗi dữ liệu!!!!'];
            }
            if ($employeesPosition->employees !== "") {
                $employeesPosition->employees->delete();
                if ($employeesPosition->delete()) {
                    $responses['success'] = 1;
                    $responses['id'] = $employeesPosition->id;

                    return $responses;
                }
            }
        }
        // check case delete

        return null;
    }

    public function validateMemberPosition($attributes)
    {
        if (!isset($attributes['branchesId'])) {
            return ['error' => 'Lỗi dữ liệu'];
        }

        if (!empty($attributes['branchesId'])) {
            $this->detailBranch = Offices::find($attributes['branchesId']);
            if (empty($this->detailBranch)) {
                return ['error' => 'VPĐD không tồn tại hoặc đã xóa'];
            }
        }

        if (!empty($attributes['l_position'])) {
            $this->options = Options::where('code', $attributes['l_position'])->where('key', 'position')->first();
            if (empty($this->options)) {
                return ['error' => 'Chức vụ không tồn tại hoặc đã xóa'];
            }
        }

        return true;
    }

    public function _objByFliter($params, $objOffices)
    {
        $code = $params['code'];
        $name = $params['name'];
        $status = $params['search_status'];
        $area = $params['search_area'];
        $phone = $params['phone'];
        $email = $params['email'];
        if ($code == '' && $name == '' && $status == '' && $area == '' && $phone = '' && $email = '') {
            return $obj = $objOffices;
        }
        if ($code != '') {
            $obj = $objOffices->where('code', 'like', '%' . $code . '%');
        }
        if ($name != '') {
            $obj = $objOffices->where('name', 'like', '%' . $name . '%');
        }
        if ($phone != '') {
            $obj = $objOffices->where('phone', 'like', '%' . $phone . '%');
        }
        if ($email != '') {
            $obj = $objOffices->where('email', 'like', '%' . $email . '%');
        }
        if ($status != '') {
            $obj = $objOffices->where('status', $status);
        }
        if ($area != '') {
            $obj = $objOffices->where('area', 'like', '%' . $area . '%');
        }

        return empty($obj) ? $objOffices : $obj;
    }

    public function _actionButton($key, $id = null)
    {
        $url = "'" . './representative_office/view/' . $id . "'";
        $action = '<a title="Xem VPĐD" atl="Xem VPĐD" onclick="viewmodal(' . $url . ')">
                                <i class="fa fa-eye" aria-hidden="true" style="font-size: 18px;"></i>
                            </a>';
        $action .= " <a class=\"btn-function edit-modal\" 
                        data-toggle=\"tooltip\" 
                        data-placement=\"left\" 
                        title=\"Sửa VPĐD\" 
                        atl=\"Sửa VPĐD\"
                        onclick=\"editOffices($key)\"
                        >
                                <i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\" style=\"font-size: 18px; color: #ff6600;\"></i>
                            </a>
                            ";
        $action .= " <a class=\"btn-function\" 
                        data-toggle=\"tooltip\" 
                        data-placement=\"left\" 
                        title=\"Xóa VPĐD\" 
                        atl=\"Xóa VPĐD\"
                        onclick=\"deleteOffices($key)\"
                        >
                                <i class=\"fa fa-trash-o\" aria-hidden=\"true\" style=\"font-size: 18px;\"></i>
                            </a>
                            ";
        return $action;
    }

    public function lanhdao()
    {
        $objUser = User::where([
            ['role', '=', \App\Constants\UserConstants::HN_LEADER_USER]
        ])->orWhere(
            'role', '=', \App\Constants\UserConstants::DN_LEADER_USER
        )->orWhere(
            'role', '=', \App\Constants\UserConstants::HCM_LEADER_USER
        )->get();
        return $objUser;
    }

    public function update(Request $request)
    {
        $params = $request->input();
        $id = $params['id'];
        unset($params['id']);
        $mesSuccess = 'Cập nhật văn phòng đại diện thành công';
        $errorMsg = 'Cập nhật văn phòng đại diện không thành công';

        if ($id == -1 || $id == null) {
            $objOffices = new Offices;
            $objOfficesByCode = Offices::where('code', $params['code'])->whereNull('deleted_at')->get()->toArray();
            $mesSuccess = 'Thêm mới văn phòng đại diện thành công';
            $errorMsg = 'Thêm mới văn phòng đại diện thất bại';
            if (is_array($objOfficesByCode) && count($objOfficesByCode) > 0)
                return redirect()->route('admin_representative_office_view')->with('error', 'Thêm mới VPĐD không thành công, mã văn phòng đại diện đã tồn tại');
        } else $objOffices = Offices::find($id);
        $oldImage = $objOffices->images;
        $objOffices->fill($params);
        $objOffices->option_code = 'OFFICES';

        if (!empty($params['main_office'])) {
            $objOffices->option_code = 'HEAD';
            $objOfficesByOptionCode = Offices::where([
                ['option_code', 'like', 'HEAD']
            ])->get()->toArray();
            if (is_array($objOfficesByOptionCode) && count($objOfficesByOptionCode) > 0) {
                if ($objOfficesByOptionCode[0]['id'] != $id)
                    return redirect()->route('admin_representative_office_view')->with('error', 'Thêm mới VPĐD không thành công. Đã tồn tại thông tin của Hội');
            }
        }

        if (empty($params['status']) && $id != -1 && !empty($id)) {
            $objOfficesByParent = Offices::where('parent_id', $id)->whereNull('deleted_at')->get()->toArray();
            if (is_array($objOfficesByParent) && count($objOfficesByParent) > 0)
                return redirect()->route('admin_representative_office_view')->with('error', 'VPĐD đang hoạt động, không được cập nhật trạng thái');

            $objOfficesByEmployees = DB::table('employees')
                ->join('employee_position', 'employee_position.employee_id', '=', 'employees.id')
                ->where('employee_position.branch_id', $id)
                ->whereNull('employees.deleted_at')->get()->toArray();
            if (is_array($objOfficesByEmployees) && count($objOfficesByEmployees) > 0)
                return redirect()->route('admin_representative_office_view')->with('error', 'VPĐD đang hoạt động, không được cập nhật trạng thái');
        }


        try {
            if (!empty($_FILES["images"]['name']) && $images = $this->_uploadImages()) {
                $objOffices->images = $images;
            } else $objOffices->images = $oldImage;
            $objOffices->save();
            // if ($id == -1 || $id == null) {
            //     LogServiceProvider::createSystemHistory(trans('history.fn_create_vpdd'), $objOffices);
            // } else {
            //     LogServiceProvider::createSystemHistory(trans('history.fn_save_vpdd'), $objOffices);
            // }
            return redirect()->route('admin_representative_office_view')->with('success_message', $mesSuccess);
        } catch (\Exception $e) {
            return redirect()->route('admin_representative_office_view')->with('error', $errorMsg);
        }
    }

    public function ajaxCheckValidAddNewOffice(Request $request) {
        $params = $request->input();
        $responses = [];
        $id = $params['id'];
        unset($params['id']);

        if ($id == -1 || $id == null) {
            $objOfficesByCode = Offices::where('code', $params['code'])->whereNull('deleted_at')->get()->toArray();
            if (is_array($objOfficesByCode) && count($objOfficesByCode) > 0) {
                // return redirect()->route('admin_representative_office_view')->with('error', 'Thêm mới VPĐD không thành công, mã văn phòng đại diện đã tồn tại');
                $responses['error'] = 1;
                $responses['column'] = "field1";
                return $responses;
            }
            
        }


        if (!empty($params['main_office'])) {
            $objOfficesByOptionCode = Offices::where([
                ['option_code', 'like', 'HEAD']
            ])->get()->toArray();
            if (is_array($objOfficesByOptionCode) && count($objOfficesByOptionCode) > 0) {
                if ($objOfficesByOptionCode[0]['id'] != $id) {
                    // return redirect()->route('admin_representative_office_view')->with('error', 'Thêm mới VPĐD không thành công. Đã tồn tại thông tin của Hội');
                    $responses['error'] = 2;
                    $responses['column'] = "main_office";
                    return $responses;
                }
            }
        }

        $responses['success'] = 1;
        return $responses;
        
    }

    public function _uploadImages()
    {
        $errors = array();
        $uploadedFiles = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        $bytes = 1024;
        $KB = 1024;
        $totalBytes = 1000 * $bytes * $KB;
        $dirName = date('YmdHis', time());
        $UploadFolder = public_path('files/offices');
        if (!file_exists($UploadFolder)) {
            mkdir($UploadFolder, 0755, true);
        }
        $counter = 0;
        $temp = $_FILES["images"]["tmp_name"];
        $name = $_FILES["images"]["name"];
        $UploadOk = true;
        if ($_FILES["images"]["size"] > $totalBytes) {
            $UploadOk = false;
            array_push($errors, $name . " file upload phải nhỏ hơn 10 MB.");
        }
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        if (in_array(strtolower($ext), $extension) == false) {
            $UploadOk = false;
            array_push($errors, $name . " chỉ cho phép upload các file ảnh jpeg, jpg, png, gif.");
        }
        if (file_exists($UploadFolder . "/" . $name) == true) {
            $UploadOk = false;
            array_push($errors, $name . " file này đã tồn tại.");
        }
        $nameS = "/offices_" . $dirName . $name;
        if ($UploadOk == true) {
            move_uploaded_file($temp, $UploadFolder . $nameS);
        }
        if ($counter > 0) {
            if (count($errors) > 0) {
                return false;
            }
            return $nameS;
        } else {
            return $nameS;
        }
    }

    public function destroy(Request $request)
    {
        $params = $request->input();
        $id = $params['id'];
        if (empty($id)) {
            abort(404);
        }
        $objOffice = Offices::where('id', $id)->whereNull('deleted_at')->first(); // File::find($id)
        if ($objOffice->status == 1) return redirect()->route('admin_representative_office_view')->with('error', Lang::get('offices.undeletecompletewithstatusenable'));
        $objOfficesByParent = Offices::where('parent_id', $id)->whereNull('deleted_at')->get()->toArray(); // File::find($id)
        if (is_array($objOfficesByParent) && count($objOfficesByParent) > 0) return redirect()->route('admin_representative_office_view')->with('error', 'Không thể xóa VPĐD đang được sử dụng.');
        try {
            $date = date("Y-m-d");
            $objOffice->deleted_at = $date;
            $objOffice->save();
            //LogServiceProvider::createSystemHistory(trans('history.fn_delete_vpdd'), $objOffice);
            return redirect()->route('admin_representative_office_view')->with('success_message', Lang::get('offices.deletecomplete'));
        } catch (\Exception $e) {
            return redirect()->route('admin_representative_office_view')->with('error', Lang::get('offices.undeletecomplete'));
        }
    }


}
