<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Banner;
use App\Providers\LogServiceProvider;
use App\Providers\BannerServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\UserConstants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Page;
use App\Constants\MemberConstants;
use App\Libs\Helpers\Utils;


class BannerController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const SEARCH_INPUT = 'search';
    const OBJ_NEWS = 'objNews';
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';

    const page_CODE = 'page_code';
    const CONTENT = 'content';
    const STATUS = 'status';
    const START_TIME = 'start_time';
    const END_TIME = 'end_time';
    const COUNT_NEW= 'count_news';
    const CURRENT_PAGINATOR = 'current_paginator';
    const pageS = 'pages';

    const BANNER_INDEX = 'banners.index';
    const BANNER_SHOW = 'banners.show';
    const SUCCESSES = 'successes';

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objNews = $this->_searchByCondition($request, $limit,false);
        $sumObjNews = $this->_searchByCondition($request, $limit,true);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }
        $pages = BannerServiceProvider::getAllPage();
//        print_r(json_decode(json_encode($pages)));die;
        return view('admin.banner.index', [
            self::OBJ_NEWS          => $objNews,
            'admin_user'            => UserConstants::ADMIN_USER,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_NEW         => count($sumObjNews),
            'pages'                 => $pages,
            self::PAGINATOR         => (count($sumObjNews) > 0) ? $objNews->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input('name'))) {
            $name = $request->input('name');
        } else {
            $name = '';
        }

        if (!empty($request->input('page_code'))) {
            $page_code = $request->input('page_code');
        } else {
            $page_code = '';
        }
        if (!empty($request->input('status'))) {
            $status = $request->input('status');
        } else {
            $status = '';
        }

        if (!empty($request->input('start_time'))) {
            $start_time = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input('start_time', ''))->toDateString();
        } else {
            $start_time = '';
        }

        if (!empty($request->input('end_time'))) {
            $end_time = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input('end_time', ''))->toDateString();
        } else {
            $end_time = '';
        }

        if (!empty($request->input('created_at'))) {
            $created_at = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input('created_at', ''))->toDateString();
        } else {
            $created_at = '';
        }

        $arrConditions = [
            'name'          => $name,
            'page_code'     => $page_code,
            'status'        => $status,
            'start_time'    => $start_time,
            'end_time'      => $end_time,
            'created_at'    => $created_at,
        ];

        return BannerServiceProvider::newsSearchByConditions($arrConditions, $limit,$isCount);
    }

    public function createBanner (Request $request)
    {
        $objData = [];
        $objData['name'] = $request->input('name_add');
        if(!empty($request->input('start_time')))  $objData['start_time']      = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input('start_time'))));
        if(!empty($request->input('end_time')))    $objData['end_time']        = date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input('end_time'))));
        $objData['page_code'] = $request->input('page_code');
        $objData['status'] = $request->input('status');
        $objData['content'] = $request->input('content');
        $objData['content2'] = $request->input('content2');
        if (!empty($_FILES["profile_image"])) {
            $objData['profile_image'] = $this->_uploadImages();
        }
        $objSave = Banner::create($objData);
        if (empty($objSave)) {
            return redirect()->route($this::BANNER_INDEX)->withErrors(['Tạo mới Hình Ảnh thất bại']);
        } else {
            // LogServiceProvider::createSystemHistory(trans('history.fn_create_banner'), $objData);
            return redirect()->route($this::BANNER_INDEX)->with($this::SUCCESSES, ['Tạo mới Hình Ảnh thành công']);
        }

    }
    public function updateBanner (Request $request)
    {
        $id = $request->input('id_edit');
        if (empty($id)) {
            $objData = null;
        } else {
            $objData = BannerServiceProvider::getDataById($id);
        }
        if (empty($objData)) {
            return redirect()->route($this::BANNER_INDEX)->withErrors(['Hình Ảnh không tồn tại hoặc đã xóa khỏi hệ thống']);
        }
        $objData = [];

        $objData['name'] = $request->input('name_edit');
        $objData['start_time']      = !empty($request->input('start_time')) ? date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input('start_time')))) : null;
        $objData['end_time']        = !empty($request->input('end_time')) ? date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input('end_time')))) : null;
        $objData['page_code'] = $request->input('page_code');
        if($request->input('status') > 0) $objData['status'] = $request->input('status');
        $objData['content'] = $request->input('content');
        $objData['content2'] = $request->input('content2');
        $img_old = $request->input('img_old');
        $img_new = '';
        if (!empty($_FILES["profile_image"])) {
            if($_FILES["profile_image"]['tmp_name'] != ''){
                $img_new = $this->_uploadImages();
            }
        }
        if($img_new != '' && $img_new != $img_old){
            $objData['profile_image'] = $img_new;
            if (file_exists($_SERVER['DOCUMENT_ROOT'].'/images/banners/'.$img_old)) unlink($_SERVER['DOCUMENT_ROOT'].'/images/banners/'.$img_old); // delete file image old
        }
        $objSave = Banner::where('id', $id)->update($objData);
        if (empty($objSave)) {
            return redirect()->route($this::BANNER_INDEX)->withErrors(['Cập nhật Hình Ảnh thất bại']);
        } else {
            // LogServiceProvider::createSystemHistory(trans('history.fn_save_banner'), $objData);
            return redirect()->route($this::BANNER_INDEX)->with($this::SUCCESSES, ['Cập nhật Hình Ảnh thành công']);
        }

    }
    public function show ($id, Request $request)
    {
        if (empty($id)) {
            $objData = null;
        } else {
            $objData = BannerServiceProvider::getDataById($id);
        }

        if (empty($objData)) {
            $request->session()->flash('error_ajax', ['Hình Ảnh không tồn tại hoặc đã xóa khỏi hệ thống']);
            return response()->json(['deleted' => 'error']);
        }
        if($objData['start_time'] != ''){
            $objData['start_time'] = date('d/m/Y', strtotime(substr($objData['start_time'], 0,10)));
        }
        if($objData['end_time'] != ''){
            $objData['end_time'] = date('d/m/Y', strtotime(substr($objData['end_time'], 0,10)));
        }
        $objPages = BannerServiceProvider::getAllPage();
        return response()->json(['objData' => $objData, 'objPages' => $objPages]);
    }

    public function deleteBanner ($id)
    {
        $objIntro = BannerServiceProvider::getDataById($id);
        if (empty($objIntro)) {
            return redirect()->route($this::BANNER_INDEX)->withErrors(['Hình Ảnh không tồn tại hoặc đã xóa khỏi hệ thống']);
        }else{
            if($objIntro['status'] == 2) return redirect()->route($this::BANNER_INDEX)->withErrors(['Bạn không được xóa Hình Ảnh đang hoạt động']);
        }

        $objSave = BannerServiceProvider::delete($id);
        if(empty($objSave)){
            return redirect()->route($this::BANNER_INDEX)->withErrors(['Xóa Hình Ảnh thất bại']);
        } else {
            // LogServiceProvider::createSystemHistory(trans('history.fn_delete_banner'), $objSave);
            return redirect()->route($this::BANNER_INDEX)->with($this::SUCCESSES, ['Xóa Hình Ảnh thành công']);
        }
    }

    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }
    public function _uploadImages()
    {
        $errors = array();
        $uploadedFiles = array();
        $extension = array("jpeg", "jpg", "png", "gif", 'bmp');
        $bytes = 1024;
        $KB = 1024;
        $totalBytes = 10 * $bytes * $KB;
        $file_name = date('YmdHis', time());
        $patchUpload = public_path('images').'/banners/';
        if (!file_exists($patchUpload)) {
            mkdir($patchUpload, 0755, true);
        }
        $temp = $_FILES["profile_image"]["tmp_name"];
        $name = $_FILES["profile_image"]["name"];
        $UploadOk = true;
        if ($_FILES["profile_image"]["size"] > $totalBytes) {
            $UploadOk = false;
            array_push($errors, $name . " file size is larger than the 10 MB.");
        }
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        if (in_array(strtolower($ext), $extension) == false) {
            $UploadOk = false;
            array_push($errors, $name . " is invalid file type.");
        }
        /*if (file_exists($patchUpload . $name) == true) {
            $UploadOk = false;
            array_push($errors, $name . " file is already exist.");
        }*/
        $strName = '';
        if($name != ''){
            $arr = explode('.',$name);
            $strName = $arr[count($arr)-1];
        }
        $name_result = "banners_" . $file_name .'_'. Utils::randomString(6).'.'.$strName;
        if ($UploadOk == true) {
            move_uploaded_file($temp, $patchUpload . $name_result);
        }

        return $name_result;
    }
}
