<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\NewOption;
use App\Providers\IntroductionServiceProvider;
use App\Providers\LogServiceProvider;
use App\Constants\CommontConstants;
use App\Constants\UserConstants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class IntroductionController extends Controller
{
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const SEARCH_INPUT = 'search';
    const OBJ_NEWS = 'objNews';
    const RESPONSE_VIEW = 'response_view';
    const PAGINATOR = 'paginator';

    const TITLE = 'title';
    const OPTION_CODE = 'option_code';
    const CONTENT = 'content';
    const STATUS = 'status';
    const START_TIME = 'start_time';
    const END_TIME = 'end_time';
    const COUNT_NEW= 'count_news';
    const CURRENT_PAGINATOR = 'current_paginator';
    const OPTIONS = 'options';
    const STATUS_PUBLIC = 1;
    const STATUS_DRAFT = 0;

    const INTRODUCTION_INDEX = 'introductions.index';
    const INTRODUCTION_SHOW = 'introductions.show';
    const SUCCESSES = 'successes';
    public static $statusOption = [
        self::STATUS_PUBLIC => 'Hoạt động',
        self::STATUS_DRAFT => 'Khóa'
    ];

    public function index(Request $request)
    {
        $limit = self::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $objNews = $this->_searchByCondition($request, $limit,false);
        $sumObjNews = $this->_searchByCondition($request, $limit,true);
        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        }else {
            $current_page = ($current_page -1 ) * 10;
        }
    
        $options = IntroductionServiceProvider::getAllOption(CommontConstants::INTRODUCTION); 
        return view('admin.introduction.index', [
            self::OBJ_NEWS          => $objNews,
            'admin_user'            => UserConstants::ADMIN_USER,
            'statusOption' => self::$statusOption,
            self::RESPONSE_VIEW     => $request,
            self::COUNT_NEW         => count($sumObjNews),
            self::OPTIONS           => $options,
            self::PAGINATOR         => (count($sumObjNews) > 0) ? $objNews->appends(request()->except('page')) : null,
            self::CURRENT_PAGINATOR => $current_page,
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(self::TITLE))) {
            $title = $request->input(self::TITLE);
        } else {
            $title = '';
        }

        if (!empty($request->input(self::OPTION_CODE))) {
            $option_code = [$request->input(self::OPTION_CODE)];
        } else {
            $option_code = ['INTRODUC01', 'INTRODUC02', 'INTRODUC03', 'INTRODUC04', 'INTRODUC05', 'INTRODUC06', 'INTRODUC07', 'INTRODUC08', 'INTRODUC09', 'INTRODUC10'];
        }

        if ($request->input('search_status') != '') {
            $status = $request->input('search_status');
        } else {
            $status = '';
        }

        if (!empty($request->input('created_at'))) {
            $created_at = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input('created_at', ''))->toDateString();
        } else {
            $created_at = '';
        }

        $arrConditions = [
            self::TITLE         => $title,
            self::OPTION_CODE   => $option_code,
            'status'            => $status,
            'created_at'        => $created_at
        ];

        return IntroductionServiceProvider::newsSearchByConditions($arrConditions, $limit,$isCount);
    }
    public function show ($id)
    {

        if (empty($id)) {
            $objIntro = null;
        } else {
            $objIntro = IntroductionServiceProvider::getNewById($id);
        }

        if (empty($objIntro)) {
            abort(404);
        }
        $arrOptions = IntroductionServiceProvider::getAllOption(CommontConstants::INTRODUCTION);
        return view('admin.introduction.edit', [
            'objIntro' => $objIntro,
            'arrOptions' => $arrOptions,
        ]);
    }

    public function detail ($id)
    {

        if (empty($id)) {
            $objIntro = null;
        } else {
            $objIntro = IntroductionServiceProvider::getNewById($id);
        }

        if (empty($objIntro)) {
            return redirect()->route($this::INTRODUCTION_INDEX)->withErrors(['Bài giới thiệu không tồn tại hoặc đã xóa khỏi hệ thống']);
        }
        $arrOptions = IntroductionServiceProvider::getAllOption(CommontConstants::INTRODUCTION);
        return view('admin.introduction.detail', [
            'objIntro' => $objIntro,
            'arrOptions' => $arrOptions,
        ]);
    }
    public function destroy ($id)
    {
        if (empty($id)) {
            abort(404);
        }
        $objIntro = IntroductionServiceProvider::getNewById($id);
        if (empty($objIntro)) {
            return redirect()->route($this::INTRODUCTION_INDEX)->withErrors(['Bài giới thiệu không tồn tại hoặc đã xóa khỏi hệ thống']);
        }else{
            if($objIntro->status == 1){
                return redirect()->route($this::INTRODUCTION_INDEX)->withErrors(['Bạn không được phép xóa bài giới thiệu đang hoạt động']);
            }else{
                $objNew = IntroductionServiceProvider::delete($id);

                if (empty($objNew)) {
                    return redirect()->route($this::INTRODUCTION_INDEX)->withErrors(['Xóa bài giới thiệu thất bại']);
                } else {
                    // LogServiceProvider::createSystemHistory(trans('history.fn_delete_introduction'), [
                    //     'introductionId' => $id,
                    //     'introduction' => $objNew
                    // ]);
                    return redirect()->route($this::INTRODUCTION_INDEX)->with($this::SUCCESSES, ['Xóa bài giới thiệu thành công']);
                }
            }
        }
    }
    public function create ()
    {
        $arrOptions = IntroductionServiceProvider::getAllOption(CommontConstants::INTRODUCTION);
        return view('admin.introduction.register', [
            'arrOptions' => $arrOptions,
        ]);
    }

    public function store (Request $request)
    {

        $validator = Validator::make($request->all(), NewOption::$rule, NewOption::$message);

        $validator->setAttributeNames(NewOption::$niceAttributeName);

        if ($validator->fails()) {
            return redirect()
                ->route('introductions.create')
                ->withErrors($validator)
                ->withInput();
        }
        $arrData = [
            $this::OPTION_CODE      => $request->input($this::OPTION_CODE),
            $this::TITLE            => $request->input($this::TITLE),
            $this::STATUS           => $request->input($this::STATUS),
            $this::CONTENT          => $request->input($this::CONTENT),
        ];
        $arrData[$this::START_TIME]     = !empty($request->input($this::START_TIME)) ? date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input($this::START_TIME)))) : date('Y-m-d H:i:s');
        $arrData[$this::END_TIME]       = !empty($request->input($this::END_TIME)) ? date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input($this::END_TIME)))) : null;
        $objNewOption = NewOption::create($arrData);

        if (empty($objNewOption)) {
            return redirect()->route('introductions.create')->withErrors(['Tạo mới bài giới thiệu thất bại']);
        } else {
            // LogServiceProvider::createSystemHistory(trans('history.fn_create_introduction'), [
            //     'introduction' => $objNewOption
            // ]);
            return redirect()->route($this::INTRODUCTION_INDEX)->with($this::SUCCESSES, ['Tạo mới bài giới thiệu thành công']);
        }
    }
    public function checkStatus ($id, $status, $option_code, Request $request)
    {
        $data = IntroductionServiceProvider::getNewByOptionCode($id, $status, $option_code);

        if ($data == false) {
            return response()->json(['error' => 'error', 'content' => 'Chuyên mục này có bài viết đang hoạt động. Vui lòng khóa bài viết đó trước.']);
        }
        return response()->json(['error' => '']);
    }

    public function update (Request $request, $id)
    {
        if (empty($id)) {
            abort(404);
        }

        $objNew = IntroductionServiceProvider::getNewById($id);
        if (empty($objNew)) {
            return redirect()->route($this::INTRODUCTION_INDEX)->withErrors(['Bài giới thiệu không tồn tại hoặc đã xóa khỏi hệ thống']);
        }

        $validator = Validator::make($request->all(), NewOption::$rule, NewOption::$message);
        $validator->setAttributeNames(NewOption::$niceAttributeName);

        if ($validator->fails()) {
            return redirect()
                ->route($this::INTRODUCTION_SHOW, [$id])
                ->withErrors($validator)
                ->withInput();
        }
        $arrData = [
            'id'                    => $id,
            $this::OPTION_CODE      => $request->input($this::OPTION_CODE),
            $this::TITLE            => $request->input($this::TITLE),
            $this::STATUS           => $request->input($this::STATUS),
            $this::CONTENT          => $request->input($this::CONTENT),
        ];
        if(!empty($request->input($this::START_TIME))) $arrData[$this::START_TIME]     =  date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input($this::START_TIME))));
        $arrData[$this::END_TIME]       = !empty($request->input($this::END_TIME)) ? date('Y-m-d H:i:s', strtotime(str_replace('/','-',$request->input($this::END_TIME)))) : null;
        $objNew = IntroductionServiceProvider::update($arrData);
        
        if (empty($objNew)) {
            return redirect()->route($this::INTRODUCTION_INDEX)->withErrors(['Cập nhật bài giới thiệu thất bại']);
        } else {
            // LogServiceProvider::createSystemHistory(trans('history.fn_save_introduction'), [
            //     'introduction' => $objNew
            // ]);
            return redirect()->route($this::INTRODUCTION_INDEX)->with($this::SUCCESSES, ['Cập nhật bài giới thiệu thành công']);
        }
    }



    private function _checkRuleAndRequestUpdate ($requests, $objData, $arrRule)
    {
        foreach ($requests->all() as $key => $request) {
            if ($request == $objData->{$key} || $request == '') {
                if (array_key_exists($key, $arrRule)) {
                    unset($arrRule[$key]);
                }
                $requests->merge([$key => null]);
            }
        }

        return [
            $requests,
            $arrRule
        ];
    }
}
