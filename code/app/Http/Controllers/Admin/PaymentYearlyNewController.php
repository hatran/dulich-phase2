<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Constants\CommontConstants;
use App\Providers\MemberPaymentsYearlyServiceProvider;
use App\Providers\MemberServiceProvider;
use App\Models\Offices;
use App\Libs\Helpers\Utils;

class PaymentYearlyNewController extends Controller {

	const NUMBER_PAYMENT = 'number_payment';
	const FROM_YEAR = 'from-year';
    const TO_YEAR = 'to-year';
    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const STATUS = 'status';
    const FLAG = 'flag';
    const FILE_CODE = 'file_code';

	public function searchPaymentFirst(Request $request) {
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;
        $query_string = $_SERVER['QUERY_STRING'] ?? '';
        $objMembers = $this->_searchByConditionPaymentFirst($request, $limit);
        $year = Utils::getYears();
        $currentYear = date('Y');
        return view('admin.payment_yearly_new.payment_first', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::COUNT_MEMBER => $objMembers->total(),
            CommontConstants::PAGINATOR => !empty($objMembers) ? $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $this->getCurrentCursor($limit, $request),
            'offices' => Offices::searchByAllOffices(),
            'query_string' => $query_string,
            'year' => $year,
            'currentYear' => $currentYear
        ]);
	}

	public function searchPaymentYearly(Request $request) {
        $fromYearSearch = $request->input(self::FROM_YEAR, '');
        $toYearSearch = $request->input(self::TO_YEAR, '');
        $year = Utils::getYears();

        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;

        $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
        $currentYear = date('Y');
        $objMembers = $this->_searchByConditionPaymentYearly($request, $limit);
// dd($objMembers);
        return view('admin.payment_yearly_new.payment_yearly', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::ALL_SEARCH => $this->searchByAllArea(),
            CommontConstants::COUNT_MEMBER => $objMembers->total(),
            CommontConstants::PAGINATOR => !empty($objMembers) ? $objMembers->appends(request()->except('page')) : null,
            CommontConstants::CURRENT_PAGINATOR => $this->getCurrentCursor($limit, $request),
            'offices' => Offices::searchByAllOffices(),
            'year' => $year,
            'fromYearSearch' => $fromYearSearch,
            'toYearSearch' => $toYearSearch,
            'query_string' => $query_string,
            'currentYear' => $currentYear
        ]);
	}

	private function _searchByConditionPaymentFirst ($request, $limit)
    {
        $arrConditions = [
            self::NUMBER_PAYMENT                    => $request->input(self::NUMBER_PAYMENT, ''),
            CommontConstants::FULL_NAME             => $request->input(CommontConstants::FULL_NAME),
            CommontConstants::TOURIST_GUIDE_CODE    => $request->input(CommontConstants::TOURIST_GUIDE_CODE),
            CommontConstants::PROVINCE_CODE         => $request->input(CommontConstants::PROVINCE_CODE),
            CommontConstants::FILE_CODE             => $request->input(CommontConstants::FILE_CODE)
        ];

        return MemberPaymentsYearlyServiceProvider::searchMemberByConditionsPaymentFirst($arrConditions, $limit);
    }

    private function _searchByConditionPaymentYearly ($request, $limit)
    {
        $arrConditions = [
            self::NUMBER_PAYMENT                    => $request->input(self::NUMBER_PAYMENT, ''),
            CommontConstants::FULL_NAME             => $request->input(CommontConstants::FULL_NAME),
            CommontConstants::TOURIST_GUIDE_CODE    => $request->input(CommontConstants::TOURIST_GUIDE_CODE),
            CommontConstants::MEMBER_CODE           => $request->input(CommontConstants::MEMBER_CODE),
            CommontConstants::STATUS                => $request->input(CommontConstants::STATUS),
            CommontConstants::PROVINCE_CODE         => $request->input(CommontConstants::PROVINCE_CODE),
           self::FROM_YEAR                         => $request->input(self::FROM_YEAR, ''),
           self::TO_YEAR                           => $request->input(self::TO_YEAR, ''),
            self::TO_DATE                           => $request->input(self::TO_DATE, ''),
            self::FROM_DATE                         => $request->input(self::FROM_DATE, ''),
            self::STATUS                            => $request->input(self::STATUS),
            self::FLAG                            => $request->input(self::FLAG),
            self::FILE_CODE                         => $request->input(self::FILE_CODE),
        ];

        // dd($arrConditions);

        return MemberPaymentsYearlyServiceProvider::searchMemberByConditionsPaymentYearly($arrConditions, $limit);
    }

    public function destroy($id) {
        if (empty($id)) {
            return response()->json(['errors' => ['memberInvalid' => 'Xóa hội phí thường niên thất bại.']]);
        } else {
            $objMemberPaymentYearly = MemberPaymentsYearlyServiceProvider::getMemberPaymentById($id);
            if (!empty($objMemberPaymentYearly) && !empty($objMemberPaymentYearly->memberId)) {
                $objMember = MemberServiceProvider::getMemberById($objMemberPaymentYearly->memberId);
            }
        }

        if (empty($objMember) && empty($objMemberPaymentYearly)) {
            return response()->json(['errors' => ['memberInvalid' => 'Xóa hội phí thường niên thất bại.']]);
        }

        $isDelete = MemberPaymentsYearlyServiceProvider::deletePaymentYearlyNew($id, $objMember->status);

        if (empty($isDelete)) {
            return response()->json(['errors' => ['memberInvalid' => 'Xóa hội phí thường niên thất bại.']]);
        } else {
            return response()->json(['success' => 'Xoá hội phí thường niên thành công']);
        }
    }
}