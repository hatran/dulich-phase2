<?php

namespace App\Http\Controllers\Admin;

use App\Constants\MemberConstants;
use App\Constants\CommontConstants;
use App\Http\Controllers\Controller;
use App\Providers\LeaderSigningDecisionProvider;
use App\Providers\MemberDecisionServiceProvider;
use App\Providers\MemberServiceProvider;
use App\Providers\UserServiceProvider;
use App\Providers\LogServiceProvider;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mail;
use Carbon\Carbon;
use Response;
use Validator;
use App\Models\Offices;
use App\Models\Option;
use App\Models\Branches;
use App\Providers\ListMemberServiceProvider;
use App\Models\MajorSkill;
use App\Models\Major;
use App\Providers\MemberPaymentsYearlyServiceProvider;
use App\Providers\CodeServiceProvider;
use App\Models\OtherConstant;
use App\Models\WorkConstant;
use App\Models\Code;
use App\Models\MemberDecision;
use App\Models\Member;
use App\Models\MemberRank;
use App\Models\Rank;
use App\Models\LeaderSigningDecision;
use Illuminate\Validation\Rule;

class MemberDecisionController extends Controller
{
    const OBJ_LEADER_SIGN = 'objLeaders';
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const MEMBER_ID = 'memberId';
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const GENDER = 'gender';
    const BIRTHDAY = 'birthday';
    const DOB = 'bod';
    const BIRTH_YEAR = 'birthYear';
    const BIRTH_MONTH = 'birthMonth';
    const BIRTH_DATE = 'birthDate';
    const CMT_CCCD = 'cmtCccd';
    const ISSUED_BY = 'issuedBy';
    const PERMANENT_ADDRESS = 'permanentAddress';
    const ADDRESS = 'address';
    const FIRST_MOBILE = 'firstMobile';
    const SECOND_MOBILE = 'secondMobile';
    const FIRST_EMAIL = 'firstEmail';
    const SECOND_EMAIL = 'secondEmail';
    const EXPERIENCE_YEAR = 'experienceYear';
    const INBOUND_OUTBOUND = 'inboundOutbound';
    const EXPERIENCE_LEVEL = 'experienceLevel';
    const OTHER_SKILLS = 'otherSkills';
    const OTHER_INFORMATION = 'otherInformation';
    const TOURIST_GUIDE_LEVEL = 'touristGuideLevel';
    const ACHIEVEMENTS = 'achievements';
    const STATUS = 'status';
    const PROVINCE_CODE = 'province_code';
    const IS_VERIFIED = 'is_verified';
    const IS_FEE = 'is_fee';
    const IS_SIGNED = 'is_signed';
    const MEMBER_TYPE = 'member_type';
    const GUIDE_LANGUAGE = 'guideLanguage';
    const EDUCATIONS = 'educations';
    const BRANCH_ID = 'branchId';
    const EDUCATION_BRANCHES = 'educationBranches';
    const DEGREE_ID = 'degreeId';
    const EDUCATION_DEGREES = 'educationDegrees';
    const FILE = 'file';
    const EDUCATION_FILES = 'educationFiles';
    const LANGUAGE = 'language';
    const LEVEL2 = 'level2';
    const LANGUAGE_ID = 'languageId';
    const LEVEL_ID = 'levelId';
    const MAJOR_SKILLS = 'majorSkills';
    const IDS = 'ids';
    const MAJORS = 'majors';
    const FILES = 'files';
    const MAJOR_FILES = 'majorFiles';
    const LANGUAGE_SKILLS = 'languageSkills';
    const LANGUAGES = 'languages';
    const LANGUAGE_LEVELS = 'languageLevels';
    const ELEMENTS = 'elements';
    const WORK_HISTORY = 'workHistory';
    const WORK_ELEMENT_NAMES = 'workElementNames';
    const WORK_TYPE_OF_CONTRACTS = 'workTypeOfContracts';
    const WORK_FROM_DATES = 'workFromDates';
    const WORK_TO_DATES = 'workToDates';
    const ELEMENT_NAME = 'elementName';
    const TYPE_OF_CONTRACT = 'typeOfContract';
    const FROM_DATE = 'fromDate';
    const TO_DATE = 'toDate';
    const GROUP_SIZE_ID = 'groupSizeId';
    const GROUP_SIZES = 'groupSizes';
    const TYPE_GUIDE_ID = 'typeGuideId';
    const TYPE_GUIDES = 'typeGuides';
    const AVATAR = 'avatar';
    const USER_PHOTO = 'userPhoto';
    const FILE_NAME = 'fileName';
    const ACHIEVEMENT_FILES = 'achievementFiles';
    const TYPE_OF_PLACE = 'typeOfPlace';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EXPIRATION_DATE = 'expirationDate';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const DATE_ISSUED = 'dateIssued';
    const COOKIE_REGISTER_TOKEN = 'regis_token';
    const CLUB = 'club';
    const OBJ_MEMBER = 'objMember';
    const OBJ_MEM = 'objMem';
    const MEMBER_BELONGS_TO_CHI_HOI = 1;
    const MEMBER_BELONGS_TO_HOI = 2;
    const LOCAL_TRAVEL_GUIDE = 3;
    const PROVINCE = 'province';
    const ACCEPTED = 'acceptTermsAndPolicies';
    const DELETED = 'is_delete';
    const SUCCESSES = 'successes';
    const MAJORS_SKILLS = 'objMajorSkills';
    const OBJ_MAJORS = 'objMajor';
    const OBJ_LANGUAGE = 'objLanguage';
    const OBJ_LANGUAGE_LEVEL = 'objLanguageLevel';
    const WORKHISTORY = 'workHistories';
    const TYPE_GUIDE_NAME = 'typeGuideName';
    const GROUP_SIZE_NAME = 'groupSizeName';
    const BRAND = 'objBrand';
    const DEGREE = 'objDegree';
    const OBJ_PAYMENT = 'paymentInfor';
    const OBJ_NOTES = 'notes';
    const POSITION = 'position';
    const PROFILE_IMG = 'profileImg';
    const FORTE_TOUR = 'forteTour';

    protected $messages = [
        'number_decision.required' => 'Số quyết định không được bỏ trống',
        'leader_sign.required'     => 'Người quyết dịnh không được bỏ trống',
        'date_sign.required'       => 'Ngày quyết dịnh không được bỏ trống',
    ];
    protected $rules =
        [
            'number_decision' => 'required|numeric',
            'leader_sign'     => 'required|numeric',
            'date_sign'   => 'required',
        ];
    protected $messages_file = [
        'result_file.mimes' => 'File không đúng định dạng',
        'result_file.max' => 'Dung lượng file quyết định cho phép là 10M',
        'regex' => 'File name chứa ký tự đặc biệt',
        'unique' => ':attribute đã được sử dụng'
    ];
    protected $rules_file =
        [
            'number_decision' => 'required',
            'result_file' => 'nullable|mimes:pdf,jpg,png|regex:/^[a-zA-Z0-9_\-]*$/',

        ];

    public function index(Request $request)
    {
        $status = $request->input(CommontConstants::STATUS, '');
        if($status && !in_array($status, [MemberConstants::SIGNING_WAITING, MemberConstants::CODE_PROVIDING_WAITING])) {
            abort(404);
        }

        $queryCode = Code::where('prefix', 4)->where('year', date('Y'));
        $limit = CommontConstants::DEFAULT_LIMIT_MEMBER_PER_PAGE;

        $objMembers = $this->_searchByCondition($request, $limit,false);
        $sumObjMembers = $this->_searchByCondition($request, $limit,true);
        $objLeaders = LeaderSigningDecisionProvider::getAllLeaderSignDecision();

        $decisionNumber = $queryCode->first();
        if (empty($decisionNumber)) {
            Code::where('prefix', 4)->update([
                'year' => date('Y'),
                'current_code' => '000'
            ]);
            $decisionNumber = $queryCode->first();
        }

        $current_page =  empty($request['page'])? 0 :$request['page'];
        if($current_page <=1){
            $current_page = 0;
        } else {
            $current_page = ($current_page -1 ) * 10;
        }

        $searchCommon = $this->getStatusByStep();
        // $searchByRoleOrArea = $this->getSearchByRoleOrArea($request);

        return view('admin.decision.index', [
            CommontConstants::OBJ_MEMBERS => $objMembers,
            CommontConstants::RESPONSE_VIEW => empty($request) ? array() : $request,
            CommontConstants::STEP => $searchCommon[CommontConstants::STEP],
            CommontConstants::STATUS_ARRAY => $searchCommon[CommontConstants::STATUS_ARRAY],
            // CommontConstants::AREA_OR_ROLE => $searchByRoleOrArea,
            // CommontConstants::HN_SEARCH => $this->searchByHnArea(),
            // CommontConstants::DN_SEARCH => $this->searchByDnArea(),
            // CommontConstants::HCM_SEARCH => $this->searchByHcmArea(),
            CommontConstants::ALL_SEARCH => $this->searchByAllArea(),
            CommontConstants::COUNT_MEMBER => count($sumObjMembers),
            CommontConstants::PAGINATOR => (count($sumObjMembers) > 0) ? $objMembers->appends(request()->except('page')) : null,
            self::OBJ_LEADER_SIGN => $objLeaders,
            CommontConstants::CURRENT_PAGINATOR => $current_page,
            'status' => $status,
            'offices' => Offices::searchByAllOffices(),
            'decisionNumber' => $decisionNumber
        ]);
    }

    private function _searchByCondition($request, $limit, $isCount)
    {
        if (!empty($request->input(CommontConstants::FROM_DATE))) {
            $fromDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::FROM_DATE, ''))->toDateString();
        } else {
            $fromDate = '';
        }

        if (!empty($request->input(CommontConstants::TO_DATE))) {
            $toDate = Carbon::createFromFormat(CommontConstants::D_M_Y_FORMAT, $request->input(CommontConstants::TO_DATE, ''))->toDateString();
        } else {
            $toDate = '';
        }

        if (!empty($request->input(CommontConstants::PROVINCE_TYPE))) {
            $getProvinceType = $this->searchByProvinceType($request->input(CommontConstants::PROVINCE_TYPE));
        }
        else {
            $getProvinceType = '';
        }

        $arrConditions = [
            CommontConstants::FULL_NAME => $request->input(CommontConstants::FULL_NAME, ''),
            CommontConstants::STATUS => $request->input(CommontConstants::STATUS, ''),
            CommontConstants::TOURIST_GUIDE_CODE => $request->input(CommontConstants::TOURIST_GUIDE_CODE, ''),
            CommontConstants::FROM_DATE => $fromDate,
            CommontConstants::TO_DATE => $toDate,
            CommontConstants::PROVINCE_CODE => $request->input(CommontConstants::PROVINCE_CODE, ''),
            CommontConstants::PROVINCE_TYPE => $getProvinceType,
            CommontConstants::FILE_CODE => $request->input(CommontConstants::FILE_CODE),
        ];

        return MemberDecisionServiceProvider::decisionSearchByConditions($arrConditions, $limit,$isCount);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatorInput = Validator::make(Input::all(), $this->rules, $this->messages);
        if ($validatorInput->fails()) {
            return Response::json(array('errors' => $validatorInput->getMessageBag()->toArray()));
        } else {
            $dataReload = MemberServiceProvider::getMemberById($id);

            if ($dataReload->is_delete == 1) {
                 return response()->json(['errors' => ['memberDelete' => 'Hội viên không tồn tại hoặc đã xóa.']]);
            } elseif ($request->get('member_status') != $dataReload->status) {
                $message = $dataReload->status == MemberConstants::FEE_WAITING ? 'Hội viên chưa đóng lệ phí' : 'Hồ sơ đã được xử lý';
                return response()->json(['errors' => ['memberExecuted' => $message]]);
            }
            MemberDecisionServiceProvider::updateMemberDecision($id, $request, false);
            $objCode = Code::select('current_code')->where('prefix', 4)->first();
            $number_decision = $request->input('number_decision');
            if ($number_decision > $objCode->current_code) {
                Code::where('prefix', 4)->update([
                    'current_code' => $number_decision
                ]);
            }

            return response()->json($dataReload);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        MemberDecisionServiceProvider::softDeletebyMemberId($id);
        $dataReload = MemberServiceProvider::getMemberById($id);
        // LogServiceProvider::createSystemHistory(trans('history.fn_delete_decision'), [
        //     'memberId' => $id,
        //     'decision' => $dataReload
        // ]);
        return response()->json($dataReload);

    }

    public function doCreateMemberCard($id, Request $request)
    {

        if (empty($id)) {
            $objMember = null;
        } else {
            $objMember = MemberServiceProvider::getMemberById($id);
        }

        if (empty($objMember)) {
            abort(404);
        }
//        if ($request->input('is_print') == 1 && UserServiceProvider::isMemberShipCardIssuer()) {
        if ($request->input('is_print') == 1) {
            // In thông tin thành viên
   //          $arrUpdateData = [
   //              'is_print' => MemberConstants::IS_PRINT,
   //          ];
   //          MemberDecisionServiceProvider::updateMemberWhenDoReport($id, $arrUpdateData);
			// MemberDecisionServiceProvider::updateMember($id);
			// $typeOfPlaces = UserServiceProvider::getTypeOfPlace();

            // $objEducation = MemberServiceProvider::getEducationByMemberId($objMember->id);
            // $objLanguageSkills = MemberServiceProvider::getLanguageAchievedByMemberId($objMember->id);

            // $objMajorSkills =  MajorSkill::where('memberId', $objMember->id)->first();

            // if (empty($objMajorSkills)) {
                // $objMajor = null;
            // } else {
                // $objMajor = Major::where('id', $objMajorSkills->majorId)->first();
            // }

            // $workHistories = MemberServiceProvider::getWorkHistoryByMemberId($objMember->id);
            // $objElement = MemberServiceProvider::getElementByMemeber($objMember);
            // $paymentInfor = \App\Models\MemberPayments::where('memberId', $objMember->id)->first();
            // $notes = \App\Providers\MemberServiceProvider::getDetailMemberById($objMember->id);
            // $paymentYearly = MemberPaymentsYearlyServiceProvider::getAllPaymentYearlyByMemberId($objMember->id);

            // $otherConstant = $workConstant = $provincial = $chihoi = $clbthuochoi = [];
            // $otherTmp = OtherConstant::where('status', 1)->orderBy('otherName', 'asc')->get();
            // $workTmp = WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get();
            // $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
            // $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
            // $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();

            // $languages = ListMemberServiceProvider::getAllLanguages();
            // $forteTours = ListMemberServiceProvider::getAllForteTours();

            // if(!empty($otherTmp)){
                // foreach($otherTmp as $key => $value){
                    // $otherConstant[$value->id] = $value->otherName;
                // }
            // }
            // if(!empty($workTmp)){
                // foreach($workTmp as $key => $value){
                    // $workConstant[$value->id] = $value->workName;
                // }
            // }
            // if(!empty($optionTmp)){
                // foreach($optionTmp as $key => $value){
                    // $provincial[$value->id] = $value->value;
                // }
            // }
            // if(!empty($chihoiTmp)){
                // foreach($chihoiTmp as $key => $value){
                    // $chihoi[$value->id] = $value->name;
                // }
            // }
            // if(!empty($clbthuochoTmp)){
                // foreach($clbthuochoTmp as $key => $value){
                    // $clbthuochoi[$value->id] = $value->name;
                // }
            // }

            // if ($objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER) {
                // $memberRank = MemberRank::getRankByMemberId($id);
                // $ranks = Rank::getAllRanks();
            // }


			$objDecision = MemberDecisionServiceProvider::getMemberDecisionByMemberId($id);

			if (!empty($objDecision)) {
				$objSigner = LeaderSigningDecision::where('id', $objDecision->leaderId)->where('status', 1)->first();
			}
            return view('admin.member.print_decision', [
                CommontConstants::OBJ_MEMBER => $objMember,
                'objDecision' => !empty($objDecision) ? $objDecision : '',
				'objSigner' => !empty($objSigner) ? $objSigner : '',
            ]);
        }

        return redirect()->route('admin_member_create_card_view');
    }


    public function getDetail ($id, Request $request)
    {
        if (empty($id)) {
            $objData = null;
        } else {
            $objData = MemberDecisionServiceProvider::getDataByMemberID([], 1,false,$id);
            if(isset($objData[0])) $objData = $objData[0];
        }
        if (empty($objData)) {
            $request->session()->flash('error_ajax', ['Hội viên không tồn tại hoặc đã xóa khỏi hệ thống']);
            return response()->json(['deleted' => 'error']);
        }

        return response()->json(['objData' => $objData]);
    }

    public function checkprovidingdecision($id, Request $request) {
        $member_decision_file = MemberDecision::where('memberId', $id)->where('is_delete', 0)->first();
        if (empty($member_decision_file)) {
            return response()->json(['data' => 'error']);
        }
        return response()->json(['data' => 'success']);
    }

    public function providingDecisionMany (Request $request)
    {
        $strMemberIdList = $request->input('list_id');
        $arrMemberId = explode(',', $strMemberIdList);
        $queryCode = Code::where('prefix', 4)->where('year', date('Y'));
        $objLeaders = LeaderSigningDecisionProvider::getLeaderSignDecisionFirst();
        $fileId = null;
        $date_sign = date('Y-m-d');
        $decisionNumber = $queryCode->first();
        if (empty($decisionNumber)) {
            Code::where('prefix', 4)->update([
                'year' => date('Y'),
                'current_code' => '000'
            ]);
            $decisionNumber = $queryCode->first();
        }

        $a = 0;
        foreach ($arrMemberId as $key => $memberId) {
            $a = $key + 1;
            $number = sprintf('%03d', $decisionNumber->current_code + $key + 1);
            $decicionObject[] = [
                'memberId' => $memberId,
                'number_decisive' => $number.'/QĐ-HHDVDLVN',
                'leaderId' => $objLeaders->id,
                'sign_date' => $date_sign,
                'fileId' => $fileId,
                'is_signed' => '1',
                'is_delete' => '0',
                'is_print' => '1',
                'createdAt' => date('Y-m-d H:i:s'),
                'updatedAt' => NULL,
                'deleteAt' => NULL
            ];
        }
        MemberDecision::insert($decicionObject);
        Code::where('prefix', 4)->update([
            'current_code' => sprintf('%03d', $decisionNumber->current_code + $a)
        ]);

        Member::whereIn('id', $arrMemberId)->whereNull('is_delete')->update([
            'status' => MemberConstants::CODE_PROVIDING_WAITING
        ]);

        return redirect()->route('admin_member_list_decision_view', ['status' => MemberConstants::SIGNING_WAITING])->with('successes', ['Cấp quyết định thành công']);
    }
}
