<?php

namespace App\Http\Controllers;

use App\Models\Branches;
use App\Repositories\Branches\BranchesRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;
use App\Constants\MemberConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
use App\Models\Offices;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    const STEP = 'step';
    const STATUS_ARRAY = 'arrFile';
    const AREA_HN = 'HN';
    const AREA_DN = 'DN';
    const AREA_HCM = 'HCM';

    public static function getStatusByStep($status = null)
    {
    	$step = 0;
        $arrFile = [];
		if (Route::currentRouteName() == 'admin_member_list_view') {
		    $arrFile = MemberConstants::$file_const_display_status_search;
		    $step = 0;
		} else {
		    if (Route::currentRouteName() == 'admin_member' && (UserServiceProvider::isExpertRole() || UserServiceProvider::isAdmin()) && (isset($status) && in_array($status, [1, 2, 7]))) {
		        $arrFile = MemberConstants::$file_expert_const;
		        $step = 1;
		    } elseif (Route::currentRouteName() == 'admin_member' && (UserServiceProvider::isLeaderRole() || UserServiceProvider::isAdmin()) && (isset($status) && in_array($status, [3, 8, 22]))) {
		        $arrFile = MemberConstants::$file_leader_const;
		        $step = 2;
		    } elseif (Route::currentRouteName() == 'admin_member_list_payment_view') {
		        $arrFile = MemberConstants::$file_accountant_const;
		        $step = 3;
		    } else if(Route::currentRouteName() == 'admin_member_create_card_view') {
                $arrFile = MemberConstants::getMemberShipCardIssuerStatusScope('create_card');
                $step = 5;
            } elseif (Route::currentRouteName() == 'admin_member_print_excel_member_card_view') {
                $arrFile = MemberConstants::getMemberShipCardIssuerStatusScope('print_card');
                $step = 6;
            } else if (Route::currentRouteName() == 'admin_member_list_decision_view'){
                $arrFile = MemberConstants::getMemberShipCardIssuerStatusScope('decision');
                $step = 4;
            } elseif (Route::currentRouteName() == 'admin_member_list_payment_yearly_view') {
                $arrFile = MemberConstants::$member_annual_fees;
            } else if (Route::currentRouteName() == 'admin_member_rank_view') {
                $arrFile = MemberConstants::$member_official;
            }
		}
		
		return array(self::STEP => $step, self::STATUS_ARRAY => $arrFile);
    }

    public static function getSearchByRoleOrArea($response_view)
    {
        $branches = new BranchesRepository(new Branches());

		// Thay doi logic 14062018
        if (UserServiceProvider::isAccountant() || UserServiceProvider::isMemberShipCardIssuer()) {
            $arrCities = $branches->getAll(['id']);
        } else {
            $vpddCode = auth()->user()->province_type;

            if (empty($vpddCode)) {
                $arrCities = [];
            } else {
                $arrCities = $branches->getAllByNotNullNotEqualParentId($vpddCode);
            }
        }
		/*if (UserServiceProvider::isHnExpertUser() || UserServiceProvider::isHnLeaderUser()) {
		    $arrCities = array_collapse([BranchConstants::$dn_typeOfPlace, BranchConstants::$hcm_typeOfPlace]);
		} elseif (UserServiceProvider::isDnExpertUser() || UserServiceProvider::isDnLeaderUser()) {
		    $arrCities = array_collapse([BranchConstants::$hn_typeOfPlace, BranchConstants::$hcm_typeOfPlace]);
		} elseif (UserServiceProvider::isHcmExpertUser() || UserServiceProvider::isHcmLeaderUser()) {
		    $arrCities = array_collapse([BranchConstants::$hn_typeOfPlace, BranchConstants::$dn_typeOfPlace]);
		} elseif (UserServiceProvider::isAccountant() || UserServiceProvider::isMemberShipCardIssuer()) {
		    $arrCities = [];
		}*/
		$search = array_get($response_view, 'province_type', '');
		            
		if ($search == BranchConstants::TYPE_OF_PLACE_HN) {
		    // $arrCities = array_collapse([BranchConstants::$dn_typeOfPlace, BranchConstants::$hcm_typeOfPlace]);
            $area = self::AREA_HN;
		} elseif ($search == BranchConstants::TYPE_OF_PLACE_DN) {
            $area = self::AREA_DN;
		    // $arrCities = array_collapse([BranchConstants::$hn_typeOfPlace, BranchConstants::$hcm_typeOfPlace]);
		} elseif ($search == BranchConstants::TYPE_OF_PLACE_HCM) {
            $area = self::AREA_HCM;
		    // $arrCities = array_collapse([BranchConstants::$hn_typeOfPlace, BranchConstants::$dn_typeOfPlace]);
		}

		if (!empty($area)) {
            $arrCities = Offices::getBranchesByArea($area, ['id']);
        }

		// $arrCities = array_except($allPlaces, $places);

		/*if (($search == BranchConstants::TYPE_OF_PLACE_HN) || empty($search)) {
		    $arrCities['65'] = "CLB thuộc hội - CLB HDV tiếng Nhật Hà Nội";
		}*/

		return $arrCities; 
    }

    public static function searchByHnArea()
    {
    	/*$arrCities = [];
        $exceptedPlaces = array_collapse([BranchConstants::$dn_typeOfPlace, BranchConstants::$hcm_typeOfPlace]);
        $arrCities = array_except(BranchConstants::$name, $exceptedPlaces);
        $arrCities['65'] = "CLB thuộc hội - CLB HDV tiếng Nhật Hà Nội";*/

        $arrCities = Offices::getBranchesByArea(self::AREA_HN, ['id']);
        return json_encode($arrCities);
    }

    public static function searchByDnArea()
    {
    	/*$arrCities = [];
        $exceptedPlaces = array_collapse([BranchConstants::$hn_typeOfPlace, BranchConstants::$hcm_typeOfPlace]);                
        $arrCities = array_except(BranchConstants::$name, $exceptedPlaces);*/

        $arrCities = Offices::getBranchesByArea(self::AREA_DN, ['id']);
        return json_encode($arrCities);
    }

    public static function searchByHcmArea()
    {
    	/*$arrCities = [];
        $exceptedPlaces = array_collapse([BranchConstants::$hn_typeOfPlace, BranchConstants::$dn_typeOfPlace]);
        $arrCities = array_except(BranchConstants::$name, $exceptedPlaces);*/

        $arrCities = Offices::getBranchesByArea(self::AREA_HCM, ['id']);
        return json_encode($arrCities);
    }

    public static function searchByAllArea()
    {
    	$exceptedPlaces = array_collapse([]);
        $arrCities = array_except(Branches::getBranches01(), $exceptedPlaces);
        $arrCities['65'] = "CLB thuộc hội - CLB HDV tiếng Nhật Hà Nội";
        return json_encode($arrCities);
    }

    public function searchByActivityRegistrationPlace(Request $request)
    {
        $data = [
            'status' => 1
        ];
        $parent_id = $request->input('parent_id');
        $status = $request->input('status');
        $provinces_type = $request->input('province_type');
        $getActivities = array();
        if ($status == 1) {
            if (count($provinces_type) != 0) {
                foreach ($provinces_type as $province_type) {
                    $getActivities = array_merge($getActivities, Offices::getProvinceCodeByArea($province_type));
                }
            }
        }
        else {
            $getActivities = Offices::getProvinceCodeByProvinceType($parent_id);
        }
        $data['value'] = $getActivities; 
        return response()->json($data);
    }

    public function searchByProvinceType($province_type)
    {
        $getProvinceType = array();
        $getProvinceCode = Offices::getProvinceCodeByProvinceType($province_type);
        foreach ($getProvinceCode as $provinceCode) {
            $getProvinceType[] = $provinceCode['id'];
        }
        if (count($getProvinceType) == 0) {
            $getProvinceType[] = -1;
        }
        return $getProvinceType;
    }

    public function getCurrentCursor ($limit = 10, Request $request)
    {
        $currentPage =  $request->input('page', 1);
        if($currentPage <= 1){
            $currentCursor = 0;
        }else {
            $currentCursor = ($currentPage - 1 ) * $limit;
        }

        return $currentCursor;
    }
}
