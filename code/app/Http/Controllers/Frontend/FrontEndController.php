<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontEndBaseController;
use App\Providers\UserServiceProvider;
use App\Providers\VitaServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Input;
use App\Models\News;
use App\Repositories\News\NewsRepository;
use App\Repositories\ClubOfHead\ClubOfHeadInterface;
use App\Repositories\Branches\BranchesInterface;
use App\Repositories\ClubOfBranch\ClubOfBranchInterface;
use URL;
use App\Models\Information;

class FrontEndController extends FrontEndBaseController {

    const LIMIT = 5;
    const LIMIT_MEMBER = 4;
    const POSITION_KEY = 'position';
    const TYPE_BRANCH = 'branch';
    const TYPE_CLUBOFHEAD = 'club';
    const TYPE_CLUBOFBRANCH = 'clubOfBranch';
    const BASE_AJAX_BRANCH = '/ajax/get-content-club-of-branch/';

    /**
     * Tin tức và sự kiện hội
     */
    const NEWS_CODE = 'NEWS01';

    public function index() {
        $newsRepo = new NewsRepository(new News());
        $lastestNews = $newsRepo->getLastestNewsHomePages($this::NEWS_CODE);
        $information = Information::getAllInformationWithParentNull();
        return view('frontend.index', ['lastestNews' => $lastestNews, 'information' => $information]);
    }

    public function gioithieu() {
        return view('frontend.gioithieu', [
            'memberBCHList' => VitaServiceProvider::getAllExecutiveBoardMember()
        ]);
    }

    public function chihoi(Request $request, BranchesInterface $branches) {
        $page = $request->get('page', 1);
        $listBranches = $branches->listAllBranches();
        $currentBranch = $request->get('branch_name', '');
        $parames = [
            'listBranches' => $listBranches,
            'currentBranch' => $currentBranch,
            'offsets' => ($page - 1) * self::LIMIT,
            'type' => self::TYPE_BRANCH
        ];
        if ($currentBranch != '') {
            if (!$request->ajax()) {
                $rules = ['captcha' => 'required|captcha'];
                $validator = Validator::make($request->all(), $rules, ['captcha' => 'Mã xác nhận không hợp lệ', 'required' => 'Mã xác nhận không được bỏ trống']);
                if ($validator->fails()) {
                    return redirect(url()->current())->withInput(['currentBranch' => $currentBranch])->withErrors
                                    ($validator->messages());
                }
            }

            // check exist
            $detailBranch = $branches->find($currentBranch);
            if (empty($detailBranch)) {
                abort(404);
            }

            $listOption = $branches->getOptionsByKey(self::POSITION_KEY);
            $listManager = $branches->paginateManager($currentBranch, self::LIMIT)->appends(['branch_name' => $currentBranch, 'manager' => true]);

            //$listMember = $branches->paginateMember($currentBranch, self::LIMIT_MEMBER, $request->all())->setPath(route('front_end_ajax_get_member', ['id' => $currentBranch, 'type' => self::TYPE_BRANCH]))->appends(['member' => true]);
            $listMember = null;
            $listLanguage = \App\Models\Language::getAllLanguages();

            $parames['listManager'] = $listManager;
            $parames['listOption'] = $listOption;
            $parames['listMember'] = $listMember;
            $parames['listLanguage'] = $listLanguage;
            $parames['type'] = self::TYPE_BRANCH;
            if ($request->ajax() && $request->get('manager')) {
                $html = view('frontend.newdesign.branches.list_manager', $parames)->render();
                return response()->json(array('html' => $html));
            }
        }

        return view('frontend.newdesign.chihoi', $parames);
    }

    public function clb(Request $request, ClubOfHeadInterface $branches) {
        $page = $request->get('page', 1);
        $listClb = $branches->getListClub();
        $currentClb = $request->get('clb_name', '');

        $parames = [
            'listClb' => $listClb,
            'currentClb' => $currentClb,
            'offsets' => ($page - 1) * self::LIMIT,
            'type' => self::TYPE_CLUBOFHEAD
        ];

        if ($currentClb != '') {
            if (!$request->ajax()) {
                $rules = ['captcha' => 'required|captcha'];
                $validator = Validator::make($request->all(), $rules, ['captcha' => 'Mã xác nhận không hợp lệ', 'required' => 'Mã xác nhận không được bỏ trống']);
                if ($validator->fails()) {
                    return redirect(url()->current())->withInput(['currentClb' => $currentClb])->withErrors($validator->messages());
                }
            }

            // check exist
            $detailBranch = $branches->find($currentClb);
            if (empty($detailBranch)) {
                abort(404);
            }

            $listOption = $branches->getOptionsByKey(self::POSITION_KEY);
            $listManager = $branches->paginateManager($currentClb, self::LIMIT)->appends(['clb_name' => $currentClb, 'manager' => true]);

            //$listMember = $branches->paginateMember($currentClb, self::LIMIT_MEMBER, $request->all())->setPath(route('front_end_ajax_get_member', ['id' => $currentClb, 'type' => self::TYPE_CLUBOFHEAD]))->appends(['member' => true]);
            $listMember = null;
            $listLanguage = \App\Models\Language::getAllLanguages();

            $parames['listManager'] = $listManager;
            $parames['listOption'] = $listOption;
            $parames['listMember'] = $listMember;
            $parames['listLanguage'] = $listLanguage;
            $parames['type'] = self::TYPE_CLUBOFHEAD;

            if ($request->ajax() && $request->get('manager')) {
                $html = view('frontend.newdesign.clubofhead.list_manager', $parames)->render();
                return response()->json(array('html' => $html));
            }
        }

        return view('frontend.newdesign.clb', $parames);
    }

    public function news() {
        return view('frontend.news');
    }

    public function newsDetail($alias) {
        return view('frontend.news_detail');
    }

    public function hoivien() {
        return view('frontend.hoivien');
    }

    /**
     * Handle redirect login
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirect() {
        if (UserServiceProvider::isNormalUser()) {
            return redirect()->route('front_end_index');
        } else {
            if (auth()->user()->hasPermissions('admin_member_list_view')) {
                return redirect()->route('admin_member_list_view');
            }

            if (auth()->user()->is_traveler_company) {
                return redirect()->route('company_evaluate_list_frontend');
            }

            if ($firstRoute = auth()->user()->getFirstRouteAccess()) {
                if (auth()->user()->hasPermissions('admin_editor_listmember')) {
                    return redirect()->route('admin_editor_listmember');
                }
                return redirect()->route($firstRoute);
            }

            abort(404);
        }
    }

    public function ajaxGetNews($type, $id, Request $request) {
        $newsRepo = new NewsRepository(new News());
        $html = '';
        if ($request->ajax()) {
            if (!empty($id) && in_array($type, [self::TYPE_BRANCH, self::TYPE_CLUBOFHEAD])) {
                $optionCode = $type == self::TYPE_BRANCH ? NewsRepository::NEWS_BRANCH : NewsRepository::NEWS_CLUB;
                $listNews = $newsRepo->paginateNewsByBranchId($optionCode, $id);
                $view = self::TYPE_BRANCH ? 'frontend.newdesign.branches.list_news_events' : 'frontend.newdesign.clubofhead.list_news_events';
                $html = view($view, [
                    'id' => $id,
                    'listNews' => $listNews,
                        ])->render();
            }
        }

        return response()->json(array('html' => $html));
    }

    public function ajaxGetDetailNews($id, Request $request) {
        if (!$id) {
            return response()->json(array('html' => ''));
        }
        if ($request->ajax()) {
            $detail = News::where('status', 1)
                    ->where('id', $id)
                    ->first();
            if (!empty($detail)) {
                if ($detail->option_code == NewsRepository::NEWS_BRANCH) {
                    $link = URL::route('front_end_ajax_get_news', ['type' => self::TYPE_BRANCH, 'id' => $detail->branch_id]);
                } else {
                    $link = URL::route('front_end_ajax_get_news', ['type' => self::TYPE_CLUBOFHEAD, 'id' => $detail->branch_id]);
                }
                $breadcrumb = [
                    'link' => $link,
                    'text' => 'Trở lại'
                ];
                $html = view('frontend.newdesign.branches.news-detail', [
                    'detail' => $detail,
                    'breadcrumb' => $breadcrumb
                ])->render();

                return response()->json(array('html' => $html));
            }
        }
    }

    public function ajaxGetHoiVien($branchId, $type, Request $request, BranchesInterface $branches, ClubOfHeadInterface $clubOfHead, ClubOfBranchInterface $clubOfBranch) {
        $rules = ['captcha2' => 'required|captcha'];
        $validator = Validator::make($request->all(), $rules, [
                    'captcha' => 'Mã xác nhận không hợp lệ',
                    'required' => 'Mã xác nhận không được bỏ trống'
        ]);
        if ($request->isMethod('post') && $validator->fails()) {
            return response()->json(['error' => true, 'messages' => $validator->messages()]);
        }
        $html = '';
        if ($request->ajax()) {
            if (!empty($branchId)) {
                switch ($type) {
                    case self::TYPE_BRANCH:
                        $branchName = $branches;
                        break;
                    case self::TYPE_CLUBOFBRANCH;
                        $branchName = $clubOfBranch;
                        break;
                    case self::TYPE_CLUBOFHEAD;
                        $branchName = $clubOfHead;
                        break;
                    default :
                        $branchName = $branches;
                }
                $listMember = $branchName->paginateMember($branchId, self::LIMIT_MEMBER, $request->all());
                $listLanguage = \App\Models\Language::getAllLanguages();
                $html = view('frontend.newdesign.clubofhead.list_member', [
                    'listMember' => $listMember,
                    'listLanguage' => $listLanguage
                        ])->render();
            }
        }

        return response()->json(array('html' => $html));
    }

    public function ajaxGetHoiVienExtend($branchId, $type, Request $request, BranchesInterface $branches) {
        
        $html = '';
       
        if (!empty($branchId)) {
            switch ($type) {
                case self::TYPE_BRANCH:
                    $branchName = $branches;
                    break;
                default :
                    $branchName = $branches;
            }
            $listMember = $branchName->paginateMember($branchId, self::LIMIT_MEMBER, $request->all());
            $listLanguage = \App\Models\Language::getAllLanguages();
            $html = view('frontend.newdesign.clubofhead.list_member', [
                'listMember' => $listMember,
                'listLanguage' => $listLanguage
                    ])->render();
        }
       

        return response()->json(array('html' => $html));
    }

    public function ajaxClubOfBranches($branchId, Request $request, ClubOfBranchInterface $branches) {
        $listClb = $branches->getClbByBranchesId($branchId);

        if ($request->ajax()) {
            $html = view('frontend.newdesign.branches.club', ['listClb' => $listClb, 'baseAjaxBranch' => self::BASE_AJAX_BRANCH])->render();
            return response()->json(array('html' => $html));
        }
    }

    public function ajaxClbGetContent($id, Request $request, ClubOfBranchInterface $branches) {
        $html = '';
        $page = $request->get('page', 1);
        $parames = ['offsets' => ($page - 1) * self::LIMIT];

        $detailBranch = $branches->find($id);

        if (empty($detailBranch)) {
            abort(404);
        }
        $listManager = $branches->paginateManager($id, self::LIMIT)->appends(['manager' => true]);
        $listOption = $branches->getOptionsByKey(self::POSITION_KEY);

        //$listMember = $branches->paginateMember($id, self::LIMIT_MEMBER, $request->all())->setPath(route('front_end_ajax_get_member', ['id' => $id, 'type' => self::TYPE_CLUBOFBRANCH]))->appends(['member' => true]);
        $listMember = null;
        $listLanguage = \App\Models\Language::getAllLanguages();
        $parames['listManager'] = $listManager;
        $parames['listOption'] = $listOption;
        $parames['listMember'] = $listMember;
        $parames['listLanguage'] = $listLanguage;
        $parames['type'] = self::TYPE_CLUBOFBRANCH;
        $parames['id'] = $id;
        if ($request->ajax() && $request->get('manager')) {
            $html = view('frontend.newdesign.branches.club_list_manager', $parames)->render();
            return response()->json(array('html' => $html));
        } elseif ($request->ajax()) {
            $html = view('frontend.newdesign.branches.club_main', $parames)->render();
            return response()->json(array('html' => $html));
        }
    }
}
