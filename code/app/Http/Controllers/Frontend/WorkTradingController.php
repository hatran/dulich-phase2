<?php
/**
 * Created by PhpStorm.
 * User: GMO
 * Date: 5/14/2018
 * Time: 3:45 PM
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontEndBaseController;
use App\Providers\IntroductionServiceProvider;

class WorkTradingController extends FrontEndBaseController
{
	const INTRO_10 = 'INTRODUC10'; 
    public function index(){
    	$data = IntroductionServiceProvider::getNewsByOption(self::INTRO_10);
        return view('frontend.newdesign.works.index', [
            'data' => $data
        ]);
    }
}