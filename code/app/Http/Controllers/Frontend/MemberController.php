<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontEndBaseController;
use App\Models\MemberRank;
use App\Models\Rank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Language;
use App\Models\Member;
use App\Models\Offices;
use DB;
use App\Constants\MemberConstants;
use App\Constants\CommontConstants;
use App\Models\User;
use Hash;
use App\Providers\UserServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\Emails;
use App\Jobs\SendEmail;
use App\Libs\Helpers\LogsHelper;
use App\Providers\MemberServiceProvider;

class MemberController extends FrontEndBaseController
{
    const BRANCHES_CLUB = 'club';

    protected $_count = 0;

    const SUCCESSES = 'successes';
    const EMAIL_14 = 'EMAIL14';

    public function hoivien(request $_request)
    {
        $languages = $this->getLanguage();
        $data = $_request->input();
        $members = $this->_objByFliter($data);
        $objClub = $this->getOptionsClub();
        $objBranche = $this->getBraches();
        $request = $_request->input();
        unset($request['check']);
        unset($request['_token']);
        unset($request['captcha']);

        if (empty($request)) {
            $members = "";
        }

        $total = $this->_count;
        $ranks = Rank::getAllRanks();
        if ($_request->input('check')) {
            return view('frontend.ajax.hoivien', compact('members', 'languages', 'objClub', 'objBranche', 'request', 'total', 'ranks'));
        } else {
            if ($_request->input('tmp')) {
                return view('frontend.ajax.hoivien', compact('members', 'languages', 'objClub', 'objBranche', 'request', 'total', 'ranks'));
            }
            return view('frontend.newdesign.hoivien', compact('members', 'languages', 'objClub', 'objBranche', 'request', 'total', 'ranks'));
        }
    }

    //hoivien list user, search
    public function _objByFliter($members)
    {
        $captcha = '';
        $token = '';
        if (!empty($members['captcha'])) $captcha = $members['captcha'];
        if (!empty($members['_token'])) $token = $members['_token'];

        $name = '';
        $id = '';
        $id2 = '';
        $id3 = '';
        $language = '';
        $province_code = '';
        if (!empty($members['name'])) $name = $members['name'];
        if (!empty($members['id2'])) $id2 = $members['id2'];
        if (!empty($members['id3'])) $id3 = $members['id3'];
        if (!empty($members['language'])) $language = $members['language'];
        if (!empty($members['active']) || !empty($members['type'])) {
            $province_code = (!empty($members['active'])) ? $members['active'] : $members['type'];
        }
        $rules = [];
        if (!empty($members['check'])) {
            $captcha = array("_token" => $token, "captcha" => $captcha, "check" => '');
            $validator = Validator::make($captcha, $rules);
            if (!empty($validator) && $validator->fails()) {
                $obj = '';
            } else {
                if ($name == null && $id2 == null && $id3 == null && empty($members['active']) && empty($members['type']) && empty($members['language']) && $language == null) {
                    $obj = DB::table('members')->where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)->whereNull('deleteAt')->whereNull('is_delete');
                }
                else {
                    $obj = DB::table('members')->where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)->whereNull('deleteAt')->whereNull('is_delete')->where([
                        ['fullName', 'like', '%' . $name . '%'],
                        ['member_code', 'like', '%' . $id2 . '%'],
                        ['touristGuideCode', 'like', '%' . $id3 . '%']
                    ]);
                    if ($province_code != '') {
                        $obj->where('province_code', $province_code);
                    }
                    if ($language != '') {
                        $obj->where('guideLanguage', 'like', '%' . $language . '%');
                    }
                }
            }
        } else {
            if ($name == null && $id2 == null && $id3 == null && empty($members['active']) && empty($members['type']) && empty($members['language']) && $language == null) {
                    $obj = DB::table('members')->where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)->whereNull('deleteAt')->whereNull('is_delete');
            }
            else {
                $obj = DB::table('members')->where('status', MemberConstants::MEMBER_OFFICIAL_MEMBER)->whereNull('deleteAt')->whereNull('is_delete')->where([
                    ['fullName', 'like', '%' . $name . '%'],
                    ['member_code', 'like', '%' . $id2 . '%'],
                    ['touristGuideCode', 'like', '%' . $id3 . '%']
                ]);
                if ($province_code != '') {
                    $obj->where('province_code', $province_code);
                }
                if ($language != '') {
                    $obj->where('guideLanguage', 'like', '%' . $language . '%');
                }
            }
        }

        $this->_count = (!empty($obj) && is_array($obj->get()->toArray())) ? count($obj->get()->toArray()) : 0;

        if (!empty($obj)) {
            $obj = $obj->paginate(4);
        }
        return $obj;
    }

    public function getLanguage()
    {
        $languages = Language::where('status', 1)->get();
        return $languages;
    }

    public function getOptionsClub()
    {
        $objOffices = Offices::where('option_code', 'BRANCHES03')->where('status', 1)->whereNull('deleted_at')->orderBy('name', 'asc')->get();
        $arrayOffices = $objOffices->toArray();
        // html entity
        $resultArray = array();
        foreach ($arrayOffices as $objOffices) {
            $resultItem = array();
            foreach ($objOffices as $key => $item) {
                $resultItem[$key] = e($item);
            }
            $resultArray[] = $resultItem;
        }
        $lastResult = array();
        foreach ($resultArray as $key => $office) {
            $lastResult[] = $office;
        }
        return $lastResult;

    }

    public function getBraches()
    {
        $objOffices = Offices::where('option_code', 'BRANCHES01')->where('status', 1)->whereNull('deleted_at')->orderBy('name', 'asc')->get();
        $arrayOffices = $objOffices->toArray();
        // html entity
        $resultArray = array();
        foreach ($arrayOffices as $objOffices) {
            $resultItem = array();
            foreach ($objOffices as $key => $item) {
                $resultItem[$key] = e($item);
            }
            $resultArray[] = $resultItem;
        }
        $lastResult = array();
        foreach ($resultArray as $key => $office) {
            $lastResult[] = $office;
        }
        return $lastResult;

    }

    public function getMemberOfGroup() {

    }

    public function showChangePasswordForm(Request $request){
        if (!Auth::check()) {
            abort(404);
        }
        if ($request->isMethod('post')){

            $rule = [
                'old_password' => 'required|pwdvalidation',
                'password' => User::$rule['password'] . '|different:old_password',
                'password_confirmation' => User::$rule['password_confirmation'],
            ];
            Validator::extend('pwdvalidation', function($field, $value, $parameters)
            {
                return Hash::check($value, auth()->user()->password);
            });
            $messages = User::$message;
            $messages['pwdvalidation'] = 'Mật khẩu cũ không chính xác';
            $messages['different'] = 'Mật khẩu mới phải không trùng với mật khẩu cũ';
            $validator = Validator::make($request->all(), $rule, $messages);
            $validator->setAttributeNames(User::$niceAttributeName);

            if ($validator->fails()) {
                return redirect(url()->current())
                    ->withErrors($validator)
                    ->withInput();
            }

            $objUser = UserServiceProvider::update([
                'id' => auth()->user()->id,
                'password' => bcrypt($request->get('password'))
            ]);
            if (empty($objUser)) {
                return redirect(url()->current())->withErrors(['Thay đổi mật khẩu thất bại.']);
            } else {
                return redirect(url()->current())->with
                ($this::SUCCESSES, ['Thay đổi mật khẩu thành công.']);
            }
        }

        return view('frontend.changepassword');
    }

    public function forgotPassword(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(url()->previous())
                    ->withErrors($validator)
                    ->withInput();
        }

        $email = $request->get('email');
        $user = User::query()->where('email', $request->get('email'))->whereNull('deleted_at')->first();

        if (empty($user)) {
            return redirect(url()->previous())->withErrors(['Email này đang không tồn tại.']);
        }

        $password = Str::random(6);
        $user->password = bcrypt($password);
        $user->save();

        try {
            $to = [['address' => $email, 'name' => $user->fullname]];
            $subject = '';
            $getEmail = Emails::query()->select('content', 'title', 'attach')->where('option_code', self::EMAIL_14)->first();
            $attach = null;
            
            if (!empty($getEmail)) {
                $homePage = route('front_end_index');
                $verifyUrl = '<a href="'.$homePage.'">'.$homePage.'</a>';
                $subject = $getEmail->title;
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($user->fullname), $getEmail->content);
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_USERNAME, htmlentities($user->username), $getEmail->content);
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_PASSWORD, htmlentities($password), $getEmail->content);
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_URL_TRANGCHU, $verifyUrl, $getEmail->content);
    
                if (!empty($getEmail->attach)) {
                    $attach = [
                        'realpath' => $getEmail->attach,
                        'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                        'mime' => 'pdf'
                    ];
                }
            }
            $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
            $template = 'emails.forgotpassword';
            dispatch((new SendEmail($template, $data, $to, '', $subject, $attach))->delay(5));
            MemberServiceProvider::updateMemberUpdatedAt($user->id);
        }
        catch (\Exception $exception) {
            LogsHelper::trackByFile('send_reset_password', 'Error when send reset password email (Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ')');
            return redirect(url()->previous())->withErrors(['Đã có lỗi xảy ra trong quá trình gửi mail.']);
        }

        $urlPrevious = url()->previous();
        if (str_contains($urlPrevious, 'travelercompanyrank')) {
            return redirect($urlPrevious)->with($this::SUCCESSES, ['Đã gửi lại mật khẩu vào email '.$email.' thành công.']);
        }

        return redirect($urlPrevious)->with($this::SUCCESSES, ['Vui lòng check email '.$email.' để nhận thông tin tài khoản.']);
    }

}
