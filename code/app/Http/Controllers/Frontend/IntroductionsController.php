<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontEndBaseController;
use App\Providers\IntroductionServiceProvider;

class IntroductionsController extends FrontEndBaseController
{
    const INTRO_1 = 'INTRODUC01'; //vechungtoi
    const INTRO_2 = 'INTRODUC02'; //Tầm nhìn và sứ mệnh
    const INTRO_3 = 'INTRODUC03'; //Quy định
    const INTRO_4 = 'INTRODUC04'; //Quy chế hoạt động
    const INTRO_5 = 'INTRODUC05'; //Hướng dẫn đăng ký hội viên
    public function vechungtoi(){
        $data = [];
        $data = IntroductionServiceProvider::getNewsByOption(self::INTRO_1);
        return view('frontend.newdesign.introductions.vechungtoi', [
            'data' => $data
        ]);
    }
    public function tamnhinvasumenh(){
        $data = [];
        $data = IntroductionServiceProvider::getNewsByOption(self::INTRO_2);
        return view('frontend.newdesign.introductions.tamnhinvasumenh', [
            'data' => $data
        ]);
    }
    public function quychehoatdong(){
        $data = [];
        $data = IntroductionServiceProvider::getNewsByOption(self::INTRO_4);
        return view('frontend.newdesign.introductions.quychehoatdong', [
            'data' => $data
        ]);
    }
    public function quydinh(){
        $data = [];
        $data = IntroductionServiceProvider::getNewsByOption(self::INTRO_3);
        return view('frontend.newdesign.introductions.quydinh', [
            'data' => $data
        ]);
    }
    public function huongdandangky(){
        $data = [];
        $data = IntroductionServiceProvider::getNewsByOption(self::INTRO_5);
        return view('frontend.newdesign.introductions.huongdandangky', [
            'data' => $data
        ]);
    }
    /*public function sangiaodich(){

        return view('frontend.newdesign.introductions.sangiaodich', [
            'data' => []
        ]);
    }
    public function daotao(){

        return view('frontend.newdesign.introductions.daotao', [
            'data' => []
        ]);
    }
    public function xephanghdv(){

        return view('frontend.newdesign.introductions.xephanghdv', [
            'data' => []
        ]);
    }*/

}
