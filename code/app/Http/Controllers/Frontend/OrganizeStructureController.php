<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontEndBaseController;
use App\Models\Branches;
use App\Models\Employees;
use App\Models\Option;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class OrganizeStructureController extends FrontEndBaseController
{

    //ban chuyen mon
    const BAN_CHAP_HANH_VALUE = 'DEPARTMENT04';
    const BAN_THUONG_TRUC_VALUE = 'DEPARTMENT01';
    const BAN_CO_VAN_VALUE = 'DEPARTMENT02';
    const BAN_CHUYEN_MON_VALUE = 'DEPARTMENT03';

    const BAN_HOI_VIEN_VALUE = 'DEPARTMENT03_01';
    const BAN_DAO_TAO_VALUE = 'DEPARTMENT03_02';
    const BAN_TTSK_VALUE = 'DEPARTMENT03_03';

    const PHONG_BAN_KEY = 'department';
    const PHONG_BAN_CON_KEY = 'sub_department';
    const CHUC_VU_KEY = 'position';

    const DATA_PER_PAGE = 40;

    public function getPositionFromOptionCodeEmployee($optionCode) {
        $value = '';
        $option = Option::all()->where('key', self::CHUC_VU_KEY)->where('code', $optionCode)->first();
        if($option) $value = $option->value;
        return $value;
    }

    public function getEmployeesFromSmallBoard($code) {
        $employeeData = Employees::where('employees.status',1)->join('employee_position', 'employee_position.employee_id', '=', 'employees.id')
            ->whereNull('employees.deleted_at')
            ->where('status',1)
            ->where('branch', $code)->get();
        $employees = [];
        foreach ($employeeData as $employee) {
            $employees[] = [
                'profile_image' => $employee->profile_image,
                'fullname' => $employee->fullname,
                'option_value' => $this->getPositionFromOptionCodeEmployee($employee->option_code),
                'company' => $employee->company,
                'address' => $employee->address,
                'email' => $employee->email,
                'phone' => $employee->phone
            ];
        }
        return $employees;
    }
    // ban chuyen mon
    public function specialBoard() {
        // lay nhan vien ban hoi vien
        $employees['banhoivien'] = $this->getEmployeesFromSmallBoard(self::BAN_HOI_VIEN_VALUE);
        // lay nhan vien trong ban dao tao
        $employees['bandaotao'] = $this->getEmployeesFromSmallBoard(self::BAN_DAO_TAO_VALUE);
        // lay nhan vien trong ban truyen thong su kien
        $employees['banttsk'] = $this->getEmployeesFromSmallBoard(self::BAN_TTSK_VALUE);

        return view('frontend.newdesign.organize.special_board', compact('employees'));
    }

    // ban thuong truc
    public function standingCommittee(Request $request) {
        // lay code cua ban thuong truc
        $employees = $this->getEmployeesFromSmallBoard(self::BAN_THUONG_TRUC_VALUE);
        $result = []; $i = 0;
        foreach($employees as $employee){
            $i ++;
            if ($i > 6) $employee['profile_image'] = '';
            $result[] = $employee;
        }
        $employees = $result;
        
        $employees = $this->paginatePage($employees);

        // set url path for generted links
        $employees->setPath($request->url());

        return view('frontend.newdesign.organize.standing_committee', compact('employees'));
    }

    // ban co van
    public function advisoryBoard(Request $request) {
        // lay code cua ban co van
        $employees = $this->getEmployeesFromSmallBoard(self::BAN_CO_VAN_VALUE);
        $employees = $this->paginatePage($employees);

        // set url path for generted links
        $employees->setPath($request->url());
        return view('frontend.newdesign.organize.advisory_board', compact('employees'));
    }

    // ban chap hanh
    public function executiveCommittee(Request $request) {
        $employees = $this->getEmployeesFromSmallBoard(self::BAN_CHAP_HANH_VALUE);
        $employees = $this->paginatePage($employees);

        // set url path for generted links
        $employees->setPath($request->url());
        return view('frontend.newdesign.organize.executive_committee', compact('employees'));
    }

    public function paginatePage($items) {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($items);
        $perPage = self::DATA_PER_PAGE;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        return new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
    }
}
