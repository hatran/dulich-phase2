<?php
/**
 * Created by PhpStorm.
 * User: GMO
 * Date: 5/14/2018
 * Time: 3:33 PM
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontEndBaseController;
class TrainingController extends FrontEndBaseController
{
    public function index(){
        return view('frontend.newdesign.training.index');
    }
}