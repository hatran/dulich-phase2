<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontEndBaseController;
use Illuminate\Http\Request;
use App\Models\News;
use App\Repositories\News\NewsRepository;
use App\Repositories\Branches\BranchesInterface;
use App\Models\Information;
use App\Repositories\News\NewsAdmin;
use Illuminate\Support\Facades\Auth;

class NewsController extends FrontEndBaseController
{
    /**
     * Giao thông
     */
    const INFORMATION_TRAFFIC_CODE = 'GT001';

    /**
     * Phong tục tập quán
     */
    const CUSTOM_AND_PRACTICES = 'PT001';

    /**
     * Lễ hội
     */
    const INFORMATION_FESTIVAL_CODE = 'LH001';

    /**
     * Điểm đến
     */
    const INFORMATION_DESTINATION_CODE = 'DD001';

    /**
     * Trang phục
     */
    const INFORMATION_COSTUME_CODE = 'TP001';
    /**
     * Tin tức và sự kiện hội
     */
    const NEWS_CODE = 'NEWS01';
    /**
     * Tin tức và sự kiện chi hội
     */
    const NEWS_CODE2 = 'NEWS02';
    const TYPE_BRANCH = 'branch';
    const SHARE_EXPERIANCE = 39;
    const SHARE_EXPERIANCE_PARENT = 17;

    public function __construct() {
        $this->news = new NewsAdmin();
    }

    public function news()
    {
        $newRepo = new NewsRepository(new News());
        $news = $newRepo->getListNewsPage(self::NEWS_CODE);
        return view('frontend.newdesign.news.sukienhoi', compact('news'));
    }

    public function news2($id, BranchesInterface $branches)
    {
        $newRepo = new NewsRepository(new News());
        $news = $newRepo->getListNews2(self::NEWS_CODE2, $id);
        $listBranches = $branches->listAllBranches();
        $detailBranch = $branches->find($id);
        if (empty($detailBranch)) {
            abort(404);
        }
        return view('frontend.newdesign.news.sukienchihoi', compact('news', 'listBranches', 'id', 'detailBranch'));
    
    }

    public function detail($id, Request $request)
    {
        $news = News::where('id', $id)->first();
        if (empty($news)) {
            abort(404);
        }
        return view('frontend.newdesign.news.detail', compact('news'));
    }

    public function detail2($id, Request $request, BranchesInterface $branches)
    {
        $news = News::where('id', $id)->first();
        if (!empty($news)) {
            $newsRepo = new NewsRepository(new News());
            $newsRelated = $newsRepo->findRelatedNews($id, $news->branch_id);
        }
        if (empty($newsRelated)) {
            $newsRelated = '';
        }
    
        $listBranches = $branches->listAllBranches();
        $detailBranch = $branches->find($news->branch_id);
     
        if (empty($detailBranch)) {
            abort(404);
        }
        
        return view('frontend.newdesign.news.detail2', compact('news', 'listBranches', 'id', 'detailBranch', 'newsRelated'));
    }

    /**
     * List news Giao thông
     */
    public function trafficList()
    {
        $newRepo = new NewsRepository(new News());
        $listThongtin = $newRepo->getListNews(self::INFORMATION_TRAFFIC_CODE);

        return view('frontend.newdesign.info.traffic', compact('listThongtin'));
    }

    /**
     * Detail news Giao thông
     */
    public function trafficDetail($id)
    {
        $newRepo = new NewsRepository(new News());
        $detail = $newRepo->find($id);
        if (empty($detail)) {
            abort(404);
        }

        return view('frontend.newdesign.info.traffic_detail', compact('detail'));
    }

    /**
     * List news Phong tục tập quán
     */
    public function customandpracticesList()
    {
        $newRepo = new NewsRepository(new News());
        $listThongtin = $newRepo->getListNews(self::CUSTOM_AND_PRACTICES);

        return view('frontend.newdesign.info.customandpractices', compact('listThongtin'));
    }

    /**
     * Detail news Phong tục tập quán
     */
    public function customandpracticesDetail($id)
    {
        $newRepo = new NewsRepository(new News());
        $detail = $newRepo->find($id);
        if (empty($detail)) {
            abort(404);
        }

        return view('frontend.newdesign.info.customandpractices_detail',
            compact('detail'));
    }

    /**
     * List news Lễ hội
     */
    public function festivalList()
    {
        $newRepo = new NewsRepository(new News());
        $listThongtin = $newRepo->getListNews(self::INFORMATION_FESTIVAL_CODE);

        return view('frontend.newdesign.info.festival', compact('listThongtin'));
    }

    /**
     * Detail news Lễ hội
     */
    public function festivalDetail($id)
    {
        $newRepo = new NewsRepository(new News());
        $detail = $newRepo->find($id);
        if (empty($detail)) {
            abort(404);
        }

        return view('frontend.newdesign.info.festival_detail', compact('detail'));
    }

    /**
     * List news Điểm đến
     */
    public function destinationList()
    {
        $newRepo = new NewsRepository(new News());
        $listThongtin = $newRepo->getListNews(self::INFORMATION_DESTINATION_CODE);

        return view('frontend.newdesign.info.destination', compact('listThongtin'));
    }

    /**
     * Detail news Điểm đến
     */
    public function destinationDetail($id)
    {
        $newRepo = new NewsRepository(new News());
        $detail = $newRepo->find($id);
        if (empty($detail)) {
            abort(404);
        }

        return view('frontend.newdesign.info.destination_detail', compact('detail'));
    }

    /**
     * List news Trang phục
     */
    public function costumeList()
    {
        $newRepo = new NewsRepository(new News());
        $listThongtin = $newRepo->getListNews(self::INFORMATION_COSTUME_CODE);

        return view('frontend.newdesign.info.costume', compact('listThongtin'));
    }

    /**
     * Detail news Trang phục
     */
    public function costumeDetail($id)
    {
        $newRepo = new NewsRepository(new News());
        $detail = $newRepo->find($id);
        if (empty($detail)) {
            abort(404);
        }

        return view('frontend.newdesign.info.costume_detail', compact('detail'));
    }

    public function infoList()
    {
        $information = Information::getAllInformationWithParentNull();
        // $this::parent();
        return view('frontend.newdesign.info.list', compact('information'));
    }

    public function infoCommon($newRepo) {
        $getParentInfo = $newRepo->getInformationByPluck();
        $arr = array();
        foreach ($getParentInfo as $key => $value) {
            $getChildInfo = $newRepo->getSubInfor($key);
            if (count($getChildInfo) == 0) {
                $arr[$key] = array();
            }
            foreach ($getChildInfo as $childInfo) {
                $arr[$key][] = $childInfo;
            }
        }

        return ['arr' => $arr, 'getParentInfo' => $getParentInfo];
    }

    public function subInfoList($id, $subid = null)
    {
        $newRepo = new NewsRepository(new News());
        $infoCommon = $this->infoCommon($newRepo);
        $getParentInfo = $infoCommon['getParentInfo'];
        $arr = $infoCommon['arr'];
        if ($subid == null) {
            $info = $newRepo->getParInformation($id);
            if (empty($info)) {
                abort(404);
            }
        }
        else {
            $info = $newRepo->getSubInformation1($id, $subid);
            if (empty($info)) {
                abort(404);
            }
            if (!Auth::check()) {
                $mess = 'Vui lòng đăng nhập để xem danh mục thông tin này';
                return view('frontend.newdesign.info.authenticated', compact('id', 'arr', 'getParentInfo', 'mess', 'info'));
            }
            
            if ($subid == self::SHARE_EXPERIANCE) {
                $listInfomation = $this->news->infomationCode;
                return view('frontend.newdesign.info.page_create', compact('listInfomation', 'id', 'info', 'arr', 'getParentInfo', 'subid'));
            }
        }  

        if (empty($info)) {
            $listThongtin = [];
        } 
        else {
            $listThongtin = $newRepo->getListNews($info->id);
        }
        
        return view('frontend.newdesign.info.page', compact('id', 'arr', 'getParentInfo', 'listThongtin', 'info'));
    }

    public function subInfoDetail($id, $subid = 0, $newsid)
    {
        $newRepo = new NewsRepository(new News());
        $infoCommon = $this->infoCommon($newRepo);
        $getParentInfo = $infoCommon['getParentInfo'];
        $arr = $infoCommon['arr'];
        if ($subid == 0) {
            $info = $newRepo->getParInformation($id);
        }
        else {
            $info = $newRepo->getSubInformation1($id, $subid);
            if (!Auth::check()) {
                $mess = 'Vui lòng đăng nhập để xem danh mục thông tin này';
                return view('frontend.newdesign.info.authenticated', compact('id', 'arr', 'getParentInfo', 'mess', 'info'));
            }
        }
      
        $news = $newRepo->find($newsid);
      
        if (empty($news)) {
            abort(404);
        }
        return view('frontend.newdesign.info.page_detail', compact('id', 'news', 'info', 'arr', 'getParentInfo'));
        
    }

    public function storeNews(Request $request) {
        try {
            $request->merge(array_map('trim', $request->all()));
            $this->news->create($request->all());
            return redirect()->route('front_sub_info_list', ['id' => self::SHARE_EXPERIANCE_PARENT, 'subid' => self::SHARE_EXPERIANCE])->with('successes', ['Thêm mới thông tin chia sẻ kinh nghiệm thành công']);
        } catch (ValidationException $e) {
            return redirect()->route('front_sub_info_list', ['id' => self::SHARE_EXPERIANCE_PARENT, 'subid' => self::SHARE_EXPERIANCE])->withInput()->withErrors($e->getErrors());
        }
    }
}
