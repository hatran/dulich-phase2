<?php
/**
 * Created by PhpStorm.
 * User: GMO
 * Date: 5/14/2018
 * Time: 3:52 PM
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontEndBaseController;
use App\Libs\Helpers\Utils;
use App\Models\Member;
use App\Models\Option;
use App\Models\User;
use App\Providers\IntroductionServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Constants\MemberConstants;
use App\Constants\CommontConstants;
use App\Constants\RankConstants;
use App\Models\Knowledge;
use App\Models\SubjectKnowledge;
use App\Models\Rank;
use App\Providers\MemberServiceProvider;
use App\Providers\OpenQuizProvider;
use App\Providers\QuestionProvider;
use DB;
use App\Models\QuestionTmp;
use App\Models\AnswerTmp;
use App\Models\QuizTmp;
use App\Models\MemberRank;
use App\Constants\QuizConstants;
use App\Models\Emails;
use App\Jobs\SendEmail;
use App\Libs\Helpers\LogsHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SurveyControllerController extends FrontEndBaseController
{
	const INTRO_6 = 'INTRODUC06';
	const INTRO_7 = 'INTRODUC07';
	const INTRO_8 = 'INTRODUC08';
	const INTRO_9 = 'INTRODUC09';
    const PAGINATE_PER_PAGE = 20;
    const SUCCESSES = 'successes';
    const BCC_EMAIL = 'hoihdv@gmail.com';
    const EMAIL05 = 'EMAIL05';
    const MESS_1 = 'Bạn phải là hội viên mới được sử dụng chức năng này';
    const MESS_2 = 'Vui lòng đăng nhập để sử dụng chức năng này';
    const MESS_3 = 'Vui lòng liên hệ với văn phòng Hội Hướng Dẫn Viên Du Lịch Việt Nam để biết thêm chi tiết.';
    const INTERNATIONAL = 'HDVQT';
    const LOCAL = 'HDVND';
    const EMAIL16 = 'EMAIL16';

    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const GENDER = 'gender';
    const BIRTH_YEAR = 'birthYear';
    const BIRTH_MONTH = 'birthMonth';
    const BIRTH_DATE = 'birthDate';
    const FIRST_MOBILE = 'firstMobile';
    const FIRST_EMAIL = 'firstEmail';
    const EXPIRATION_DATE = 'expirationDate';
    const PROVINCE = 'province';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const MAJORS = 'majors';
    const DELETED = 'is_delete';

    protected $rules = [
        self::FULL_NAME             => 'required|max:50',
        self::GENDER                => 'required|in:1,2',
        self::BIRTH_YEAR            => 'required|max:4',
        self::BIRTH_MONTH           => 'required|max:2',
        self::BIRTH_DATE            => 'required|max:2',
        self::TYPE_OF_TRAVEL_GUIDE  => 'required|in:1,2',
        self::FIRST_MOBILE          => 'required|numeric',
        self::FIRST_EMAIL           => 'required|email',
        self::PROVINCE              => 'required',
        self::MAJORS                => 'required',
        self::EXPIRATION_DATE       => 'required',
    ];

    protected $messages = [
        'fullName.required'               => 'Họ và tên là trường bắt buộc',
        'fullName.max'                    => 'Họ và tên chỉ được điền tối đa :max chữ số',
        'gender.required'                 => 'Giới tính là trường bắt buộc',
        'gender.in'                       => 'Giới tính không hợp lệ',
        'birthDate.required'              => 'Ngày sinh là trường bắt buộc',
        'birthDate.max'                   => 'Ngày sinh chỉ được điền tối đa :max số',
        'birthMonth.required'             => 'Tháng sinh là trường bắt buộc',
        'birthMonth.max'                  => 'Tháng sinh chỉ được điền tối đa :max số',
        'birthYear.required'              => 'Năm sinh là trường bắt buộc',
        'birthYear.max'                   => 'Năm sinh chỉ được điền tối đa :max số',
        'touristGuideCode.required'       => 'Số thẻ HDV du lịch là trường bắt buộc',
        'touristGuideCode.digits'         => 'Số thẻ HDV du lịch là số có 9 chữ số, vui lòng nhập lại',
        'touristGuideCode.unique'         => 'Số thẻ HDV du lịch đã tồn tại',
        'firstMobile.required'            => 'Điện thoại di động là trường bắt buộc',
        'firstMobile.numeric'             => 'Điện thoại di động phải có dạng số',
        'firstEmail.required'             => 'Email là trường bắt buộc',
        'firstEmail.email'                => 'Email sai định dạng',
        'typeOfTravelGuide.required'      => 'Hướng dẫn viên là trường bắt buộc phải điền',
        'typeOfTravelGuide.in'            => 'Hướng dẫn viên không hợp lệ',
        'expireDate.required'             => 'Ngày hết hạn là trường bắt buộc phải điền',
        'expirationDate.required'         => 'Ngày hết hạn đối là trướng bắt buộc phải điền',
        'majors.required'                 => 'Đăng ký hạng HDV là trường bắt buộc',
        'province.required'               => 'Địa bàn hoạt động là trường bắt buộc'
    ];

    public function index(){
        return view('frontend.newdesign.survey.index', [
            'data' => IntroductionServiceProvider::getNewsByOption(self::INTRO_6)
        ]);
    }

    public function criteria(){
        return view('frontend.newdesign.survey.criteria', [
            'data' => IntroductionServiceProvider::getNewsByOption(self::INTRO_7)
        ]);
    }

    public function register(){
        return view('frontend.newdesign.survey.register', [
            'data' => IntroductionServiceProvider::getNewsByOption(self::INTRO_8)
        ]);
    }

    public function score(){
        return view('frontend.newdesign.survey.score', [
            'data' => IntroductionServiceProvider::getNewsByOption(self::INTRO_9)
        ]);
    }

    public function infoQuestion() {
        /**
         * update comment 20230104 (cho phép xem màn hình question khi chưa cần login)
         * $role = Auth::user()->role ?? null;
        if ($role != 1) {
            return view('frontend.newdesign.survey.unauthenticated', [
                'data' => self::MESS_1
            ]);
        }
         **/

        $knowledges = Knowledge::getSubjectKnowledge();
        $arrKnowledge = [];
        $knowledge = [];
        foreach ($knowledges as $item) {
            $arrKnowledge[$item->knowledge_id][] = $item;
            $knowledge[$item->knowledge_id] = $item->parent_name;
        }
        
        return view('frontend.newdesign.survey.infoquestion', ['knowledge' => $knowledge, 'arrKnowledge' => $arrKnowledge]);
   	}

    public function infoRegister() {
	    $template_unauthen = 'frontend.newdesign.survey.unauthenticated';
	    $template_regist = 'frontend.newdesign.survey.registered';
	    $template_info = 'frontend.newdesign.survey.inforegister';
	    $template_not_member = 'frontend.newdesign.survey.notmember';

	    if (! auth()->check()) {
            $provincial = Option::query()
                ->where('status', 1)
                ->where('key', 'provincial')
                ->orderBy('id', 'asc')
                ->get();
	        return view($template_not_member, [
	            'ranks' => Rank::getAllRanksForNotMember(),
                'provincial' => $provincial,
            ]);
        }

        $user = Auth::user();
//        if ($user->role != 1) {
//            return view($template_unauthen, [
//                'data' => self::MESS_1
//            ]);
//        }
        $memberId = $user->memberId ?? 0;
        $member = MemberServiceProvider::getMemberById($memberId);

        if (! $member) {
            abort(404);
        }

        if ($member->typeOfTravelGuide == 3) {
            return view($template_unauthen, [
                'data' => self::MESS_3
            ]);
        }

        if (in_array($member->rank_status, [MemberConstants::RANK_REGISTER], false)) {
            $memberRank = MemberRank::getRankNameByMemberId($memberId);
            return view($template_regist, compact('member', 'memberRank'));
        }

        $code = $member->typeOfTravelGuide == 1 ? self::INTERNATIONAL : self::LOCAL;
        $ranks = Rank::getAllRanksByCode($code);
        $memberOfficial = MemberConstants::MEMBER_OFFICIAL_MEMBER;
        return view($template_info, compact('member', 'ranks', 'memberOfficial'));
    }

    public function updateInfoRegister(Request $request) {
        $ranks = Rank::getAllRanks();
        $objMember = Auth::user();
        $memberId = $objMember->memberId ?? 0;
        $member = MemberServiceProvider::getMemberById($memberId);

        if (empty($member)) {
            return redirect()->route('front_end_inforegister')->withErrors(['Hồ sơ không tồn tại.']);
        }

        if ($member->is_delete == 1) {
            return redirect()->route('front_end_inforegister', compact('member', 'ranks', 'memberOfficial'))->withErrors(['Hồ sơ đã xóa khỏi hệ thống.']);
        }

        if ($member->status != MemberConstants::MEMBER_OFFICIAL_MEMBER) {
            return redirect()->route('front_end_inforegister')->withErrors(['Ông/Bà chưa là hội viên chính thức.']);
        }

        $memberRank = MemberRank::query()
            ->where('member_id', $member->id)
            ->where('is_deleted', 0)
            ->orderBy('rank_id', 'asc')
            ->first();

        $rankId = $request->input(self::MAJORS);
        if ($memberRank && $memberRank->rank_id <= $rankId) {
            return redirect()->route('front_end_inforegister')
                ->withErrors(['Bạn đã đăng ký hạng HDV cao hơn hoặc bằng trước đó!'])
                ->withInput();
        }

        $member->rank_status = MemberConstants::RANK_REGISTER;
        $member->save();
        MemberServiceProvider::createMemberRank($member->id, $request->all());
        $mailTemplate = 'rank_registered';
        self::_sendApprovedMail($member->fullName, $member->firstEmail, $mailTemplate);
        return redirect()->route('front_end_inforegister')->with(CommontConstants::SUCCESSES, 'Ông/bà đã đăng ký '. Rank::find($rankId)->name ?? '');
    }

    private function handleTouristGuideCode($touristGuideCode, $rankId) {
        $fetchMember = MemberRank::query()->select('members.*', 'member_rank.rank_id')
            ->join('members', function ($join) use ($touristGuideCode) {
                $join->on('members.id', '=', 'member_rank.member_id')
                    ->where('members.status', MemberConstants::MEMBER_OFFICIAL_MEMBER)
                    ->whereNull('members.is_delete')
                    ->where('touristGuideCode', $touristGuideCode);
            })
            ->where('is_deleted', 0)
            ->groupBy(['member_rank.rank_id'])
            ->orderBy('member_rank.rank_id', 'asc')
            ->first();

        if ($fetchMember) {
            if ($fetchMember->rank_id <= $rankId) {
                return 'error';
            } else {
                $memberId = $fetchMember->id;
                Member::query()->where('id', $memberId)->update([
                    'rank_status' => MemberConstants::RANK_REGISTER,
                ]);
                MemberServiceProvider::createMemberRank($memberId, [self::MAJORS => $rankId]);
                $mailTemplate = 'rank_registered';
                self::_sendApprovedMail($fetchMember->fullName, $fetchMember->firstEmail, $mailTemplate);
                return 'success';
            }
        }
        return false;
    }

    public function updateInfoRegisterNotMember(Request $request) {
        $this->rules[self::TOURIST_GUIDE_CODE] = [
            'required',
            'digits:9'
        ];
        $validator = Validator::make($request->all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            return redirect()->route('front_end_inforegister')
                ->withErrors($validator)
                ->withInput();
        }
        try {
            DB::beginTransaction();
            $touristGuideCode = $request->input(self::TOURIST_GUIDE_CODE);
            $rankId = $request->input(self::MAJORS);

            switch ($this->handleTouristGuideCode($touristGuideCode, $rankId)) {
                case 'error':
                    return redirect()->route('front_end_inforegister')
                        ->withErrors(['Bạn đã đăng ký hạng HDV cao hơn hoặc bằng trước đó!'])
                        ->withInput();
                case 'success':
                    return redirect()->route('front_end_inforegister')
                        ->with(CommontConstants::SUCCESSES, 'Ông/bà đã đăng ký '. Rank::find($rankId)->name ?? '');
            }

            $inputEmail = $request->input(self::FIRST_EMAIL);
            $emailUser = User::query()->where('email', $inputEmail)->whereNull('deleted_at')->first();
            if (!empty($emailUser)) {
                return redirect()->route('front_end_inforegister')
                    ->withErrors(['Email đã tồn tại. Vui lòng chọn email khác'])
                    ->withInput();
            }
            $fullName = $request->input(self::FULL_NAME);
            $touristGuideCode = $request->input(self::TOURIST_GUIDE_CODE);
            $defaultPassword = Utils::randomString();

            $emailMember = Member::query()
                ->where(function ($query) use ($inputEmail)  {
                    $query->where(self::FIRST_EMAIL, $inputEmail)
                        ->orWhere('secondEmail', $inputEmail);
                    })
                ->whereNull('is_delete')
                ->first();

            if (!empty($emailMember)) {
                return redirect()->route('front_end_inforegister')->withInput()->withErrors(['Email đã tồn tại. Vui lòng chọn email khác']);
            }

            $birthDate = $request->input(self::BIRTH_DATE);
            $birthMonth = $request->input(self::BIRTH_MONTH);
            $birthYear = $request->input(self::BIRTH_YEAR);
            $expirationDate = str_replace('/', '-', $request->input(self::EXPIRATION_DATE));
            $objMem = new Member();
            $objMem->fullName = $fullName;
            $objMem->firstEmail = $inputEmail;
            $objMem->firstMobile = $request->input(self::FIRST_MOBILE);
            $objMem->touristGuideCode = $touristGuideCode;
            $objMem->expirationDate = date('Y-m-d H:i:s', strtotime($expirationDate));
            $objMem->gender = $request->input(self::GENDER);
            $objMem->typeOfTravelGuide = $request->input(self::TYPE_OF_TRAVEL_GUIDE);
            $objMem->province = $request->input(self::PROVINCE);
            $objMem->birthday = $birthYear . '-' . $birthMonth . '-' . $birthDate . ' 00:00:00';
            $objMem->status = -1;
            $objMem->rank_status = MemberConstants::RANK_REGISTER;
            $objMem->save();

            MemberServiceProvider::createMemberRank($objMem->id, [self::MAJORS => $rankId]);
            $mailTemplate = 'rank_registered_not_member';
            $objUser = User::query()->create([
                'fullname'          => $fullName,
                'username'          => $touristGuideCode,
                'email'             => $inputEmail,
                'role'              => 1,
                'status'            => 1,
                'password'          => bcrypt($defaultPassword),
                'memberId'          => $objMem->id,
            ]);

            if (empty($objUser)) {
                DB::rollBack();
                return redirect()->route('front_end_inforegister')
                    ->withErrors(['Tạo mới người dùng không phải hội viên thất bại'])
                    ->withInput();
            }
            self::_sendRegisterRankNotMemberMail($objUser, $defaultPassword, $mailTemplate);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->route('front_end_inforegister')->withInput()->withErrors(['Đăng ký không thành công!']);
        }

        return redirect()->route('front_end_inforegister')->with(CommontConstants::SUCCESSES, 'Ông/bà đã đăng ký '. Rank::find($rankId)->name ?? '' . '. Vui lòng kiểm tra email để đăng nhập');
    }

    private static function _sendApprovedMail($name, $email, $mailTemplate)
    {
        if (!empty($email)) {
            try {
                $to = [['address' => $email, 'name' => $name]];
                $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
                $getEmail = Emails::query()
                    ->select('content', 'attach')
                    ->where('option_code', self::EMAIL05)
                    ->first();
                
                $attach = null;
                if (!empty($getEmail)) {
                    $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($name), $getEmail->content);
    
                    if (!empty($getEmail->attach)) {
                        $attach = [
                            'realpath' => $getEmail->attach,
                            'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                            'mime' => 'pdf'
                        ];
                    }
                }

                $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
                $template = 'admin.member_rank.email.' . $mailTemplate;
                dispatch((new SendEmail($template, $data, $to, $bcc, $getEmail->title ?? '', $attach))->delay(5));
            } catch (\Exception $exception) {
                LogsHelper::trackByFile('send_mail_rank_registered', 'Error when send rank registered mail(Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$name], true) . ')');
            }
        }
    }

    private static function _sendRegisterRankNotMemberMail($objUser, $password, $mailTemplate)
    {

        try {
            $to = [['address' => $objUser->email, 'name' => $objUser->fullname]];
            $bcc = [['address' => self::BCC_EMAIL, 'name' => self::BCC_EMAIL]];
            $getEmail = Emails::query()
                ->select('content', 'attach')
                ->where('option_code', self::EMAIL16)
                ->first();

            $attach = null;
            if (!empty($getEmail)) {
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_TEN, htmlentities($objUser->fullname), $getEmail->content);
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_USERNAME, htmlentities($objUser->username), $getEmail->content);
                $getEmail->content = str_replace(CommontConstants::PLACEHOLDER_PASSWORD, htmlentities($password), $getEmail->content);

                if (!empty($getEmail->attach)) {
                    $attach = [
                        'realpath' => $getEmail->attach,
                        'as' => substr($getEmail->attach, strpos($getEmail->attach, 'pdf\\') + 4),
                        'mime' => 'pdf'
                    ];
                }
            }

            $data = ['email' => !empty($getEmail) ? $getEmail->content : ''];
            $template = 'admin.member_rank.email.' . $mailTemplate;
            dispatch((new SendEmail($template, $data, $to, $bcc, $getEmail->title ?? '', $attach))->delay(5));
        } catch (\Exception $exception) {
            LogsHelper::trackByFile('send_mail_rank_registered_not_member', 'Error when send rank registered not member mail(Exception: ' . $exception->getMessage() . ' on Line ' . $exception->getLine() . ' in File ' . $exception->getFile() . ', Data: ' . print_r([$objUser->fullName], true) . ')');
        }
    }

    public function joinQuiz() {
        $memberId = Auth::user()->memberId ?? 0;
        $member = MemberServiceProvider::getMemberById($memberId);
        if (empty($member)) {
            abort(404);
        }
        $member_rank_status = $member->rank_status;
        if (!in_array($member_rank_status, [MemberConstants::RANK_REGISTER, MemberConstants::RANK_RANKED])) {
            abort(404);
        }

        $registered_rank_name = MemberRank::getRankIdAndNameByMemberId($memberId);
        if (empty($registered_rank_name)) {
            abort(404);
        }
        $quiz_status = OpenQuizProvider::getQuizStatus($memberId);
        $open = QuizConstants::OPENED_QUIZ;
        $member_score = MemberRank::getAllByJoinDate($memberId, $registered_rank_name->id);

        return view('frontend.newdesign.survey.joinquiz', compact('registered_rank_name', 'quiz_status', 'open', 'member_score'));
    }

    public function quiz(Request $request) {
        $memberId = Auth::user()->memberId ?? 0;
        $member = MemberServiceProvider::getMemberById($memberId);
        if (empty($member)) {
            abort(404);
        }

        $member_rank_status = $member->rank_status;
        if (!in_array($member_rank_status, [MemberConstants::RANK_REGISTER, MemberConstants::RANK_RANKED])) {
            abort(404);
        }

        DB::beginTransaction();
        try {
            $quiz_status = OpenQuizProvider::getQuizStatus($memberId);
            if ($quiz_status->open != QuizConstants::OPENED_QUIZ) {
                AnswerTmp::deleteAllAnswerByMemberID($memberId);
                QuestionTmp::deleteAllQuestionByMemberID($memberId);
                QuizTmp::deleteTimeByMemberID($memberId);
                DB::commit();
                return redirect()->route('front_end_joinquiz')->withErrors(['Phần thi kiến thức xếp hạng HDV đóng']);
            }

            $checkQuestion = QuestionTmp::query()->where('member_id', $memberId)->first();
            if (!$checkQuestion) {
                $number_of_3_stars_local_question = QuizConstants::QUESTION_3_STAR_LOCAL;
                $number_of_3_stars_international_question = QuizConstants::QUESTION_3_STAR_INTERNATIONAL;

                $question = QuestionProvider::getFirstRandomQuestions($number_of_3_stars_international_question, RankConstants::RANK_3_STAR, $number_of_3_stars_international_question[1]);
                $registered_rank = MemberRank::getRankIDByMemberId($memberId);
                if (empty($registered_rank)) {
                    return redirect()->route('front_end_joinquiz')->withErrors(['Bạn chưa đăng ký xếp hạng HDV']);
                }

                unset($number_of_3_stars_local_question[1]);
                unset($number_of_3_stars_international_question[1]);
                if ($registered_rank->rank_id == RankConstants::HDV_QT_5_STAR) {
                    $number_of_4_stars_international_question = QuizConstants::QUESTION_4_STAR_INTERNATIONAL;
                    $number_of_5_stars_international_question = QuizConstants::QUESTION_5_STAR_INTERNATIONAL;

                    foreach ($number_of_3_stars_international_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_3_STAR, $value);
                    }

                    foreach ($number_of_4_stars_international_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_4_STAR, $value);
                    }

                    foreach ($number_of_5_stars_international_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_5_STAR, $value);
                    }
                }
                else if ($registered_rank->rank_id == RankConstants::HDV_QT_4_STAR) {
                    $number_of_4_stars_international_question = QuizConstants::QUESTION_4_STAR_INTERNATIONAL;
                    foreach ($number_of_3_stars_international_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_3_STAR, $value);
                    }

                    foreach ($number_of_4_stars_international_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_4_STAR, $value);
                    }
                }
                else if ($registered_rank->rank_id == RankConstants::HDV_QT_3_STAR) {
                    foreach ($number_of_3_stars_international_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_3_STAR, $value);
                    }
                }
                else if ($registered_rank->rank_id == RankConstants::HDV_ND_5_STAR || $registered_rank->rank_id == RankConstants::HDV_TD_5_STAR) {
                    $number_of_4_stars_local_question = QuizConstants::QUESTION_4_STAR_LOCAL;
                    $number_of_5_stars_local_question = QuizConstants::QUESTION_5_STAR_LOCAL;

                    foreach ($number_of_3_stars_local_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_3_STAR, $value);
                    }

                    foreach ($number_of_4_stars_local_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_4_STAR, $value);
                    }

                    foreach ($number_of_5_stars_local_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_5_STAR, $value);
                    }
                }
                else if ($registered_rank->rank_id == RankConstants::HDV_ND_4_STAR || $registered_rank->rank_id == RankConstants::HDV_TD_4_STAR) {
                    $number_of_4_stars_local_question = QuizConstants::QUESTION_4_STAR_LOCAL;
                    foreach ($number_of_3_stars_local_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_3_STAR, $value);
                    }

                    foreach ($number_of_4_stars_local_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_4_STAR, $value);
                    }
                }
                else if ($registered_rank->rank_id == RankConstants::HDV_ND_3_STAR || $registered_rank->rank_id == RankConstants::HDV_TD_3_STAR) {
                    foreach ($number_of_3_stars_local_question as $key => $value) {
                        $question = QuestionProvider::unionAllQuestion($question, $key, RankConstants::RANK_3_STAR, $value);
                    }
                }

                $randomQuestions = $question->get();
                $random_tmp = [];
                $dataDisplayArr = [];
                $dataTmp = [];
                $increment = 1;
                foreach ($randomQuestions as $questions) {
                    $dataInsert = [
                        'question_id' => $questions->id,
                        'subject_knowledge_id' => $questions->subject_knowledge_id,
                        'member_id' => $memberId,
                        'content' => $questions->content,
                        'answer1' => $questions->answer1,
                        'answer2' => $questions->answer2,
                        'answer3' => $questions->answer3,
                        'answer4' => $questions->answer4,
                        'answer_correct' => $questions->answer_correct,
                        'created_at' => date('Y-m-d H:i:s')
                    ];
                    $dataTmp = array_merge($dataInsert, ['id' => $increment]);
                    $random_tmp[] = $dataInsert;
                    $dataDisplayArr[] = $dataTmp;
                    $increment++;
                }

                QuestionTmp::query()->insert($random_tmp);
                $dt = Carbon::now();
                $quiz_tmp = new QuizTmp();
                $quiz_tmp->member_id = $memberId;
                $quiz_tmp->start_time = $dt->toDateTimeString();

                $callback = function ($rankId) {
                    $quizTimes = [
                        RankConstants::HDV_QT_5_STAR => QuizConstants::$time['5_STAR_INTERNATIONAL'],
                        RankConstants::HDV_QT_4_STAR => QuizConstants::$time['4_STAR_INTERNATIONAL'],
                        RankConstants::HDV_QT_3_STAR => QuizConstants::$time['3_STAR_INTERNATIONAL'],
                        RankConstants::HDV_ND_5_STAR => QuizConstants::$time['5_STAR_LOCAL'],
                        RankConstants::HDV_ND_4_STAR => QuizConstants::$time['4_STAR_LOCAL'],
                        RankConstants::HDV_ND_3_STAR => QuizConstants::$time['3_STAR_LOCAL'],
                    ];
                    return $quizTimes[$rankId] ?? [0];
                };

                $min = $callback($registered_rank->rank_id);
                $dt->addMinutes($min);
                $quiz_tmp->join_time = $dt->toDateTimeString();
                $quiz_tmp->current_page = 1;
                $quiz_tmp->save();
                $users = $dataDisplayArr;
                $questionTotal = count($dataDisplayArr);
                $now = $quiz_tmp;
            } else {
                $users = QuestionTmp::query()->where('member_id', $memberId)->get();
                $questionTotal = $users->count();
                $now = QuizTmp::getTimeByMemberID($memberId);
            }
            $subjectKnowledges = SubjectKnowledge::getAllSubjectKnowledgeIDName();
            $quizNavigations = [];
            $incre = 0;
            for ($i = 1; $i <= $questionTotal; $i++) {
                $quizNavigations[$incre][] = $i;
                if ($i % 4 === 0) {
                    $incre++;
                }
            }
            DB::commit();
            return view('frontend.test', compact('users', 'isSubmit', 'now', 'subjectKnowledges', 'quizNavigations'));
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('front_end_joinquiz')->withErrors(['Có lỗi xảy ra trước khi vào quiz']);
        }

    }

    public function checkAnswer(Request $request) {
        $memberId = Auth::user()->memberId ?? 0;
        $this->saveAnswerTmp($memberId, $request->all());
        DB::beginTransaction();
        try {
            $baseQuestionScore = 0;
            $subjectQuestionScore = 0;

            $fetchQuestionAnswered = QuestionProvider::getAnswerMember($memberId);
            foreach (array_diff_key(QuizConstants::iniAnsweredQuestion, $fetchQuestionAnswered) as $key => $value) {
                $fetchQuestionAnswer[$key] = $value;
            }

            $memberRank = MemberRank::getInfoRankByMemberId($memberId);
            list($base, $sub) = Utils::getBaseAndSubjectKnowledge($memberRank->rank_id);

            $checkMember = new MemberRank();
            foreach ($fetchQuestionAnswered as $knowledgeId => $total) {
                if ($knowledgeId === QuizConstants::BASE_QUESTION) {
                    $checkMember->base_question = $total;
                    $baseQuestionScore = round($total / $base * 40);
                } else {
                    $checkMember->subject_question = $total;
                    $subjectQuestionScore = round($total / $sub * 60);
                }
            }

            $getQuizTmp = QuizTmp::getTimeByMemberID($memberId);
            $checkMember->member_id = $memberId;
            $checkMember->rank_id = $memberRank->rank_id;
            $checkMember->score = $baseQuestionScore + $subjectQuestionScore;
            $checkMember->join_date = $getQuizTmp->start_time;
            $checkMember->save();

            //check join_date null => xoa
            MemberRank::deleleJoinDateNull($memberId);
            AnswerTmp::deleteAllAnswerByMemberID($memberId);
            QuestionTmp::deleteAllQuestionByMemberID($memberId);
            QuizTmp::deleteTimeByMemberID($memberId);
            DB::commit();
            return redirect()->route('front_end_joinquiz')->with($this::SUCCESSES, ['Bạn vừa hoàn thành phần thi kiến thức xếp hạng HDV']);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('front_end_joinquiz')->withErrors(['Có lỗi xảy ra khi trả lời câu hỏi']);
        }

    }

    private function saveAnswerTmp($memberId, $items) {
	    $arrAnswer = [];
        foreach ($items as $key => $value) {
            $arrAnswer[] = [
                'question_id' => $key,
                'member_id' => $memberId,
                'answer' => $value,
            ];
        }

        if (count($arrAnswer)) {
            DB::table('answer_tmp')->insert($arrAnswer);
        }
    }
}
