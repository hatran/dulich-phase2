<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontEndBaseController;
use App\Models\Offices;
use App\Models\EmployeesPosition;

/**
 * Class BranchesController
 * @package App\Http\Controllers\Frontend
 */
class BranchesController extends FrontEndBaseController
{

    const POSITION_KEY = 'position';

    protected $_officeArray = null;
    protected $_listManager = null;
    protected $_countOffice = 0;

    public function __construct()
    {
        parent::__construct();
        $this->getOfficesArray();
        $this->_countOffice = count($this->_officeArray) + 1;
    }

    public function hoivien()
    {
        $office = $this->_officeArray;
        $officeCount = $this->_countOffice;
        return view('frontend.newdesign.vpdd', compact('office', 'officeCount'));
    }

    public function getLanhDao($id)
    {
        $listManager = $this->getEmployeesPositionByBranchId($id);
        return $listManager;
    }

    public function getEmployeesPositionByBranchId($id) {
        return EmployeesPosition::where('branch_id', $id)->with(['employees'])->orderBy('created_at', 'asc')->get();
    }

    public function getOfficesArray()
    {
        $objOffices = Offices::where('status',1)
            ->whereNull('parent_id')
            ->whereIn('option_code',['HEAD','OFFICES'])
            ->whereNull('deleted_at')
            ->orderByRaw("FIELD(option_code , 'HEAD', 'OFFICES') ASC")
            ->get();
        $arrayOffices = $objOffices->toArray();

        // html entity
        $resultArray = array();
        foreach ($arrayOffices as $objOffices) {
            $resultItem = array();
            foreach ($objOffices as $key => $item) {
                $resultItem[$key] = e($item);
            }
            $resultItem['manage'] = $this->getOnlyGiamDoc($this->getLanhDao($objOffices['id']));
            $resultArray[] = $resultItem;
        }
        $lastResult = array();
        foreach ($resultArray as $key => $office) {
            if (!empty($office['parent_id'])) continue;
            $lastResult[] = $office;
        }
        $this->_officeArray = $lastResult;
    }

    public function getOnlyGiamDoc($managers){
        $result = null;
        foreach($managers as $manager){
            if($manager['option_code'] == 'cdddd'){
                return $manager;
            }
        }
        return null;
    }

}
