<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Member;
use App\Constants\MemberConstants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class VerifyOfficalMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $touristCode = $request['touristCode'];

        $username = $request['username'];
        
        $password = $request['password'];

        if(empty($touristCode)) {
            $res['message'] = "Lỗi thông tin không hợp lệ, xem xét lại param touristCode";
            $res['errorCode'] = "2";
            $res['memberInfo'] = null;    
        } else {
            $findMember = DB::table('members')->where('status',MemberConstants::MEMBER_OFFICIAL_MEMBER)->whereNull('deleteAt')->whereNull('is_delete')->where('touristGuideCode',$touristCode)->first();
            if(empty($findMember)) {
                $res['message'] = "Lỗi thông tin không hợp lệ, xem xét lại param touristCode";
                $res['errorCode'] = "2";
                $res['memberInfo'] = null; 
            } else {
                $res['message'] = "Success";
                $res['errorCode'] = "0";
				$memberInfo['hovaten'] = $findMember->fullName;
                $memberInfo['ma_the_hdv'] = $findMember->touristGuideCode; 
                $memberInfo['ma_the_hv'] = $findMember->member_code;
				$memberInfo['ngay_gia_nhap'] = $findMember->member_from;
				$memberInfo['han_the_hv'] = date('d/m/Y', strtotime($findMember->member_code_expiration));$memberInfo['status'] = array_get(MemberConstants::$file_const_display_status, $findMember->status, '');
                $res['memberInfo'] = $memberInfo;
            }
        }

        return response()->json(['data' =>  $res], 200);
    }
}
