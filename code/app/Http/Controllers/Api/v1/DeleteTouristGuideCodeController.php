<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DeleteTouristGuideCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $touristGuideCode = Member::select(DB::raw('touristGuideCode, count(touristGuideCode) as code_count'))        ->whereIn('status', [-1, 1])
                    ->whereNull('is_delete')
                    ->whereNotNull('touristGuideCode')
                    ->where('touristGuideCode', '<>', "' '")
                    ->groupBy('touristGuideCode');

        $touristGuideCodeArr = [];
        $touristGuideCode->chunk(100, function ($codes) use (&$touristGuideCodeArr) {
            foreach ($codes as $code) {
                if ($code->code_count > 1) {
                    $touristGuideCodeArr[] = $code->touristGuideCode;
                }
            }
        });
     
        $status = Member::whereIn('touristGuideCode', $touristGuideCodeArr)->where('status' , -1)->delete();

        return true;
    }
}
