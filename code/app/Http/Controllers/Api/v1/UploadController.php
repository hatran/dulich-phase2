<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests\StoreFileRequest;
use App\Libs\Storage\StorePhoto;
use App\Models\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreFileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFileRequest $request)
    {
        //
        $file = $request->file('file');

        if (empty($file))
            return response()->json(['error' => 'File can not empty.'], 400);

        $filePrefix = uniqid();

        $storeFile = new StorePhoto($file->getRealPath(), $filePrefix);
        $storeFile->moveToCollection();


        $fileCreated = File::create([
            'originalName' => $file->getClientOriginalName(),
            'absPath' => $storeFile->getRealPath(),
            'relativeUrl' => $storeFile->getRelativeUrl(),
            'mime' => $storeFile->getFileInfo()->getMimeType(),
            'size' => $storeFile->getFileInfo()->getSize(),
            'status' => 1,
        ]);

        if ($fileCreated)

            return $fileCreated ? response()->json(['success' => 'files' . DIRECTORY_SEPARATOR . $storeFile->getRelativeUrl()], 200)
                : response()->json(['error' => 'error happened'], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
