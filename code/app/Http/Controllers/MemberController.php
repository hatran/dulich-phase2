<?php

namespace App\Http\Controllers;

use App\Constants\MemberConstants;
use App\Http\Controllers\Admin\Traits\DownloadZipFolder;
use App\Jobs\SendSms;
use App\Libs\Helpers\Utils;
use App\Models\Branches;
use App\Models\EducationBranch;
use App\Models\EducationDegree;
use App\Models\GroupSize;
use App\Models\Language;
use App\Models\LanguageLevel;
use App\Models\Major;
use App\Models\MajorSkill;
use App\Models\TypeGuide;
use App\Models\Member;
use App\Models\OtherConstant;
use App\Models\WorkConstant;
use App\Models\Option;
use App\Providers\MemberServiceProvider;
use App\Rules\RequiredExpireDate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use UrlSigner;
use App\Models\ForteTour;
use App\Models\Rank;
use App\Models\MemberRank;

class MemberController extends Controller
{
    use DownloadZipFolder;

    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const MEMBER_ID = 'memberId';
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const FULL_NAME = 'fullName';
    const GENDER = 'gender';
    const BIRTHDAY = 'birthday';
    const DOB = 'bod';
    const BIRTH_YEAR = 'birthYear';
    const BIRTH_MONTH = 'birthMonth';
    const BIRTH_DATE = 'birthDate';
    const CMT_CCCD = 'cmtCccd';
    const ISSUED_BY = 'issuedBy';
    const PERMANENT_ADDRESS = 'permanentAddress';
    const ADDRESS = 'address';
    const FIRST_MOBILE = 'firstMobile';
    const SECOND_MOBILE = 'secondMobile';
    const FIRST_EMAIL = 'firstEmail';
    const SECOND_EMAIL = 'secondEmail';
    const EXPERIENCE_YEAR = 'experienceYear';
    const INBOUND_OUTBOUND = 'inboundOutbound';
    const EXPERIENCE_LEVEL = 'experienceLevel';
    const OTHER_SKILLS = 'otherSkills';
    const OTHER_INFORMATION = 'otherInformation';
    const TOURIST_GUIDE_LEVEL = 'touristGuideLevel';
    const ACHIEVEMENTS = 'achievements';
    const STATUS = 'status';
    const PROVINCE_CODE = 'province_code';
    const IS_VERIFIED = 'is_verified';
    const IS_FEE = 'is_fee';
    const IS_SIGNED = 'is_signed';
    const MEMBER_TYPE = 'member_type';
    const GUIDE_LANGUAGE = 'guideLanguage';
    const EDUCATIONS = 'educations';
    const BRANCH_ID = 'branchId';
    const EDUCATION_BRANCHES = 'educationBranches';
    const DEGREE_ID = 'degreeId';
    const EDUCATION_DEGREES = 'educationDegrees';
    const FILE = 'file';
    const EDUCATION_FILES = 'educationFiles';
    const LANGUAGE = 'language';
    const LEVEL2 = 'level2';
    const LANGUAGE_ID = 'languageId';
    const LEVEL_ID = 'levelId';
    const MAJOR_SKILLS = 'majorSkills';
    const IDS = 'ids';
    const MAJORS = 'majors';
    const FILES = 'files';
    const MAJOR_FILES = 'majorFiles';
    const LANGUAGE_SKILLS = 'languageSkills';
    const LANGUAGES = 'languages';
    const LANGUAGE_LEVELS = 'languageLevels';
    const ELEMENTS = 'elements';
    const WORK_HISTORY = 'workHistory';
    const WORK_ELEMENT_NAMES = 'workElementNames';
    const WORK_TYPE_OF_CONTRACTS = 'workTypeOfContracts';
    const WORK_FROM_DATES = 'workFromDates';
    const WORK_TO_DATES = 'workToDates';
    const ELEMENT_NAME = 'elementName';
    const TYPE_OF_CONTRACT = 'typeOfContract';
    const FROM_DATE = 'fromDate';
    const TO_DATE = 'toDate';
    const GROUP_SIZE_ID = 'groupSizeId';
    const GROUP_SIZES = 'groupSizes';
    const TYPE_GUIDE_ID = 'typeGuideId';
    const TYPE_GUIDES = 'typeGuides';
    const AVATAR = 'avatar';
    const USER_PHOTO = 'userPhoto';
    const FILE_NAME = 'fileName';
    const ACHIEVEMENT_FILES = 'achievementFiles';
    const TYPE_OF_PLACE = 'typeOfPlace';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const EXPIRATION_DATE = 'expirationDate';
    const EMAIL_VERIFIED = 'emailVerified';
    const PHONE_VERIFIED = 'phoneVerified';
    const DATE_ISSUED = 'dateIssued';
    const COOKIE_REGISTER_TOKEN = 'regis_token';
    const CLUB = 'club';
    const OBJ_MEMBER = 'objMember';
    const OBJ_MEM = 'objMem';
    const MEMBER_BELONGS_TO_CHI_HOI = 1;
    const MEMBER_BELONGS_TO_HOI = 2;
    const LOCAL_TRAVEL_GUIDE = 3;
    const PROVINCE = 'province';
    const ACCEPTED = 'acceptTermsAndPolicies';
    const DELETED = 'is_delete';
    const SUCCESSES = 'successes';
    const MAJORS_SKILLS = 'objMajorSkills';
    const OBJ_MAJORS = 'objMajor';
    const OBJ_LANGUAGE = 'objLanguage';
    const OBJ_LANGUAGE_LEVEL = 'objLanguageLevel';
    const WORKHISTORY = 'workHistories';
    const TYPE_GUIDE_NAME = 'typeGuideName';
    const GROUP_SIZE_NAME = 'groupSizeName';
    const BRAND = 'objBrand';
    const DEGREE = 'objDegree';
    const OBJ_PAYMENT = 'paymentInfor';
    const OBJ_NOTES = 'notes';
    const POSITION = 'position';
    const PROFILE_IMG = 'profileImg';
    const FORTE_TOUR = 'forteTour';

    protected $rules = [
        // 'userPhoto'      => 'required|image|dimensions:max_width=100,max_height=100', // remove check image size 20180102
        self::FULL_NAME             => 'required|max:50',
        self::AVATAR                => 'required',
        self::GENDER                => 'required|in:1,2',
        self::TYPE_OF_PLACE         => 'required|in:1,2',
        self::BIRTH_YEAR            => 'required|max:4',
        self::BIRTH_MONTH           => 'required|max:2',
        self::BIRTH_DATE            => 'required|max:2',
        self::DOB                   => 'date|before:today',
        self::TYPE_OF_TRAVEL_GUIDE  => 'required|in:1,2,3',
        self::EXPIRATION_DATE       => 'required_if:typeOfTravelGuide,1|required_if:typeOfTravelGuide,2',
        self::INBOUND_OUTBOUND      => 'required_if:typeOfTravelGuide,1',
        self::CMT_CCCD              => 'required',
        self::DATE_ISSUED           => 'required|date_format:"d/m/Y"',
        self::ISSUED_BY             => 'required',
        self::PERMANENT_ADDRESS     => 'required',
        self::ADDRESS               => 'required',
        self::FIRST_MOBILE          => 'required|numeric',
        self::SECOND_MOBILE         => 'nullable|numeric',
        self::FIRST_EMAIL           => 'required|email',
        self::SECOND_EMAIL          => 'nullable|email',
        self::PROVINCE_CODE         => 'required_if:typeOfPlace,1',
        self::CLUB                  => 'required_if:typeOfPlace,2',
        self::EDUCATION_BRANCHES    => 'required',
        self::EDUCATION_DEGREES     => 'required',
        self::LANGUAGE              => 'required_if:typeOfTravelGuide,1',
        self::LEVEL2                => 'required_if:typeOfTravelGuide,1',
        self::TYPE_GUIDES           => 'required',
        self::GROUP_SIZES           => 'required',
        self::EXPERIENCE_YEAR       => 'required|max:4',
        self::EXPERIENCE_LEVEL      => 'required',
        self::GUIDE_LANGUAGE        => 'required',
        self::MAJORS                => 'required',
        self::PROVINCE              => 'required',
    ];

    protected $messages = [
        'fullName.required'               => 'Họ và tên là trường bắt buộc',
        'fullName.max'                    => 'Họ và tên chỉ được điền tối đa :max chữ số',
        'gender.required'                 => 'Giới tính là trường bắt buộc',
        'gender.in'                       => 'Giới tính không hợp lệ',
        'birthDate.required'              => 'Ngày sinh là trường bắt buộc',
        'birthDate.max'                   => 'Ngày sinh chỉ được điền tối đa :max số',
        'birthMonth.required'             => 'Tháng sinh là trường bắt buộc',
        'birthMonth.max'                  => 'Tháng sinh chỉ được điền tối đa :max số',
        'birthYear.required'              => 'Năm sinh là trường bắt buộc',
        'birthYear.max'                   => 'Năm sinh chỉ được điền tối đa :max số',
        'bod.date'                        => 'Ngày tháng năm sinh không hợp lệ',
        'bod.before'                      => 'Ngày tháng năm sinh không hợp lệ, hướng dẫn viên dường như quá nhỏ tuổi',
        'profileImg.required'             => 'Thiếu ảnh chân dung',
        'touristGuideCode.required'       => 'Số thẻ HDV du lịch là trường bắt buộc',
        'touristGuideCode.digits'         => 'Số thẻ HDV du lịch là số có 9 chữ số, vui lòng nhập lại',
        'touristGuideCode.unique'         => 'Số thẻ HDV du lịch đã tồn tại',
        'cmtCccd.required'                => 'Số CMT/CCCD  là trường bắt buộc',
        'cmtCccd.unique'                  => 'Số CMT/CCCD  đã tồn tại',
        'dateIssued.required'             => 'Ngày cấp CMT/CCCD là trường bắt buộc',
        'dateIssued.date_format'          => 'Ngày cấp CMT/CCCD không đúng định dạng',
        'issuedBy.required'               => 'Nơi cấp ngày CMT/CCCD là trường bắt buộc',
        'permanentAddress.required'       => 'Địa chỉ thường trú là trường bắt buộc',
        'address.required'                => 'Địa chỉ liên hệ là trường bắt buộc',
        'firstMobile.required'            => 'Điện thoại di động là trường bắt buộc',
        'firstMobile.numeric'             => 'Điện thoại di động phải có dạng số',
        'secondMobile.numeric'            => 'Điện thoại di động phải có dạng số',
        'firstEmail.required'             => 'Email là trường bắt buộc',
        'firstEmail.email'                => 'Email sai định dạng',
        'secondEmail.email'               => 'Email sai định dạng',
        'guideLanguage.required'          => 'Ngôn ngữ hướng dẫn là trường bắt buộc',
        'typeOfPlace.required'            => 'Đăng ký sinh hoạt tại là trường bắt buộc',
        'typeOfPlace.in'                  => 'Đăng ký sinh hoạt tại không hợp lệ',
        'typeOfTravelGuide.required'      => 'Hướng dẫn viên là trường bắt buộc phải điền',
        'typeOfTravelGuide.in'            => 'Hướng dẫn viên không hợp lệ',
        'expireDate.required'             => 'Ngày hết hạn là trường bắt buộc phải điền',
        // 'expirationDate.required_if'      => 'Bạn phải điền Ngày hết hạn đối với HDV du lịch tại điểm',
        'expirationDate.required'         => 'Ngày hết hạn đối là trướng bắt buộc phải điền',
        'inboundOutbound.required_if'     => 'Bạn phải chọn Inbound / Outbound đối với HDV du lịch quốc tế',
        'province_code.required_if'       => 'Bạn phải chọn chi hội nơi đăng ký sinh hoạt',
        'club.required_if'                => 'Bạn phải chọn CLB thuộc hội đăng ký sinh hoạt',
        'educationBranches.required'      => 'Chuyên ngành là trường bắt buộc',
        'educationDegrees.required'       => 'Trình độ là trường bắt buộc',
        'major.required'                  => 'Thông tin về nghiệp vụ là trường bắt buộc',
        'language.required_if'            => 'Chuyên ngành ngoại ngữ là trường bắt buộc đối với HDV du lịch quốc tế',
        'level2.required_if'              => 'Trình độ ngoại ngữ là trường bắt buộc đối với HDV du lịch quốc tế',
        'workElementNames[1].required'    => 'Tên doanh nghiệp đã và đang công tác là trường bắt buộc',
        'workTypeOfContracts[1].required' => 'Hình thức lao động là trường bắt buộc',
        'workFromDates[1].required'       => 'Thời gian bắt thúc là trường bắt buộc',
        'workToDates[1].required'         => 'Thời gian kết thúc là trường bắt buộc',
        'typeGuides.required'             => 'Loại hình là trường bắt buộc',
        'groupSizes.required'             => 'Quy mô đoàn là trường bắt buộc',
        'experienceYear.required'         => 'Năm bắt đầu hướng dẫn là trường bắt buộc',
        'experienceYear.max'              => 'Năm bắt đầu hướng dẫn không hợp lệ',
        'experienceLevel.required'        => 'Trình độ là trường bắt buộc',
        'majors.required'                 => 'Thông tin về nghiệp vụ hướng dẫn là trường bắt buộc',
        'province.required'               => 'Thông tin tỉnh thành là trường bắt buộc',
        'forteTour[1].required'           => 'Tuyến du lịch là trường bắt buộc',
    ];

    private function getRegisterStep(Request $request)
    {
        $step = intval($request->input('step', 1));
        if ($step < 1 || $step > 3) {
            $step = 1;
        }
        return $step;
    }

    /**
     * Show the application dashboard.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function getRegister(Request $request)
    {
        switch ($this->getRegisterStep($request)) {
            case 3:
                return $this->getStep3($request);
            case 2:
                return $this->getStep2($request);
            default:
                return $this->getStep1($request);
        }
    }

    public function getStep1(Request $request)
    {
        if (!empty($id = Cookie::get(self::COOKIE_REGISTER_TOKEN)) && $request->input('step') == '1') {
            $objMember = MemberServiceProvider::getMemberById($id);
            $objMem = MemberServiceProvider::getMemberForteTourAndGuideLanguageById($id);
        }

        if(!empty($objMember)) {
            MemberServiceProvider::undoPhoneVerified($id);
        }

        $objMajors = Major::where('status', 1)->orderBy('majorName', 'asc')->get();
        $position  = count($objMajors) + 29;
        return view('member.register.step1', [
            'step' => 1,
            self::EDUCATION_BRANCHES => EducationBranch::where('status', 1)->orderBy('branchName', 'asc')->get(),
            self::EDUCATION_DEGREES => EducationDegree::where('status', 1)->orderBy('degree', 'asc')->get(),
            self::MAJORS => $objMajors,
            self::LANGUAGES => Language::where('status', 1)->orderBy('position', 'asc')->get(),
            self::LANGUAGE_LEVELS => LanguageLevel::where('status', 1)->orderBy('levelName', 'asc')->get(),
            self::GROUP_SIZES => GroupSize::where('status', 1)->orderBy('id', 'asc')->get(),
            self::TYPE_GUIDES => TypeGuide::where('status', 1)->orderBy('typeName', 'asc')->get(),
            self::OBJ_MEMBER => empty($objMember) ? null : $objMember,
            self::OBJ_MEM => empty($objMem) ? null : $objMem,
            self::POSITION => $position,
            self::FORTE_TOUR => ForteTour::where('status', 1)->orderBy('id', 'asc')->get(),
            'branch_chihoi' => Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get(),
            'branch_clbthuochoi' => Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get(),
            'otherConstant' => OtherConstant::where('status', 1)->orderBy('id', 'asc')->get(),
            'workConstant' => WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get(),
            'provincial' => Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get()
        ]);
    }

    public function getStep2(Request $request)
    {
        if (!$request->has('id')) {
            abort(404);
        }
        $memberId = $request->input('id');

        /*$objMemberCheck = MemberServiceProvider::getMemberById($memberId);
        if (!empty($objMemberCheck)) {
            abort(404);
        }*/

        // $phoneToken = MemberServiceProvider::getPhoneToken($memberId);
        return view('member.register.step3', [
            'step' => 2,
            self::MEMBER_ID => $memberId,
            // 'phoneToken' => $phoneToken
        ]);
    }

    public function getStep3(Request $request)
    {
        if (!$request->has('id')) {
            abort(404);
        }

        /*$memberId = $request->input('id');
        $objMemberCheck = MemberServiceProvider::getMemberById($memberId);
        if (!empty($objMemberCheck)) {
            abort(404);
        }*/

        return view('member.register.step4', [
            'step' => 3,
            self::MEMBER_ID => $request->input('id'),
        ]);
    }

    public function postRegister(Request $request)
    {
        switch ($this->getRegisterStep($request)) {
            case 3:
                return $this->postStep3($request);
            case 2:
                return $this->postStep2($request);
            default:
                return $this->postStep1($request);
        }
    }

    private function normalizedTouristGuideCodeFrom(Request $request)
    {
        $this->rules[self::TOURIST_GUIDE_CODE] = [
            'required',
            'digits:9',
            Rule::unique('members')->where(function ($query) {
                return $query->where(self::PHONE_VERIFIED, 1)->where(self::ACCEPTED, 1)->whereNull(self::DELETED);
            })
        ];

        $touristGuideCode = $request->input(self::TOURIST_GUIDE_CODE);
        $touristGuideCode = trim($touristGuideCode);
        $touristGuideCode = str_replace(' ', '', $touristGuideCode);
        return [self::TOURIST_GUIDE_CODE => $touristGuideCode];
    }

    private function combineDobFrom(Request $request)
    {
        return [self::DOB => $request->input(self::BIRTH_YEAR) . '-' . $request->input(self::BIRTH_MONTH) . '-' . $request->input(self::BIRTH_DATE)];
    }

    private function getEducationsFrom(Request $request)
    {
        return [
            self::BRANCH_ID => $request->input(self::EDUCATION_BRANCHES, 1),
            self::DEGREE_ID => $request->input(self::EDUCATION_DEGREES, 1),
            self::FILE => $request->file(self::EDUCATION_FILES, 0)
        ];
    }

    private function getLanguageSkillsFrom(Request $request)
    {
        if (empty($request->input(self::LANGUAGE)) || empty($request->input(self::LEVEL2))) {
            $languageSkills[] = [];
        } else {
            $languageSkills[] = [
                self::LANGUAGE_ID => $request->input(self::LANGUAGE),
                self::LEVEL_ID => $request->input(self::LEVEL2),
                self::FILE => null,
            ];
        }

        return $languageSkills;
    }

    private function getWorkHistoryFrom(Request $request)
    {
        $workHistory = [];
        $workElementNames = $request->input(self::WORK_ELEMENT_NAMES, []);
        $workTypeOfContracts = $request->input(self::WORK_TYPE_OF_CONTRACTS, []);
        $workFromDates = $request->input(self::WORK_FROM_DATES, []);
        $workToDates = $request->input(self::WORK_TO_DATES, []);
        foreach ($workElementNames as $key => $workElementName) {
            $workHistory[] = [
                self::ELEMENT_NAME => $workElementName,
                self::TYPE_OF_CONTRACT => $workTypeOfContracts[$key],
                self::FROM_DATE => isset($workFromDates[$key]) ? $workFromDates[$key] . '-01-01 00:00:00' : null,
                self::TO_DATE => isset($workToDates[$key]) ? $workToDates[$key] . '-01-01 00:00:00' : null,
            ];
        }

        return $workHistory;
    }

    private function getElementsFrom(Request $request)
    {
        return [
            self::GROUP_SIZE_ID => $request->input(self::GROUP_SIZES, 1),
            self::TYPE_GUIDE_ID => $request->input(self::TYPE_GUIDES, 1),
        ];
    }

    private function getMemberAttrsFrom(Request $request)
    {
        $educations[] = $this->getEducationsFrom($request);
        $languageSkills = $this->getLanguageSkillsFrom($request);
        $workHistory = $this->getWorkHistoryFrom($request);
        $elements[] = $this->getElementsFrom($request);

        if (empty($request->input(self::TYPE_OF_PLACE)) || $request->input(self::TYPE_OF_PLACE) == self::MEMBER_BELONGS_TO_CHI_HOI) {
            $request->merge([self::TYPE_OF_PLACE => self::MEMBER_BELONGS_TO_CHI_HOI]);
            $provinceCode = $request->input(self::PROVINCE_CODE);
        } else {
            $request->merge([self::TYPE_OF_PLACE => self::MEMBER_BELONGS_TO_HOI]);
            $provinceCode = $request->input(self::CLUB);
        }

        return [
            self::FULL_NAME => $request->input(self::FULL_NAME),
            self::GENDER => $request->input(self::GENDER),
            self::BIRTHDAY => $request->input(self::DOB),
            self::TOURIST_GUIDE_CODE => $request->input(self::TOURIST_GUIDE_CODE),
            self::EXPIRATION_DATE => empty($request->input(self::EXPIRATION_DATE)) ? null : \DateTime::createFromFormat('d/m/Y', $request->input(self::EXPIRATION_DATE))->format('Y-m-d'),
            self::CMT_CCCD => $request->input(self::CMT_CCCD),
            self::DATE_ISSUED => empty($request->input(self::DATE_ISSUED)) ? null : \DateTime::createFromFormat('d/m/Y', $request->input(self::DATE_ISSUED))->format('Y-m-d'),
            self::ISSUED_BY => $request->input(self::ISSUED_BY),
            self::PERMANENT_ADDRESS => $request->input(self::PERMANENT_ADDRESS),
            self::ADDRESS => $request->input(self::ADDRESS),
            self::FIRST_MOBILE => $request->input(self::FIRST_MOBILE),
            self::SECOND_MOBILE => $request->input(self::SECOND_MOBILE),
            self::FIRST_EMAIL => $request->input(self::FIRST_EMAIL),
            self::SECOND_EMAIL => $request->input(self::SECOND_EMAIL),
            self::TYPE_OF_TRAVEL_GUIDE => $request->input(self::TYPE_OF_TRAVEL_GUIDE),
            self::TYPE_OF_PLACE => $request->input(self::TYPE_OF_PLACE),
            self::EXPERIENCE_YEAR => $request->input(self::EXPERIENCE_YEAR),
            self::INBOUND_OUTBOUND => $request->input(self::INBOUND_OUTBOUND),
            self::EXPERIENCE_LEVEL => $request->input(self::EXPERIENCE_LEVEL),
            self::OTHER_SKILLS => $request->input(self::OTHER_SKILLS),
            self::OTHER_INFORMATION => $request->input(self::OTHER_INFORMATION),
            self::TOURIST_GUIDE_LEVEL => $request->input(self::TOURIST_GUIDE_LEVEL),
            self::ACHIEVEMENTS => $request->input(self::ACHIEVEMENTS),
            self::STATUS => MemberConstants::VERIFICATION_WAITING,
            self::PROVINCE_CODE => $provinceCode,
            self::IS_VERIFIED => null,
            self::IS_FEE => null,
            self::IS_SIGNED => null,
            self::MEMBER_TYPE => null,
            self::GUIDE_LANGUAGE => $request->input(self::GUIDE_LANGUAGE),
            self::FORTE_TOUR => $request->input(self::FORTE_TOUR),
            self::EDUCATIONS => $educations,
            self::MAJOR_SKILLS => [
                self::IDS => $request->input(self::MAJORS, null),
                self::FILES => $request->file(self::MAJOR_FILES, []),
            ],
            self::LANGUAGE_SKILLS => $languageSkills,
            self::ELEMENTS => $elements,
            self::WORK_HISTORY => $workHistory,
            self::AVATAR => $request->input(self::AVATAR),
            self::ACHIEVEMENT_FILES => $request->file(self::ACHIEVEMENT_FILES, []),
            self::PROVINCE => $request->input(self::PROVINCE),
        ];
    }

    public function postStep1(Request $request)
    {
        $request->merge($this->normalizedTouristGuideCodeFrom($request));
        $request->merge($this->combineDobFrom($request));

        $validator = Validator::make($request->all(), $this->rules, $this->messages);
        if ($validator->fails()) {
            return redirect('/member/register?step=' . $request->input('step'))
                ->withErrors($validator)
                ->withInput();
        }

        $attributes = $this->getMemberAttrsFrom($request);
        $objMember = MemberServiceProvider::createMemberWithRegistering($attributes);

        if (empty($objMember) || empty($objMember->id)) {
            return redirect('/member/register')
                ->withErrors(['create_member_fail' => 'Lỗi khi thực hiện tạo mới member, vui lòng thử lại'])
                ->withInput();
        }

        // send sms token

        $firstMobile = $objMember->firstMobile;
        $secondMobile = $objMember->secondMobile;
        $smsContent = 'Ma xac nhan dang ky Hoi vien Hoi Huong dan vien Du lich Viet Nam cua ong/ba la: %s. Vui long xac nhan trong vong 24 gio.';

        // Utils::sendSms($firstMobile, sprintf($smsContent, $objMember->phoneToken));
        dispatch((new SendSms($firstMobile, sprintf($smsContent, $objMember->phoneToken)))->delay(Carbon::now()->addSeconds(300)));

        if (!empty($secondMobile)) {
            // Utils::sendSms($secondMobile, sprintf($smsContent, $objMember->phoneToken));
            dispatch((new SendSms($secondMobile, sprintf($smsContent, $objMember->phoneToken)))->delay(Carbon::now()->addSeconds(300)));
        }

        // write id to cookie for back button, allow 10 minute back
        Cookie::queue(self::COOKIE_REGISTER_TOKEN, $objMember->id, 10);

        return redirect('/member/register?step=2&id=' . $objMember->id);
    }

    public function postStep2(Request $request)
    {
        $memberId = $request->input(self::MEMBER_ID);

        /*$objMemberCheck = MemberServiceProvider::getMemberById($request->input('id'));
        if (!empty($objMemberCheck)) {
            abort(404);
        }*/

        $member = MemberServiceProvider::verifyPhoneToken(
            $memberId,
            $request->input('phoneToken')
        );

        if (empty($member)) {
            return redirect('/member/register?step=2&id=' . $memberId)
                ->withErrors([
                    'Mã số xác nhận không đúng',
                ]);
        }

        return redirect('/member/register?step=3&id=' . $memberId);
    }

    public function postStep3(Request $request)
    {
        $memberId = $request->input(self::MEMBER_ID);

        /*$objMemberCheck = MemberServiceProvider::getMemberById($request->input('id'));
        if (!empty($objMemberCheck)) {
            abort(404);
        }*/

        if (empty($memberId)) {
            abort(404);
        }

        $member = MemberServiceProvider::acceptTermsAndPolicies(
            $memberId
        );

        if (empty($member)) {
            return redirect('/member/register?step=4&id=' . $memberId)
                ->withErrors([
                    'Không thể tiếp tục đăng ký',
                ]);
        }

        if (!empty($memberId)) {
            MemberServiceProvider::sendWelcomeMail($memberId);
        }

        Cookie::queue(Cookie::forget(self::COOKIE_REGISTER_TOKEN));

        return view('member.register.thankyou', [
            ['step' => 4]
        ]);
    }

    public function downloadContractFile(Request $request)
    {
        $filePath = storage_path() .'/pdf/'. $request->input('fileName');
        if (file_exists($filePath)) {
            return response()->download($filePath, $request->input('fileNameSaved') . '.pdf', [
                'Content-Length: '. filesize($filePath)
            ]);
        } else {
            return 'File không tồn tại';
        }
    }

    public function downloadZipFolder(Request $request)
    {
        list($zipFileName, $zipPath) = $this->execute(
            $request->input('fileNameSaved'),
            $request->input('fileName')
        );

        if (file_exists($zipPath)) {
            return response()->download($zipPath, $zipFileName, [
                'Content-Length: ' . filesize($zipPath)
            ]);
        }

        return 'File không tồn tại';
    }

    public function regenerateAndDownloadZipFolder(Request $request)
    {
        if (empty($memberId = $request->input('memberId'))) {
            return 'Vui lòng nhập memberId';
        }
        MemberServiceProvider::saveDataMemberIntoPdf($memberId);

        $hashMemberId = md5($memberId);
        $fileName = [
            $hashMemberId . '_detail.pdf',
            $hashMemberId . '.pdf'
        ];
        $fileNameSaved = $request->input('fileCode');

        list($zipFileName, $zipPath) = $this->execute(
            $fileNameSaved,
            $fileName
        );

        if (file_exists($zipPath)) {
            return response()->download($zipPath, $zipFileName, [
                'Content-Length: ' . filesize($zipPath)
            ]);
        }

        return 'Đã có lỗi';
    }

    public function showRejected($id)
    {
        if (empty($id)) {
            abort(404);
        }

        $objMember = MemberServiceProvider::getMemberById($id);

        if (is_null($objMember)) {
            abort(404);
        }

        $objEducation = MemberServiceProvider::getEducationByMemberId($objMember->id);
        $objLanguageSkills = MemberServiceProvider::getLanguageAchievedByMemberId($objMember->id);
        // các bảng này quan hệ với member loại 1 - 1
        $objMajorSkills =  MajorSkill::where('memberId', $objMember->id)->first();
        $objMajor = Major::where('id', $objMajorSkills->majorId)->first();
        $workHistories = MemberServiceProvider::getWorkHistoryByMemberId($objMember->id);
        $objElement = MemberServiceProvider::getElementByMemeber($objMember);
        $paymentInfor = \App\Models\MemberPayments::where('memberId', $objMember->id)->first();

        $otherConstant = $workConstant = $provincial = $chihoi = $clbthuochoi = [];
        $otherTmp = OtherConstant::where('status', 1)->orderBy('otherName', 'asc')->get();
        $workTmp = WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get();
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($otherTmp)){
            foreach($otherTmp as $key => $value){
                $otherConstant[$value->id] = $value->otherName;
            }
        }
        if(!empty($workTmp)){
            foreach($workTmp as $key => $value){
                $workConstant[$value->id] = $value->workName;
            }
        }
        if(!empty($optionTmp)){
            foreach($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if(!empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }
        if(!empty($clbthuochoTmp)){
            foreach($clbthuochoTmp as $key => $value){
                $clbthuochoi[$value->id] = $value->name;
            }
        }

        return view('member.rejected.detail', [
            self::OBJ_MEMBER => $objMember,
            self::TYPE_OF_TRAVEL_GUIDE => empty($objMember->typeOfTravelGuide) ? '' : $objMember->typeOfTravelGuide,
            self::TYPE_OF_PLACE => empty($objMember->typeOfPlace) ? '' : $objMember->typeOfPlace,
            self::BRAND => empty($objEducation[self::BRAND]) ? '' : $objEducation[self::BRAND],
            self::DEGREE => empty($objEducation[self::DEGREE]) ? '' : $objEducation[self::DEGREE],
            self::OBJ_MAJORS => empty($objMajor) ? '' : $objMajor,
            self::OBJ_LANGUAGE => empty($objLanguageSkills[self::OBJ_LANGUAGE]) ? '' : $objLanguageSkills[self::OBJ_LANGUAGE],
            self::OBJ_LANGUAGE_LEVEL => empty($objLanguageSkills[self::OBJ_LANGUAGE_LEVEL]) ? '' : $objLanguageSkills[self::OBJ_LANGUAGE_LEVEL],
            self::WORKHISTORY => empty($workHistories) ? '' : $workHistories,
            self::TYPE_GUIDE_NAME =>empty($objElement[self::TYPE_GUIDE_NAME]) ? '' : $objElement[self::TYPE_GUIDE_NAME],
            self::GROUP_SIZE_NAME =>empty($objElement[self::GROUP_SIZE_NAME]) ? '' : $objElement[self::GROUP_SIZE_NAME],
            self::OBJ_PAYMENT => empty($paymentInfor) ? '' : $paymentInfor,
            self::OBJ_NOTES => '',
            self::PROFILE_IMG => empty($objMember->profileImg) ? '' : $objMember->profileImg,
            'otherConstant' => $otherConstant,
            'workConstant' => $workConstant,
            'provincial' => $provincial,
            'branch_chihoi' => $chihoi,
            'branch_clbthuochoi' => $clbthuochoi,
            'isProfile' => false,
            'languages' => Language::getAllLanguages(),
            'forteTours' => ForteTour::getAllForteTour()
        ]);
    }

    public function showDetails($id)
    {
        if (empty($id)) {
            abort(404);
        }
        $objMember = MemberServiceProvider::getMemberById($id);
        if (empty($objMember)) {
            abort(404);   
        }

        $objEducation = MemberServiceProvider::getEducationByMemberId($objMember->id);
        $objLanguageSkills = MemberServiceProvider::getLanguageAchievedByMemberId($objMember->id);
        // các bảng này quan hệ với member loại 1 - 1
        $objMajorSkills =  MajorSkill::where('memberId', $objMember->id)->first();
        $objMajor = Major::where('id', $objMajorSkills->majorId)->first();
        $workHistories = MemberServiceProvider::getWorkHistoryByMemberId($objMember->id);
        $objElement = MemberServiceProvider::getElementByMemeber($objMember);
        $paymentInfor = \App\Models\MemberPayments::where('memberId', $objMember->id)->first();
        $otherConstant = $workConstant = $provincial = $chihoi = $clbthuochoi = [];
        $otherTmp = OtherConstant::where('status', 1)->orderBy('otherName', 'asc')->get();
        $workTmp = WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get();
        $optionTmp = Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get();
        $chihoiTmp = Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get();
        $clbthuochoTmp = Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get();
        if(!empty($otherTmp)){
            foreach($otherTmp as $key => $value){
                $otherConstant[$value->id] = $value->otherName;
            }
        }
        if(!empty($workTmp)){
            foreach($workTmp as $key => $value){
                $workConstant[$value->id] = $value->workName;
            }
        }
        if(!empty($optionTmp)){
            foreach($optionTmp as $key => $value){
                $provincial[$value->id] = $value->value;
            }
        }
        if(!empty($chihoiTmp)){
            foreach($chihoiTmp as $key => $value){
                $chihoi[$value->id] = $value->name;
            }
        }
        if(!empty($clbthuochoTmp)){
            foreach($clbthuochoTmp as $key => $value){
                $clbthuochoi[$value->id] = $value->name;
            }
        }

        if ($objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER) {
            $ranks = Rank::getAllRanks();
            $memberRank = MemberRank::getRankByMemberId($id);
        }

        return view('member.update.detail', [
            self::OBJ_MEMBER => $objMember,
            self::TYPE_OF_TRAVEL_GUIDE => empty($objMember->typeOfTravelGuide) ? '' : $objMember->typeOfTravelGuide,
            self::TYPE_OF_PLACE => empty($objMember->typeOfPlace) ? '' : $objMember->typeOfPlace,
            self::BRAND => empty($objEducation[self::BRAND]) ? '' : $objEducation[self::BRAND],
            self::DEGREE => empty($objEducation[self::DEGREE]) ? '' : $objEducation[self::DEGREE],
            self::OBJ_MAJORS => empty($objMajor) ? '' : $objMajor,
            self::OBJ_LANGUAGE => empty($objLanguageSkills[self::OBJ_LANGUAGE]) ? '' : $objLanguageSkills[self::OBJ_LANGUAGE],
            self::OBJ_LANGUAGE_LEVEL => empty($objLanguageSkills[self::OBJ_LANGUAGE_LEVEL]) ? '' : $objLanguageSkills[self::OBJ_LANGUAGE_LEVEL],
            self::WORKHISTORY => empty($workHistories) ? '' : $workHistories,
            self::TYPE_GUIDE_NAME =>empty($objElement[self::TYPE_GUIDE_NAME]) ? '' : $objElement[self::TYPE_GUIDE_NAME],
            self::GROUP_SIZE_NAME =>empty($objElement[self::GROUP_SIZE_NAME]) ? '' : $objElement[self::GROUP_SIZE_NAME],
            self::OBJ_PAYMENT => empty($paymentInfor) ? '' : $paymentInfor,
            self::PROFILE_IMG => empty($objMember->profileImg) ? '' : $objMember->profileImg,
            'otherConstant' => $otherConstant,
            'workConstant' => $workConstant,
            'provincial' => $provincial,
            'branch_chihoi' => $chihoi,
            'branch_clbthuochoi' => $clbthuochoi,
            'isProfile' => true,
            'languages' => Language::getAllLanguages(),
            'forteTours' => ForteTour::getAllForteTour(),
            'ranks' => $ranks ?? '',
            'memberRank' => $memberRank ?? '',
            'memberOfficial' => MemberConstants::MEMBER_OFFICIAL_MEMBER,
            'rank_status' => MemberConstants::$rank_status
        ]);
    }

    public function showEdit($id)
    {
        $objMember = Member::where('id', $id)->firstOrFail();
        $objMem = MemberServiceProvider::getMemberForteTourAndGuideLanguageById($id);
        $enable_edit = false;
        if(in_array($objMember->status, [MemberConstants::UPDATE_INFO_1, MemberConstants::UPDATE_INFO_2, MemberConstants::MEMBER_OFFICIAL_MEMBER, MemberConstants::APPROVE_WAITING_UPDATE])){
            $enable_edit = true;
            if($objMember->status == MemberConstants::UPDATE_INFO_2){
                $objMember = MemberServiceProvider::getMemberTmpById($id);
                unset($objMember->id);
                $objMember->id = $objMember->member_id;
                unset($objMember->member_id);
            }
        }

        $objMajors = Major::where('status', 1)->orderBy('majorName', 'asc')->get();
        $position  = count($objMajors) + 29;

//        print_r(json_decode(json_encode($objMember)));
        return view('member.update.edit', [
            self::EDUCATION_BRANCHES => EducationBranch::where('status', 1)->orderBy('branchName', 'asc')->get(),
            self::EDUCATION_DEGREES => EducationDegree::where('status', 1)->orderBy('degree', 'asc')->get(),
            self::MAJORS => $objMajors,
            self::LANGUAGES => Language::where('status', 1)->orderBy('position', 'asc')->get(),
            self::LANGUAGE_LEVELS => LanguageLevel::where('status', 1)->orderBy('levelName', 'asc')->get(),
            self::GROUP_SIZES => GroupSize::where('status', 1)->orderBy('id', 'asc')->get(),
            self::TYPE_GUIDES => TypeGuide::where('status', 1)->orderBy('typeName', 'asc')->get(),
            self::OBJ_MEMBER => empty($objMember) ? null : $objMember,
            self::OBJ_MEM => empty($objMem) ? null : $objMem,
            'enable_edit' => $enable_edit,
            self::POSITION => $position,
            'otherConstant' => OtherConstant::where('status', 1)->orderBy('otherName', 'asc')->get(),
            'workConstant' => WorkConstant::where('status', 1)->orderBy('workName', 'asc')->get(),
            'provincial' => Option::where('status', 1)->where('key', 'provincial')->orderBy('id', 'asc')->get(),
            'branch_chihoi' => Branches::where('option_code', 'BRANCHES01')->where('status', 1)->orderBy('id', 'asc')->get(),
            'branch_clbthuochoi' => Branches::where('option_code', 'BRANCHES03')->where('status', 1)->orderBy('id', 'asc')->get(),
            self::FORTE_TOUR => ForteTour::where('status', 1)->get(),
        ]);
    }

    private function ignoreSomeValidationRules(Request $request, $id)
    {
        // We don't need validate avatar in case of no change
        $fileName = $request->input(self::FILE_NAME);
        $file = $request->file(self::USER_PHOTO, null);
        if (!empty($fileName) && is_null($file)) {
            unset($this->rules[self::USER_PHOTO]);
        }

        $objMember = MemberServiceProvider::getMemberNeedUpdate($id);
        $touristGuideCode = $objMember->touristGuideCode;
        if ($touristGuideCode === $request->input(self::TOURIST_GUIDE_CODE)) {
            unset($this->rules[self::TOURIST_GUIDE_CODE]);
        }
    }

    private function ignoreSomeAttributes(Request $request, &$attributes)
    {
        // We don't need update avatar in case of no change
        $fileName = $request->input(self::FILE_NAME);
        $file = $request->file(self::USER_PHOTO, null);
        if (!empty($fileName) && is_null($file)) {
            unset($attributes[self::AVATAR]);
        }
    }

    public function updateProfile(Request $request, $id)
    {
        // $secureUrl = UrlSigner::sign(url('/member/update/' . $id . '/edit'));
        // $secureQuery = substr($secureUrl, strrpos($secureUrl, "?"));

        $request->merge($this->normalizedTouristGuideCodeFrom($request));
        $request->merge($this->combineDobFrom($request));
        $this->ignoreSomeValidationRules($request, $id);
        $validator = Validator::make($request->all(), $this->rules, $this->messages);
        // $secureUrl = url()->full();
        // $secureQuery = substr($secureUrl, strrpos($secureUrl, "?"));

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $attributes = $this->getMemberAttrsFrom($request);
        $this->ignoreSomeAttributes($request, $attributes);

        $objMember = null;
        try {
            $objMember = MemberServiceProvider::getMemberById($id);
            if ($objMember->status == MemberConstants::UPDATE_INFO_1 || $objMember->status == MemberConstants::APPROVE_WAITING_UPDATE) {
                $objMember = MemberServiceProvider::updateMemberWith($attributes, $id);
            } else if ($objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER || $objMember->status == MemberConstants::UPDATE_INFO_2) {
                $objMember = MemberServiceProvider::updateMemberTmpWith($attributes, $id);
            }

        } catch(\Exception $e) {
            return redirect()->back()
                // ->withErrors(['create_member_fail' => $e->getMessage()]) // just for debug purpose
                ->withErrors(['create_member_fail' => 'Lỗi khi thực hiện cập nhật member, vui lòng thử lại'])
                ->withInput();
        }
        if (empty($objMember) || empty($objMember->id)) {
            return redirect()->back()
                ->withErrors(['create_member_fail' => 'Lỗi khi thực hiện cập nhật member, vui lòng thử lại'])
                ->withInput();
        }

        $detailUrl = url('/member/update/' . $id . '/detail');
        $secureUrl = UrlSigner::sign($detailUrl);
        $secureQuery = substr($secureUrl, strrpos($secureUrl, "?"));
        return redirect($detailUrl . $secureQuery)->with(self::SUCCESSES, 'Cập nhật hồ sơ thành công');;
    }

    public function updateProfileAd(Request $request, $id)
    {
        // $secureUrl = UrlSigner::sign(url('/member/update/' . $id . '/edit'));
        // $secureQuery = substr($secureUrl, strrpos($secureUrl, "?"));

        $request->merge($this->normalizedTouristGuideCodeFrom($request));
        $request->merge($this->combineDobFrom($request));
        
        // $this->ignoreSomeValidationRules($request, $id);
        // $validator = Validator::make($request->all(), $this->rules, $this->messages);
        // // $secureUrl = url()->full();
        // // $secureQuery = substr($secureUrl, strrpos($secureUrl, "?"));

        // if ($validator->fails()) {
        //     return redirect()->back()
        //         ->withErrors($validator)
        //         ->withInput();
        // }

        $attributes = $this->getMemberAttrsFrom($request);
        $this->ignoreSomeAttributes($request, $attributes);

        $objMember = null;
        try {
            // $objMember = MemberServiceProvider::getMemberById($id);
            $objMember = MemberServiceProvider::updateMemberWithoutStatus($attributes, $id);
            MemberServiceProvider::saveDataMemberIntoPdf($id);
            // $objMember->fullName = $request->input('fullName');
            // $objMember->gender = $request->input('gender');
            // $objMember->birthday = $request->input(self::DOB);
            // $objMember->profileImg = $request->input('avatar');
            // $objMember->update();
        } catch(\Exception $e) {
            return redirect()->route('admin_member_list_view')
                // ->withErrors(['create_member_fail' => $e->getMessage()]) // just for debug purpose
                ->withErrors(['error_message' => 'Lỗi khi thực hiện cập nhật member, vui lòng thử lại'])
                ->withInput();
        }
        if (empty($objMember) || empty($objMember->id)) {
            return redirect()->route('admin_member_list_view')->withErrors(['error_message' => 'Lỗi khi thực hiện cập nhật member, vui lòng thử lại'])
                ->withInput();
        }

        // $detailUrl = url('/member/update/' . $id . '/detail');
        // $secureUrl = UrlSigner::sign($detailUrl);
        // $secureQuery = substr($secureUrl, strrpos($secureUrl, "?"));
        return redirect()->route('admin_member_list_view')->with('success_message', 'Cập nhật hồ sơ thành công');
    }

    /*public function reSendRegisterMailToken ($id)
    {
        if (empty($objMember) || empty($objMember->id)) {
            return false;
        }

        MemberServiceProvider::sendMailToken($objMember->id);

        return redirect()->route('member_register_view', ['step' => 2, 'id' => $objMember->id]);
    }*/

    public function reSendSmsPhoneToken (Request $request)
    {
        $data = [
            'status' => 1
        ];
        $id = $request->input('memberId');
        $objMember = MemberServiceProvider::getMemberById($id);
     
        if (empty($objMember) || empty($objMember->id)) {
            $data['status'] = 0;
        }

        // send sms token
        $firstMobile = $objMember->firstMobile;
        $secondMobile = $objMember->secondMobile;
        $smsContent = 'Ma xac nhan dang ky Hoi vien Hoi Huong dan vien Du lich Viet Nam cua ong/ba la: %s. Vui long xac nhan trong vong 24 gio.';

        dispatch((new SendSms($firstMobile, sprintf($smsContent, $objMember->phoneToken)))->delay(Carbon::now()->addSeconds(2)));

        if (!empty($secondMobile)) {
            dispatch((new SendSms($secondMobile, sprintf($smsContent, $objMember->phoneToken)))->delay(Carbon::now()->addSeconds(2)));
        }
        $data['value'] = $objMember;
        return \Response::json($data);
    }
}
