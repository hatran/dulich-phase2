<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;
use App\Constants\MemberConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
use App\Providers\BannerServiceProvider;
use App\Models\Offices;
use App\Models\EmployeesPosition;
class FrontEndBaseController extends Controller
{
    public function __construct ()
    {

        $routerName = Route::currentRouteName();
        $page_code = $bannerName = '';
        $arrBanner = ['front_end_index', 'front_vechungtoi', 'front_tamnhinvasumenh', 'front_quychehoatdong', 'front_quydinh', 'front_huongdandangky', 'front_end_hoivien', 'front_end_chihoi', 'front_end_clb', 'front_end_executive_committee', 'front_end_standing_committee', 'front_end_special_board', 'front_end_standing_advisory_board', 'front_end_vanphongdaidien', 'front_end_news_1', 'THONGTIN', 'DAOTAO', 'SANGIAODICH', 'XEPHANGHDV'];
        //if(in_array($routerName, $arrBanner)){
            //trang chu
            if($routerName == 'front_end_index'){
                $page_code = 'HOME';
            }
            //ve chung toi
            if($routerName == 'front_vechungtoi'){
                $page_code = 'INTRODUCTION';
            }
            //tam nhin va su menh
            if($routerName == 'front_tamnhinvasumenh'){
                $page_code = 'TAMNHINVASUMENH';
            }
            //quy che hoat dong
            if($routerName == 'front_quychehoatdong'){
                $page_code = 'QUYCHEHOATDONG';
            }
            //quy dinh
            if($routerName == 'front_quydinh'){
                $page_code = 'QUYDINH';
            }
            //huong dan dang ky
            if($routerName == 'front_huongdandangky'){
                $page_code = 'HUONGDANDANGKY';
            }
            //hoi vien
            if($routerName == 'front_end_hoivien'){
                $page_code = 'HOIVIEN';
            }
            if($routerName == 'front_end_chihoi'){
                $page_code = 'CHIHOI';
            }
            if($routerName == 'front_end_clb'){
                $page_code = 'CLBTHUOCHOI';
            }
            //ban chap hanh
            if($routerName == 'front_end_executive_committee'){
                $page_code = 'BANCHAPHANH';
            }
            //ban thuong truc
            if($routerName == 'front_end_standing_committee'){
                $page_code = 'BANTHUONGTRUC';
            }
            //ban chuyen mon
            if($routerName == 'front_end_special_board'){
                $page_code = 'BANCHUYENMON';
            }
            //ban co van
            if($routerName == 'front_end_standing_advisory_board'){
                $page_code = 'BANCOVAN';
            }
            if($routerName == 'front_end_vanphongdaidien'){
                $page_code = 'VPDD';
            }
            if($routerName == 'front_end_news_1' || $routerName == 'front_end_news' || $routerName == 'front_end_news_detail'){
                $page_code = 'TINTUCSUKIEN';
            }
            // 4 cai dươi chưa ai làm nên chưa co routername
            if($routerName == 'front_info_traffic' || $routerName == 'front_info_traffic_detail'
                || $routerName == 'front_info_lehoi' || $routerName == 'front_info_lehoi_detail'
                || $routerName == 'front_info_diemden' || $routerName == 'front_info_diemden_detail'
                || $routerName == 'front_info_trangphuc' || $routerName == 'front_info_trangphuc_detail'
                || $routerName == 'front_info_customandpractices' || $routerName == 'front_info_customandpractices_detail'){
                $page_code = 'THONGTIN';
            }
            if($routerName == 'front_info_list'){
                $page_code = 'HOME';
            }
            if($routerName == 'front_end_training'){
                $page_code = 'DAOTAO';
            }
            if($routerName == 'front_end_work_trading'){
                $page_code = 'SANGIAODICH';
            }
            if($routerName == 'front_end_survey'){
                $page_code = 'XEPHANGHDV';
            }
            $arrBaOfPage = BannerServiceProvider::getBannerByPage($page_code);
            if(!empty($arrBaOfPage)) $bannerName = $arrBaOfPage ;
            /*if($page_code == 'HOME'){
                $bannerName = [];
                if(!empty($arrBaOfPage)) $bannerName = $arrBaOfPage ;
            }else{
                if(isset($arrBaOfPage['profile_image']) && $arrBaOfPage['profile_image'] != '') $bannerName = $arrBaOfPage['profile_image'] ;
            }*/
        $arrVpdd = [];
        if($page_code == 'HOME'){
            $arrVpdd = $this->getOfficesArray();
        }
        //}
        
        $countVPDD = count($arrVpdd);
        $arrRan = [];
        // if($countVPDD > 1){
        //     if($countVPDD < 5){
        //         $tmp = 5 - $countVPDD  ;
        //         $random_keys=array_rand($arrVpdd,$tmp);
        //         $arrMer = [];
        //         if(!isset($random_keys[0])){
        //             $arrRan[0] = $random_keys;
        //         }else{
        //             $arrRan = $random_keys;
        //         }
        //         if(!empty($arrRan)) {
        //             foreach ($arrRan as $value) {
        //                 $arrMer[] = $arrVpdd[$value];
        //             }
        //         }
        //         $arrVpdd = array_merge($arrVpdd,$arrMer);
        //     }

        //     if($countVPDD > 5){
        //         $tmpC = $countVPDD % 5;
        //         if($tmpC > 0){
        //             $tmp = 5 - $tmpC;
        //             $random_keys=array_rand($arrVpdd,$tmp);
        //             if(!isset($random_keys[0])){
        //                 $arrRan[0] = $random_keys;
        //             }else{
        //                 $arrRan = $random_keys;
        //             }
        //             $arrMer = [];
        //             if(!empty($arrRan)){
        //                 foreach($arrRan as $value){
        //                     $arrMer[] = $arrVpdd[$value];
        //                 }
        //             }
        //             $arrVpdd = array_merge($arrVpdd,$arrMer);
        //         }
        //     }

        // }
        $bannerName['vpdd'] = $arrVpdd;
        view()->share('bannerName', $bannerName);
        view()->share('headOffice', $this->getOfficesHead());
    }
    public function getLanhDao($id)
    {
        $listManager = $this->getEmployeesPositionByBranchId($id);
        return $listManager;
    }

    public function getEmployeesPositionByBranchId($id) {
        return EmployeesPosition::where('branch_id', $id)->with(['employees'])->orderBy('created_at', 'asc')->get();
    }

    public function getOfficesHead()
    {
        $headOffice = Offices::where('status',1)
            ->where('option_code', '=' ,'HEAD')
            ->whereNull('deleted_at')
            ->first();

        return $headOffice->toArray();
    }

    public function getOfficesArray()
    {
        $objOffices = Offices::where('status',1)
            ->whereNull('parent_id')
            ->whereIn('option_code',['HEAD','OFFICES'])
            ->whereNull('deleted_at')
            ->orderByRaw("FIELD(option_code , 'HEAD', 'OFFICES') ASC")
            ->get();
        $arrayOffices = $objOffices->toArray();

        // html entity
        $resultArray = array();
        foreach ($arrayOffices as $objOffices) {
            $resultItem = array();
            foreach ($objOffices as $key => $item) {
                $resultItem[$key] = e($item);
            }
            //$resultItem['manage'] = $this->getOnlyGiamDoc($this->getLanhDao($objOffices['id']));
            $resultArray[] = $resultItem;
        }
        $lastResult = array();
        foreach ($resultArray as $key => $office) {
            if (!empty($office['parent_id'])) continue;
            $lastResult[] = $office;
        }
        return $lastResult;
    }

    public function getOnlyGiamDoc($managers){
        $result = null;
        foreach($managers as $manager){
            if($manager['option_code'] == 'cdddd'){
                return $manager;
            }
        }
        return null;
    }


}
