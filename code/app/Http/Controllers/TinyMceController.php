<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

/**
 * Description of TinyMceController
 *
 * @author DuyDuc
 */

class TinyMceController {
    public function tinyMceUpload(Request $request) {
        reset($_FILES);
        $file = $request->file;
        $fileArray = array('image' => $file);
        $rules = [
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ];
        $message = [
            'mimes' => ':attribute phải có định dạng: :values.',
        ];
        $niceAttr = ['image' => 'Tệp tin'];
        $validator = Validator::make($fileArray, $rules, $message, $niceAttr);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()]);
        } else {
            $filePath = public_path('/images/news');
            $fileUrl = asset('images/news');
            $fileOriginalName = $file->getClientOriginalName();
            $fileName = str_slug(explode('.', $fileOriginalName)[0]) .'.'. $file->getClientOriginalExtension();
            $file->move($filePath, $fileName);
            $fileName = $fileUrl. '/' . $fileName;
            echo json_encode(array('location' => $fileName));
        }
    }
}
