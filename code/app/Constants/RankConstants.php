<?php

namespace App\Constants;

class RankConstants
{
    const RANK_3_STAR = 1;
    const RANK_4_STAR = 2;  
    const RANK_5_STAR = 3;  

    public static $rank = [
    	1 => 'Hạng 3 sao',
    	2 => 'Hạng 4 sao',
    	3 => 'Hạng 5 sao' 
    ];

    public static $rank_name_id = [
        'Hạng 3 sao' => 1,
        'Hạng 4 sao' => 2,
        'Hạng 5 sao' => 3
    ];

    public static $question_rank = ['Hạng 3 sao', 'Hạng 4 sao', 'Hạng 5 sao'];

    public static $answer = [1, 2, 3, 4];

    const HDV_QT_5_STAR = 1;
    const HDV_QT_4_STAR = 2;
    const HDV_QT_3_STAR = 3;
    const HDV_ND_5_STAR = 4;
    const HDV_ND_4_STAR = 5;
    const HDV_ND_3_STAR = 6;
    const HDV_TD_5_STAR = 7;
    const HDV_TD_4_STAR = 8;
    const HDV_TD_3_STAR = 9;

    public static $position = [
        'Giám đốc' => 'Giám đốc',
        'Phó giám đốc' => 'Phó giám đốc',
        'Trưởng phòng điều hành' => 'Trưởng phòng điều hành' 
    ];
}
