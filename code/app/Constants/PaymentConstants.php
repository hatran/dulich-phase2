<?php
/**
 * Created by PhpStorm.
 * User: daiduong47
 * Date: 11/02/2018
 * Time: 3:04 AM
 */

namespace App\Constants;


class PaymentConstants
{
    const METHOD_VIETCOMBANK = 1;

    public static $arrPaymentMethodName = [
        self::METHOD_VIETCOMBANK => 'VietComBank'
    ];
}
