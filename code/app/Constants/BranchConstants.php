<?php

namespace App\Constants;


class BranchConstants
{
    public static $name = [
        1  => 'Hà Nội',
        2  => 'TP. Hồ Chí Minh',
        3  => 'Hải Phòng',
        4  => 'Miền Trung',
        5  => 'Cần Thơ',
        6  => 'An Giang',
        7  => 'Bà Rịa – Vũng Tàu',
        8  => 'Bắc Giang',
        9  => 'Bắc Kạn',
        10 => 'Bạc Liêu',
        11 => 'Bắc Ninh',
        12 => 'Bến Tre',
        13 => 'Bình Định',
        14 => 'Bình Dương',
        15 => 'Bình Phước',
        16 => 'Bình Thuận',
        17 => 'Cà Mau',
        18 => 'Cao Bằng',
        19 => 'Đắk Lắk',
        20 => 'Đắk Nông',
        21 => 'Điện Biên',
        22 => 'Đồng Nai',
        23 => 'Đồng Tháp',
        24 => 'Gia Lai',
        25 => 'Hà Giang',
        26 => 'Hà Nam',
        27 => 'Hà Tĩnh',
        28 => 'Hải Dương',
        29 => 'Hậu Giang',
        30 => 'Hòa Bình',
        31 => 'Hưng Yên',
        32 => 'Khánh Hòa',
        33 => 'Kiên Giang',
        34 => 'Kon Tum',
        35 => 'Lai Châu',
        36 => 'Lạng Sơn',
        37 => 'Lào Cai',
        38 => 'Lâm Đồng',
        39 => 'Long An',
        41 => 'Nam Định',
        42 => 'Nghệ An',
        43 => 'Ninh Bình',
        44 => 'Ninh Thuận',
        45 => 'Phú Thọ',
        46 => 'Phú Yên',
        47 => 'Quảng Bình',
        48 => 'Quảng Nam',
        49 => 'Quảng Ngãi',
        50 => 'Quảng Ninh',
        51 => 'Quảng Trị',
        52 => 'Sóc Trăng',
        53 => 'Sơn La',
        54 => 'Tây Ninh',
        55 => 'Thái Bình',
        56 => 'Thái Nguyên',
        57 => 'Thanh Hóa',
        58 => 'Thừa Thiên Huế',
        59 => 'Tiền Giang',
        60 => 'Trà Vinh',
        61 => 'Tuyên Quang',
        62 => 'Vĩnh Long',
        63 => 'Vĩnh Phúc',
        64 => 'Yên Bái',
    ];
    //city
    public static $city = [
        1  => 'Hà Nội',
        2  => 'TP. Hồ Chí Minh',
        3  => 'Hải Phòng',
        4  => 'Đà Nẵng',
        5  => 'Cần Thơ',
        6  => 'An Giang',
        7  => 'Bà Rịa – Vũng Tàu',
        8  => 'Bắc Giang',
        9  => 'Bắc Kạn',
        10 => 'Bạc Liêu',
        11 => 'Bắc Ninh',
        12 => 'Bến Tre',
        13 => 'Bình Định',
        14 => 'Bình Dương',
        15 => 'Bình Phước',
        16 => 'Bình Thuận',
        17 => 'Cà Mau',
        18 => 'Cao Bằng',
        19 => 'Đắk Lắk',
        20 => 'Đắk Nông',
        21 => 'Điện Biên',
        22 => 'Đồng Nai',
        23 => 'Đồng Tháp',
        24 => 'Gia Lai',
        25 => 'Hà Giang',
        26 => 'Hà Nam',
        27 => 'Hà Tĩnh',
        28 => 'Hải Dương',
        29 => 'Hậu Giang',
        30 => 'Hòa Bình',
        31 => 'Hưng Yên',
        32 => 'Khánh Hòa',
        33 => 'Kiên Giang',
        34 => 'Kon Tum',
        35 => 'Lai Châu',
        36 => 'Lạng Sơn',
        37 => 'Lào Cai',
        38 => 'Lâm Đồng',
        39 => 'Long An',
        41 => 'Nam Định',
        42 => 'Nghệ An',
        43 => 'Ninh Bình',
        44 => 'Ninh Thuận',
        45 => 'Phú Thọ',
        46 => 'Phú Yên',
        47 => 'Quảng Bình',
        48 => 'Quảng Nam',
        49 => 'Quảng Ngãi',
        50 => 'Quảng Ninh',
        51 => 'Quảng Trị',
        52 => 'Sóc Trăng',
        53 => 'Sơn La',
        54 => 'Tây Ninh',
        55 => 'Thái Bình',
        56 => 'Thái Nguyên',
        57 => 'Thanh Hóa',
        58 => 'Thừa Thiên Huế',
        59 => 'Tiền Giang',
        60 => 'Trà Vinh',
        61 => 'Tuyên Quang',
        62 => 'Vĩnh Long',
        63 => 'Vĩnh Phúc',
        64 => 'Yên Bái',
    ];
    public static $club = [
        65 => 'CLB HDV tiếng Nhật Hà Nội',
    ];

    // update 20180122, thay đổi danh sách tỉnh thành mới, loại bỏ Hậu Giang, code 29
    public static $hn_typeOfPlace  = [35, 53, 21, 30, 57, 37, 64, 61, 56, 9, 18, 25, 25, 45, 8, 36, 11, 50, 1, 3, 28, 43, 31, 55, 41, 26, 63, 65];
    public static $dn_typeOfPlace  = [42, 27, 47, 51, 58, 4, 48, 49, 13, 46, 32, 44, 16, 24, 34, 38, 19, 20];
    public static $hcm_typeOfPlace = [15, 22, 14, 54, 7, 2, 59, 12, 5, 6, 10, 17, 33, 23, 60, 52, 39, 62, 29];

    const TYPE_OF_PLACE_HN  = 1;
    const TYPE_OF_PLACE_DN  = 2;
    const TYPE_OF_PLACE_HCM = 3;

    public static $arrProvinceType = [
        self::TYPE_OF_PLACE_HN  => 'Hội Hà Nội',
        self::TYPE_OF_PLACE_DN  => 'VPĐD Đà Nẵng',
        self::TYPE_OF_PLACE_HCM => 'VPĐD TP Hồ Chí Minh',
    ];

    const TYPE_OF_PLACE_CHI_HOI = 1;
    const TYPE_OF_PLACE_CLB_THUOC_HOI = 2;

    public static $typeOfPlace = [
        self::TYPE_OF_PLACE_CHI_HOI       => 'Chi hội',
        self::TYPE_OF_PLACE_CLB_THUOC_HOI => 'CLB thuộc hội',
    ];
}
