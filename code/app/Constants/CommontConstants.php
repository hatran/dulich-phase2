<?php

namespace App\Constants;


class CommontConstants
{
    const ENABLE  = 1;
    const DISABLE = 0;
    const INTRODUCTION = 'introduction';
    const CATEGORY = 'categories';
    const DEFAULT_LIMIT_MEMBER_PER_PAGE = 10;
    const DEFAULT_LIMIT_MEMBER_PER_PAGE_PRINT = 100;
    const RESPONSE_VIEW = 'response_view';
    const COUNT_MEMBER = 'count_members';
    const PAGINATOR = 'paginator';
    const CURRENT_PAGINATOR = 'current_paginator';

    const OBJ_MEMBERS = 'objMembers';
    const OBJ_MEMBER = 'objMember';
    const TYPE_OF_TRAVEL_GUIDE = 'typeOfTravelGuide';
    const TYPE_OF_PLACE = 'typeOfPlace';
    const BRAND = 'objBrand';
    const DEGREE = 'objDegree';
    const MAJORS_SKILLS = 'objMajorSkills';
    const MAJORS = 'objMajor';
    const LANGUAGE = 'objLanguage';
    const LANGUAGE_LEVEL = 'objLanguageLevel';
    const WORKHISTORY = 'workHistories';
    const TYPE_GUIDE_NAME = 'typeGuideName';
    const GROUP_SIZE_NAME = 'groupSizeName';
    const OBJ_PAYMENT = 'paymentInfor';
    const OBJ_NOTES = 'notes';
    const STEP = 'step';
    const STATUS_ARRAY = 'arrFile';
    const AREA_OR_ROLE = 'arrCities';
    const HN_SEARCH = 'hnArea';
    const DN_SEARCH = 'dnArea';
    const HCM_SEARCH = 'hcmArea';
    const ALL_SEARCH = 'allArea';

    const FROM_DATE = 'from_date';
    const TO_DATE = 'to_date';
    const TOURIST_GUIDE_CODE = 'touristGuideCode';
    const STATUS = 'status';
    const FULL_NAME = 'fullName';
    const D_M_Y_FORMAT = 'd/m/Y';
    const PROVINCE_TYPE = 'province_type';
    const PROVINCE_CODE = 'province_code';
    const FILE_CODE = 'file_code';
    const MEMBER_TYPE = 'member_type';
    const IS_PRINTING = 'is_printed_card';
    const FULL_NAME_USER = 'fullname';
    const USERNAME = 'username';
    const EMAIL = 'email';
    const PWD_INPUT = 'password';
    const MEMBER_ID = 'memberId';
    const MEMBER_CODE = 'member_code';
    const SUCCESSES = 'successes';
    const SUCCESSES_MSG = 'success_message';

    const OBJAPPROVEDBY = 'objApprovedBy';
    const APPROVEDBYNAME = 'approvedByName';
    const OBJVERIFIEDBY = 'objVerifiedBy';
    const VERIFIEDBYNAME = 'verifiedByName';
    const STATE = 'state';

    const GUIDE_LANGUAGE = 'guideLanguage';
    const PROFILE_IMG = 'profileImg';
    const IS_FEE = 'is_fee';

    const PLACEHOLDER_TEN = '$PLACEHOLDER_TEN$';
    const PLACEHOLDER_MAHOSO = '$PLACEHOLDER_MAHOSO$';
    const PLACEHOLDER_MAHOIVIEN = '$PLACEHOLDER_MAHOIVIEN$';
    const PLACEHOLDER_URL_PHIEUTHONGTIN = '$PLACEHOLDER_URL_PHIEUTHONGTIN$';
    const PLACEHOLDER_URL_DONDANGKY = '$PLACEHOLDER_URL_DONDANGKY$';
    const PLACEHOLDER_USERNAME = '$PLACEHOLDER_USERNAME$';
    const PLACEHOLDER_PASSWORD = '$PLACEHOLDER_PASSWORD$';
    const PLACEHOLDER_URL_TRANGCHU = '$PLACEHOLDER_URL_TRANGCHU$';
	const PLACEHOLDER_TAI_DK = '$PLACEHOLDER_TAI_DK$';
    const PLACEHOLDER_TAI_PHIEU = '$PLACEHOLDER_TAI_PHIEU$';
    const PLACEHOLDER_HOIPHI = '$PLACEHOLDER_HOIPHI$';
    const PLACEHOLDER_HOIPHI_DANOP = '$PLACEHOLDER_HOIPHI_DANOP$';

    const MEMBER_FROM = 'member_from';
	
    public static $year = [
        2018 =>  '2018',
        2019 =>  '2019',
        2020 =>  '2020',
        2021 =>  '2021',
        2022 =>  '2022',
        2023 =>  '2023'
    ];
}
