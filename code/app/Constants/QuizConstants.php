<?php

namespace App\Constants;

class QuizConstants
{
    const CLOSED_QUIZ = 0;
    const OPENED_QUIZ = 1;    

    public static $time = [
    	'3_STAR_LOCAL' => 33,
    	'4_STAR_LOCAL' => 45,
    	'5_STAR_LOCAL' => 57,
    	'3_STAR_INTERNATIONAL' => 40,
    	'4_STAR_INTERNATIONAL' => 55,
    	'5_STAR_INTERNATIONAL' => 70
    ];

    public static $total_base_knowledge = [
    	'3_STAR_LOCAL' => 20,
    	'4_STAR_LOCAL' => 28,
    	'5_STAR_LOCAL' => 36,
    	'3_STAR_INTERNATIONAL' => 20,
    	'4_STAR_INTERNATIONAL' => 28,
    	'5_STAR_INTERNATIONAL' => 36
    ];

    public static $total_subject_knowledge = [
    	'3_STAR_LOCAL' => 45,
    	'4_STAR_LOCAL' => 61,
    	'5_STAR_LOCAL' => 77,
    	'3_STAR_INTERNATIONAL' => 60,
    	'4_STAR_INTERNATIONAL' => 82,
    	'5_STAR_INTERNATIONAL' => 104
    ];

    public static $total = [
    	'3_STAR_LOCAL' => 65,
    	'4_STAR_LOCAL' => 89,
    	'5_STAR_LOCAL' => 113,
    	'3_STAR_INTERNATIONAL' => 80,
    	'4_STAR_INTERNATIONAL' => 110,
    	'5_STAR_INTERNATIONAL' => 140
    ];

    const QUESTION_3_STAR_LOCAL = [1 => 5, 2 => 5, 3 => 5, 6 => 5, 7 => 5, 8 => 10, 9 => 5, 10 => 5, 11 => 15, 12 => 5];
    const QUESTION_4_STAR_LOCAL = [1 => 2, 2 => 2, 3 => 2, 6 => 2, 7 => 2, 8 => 3, 9 => 2, 10 => 2, 11 => 5, 12 => 2];
    const QUESTION_5_STAR_LOCAL = [1 => 2, 2 => 2, 3 => 2, 6 => 2, 7 => 2, 8 => 3, 9 => 2, 10 => 2, 11 => 5, 12 => 2];

    const QUESTION_3_STAR_INTERNATIONAL = [1 => 5, 2 => 5, 3 => 5, 6 => 5, 7 => 5, 8 => 10, 9 => 5, 10 => 5, 11 => 15, 12 => 5, 13 => 5, 14 => 5, 15 => 5];
    const QUESTION_4_STAR_INTERNATIONAL = [1 => 2, 2 => 2, 3 => 2, 6 => 2, 7 => 2, 8 => 3, 9 => 2, 10 => 2, 11 => 5, 12 => 2, 13 => 2, 14 => 2, 15 => 2];
    const QUESTION_5_STAR_INTERNATIONAL = [1 => 2, 2 => 2, 3 => 2, 6 => 2, 7 => 2, 8 => 3, 9 => 2, 10 => 2, 11 => 5, 12 => 2, 13 => 2, 14 => 2, 15 => 2];

    const BASE_QUESTION = 1;
    const SUBJECT_QUESTION = 2;

    /**
     * key :
     *  1 => base_question
     *  2 => subject_question
     */
    const iniAnsweredQuestion = [
        self::BASE_QUESTION => 0,
        self::SUBJECT_QUESTION => 0,
    ];

    const OPEN_QUIZ_STATUS = [
        1 => 'Đang mở thi xếp hạng',
        2 => 'Đang đóng thi xếp hạng'
    ];
}
