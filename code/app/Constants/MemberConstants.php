<?php

namespace App\Constants;

class MemberConstants
{
    // Trạng thái hồ sơ hội viên mới
    const VERIFICATION_WAITING              = 1;
    const VERIFICATION_WAITING_VAL          = 'Chờ thẩm định';

    const VERIFICATION_REJECTED              = 7;
    const VERIFICATION_REJECTED_VAL          = 'Từ chối thẩm định';

    // yêu cầu bổ sung thẩm định
    const UPDATE_INFO_1                      = 2;
    const UPDATE_INFO_1_VAL                  = 'Bổ sung thông tin';

    const APPROVAL_WAITING                   = 3;
    const APPROVAL_WAITING_VAL               = 'Chờ phê duyệt'; // thẩm định xong --------------------

    // Yêu cầu bổ sung phê duyệt
    const APPROVE_WAITING_UPDATE             = 22;
    const APPROVE_WAITING_UPDATE_VAL         = 'Bổ Sung thông tin';

    const APPROVAL_REJECTED                  = 8;
    const APPROVAL_REJECTED_VAL              = 'Từ chối phê duyệt'; // --------------------

    const FEE_WAITING                        = 4;
    const FEE_WAITING_VAL                    = 'Chờ đóng hội phí lần đầu'; // đã phê duyệt

    const SIGNING_WAITING                    = 5;
    const SIGNING_WAITING_VAL                = 'Chờ ký quyết định hội viên'; // đã đóng lệ phí
    const SIGNING_WAITING_VAL_DISPLAY        = 'Đã đóng lệ phí + hội phí'; // đã đóng lệ phí

    const CODE_PROVIDING_WAITING             = 6;
    const CODE_PROVIDING_WAITING_VAL         = 'Chờ cấp mã hội viên'; // đã ký xong

    const CARD_PRINTING_WAITING              = 9;
    const CARD_PRINTING_WAITING_VAL          = 'Chờ in thẻ lần đầu'; // đã được cấp mã hội viên

    // Bổ sung thông tin trong trường hợp hồ sơ mới (đã được cấp mã thẻ hội viên)
    const UPDATE_INFO_2                      = 10;
    const UPDATE_INFO_2_VAL                  = 'Yêu cầu Cập nhật thông tin'; // --------------------

    const MEMBER_OFFICIAL_MEMBER             = 13;
    const MEMBER_OFFICIAL_MEMBER_VAL         = 'Hội viên chính thức'; // khi sang status này bắt buộc

    const MEMBER_STORED                     = 14;
    const MEMBER_STORED_VAL                 = 'Lưu kho'; // luu kho khi hoi vien chua dong hoi phi

    const MEMBER_DISTROY                     = 15;
    const MEMBER_DISTROY_VAL                 = 'Chấm dứt tư cách hội viên'; // Khi đã là hội viên chính thức

    const MEMBER_PENDDING                    = 16;
    const MEMBER_PENDDING_VAL                = 'Tạm dừng tư cách hội viên'; // Khi đã là hội viên chính thức

    const CODE_PROVIDING_REWAITING             = 61;
    const CODE_PROVIDING_REWAITING_VAL         = 'Chờ cấp lại thẻ hội viên'; // đã ký xong

    const IS_VERIFIED          = 1;
    const IS_VERIFIED_VAL      = 'Đã xác minh hồ sơ'; // cầm hồ sơ đến xác minh

    const IS_FEE               = 1;
    const IS_FEE_VAL           = 'Chờ ký quyết định hội viên'; // đã nộp lệ phí

    // kiem tra in tờ trình
    const IS_PRINT           = 1;
    const IS_PRINT_VAL        = 'Đã In';

    // IS_SIGNED == 2 'Chờ quyết định' //
    const IS_SIGNED            = 1;
    const IS_SIGNED_VAL        = 'Đã ký quyết định';

    // 'chờ cấp mã hội' viên sau khi đã ký quyết định

    const HONORARY_MEMBERS     = 1;
    const HONORARY_MEMBERS_VAL = 'Hội viên danh dự';

    const ASSOCIATE_MEMBER     = 2;
    const ASSOCIATE_MEMBER_VAL = 'Hội viên liên kết';

    const OFFICIAL_MEMBER      = 3;
    const OFFICIAL_MEMBER_VAL  = 'Hội viên chính thức';


    const IS_VERIFIED_ACTION = 1; // THẨM ĐỊNH
    const IS_APPROVED_ACTION = 2; // PHÊ DUYỆT
    const IS_PAID_ACTION = 3; // Đóng hội phí , lệ phí
    const IS_PROVIDING_WAITTING_VAL_ACTION = 4; // Cấp lại thẻ hội viên
    const IS_CARD_RENEWED_WAITTING_ACTION = 5; // Gia hạn thẻ
	const IS_NUMBER_TOUR_GUIDE_CHANGE = 4; // đổi số thẻ hướng dẫn viên

    public static $member_type = [
        self::HONORARY_MEMBERS => self::HONORARY_MEMBERS_VAL,
        self::ASSOCIATE_MEMBER => self::ASSOCIATE_MEMBER_VAL,
        self::OFFICIAL_MEMBER  => self::OFFICIAL_MEMBER_VAL
    ];

    // cờ css tương ứng từng trạng thái
    public static $file_const_flag_css = [
        // self::WAITING          => 'pending',
        self::VERIFICATION_WAITING   => 'pending',
        // self::REQUESTING       => 'requested',
        self::UPDATE_INFO_1          => 'requested',
        // self::WAITING_APPROVED => '',
        self::APPROVAL_WAITING => '',
        // self::APPROVED         => 'approved', // Đã bổ sung
        self::FEE_WAITING         => 'approved', // Đã bổ sung
        self::IS_SIGNED        => 'done', // Đã ký quyết định
        self::IS_VERIFIED      => 'verified', // Đã xác minh hồ sơ
    ];

    // Phạm vi phê duyệt chung hiển thị status
    public static $file_const_display_status_search = [
        self::VERIFICATION_WAITING   => self::VERIFICATION_WAITING_VAL,
        self::VERIFICATION_REJECTED  => self::VERIFICATION_REJECTED_VAL,
        self::UPDATE_INFO_1          => self::UPDATE_INFO_1_VAL,
        self::APPROVAL_REJECTED      => self::APPROVAL_REJECTED_VAL,
        self::APPROVAL_WAITING       => self::APPROVAL_WAITING_VAL,
        self::FEE_WAITING            => self::FEE_WAITING_VAL,
        self::SIGNING_WAITING        => self::SIGNING_WAITING_VAL,
        self::CODE_PROVIDING_WAITING => self::CODE_PROVIDING_WAITING_VAL,
        self::CARD_PRINTING_WAITING  => self::CARD_PRINTING_WAITING_VAL,
        self::UPDATE_INFO_2          => self::UPDATE_INFO_2_VAL,
        self::MEMBER_OFFICIAL_MEMBER => self::MEMBER_OFFICIAL_MEMBER_VAL,
        self::MEMBER_STORED          => self::MEMBER_STORED_VAL,
        self::MEMBER_PENDDING        => self::MEMBER_PENDDING_VAL,
        self::MEMBER_DISTROY         => self::MEMBER_DISTROY_VAL
    ];

    // Phạm vi phê duyệt chung hiển thị status
    public static $file_const_display_status = [
        self::VERIFICATION_WAITING   => self::VERIFICATION_WAITING_VAL,
        self::VERIFICATION_REJECTED  => self::VERIFICATION_REJECTED_VAL,
        self::UPDATE_INFO_1          => self::UPDATE_INFO_1_VAL,
        self::APPROVAL_REJECTED      => self::APPROVAL_REJECTED_VAL,
        self::APPROVAL_WAITING       => self::APPROVAL_WAITING_VAL,
        self::FEE_WAITING            => self::FEE_WAITING_VAL,
        self::SIGNING_WAITING        => self::SIGNING_WAITING_VAL,
        self::CODE_PROVIDING_WAITING => self::CODE_PROVIDING_WAITING_VAL,
        self::CARD_PRINTING_WAITING  => self::CARD_PRINTING_WAITING_VAL,
        self::UPDATE_INFO_2          => self::UPDATE_INFO_2_VAL,
        self::MEMBER_OFFICIAL_MEMBER => self::MEMBER_OFFICIAL_MEMBER_VAL,
        self::MEMBER_STORED         => self::MEMBER_STORED_VAL,
        self::APPROVE_WAITING_UPDATE         => self::APPROVE_WAITING_UPDATE_VAL,
        self::MEMBER_PENDDING        => self::MEMBER_PENDDING_VAL,
        self::MEMBER_DISTROY         => self::MEMBER_DISTROY_VAL,
        self::CODE_PROVIDING_REWAITING  => 'Chờ cấp lại thẻ',
        130                          => 'Đã in thẻ mới',
        131                          => 'Đã in thẻ gia hạn',
    ];

    const APPROVAL_WAITING_VAL_HISTORY = "Đồng ý thẩm định";
    const FEE_WAITING_VAL_HISTORY = "Đồng ý phê duyệt";

    public static $file_const_history = [
        self::VERIFICATION_WAITING   => self::VERIFICATION_WAITING_VAL,
        self::VERIFICATION_REJECTED  => self::VERIFICATION_REJECTED_VAL,
        self::UPDATE_INFO_1          => self::UPDATE_INFO_1_VAL,
        self::APPROVAL_REJECTED      => self::APPROVAL_REJECTED_VAL,
        self::APPROVAL_WAITING       => self::APPROVAL_WAITING_VAL_HISTORY,
        self::FEE_WAITING            => self::FEE_WAITING_VAL_HISTORY,
        self::APPROVE_WAITING_UPDATE => self::APPROVE_WAITING_UPDATE_VAL
    ];

    public static $file_const = [
        self::VERIFICATION_WAITING   => self::VERIFICATION_WAITING_VAL,
        self::VERIFICATION_REJECTED  => self::VERIFICATION_REJECTED_VAL,
        self::UPDATE_INFO_1          => self::UPDATE_INFO_1_VAL,
        self::APPROVAL_WAITING       => self::APPROVAL_WAITING_VAL,
        self::FEE_WAITING            => self::FEE_WAITING_VAL,
        self::SIGNING_WAITING        => self::SIGNING_WAITING_VAL,
        self::CODE_PROVIDING_WAITING => self::CODE_PROVIDING_WAITING_VAL
    ];

    public static $file_download_const = [
        self::VERIFICATION_WAITING  => self::VERIFICATION_WAITING_VAL,
        self::VERIFICATION_REJECTED => self::VERIFICATION_REJECTED_VAL,
        self::UPDATE_INFO_1         => self::UPDATE_INFO_1_VAL,
        self::APPROVAL_WAITING      => self::APPROVAL_WAITING_VAL,
        self::APPROVAL_REJECTED     => self::APPROVAL_REJECTED_VAL,
        self::APPROVE_WAITING_UPDATE => self::APPROVE_WAITING_UPDATE_VAL,
    ];

    // Phạm vi phê duyệt dành cho chuyên gia
    public static $file_expert_const = [
        self::VERIFICATION_WAITING  => self::VERIFICATION_WAITING_VAL,
        self::VERIFICATION_REJECTED => self::VERIFICATION_REJECTED_VAL,
        self::UPDATE_INFO_1         => self::UPDATE_INFO_1_VAL
    ];

    // Phạm vi phê duyệt dành cho lãnh đạo
    public static $file_leader_const = [
        self::APPROVAL_WAITING  => self::APPROVAL_WAITING_VAL,
        self::APPROVAL_REJECTED => self::APPROVAL_REJECTED_VAL,
        self::APPROVE_WAITING_UPDATE     => self::APPROVE_WAITING_UPDATE_VAL,
    ];

    // Phạm vi phê duyệt dành cho kế toán
    public static $file_accountant_const = [
        self::FEE_WAITING     => self::FEE_WAITING_VAL,
        self::SIGNING_WAITING => self::SIGNING_WAITING_VAL,
    ];

    // Phạm vi phê duyệt dành cho member ship card
    // role = 9 ở màn hình quyết định là status = 5, 6 // decision
    public static $file_membership_card_issuer_const = [
        self::SIGNING_WAITING        => self::SIGNING_WAITING_VAL,
        self::CODE_PROVIDING_WAITING => self::CODE_PROVIDING_WAITING_VAL,
    ];
    // role = 9 ở màn hình tạo mã thẻ là status = 6, 9 // create_card
    public static $file_membership_card_issuer_create_card_const = [
        self::CODE_PROVIDING_WAITING => self::CODE_PROVIDING_WAITING_VAL,
        self::CARD_PRINTING_WAITING  => self::CARD_PRINTING_WAITING_VAL,
        61                           => 'Chờ cấp lại thẻ'
    ];
    // role = 9 ở màn hình in danh sách thẻ status = 9, 13 // print_card
    public static $file_membership_card_issuer_print_card_const = [
        self::CARD_PRINTING_WAITING  => self::CARD_PRINTING_WAITING_VAL,
        self::MEMBER_OFFICIAL_MEMBER => self::MEMBER_OFFICIAL_MEMBER_VAL
    ];

    // role = 9 ở màn hình in danh sách thẻ status = 9, 13 // print_card
    public static $printed_card_status = [
        self::CARD_PRINTING_WAITING  => self::CARD_PRINTING_WAITING_VAL,
        self::MEMBER_OFFICIAL_MEMBER => self::IS_PRINT_VAL
    ];
    // role = Ke toan hieu
    const MEMBERS_ANNUAL_FEES_YES    = 1;
    const MEMBERS_ANNUAL_FEES_YES_VAL = 'Đã Nộp Hội Phí Thường Niên';
    const MEMBERS_ANNUAL_FEES_NO    = 0;
    const MEMBERS_ANNUAL_FEES_NO_VAL = 'Chưa Nộp Hội Phí Thường Niên';
    const MEMBERS_FIRST_FEES_YES_VAL = 'Đã Nộp Hội Phí Lần Đầu';
    public static $member_annual_fees  = [
        -1 => '',
		4 => 'Chờ Đóng Hội Phí Lần Đầu',
		41 => self::MEMBERS_FIRST_FEES_YES_VAL,
        self::MEMBERS_ANNUAL_FEES_YES  => self::MEMBERS_ANNUAL_FEES_YES_VAL,
        self::MEMBERS_ANNUAL_FEES_NO => self::MEMBERS_ANNUAL_FEES_NO_VAL
    ];

    public static $member_official = [
        self::MEMBER_OFFICIAL_MEMBER  => self::MEMBER_OFFICIAL_MEMBER_VAL,
    ];

    public static function getMemberShipCardIssuerStatusScope ($scope = 'decision')
    {
        if ($scope == 'decision') {
            return self::$file_membership_card_issuer_const;
        } elseif ($scope == 'create_card') {
            return self::$file_membership_card_issuer_create_card_const;
        } elseif ($scope == 'print_card') {
            return self::$printed_card_status;
        } else {
            return [];
        }
    }

    // Danh sách văn phòng đại diện
    public static $representative_office = [
        '1' => 'Hà Nội',
        '4' => 'Đà Nẵng',
        '2' => 'HCM',
    ];

    public static $member_typeOfTravelGuide = [
        1 => 'HDV du lịch Quốc Tế',
        2 => 'HDV du lịch Nội Địa',
        3 => 'HDV du lịch Tại Điểm'
    ];

    public static $rank_status = [
        1 => 'Đã đăng ký',
        2 => 'Chưa xếp hạng',
        3 => 'Đã nộp đủ hồ sơ',
        4 => 'Đã xếp hạng'
    ];

    const RANK_RANKED = 4;
    const RANK_UNRANKED = 2;
    const RANK_REGISTER = 1;
    const UNRANK = 10;
    const RANK_UPDATE_FILE = 3;

    public static $member_step = [
        0 => 'Cấp mới',
        1 => 'Gia hạn',
        2 => 'Cấp lại'
    ];

    public static $member_status_in_general_report = [
        self::MEMBER_OFFICIAL_MEMBER  => self::MEMBER_OFFICIAL_MEMBER_VAL,
        130 => 'Hội viên in thẻ lần đầu',
        131 => 'Hội viên in thẻ gia hạn',
        132 => 'Hội viên in thẻ cấp lại',
        self::VERIFICATION_REJECTED => 'Hội viên bị từ chối thẩm định',
        self::APPROVAL_REJECTED => self::APPROVAL_REJECTED_VAL,
        self::MEMBER_STORED => self::MEMBER_STORED_VAL,
        self::MEMBER_DISTROY => self::MEMBER_DISTROY_VAL,
        self::MEMBER_PENDDING => self::MEMBER_PENDDING_VAL,
        self::VERIFICATION_WAITING   => self::VERIFICATION_WAITING_VAL,
        self::UPDATE_INFO_1          => self::UPDATE_INFO_1_VAL,
        self::APPROVAL_WAITING       => self::APPROVAL_WAITING_VAL,
        self::FEE_WAITING            => self::FEE_WAITING_VAL,
        self::SIGNING_WAITING        => self::SIGNING_WAITING_VAL,
        self::CODE_PROVIDING_WAITING => self::CODE_PROVIDING_WAITING_VAL,
        self::UPDATE_INFO_2          => self::UPDATE_INFO_2_VAL,
        self::CODE_PROVIDING_REWAITING  => 'Chờ cấp lại thẻ',
    ];

    const WAITTING_PRINT_ACTION_TYPE = 91;
}
