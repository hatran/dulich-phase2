<?php

namespace App\Constants;


class UserConstants
{
    // user role const
    const NORMAL_USER                    = 587723;  // Hội viên bình thường
    const ACCOUNTANT_USER                = 587724;  // Kế Toán
    const ADMIN_USER                     = 914349;  // Quản trị viên Hội
    // const ADMIN_USER_SUB_GROUP           = 31; // Quản trị viên Chi hội / CLB thuộc Hội
    const CONTENT_MANAGER_SUB_GROUP_USER = 4;  // Biên tập viên Chi hội / CLB thuộc Hội
    const CONTENT_MANAGER_GROUP_USER     = 509575;  // Biên tập viên hội
    // const EXPERT_USER                 = 6;  // Chuyên viên thẩm định hồ sơ (không có role này, đề nghị xóa bỏ)
    const HN_EXPERT_USER                 = 203121; // Chuyên viên thẩm định hồ sơ tại Hà Nội province_type = 1
    const DN_EXPERT_USER                 = 203121; // Chuyên viên thẩm định hồ sơ tại Đà Nẵng province_type = 2
    const HCM_EXPERT_USER                = 203121; // Chuyên viên thẩm định hồ sơ tại TP. HCM province_type = 3
    // const LEADER_USER                 = 7;  // Lãnh đạo Ban / lãnh đạo VPĐD (role này và role 71 giống nhau, đề nghị xóa bỏ)
    const HN_LEADER_USER                 = 111687; // Lãnh đạo Ban tại Hà Nội province_type = 1
    const DN_LEADER_USER                 = 111687; // Lãnh đạo VPĐD tại Đà Nẵng province_type = 2
    const HCM_LEADER_USER                = 111687; // Lãnh đạo VPĐD tại TP. HCM province_type = 3
    const LEADER_MANAGER_USER            = 111687;  // Lãnh đạo Hội
    const MEMBERSHIP_CARD_ISSUER         = 666530;  // Chuyên viên cấp thẻ Hội viên


    // Quản trị viên Hội
    // Biên tập viên Hội
    // Kế toán Hội
    // Quản trị viên Chi hội
    // Biên tập viên Chi hội/CLB thuộc Hội
    // Chuyên viên thẩm định hồ sơ tại Hà Nội province_type = 1
    // Chuyên viên thẩm định hồ sơ tại Đà Nẵng province_type = 2
    // Chuyên viên thẩm định hồ sơ tại TP. HCM province_type = 3
    // Lãnh đạo Ban tại Hà Nội province_type = 1
    // Lãnh đạo VPĐD tại Đà Nẵng province_type = 2
    // Lãnh đạo VPĐD tại TP. HCM province_type = 3
    // Lãnh đạo Hội


    public static $arrUserRoles = [
        self::NORMAL_USER                    => 'Hội viên bình thường',
        self::ACCOUNTANT_USER                => 'Kế toán Hội',
        // self::ADMIN_USER_SUB_GROUP           => 'Quản trị viên Chi hội / CLB thuộc Hội',
        self::ADMIN_USER                     => 'Quản trị viên Hội',
        self::CONTENT_MANAGER_SUB_GROUP_USER => 'Biên tập viên Chi hội / CLB thuộc Hội',
        self::CONTENT_MANAGER_GROUP_USER     => 'Biên tập viên hội',
        // self::EXPERT_USER                    => 'Chuyên viên thẩm định hồ sơ',
        self::HN_EXPERT_USER                 => 'Chuyên viên thẩm định hồ sơ tại Hà Nội',
        self::DN_EXPERT_USER                 => 'Chuyên viên thẩm định hồ sơ tại Đà Nẵng',
        self::HCM_EXPERT_USER                => 'Chuyên viên thẩm định hồ sơ tại TP. HCM',
        // self::LEADER_USER                    => 'Lãnh đạo Ban / lãnh đạo VPĐD',
        self::HN_LEADER_USER                 => 'Lãnh đạo Ban tại Hà Nội',
        self::DN_LEADER_USER                 => 'Lãnh đạo VPĐD tại Đà Nẵng',
        self::HCM_LEADER_USER                => 'Lãnh đạo VPĐD tại TP. HCM',
        self::LEADER_MANAGER_USER            => 'Lãnh đạo Hội',
        self::MEMBERSHIP_CARD_ISSUER         => 'Chuyên viên cấp thẻ Hội viên'
    ];

    public static $arrUserStatus = [
        CommontConstants::ENABLE  => 'Đang hoạt động',
        CommontConstants::DISABLE => 'Khóa',
    ];
}
