<?php
	class SmsBrandnameProvider {

		private $userName;
		private $password;
		private $parameters = array();

		const SERVICE_URI = 'http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl';
				
		const TEST_USER = 'smsbrand_hhdlvietnam';
		const TEST_PASS = '123456a@';
		const TEST_CPCODE = 'HHDLVIETNAM';
		const TEST_ALIAS = 'HOI HDV';
		
		/**
		 * Function to handle SMS Send operation
		 * @param <String> $message
		 * @param <String> $toNumbers One number
		 */
		public function sendBulkSms($message, $toNumbers) {
				$client = new SoapClient(self::SERVICE_URI);
				$params = array(    "User" => self::TEST_USER,    "Password" => self::TEST_PASS,    "CPCode" => self::TEST_CPCODE,    "RequestID" => "1",    "UserID" => $toNumbers,     "ReceiverID" => $toNumbers,    "ServiceID" => self::TEST_ALIAS,    "CommandCode" => "bulksms",    "Content" => $message,    "ContentType" => "0"     );
				$response = $client->__soapCall("wsCpMt", array($params));
				return $response;
		}
	}

	$obj = new SmsBrandnameProvider();
	var_dump($obj->sendBulkSms('TEST','84969398478'));
?>  