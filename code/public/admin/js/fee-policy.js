var isValidate = false;
function validate(val) {
    isValidate = false;
    if(val != "edit") {
        //Validate code
        checkRequire("code","Mã chính sách","form-code") || checkMaxlength("code","Mã chính sách","form-code",255);
    } 
    
    //Validate name
    checkRequire("name","Tên chính sách","form-name") || checkMaxlength("name","Tên chính sách","form-name",255);

    //Validate start date
    checkRequire("start_date","Ngày bắt đầu","form-start-date") ||
     checkDate("start_date","Ngày bắt đầu","form-start-date") || 
     (val != "edit" && checkDateRange("start_date","Ngày bắt đầu","form-start-date","_newdate","bigger","ngày hiện tại")) ||
     checkDateRange("start_date","Ngày bắt đầu","form-start-date","end_date","smaller","Ngày kết thúc")

     //Validate end date
    checkRequire("end_date","Ngày kết thúc","form-end-date") ||
     checkDate("end_date","Ngày kết thúc","form-end-date") || 
     checkDateRange("end_date","Ngày kết thúc","form-end-date","_newdate","bigger","ngày hiện tại") ||
     checkDateRange("end_date","Ngày kết thúc","form-end-date","start_date","bigger","Ngày bắt đầu")

     //Validate money
     checkRequire("money","Mức ưu đãi","form-money") || checkNumber("money","form-money")

     //Validate note
     checkMaxlength("note","Ghi chú","form-note",5000);
}

function removeValidate(id) {
    $(`#${id}`).hasClass("has-error") && $(`#${id}`).removeClass("has-error");
    $(`#${id} .help-block`).html("")
}

function checkNumber(name,id) {
    const val = $(`input[name=${name}]`) ? $(`input[name=${name}]`).val() : "";
    if(isNaN(val)) {
        if(!$(`#${id}`).hasClass("has-error")) $(`#${id}`).addClass("has-error");
        $(`#${id} .help-block`).html(`* Chỉ được nhập số`);
        isValidate = true;
        return true;
    }

    removeValidate(id);
    return false;
}

function checkRequire(name,title,id) {
    const val = $(`input[name=${name}]`) ? $(`input[name=${name}]`).val() : "";
    if(!val) {
        if(!$(`#${id}`).hasClass("has-error")) $(`#${id}`).addClass("has-error");
        $(`#${id} .help-block`).html(`* ${title} là trường bắt buộc nhập`);
        isValidate = true;
        return true;
    }

    removeValidate(id);
    return false;
}

function checkMaxlength(name,title,id,maxlength) {
    const val = $(`input[name=${name}]`) ? $(`input[name=${name}]`).val() : "";
    if(val && val.length > maxlength) {
        if(!$(`#${id}`).hasClass("has-error")) $(`#${id}`).addClass("has-error");
        $(`#${id} .help-block`).html(`* Độ dài tối đa của ${title} là ${maxlength} kí tự`);
        isValidate = true;
        return true;
    }

    removeValidate(id);
    return false;
}

function checkDate(name,title,id) {
    const val = $(`input[name=${name}]`) ? $(`input[name=${name}]`).val() : "";
    if(val && !isDate(val)) {
        if(!$(`#${id}`).hasClass("has-error")) $(`#${id}`).addClass("has-error");
        $(`#${id} .help-block`).html(`* Không đúng định dạng`);
        isValidate = true;
        return true;
    } 

    removeValidate(id);
    return false;
}

function checkDateRange(name,title,id,range,type,title2) {
    const val = $(`input[name=${name}]`) ? $(`input[name=${name}]`).val() : "";
   
    if(val && isDate(val)) {
        let dateRange = 0;
        
        if(range == "_newdate") {
            let newDate = new Date();
            newDate.setHours(0, 0, 0);
            dateRange = newDate.getTime();
        } else {
            const valRange = $(`input[name=${range}]`) ? $(`input[name=${range}]`).val() : "";
            if(isDate(valRange)) {
                const dateRaneStr = convertDate(valRange);
                dateRange = new Date(dateRaneStr) ? new Date(dateRaneStr).getTime() : 0;
            } else {
                removeValidate(id);
                return false;
            }
            
        }

        const dateVal = new Date(convertDate(val)) ? new Date(convertDate(val)).getTime() : 0;

        if(dateRange != 0 && type == "bigger" && dateVal != 0 &&  dateVal < dateRange) {
            if(!$(`#${id}`).hasClass("has-error")) $(`#${id}`).addClass("has-error");
            $(`#${id} .help-block`).html(`* ${title} phải lớn hơn ${title2}`);
            isValidate = true;
            return true;
        } else if(dateRange != 0 && type == "smaller" && dateVal != 0 &&  dateVal > dateRange) {
            if(!$(`#${id}`).hasClass("has-error")) $(`#${id}`).addClass("has-error");
            $(`#${id} .help-block`).html(`* ${title} phải nhỏ hơn ${title2}`);
            isValidate = true;
            return true;
        }
        
    } 

    removeValidate(id);
    return false;
}

// Check is date
const isDate = (date) => {
    return (new Date(date) !== "Invalid Date") && moment(date, "DD/MM/YYYY", true).isValid();
}

//convert date
function convertDate(date) {
    if(date) {
        const valRangeArr = date.split("/");
        return valRangeArr[2] + "-" + valRangeArr[1] + "-" + valRangeArr[0];
    }
    return "";
}