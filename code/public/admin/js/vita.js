var $table = $('#table');

jQuery(window).ready(function () {
    jQuery(".tooltip").tooltip();
});

function cellStyle(value, row, index, field) {
    var classes = [];
    classes['order_number'] = 'text-center';
    classes['action'] = 'text-center';
    return {
        classes: classes[field]
    };
}