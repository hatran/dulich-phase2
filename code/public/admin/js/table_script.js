// init variables
var trcopy;
var editing = 0;
var tdediting = 0;
var editingtrid = 0;
var editingtdcol = 0;
var inputs = ':checked,:selected,:text,textarea,select';

$(document).ready(function () {
    // init table
    blankrow = '<tr valign="top" class="inputform"><td></td>';
    for (i = 0; i < columns.length; i++) {
        // Create input element as per the definition
        input = createInput(i, '');
        blankrow += '<td class="ajaxReq">' + input + '</td>';
    }
    blankrow += '<td class="text-center"><a href="javascript:;" class="' + savebutton + '"><i class="fa fa-save" style="font-size: 18px; color: #ff6600;"></i></a></td></tr>';
    // append blank row at the end of table
    $("." + table).append(blankrow);

    setTimeout(function () {
        $('#inputColumn0').focus();
    }, 100);

    // Delete record
    $(document).on("click", "." + deletebutton, function () {
        var id = $(this).attr("id");
        if (id) {
            var username = $('tr#'+id).find('.l_fullname').html();
            if (confirm("Bạn muốn xóa lãnh đạo " + username)) {
                ajax("rid=" + id, "del");
            }
        }
    });
    buzy = false;
    // Add new record
    $("." + savebutton).on("click", function () {
        var validation = 1;
        var validationPhone = 1;
        $('body').find('.label-err').remove();
        var $inputs =
                $(document).find("." + table).find(inputs).filter(function (i) {
            // check if input element is blank ??
            if (columns[i] == 'l_fullname' || columns[i] == 'l_position') {
                $(this).parent().find('.label-err').remove();
                if ($.trim(this.value) == "") {
                    $(this).removeClass('success').addClass("error_border");
                    $(this).parent().append('<span class="label-err text-danger small" style="white-space: nowrap;">'+placeholder[i]+' không được bỏ trống</span>');
                    validation = 0;
                } else {
                    $(this).removeClass('error_border').addClass("success");
                    $(this).parent().find('.label-err').remove();
                    validation = 1;
                }
            }
            return $.trim(this.value);
        });
        var emailInput = $(this).parent().parent().find('input[name="l_email"]').val();
        if (emailInput != '' && validateEmail(emailInput) === false) {
            $(this).parent().parent().find('input[name="l_email"]').addClass("error_border");
            $(this).parent().parent().find('input[name="l_email"]').parent().append('<span class="label-err text-danger small" style="white-space: nowrap;">Email không hợp lệ</span>');
            validation = 0;
        } else {
            $(this).parent().parent().find('input[name="l_email"]').addClass("success").removeClass('error_border');
            $(this).parent().parent().find('input[name="l_email"]').parent().find('.label-err').remove();
            validation = 1;
        }
        let l_birthdayInput = $(this).parent().parent().find('input[name="l_birthday"]').val();
        if (l_birthdayInput != '') {
            var messvalid = isValidDate(l_birthdayInput);
            if (messvalid != true) {
                $(this).parent().parent().find('input[name="l_birthday"]').addClass("error_border");
                $(this).parent().parent().find('input[name="l_birthday"]').parent().append('<span class="label-err text-danger small" style="white-space: nowrap;">'+isValidDate(l_birthdayInput)+'</span>');
                validationPhone = 0;
            } else {
                $(this).parent().parent().find('input[name="l_birthday"]').addClass("success").removeClass('error_border');
                $(this).parent().parent().find('input[name="l_birthday"]').parent().find('.label-err').remove();
                validationPhone = 1;
            }
        } else {
            $(this).parent().parent().find('input[name="l_birthday"]').addClass("success").removeClass('error_border');
            $(this).parent().parent().find('input[name="l_birthday"]').parent().find('.label-err').remove();
            validationPhone = 1;
        }
        var array = $inputs.map(function () {
            //console.log(this.value);
            //console.log(this);
            return this.value;
        }).get();

        var serialized = $inputs.serialize();
        if (validation == 1 && validationPhone == 1 && buzy == false) {
            ajax(serialized, "save");
        }
    });

    // Add edit record
    $(document).on("click", "." + editbutton, function () {
        var id = $(this).attr("id");
        if (id && editing == 0 && tdediting == 0) {
            // hide editing row, for the time being
            $("." + table + " tr:last-child").fadeOut("fast");

            var html;
            html += "<td>" + $("." + table + " tr[id=" + id + "] td:first-child").html() + "</td>";
            for (i = 0; i < columns.length; i++) {
                // fetch value inside the TD and place as VALUE in input field
                var val = $(document).find("." + table + " tr[id=" + id + "] td[class='" + columns[i] + "']").html();
                input = createInput(i, val);
                html += '<td>' + input + '</td>';
            }
            html += '<td class="text-center"><a title="Thực hiện sửa" href="javascript:;" id="' + id + '" class="' + updatebutton + '"><i class="fa fa-check-circle-o" style="font-size: 18px; color: #ff6600;"></i></a> <a title="Hủy cập nhật" href="javascript:;" id="' + id + '" class="' + cancelbutton + '"><i class="fa fa-times-rectangle" style="font-size: 18px;"></i></a></td>';
            
            // Before replacing the TR contents, make a copy so when user clicks on 
            trcopy = $("." + table + " tr[id=" + id + "]").html();
            $("." + table + " tr[id=" + id + "]").html(html);

            // set editing flag
            editing = 1;
        }
    });

    $(document).on("click", "." + cancelbutton, function () {
        var id = $(this).attr("id");
        $("." + table + " tr[id='" + id + "']").html(trcopy);
        $("." + table + " tr:last-child").fadeIn("fast");
        editing = 0;
    });
    $(document).on("click", "." + updatebutton, function () {
        var validation = 1;
        var validationPhone = 1;
        $('.label-err').remove();
        id = $(this).attr("id");
        var $inputs =
                $(document).find("." + table + ' tr#' + id).find(inputs).filter(function () {
            return this.value == '' ? ' ' : $.trim(this.value);
        });

        var emailInput = $(this).parent().parent().find('input[name="l_email"]').val();
        if (emailInput != '' && validateEmail(emailInput) === false) {
            $(this).parent().parent().find('input[name="l_email"]').addClass("error_border");
            $(this).parent().parent().find('input[name="l_email"]').parent().append('<span class="label-err text-danger small" style="white-space: nowrap;">Email không hợp lệ</span>');
            validation = 0;
        } else {
            $(this).parent().parent().find('input[name="l_email"]').addClass("success").removeClass('error_border');
            $(this).parent().parent().find('input[name="l_email"]').parent().find('.label-err').remove();
            validation = 1;
        }
        let l_birthdayInput = $(this).parent().parent().find('input[name="l_birthday"]').val();
        if (l_birthdayInput != '') {
            var messvalid = isValidDate(l_birthdayInput);
            if (messvalid != true) {
                $(this).parent().parent().find('input[name="l_birthday"]').addClass("error_border");
                $(this).parent().parent().find('input[name="l_birthday"]').parent().append('<span class="label-err text-danger small" style="white-space: nowrap;">'+isValidDate(l_birthdayInput)+'</span>');
                validationPhone = 0;
            } else {
                $(this).parent().parent().find('input[name="l_birthday"]').addClass("success").removeClass('error_border');
                $(this).parent().parent().find('input[name="l_birthday"]').parent().find('.label-err').remove();
                validationPhone = 1;
            }
        } else {
            $(this).parent().parent().find('input[name="l_birthday"]').addClass("success").removeClass('error_border');
            $(this).parent().parent().find('input[name="l_birthday"]').parent().find('.label-err').remove();
            validationPhone = 1;
        }

        var array = $inputs.map(function () {
            return this.value;
        }).get();
        console.log(validation);
        serialized = $inputs.serialize();
        if (validation == 1 && validationPhone == 1 && buzy == false) {
            ajax(serialized + "&rid=" + id, "update");
        }
        // clear editing flag
        editing = 0;
    });

    // td lost focus event

    $(document).on("blur", "." + table + " td", function (e) {
        if (tdediting == 1) {
            var newval = $("." + table + " tr[id='" + editingtrid + "'] td[class='" + editingtdcol + "']").find(inputs).val();
            ajax(editingtdcol + "=" + newval + "&rid=" + editingtrid, "updatetd");
        }
    });

});

createInput = function (i, str) {
    str = typeof str !== 'undefined' ? str : null;
    //alert(str);
    if (columns[i] == 'l_phone') {
        input = '<input maxlength="' + maxLength[i] + '" class="form-control" type=' + inputType[i] + ' name=' + columns[i] + ' placeholder="' + placeholder[i] + '" value="' + str + '" onpaste="return onPasteNumber(event)" onkeypress="return isNumber(event)" >';
    } else if (inputType[i] == "text") {
        input = '<input maxlength="' + maxLength[i] + '" class="form-control" id="inputColumn'+i+'" type=' + inputType[i] + ' name=' + columns[i] + ' placeholder="' + placeholder[i] + '" value="' + str + '" >';
    } else if (inputType[i] == "textarea") {
        input = '<textarea name=' + columns[i] + ' placeholder="' + placeholder[i] + '">' + str + '</textarea>';
    } else if (inputType[i] == "select") {
        input = '<select class="form-control" name=' + columns[i] + '>';
        if (columns[i] == 'l_position') {
            selectOpt2 = selectOpt;
            $.each(selectOpt2, function (key, value) {
                //console.log(selectOpt[i]);
                selected = "";
                if (str == value)
                    selected = "selected";
                input += '<option value="' + key + '" ' + selected + '>' + selectOpt2[key] + '</option>';
            });
        }
        input += '</select>';
        //console.log(str);
    }
    return input;
}

ajax = function (params, action) {
    buzy = true;
    $.ajax({
        type: "POST",
        url: ajaxSaveData,
        headers: {
            'X-CSRF-TOKEN': X_CSRF_TOKEN
        },
        data: params + "&action=" + action + '&branchesId=' + currentBranchId,
        dataType: "json",
        success: function (response) {
            switch (action) {
                case "save":
                    var seclastRow = $("." + table + " tr").length;
                    if (response.success == 1) {
                        var indexRow = parseInt(seclastRow - 1);
                        if ($("." + table).find('.no-item').length > 0) {
                            $("." + table).find('.no-item').remove();
                            indexRow = indexRow - 1;
                        }
                        var html = "";

                        html += "<td class='text-center'>" + indexRow + "</td>";
                        for (i = 0; i < columns.length; i++) {
                            html += '<td class="' + columns[i] + '">' + response[columns[i]] + '</td>';
                        }
                        html += '<td class="text-center"><a href="javascript:;" id="' + response["id"] + '" class="ajaxEdit"><i class="fa fa-edit" style="font-size: 18px; color: #ff6600;"></i></a> <a href="javascript:;" id="' + response["id"] + '" class="' + deletebutton + '"><i class="fa fa-times" style="font-size: 18px;"></i></a></td>';
                        // Append new row as a second last row of a table
                        $("." + table + " tr").last().before('<tr id="' + response.id + '">' + html + '</tr>');

                        if (effect == "slide") {
                            // Little hack to animate TR element smoothly, wrap it in div and replace then again replace with td and tr's ;)
                            $("." + table + " tr:nth-child(" + seclastRow + ")").find('td')
                                    .wrapInner('<div style="display: none;" />')
                                    .parent()
                                    .find('td > div')
                                    .slideDown(100, function () {
                                        var $set = $(this);
                                        $set.replaceWith($set.contents());
                                    });
                        } else if (effect == "flash") {
                            $("." + table + " tr:nth-child(" + seclastRow + ")").effect("highlight", {color: '#acfdaa'}, 100);
                        } else
                            $("." + table + " tr:nth-child(" + seclastRow + ")").effect("highlight", {color: '#acfdaa'}, 1000);
                        showMess('Thêm mới thành công');
                        // Blank input fields
                        $('tr.inputform td').find('input').val('');
                    }
                    break;
                case "del":
                    if (response.success == 1) {
                        $("." + table + " tr[id='" + response.id + "']").slideUp().remove();
                        showMess('Xóa lãnh đạo thành công');
                        var indexTd = 0;
                        $("." + table + " tr td:first-child").each(function() {
                            if ($(this).text() != '') {
                                $(this).text(indexTd+1);
                            }
                            indexTd++;
                        });
                    }
                    break;
                case "update":
                    $("." + cancelbutton).trigger("click");
                    for (i = 0; i < columns.length; i++) {
                        $("tr[id='" + response.id + "'] td[class='" + columns[i] + "']").html(response[columns[i]]);
                    }
                    break;
                case "updatetd":
                    //$("."+cancelbutton).trigger("click");
                    var newval = $("." + table + " tr[id='" + editingtrid + "'] td[class='" + editingtdcol + "']").find(inputs).val();

                    //alert($("."+table+" tr[id='"+editingtrid+"'] td[class='"+editingtdcol+"']").html());
                    $("." + table + " tr[id='" + editingtrid + "'] td[class='" + editingtdcol + "']").html(newval);

                    //$("."+table+" tr[id='"+editingtrid+"'] td[class='"+editingtdcol+"']").html(newval);
                    // remove editing flag
                    tdediting = 0;
                    $("." + table + " tr[id='" + editingtrid + "'] td[class='" + editingtdcol + "']").effect("highlight", {color: '#acfdaa'}, 1000);
                    break;
            }
            if (response.error) {
                $('.ajax-message').removeClass('alert-success').removeClass('hidden').addClass('alert-danger').html(response.error);
                setTimeout(function () {// wait for 5 secs(2)
                    window.parent.location.reload(); // then reload the page.(3)
                }, 2000);
            }
            buzy = false;
        },
        error: function () {
            //alert("Unexpected error, Please try again");
            buzy = false;
        }
    });
}
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function showMess(__mes) {
    $('.ajax-message').html(__mes).removeClass('hidden');
    setTimeout(function(){
        $('.ajax-message').addClass('hidden');
    }, 2000);
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function onPasteNumber(evt) {
    try {
        var txt = evt.clipboardData.getData('text/plain');
        setTimeout(function () {
            $(evt.target).val(txt.replace(/[^0-9]/g, ''));
        }, 5);
    } catch (err) {
    }
}
function isValidDate(dateString) {
    if (!isValidDateFormat(dateString)) {
        return 'Ngày sinh định dạng là dd/mm/yyyy';
    }
    var res = dateString.split("/");
    var date1 = new Date(res[2] + '-' + res[1] + '-' + res[0]);
    var date2 = new Date(getCurrentDate());
    if( (date1.getTime() > date2.getTime())) {
        return 'Ngày sinh phải nhỏ hơn ngày hiện tại';
    }

    return true;
}
function isValidDateFormat(date)
{
    var matches = /^(\d{1,2})[\/](\d{1,2})[\/](\d{4})$/.exec(date);
    if (matches == null) return false;
    var d = matches[1];
    var m = matches[2] - 1;
    var y = matches[3];
    var composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
}
function getCurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0'+dd;
    }
    if (mm < 10) {
        mm = '0'+mm;
    }
    todayString = yyyy + '-' + mm + '-' + dd;
    return todayString;
}