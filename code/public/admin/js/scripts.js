$(document).ready(function(){
    //$(this).tooltip();
    //nav
    $(".fa-navicon").click(function(){
        $(".main-menu").toggleClass("show");
    });
    $(".main-menu").on("click", function(e){
        if(e.target === this){
            $(this).removeClass("show");
        }
    });

    //upload file
    $('input[type=file]').change(function() {
        var filename = $(this)[0].files[0].name;
        $(".spli.icon-close").remove();
        $(this).parent().find("label").removeClass("fa fa-upload");
        $(this).parent().prepend("<span class='spli icon-close'></span>");
        $(this).parent().find("label").text(filename);
        $(".spli.icon-close").click(function(){
            $(this).parent().find("input[type=file]").val("");
            $(this).parent().find("label")
                .addClass("fa fa-upload")
                .text(" Tải hình ảnh đại diện của bạn lên");
            $(this).remove();
        });
    });

    //radio
    $(".radio-list .choice").click(function(){
        $(this).parent().find(".choice").removeClass("chosen");
        $(this).addClass("chosen");
    });

    //checkbox
    $(".checkbox-list .choice").click(function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this).toggleClass("chosen");
        $(this).find("input").prop("checked", !$(this).find("input").prop("checked"));
    });

    //print
    function checkedNames() {
        check = 0;
        $("input[name=member]:checked").each( function () {
            check = 1;
        });
        return check;
    }
    $("#print_button").click(function() {
        if (checkedNames() == 1) {
            var printData = [];
            for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                printData.push($("input[name='member']:checked")[i].value);
            }
            $("input[name='print']").val(printData);
            var hasSearch = window.location.href.indexOf('?');
            if (hasSearch > 1) {
                window.location.href = window.location.href + '&print=' + printData;
            }
            else {
                window.location.href = window.location.href + '?print=' + printData;
            }
        }else{
            toastr.clear();
            $("#print_button").css('pointer-events', 'none');
            toastr.options.onHidden = function() { $("#print_button").css('pointer-events', 'auto'); }
            toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
        }
    }).focusout (function(){
        if (checkedNames() == 1) {
            window.location.href = '/officesys/member/export_card_list';
            return false;
        }
    });;

    //date time
    $('.date').datetimepicker({
        locale: 'vi',
        viewMode: 'days',
        format: 'DD/MM/Y',
    });
});
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function onPasteNumber(evt) {
    try {
        var txt = evt.clipboardData.getData('text/plain');
        setTimeout(function () {
            $(evt.target).val(txt.replace(/[^0-9]/g, ''));
        }, 5);
    } catch (err) {
    }
}
jQuery.fn.putCursorAtEnd = function() {

  return this.each(function() {
    
    // Cache references
    var $el = $(this),
        el = this;

    // Only focus if input isn't already
    if (!$el.is(":focus")) {
     $el.focus();
    }

    // If this function exists... (IE 9+)
    if (el.setSelectionRange) {

      // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
      var len = $el.val().length * 2;
      
      // Timeout seems to be required for Blink
      setTimeout(function() {
        el.setSelectionRange(len, len);
      }, 1);
    
    } else {
      
      // As a fallback, replace the contents with itself
      // Doesn't work in Chrome, but Chrome supports setSelectionRange
      $el.val($el.val());
      
    }

    // Scroll to the bottom, in case we're in a tall textarea
    // (Necessary for Firefox and Chrome)
    this.scrollTop = 999999;

  });

};