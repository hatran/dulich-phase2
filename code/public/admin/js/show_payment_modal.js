function showPopup(memberId, postfix) {
    var modalId = '#' + postfix + 'Modal';
    var title = 'Chi tiết thông tin hội phí';
    $('.modal-title').text(title);
    $(modalId).modal('show');
    $.ajax({
        url: urlPayment + "/" + memberId,
        type: 'GET',
        success: function (response) {
            var objMember = response.objMember;
            var objMemberPayment = response.objMemberPayment;
    
            $('#code' + postfix).text(objMember.touristGuideCode);
            $('#name' + postfix).text(objMember.fullName);
            $('#birthday' + postfix).text(objMember.formattedBirthday);
            $("#verified_at").val(toTimestamp(objMember.verified_at));
            $("#member_code" + postfix).text(objMember.member_code);
            $("#identitycard" + postfix).text(objMember.cmtCccd);
            $("#joining" + postfix).text(objMember.member_from);
            $("#provincecode" + postfix).text(objMember.provinceName);
            var table_payment = $('.table_payment tbody');
            $('.table_payment tbody tr').remove();
            if (objMemberPayment !== null) {
                objMemberPayment.forEach(function (payment, index) {
                    var paymentNote = payment.note != null ? payment.note : '';
                    var paymentContent = payment.payment_content != null ? payment.payment_content : '';
                    var incre = index + 1;
                    table_payment.append(
                        "<tr>" +
                        "<td>" + incre + "</td>" +
                        "<td>" + payment.number_payment + "</td>" +
                        "<td>" + payment.date_payment + "</td>" +
                        "<td>" + payment.currency + "</td>" +
                        "<td>" + payment.member_code_expiration.substr(0,10).replaceAll('-', '/') + "</td>" +
                        "<td>" + paymentContent + "</td>" +
                        "<td>" + paymentNote + "</td>" +
                        "</tr>"
                    );
                });
            }
        }
    });
}
$(document).on('click', '.show-modal-payment', function (event) {
    event.preventDefault();
    var memberId = $(this).data('id');
    showPopup(memberId, '_show');
});

function toTimestamp(strDate){
    var datum = Date.parse(strDate);
    return datum/1000;
}