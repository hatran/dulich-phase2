var ValidateSize = function (file) {
    if (file != null) return true;
    var zFileSize = file.files[0].size / 1024 / 1024; // in MB
    if (zFileSize > 10) {
        return false;
    } else {
        return true;
    }
};

jQuery(window).ready(function () {
    jQuery(".tooltip").tooltip();
    jQuery('.error').hide();
});

var isViewModal = function (type) {
    if (type == '1') {
        jQuery('.modal-footer.view').show();
        jQuery('.modal-footer.edit').hide();
    } else {
        jQuery('.modal-footer.view').hide();
        jQuery('.modal-footer.edit').show();
    }
};

var viewmodal = function (url) {
    $.ajax({
        url: url,
        success: function (result) {
            $("#viewModal .content").html(result);
            jQuery("#viewModal").modal();
        }
    });
};

function validateImg() {
    jQuery('#upload-image-error').hide();
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if (ValidateSize(document.getElementById("upload-image")) == false) {
        jQuery('#upload-image').val('');
	jQuery('#upload-image-error').text("Chỉ được upload file tối đa 10MB ");
        jQuery('#upload-image-error').show();
        return false;
    } else if (jQuery("#upload-image").val() != '' && jQuery.inArray(jQuery("#upload-image").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        jQuery('#upload-image').val('');
	jQuery('#upload-image-error').text("Các định dạng cho phép : " + fileExtension.join(', '));
        jQuery('#upload-image-error').show();
        return false;
    }
    return true;
}

setTimeout(function () {
    $('.alert').slideUp();
}, 10000);


function validateForm(){
    var inputValidate = true;
    jQuery('.form-control.required').each(function () {
        var input = jQuery(this);
        if (input.val() == '') {
            input.addClass('is-invalid');
            input.parent().find('.error').show();
            inputValidate = false;
        } else {
            input.removeClass('is-invalid');
            input.parent().find('.error').hide();
        }
    });
    return inputValidate;
}
