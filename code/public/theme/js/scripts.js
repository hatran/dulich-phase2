$(document).ready(function(){
    //nav
    $(".fa-navicon").click(function(){
        $(".main-menu").toggleClass("show");
    });
    $(".main-menu").on("click", function(e){
        if(e.target === this){
            $(this).removeClass("show");
        }
    });

    //slide
    $(".page-slide").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true
    });

    //tab
    $(".tabs .tab").click(function(){
        $(".tabs .tab, .table-content").removeClass("active");
        $(this).addClass("active");
        $($(this).attr("data-tab")).addClass("active");
    });

    //related news
    $(".related-news").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<span class="fa fa-angle-left"></span>',
        nextArrow: '<span class="fa fa-angle-right"></span>',
        autoplay: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    //blocks
    $(".block-heading").click(function(){
        $(this).parent().find(".block-content").toggleClass("showout");
    });

    //date time
    $('.date').datetimepicker({
        locale: 'vi',
        viewMode: 'days',
        format: 'DD'
    }).val('');
    $('.month').datetimepicker({
        locale: 'vi',
        viewMode: 'months',
        format: 'MM'
    }).val('');
    $('.year').datetimepicker({
        locale: 'vi',
        viewMode: 'years',
        format: 'YYYY',
        maxDate: new Date()
    }).val('');
    $('#expireDate').datetimepicker({
        locale: 'vi',
        format: 'DD/MM/YYYY',
        minDate: new Date()
    }).val('');
    $("#cmtDate").datetimepicker({
        locale: 'vi',
        format: 'DD/MM/YYYY',
        maxDate: new Date()
    }).val('');

    //upload file
    $(document).on('change', 'input[type=file]', function () {
        var filename = $(this)[0].files[0].name,
            $parent = $(this).parent(),
            /*autoRemove = $parent.attr('data-auto-remove'),*/
            $label = $parent.find("label");
        $label.removeClass("fa fa-upload");
        var currentText = $label.text();
        $label.text(filename);
        if ($parent.find(".spli.icon-close").length === 0) {
            $parent.prepend("<span class='spli icon-close'></span>")
                .find(".spli.icon-close")
                .click(function () {
                    $parent.find("input[type=file]").val('');
                    $parent.find("label")
                        .addClass("fa fa-upload")
                        .text(currentText);
                    $(this).remove();
                    /*if (autoRemove) {
                        $(this).parent().remove();
                    }
                    else {
                        $(this).remove();
                    }*/
                });
        }
    });

    //input
    $('#phone1, #phone2').on('keypress', function(e){
        return e.metaKey || e.which <= 0 || e.which === 8 || /[0-9]/.test(String.fromCharCode(e.which));
    });

    //radio
    $(".selectdrop input").val($(this).parent().find(".selectdropdown .active.choice label").text());
    $(".selectdrop").click(function(){
        $(this).find(".selectdropdown").toggleClass("show");
    });
    $(".selectdropdown .choice").click(function(){
        $(this).closest(".selectdrop").find(".selectdrop-input").val($(this).find("label").text());
    });
    $('.radio-list .choice').click(function(){
        $(this).closest(".field-wrap").find("input[type=radio]").prop("checked", false);
        $(this).find("input[type=radio]").prop("checked", true);
        $(this).closest(".field-wrap").find(".choice").removeClass("chosen");
        $(this).addClass("chosen");

        /*if($(this).find("input[name=typeOfTravelGuide]").val() === "3"){
            $('#expireDate').parent().removeClass("hidden");
        }else{
            $('#expireDate').parent().addClass("hidden");
            $('#expireDate').val("");
        }*/
    });

    $("#submit_button_register").css('pointer-events', 'none');
    //checkbox
    $("#agreePolicy").click(function(e){
        /*e.stopPropagation();
        e.preventDefault();*/
        $("#mustAccepted").toggleClass("chosen");
        if($("#mustAccepted").hasClass("chosen")) {
            $("#submit_button_register").css('pointer-events', 'all');
        } else {
            $("#submit_button_register").css('pointer-events', 'none');
        }
        console.log("aaa");
    });
    
    //add field
    var $workBlock = $('#work-block'),
        workId = 1;
    $workBlock.on('click', '.delete-field', function () {
        $(this).parent().remove();

        var order = 0;
        $workBlock.find('.ol-list').each(function () {
            $(this).attr('data-ol', ++order);
        });
    });
    $(".add-field").click(function () {
        ++workId;
        $('<div class="new-ol ol-list" data-ol="0">\n' +
            '    <div class="field-wrap col-md-4 col-sm-12 col-xs-12">\n' +
            '        <label for="company' + workId + '">Tên doanh nghiệp đã và đang công tác</label>\n' +
            '        <input id="company' + workId + '" type="text" name="workElementNames[' + workId + ']" >\n' +
            '    </div>\n' +
            '    <div class="dropfield field-wrap col-md-3 col-sm-2 col-xs-12">\n' +
            '        <label for="jobStatus' + workId + '">Hình thức lao động</label>\n' +
            '        <select id="jobStatus' + workId + '" name="workTypeOfContracts[' + workId + ']">\n' +
            '            <option value="0" selected>Hợp đồng lao động</option>\n' +
            '            <option value="1">Freelancer</option>\n' +
            '        </select>\n' +
            '    </div>\n' +
            '    <div class="col-md-5 col-sm-6 col-xs-12">\n' +
            '        <div class="row">\n' +
            '            <div class="datetime field-wrap col-md-6">\n' +
            '                <label for="jobYearFrom' + workId + '">Thời gian bắt đầu</label>\n' +
            '                <input id="jobYearFrom' + workId + '" class="year datetime-input" type="text" maxlength="4" name="workFromDates[' + workId + ']" placeholder="Từ năm">\n' +
            '                <span class="datetime-icon fa fa-calendar"></span>\n' +
            '            </div>\n' +
            '            <div class="datetime field-wrap col-md-6">\n' +
            '                <label for="jobYearTo' + workId + '">Thời gian kết thúc</label>\n' +
            '                <input id="jobYearTo' + workId + '" class="year datetime-input" type="text" maxlength="4" name="workToDates[' + workId + ']"  placeholder="Đến năm">\n' +
            '                <span class="datetime-icon fa fa-calendar"></span>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '    <span class="delete-field icon-close"></span>\n' +
            '    <div class="clearfix"></div>\n' +
            '</div>').insertBefore($(this));
        var order = 0;
        $workBlock.find('.ol-list').each(function () {
            $(this).attr('data-ol', ++order);
        });

        $workBlock.find('.ol-list').last().find('.year').datetimepicker({
            locale: 'vi',
            viewMode: 'years',
            format: 'YYYY'
        });
    });

    var $educationBlock = $('#education-block'),
        $educationBlockSpecialized = $educationBlock.find('#specialized');
    $educationBlockSpecialized.change(function () {
        $educationBlock.find('#level').val("");
    });

    var $majorBlock = $('#major-block'),
        $majorBlockUploadList = $majorBlock.find('.upload-list'),
        majorFileId = 1;
    $majorBlockUploadList.on('change', '[type="file"]', function () {
        if ($(this).attr('id') === 'majorFile' + majorFileId) {
            ++majorFileId;
            $majorBlockUploadList.append('<div class="btn-upload" data-auto-remove="1">\n' +
                '    <label class="fa fa-upload" for="majorFile' + majorFileId + '"> Tải lên bản sao có chứng thực văn bằng hoặc giấy chứng nhận</label>\n' +
                '    <input id="majorFile' + majorFileId + '" type="file" name="majorFiles[]" accept="image/*">\n' +
                '</div>')
        }
    });

    var $languageSkillBlock = $('#language-skill-block'),
        $languageSkillBlockChoices = $languageSkillBlock.find('.radio-list .choice'),
        $languageSkillBlockList = $languageSkillBlock.find('.list'),
        $languageSkillBlockLanguage = $languageSkillBlock.find('#language'),
        languageSkillId = 0;
    $languageSkillBlockLanguage.change(function () {
        $languageSkillBlockChoices.removeClass('chosen');
    });
    $languageSkillBlock.on('click', '.delete-field', function () {
        $(this).parent().remove();

        var order = 0;
        $languageSkillBlock.find('.list-order').each(function () {
            $(this).text(++order);
        });
    });
    $languageSkillBlockChoices.click(function (e) {
        var $chosen = $languageSkillBlockChoices.filter('.chosen');
        var langId = $languageSkillBlockLanguage.val(),
            levelId = $chosen.attr('data-id');
        if (langId && levelId && $('#lang-skill-' + langId + '-' + levelId).length <= 0) {
            var langName = $languageSkillBlockLanguage.find('[value="' + langId + '"]').text(),
                levelName = $chosen.text();
            ++languageSkillId;
            /*$languageSkillBlockList.append('<li id="lang-skill-' + langId + '-' + levelId + '">\n' +
                '   <span class="delete-field icon-close"></span>\n' +
                '   <span class="list-order"></span>. ' + langName + ' - ' + levelName + '\n' +
                '   <span class="btn-upload">\n' +
                '       <label class="fa fa-upload" for="languageFile' + languageSkillId + '"> Tải lên bản sao có chứng thực văn bằng</label>\n' +
                '       <input id="languageFile' + languageSkillId + '" type="file" name="languageFiles[' + languageSkillId + ']" accept="image/!*">\n' +
                '   </span>\n' +
                '   <input type="hidden" name="languages[' + languageSkillId + ']" value="' + langId + '">\n' +
                '   <input type="hidden" name="languageLevels[' + languageSkillId + ']" value="' + levelId + '">\n' +
                '</li>');*/

            $languageSkillBlockList.append('<li id="lang-skill-' + langId + '-' + levelId + '">\n' +
                '   <span class="delete-field icon-close"></span>\n' +
                '   <span class="list-order"></span>. ' + langName + ' - ' + levelName + '\n' +
                '   <input type="hidden" name="languages[' + languageSkillId + ']" value="' + langId + '">\n' +
                '   <input type="hidden" name="languageLevels[' + languageSkillId + ']" value="' + levelId + '">\n' +
                '</li>');

            var order = 0;
            $languageSkillBlockList.find('.list-order').each(function () {
                $(this).text(++order);
            });
        }
    });

    var $achievementBlock = $('#achievement-block'),
        $achievementBlockUploadList = $achievementBlock.find('.upload-list'),
        achievementFileId = 1;
    $achievementBlockUploadList.on('change', '[type="file"]', function () {
        if ($(this).attr('id') === 'achievement' + achievementFileId) {
            ++achievementFileId;
            $achievementBlockUploadList.append('<div class="btn-upload" data-auto-remove="1">\n' +
                '    <label class="fa fa-upload" for="achievement' + achievementFileId + '"> Tải lên các thành tích</label>\n' +
                '    <input id="achievement' + achievementFileId + '" type="file" name="achievementFiles[]" accept="image/*">\n' +
                '</div>')
        }
    });

    //step
    $(".next-step").click(function(){
        $("form[name=user-info]").submit();
    });
    $(".prev-step").click(function(){
        window.history.back();
    });

    //validation
    $("form[name=user-info]").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            fullName: {
                required: true
            },
            gender: {
                required: true,
                valueNotEquals: ""
            },
            birthDate: {
                required: true
            },
            birthMonth: {
                required: true
            },
            birthYear: {
                required: true
            },
            userPhoto: {
                required: true
            },
            touristGuideCode: {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 9
            },
            expirationDate: {
                required: true
            },
            cmtCccd: {
                required: true
            },
            dateIssued: {
                required: true
            },
            issuedBy: {
                required: true
            },
            permanentAddress: {
                required: true
            },
            address: {
                required: true
            },
            firstMobile: {
                required: true,
                digits: true
            },
            firstEmail: {
                required: true,
                email: true
            },
            emailConfirm: {
                required: true,
                email: true,
                equalTo: "#email1"
            },
            emailToken: {
                required: true
            },
            phoneToken: {
                required: true
            },
            accept: {
                required: true
            },
            typeOfTravelGuide: {
                required: true
            },
            typeOfPlace: {
                required: true
            },
            specialized: {
                required: true
            },
            educationBranches: {
                required: true,
                valueNotEquals: ""
            },
            educationDegrees: {
                required: true,
                valueNotEquals: ""
            },
            level: {
                required: true
            },
            majors: {
                required: true
            },
            language: {
                required: true
            },
            level2: {
                required: true
            },
            "workElementNames[1]": {
                required: true
            },
            "workTypeOfContracts[1]": {
                required: true
            },
            "workFromDates[1]": {
                required: true
            },
            "workToDates[1]": {
                required: true
            },
            typeGuides: {
                required: true,
                valueNotEquals: ""
            },
            groupSizes: {
                required: true,
                valueNotEquals: ""
            },
            type: {
                required: true
            },
            strength: {
                required: true
            },
            experienceYear: {
                required: true
            },
            experienceLevel: {
                required: true
            }
        },

        messages: {
            fullName: {
                required: "Trường bắt buộc"
            },
            gender: {
                required: "Trường bắt buộc",
                valueNotEquals: "Trường bắt buộc"
            },
            birthDate: {
                required: "Trường bắt buộc"
            },
            birthMonth: {
                required: "Trường bắt buộc"
            },
            birthYear: {
                required: "Trường bắt buộc"
            },
            userPhoto: {
                required: "Trường bắt buộc"
            },
            touristGuideCode: {
                required: "Trường bắt buộc",
                number: "Số thẻ HDV du lịch phải là số",
                minlength: "Số thẻ HDV du lịch phải là số có 9 chữ số",
                maxlength: "Số thẻ HDV du lịch phải là số có 9 chữ số"
            },
            expirationDate: {
                required: "Trường bắt buộc"
            },
            cmtCccd: {
                required: "Trường bắt buộc"
            },
            dateIssued: {
                required: "Trường bắt buộc"
            },
            issuedBy: {
                required: "Trường bắt buộc"
            },
            permanentAddress: {
                required: "Trường bắt buộc"
            },
            address: {
                required: "Trường bắt buộc"
            },
            firstMobile: {
                required: "Trường bắt buộc",
                digits: "Số điện thoại phải là số"
            },
            firstEmail: {
                required: "Trường bắt buộc",
                email: "Định dạng email không hợp lệ"
            },
            emailConfirm: {
                required: "Trường bắt buộc",
                email: "Định dạng email không hợp lệ",
                equalTo: "Email xác nhận không khớp"
            },
            emailToken: {
                required: "Trường bắt buộc"
            },
            phoneToken: {
                required: "Trường bắt buộc"
            },
            accept: {
                required: "Bạn phải đồng ý với quy chế hoạt động và quy định của Hội hướng dẫn viên du lịch Việt Nam"
            },
            typeOfTravelGuide: {
                required: "Trường bắt buộc"
            },
            typeOfPlace: {
                required: "Trường bắt buộc"
            },
            educationBranches: {
                required: "Trường bắt buộc"
            },
            educationDegrees: {
                required: "Trường bắt buộc"
            },
            specialized: {
                required: "Trường bắt buộc"
            },
            level: {
                required: "Trường bắt buộc"
            },
            majors: {
                required: "Trường bắt buộc"
            },
            language: {
                required: "Trường bắt buộc"
            },
            level2: {
                required: "Trường bắt buộc"
            },
            "workElementNames[1]": {
                required: "Trường bắt buộc"
            },
            "workTypeOfContracts[1]": {
                required: "Trường bắt buộc"
            },
            "workFromDates[1]": {
                required: "Trường bắt buộc"
            },
            "workToDates[1]": {
                required: "Trường bắt buộc"
            },
            typeGuides: {
                required: "Trường bắt buộc"
            },
            groupSizes: {
                required: "Trường bắt buộc"
            },
            type: {
                required: "Trường bắt buộc"
            },
            strength: {
                required: "Trường bắt buộc"
            },
            experienceYear: {
                required: "Trường bắt buộc"
            },
            experienceLevel: {
                required: "Trường bắt buộc"
            }
        },
        highlight: function(element) {
            $(element).closest('.field-wrap, .btn-upload').addClass('has-error');
        },
        unhighlight: function (error) {
            $(error).closest('.field-wrap, .btn-upload').find(".help-block").remove();
            console.log($(error));
        },
        invalidHandler: function() {
            $(this).find(".field-wrap.has-error:first").focus();
        },
        errorPlacement: function(error, element) {
            element.closest(".field-wrap, .btn-upload, .field-col").append(error);
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg !== value;
    }, "Value must not equal arg.");
});
