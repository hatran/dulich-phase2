$(document).ready(function(){
    //nav
    $(".fa-navicon").click(function(){
        $(".main-menu").toggleClass("show");
    });
    $(".main-menu").on("click", function(e){
        if(e.target === this){
            $(this).removeClass("show");
        }
    });

    //slide
    $(".page-slide").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        autoplay: true
    });

    //tab
    $(".tabs .tab").click(function(){
        $(".tabs .tab, .table-content").removeClass("active");
        $(this).addClass("active");
        $($(this).attr("data-tab")).addClass("active");
    });

    //related news
    $(".related-news").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<span class="fa fa-angle-left"></span>',
        nextArrow: '<span class="fa fa-angle-right"></span>',
        autoplay: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    //blocks
    $(".block-heading").click(function(){
        $(this).parent().find(".block-content").toggleClass("showout");
    });

    //date time
    var today = new Date();
	var five_years_ago = new Date(today.getFullYear()-5,today.getMonth(),today.getDay());
	var a_week_ago = new Date(today.getFullYear(),today.getMonth(),today.getDay()-7);
	var next_week = new Date(today.getFullYear(),today.getMonth()+1,today.getDay());
	var next_year = new Date(today.getFullYear()+7,today.getMonth(),today.getDay());
    $('.day').datetimepicker({
        locale: 'vi',
        viewMode: 'days',
        format: 'DD'
    }).val('');
    $('.month').datetimepicker({
        locale: 'vi',
        viewMode: 'months',
        format: 'MM'
    }).val('');
    $('.year').datetimepicker({
        locale: 'vi',
        viewMode: 'years',
        format: 'YYYY',
        maxDate: today
    }).val('');
    $('#expireDate').datetimepicker({
        locale: 'vi',
        format: 'DD/MM/YYYY',
        minDate: next_week
    }).val('');
    $("#cmtDate").datetimepicker({
        locale: 'vi',
        format: 'DD/MM/YYYY',
        maxDate: a_week_ago
    }).val('');

	 $('#year').datetimepicker({
        locale: 'vi',
        viewMode: 'years',
        format: 'YYYY',
        maxDate: five_years_ago
    }).val('');

    // var selectAvatar = function (filename, $input) {
    //     var $parent = $input.parent(),
    //         $label = $parent.find("label");
    //     $input.closest(".btn-upload").find(".help-block").remove();
    //     $label.removeClass("fa fa-upload");
    //     var currentText = $label.text();
    //     $label.text(filename);
    //     $('#fileName').val(filename);
    //     if ($parent.find(".spli.icon-close").length === 0) {
    //         $parent.prepend("<span class='spli icon-close'></span>")
    //             .find(".spli.icon-close")
    //             .click(function () {
    //                 $parent.find("input[type=file]").val('');
    //                 $('#fileName').val('');
    //                 $parent.find("label")
    //                     .addClass("fa fa-upload")
    //                     .text(currentText);
    //                 $(this).remove();
    //             });
    //     }
    // };
    //
    // $(document).on('change', 'input[type=file]', function () {
    //     var filename = $(this)[0].files[0].name;
    //     selectAvatar(filename, $(this));
    // });


    //input
    $('#phone1, #phone2').on('keypress', function(e){
        return e.metaKey || e.which <= 0 || e.which === 8 || /[0-9]/.test(String.fromCharCode(e.which));
    });

    //radio
    $(".selectdrop input").val($(this).parent().find(".selectdropdown .active.choice label").text());
    $(".selectdrop").click(function(){
        $(this).find(".selectdropdown").toggleClass("show");
    });
    $(".selectdropdown .choice").click(function(){
        $(this).closest(".selectdrop").find(".selectdrop-input").val($(this).find("label").text());
    });
    $('.radio-list .choice').click(function(){
        $(this).closest(".field-wrap").find("input[type=radio]").prop("checked", false);
        $(this).find("input[type=radio]").prop("checked", true);
        $(this).closest(".field-wrap").find(".choice").removeClass("chosen");
        $(this).addClass("chosen");

        if($(this).find("input[name=typeOfTravelGuide]").length) {
            if ($(this).find("input[name=typeOfTravelGuide]").val() === "3") {
                /*$('#expireDate').parent().removeClass("hidden");
                $('#expireDate').val("");*/
                $('#inboundOutbound').parent().addClass("hidden");
                $("#language-skill-block .block-heading .star").hide();
                $('#expireDate').parent().addClass("hidden");
            } else if ($(this).find("input[name=typeOfTravelGuide]").val() === "1") {
                $('#inboundOutbound').parent().removeClass("hidden");
                $('#inboundOutbound').val("");
                // $('#expireDate').parent().addClass("hidden");
                $("#language-skill-block .block-heading .star").show();
                $('#expireDate').parent().removeClass("hidden");
            } else {
                if ($(this).find("input[name=typeOfTravelGuide]").val() === "2") {
                    $("#language-skill-block .block-heading .star").hide();
                }
                // $('#expireDate, #inboundOutbound').parent().addClass("hidden");
                $('#inboundOutbound').parent().addClass("hidden");
                $('#expireDate').parent().removeClass("hidden");
            }
        }
    });

    $("#submit_button_register").css('pointer-events', 'none');
    //checkbox
    $("#agreePolicy").click(function(e){
        /*e.stopPropagation();
        e.preventDefault();*/
        $("#mustAccepted").toggleClass("chosen");
		if($("#mustAccepted").hasClass("chosen")) {
			$("#submit_button_register").css('pointer-events', 'all');
		} else {
			$("#submit_button_register").css('pointer-events', 'none');
		}
		console.log("aaa");
    });

    //add field
    var $workBlock = $('#work-block'),
        workId = 1;
    $workBlock.on('click', '.delete-field', function () {
        $(this).parent().remove();

        var order = 0;
        $workBlock.find('.ol-list').each(function () {
            $(this).attr('data-ol', ++order);
        });
    });
    $(".add-field").click(function () {
        if(workId === MAX_FIELD_CAREER_PATH) return;
        ++workId;
        $('<div class="new-ol ol-list" data-ol="0">\n' +
            '    <div class="field-wrap col-md-4 col-sm-12 col-xs-12">\n' +
            '        <label for="company' + workId + '">Tên doanh nghiệp đã và đang công tác</label>\n' +
            '        <input id="company' + workId + '" type="text" maxlength="100" name="workElementNames[' + workId + ']" >\n' +
            '    </div>\n' +
            '    <div class="dropfield field-wrap col-md-3 col-sm-2 col-xs-12">\n' +
            '        <label for="jobStatus' + workId + '">Hình thức lao động</label>\n' +
            '        <select id="jobStatus' + workId + '" name="workTypeOfContracts[' + workId + ']">\n' +
            '            <option value></option>\n' +
            '            <option value="1">Hợp đồng lao động</option>\n' +
            '            <option value="2">Freelancer</option>\n' +
            '        </select>\n' +
            '    </div>\n' +
            '    <div class="col-md-5 col-sm-6 col-xs-12">\n' +
            '        <div class="row">\n' +
            '            <div class="datetime field-wrap col-md-6">\n' +
            '                <label for="jobYearFrom' + workId + '">Thời gian bắt đầu</label>\n' +
            '                <input id="jobYearFrom' + workId + '" class="year datetime-input" type="text" maxlength="4" name="workFromDates[' + workId + ']" placeholder="Từ năm">\n' +
            '                <span class="datetime-icon fa fa-calendar"></span>\n' +
            '            </div>\n' +
            '            <div class="datetime field-wrap col-md-6">\n' +
            '                <label for="jobYearTo' + workId + '">Thời gian kết thúc</label>\n' +
            '                <input id="jobYearTo' + workId + '" class="year datetime-input" type="text" maxlength="4" name="workToDates[' + workId + ']"  placeholder="Đến năm">\n' +
            '                <span class="datetime-icon fa fa-calendar"></span>\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '    <span class="delete-field icon-close"></span>\n' +
            '    <div class="clearfix"></div>\n' +
            '</div>').insertBefore($(this));
        var order = 0;
        $workBlock.find('.ol-list').each(function () {
            $(this).attr('data-ol', ++order);
        });

        $workBlock.find('.ol-list').last().find('.year').datetimepicker({
            locale: 'vi',
            viewMode: 'years',
            format: 'YYYY'
        });
    });

    var optionsForteTour = "";
    if (forteTour != "") {
        $.each(JSON.parse(forteTour), function (key, value) {
            optionsForteTour += '<option value="'+value.id+'">'+value.name+'</option>\n';
        });
    }
    var $increForte = $(".ol-forte-list").last().attr('data-forte-ol');
    var $forteBlock = $('#forte-block'),
        forteId = $increForte - 1;
    $forteBlock.on('click', '.delete-forte-field', function () {
        $(this).parent().remove();

        var order = 0;
        $forteBlock.find('.ol-forte-list').each(function () {
            $(this).attr('data-forte-ol', ++order);
        });
    });

    $(".add-forte-field").click(function () {
        if(forteId === MAX_FIELD_CAREER_PATH) return;
        ++forteId;
        var increForteId = forteId + 1;
        $('<div class="forte-ol ol-forte-list" data-forte-ol="0">\n' +
            '    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">\n' +
            '        <label for="forte' + forteId + '" style="width: auto;">Tuyến du lịch sở trường '+ increForteId +' </label>\n' +
            '        <span class="delete-forte-field icon-close" style="font-size: 16px;color: #2481bf;cursor: pointer;float:right;"></span>\n' +
            '        <div style="clear:both;"></div>\n' +
            '        <select id="forte' + forteId + '" name="forteTour[' + forteId + ']" style="box-shadow:none">\n' +
            '            <option value></option>\n' +
            '            '+optionsForteTour+
            '        </select>\n' +
            '    </div>\n' +
      
     
            '</div>').insertBefore($(this));
        var order = 0;
        $forteBlock.find('.ol-forte-list').each(function () {
            $(this).attr('data-forte-ol', ++order);
        });
    });
    var optionsLanguages = "";
    if (languages != "") {
        $.each(JSON.parse(languages), function (key, value) {
            optionsLanguages += '<option value="'+value.id+'">'+value.languageName+'</option>\n';
        });
    }
    var $increLang = $(".ol-guideLanguage-list").last().attr('data-guidelanguage-ol');
    var $guideLanguageBlock = $('#guideLanguage-block'),
        guideLanguageId = $increLang - 1;
    $guideLanguageBlock.on('click', '.delete-guideLanguage-field', function () {
        $(this).parent().remove();

        var order = 0;
        $guideLanguageBlock.find('.ol-guideLanguage-list').each(function () {
            $(this).attr('data-guideLanguage-ol', ++order);
        });
    });

    $(".add-guideLanguage-field").click(function () {
        console.log(guideLanguageId);
        if(guideLanguageId === MAX_FIELD_CAREER_PATH) return;
        ++guideLanguageId;
        var increGuideLanguageId = guideLanguageId + 1;
        $('<div class="guideLanguage-ol ol-guideLanguage-list" data-guideLanguage-ol="0">\n' +
            '    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">\n' +
            '        <label for="guideLanguage' + guideLanguageId + '" style="width: auto;">Ngôn ngữ hướng dẫn '+ increGuideLanguageId +' </label>\n' +
            '        <span class="delete-guideLanguage-field icon-close" style="font-size: 16px;color: #2481bf;cursor: pointer;float:right;"></span>\n' +
            '        <div style="clear:both;"></div>\n' +
            '        <select id="guideLanguage' + guideLanguageId + '" name="guideLanguage[' + guideLanguageId + ']" style="box-shadow:none">\n' +
            '            <option value></option>\n' +
            ''           +optionsLanguages+
            '        </select>\n' +
            '    </div>\n' +
      
     
            '</div>').insertBefore($(this));
        var order = 0;
        $guideLanguageBlock.find('.ol-guideLanguage-list').each(function () {
            $(this).attr('data-guideLanguage-ol', ++order);
        });
    });

    var $educationBlock = $('#education-block'),
        $educationBlockSpecialized = $educationBlock.find('#specialized');
    $educationBlockSpecialized.change(function () {
        $educationBlock.find('#level').val("");
    });

    var $majorBlock = $('#major-block'),
        $majorBlockUploadList = $majorBlock.find('.upload-list'),
        majorFileId = 1;
    $majorBlockUploadList.on('change', '[type="file"]', function () {
        if ($(this).attr('id') === 'majorFile' + majorFileId) {
            ++majorFileId;
            $majorBlockUploadList.append('<div class="btn-upload" data-auto-remove="1">\n' +
                '    <label class="fa fa-upload" for="majorFile' + majorFileId + '"> Tải lên bản sao có chứng thực văn bằng hoặc giấy chứng nhận</label>\n' +
                '    <input id="majorFile' + majorFileId + '" type="file" name="majorFiles[]" accept="image/*">\n' +
                '</div>')
        }
    });

    var $languageSkillBlock = $('#language-skill-block'),
        $languageSkillBlockChoices = $languageSkillBlock.find('.radio-list .choice'),
        $languageSkillBlockList = $languageSkillBlock.find('.list'),
        $languageSkillBlockLanguage = $languageSkillBlock.find('#language'),
        languageSkillId = 0;
    $languageSkillBlockLanguage.change(function () {
        $languageSkillBlockChoices.removeClass('chosen');
    });
    $languageSkillBlock.on('click', '.delete-field', function () {
        $(this).parent().remove();

        var order = 0;
        $languageSkillBlock.find('.list-order').each(function () {
            $(this).text(++order);
        });
    });
    $languageSkillBlockChoices.click(function (e) {
        var $chosen = $languageSkillBlockChoices.filter('.chosen');
        var langId = $languageSkillBlockLanguage.val(),
            levelId = $chosen.attr('data-id');
        if (langId && levelId && $('#lang-skill-' + langId + '-' + levelId).length <= 0) {
            var langName = $languageSkillBlockLanguage.find('[value="' + langId + '"]').text(),
                levelName = $chosen.text();
            ++languageSkillId;
            /*$languageSkillBlockList.append('<li id="lang-skill-' + langId + '-' + levelId + '">\n' +
                '   <span class="delete-field icon-close"></span>\n' +
                '   <span class="list-order"></span>. ' + langName + ' - ' + levelName + '\n' +
                '   <span class="btn-upload">\n' +
                '       <label class="fa fa-upload" for="languageFile' + languageSkillId + '"> Tải lên bản sao có chứng thực văn bằng</label>\n' +
                '       <input id="languageFile' + languageSkillId + '" type="file" name="languageFiles[' + languageSkillId + ']" accept="image/!*">\n' +
                '   </span>\n' +
                '   <input type="hidden" name="languages[' + languageSkillId + ']" value="' + langId + '">\n' +
                '   <input type="hidden" name="languageLevels[' + languageSkillId + ']" value="' + levelId + '">\n' +
                '</li>');*/

            $languageSkillBlockList.append('<li id="lang-skill-' + langId + '-' + levelId + '">\n' +
                '   <span class="delete-field icon-close"></span>\n' +
                '   <span class="list-order"></span>. ' + langName + ' - ' + levelName + '\n' +
                '   <input type="hidden" name="languages[' + languageSkillId + ']" value="' + langId + '">\n' +
                '   <input type="hidden" name="languageLevels[' + languageSkillId + ']" value="' + levelId + '">\n' +
                '</li>');

            var order = 0;
            $languageSkillBlockList.find('.list-order').each(function () {
                $(this).text(++order);
            });
        }
    });

    var $achievementBlock = $('#achievement-block'),
        $achievementBlockUploadList = $achievementBlock.find('.upload-list'),
        achievementFileId = 1;
    $achievementBlockUploadList.on('change', '[type="file"]', function () {
        if ($(this).attr('id') === 'achievement' + achievementFileId) {
            ++achievementFileId;
            $achievementBlockUploadList.append('<div class="btn-upload" data-auto-remove="1">\n' +
                '    <label class="fa fa-upload" for="achievement' + achievementFileId + '"> Tải lên các thành tích</label>\n' +
                '    <input id="achievement' + achievementFileId + '" type="file" name="achievementFiles[]" accept="image/*">\n' +
                '</div>')
        }
    });

    //step
    $(".next-step").click(function() {
        $("form[name=user-info]").submit();
    });
    $(".prev-step").click(function(){
        window.history.back();
    });

	$.validator.addMethod('totalCheckLanguage', function(value, element, params) {
		var typeofTravel = $(".chosen input[name=typeOfTravelGuide]").val(),
			language = $("#language").val(),
			languageLevel = $("#level2").val();
		return (typeofTravel == "1" && value == "")? false : true;
	}, "Chuyên ngành ngoại ngữ là trường bắt buộc đối với HDV du lịch quốc tế");

	$.validator.addMethod('totalCheckLanguageLevel', function(value, element, params) {
		var typeofTravel = $(".chosen input[name=typeOfTravelGuide]").val(),
			language = $("#language").val(),
			languageLevel = $("#level2").val();
		return (typeofTravel == "1" && value == "")? false : true;
	}, "Trình độ ngoại ngữ là trường bắt buộc đối với HDV du lịch quốc tế");

	$.validator.addMethod('checkWorkAt', function() {
        var typeOfPlace = $(".chosen input[name=typeOfPlace]").val();
        var province_code = $("#province_code").val();
        var club = $("#club").val();
        if(typeOfPlace == undefined){
            return false;
        }
        if(typeOfPlace == "1"){
            return province_code ? true : false;
        } else if(typeOfPlace == "2"){

            return club ? true : false;
        }
        // return (typeOfPlace != "" && (province_code != undefined || club != undefined )) ? true : false;
    }
    , "Đăng ký sinh hoạt là trường bắt buộc");

    //validation
    $("form[name=user-info]").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input        
        rules: {
            fullName: {
                required: true
            },
            avatar : {
              required: true
            },
            gender: {
                required: true,
                valueNotEquals: ""
            },
            birthDate: {
                required: true
            },
            birthMonth: {
                required: true
            },
            birthYear: {
                required: true
            },
            touristGuideCode: {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 9
            },
            expirationDate: {
                required: true
            },
            cmtCccd: {
                required: true
            },
            dateIssued: {
                required: true
            },
            issuedBy: {
                required: true
            },
            permanentAddress: {
                required: true
            },
            address: {
                required: true
            },
            firstMobile: {
                required: true,
                digits: true
            },
            firstEmail: {
                required: true,
                email: true
            },
            emailConfirm: {
                required: true,
                email: true,
                equalTo: "#email1"
            },
            emailToken: {
                required: true
            },
            phoneToken: {
                required: true
            },
            accept: {
                required: true
            },
            typeOfTravelGuide: {
                required: true
            },
            typeOfPlace: {
                required: true,
                checkWorkAt: true
            },
            educationBranches: {
                required: true,
                valueNotEquals: ""
            },
            educationDegrees: {
                required: true,
                valueNotEquals: ""
            },
            majors: {
                required: true
            },
            language: {
                //required: true
				totalCheckLanguage: ['typeOfTravelGuide']
            },
            level2: {
                //required: true
				totalCheckLanguageLevel: ['typeOfTravelGuide']
            },
			typeGuides: {
                required: true,
                valueNotEquals: ""
            },
			groupSizes: {
                required: true,
                valueNotEquals: ""
            },
			"workElementNames[1]": {
                required: true
            },
            "workTypeOfContracts[1]": {
                required: true
            },
            "workFromDates[1]": {
                required: true
            },
            "workToDates[1]": {
                required: true
            },
            "forteTour[1]": {
                required: true
            },
            /*type: {
                required: true
            },
            strength: {
                required: true
            },*/
            experienceYear: {
                required: true
            },
            experienceLevel: {
                required: true
            },
            guideLanguage: {
                required: true
            },
            inboundOutbound : {
                required: true
            },
            province: {
                required: true
            },
        },

        messages: {
            fullName: {
                required: "Họ và tên là trường bắt buộc"
            },
            avatar: {
                required: "Ảnh đại diện là trường bắt buộc"
            },
            gender: {
                required: "Giới tính là trường bắt buộc",
                valueNotEquals: "Giới tính là trường bắt buộc"
            },
            birthDate: {
                required: "Ngày sinh là trường bắt buộc"
            },
            birthMonth: {
                required: "Tháng sinh là trường bắt buộc"
            },
            birthYear: {
                required: "Năm sinh là trường bắt buộc"
            },
            touristGuideCode: {
                number: "Số thẻ HDV du lịch phải là số",
                minlength: "Số thẻ HDV du lịch phải là số có 9 chữ số",
                maxlength: "Số thẻ HDV du lịch phải là số có 9 chữ số",
                required: "Số thẻ HDV du lịch là trường bắt buộc"
            },
            expirationDate: {
                required: "Ngày hết hạn là trường bắt buộc phải điền"
            },
            cmtCccd: {
                required: "Số CMT/CCCD  là trường bắt buộc"
            },
            dateIssued: {
                required: "Ngày cấp CMT/CCCD là trường bắt buộc"
            },
            issuedBy: {
                required: "Nơi cấp ngày CMT/CCCD là trường bắt buộc"
            },
            permanentAddress: {
                required: "Địa chỉ thường trú là trường bắt buộc"
            },
            address: {
                required: "Địa chỉ liên hệ là trường bắt buộc"
            },
            firstMobile: {
                required: "Điện thoại di động là trường bắt buộc",
                digits: "Số điện thoại phải là số"
            },
            firstEmail: {
                required: "Email là trường bắt buộc",
                email: "Định dạng email không hợp lệ"
            },
            emailConfirm: {
                required: "Trường bắt buộc",
                email: "Định dạng email không hợp lệ",
                equalTo: "Email xác nhận không khớp"
            },
            emailToken: {
                required: "Vui lòng nhập mã xác thực email"
            },
            phoneToken: {
                required: "Vui lòng nhập Mã xác thực điện thoại"
            },
            accept: {
                required: "Bạn phải đồng ý với quy chế hoạt động và quy định của Hội hướng dẫn viên du lịch Việt Nam"
            },
            typeOfTravelGuide: {
                required: "Hướng dẫn viên là trường bắt buộc phải điền"
            },
            typeOfPlace: {
                required: "Đăng ký sinh hoạt tại là trường bắt buộc",
                checkWorkAt: "Đăng ký sinh hoạt tại là trường bắt buộc"
            },
            educationBranches: {
                required: "Chuyên ngành là trường bắt buộc"
            },
            educationDegrees: {
                required: "Trình độ là trường bắt buộc"
            },
            majors: {
                required: "Thông tin về nghiệp vụ hướng dẫn là trường bắt buộc"
            },
            language: {
                required: "Chuyên ngành là trường bắt buộc"
            },
            level2: {
                required: "Trình độ là trường bắt buộc"
            },
			typeGuides: {
                required: "Loại hình là trường bắt buộc"
            },
			groupSizes: {
                required: "Quy mô đoàn là trường bắt buộc"
            },
            "workElementNames[0]": {
                required: "Tên doanh nghiệp đã và đang công tác là trường bắt buộc"
            },
            "forteTour[0]": {
                required: "Tuyến du lịch là trường bắt buộc"
            },
            "workTypeOfContracts[0]": {
                required: "Hình thức lao động là trường bắt buộc"
            },
            "workFromDates[0]": {
                required: "Thời gian bắt thúc là trường bắt buộc"
            },
            "workToDates[0]": {
                required: "Thời gian kết thúc là trường bắt buộc"
            },
            /*type: {
                required: "Loại hình là trường bắt buộc"
            },
            strength: {
                required: "Quy mô đoàn là trường bắt buộc"
            },*/
            experienceYear: {
                required: "Năm bắt đầu hướng dẫn là trường bắt buộc"
            },
            experienceLevel: {
                required: "Số năm kinh nghiệm là trường bắt buộc"
            },
            guideLanguage: {
                required: "Ngôn ngữ hướng dẫn là trường bắt buộc"
            },
            inboundOutbound: {
                required: "Bạn phải chọn Inbound / Outbound đối với HDV du lịch quốc tế"
            },
            province: {
                required: "Thông tin tỉnh thành là trường bắt buộc"
            },
        },
        highlight: function(element) {
            if(element.name == "typeOfPlace") {
                var typeOfPlace = $(".chosen input[name=typeOfPlace]").val();
                if (typeOfPlace == "2") {
                    $("#club").closest('.field-wrap').addClass('has-error');
                    $("#club").css('box-shadow', '0 0 10px #ce3f38');
                    $("#province_code").closest('.field-wrap').removeClass('has-error');
                    $("#province_code").css('box-shadow', 'none');
                } else {
                    $("#province_code").closest('.field-wrap').addClass('has-error');
                    $("#province_code").css('box-shadow', '0 0 10px #ce3f38');
                    $("#club").closest('.field-wrap').removeClass('has-error');
                    $("#club").css('box-shadow', 'none');
                }
            } else if(element.name == 'avatar'){
                $(element).closest('.fileinput-button').addClass('has-error');
            } else {
                $(element).closest('.field-wrap').addClass('has-error');
                $(element).css('box-shadow', '0 0 10px #ce3f38');
            }
        },
        unhighlight: function(element) {
            $(element).closest('.field-wrap').removeClass('has-error');
            $(element).css('box-shadow', 'none');
            $(element).closest('.field-wrap, .btn-upload').find(".help-block").remove();
            $(element).closest('.fileinput-button').removeClass('has-error');
        },
        invalidHandler: function() {
            if ($('#confirmModal').length ) {
                $('#confirmModal').modal('hide');
            }
            $.each($(this).find(".field-wrap.has-error"), function (idx, val) {
                if (!$(val).closest('.block-content').hasClass('showout')) $(val).closest('.block-content').addClass('showout');
            });
            $(this).find(".field-wrap.has-error:first").children(2).focus();
        },
        errorPlacement: function(error, element) {
            element.closest(".field-wrap, .btn-upload, .field-col, .radio-list").append(error);
        },
        submitHandler: function(form) {
            if (form.id === 'update-user-profile') {
                $('#confirmModal').modal('show');
            }
            else if (form.id === 'update-admin-user-profile') {
               
            } else {
                form.submit();
            }
        }
    });

	$.validator.addMethod("valueNotEquals", function(value, element, arg){
		return arg !== value;
	}, "Trường bắt buộc");

	$('#fileupload').on('change', function (e) {
        if($(this).val()) {
            $(this).closest('.fileinput-button').removeClass('has-error');
            $(document).find('#avatar-error').addClass('hidden');
        }
    })
});
