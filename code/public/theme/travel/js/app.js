function convertHex(a, c) {
    return a = a.replace("#", ""), r = parseInt(a.substring(0, 2), 16), g = parseInt(a.substring(2, 4), 16), b = parseInt(a.substring(4, 6), 16), result = "rgba(" + r + ", " + g + ", " + b + ", " + c + ")", result
} ! function (a) {
    "use strict";

    function e() {
        a("*[data-pattern-overlay-darkness-opacity]").each(function () {
            var b = a(this).data("pattern-overlay-darkness-opacity");
            a(this).css("background-color", convertHex("#000000", b))
        }), a("*[data-pattern-overlay-background-image]").each(function () {
            "none" == a(this).data("pattern-overlay-background-image") ? a(this).css("background-image", "none") : "yes" == a(this).data("pattern-overlay-background-image") && a(this).css("background-image")
        }), a("*[data-remove-pattern-overlay]").each(function () {
            "yes" == a(this).data("remove-pattern-overlay") && a(this).css("background", "none")
        }), a("*[data-bg-color]").each(function () {
            var b = a(this).data("bg-color");
            a(this).css("background-color", b)
        }), a("*[data-bg-color-opacity]").each(function () {
            var b = a(this).data("bg-color-opacity"),
                c = a(this).css("background-color"),
                d = c.replace("rgb", "rgba").replace(")", ", " + b + ")");
            a(this).css("background-color", d)
        }), a("*[data-bg-img]").each(function () {
            var b = a(this).data("bg-img");
            a(this).css("background-image", "url('" + b + "')")
        }), a("*[data-parallax-bg-img]").each(function () {
            var b = a(this).data("parallax-bg-img");
            a(this).css({
				"background-image": "url('../images/banners/" + b + "')",
				"background-position": "center center"
			});
        })
    }

    function f() {
        var a = jRespond([{
            label: "smallest",
            enter: 0,
            exit: 479
        }, {
            label: "handheld",
            enter: 480,
            exit: 767
        }, {
            label: "tablet",
            enter: 768,
            exit: 991
        }, {
            label: "laptop",
            enter: 992,
            exit: 1199
        }, {
            label: "desktop",
            enter: 1200,
            exit: 1e4
        }]);
        a.addFunc([{
            breakpoint: "desktop",
            enter: function () {
                d.addClass("device-lg")
            },
            exit: function () {
                d.removeClass("device-lg")
            }
        }, {
            breakpoint: "laptop",
            enter: function () {
                d.addClass("device-md")
            },
            exit: function () {
                d.removeClass("device-md")
            }
        }, {
            breakpoint: "tablet",
            enter: function () {
                d.addClass("device-sm")
            },
            exit: function () {
                d.removeClass("device-sm")
            }
        }, {
            breakpoint: "handheld",
            enter: function () {
                d.addClass("device-xs")
            },
            exit: function () {
                d.removeClass("device-xs")
            }
        }, {
            breakpoint: "smallest",
            enter: function () {
                d.addClass("device-xxs")
            },
            exit: function () {
                d.removeClass("device-xxs")
            }
        }])
    }

    function i() {
        a("#website-loading").find(".loader").delay(0).fadeOut(500), a("#website-loading").delay(600).fadeOut(300)
    }

    function j() {
        a("#full-container").fitVids()
    }

    function k() {
        a(".img-bg").each(function () {
            var b = a(this).find("img").attr("src");
            a(this).css("background-image", "url('" + b + "')"), a(this).find("img").css({
                opacity: 0,
                visibility: "hidden"
            })
        })
    }

    function l() {
        a(".fullscreen").css("height", a(window).height())
    }

    function n() {
        a(window).scrollTop();
        a(window).scrollTop() > 800 ? a(".scroll-top").addClass("show") : a(".scroll-top").removeClass("show")
    }

    function o() {
        a(function () {
            (d.hasClass("device-lg") || d.hasClass("device-md") || d.hasClass("device-sm")) && a.stellar({
                horizontalScrolling: !1,
                verticalOffset: -250,
                responsive: !0
            })
        })
    }

    function p() {
        a("#main-menu").clone().appendTo("#mobile-menu").removeClass().addClass("mobile-menu");
        var b = document.querySelector("#mobile-menu");
        SimpleScrollbar.initEl(b), a(".mobile-menu").superfish({
            popUpSelector: "ul, .megamenu",
            cssArrows: !0,
            delay: 300,
            speed: 150,
            speedOut: 150,
            animation: {
                opacity: "show",
                height: "show"
            },
            animationOut: {
                opacity: "hide",
                height: "hide"
            }
        }), a(".mobile-menu-btn").on("click", function (b) {
            b.preventDefault(), a(this).toggleClass("is-active"), a("#mobile-menu").slideToggle(250)
        })
    }

    function q() {
        a(".main-menu").superfish({
            popUpSelector: "ul",
            cssArrows: !0,
            delay: 300,
            speed: 100,
            speedOut: 100,
            animation: {
                opacity: "show"
            },
            animationOut: {
                opacity: "hide"
            }
        })
    }

    function r() {
        a.scrollIt({
            upKey: !1,
            downKey: !1,
            scrollTime: 0,
            activeClass: "current",
            onPageChange: null,
            topOffset: -90
        }), a("#main-menu li a").on("click", function (b) {
            var c = a(this);
            a("html, body").stop().animate({
                scrollTop: a(c.attr("href")).offsetTop - 90
            }, 1200, "easeInOutExpo");
            var e = d.hasClass("device-sm") || d.hasClass("device-xs") || d.hasClass("device-xxs");
            e && (a("#mobile-menu").slideUp(250), a(".mobile-menu-btn").removeClass("is-active"))
        })
    }

    function t() {
        var b = a(".banner-slider > .owl-carousel");
        b.owlCarousel({
            items: 1,
            rtl: h,
            autoplay: 1,
            autoplaySpeed: 600,
            autoplayTimeout: 3e3,
            dragEndSpeed: 500,
            autoplayHoverPause: !0,
            loop: !0,
            slideBy: 1,
            margin: 10,
            stagePadding: 0,
            nav: !1,
            dots: !0,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            responsive: {
                0: {},
                480: {},
                768: {}
            },
            autoHeight: !1,
            autoWidth: !1,
            navRewind: !0,
            center: !1,
            dotsEach: 1,
            dotData: !1,
            lazyLoad: !1,
            smartSpeed: 600,
            fluidSpeed: 5e3,
            navSpeed: 500,
            dotsSpeed: 500
        }), b.on("translated.owl.carousel", function (a) {
            var c = b.find(".owl-item.active .banner-center-box"),
                d = parseInt(c.children("h1, .description, .btn").css("top"), 10);
            0 != d && (console.log("It's the next...."), console.log("Same Slide!"), setTimeout(function () {
                setTimeout(function () {
                    c.children("h1").css("top", 100).animate({
                        opacity: 1
                    }, {
                            duration: 400,
                            queue: !1
                        }).animate({
                            top: 0
                        }, {
                            duration: 600,
                            easing: "easeOutExpo"
                        })
                }, 0), setTimeout(function () {
                    c.children(".description").css("top", 100).animate({
                        opacity: 1
                    }, {
                            duration: 400,
                            queue: !1
                        }).animate({
                            top: 0
                        }, {
                            duration: 600,
                            easing: "easeOutExpo"
                        })
                }, 100), setTimeout(function () {
                    c.children(".btn").css("top", 100).animate({
                        opacity: 1
                    }, {
                            duration: 400,
                            queue: !1
                        }).animate({
                            top: 0
                        }, {
                            duration: 600,
                            easing: "easeOutExpo"
                        })
                }, 200)
            }, 150))
        }), b.on("drag.owl.carousel", function (a) {
            b.find(".owl-item:not( .active )").find(".banner-center-box > *").animate({
                opacity: 0
            }, 150).css("top", 1)
        }), b.on("changed.owl.carousel", function (a) {
            b.find(".banner-center-box > *").animate({
                opacity: 0
            }, 150).css("top", 1)
        }), b.on("resized.owl.carousel", function (a) {
            b.find(".banner-center-box > *").animate({
                opacity: 1
            }, 150)
        })
    }

    function u() {
        var b = a(window).scrollTop(),
            c = a(".banner-slider").height() + a("#header").height();
        (d.hasClass("device-lg") || d.hasClass("device-md") || d.hasClass("device-sm")) && (b < c && a(".banner-slider").css({
            transform: "translateY(" + .4 * b + "px)"
        }), b < c && a(".slide-content").css({
            opacity: 1 - (b - 120) / 400
        }))
    }

    function w() {
        a(".slider-img-bg .owl-item > li").each(function () {
            var b = a(this).find(".slide").children("img").attr("src");
            a(this).css({
                "background-image": "url('" + b + "')",
                "background-color": "#ccc",
                "background-position": "center center",
                "background-size": "cover",
                "background-repeat": "no-repeat"
            })
        })
    }

    function x() {
        a(".slider-img-bg").each(function () {
            var b = a(this).closest("div").height();
            a(".banner-parallax").children(".banner-slider").length > 0 && a(".banner-parallax").height(a(".banner-slider").height()), a(this).find(".owl-item > li .slide").children("img").css({
                display: "block",
                height: b,
                opacity: 0
				
            })
        })
    }

    function I() {
        var b = a(".info-slider > .owl-carousel");
        b.owlCarousel({
            rtl: h,
            autoplay: 1,
            autoplaySpeed: 600,
            autoplayTimeout: 3e3,
            dragEndSpeed: 500,
            autoplayHoverPause: !0,
            loop: 1,
            slideBy: 1,
            margin: 30,
            stagePadding: 0,
            nav: !1,
            dots: !0,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            },
            autoHeight: !1,
            autoWidth: !1,
            navRewind: !0,
            center: !1,
            dotsEach: 1,
            dotData: !1,
            lazyLoad: !1,
            smartSpeed: 600,
            fluidSpeed: 5e3,
            navSpeed: 500,
            dotsSpeed: 500
        })
    }
	
	function J() {
        var b = a(".popular-packages-slider > .owl-carousel");
        b.owlCarousel({
            rtl: h,
            autoplay: 1,
            autoplaySpeed: 600,
            autoplayTimeout: 3e3,
            dragEndSpeed: 500,
            autoplayHoverPause: !0,
            loop: 1,
            slideBy: 1,
            margin: 30,
            stagePadding: 0,
            nav: !1,
            dots: !0,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            },
            autoHeight: !1,
            autoWidth: !1,
            navRewind: !0,
            center: !1,
            dotsEach: 1,
            dotData: !1,
            lazyLoad: !1,
            smartSpeed: 600,
            fluidSpeed: 5e3,
            navSpeed: 500,
            dotsSpeed: 500
        })
    }
	
	function I() {
        var b = a(".tour-guide-slider > .owl-carousel");
        b.owlCarousel({
            rtl: h,
            autoplay: 1,
            autoplaySpeed: 600,
            autoplayTimeout: 3e3,
            dragEndSpeed: 500,
            autoplayHoverPause: !0,
            loop: 1,
            slideBy: 1,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 1
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            },
            autoHeight: !1,
            autoWidth: !1,
            navRewind: !0,
            center: !1,
            dotsEach: 1,
            dotData: !1,
            lazyLoad: !1,
            smartSpeed: 600,
            fluidSpeed: 5e3,
            navSpeed: 500,
            dotsSpeed: 500
        })
    }

    a(document).on("ready", function () {
        f(), e(), j(), l(), u(), k(), p(), q(), r(), t(), I(), J(), w(), x()
    }), a(window).on("load", function () {
        i(), w(), o()
    }), a(window).on("resize", function () {
        x(), f(), l(), o()
    }), a(window).on("scroll", function () {
        u(), n()
    });
    var h, a = jQuery.noConflict(),
        d = (a(window), a(this), a("body")),
        g = a("html").css("direction");
    h = "rtl" == g, a(".scroll-top, .scroll-top-btn").click(function (b) {
        b.preventDefault(), console.log("button clicked...."), a("html, body").animate({
            scrollTop: 0
        }, 700)
    });
}(jQuery);