 function ajaxSearchOptionCode(urlRoute, itemVal, selector, itemSelected = null) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: urlRoute,
        cache: "false",
        dataType: "json",
        data: {
            id: itemVal 
        },
        success: function(response) {
            var items = response.data;
            $.each(items, function (i, item) {
                if (i == itemSelected) {
                    selector.append($('<option>', { 
                        value: i,
                        text : item,
                        selected: true 
                    }));
                }
                else {
                    selector.append($('<option>', { 
                        value: i,
                        text : item 
                    }));
                }
            });
        }
    });
}