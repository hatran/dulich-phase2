function convertHex(a, c) {
    return a = a.replace("#", ""), r = parseInt(a.substring(0, 2), 16), g = parseInt(a.substring(2, 4), 16), b = parseInt(a.substring(4, 6), 16), result = "rgba(" + r + ", " + g + ", " + b + ", " + c + ")", result
} ! function (a) {
    "use strict";

    function e() {
        a("*[data-pattern-overlay-darkness-opacity]").each(function () {
            var b = a(this).data("pattern-overlay-darkness-opacity");
            a(this).css("background-color", convertHex("#000000", b))
        }), a("*[data-pattern-overlay-background-image]").each(function () {
            "none" == a(this).data("pattern-overlay-background-image") ? a(this).css("background-image", "none") : "yes" == a(this).data("pattern-overlay-background-image") && a(this).css("background-image")
        }), a("*[data-remove-pattern-overlay]").each(function () {
            "yes" == a(this).data("remove-pattern-overlay") && a(this).css("background", "none")
        }), a("*[data-bg-color]").each(function () {
            var b = a(this).data("bg-color");
            a(this).css("background-color", b)
        }), a("*[data-bg-color-opacity]").each(function () {
            var b = a(this).data("bg-color-opacity"),
                c = a(this).css("background-color"),
                d = c.replace("rgb", "rgba").replace(")", ", " + b + ")");
            a(this).css("background-color", d)
        }), a("*[data-bg-img]").each(function () {
            var b = a(this).data("bg-img");
            a(this).css("background-image", "url('" + b + "')")
        }), a("*[data-parallax-bg-img]").each(function () {
            var b = a(this).data("parallax-bg-img");
            a(this).css({
                "background-image": "url('../images/banners/" + b + "')",
                "background-position": "center center"
            });
        })
    }

    function f() {
        var a = jRespond([{
            label: "smallest",
            enter: 0,
            exit: 479
        }, {
            label: "handheld",
            enter: 480,
            exit: 767
        }, {
            label: "tablet",
            enter: 768,
            exit: 991
        }, {
            label: "laptop",
            enter: 992,
            exit: 1199
        }, {
            label: "desktop",
            enter: 1200,
            exit: 1e4
        }]);
        a.addFunc([{
            breakpoint: "desktop",
            enter: function () {
                d.addClass("device-lg")
            },
            exit: function () {
                d.removeClass("device-lg")
            }
        }, {
            breakpoint: "laptop",
            enter: function () {
                d.addClass("device-md")
            },
            exit: function () {
                d.removeClass("device-md")
            }
        }, {
            breakpoint: "tablet",
            enter: function () {
                d.addClass("device-sm")
            },
            exit: function () {
                d.removeClass("device-sm")
            }
        }, {
            breakpoint: "handheld",
            enter: function () {
                d.addClass("device-xs")
            },
            exit: function () {
                d.removeClass("device-xs")
            }
        }, {
            breakpoint: "smallest",
            enter: function () {
                d.addClass("device-xxs")
            },
            exit: function () {
                d.removeClass("device-xxs")
            }
        }])
    }

    function i() {
        a("#website-loading").find(".loader").delay(0).fadeOut(500), a("#website-loading").delay(600).fadeOut(300)
    }

    function j() {
        a("#full-container").fitVids()
    }

    function k() {
        a(".img-bg").each(function () {
            var b = a(this).find("img").attr("src");
            a(this).css("background-image", "url('" + b + "')"), a(this).find("img").css({
                opacity: 0,
                visibility: "hidden"
            })
        })
    }

    function l() {
        a(".fullscreen").css("height", a(window).height())
    }

    function n() {
        a(window).scrollTop();
        a(window).scrollTop() > 800 ? a(".scroll-top").addClass("show") : a(".scroll-top").removeClass("show")
    }

    function o() {
        a(function () {
            (d.hasClass("device-lg") || d.hasClass("device-md") || d.hasClass("device-sm")) && a.stellar({
                horizontalScrolling: !1,
                verticalOffset: -250,
                responsive: !0
            })
        })
    }

    function p() {
        var flag = true;
        a("#main-menu").clone().appendTo("#mobile-menu").removeClass().addClass("mobile-menu");
        var b = document.querySelector("#mobile-menu");
        SimpleScrollbar.initEl(b), a(".mobile-menu").superfish({
            popUpSelector: "ul, .megamenu",
            cssArrows: !0,
            delay: 300,
            speed: 150,
            speedOut: 150,
            animation: {
                opacity: "show",
                height: "show"
            },
            animationOut: {
                opacity: "hide",
                height: "hide"
            }
        }), a(".mobile-menu-btn").on("click", function (b) {
            b.preventDefault(), a(this).toggleClass("is-active"), a("#mobile-menu").slideToggle(250)
        });
    }

    function q() {
        a(".main-menu").superfish({
            popUpSelector: "ul",
            cssArrows: !0,
            delay: 300,
            speed: 100,
            speedOut: 100,
            animation: {
                opacity: "show"
            },
            animationOut: {
                opacity: "hide"
            }
        })
    }

    function r() {
        a.scrollIt({
            upKey: !1,
            downKey: !1,
            scrollTime: 0,
            activeClass: "current",
            onPageChange: null,
            topOffset: -90
        }), a("#main-menu li a").on("click", function (b) {
            var c = a(this);
            a("html, body").stop().animate({
                scrollTop: a(c.attr("href")).offsetTop - 90
            }, 1200, "easeInOutExpo");
            var e = d.hasClass("device-sm") || d.hasClass("device-xs") || d.hasClass("device-xxs");
            e && (a("#mobile-menu").slideUp(250), a(".mobile-menu-btn").removeClass("is-active"))
        })
    }

   

    a(document).on("ready", function () {
        f(), e(), j(), l(), k(), p(), q(), r()
    }), a(window).on("load", function () {
        i(), o()
    }), a(window).on("resize", function () {
        f(), l(), o()
    }), a(window).on("scroll", function () {
        n()
    });
    var h, a = jQuery.noConflict(),
        d = (a(window), a(this), a("body")),
        g = a("html").css("direction");
    h = "rtl" == g, a(".scroll-top, .scroll-top-btn").click(function (b) {
        b.preventDefault(), console.log("button clicked...."), a("html, body").animate({
            scrollTop: 0
        }, 700)
    });
}(jQuery);