jQuery('body').on('click', '.paginate-clb .pagination a', function() {
    ajaxContent(jQuery(this).attr('href'), ".ajax-bld");

    return false;
}).on('click', '.row-member-list .pagination a', function() {
    ajaxContent(jQuery(this).attr('href'), ".row-member-list");

    return false;
}).on('click', '.news-events .pagination a', function() {
    ajaxContent(jQuery(this).attr('href'), ".news-events");

    return false;
}).on('submit', 'form[name="filter-member"]', function(e) {
    e.preventDefault();
    jQuery('.error-search-member').html('').addClass('hidden');
    var dataForm = jQuery(this).serialize();
    jQuery.ajax({
        headers: {
            'X-CSRF-Token': jQuery('meta[name="csrf-token"]').attr('content')
        },
        url: jQuery(this).attr('action'),
        type: "POST",
        datatype: "json",
        data: dataForm
    }).done(function(data)
    {
        if (data.error == true) {
            jQuery('.error-search-member').html(data.messages.captcha2[0]).removeClass('hidden');
        } else {
            jQuery('.row-member-list').empty().html(data.html);
            if (jQuery("#branchInfo").length) {
                jQuery("#branchInfo").attr("onclick", "tabBranchInfo()");
            }
        }
        jQuery('.captcha_hoivien > img').replaceWith('<img id="captcha" class="captcha" src="/captcha/flat?'+Math.random()+'" />');
        jQuery('#captcha2').val('');
        jQuery('#captcha').val('');
    });
}).on('submit', 'form[name="filter"]', function(e) {
	jQuery(this).find('.text-danger').remove();
    if(jQuery('#clb_name').val() == '') {
        var nameMess = jQuery('#clb_name').attr('name') == 'clb_name' ? 'Tên CLB' : 'Tên Chi Hội';
        jQuery('#clb_name').parent().append('<div class="text-danger">'+nameMess+' không được bỏ trống.</div>');
        return false;
    } else if (jQuery(this).find('input[name="captcha"]').val() == '') {
        jQuery(this).find('input[name="captcha"]').parent().append('<div class="text-danger">Mã xác nhận không được bỏ trống.</div>');
        return false;
    } else {
        jQuery(this).submit();
    }
    return false;
}).on('click', '.btn-back-custom', function () {
    var linkBack = jQuery(this).attr('href');
    ajaxContent(linkBack, jQuery('#content-tab2'));
    return false;
});

var tabBranchInfo = function () {
    var branchName = getURLParameter("branch_name");
    jQuery('.error-search-member').html('').addClass('hidden');
    var urli = "/ajax/ajax-get-member-ext/"+branchName+"/branch";
    var dataForm = jQuery("#content-tab1").find(jQuery('form[name="filter-member"]')).serialize();
    jQuery.ajax({
        headers: {
            'X-CSRF-Token': jQuery('meta[name="csrf-token"]').attr('content')
        },
        url: urli,
        type: "POST",
        datatype: "json",
        data: dataForm
    }).done(function(data)
    {
        console.log(data);
        if (data.error == true) {
            jQuery('.error-search-member').html(data.messages.captcha2[0]).removeClass('hidden');
        } else {
            jQuery('.row-member-list').empty().html(data.html);
        }
        jQuery('.captcha_hoivien > img').replaceWith('<img id="captcha" class="captcha" src="/captcha/flat?'+Math.random()+'" />');
        jQuery('#captcha2').val('');
        jQuery('#captcha').val('');
    });
}

function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++){
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}


var ajaxContent = function(url, content, options)
{
    jQuery.ajax({
        url: url,
        type: "GET",
        datatype: "html",
        data: options
    }).done(function(data)
    {
        jQuery(content).empty().html(data.html);
    });
};
function tabContentAjax(_event) {
    var now_tab = _event.target // activated tab
    var urlAjax = jQuery(now_tab).attr('data-link');
    var divid = jQuery(now_tab).attr('href').substr(1);
    ajaxContent(urlAjax, jQuery("#"+divid));
}
function getBrancheInfoById(__this) {
    var __idBr = __this.val();
    if (__idBr == '') {
        jQuery('.clb-group-content').html('');
    } else {
        var __url = baseBranchesAjax + __idBr;
        ajaxContent(__url, '.clb-group-content');
    }
}
function ajaxNewsDetail(_this) {
    var linkDetail = _this.attr('data-href');
    ajaxContent(linkDetail, jQuery("#content-tab2"));
}
function reloadCaptcha(__target) {
    jQuery('.fs20').attr("style", "margin-top: 33px !important");
    __target.attr("src", "/captcha/flat?"+Math.random());
}
function reloadCaptcha2(__target) {
    jQuery('.fs20').attr("style", "margin-top: 33px !important");
    __target.parent().parent().find('#captcha').attr("src", "/captcha/flat?"+Math.random());
}