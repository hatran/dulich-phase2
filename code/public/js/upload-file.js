var UploadFile = (function () {
    var fileUpload = $('#fileupload');
    var avatar = $('#avatar');
    var fileUploaded = $('#fileuploaded');

    function init() {
        fileUpload.fileupload({
            url: window.location.origin + '/upload',
            type: 'POST',
            dataType: 'json',
            paramName: 'file',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i, // Only allow upload image file. (File type is gif, jpeg, png...)
            limitMultiFileUploads: 1,
            maxFileSize : 10000000, // Maximum acceptable file is 10M
            done: function (e, data) {
                if (data.result.success !== 'undefined') {
                    var imageUploadedUrl = window.location.origin + '/' + data.result.success;
					console.log(imageUploadedUrl);
                    if(imageUploadedUrl.length){
                        avatar.val(imageUploadedUrl);
                        fileUploaded.text(imageUploadedUrl.split('/').pop());
                    }
                }
            },
            fail: function (e, data) {
                return toastr.error('Không thể thêm ảnh đại diện.', 'Lỗi');
            }
        });
    }

    return {
        init : function () {
            return init();
        }
    };
})();