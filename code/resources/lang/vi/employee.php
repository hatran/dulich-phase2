<?php
/**
 * Created by PhpStorm.
 * User: quantt
 * Date: 4/4/2018
 * Time: 11:45 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'fullname'   => 'Họ tên',
    'department' => 'Ban',
    'position'   => 'Chức vụ',
    'company'    => 'Nơi công tác',
    'address'    => 'Địa chỉ',
    'phone'      => 'Số điện thoại',
    'email'      => 'Email',
    'action'     => 'Chức năng'

];