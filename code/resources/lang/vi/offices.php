<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'code' => 'Mã VPĐD',
    'name' => 'Tên VPĐD',
    'addr' => 'Địa chỉ',
    'phone' => 'Số điện thoại',
    'email' => "Email",
    'payment_info' => 'Thông tin ngân hàng',
    'note' => "Ghi chú",
    'status' => "Trạng thái",
    'action' => "Chức năng",
    'active' => "Hoạt động",
    'deactive' => "Không hoạt động",
    'total' => "Tổng số bản ghi",
    'addnew' => 'Thêm mới',
    'close' => 'Thoát',
    'save' => 'Lưu',
    'upload_image' => 'Ảnh đại diện',
    'editoffices' => 'Sửa VPĐD',
    'deletetitle' => 'Xóa VPĐD ',
    'addoffice'     => 'Thêm mới VPĐD',
    'haveerrors'    => 'Xảy ra lỗi, không thể cập nhật',
    'success'       => 'Thêm mới VPĐD thành công',
    'delete'        => 'Xóa',
    'undeletecomplete' => 'Xóa VPĐD thất bại',
    'deletecomplete' => 'Xóa VPĐD thành công',
    'employee' => 'Ban chấp hành',
    'undeletecompletewithstatusenable' => 'Không thể xóa với trạng thái đang hoạt động',

];
