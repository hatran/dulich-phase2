<?php

return [
    'description.view_action' => 'Xem',

    'fn_create_user' => 'Tạo mới tài khoản người dùng',
    'fn_update_user' => 'Cập nhật người dùng',
    'fn_delete_user' => 'Xóa người dùng',


    'fn_verify_member' => 'Thẩm định hồ sơ thành công',
    'fn_verify_update_member' => 'Thẩm định yêu cầu bổ sung hồ sơ thành công',
    'fn_verify_reject_member' => 'Từ chối thẩm định hồ sơ',
    'fn_saveDataMemberIntoPdf' => 'Tạo file pdf thông tin thành viên và thông tin chi tiết thành viên',

    'fn_approve_member' => 'Phê duyệt hồ sơ thành công',
    'fn_approve_update_member' => 'Phê duyệt yêu cầu bổ sung hồ sơ thành công',
    'fn_approve_reject_member' => 'Từ chối phê duyệt hồ sơ thành công',

    'fn_manager_success' => 'Đồng ý Phê duyệt cập nhật hồ sơ',
    'fn_manager_reject' => 'Từ chối Phê duyệt cập nhật hồ sơ',

    'fn_revert_status_payment' => 'Cập nhật trạng thái hồ sơ từ Chờ ký quyết định về Chờ nộp lệ phí + hội phí',
    'fn_delete_payment' => ' Xóa thông tin nộp lệ phí + hội phí hồ sơ',
    'fn_change_status_payment' => 'Cập nhật trạng thái hồ sơ từ Chờ nộp lệ phí + hội phí thành Chờ ký quyết định hội viên',
    'fn_save_new_payment' => 'Cập nhật thông tin nộp lệ phí + hội phí hồ sơ',

    'fn_update_decision' => 'Cập nhật thông tin Ký quyết định hồ sơ',
    'fn_delete_decision' => 'Xóa thông tin Ký quyết định hồ sơ',

    'fn_restock_save' => 'Khôi phục lưu kho hồ sơ',
    'fn_put_store' => 'Lưu kho hồ sơ',

    'fn_delete_member' => 'Xóa hồ sơ',

    'fn_delete_introduction' => 'Xóa bài viết mục giới thiệu',
    'fn_create_introduction' => 'Tạo mới bài viết mục giới thiệu',
    'fn_save_introduction' => 'Cập nhật thông tin bài viết mục giới thiệu',

    'fn_create_information' => 'Tạo mới bài viết mục thông tin',
    'fn_save_information' => 'Cập nhật bài viết mục giới thiệu',
    'fn_delete_information' => 'Xóa bài viết mục giới thiệu',

    'fn_update_employee' => 'Cập nhật thành viên',
    'fn_create_new_employee' => 'Tạo mới thành viên',
    'fn_delete_employee' => 'Xóa thành viên',

    'fn_create_clb_of_head' => 'Tạo mới CLB thuộc Hội',
    'fn_save_clb_of_head' => 'Cập nhật CLB thuộc Hội',
    'fn_delete_clb_of_head' => 'Xóa CLB thuộc Hội',
    'fn_save_lead_clb_of_head' => 'Cập nhật Ban Lãnh Đạo CLB thuộc Hội',

    'fn_create_clb_of_office' => 'Tạo mới CLB thuộc Chi Hội',
    'fn_save_clb_of_office' => 'Cập nhật CLB thuộc Chi Hội',
    'fn_delete_clb_of_office' => 'Xóa CLB thuộc Chi Hội',
    'fn_save_lead_clb_of_office' => 'Cập nhật Ban Lãnh Đạo CLB thuộc Chi Hội',
    'fn_save_lead_position_clb_of_office' => 'Cập nhật chức vụ Ban Lãnh Đạo CLB thuộc Chi Hội',
    'fn_delete_lead_clb_of_office' => 'Xóa Ban Lãnh Đạo CLB thuộc Chi Hội',
    'fn_create_banner' => 'Tạo mới Banner',
    'fn_save_banner' => 'Cập nhật Banner',
    'fn_delete_banner' => 'Xóa Banner',
    'fn_create_branche' => 'Tạo mới Chi Hội',
    'fn_save_branche' => 'Cập nhật Chi Hội',
    'fn_delete_branche' => 'Xóa Chi Hội',
    'fn_save_employee_branche' => 'Cập nhật Ban Lãnh Đạo Chi Hội',
    'fn_delete_new' => 'Xóa Tin Tức và Sự Kiện',
    'fn_save_news' => 'Cập nhật Tin Tức và Sự Kiện',
    'fn_create_news' => 'Tạo mới Tin Tức và Sự Kiện',
    'fn_lead_of_vpdd' => 'Cập nhật Ban Lãnh Đạo Văn Phòng Đại Diện',
    'fn_lead_position_of_vpdd' => 'Cập nhật Chức vụ Ban Lãnh Đạo Văn Phòng Đại Diện',
    'fn_create_vpdd' => 'Tạo mới Văn Phòng Đại Diện',
    'fn_save_vpdd' => 'Cập nhật Văn Phòng Đại Diện',
    'fn_delete_vpdd' => 'Xóa Văn Phòng Đại Diện',

    'fn_update_payment_yearly' => 'Cập nhật hội phí thường niên'
];
