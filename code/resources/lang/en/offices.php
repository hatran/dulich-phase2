<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

//    'code' => 'Code',
//    'name' => 'Name',
//    'addr' => 'Address',
//    'phone' => 'Phone',
//    'email' => "Email",
//    'payment_info' => 'Payment Infomation',
//    'note' => "Note",
//    'status' => "Status",
//    'action' => "Action",
//    'active' => "Active",
//    'deactive' => "Disable",
//    'total' => "Total",
//    'addnew' => 'Create New',
//    'close' => 'Close',
//    'save' => 'Save',
//    'upload_image' => 'Upload Image',
//    'editoffices' => 'Edit Office',
//    'deletetitle' => 'Delete Office',
//    'addoffice'     => 'Add new Office',
//    'haveerrors'    => 'Error, cannot update',
//    'success'       => 'Add Office Success',
//    'delete'        => 'Delete',
//    'undeletecomplete' => 'Delete fail',
//    'deletecomplete' => 'Delete Success'

    'code' => 'Mã VPĐD',
    'name' => 'Tên VPĐD',
    'addr' => 'Địa chỉ',
    'phone' => 'Số điện thoại',
    'email' => "Email",
    'payment_info' => 'Thông tin ngân hàng',
    'note' => "Ghi chú",
    'status' => "Trang thái",
    'action' => "Hành động",
    'active' => "Active",
    'deactive' => "Disable",
    'total' => "Tổng số bản ghi",
    'addnew' => 'Thêm mới',
    'close' => 'Đóng',
    'save' => 'Lưu',
    'upload_image' => 'Tải ảnh lên',
    'editoffices' => 'Sửa VPĐD',
    'deletetitle' => 'Xóa VPĐD ',
    'addoffice'     => 'Thêm mới VPĐD',
    'haveerrors'    => 'Xảy ra lỗi, không thể cập nhật',
    'success'       => 'Thêm mới VPĐD thành công',
    'delete'        => 'Xóa',
    'undeletecomplete' => 'Xóa thất bại',
    'deletecomplete' => 'Xóa thành công',
    'employee' => 'Ban chấp hành'

];
