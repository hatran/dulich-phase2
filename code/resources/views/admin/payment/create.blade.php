<div id="_addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="table-line">
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="font-all-popup-title">Số thẻ HDV</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <span id="code_add"/>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Ngày sinh</span>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-8 dialog-content">
                            <span id="birthday_add"/>
                        </div>

                    </div>
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="font-all-popup-title">Họ và tên</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <span id="name_add"/>
                        </div>
                    </div>

                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="font-all-popup-title">Họ và tên</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <span id="name_add"/>
                        </div>
                    </div>

                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="font-all-popup-title">Số chứng từ <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <input class="position-number-payment" type="text" id="number_payment_add" tabindex="201" maxlength="30" style="max-width: 120px"><br/>
                            <span class="errorNumberPayment text-left hidden error-msg"></span>
                        </div>
                        <div class="dropfield col-md-3 col-sm3 col-xs-4 no-break">
                            <span class="font-all-popup-title">Ngày chứng từ <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <input class="date datetime-input position-date" type="text" id="date_payment_add" tabindex="202" maxlength="10" style="max-width: 120px">
                            <span class="errorDatePayment text-left error-msg"></span>
                        </div>
                    </div>
                    <!-- <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Hội phí năm<span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-80 col-sm-8 col-xs-8 dialog-content">
                            <select name="year" id="year_add" tabindex="203">
                                {{--<option value="">Chọn năm</option>--}}
                                @for($i = 0; $i < 5; $i ++)
                                    <option value="{{ date('Y') + $i }}">{{ date('Y') + $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div> -->
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Số tiền <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <select name="currency_add" id="currency_add" tabindex="204" style="max-width: 90px;     width: 100%;">
                                <option value="500000">500,000</option>
                                <option value="1000000">1,000,000</option>
                                <option value="1500000">1,500,000</option>
                            </select>
                            <label for="currency_add" style="font-size: 12px; font-weight: 0; width: 5px;">VNĐ</label>
                            <br/>
                            <span class="errorCurrency text-left hidden error-msg"></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Ngày hết hạn</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <span id="expiration_add"></span>
                        </div>
                    </div>
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Số năm <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-8 dialog-content">
                            <select name="year[]" id="number_of_year_add" class="position_numberofyear" multiple="multiple" tabindex="203" style="width: 100%;">
                                @foreach($year as $y)
                                    @if ($y == $currentYear)
                                        <option value="{{ $y }}" checked>{{ $y }}</option>
                                    @else
                                        <option value="{{ $y }}">{{ $y }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br/>
                            <span class="errorNumberOfYear text-left hidden error-msg"></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div>
                                <input type="radio" name="paymentTypeAdd" value="1" checked="checked"> Lần đầu
                                <span style="margin-right: 8px;"></span>
                                <input type="radio" name="paymentTypeAdd" value="2" disabled="disabled"> Thường niên<br>
                            </div>
                        </div>
                    </div>
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Nội dung nộp tiền <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-8 dialog-content">
                            <textarea class="form-control position-content" id="content_add" rows="5" tabindex="205" maxlength="200"></textarea><br/>
                            <span class="errorContent text-left hidden error-msg"></span>
                        </div>
                    </div>

                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Ghi chú</span>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-8 dialog-content">
                            <textarea class="form-control" id="note_add" rows="5" tabindex="206" maxlength="200"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary add" id="btnsubmit" tabindex="207">
                            <span class='glyphicon glyphicon-plus'></span> Tạo mới
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal" tabindex="208">
                            <span class='glyphicon glyphicon-remove'></span> Thoát
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
