<div id="_showModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                        <span>Số thẻ HDV:</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                        <span id="code_show"/>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                        <span>Họ và tên:</span>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-8 dialog-content">
                        <span id="name_show" style="word-wrap: break-word;"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span>Ngày sinh:</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                        <span id="birthday_show"/>
                    </div>
                </div>

                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                        <span>Số tham chiếu:</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                        <span id="number_payment_show" style="word-wrap: break-word;"/>
                    </div>
                    
                    <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                        <span>Ngày chứng từ:</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                        <span id="date_payment_show"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span>Số tiền:</span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                        <span id="currency_show"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span>Nội dung nộp tiền:</span> 
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                        <span id="content_show" style="word-wrap: break-word;"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span>Ghi chú:</span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                        <span id="note_show" style="word-wrap: break-word;"/>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
