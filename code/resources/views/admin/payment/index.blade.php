<?php
setlocale(LC_MONETARY,"en_US");
use App\Providers\MemberPaymentsYearlyServiceProvider;
?>

@extends('admin.layouts.app')
@section('title', 'Quản trị Phí Thường Niên')

@section('header_embed')
    <!-- CSFR token for ajax call -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
    <style>
        a.button {
            padding-left: 20px;
            padding-right: 20px;
            margin-left: 5px;
            margin-right: 5px;
        }

        a.button > span.glyphicon {
            color: white;
        }

        .no-break {
            white-space: nowrap;
        }

        .modal-body .line {
            margin-bottom: 10px;
        }

        .dialog-content > span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }

        .dialog-content > span.error-msg {
            color: #a94442;
        }

        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            /*background-color: #f35d23;*/
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }

        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }

        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }

    </style>
@endsection

@section('content')
    <div class="page-wrap">
    @include('admin.layouts.steps')
    <!-- <div class="heading">Cập nhật thông tin đóng lệ phí, phí</div> -->
        {{--@if ($errors->has('error_message'))
            <div class="alert alert-danger">
                <div>{{ $errors->first('error_message') }}</div>
            </div>
        @endif
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <div class="alert alert-success">
                <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
            </div>
        @endif--}}
        <div class="main-content" style="padding-bottom: 75px;">
        @include('admin.payment_yearly.search_payment_yearly')
        @include('admin.layouts.message')
        <!-- <a class="button btn-primary" href="{{ route('admin_payment_fee_import_view') }}" style="float:  right;width: 150px; height: 37px; line-height: 24px;">Nhập phí từ excel</a> -->
            <a class="import_view-modal button btn-primary"
               style="float:  right;width: 150px; height: 37px; line-height: 24px; margin-bottom: 5px"
               data-file-id="-1" href="javascript:void(0)">
                Nhập phí từ excel</a>

            <a class="resend-notification-sms button btn-primary" style="float:  right;width: 100px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Gửi SMS</a>
            <a class="resend-notification-email button btn-primary" style="float:  right;width: 100px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Gửi Email</a>

            <div class="total">Tổng số: {{$count_members}}</div>

            <div class="table-wrap" style="margin-top: 5px">
                <table>
                    <thead>
                    <tr>
                        <td width="5%">Số TT</td>
                        <td width="7%">Mã hồ sơ </td>
                        <td width="7%">Số thẻ HDV</td>
                        <td width="15%">Họ và tên</td>
                        <td width="7%">Hội phí năm</td>
                        <td width="7%">Số chứng từ</td>
                        <td width="7%">Ngày chứng từ</td>
                        <td width="10%">Số tiền (VNĐ)</td>
                        <td width="7%">Ngày gia nhập VTGA</td>
                        <td width="10%">Trạng thái</td>
                        <td width="6%">Loại hội phí</td>
                        <td width="10%">Chức năng</td>
                        <td width="5%" style="position: relative; top:0px; color: #144a8b;">TB Nộp Phí<br>
                            <input type="checkbox" class="checkAll">
                            <label>&nbsp;</label>
                        </td>
                        <!-- <td width="5%">TB Nộp Phí</td> -->
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objMembers) == 0)
                        <tr>
                            <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            @php $isDelete = $objMember->is_delete; @endphp
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
								<td class="text-center">
                                    {{ $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('admin_list_member_detail_view' , !empty($objMember->mid) ? $objMember->mid : (!empty($objMember->id) ? $objMember->id : '')) }}">{{ $objMember->touristGuideCode }} </a>
                                </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }}</td>
                                <td>{{ isset($objMember->year) ? $objMember->year : '' }}</td>
                                @if (!$isDelete)
                                <td class="text-left wrap-text"> {{ $objMember->number_payment }} </td>
                                <td class="text-center"> {{ empty($objMember->date_payment) ? '' : date('d/m/Y', strtotime($objMember->date_payment)) }} </td>
                                <td class="text-right">
                                    @if (strpos($objMember->currency, '.') !== false || strpos($objMember->currency, ',') !== false)
                                        {{ $objMember->currency }}
                                    @else
                                        {{ number_format($objMember->currency, 0, ',', '.') }}
                                    @endif
                                </td>
                                @else
                                <td></td>
                                <td></td>
                                <td></td>
                                @endif
                                <td class="text-center"> {{ empty($objMember->member_from) ? '' :  $objMember->member_from }} </td>

                                @if(empty($objMember->status))
                                    <td class="text-left wrap-text"></td>
                                @else
                                    <td class="text-left">
                                        {{ array_get(\App\Constants\MemberConstants::$file_accountant_const, $objMember->status, '') }}
                                    </td>
                                @endif
                                <td>
                                    @if ($objMember->status == \App\Constants\MemberConstants::SIGNING_WAITING)
                                        Hội phí năm đầu
                                    @endif
                                </td>
                                <td align="center" class="text-center">
                                    @if($objMember->status == \App\Constants\MemberConstants::FEE_WAITING)
                                        <a class="btn-function add-modal"
                                           id="btnAddPayment"
                                           href="#"
                                           data-toggle="tooltip"
                                           data-placement="left"
                                           title="Nộp hội phí"
                                           data-id="{{ !empty($objMember->id) ? $objMember->id . '?mpuid=null' : '' }}"
                                           data-status="{{ $objMember->status }}"
                                           data-approved="{{ empty($objMember->approved_at) ? '' :  date('d/m/Y', strtotime($objMember->approved_at)) }}">
                                            <span class="glyphicon glyphicon-plus" style="font-size: 16px; color: #0000ff; "></span>
                                        </a>
                                    @elseif($objMember->status == \App\Constants\MemberConstants::SIGNING_WAITING)
                                        <a class="btn-function show-modal" href="#" data-toggle="tooltip" data-placement="left" title="Xem chứng từ" data-id="{{ !empty($objMember->id) ? $objMember->id. '?mpuid=1&payment=null' : '' }}" data-status="{{ $objMember->status }}">
                                            <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                        </a>
                                        <a class="btn-function edit-modal" href="#" data-toggle="tooltip" data-placement="left" title="Sửa chứng từ" data-id="{{ !empty($objMember->id) ? $objMember->id. '?mpuid=1&payment=null' : '' }}" data-status="{{ $objMember->status }}" data-approved="{{ date('d/m/Y', strtotime($objMember->approved_at)) }}" data-numberpayment="{{ $objMember->number_payment }}">
                                            <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                        </a>
                                        <a class="btn-function delete-modal" href="#" data-toggle="tooltip" data-placement="left" title="Xoá chứng từ"  data-id="{{ !empty($objMember->id) ? $objMember->id. '?mpuid=1&payment=null' : '' }}" data-status="{{ $objMember->status }}">
                                            <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                        </a>
                                    @endif
                                </td>

                                <td class="text-center">
                                    <span class="checkitem">
                                        <input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{ $objMember->id }}" class="checkbox-pr">
                                        <label for="member {{ $key + 1 }}">&nbsp;</label>
                                    </span>
                                </td>

                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- <div class="sent_notify" align="right">
                <a class="button" id="send-notification" href="#">Gửi Thông Báo</a>
            </div> -->
            @include('admin.layouts.pagination')
        </div>
    </div>
    @if (session('error_message'))
        <div type="hidden" id="error_message">
            {{ session('error_message') }}
        </div>
    @endif

    <input type="hidden" id="member-id"/>
    <input type="hidden" id="current-status"/>
    <input type="hidden" id="approved_at"/>
    <input type="hidden" id="verified_at"/>

    @include('admin.payment_yearly.detail')
    @include('admin.payment.create')
    @include('admin.payment.edit')
    @include('admin.payment.delete')
    @include('admin.payment.import_modal')
@endsection

@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/simple.money.format.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#number_of_year_add').select2();
            $('#number_of_year_edit').select2();
        });

        $(document).on('change', '#number_of_year_add', function (event) {
            var curVal = $(this).val();
            $("#expirationDate").html('');
            if (curVal.length != 0) {
                var new_year = curVal[curVal.length - 1];
                $("#expiration_add").html('31/12/'+new_year);
            }
        });

        $(document).on('change', '#number_of_year_edit', function (event) {
            var curVal = $(this).val();
            $("#expirationDate").html('');
            if (curVal.length != 0) {
                var new_year = curVal[curVal.length - 1];
                $("#expiration_edit").html('31/12/'+new_year);
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#btnsubmit").click();
                }
            }
        });
        function clearErrorFields() {
            $('.errorNumberPayment').addClass('hidden');
            $('.errorDatePayment').addClass('hidden');
            $('.errorCurrency').addClass('hidden');
            $('.errorContent').addClass('hidden');
        }

        var url = "{{ url('/officesys/payment') }}"
        function htmlEntities(str) {
            return str;
        }

        function showPopup(memberId, postfix) {
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';

            switch (postfix) {
                case '_add':
                    title = 'Thêm mới thông tin hội phí';
                    canEdit = true;
                    break;

                case '_edit':
                    title = 'Cập nhật thông tin hội phí';
                    canEdit = true;
                    url = "{{ url('/officesys/payment_yearly') }}";
                    break;

                case '_delete':
                    title = 'Bạn có chắc chắn muốn xóa thông tin hội phí không?';
                    url = "{{ url('/officesys/payment_yearly') }}";
                    break;
                default:
                    title = 'Chi tiết thông tin hội phí';
					url = "{{ url('/officesys/payment_yearly') }}";
            }

            $('.position-number-payment').removeClass('error_border');
            $('.position-date').removeClass('error_border');
            $('.position-currency').removeClass('error_border');
            $('.position-content').removeClass('error_border');

            $('.modal-title').text(title);
            $('#code' + postfix).text('...');
            $('#name' + postfix).text('...');
            $('#birthday' + postfix).text('...');
            if (canEdit === true) {
                $('#number_payment' + postfix).val('...');
                $('#date_payment' + postfix).val('...');
                $('#currency' + postfix).val('...');
                clearErrorFields();
            } else {
                $('#number_payment' + postfix).text('...');
                $('#date_payment' + postfix).text('...');
                $('#currency' + postfix).text('...');
            }
            $('#content' + postfix).text('...');
            $('#note' + postfix).text('...');
            $(modalId).modal('show');
            $(modalId + ' .modal-dialog').loader('show');

            $.ajax({
                url: url + "/" + memberId,
                type: 'GET',
                success: function (response) {
                    $(modalId + ' .modal-dialog').loader('hide');

                    var objMember = response.objMember;
                    var objMemberPayment = response.objMemberPayment;
                    var objMemberPaymentEdit = response.objMemberPaymentEdit;
                    $('#code' + postfix).text(objMember.touristGuideCode);
                    $('#name' + postfix).text(objMember.fullName);
                    $('#birthday' + postfix).text(objMember.formattedBirthday);
                    $("#verified_at").val(toTimestamp(objMember.verified_at));
                    var numberPayment = '';
                    var datePayment = '';
                    var currency = '';
                    var content = '';
                    var note = '';
                    $("#number_payment_h").val("");

                    var table_payment = $('.table_payment tbody');
                    $('.table_payment tbody tr').remove();

                    if (objMemberPayment !== null) {
                        objMemberPayment.forEach(function (payment, index) {
                            var incre = index + 1;
                            table_payment.append(
                                "<tr>" +
                                "<td>" + incre + "</td>" +
                                "<td>" + payment.number_payment + "</td>" +
                                "<td>" + payment.date_payment + "</td>" +
                                "<td>" + payment.currency + "</td>" +
                                "<td>" + payment.member_code_expiration.substr(0,10).replaceAll('-', '/') + "</td>" +
                                "<td>" + payment.payment_content + "</td>" +
                                "<td>" + payment.note + "</td>" +
                                "</tr>"
                            );
                        });
                    }

                    if (objMemberPaymentEdit) {
                        numberPayment = objMemberPaymentEdit.number_payment;
                        datePayment = objMemberPaymentEdit.date_payment;
                        currency = htmlEntities(objMemberPaymentEdit.currency);
                        content = htmlEntities(objMemberPaymentEdit.payment_content);
                        note = htmlEntities(objMemberPaymentEdit.note);
                        year = htmlEntities(objMemberPaymentEdit.year);
                    }
                    if (canEdit === true) {
                        $('#number_payment' + postfix).val(numberPayment);
                        $('#date_payment' + postfix).val(datePayment);
                        $('#currency' + postfix).val(currency).change();
                        if(postfix == '_edit'){
                            $("#number_of_year_edit").val([year]).change();
                        }
                    } else {
                        $('#number_payment' + postfix).text(numberPayment);
                        $('#date_payment' + postfix).text(datePayment);
                        $('#currency' + postfix).text(currency);
                    }
                    //focus number payment
                    if (postfix == '_add' || postfix == '_edit') {
                        var number_payment = $("#number_payment" + postfix);
                        setTimeout(function () {
                            number_payment.focus();
                        }, 500);
                    }

                    $('#content' + postfix).text(content);
                    $('#note' + postfix).text(note);
                }
            });

        }

        $(document).on('click', '.show-modal', function (event) {
            event.preventDefault();
            var memberId = $(this).data('id');
            showPopup(memberId, '_show');
        });

        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            var memberId = $(this).data('id');
            $('input[type=hidden]#member-id').val(memberId);
            $("#current-status").val($(this).data("status"));
            $("#approved_at").val($(this).data("approved"));
            showPopup(memberId, '_add');
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            console.log($(this).data("numberpayment"));
            var memberId = $(this).data('id');
            $('input[type=hidden]#member-id').val(memberId);
            $("#current-status").val($(this).data("status"));
            $("#approved_at").val($(this).data("approved"));
            $("#number_payment_hidden").val($(this).data("numberpayment"));
            $('#number_of_year_edit_label').text($(this).data("fromyear"));
            $('#number_of_year_edit').val($(this).data("fromyear"));
            showPopup(memberId, '_edit');
        });

        $(document).on('click', '.delete-modal', function (event) {
            event.preventDefault();
            var memberId = $(this).data('id');
            $('input[type=hidden]#member-id').val(memberId);
            $("#current-status").val($(this).data("status"));
            showPopup(memberId, '_delete');
        });

        function doAction(memberId, postfix) {
            var action = 'POST';
            var modalId = '#' + postfix + 'Modal';
            var data = {
                'number_payment': $('#number_payment' + postfix).val(),
                'currency': $('#currency' + postfix).val(),
                'date_payment': $('#date_payment' + postfix).val(),
                'content': $('#content' + postfix).val(),
                'note': $('#note' + postfix).val(),
                'status': $("#current-status").val(),
                'number_payment_hidden': postfix == '_edit' ? $("#number_payment_hidden").val() : $("#number_payment_h").val(),
                'years': postfix == '_add' ? $("#number_of_year_add").val() : $("#number_of_year_edit").val(),
				'expiration_date': $("#expiration" + postfix).text()
            };
            //validation  Ngày chứng từ
            var date = new Date();
            var today = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            var date_payment = $("#date_payment" + postfix);
            var number_payment = $('#number_payment' + postfix);
            var currency = $('#currency' + postfix);
            var content = $('#content' + postfix);
            var note = $('#note' + postfix);
            var apprved_date = parseDate($('#approved_at').val()).getTime()/1000;

            if (postfix == '_add' || postfix == '_edit') {
                $('.position-number-payment').removeClass('error_border');
                $('.errorNumberPayment').addClass('hidden');
                $('.position-date').removeClass('error_border');
                $('.errorDatePayment').addClass('hidden');
                $('.position-currency').removeClass('error_border');
                $('.errorCurrency').addClass('hidden');
                $('.position-content').removeClass('error_border');
                $('.errorContent').addClass('hidden');
            }
            var flg = 0;
            if (postfix == '_add' || postfix == '_edit') {
                if (date_payment.val() == '') {
                    $(".errorDatePayment").html('Ngày chứng từ không được bỏ trống');
                    $(".errorDatePayment").removeClass('hidden');
                    date_payment.addClass('error_border');
                    flg = 1;
                } else {
                    today = parseDate(today).getTime();
                    var verifiedDate = $('#verified_at').val();
                    var paymentDate = parseDate(date_payment.val()).getTime();
                    if (today < paymentDate) {
                        setTimeout(function () {
                            $(".errorDatePayment").html('Ngày chứng từ không được lớn hơn ngày hiện tại');
                            $(".errorDatePayment").removeClass('hidden');
                            date_payment.addClass('error_border');
                        }, 500);

                        flg = 1;
                    } else if(paymentDate < apprved_date){
                        $(".errorDatePayment").html('Ngày chứng từ không được nhỏ hơn ngày phê duyệt');
                        $(".errorDatePayment").removeClass('hidden');
                        date_payment.addClass('error_border');
                        flg = 1;
                    } else {
                        $(".errorDatePayment").addClass('hidden');
                        $(".errorDatePayment").html('');
                        date_payment.removeClass('error_border');
                    }
                }

                //validation so chung tu

                if (number_payment.val() == '') {
                    $(".errorNumberPayment").html('Số chứng từ không được bỏ trống');
                    $(".errorNumberPayment").removeClass('hidden');
                    number_payment.addClass('error_border');
                    flg = 1;
                } else {
                    $(".errorNumberPayment").html('');
                    $(".errorNumberPayment").addClass('hidden');
                    number_payment.removeClass('error_border');

                }
                //validation so tien
                if (currency.val() == '') {
                    $(".errorCurrency").html('Số tiền không được bỏ trống');
                    $(".errorCurrency").removeClass('hidden');
                    currency.addClass('error_border');
                    flg = 1;
                } else {
                    var regexSt = /^([0-9])+$/;
                    if (!regexSt.test(currency.val())) {
                        $(".errorCurrency").html('Số tiền phải là số nguyên dương > 0');
                        $(".errorCurrency").removeClass('hidden');
                        currency.addClass('error_border');
                        flg = 1;
                    } else if(currency.val() <= 0) {
                        $(".errorCurrency").html('Số tiền phải là số nguyên dương > 0');
                        $(".errorCurrency").removeClass('hidden');
                        currency.addClass('error_border');
                        flg = 1;
                    } else {
                        $(".errorCurrency").html('');
                        $(".errorCurrency").addClass('hidden');
                        currency.removeClass('error_border');
                    }
                }
                //validation Nội dung nộp tiền
                if (content.val() == '') {
                    $(".errorContent").html('Nội dung nộp tiền không được bỏ trống');
                    $(".errorContent").removeClass('hidden');
                    content.addClass('error_border');
                    flg = 1;
                } else {
                    $(".errorContent").addClass('hidden');
                    $(".errorContent").html('');
                    content.removeClass('error_border');
                }
            }

            if (postfix == '_add') {
                var a = $('#number_of_year_add').val();
                var numberOfYear = $('.select2-selection--multiple');
                $('.position_numberofyear').removeClass('error_border');
                $('.errorNumberOfYear').addClass('hidden');
                if (a.length == 0) {
                    $(".errorNumberOfYear").html('Số năm không được bỏ trống');
                    $(".errorNumberOfYear").removeClass('hidden');
                    numberOfYear.addClass('error_border');
                    flg = 1;
                }
                else {
                    $(".errorNumberOfYear").addClass('hidden');
                    $(".errorNumberOfYear").html('');
                    numberOfYear.removeClass('error_border');
                }
            }

            if (flg == 1) {
                setTimeout(function () {
                    $(modalId).modal('show');
                }, 500);
                return false;
            }

			$('#btnsubmit').text("Đang xử lý ...").attr('disabled', 'disabled');


            switch (postfix) {
                case '_edit':
                    action = 'POST';
                    url = "{{ url('/officesys/payment') }}";
					url = url + "/update";
                    break;

                case '_delete':
                    action = 'POST';
                    data = '';
                    url = "{{ url('/officesys/payment') }}";
					url = url + '/destroy';
                    break;
                case '_add':
                    action = 'POST';
            }

            $.ajax({
                type: action,
                url: url + '/' + memberId,
                data: data,
                success: function (data) {
                    clearErrorFields();

                    if (data.errors) {
                        if (data.errors.memberInvalid) {
                            toastr.warning(data.errors.memberInvalid, 'Thông báo', {timeOut: 2000});
                            $(modalId).modal('hide');
                            document.location.reload();
                        }
                        setTimeout(function () {
                            $(modalId).modal('show');
                        }, 500);
                        if (data.errors.invalid) {
                            toastr.warning(data.errors.number_payment, '', {timeOut: 2000});
                        }
                        if (data.errors.number_payment) {
                            $('.position-number-payment').addClass('error_border');
                            $('.errorNumberPayment').removeClass('hidden');
                            $('.errorNumberPayment').text(data.errors.number_payment);
                        }
                        if (data.errors.date_payment) {
                            $('.position-date').addClass('error_border');
                            $('.errorDatePayment').removeClass('hidden');
                            $('.errorDatePayment').text(data.errors.date_payment);
                        }
                        if (data.errors.currency) {
                            $('.position-currency').addClass('error_border');
                            $('.errorCurrency').removeClass('hidden');
                            $('.errorCurrency').text(data.errors.currency);
                        }
                        if (data.errors.content) {
                            $('.position-content').addClass('error_border');
                            $('.errorContent').removeClass('hidden');
                            $('.errorContent').text(data.errors.content);
                        }
                    } else {
                        toastr.success(data.success, 'Thông báo', {timeOut: 2000});
                        $(modalId).modal('hide');
                        document.location.reload(true);
                    }
                }
            });
        }

        $('.modal-footer').on('click', '.add', function () {
            var memberId = $('input[type=hidden]#member-id').val();
            doAction(memberId, '_add');
        });

        $('.modal-footer').on('click', '.edit', function () {
            var memberId = $('input[type=hidden]#member-id').val();
            doAction(memberId, '_edit');
        });

        $('.modal-footer').on('click', '.delete', function () {
            var memberId = $('#number_payment_delete').text();
            doAction(memberId, '_delete');
        });


        function sendNotification(memberIds) {
            console.log(memberIds);
            resetCheckboxes();

            $.ajax({
                type: 'POST',
                url: url + '/notifications/send',
                data: {
                    'member-ids': memberIds
                },
                success: function (data) {
                    if (data.errors) {
                        toastr.warning('Có lỗi hệ thống xảy ra. Vui lòng thử lại trong ít phút nữa', '', {timeOut: 2000});
                        console.log(data.errors);
                    } else {
                        toastr.success(data.success, 'Hệ thống đã gửi thông báo thành công', {timeOut: 1000});
                    }
                }
            });
        }

        function checkedIds() {
            return $("input[id='chk-inform']:checked").map(function () {
                return $(this).data('id');
            }).get();
        }

        function resetCheckboxes() {
            $("input[id='chk-inform']:checked").each(function () {
                $(this).prop('checked', false);
            });
        }

        $(document).on('click', '#send-notification', function (event) {
            event.preventDefault();
            isCheck = true;
            var checkIds = checkedIds();
            if (checkIds.length > 0) {
                toastr.clear()
                $("#send-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function () {
                    $("#send-notification").css('pointer-events', 'auto');
                }
                toastr.warning('Đang tiến hành gửi thông báo. Vui lòng đợi trong giây lát.', '', {timeOut: 2000});
                sendNotification(checkIds);
            } else {
                toastr.clear()
                $("#send-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function () {
                    $("#send-notification").css('pointer-events', 'auto');
                }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $(document).on('click', '.import_view-modal', function () {
            $('#importViewModal').modal('show');
        });

        var error_message = $("#error_message").text();
        if (error_message) {
            toastr.clear()
            $("#send-notification").css('pointer-events', 'none');
            toastr.options.onHidden = function () {
                $("#send-notification").css('pointer-events', 'auto');
            }
            toastr.error("File import Lệ phí, hội phí không hợp lệ", 'Lỗi', {timeOut: 2000});
        }

        $(document).ready(function () {
            document.getElementById("file_code").focus();
        });
        function toTimestamp(strDate){
            var datum = Date.parse(strDate);
            return datum/1000;
        }

        $("#import_excel").on('click', '.import_excel_submit', function () {
            validateFileUpload();
        });

        function validateFileUpload() {
            var file;
            var FileSize;
            var extension;

            if ($('#fileToUpload').val() != '') {
                file      = $('#fileToUpload')[0].files[0];
                extension = file.name.substr((file.name.lastIndexOf('.') +1));
                FileSize  = file.size / 1024 / 1024; // in MB
            } else {
                FileSize = 0;
                extension = '';
            }

            if (FileSize == 0) {
                alert('Không có file!');
            } else if (FileSize > 10){
                alert('File quá lớn, dung lượng file cho phép tối đa là 10Mb!');
            } else if (extension != 'xlsx' && extension != '') {
                alert('Bạn chỉ được phép upload file excel xlsx, vui lòng thực hiện lại');
            } else {
                $('#import_excel').get(0).submit();
            }
        }


        $('body').on('change', '.checkAll', function() {
            $(".checkbox-pr").prop('checked',$(this).is(":checked"));
            if ($(this).is(":checked") == true) {
                $(".checkbox-pr").attr('data-check', 'yes');
            }
            else {
                $(".checkbox-pr").attr('data-check', 'no');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;

            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                    if ($(this).is(':checked')){
                        anyChecked = true;
                    } else {
                        anyChecked = false;
                    }
                });
                if (anyChecked == true) {
                    __this.attr("data-check", "yes");

                    if (allRowLength == allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");

                __this.attr('data-check', 'no');
            }
        });

        function checkedNames() {
            check = 0;
            $("input[name=member]:checked").each( function () {
                check = 1;
            });
            return check;
        }

        $(".resend-notification-email").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('admin_list_member_send_notification_email_remind_completing_fee') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $(".resend-notification-sms").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('admin_list_member_send_notification_sms_remind_completing_fee') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });
    </script>
@endsection
