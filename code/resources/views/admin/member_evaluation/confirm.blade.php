<!-- Modal -->
<div class="modal fade" id="confirmEvaluation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <table id="table4" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                    <tbody>
                        <tr>
                            <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 18px; font-weight: 700; text-align: center;">Thông tin tour HDV đã hoàn thành hướng dẫn</h1></th>
                        </tr>
                        <tr>
                            <td style="width: 50%;vertical-align: middle;">Mã đoàn</td>
                            <td>
                                <span id="memberEvaluationTouristCode"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle;">Tour tuyến</td>
                            <td>
                                <span id="memberEvaluationForteTour"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle;">Số lượng khách</td>
                            <td>
                                <span id="memberEvaluationNumberOfTourist"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle;">Thời gian (từ ngày/tháng/năm - đến ngày/tháng/năm)<span class="color-required">*</span></td>
                            <td style="display: inline-block; width: 100%;">
                                <span id="memberEvaluationFromDate"></span>
                                -
                                <span id="memberEvaluationToDate"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table id="table6" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                    <tbody>
                    <tr>
                        <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 18px; font-weight: 700; text-align: center;">Nội dung chấm điểm</h1></th>
                    </tr>
                    <tr>
                        <td style="width: 50%;vertical-align: middle;">Giao tiếp, thái độ và sự ân cần</td>
                        <td>
                            <span id="scoreCriteria1"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%;vertical-align: middle;">Thuyết minh</td>
                        <td>
                            <span id="scoreCriteria2"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%;vertical-align: middle;">Giải quyết tình huống</td>
                        <td>
                            <span id="scoreCriteria3"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%;vertical-align: middle;">Văn nghệ và hoạt náo</td>
                        <td>
                            <span id="scoreCriteria4"></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%;vertical-align: middle;">Tổ chức, quản lý, làm việc nhóm</td>
                        <td>
                            <span id="scoreCriteria5"></span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-body">
                <span class="text-center" style="font-style: italic; font-weight: bold;">*Công ty Lữ hành kiểm tra lại các thông tin đánh giá lần nữa, nếu thấy cần điều chỉnh thì nhấn vào ô "Chỉnh sửa", nếu thấy đúng rồi thì nhấn vào ô "Gửi đánh giá". </span>
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-warning"
                        data-dismiss="modal" style="width: unset; border-color: unset; height: unset; padding: 10px 20px;">Chỉnh sửa</button>
                <button type="button" class="btn btn-primary" onclick="return submitForm();" style="width: unset; border-color: unset; ; height: unset; padding: 10px 20px;">Gửi đánh giá</button>
            </div>
        </div>
    </div>
</div>
<style>
    td {
        font-weight: 700;
    }
</style>