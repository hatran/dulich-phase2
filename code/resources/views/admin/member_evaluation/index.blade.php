@extends('admin.layouts.app')
@section('title', 'Quản trị danh sách đánh giá của công ty lữ hành')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .error_border {-webkit-box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);}
    .total {line-height: 30px;padding: 6px 10px;color: #0000F0 !important;border-radius: 3px;font-size: 16px;border: 0;display: inline-block;}
    .form-horizontal .control-label {padding-top: 7px;margin-bottom: 0;text-align: left;}
    .fa{cursor: pointer}
    .fa-2x {font-size: 1.2em;}
    .table-wrap table tr td {position: relative;word-wrap: break-word;white-space: normal !important;}

    .table-wrap table span {font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;}
    .tooltip-inner {
        max-width: 350px;
        width: 350px;
        position: relative;
        word-wrap: break-word;
        white-space: normal !important;
        z-index: 999999;
    }
    .rq-star {color: #ff0000;}
    .btn-primary.btn-custom{
        color: #fff;
        line-height: 21px;
        height: 35px;
        line-height: 35px;
        border: 1px solid #BBBABA;
        padding: 0 10px;
        max-width: 100%;
        display: block;
        border-radius: 2px;
    }
    .modal-title {
        margin: 0;
        font-weight: bold;
        float: left;
        font-size: 20px;
    }
</style>
<div class="main-content">
    @include('admin.member_evaluation.search')
    <div class="clearfix"></div>
    <div class="page-wrap">
        <!-- <div class="heading">Kết quả tìm kiếm</div> -->
        @include('admin.layouts.message')
        <div class="total">Tổng số: {{$count_evaluations}}</div>
        <div class="table-wrap">
            <table>
                <thead>
                    <tr>
                        <td width="3%">STT</td>
                        <td width="10%">Mã số thuế</td>
                        <td width="13%">Tên công ty lữ hành</td>
                        <td width="7%">Số thẻ HDV</td>
                        <td width="8%">Số thẻ hội viên</td>
                        <td width="15%">Họ và tên</td>
                        <td width="10%">Mã đoàn</td>
                        <td width="10%">Tour tuyến</td>
                        <td width="7%">Tổng điểm</td>
                        <td width="10%">Ngày đánh giá</td>
                        <td width="7%">Chức năng</td>
                    </tr>
                </thead>
                <tbody>
                    @if (count($objEvaluations) == 0)
                    <tr>
                        <td colspan="11" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                    </tr>
                    @else
                    @foreach($objEvaluations as $key => $item)
                    <tr>
                        <td class="text-center"> {{$current_paginator + $key + 1 }} </td>
                        <td class="text-center">{{ $item->tax_code }}</td>
                        <td>{{ $item->company_name }}</td>

                        <td class="text-center">{{ $item->touristGuideCode }}</td>
                        <td class="text-center">{{ $item->member_code }}</td>
                        <td>{{ $item->fullName }}</td>
                        <td class="text-center">{{ $item->tourist_code }}</td>
                        <td>{{ $item->forte_tour }}</td>
                        <td class="text-center">
                            <?php $score = 0; ?>
                            @foreach ($criteria as $key => $value)
                                @if ($key == $item->criteria1_code)
                                    <?php $score += $item->criteria1 * $value / 100; ?>
                                @elseif ($key == $item->criteria2_code)
                                    <?php $score += $item->criteria2 * $value / 100; ?>
                                @elseif ($key == $item->criteria3_code)
                                    <?php $score += $item->criteria3 * $value / 100; ?>
                                @elseif ($key == $item->criteria4_code)
                                    <?php $score += $item->criteria4 * $value / 100; ?>
                                @elseif ($key == $item->criteria5_code)
                                    <?php $score += $item->criteria5 * $value / 100; ?>
                                @else
                                @endif
                            @endforeach
                            {{ round($score, 2) }}
                        </td>
                        <td class="text-center">{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                        <td class="text-center">
                            <a title="Xem đánh giá" style="padding-right:10px" href="{{ route('company_evaluate_show', ['id' => $item->id, 'tourist_code' => $item->tourist_code]) }}">
                                <i class="fa fa-eye fa-2x" aria-hidden="true" style="font-size: 18px; color: #009933;"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @include('admin.layouts.pagination')
    </div>
    
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Xóa Thông tin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Bạn chắc chắn muốn xóa <span id="id-em" style="font-size:  20px;color:  blue;"></span> ra khỏi danh sách?</h4>
                    <br/>
                    <form id="delete_info" action="" method="post">
                        {{ method_field('post') }}
                        {{ csrf_field() }}
                    </form>
                </div>
                <input type="hidden" id="id_delete" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="delete_infomation();">Xóa</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Thoát</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_embed')
<script>
    function showDeleteModal(__this) {
        var dataURL = __this.attr('data-href');
        console.log(__this.attr('data-title'));
        $('#id-em').text(__this.attr('data-title'));
        $('#delete_info').attr('action', dataURL);
        $('#deleteModal').modal({show: true});
        return false;
    }
    function delete_infomation() {
        $('#delete_info').submit();
    }
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endsection