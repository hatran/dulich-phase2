<form class="page-wrap" name="" method="get" id="formSearch">
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tên công ty lữ hành
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="company_name" type="text" name="company_name" value="{{ Input::get('company_name', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Mã số thuế
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="2" id="tax_code" type="text" name="tax_code" value="{{ Input::get('tax_code', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Họ tên hướng dẫn viên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="3" id="member_name" type="text" name="member_name" value="{{ Input::get('member_name', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
           Số thẻ HDV / Số thẻ hội viên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="display: flex; margin-bottom: 10px;">
            <input class="form-control" tabindex="4" id="tourist_guide_code" type="text" name="tourist_guide_code" value="{{ Input::get('tourist_guide_code', '') }}" maxlength="50" placeholder="Số thẻ HDV" style="width: 50%">

            <input class="form-control" tabindex="5" id="member_code" type="text" name="member_code" value="{{ Input::get('member_code', '') }}" maxlength="50" placeholder="Số thẻ hội viên" style="width: 50%; margin-left: 10px;">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Mã đoàn
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="6" id="tourist_code" type="text" name="tourist_code" value="{{ Input::get('tourist_code', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tour tuyến
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="7" id="forte_tour" type="text" name="forte_tour" value="{{ Input::get('forte_tour', '') }}" maxlength="50">
        </div>
        
        {{ csrf_field() }}
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="8" type="submit" onclick="return submitForm();" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm tin tức</h4>
            </div>
            <div class="modal-body">
                <h4>Phát hành từ ngày phải nhỏ hơn ngày kết thúc</h4>
            </div>
            <div class="modal-footer">                   
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    Thoát
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).keypress(function (e) {
        if ($('#formSearch input:focus, #formSearch button:focus').length != 0) {
            if (e.which === 13) {
                submitForm();
            }
        }
    });

    function submitForm() {
        $('#formSearch').submit();
        return false;
    }

    $("#company_name").focus();
</script>