@extends('admin.layouts.app')
@section('title', 'Chi tiết đánh giá HDV - ' . $objMember->fullName)
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="page-wrap">
        <div class="block">
            <div class="block-heading">1. Thông tin HDV</div>
            <div class="block-content">
                <div class="table-line" style="width: 550px !important; margin-bottom: 0">
                    <div class="line row">
                        <div class="line-content col-md-12 col-sm-12 col-xs-12">
                            <span style="display: inline-block; width: 400px; line-height: 12px; height: 12px">Họ và tên: {{ !empty($objMember->fullName) ? htmlentities($objMember->fullName) : '' }}</span>
                             <span style="display: inline-block; width: 400px; line-height: 12px; height: 12px">Giới tính: {{ ($objMember->gender == 1) ? 'Nam' : 'Nữ' }} </span> 
                             <span>Số thẻ hội viên: {{ $objMember->member_code }}</span>
                        </div>
                    </div>
                    <div class="line row">
                        <div class="line-content col-md-12 col-sm-12 col-xs-12">
                            <span style="display: inline-block; width: 400px; line-height: 12px; height: 12px">Hướng dẫn viên: {{ !empty($typeOfTravelGuide) ? array_get($member_typeOfTravelGuide, $typeOfTravelGuide, '') : '' }}
                            </span>
                            <span>Inbound / Outbound:</span>
                                @if ($objMember->inboundOutbound == 1 && $typeOfTravelGuide == 1)
                                    <span style="font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;color: #3D3A3A;">HDV Du Lịch Quốc Tế Inbound</span>
                                @elseif ($objMember->inboundOutbound == 2 && $typeOfTravelGuide == 1)
                                    <span style="font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;color: #3D3A3A;">HDV Du Lịch Quốc Tế Outbound</span>
                                @elseif ($objMember->inboundOutbound == 3 && $typeOfTravelGuide == 1)
                                    <span style="font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;color: #3D3A3A;">HDV Du Lịch Quốc Tế Inbound - Outbound</span>
                                @endif
                        </div>
                    </div>
                    <div class="line row">
                        <div class="line-content col-md-12 col-sm-12 col-xs-12">
                            <span style="display: inline-block; width: 400px; line-height: 12px; height: 12px">Số thẻ HDV du lịch: {{ $objMember->touristGuideCode }}</span>
                        @if($typeOfTravelGuide == 1 || $typeOfTravelGuide == 2)
                        <span>
                            Ngày hết hạn: {{ empty($objMember->expirationDate) ? '' : date('d/m/Y', strtotime($objMember->expirationDate)) }}</span>
                        @endif
                        </div>                        
                    </div>
                    <div class="line row">
                        <div class="line-content col-md-12 col-sm-12 col-xs-12">
                            <span style="display: inline-block; width: 400px; line-height: 12px; height: 12px">Địa bàn hoạt động:
                            {{ $objMember->province ? array_get($provincial, $objMember->province, '') : ''}}</span>
                        
                        <span>Sinh hoạt tại:
                            {{ $typeOfPlace == 1 ? 'Chi hội ' . array_get($branch_chihoi, $objMember->province_code, '') : '' }}
                            {{ $typeOfPlace == 2 ? 'CLB thuộc hội ' . array_get($branch_clbthuochoi, $objMember->province_code, '') : '' }}</span>
                        </div>                        
                    </div>
                    <div class="line row">
                        <div class="line-content col-md-12 col-sm-12 col-xs-12">
                            <span style="display: inline-block; width: 400px; line-height: 12px; height: 12px">Điện thoại di động 1: {{ empty($objMember->firstMobile) ? '' : $objMember->firstMobile }}</span>
                            <span>Điện thoại di động 2: {{ empty($objMember->secondMobile) ? '' : htmlentities($objMember->secondMobile) }}</span>
                        </div>
                    </div>
                    <div class="line row">
                        <div class="line-content col-md-12 col-sm-12 col-xs-12">
                            <span style="display: inline-block; width: 300px; line-height: 12px; height: 12px">Email 1: {{ empty($objMember->firstEmail) ? '' : $objMember->firstEmail }}</span>
                        </div>
                    </div>
                </div>
                @if (!empty($profileImg))
                    @if($isProfile)
                        <div class="photo" style="float: right; position: absolute;right: 0; top: 36px;">
                    @else 
                        <div class="photo" style="float: right;">
                    @endif
                            <img src="{{ $profileImg }}" style="width: 150px;" height="180"/>
                        </div>
                @else
                    <div class="photo" style="float: right;">
                        <img src="{{ asset('images/3_4.jpg') }}" style="width: 150px;" height="180" />
                    </div>
                @endif
            </div>
        </div>

        <div class="block">
            <div class="block-content">
        <div id="coached" class="flat-section coached" data-scroll-index="6">
            <div class="section-content">
                <div class="container not-partner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cta-title-2">
                                <h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Thông tin công ty lữ hành</h1>
                            </div>
                            <div>
                                <div class="contentDetail">
                                    <div class="table-responsive add-front">
                                        <form >
                                            
                                            <table id="table2" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2">Thông tin công ty lữ hành</th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;">Tên công ty</td>
                                                        <td>{{ $travelerCompany->company_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Địa chỉ</td>
                                                        <td>{{ $travelerCompany->address }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Số điện thoại</td>
                                                        <td>{{ $travelerCompany->phone }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <td>{{ $travelerCompany->email }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mã số thuế </td>
                                                        <td>{{ $travelerCompany->tax_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Số giấy phép kinh doanh lữ hành quốc tế (đối với công ty kinh doanh lữ hành quốc tế)</td>
                                                        <td>{{ $travelerCompany->business_certificate }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table id="table3" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Thông tin đại diện công ty lữ hành đánh giá</h1></th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;vertical-align: middle;">Tên người cho điểm<span class="color-required">*</span></td>
                                                        <td>
                                                            {{ $memberEvaluation->name }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">Chức danh người cho điểm<span class="color-required">*</span></td>
                                                        <td>
                                                            {{ $memberEvaluation->position }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">ĐT di động người cho điểm<span class="color-required">*</span></td>
                                                        <td>
                                                            {{ $memberEvaluation->phone }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table id="table4" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Thông tin tour HDV đã hoàn thành hướng dẫn</h1></th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;vertical-align: middle;">Mã đoàn</td>
                                                        <td>
                                                            {{ $memberEvaluation->tourist_code }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">Tour tuyến</td>
                                                        <td>
                                                            {{ $memberEvaluation->forte_tour }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">Số lượng khách</td>
                                                        <td>
                                                            {{ $memberEvaluation->number_of_tourist }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">Thời gian (từ ngày/tháng/năm - đến ngày/tháng/năm)<span class="color-required">*</span></td>
                                                        <td style="display: inline-block; width: 100%;">
                                                            {{ date('d/m/Y', strtotime($memberEvaluation->from_date)) }}
                                                            -
                                                            {{ date('d/m/Y', strtotime($memberEvaluation->to_date)) }}
                                                           
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table id="table6" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Nội dung chấm điểm</h1></th>
                                                    </tr>
                                                    <?php $incre = 1; $score = 0;?>
                                                    @foreach ($criteria as $key => $value)
                                                        <tr>
                                                            <td style="width: 50%;vertical-align: middle;">{{ $value }}</td>
                                                            <td>
                                                                @if ($key == $memberEvaluation->criteria1_code)
                                                                    {{ $memberEvaluation->criteria1 }}
                                                                @elseif ($key == $memberEvaluation->criteria2_code)
                                                                    {{ $memberEvaluation->criteria2 }}
                                                                @elseif ($key == $memberEvaluation->criteria3_code)
                                                                    {{ $memberEvaluation->criteria3 }}
                                                                @elseif ($key == $memberEvaluation->criteria4_code)
                                                                    {{ $memberEvaluation->criteria4 }}
                                                                @elseif ($key == $memberEvaluation->criteria5_code)
                                                                    {{ $memberEvaluation->criteria5 }}
                                                                @endif
                                                                điểm (Trọng số {{ $percentage[$key] }}%)
                                                            </td>
                                                        </tr>
                                                    <?php $incre++; ?>
                                                    @endforeach
                                                        <tr>
                                                            <td style="vertical-align: middle;    padding-left: 0px;font-size: 20px;font-weight: 700;">Tổng điểm</td>
                                                            <td style="display: inline-block; width: 100%;">
                                                                @foreach ($percentage as $key => $value)
                                                                    @if ($key == $memberEvaluation->criteria1_code)
                                                                        <?php $score += $memberEvaluation->criteria1 * $value / 100; ?>
                                                                    @elseif ($key == $memberEvaluation->criteria2_code)
                                                                        <?php $score += $memberEvaluation->criteria2 * $value / 100; ?>
                                                                    @elseif ($key == $memberEvaluation->criteria3_code)
                                                                        <?php $score += $memberEvaluation->criteria3 * $value / 100; ?>
                                                                    @elseif ($key == $memberEvaluation->criteria4_code)
                                                                        <?php $score += $memberEvaluation->criteria4 * $value / 100; ?>
                                                                    @elseif ($key == $memberEvaluation->criteria5_code)
                                                                        <?php $score += $memberEvaluation->criteria5 * $value / 100; ?>
                                                                    @else
                                                                    @endif
                                                                @endforeach
                                                                {{ round($score, 2) }} điểm
                                                            </td>
                                                        </tr>
                                                </tbody>
                                            </table>
                                            
                                        </form>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection
