@extends('layouts.newdesign.public')
@section('title', 'Đánh giá, cho điểm HDV của công ty du lịch lữ hành')
@section('content')
    @include('layouts.newdesign.banner')

    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cta-title-2">
                            <h1 class="darkblue">Đánh giá, cho điểm HDV của công ty du lịch lữ hành</h1>
                        </div>
                        <div>
                            <div class="contentDetail">
                                <div class="table-responsive add-front">
                                    @if(session('successes'))
                                        <div id="closeButton" class="alert alert-success">
                                            <button type="button" class="close" aria-label="Close" style="width: 0%; height: 0px;">
                                                <span aria-hidden="true" style="float: right;">&times;</span>
                                            </button>
                                            <ul>
                                                @foreach (session('successes') as $success)
                                                    <li>{{ $success }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if ($errors->any())
                                        <div id="closeButton" class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form id="evaluation-form" method="POST" action="{{ route('company_create_evaluation.store') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="member_id" value="{{ $member_id }}">
                                        <input type="hidden" name="taxcode_company" value="{{ $travelerCompany->tax_code }}">
                                        <table id="table1" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                         <th style="padding-left: 0px; vertical-align: middle;"><h1 style="font-size: 14px; font-weight: 700;">Thông tin HDV</th>
                                                         <th>
                                                            @if (!empty($profileImg))
                                                                @if($isProfile)
                                                                    <div class="photo" style="position: absolute;right: 0; top: 36px;">
                                                                @else 
                                                                    <div class="photo">
                                                                @endif
                                                                        <img src="{{ $profileImg }}" style="width: 100px;" height="150"/>
                                                                    </div>
                                                            @else
                                                                <div class="photo">
                                                                    <img src="{{ asset('images/3_4.jpg') }}" style="width: 100px;" height="150" />
                                                                </div>
                                                            @endif
                                                         </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;">Họ và tên</td>
                                                        <td>{{ !empty($member->fullName) ? htmlentities($member->fullName) : '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hướng dẫn viên</td>
                                                        <td>{{ !empty($typeOfTravelGuide) ? array_get($member_typeOfTravelGuide, $typeOfTravelGuide, '') : '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Số thẻ HDV du lịch</td>
                                                        <td>{{ $member->touristGuideCode }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ngày hết hạn</td>
                                                        <td>
                                                        @if($typeOfTravelGuide == 1 || $typeOfTravelGuide == 2)
                                                          {{ empty($member->expirationDate) ? '' : date('d/m/Y', strtotime($member->expirationDate)) }}
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Địa bàn hoạt động</td>
                                                        <td> {{ $member->province ? array_get($provincial, $member->province, '') : ''}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;">Giới tính</td>
                                                        <td>{{ ($member->gender == 1) ? 'Nam' : 'Nữ' }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <table id="table2" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                            <tbody>
                                                <tr>
                                                    <th colspan="2">Thông tin công ty lữ hành</th>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;">Tên công ty</td>
                                                    <td>{{ $travelerCompany->company_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Địa chỉ</td>
                                                    <td>{{ $travelerCompany->address }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Số điện thoại</td>
                                                    <td>{{ $travelerCompany->phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $travelerCompany->email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Mã số thuế </td>
                                                    <td>{{ $travelerCompany->tax_code }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Số giấy phép kinh doanh lữ hành quốc tế (đối với công ty kinh doanh lữ hành quốc tế)</td>
                                                    <td>{{ $travelerCompany->business_certificate }}</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table id="table3" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                            <tbody>
                                                <tr>
                                                    <th colspan="2">Thông tin đại diện công ty lữ hành đánh giá</th>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;vertical-align: middle;">Tên người cho điểm<span class="color-required">*</span></td>
                                                    <td>
                                                        <input type="text" id="first" name="name" value="{{ old('name') }}" required="required" tabindex="1">
                                                        @if ($errors->has('name'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">Chức danh người cho điểm<span class="color-required">*</span></td>
                                                    <td>
                                                        <select name="position" style="padding: 0px 10px;" required="required" tabindex="2">
                                                            <option value="">Vui lòng chọn chức danh</option>
                                                            @php $oldPos = old('position'); @endphp
                                                            @foreach ($position as $pos => $val)
                                                                @if ($pos == $oldPos)
                                                                    <option value="{{ $pos }}" selected="selected">{{ $val }}</option>
                                                                @else
                                                                    <option value="{{ $pos }}">{{ $val }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('position'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('position') }}</strong>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">ĐT di động người cho điểm<span class="color-required">*</span></td>
                                                    <td>
                                                        <input type="text" name="phone" value="{{ old('phone') }}" required="required" tabindex="3">
                                                        @if ($errors->has('phone'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('phone') }}</strong>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table id="table4" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                            <tbody>
                                                <tr>
                                                    <th colspan="2">Thông tin tour HDV đã hoàn thành hướng dẫn</th>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;vertical-align: middle;">Mã đoàn<span class="color-required">*</span></td>
                                                    <td>
                                                        <input type="text" name="tourist_code" value="{{ old('tourist_code') }}" required="required" tabindex="4">
                                                        @if ($errors->has('tourist_code'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('tourist_code') }}</strong>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">Tour tuyến<span class="color-required">*</span></td>
                                                    <td>
                                                        <input type="text" name="forte_tour" value="{{ old('forte_tour') }}" required="required" tabindex="5">
                                                        @if ($errors->has('forte_tour'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('forte_tour') }}</strong>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">Số lượng khách<span class="color-required">*</span></td>
                                                    <td>
                                                        <input type="text" name="number_of_tourist" value="{{ old('number_of_tourist') }}" required="required" tabindex="6">
                                                        @if ($errors->has('number_of_tourist'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('number_of_tourist') }}</strong>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle;">Thời gian (từ ngày/tháng/năm - đến ngày/tháng/năm)<span class="color-required">*</span></td>
                                                    <td style="display: inline-block; width: 100%;">
                                                        <input type="text" style="width: 30% !important;" class="date datetime-input" name="from_date" id="start_time" value="{{ old('from_date') }}" required="required" tabindex="7">
                                                        <input type="text" id="end_time" style="width: 30% !important; margin-left: 50px;" class="date datetime-input" name="to_date" value="{{ old('to_date') }}" required="required" tabindex="8">
                                                        @if ($errors->has('from_date'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('from_date') }}</strong>
                                                            </span>
                                                        @endif
                                                        @if ($errors->has('to_date'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('to_date') }}</strong>
                                                            </span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table id="table5" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                            <tbody>
                                                <tr>
                                                    <th colspan="2">Thang điểm</th>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;font-weight: bold;">Điểm</td>
                                                    <td style="font-weight: bold;">Ghi chú</td>
                                                </tr>
                                                <tr>
                                                    <td>Từ 0 đến 30 điểm</td>
                                                    <td>Kỹ năng kém</td>
                                                </tr>
                                                <tr>
                                                    <td>Từ 31 đến 60 điểm</td>
                                                    <td>Kỹ năng trung bình</td>
                                                </tr>
                                                <tr>
                                                    <td>Từ 61 đến 75 điểm</td>
                                                    <td>Kỹ năng tốt</td>
                                                </tr>
                                                <tr>
                                                    <td>Từ 76 đến 90 điểm</td>
                                                    <td>Kỹ năng rất tốt</td>
                                                </tr>
                                                <tr>
                                                    <td>Từ 91 đến 100 điểm</td>
                                                    <td>Kỹ năng xuất sắc</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table id="table6" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                            <tbody>
                                                <tr>
                                                    <th colspan="2">Nội dung chấm điểm</th>
                                                </tr>
                                                <?php $incre = 1; $n = 9; ?>
                                                @foreach ($criteria as $key => $value)
                                                    <tr>
                                                        <td style="width: 50%;vertical-align: middle;">{{ $value }}<span class="color-required">*</span></td>
                                                        <td>
                                                            <span id="textRangeInput{{$incre}}" style="font-weight: bold;">{{ old('criteria' . $incre) ?? 50 }}</span><span style="font-weight: bold;"> điểm</span>
                                                            <input type="range" name="criteria{{$incre}}" value="{{ old('criteria' . $incre) }}" min="0" max="100" onchange="updateTextInput(this.value, 'textRangeInput{{ $incre }}');" required="required" tabindex="{{ $n }}" style="padding-right: 0px; width: 50% !important;">
                                                            <input type="hidden" name="criteria{{$incre}}_code" value="{{ $key }}">
                                                            @if ($errors->has('criteria'.$incre))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('criteria'.$incre) }}</strong>
                                                                </span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                <?php $incre++; $n++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <div style="text-align: center;"> 
                                            <a onclick="openModal()" href="javascript:void(0);" class="btn btn-primary" id="submitBtn" tabindex="14" style="border-color: #fff;padding: 10px 35px !important;">Gửi đánh giá</a>
                                            <!-- <a href="{{ URL::route('company_evaluate') }}" class="btn btn-warning" style="border-color: #fff;padding: 10px 35px !important;">Thoát</a> -->
                                        </div>
                                    </form>
                                </div>
                                <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.member_evaluation.confirm')
@endsection
<style>
    input[type='text'], select {
        width: 70% !important;
    }

    .color-required{
        color: red
    }

    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .has-error .control-label {
        color: #3D3A3A !important
    }
</style>
<script>
    function submitForm() {
        jQuery('#submitBtn').prop('disabled', true);
        jQuery('#evaluation-form').submit();
        return false;
    }

    function updateTextInput(val, att) {
        document.getElementById(att).innerHTML = val;
    }

    function openModal() {
        document.getElementById('memberEvaluationTouristCode').innerHTML = document.querySelector('input[name=tourist_code]').value;
        document.getElementById('memberEvaluationForteTour').innerHTML = document.querySelector('input[name=forte_tour]').value;
        document.getElementById('memberEvaluationNumberOfTourist').innerHTML = document.querySelector('input[name=number_of_tourist]').value;
        document.getElementById('memberEvaluationFromDate').innerHTML = document.querySelector('input[name=from_date]').value;
        document.getElementById('memberEvaluationToDate').innerHTML = document.querySelector('input[name=to_date]').value;
        document.getElementById('scoreCriteria1').innerHTML = document.querySelector('input[name=criteria1]').value;
        document.getElementById('scoreCriteria2').innerHTML = document.querySelector('input[name=criteria2]').value;
        document.getElementById('scoreCriteria3').innerHTML = document.querySelector('input[name=criteria3]').value;
        document.getElementById('scoreCriteria4').innerHTML = document.querySelector('input[name=criteria4]').value;
        document.getElementById('scoreCriteria5').innerHTML = document.querySelector('input[name=criteria5]').value;
        jQuery('#confirmEvaluation').modal();
    }
</script>
