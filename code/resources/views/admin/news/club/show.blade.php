@extends('admin.layouts.app')
@section('title', 'Chi tiết Tin Tưc và Sự Kiện CLB thuộc Hội - ' . $detail->title)
@section('content')
<style>
        .color-required{
            color: red
        }
        .error_border {
            border-color: #a94442;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        }
        .help-block strong {
            color: red
        }

        .expire_date{
            margin-bottom: 10px;
            padding-top: 7px;
            font-weight: bold;
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
        .col-md-6{
            padding-top: 7px
        }
        .col-md-1{
            width: 10%;
        }
    .detail-content {
        padding-top: 10px;
        max-height: 600px;
        overflow-y: scroll;
    }
    </style>
<div class="page-wrap">
    <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
        <div class="panel-heading">Chi tiết Tin Tức và Sự Kiện CLB thuộc Hội - {{$detail->title}}</div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="" id="updateForm">
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-2 control-label" style="padding-top: 0px;">CLB thuộc hội:</label>

                    <div class="col-md-10">
                        {{ isset($listClub[$detail->branch_id]) ? $listClub[$detail->branch_id] : ''  }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-2 control-label" style="padding-top: 0px;">Tiêu đề:</label>

                    <div class="col-md-10">
                        {{ htmlentities($detail->title)  }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="start_time" class="col-md-2 control-label">Phát hành từ ngày:</label>

                    <div class="col-md-6">
                        <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" id="errorDate" style="margin-bottom: 10px;padding-left: 0px !important;">
                            {{$detail->start_time}}
                        </div>
                        <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style=" padding-right: 0px !important; font-weight: bold">
                            Đến ngày:
                        </div>
                        <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" style=" padding-left: 0px !important;">
                            {{$detail->end_time}}
                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label for="" class="col-md-2 control-label">Trạng thái:</label>

                    <div class="col-md-6">
                        {{$detail->status == 0 ?  'Lưu nháp' : 'Hoạt động'}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="thumnail_image" class="col-md-2 control-label">Hình ảnh tóm tắt:</label>
                    <div class="col-md-10" style="padding-top: 10px">
                        <div id="image-preview" style="margin-top: 10px;float: left;">
                            @if (!empty($detail->thumnail_image))
                            <img src="{{ url('/') . '/images/news/'. $detail->thumnail_image }}" width="100" height="80"/>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                    <label for="short_description" class="col-md-2 control-label">Nội dung tóm tắt:</label>

                    <div class="col-md-10" style="padding-top: 10px">
                        {{ htmlentities($detail->short_description)  }}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label for="content" class="col-md-2 control-label">Nội dung đầy đủ:</label>
                    <div class="col-md-10 detail-content">
                        {!! ($detail->content) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-5">
                        <a href="{{ URL::route('admin.news_clb.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="10">Thoát</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
