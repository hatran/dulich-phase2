<form class="page-wrap" name="" method="get" id="formSearch">
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tiêu đề bài viết
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="search_title" placeholder="Tiêu đề bài viết" type="text" name="search_title" value="{{ Input::get('search_title', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Ngày phát hành
        </div>
        <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input tabindex="2" class="date datetime-input" type="text" name="search_start_time" id="search_start_time"
                   value="{{ Input::get('search_start_time', '') }}" placeholder="Từ ngày">
            <span class="datetime-icon fa fa-calendar"></span>
        </div>
        <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input tabindex="3" class="date datetime-input" type="text" name="search_end_time" id="search_end_time"
                   value="{{ Input::get('search_end_time', '') }}" placeholder="Đến ngày">
            <span class="datetime-icon fa fa-calendar"></span>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="4" id="search_status" name="search_status" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($statusOption))
                    @foreach($statusOption as $key => $val)
                        @php $selected = is_numeric(Input::get('search_status', '')) && Input::get('search_status', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-6"></div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Ngày tạo
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input tabindex="5" class="date datetime-input" type="text" name="search_created_at" id="search_created_at"
                   value="{{ Input::get('search_created_at', '') }}" placeholder="Ngày tạo">
            <span class="datetime-icon fa fa-calendar"></span>
        </div>
        @if (empty(auth()->user()->province_type))
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Chi hội
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select tabindex="6" id="search_branch_id" name="search_branch_id" class="form-control">
                    <option value="">Tất cả</option>
                    @if (!empty($listBranches))
                        @foreach($listBranches as $key => $val)
                            @php $selected = is_numeric(Input::get('search_branch_id', '')) && Input::get('search_branch_id', '') == $key ? 'selected="selected"' : ''; @endphp
                            <option {{$selected}} value="{{$key}}">{{$val}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        @endif
        {{ csrf_field() }}
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="6" type="submit" onclick="return submitForm();" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm tin tức</h4>
            </div>
            <div class="modal-body">
                <h4>Phát hành từ ngày phải nhỏ hơn ngày kết thúc</h4>
            </div>
            <div class="modal-footer">                   
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    Thoát
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    function parseDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    $(document).keypress(function (e) {
        if ($('#formSearch input:focus, #formSearch button:focus').length != 0) {
            if (e.which === 13) {
                submitForm();
            }
        }
    });
    $(function () {
        $("#search_title").putCursorAtEnd().focus();
    });
    function submitForm() {
        startDate = parseDate($('#search_start_time').val());
        endDate = parseDate($('#search_end_time').val());
        if(startDate != '' && endDate != '' && (startDate > endDate)) {
            $("#showAlert").modal('show');
        } else {
            $('#formSearch').submit();
        }
        return false;
    }
</script>