@extends('admin.layouts.app')
@section('title', 'Danh sách Tin Tức và Sự Kiện Chi Hội')
@section('content')
<style>
    .error_border {-webkit-box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);}
    .total {line-height: 30px;padding: 6px 10px;color: #0000F0 !important;border-radius: 3px;font-size: 16px;border: 0;display: inline-block;}
    .form-horizontal .control-label {padding-top: 7px;margin-bottom: 0;text-align: left;}
    .fa{cursor: pointer}
    .fa-2x {font-size: 1.2em;}
    .table-wrap table tr td {position: relative;word-wrap: break-word;white-space: normal !important;}
    .table-wrap table span {font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;}
    .tooltip-inner {
        max-width: 350px;
        width: 350px;
        position: relative;
        word-wrap: break-word;
        white-space: normal !important;
        z-index: 999999;
    }
    .rq-star {color: #ff0000;}
    .btn-primary.btn-custom{
        color: #fff;
        line-height: 21px;
        height: 35px;
        line-height: 35px;
        border: 1px solid #BBBABA;
        padding: 0 10px;
        max-width: 100%;
        display: block;
        border-radius: 2px;
    }
    .modal-title {
        margin: 0;
        font-weight: bold;
        float: left;
        font-size: 20px;
    }
</style>
<div class="main-content">
    @include('admin.news.branches.search')
    <div class="clearfix"></div>
    <div class="page-wrap">
        <!-- <div class="heading">Kết quả tìm kiếm</div> -->
        @include('admin.layouts.message')
        <div class="total">Tổng số: {{!empty($listData) ? $listData->total() : 0}}</div>
        <div class="field-wrap" style="display:inline-block;float: right">
            <a tabindex="6" class="btn-custom button btn-primary" href="{{ URL::route('admin.news_ch.create') }}">Thêm mới</a>
        </div>
        <div class="table-wrap">
            <table>
                <thead>
                    <tr>
                        <td width="3%">STT</td>
                        <td width="13%">Tiêu đề</td>
                        <td width="20%">Nội dung tóm tắt</td>
                        <td width="10%">Chi hội</td>
                        <td width="5%">Phát hành từ ngày</td>
                        <td width="5%">Đến ngày</td>
                        <td width="5%">Ngày tạo</td>
                        <td width="10%">Người đăng tin</td>
                        <td width="5%">Ngày sửa</td>
                        <td width="10%">Người sửa tin</td>
                        <td width="7%">Trạng thái</td>
                        <td width="7%">Chức năng</td>
                    </tr>
                </thead>
                <tbody>
                    @if(empty($listData))
                    <tr>
                        <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                    </tr>
                    @else
                    @foreach($listData as $key => $item)
                    <tr>
                        <td class="text-center"> {{$offsets + $key + 1 }} </td>
                        <td>{{ htmlentities($item->title) }}</td>
                        <td>{{ htmlentities($item->short_description) }}</td>
                        <td>{{ isset($listBranches[$item->branch_id]) ? $listBranches[$item->branch_id] : ''  }}</td>
                        <td class="text-center">{{ $item->start_time }}</td>
                        <td class="text-center">{{ $item->end_time }}</td>
                        <td class="text-center">{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                        <td class="text-center">{{ $item->created_author }}</td>
                        <td class="text-center">{{ date('d/m/Y', strtotime($item->updated_at)) }}</td>
                        <td class="text-center">{{ $item->updated_author }}</td>
                        <td>{{$item::mappingStatus($item->status)}}</td>
                        <td class="text-center">
                            <a title="Xem tin tức" style="padding-right:5px" href="{{ route('admin.news_ch.show',['id' => $item->id]) }}">
                                <i class="fa fa-eye fa-2x" aria-hidden="true" style="font-size: 18px; color: #009933;"></i>
                            </a>
                            @if ($item->created_author === $user->username || $user->role == $leaderManagerUser) 
                            <a title="Sửa tin tức" href="{{ route('admin.news_ch.edit',['id' => $item->id]) }}">
                                <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true" style="font-size: 18px; color: #ff6600;"></i>
                            </a>
                            <a title="Xóa tin tức" data-title="{{ htmlentities($item->title) }}" href="javascript:;" data-href="{{ URL::route('admin.news_ch.destroy', [$item->id]) }}" style="padding-left: 10px" onclick="showDeleteModal($(this))">
                                <i class="fa fa-trash-o fa-2x" aria-hidden="true" style="font-size: 18px;"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @include('admin.layouts.pagination')
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Xóa Tin Tức và Sự Kiện Chi Hội</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Bạn chắc chắn muốn xóa <span id="id-em" style="font-size:  20px;color:  blue;"></span> ra khỏi danh sách?</h4>
                    <br/>
                    <form id="delete_info" action="" method="post">
                        {{ method_field('post') }}
                        {{ csrf_field() }}
                    </form>
                </div>
                <input type="hidden" id="id_delete" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="delete_item();">Xóa</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Thoát</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_embed')
<script>
    function showDeleteModal(__this) {
        var dataURL = __this.attr('data-href');
        console.log(__this.attr('data-title'));
        $('#id-em').text(__this.attr('data-title'));
        $('#delete_info').attr('action', dataURL);
        $('#deleteModal').modal({show: true});
        return false;
    }
    function delete_item() {
        $('#delete_info').submit();
    }
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endsection