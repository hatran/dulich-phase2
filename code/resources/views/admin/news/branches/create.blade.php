@extends('admin.layouts.app')
@section('title', 'Thêm mới Tin Tức và Sự Kiện Chi Hội')
@section('content')
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .expire_date{
        margin-bottom: 10px;
        padding-top: 7px;
        font-weight: bold;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }
    .col-md-1{
        width: 10%;
    }
    .has-error .control-label {
        color: #3D3A3A !important
    }
</style>
    <div class="page-wrap">
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Thêm mới Tin Tức và Sự Kiện Chi Hội</div>

            <div class="panel-body">
                <form id="infomation-form" enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{ URL::route('admin.news_ch.store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="option_code" value="{{$optionCode}}" />
                    <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}">
                        <label for="branch_id" class="col-md-2 text-right">Chi hội <span class="color-required">*</span></label>
                        <div class="col-md-4">
                            <select id="branch_id" class="form-control" name="branch_id" tabindex="1">
                                <option value="">Chọn Chi Hội</option>
                                @if (!empty($listBranches))
                                    @foreach($listBranches as $key => $val)
                                        <option value="{{ $key }}" {{ (old('branch_id') == $key) ? 'selected' : '' }}> {{ $val }} </option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('branch_id'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('branch_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-2 text-right">Tiêu đề <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}"  tabindex="2" maxlength="500" >

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="start_time" class="col-md-2 control-label">Phát hành từ ngày <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" id="errorDate" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <input type="text" style="" class="date datetime-input" name="start_time" value="{{ old('start_time') }}" id="start_time" placeholder="" tabindex="3">
                                <span class="datetime-icon fa fa-calendar"></span>
                                @if ($errors->has('start_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12 expire_date" style="">
                                Đến ngày
                            </div>
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" style="margin-bottom: 10px; padding-right: 0px !important; " >
                                <input type="text" style="" class="date datetime-input" name="end_time" value="{{ old('end_time') }}" id="end_time" placeholder="" tabindex="4">
                                <span class="datetime-icon fa fa-calendar" style="right: 10px !important;"></span>
                                @if ($errors->has('end_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="" class="col-md-2 control-label">Trạng thái <span class="color-required">*</span></label>

                        <div class="col-md-6" style="padding-top: 5px">
                            <label class="radio-inline" for="statushd" tabindex="5">
                                <input id="statushd" type="radio" name="status" value="1" {{ $statusDefault == 1 ? 'checked' : '' }} >
                                Hoạt động
                            </label>
                            <label class="radio-inline" for="statusln" tabindex="6" >
                                <input id="statusln" type="radio" name="status" value="0" {{ $statusDefault == 0 ? 'checked' : '' }}>
                                Lưu nháp
                            </label>
                            @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="thumnail_image" class="col-md-2 control-label">Hình ảnh tóm tắt <span class="color-required">*</span></label>
                        <div class="col-md-10" style="padding-top: 10px">
                            <input tabindex="6" onchange="return fileValidation(event);" type="file" class="form-control-file" name="thumnail_image" id="thumnail_image">
                            @if ($errors->has('thumnail_image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('thumnail_image') }}</strong>
                                </span>
                            @endif
                            <div id="image-preview" style="margin-top: 10px;float: left;"></div>
                        </div>
                      </div>
                    <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                        <label for="short_description" class="col-md-2 control-label">Nội dung tóm tắt <span class="color-required">*</span></label><div class="col-md-10" style="padding-top: 10px">
                            <textarea maxlength="500" tabindex="7" rows="5" id="short_description" name="short_description" class="form-control" style="resize: none;">{{ old('short_description') }}</textarea>
                            @if ($errors->has('short_description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('short_description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-2 control-label">Nội dung đầy đủ <span class="color-required">*</span></label>
                        <div class="col-md-10" style="padding-top: 10px">
                            <textarea tabindex="8" rows="15" id="content_rg" name="content" class="form-control position-content tinymce">{{ old('content') }}</textarea>
                            @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button type="button" class="btn btn-primary" id="submitBtn" onclick="return submitForm();" tabindex="9">Lưu</button>
                            <a href="{{ URL::route('admin.news_ch.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="10">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>
    // Enter to submit form registerForm
    document.body.addEventListener('keydown', function(e) {
        var key = e.which;
        if (key == 13) {
            $("#registerInfo").click();
        }
    });
    var flagValidate = true;
    function fileValidation(event) {
        event.preventDefault();
        var fileInput = event.target;
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        $(fileInput).parent().find('.help-block').remove();
        $('#image-preview').html('');
        if(!allowedExtensions.exec(filePath)){
            $(fileInput).parent().append('<span class="help-block"><strong>Ảnh đại diện phải là một tập tin có định dạng: jpeg, jpg, png, gif.</strong></span>');
            fileInput.value = '';
            return false;
        } else if (fileInput.files[0].size/1024/1024 > 10) {
            mess = 'Dung lượng ảnh đại diện quá lớn: ' + fileInput.files[0].size/1024/1024 + "MB";
            $(fileInput).parent().remove('.help-block').append('<span class="help-block"><strong>'+mess+'</strong></span>');
            fileInput.value = '';
            return false;
        } else {
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image-preview').html('<img src="'+e.target.result+'" width="100" height="80"/>');
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
            return false;
        }
        
        return false;
    }
    
    $(document).ready(function () {
        document.getElementById("branch_id").focus();
    });
    function submitForm() {
        $('#submitBtn').prop('disabled', true);
        $('#infomation-form').submit();
        return false;
    }
</script>
@endsection
