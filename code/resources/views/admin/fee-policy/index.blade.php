@extends('admin.layouts.app')
@section('title', 'Danh sách chính sách phí')

<style>
    .total {line-height: 30px;padding: 6px 10px;color: #0000F0 !important;border-radius: 3px;font-size: 16px;border: 0;display: inline-block;}
    .btn-primary.btn-custom{
        color: #fff;
        line-height: 21px;
        height: 35px;
        line-height: 35px;
        border: 1px solid #BBBABA;
        padding: 0 10px;
        max-width: 100%;
        display: block;
        border-radius: 2px;
    }
    .line.row {
        margin-bottom: 10px;
    }
    .line.row .col-md-3 {
        padding-top: 7.5px;
        padding-bottom: 7.5px;
    }
</style>
@section('content')
<link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('admin/select2/js/i18n/vi.js')}}"></script>
    <div class="main-content">
    @include('admin.layouts.message')
    @include('admin.fee-policy.search')
   <div class="heading">Kết quả tìm kiếm</div>
        <div class="main-content">

            <div class="total">Tổng số: {{ $objFeePolicy->total() }}</div>
            <div class="field-wrap" style="display:inline-block;float: right">
                <a tabindex="6" class="btn-custom button btn-primary" href="{{ url('/officesys/fee-policy/create') }}">Thêm mới</a>
            </div>
            <!-- <div class="field-wrap" style="display:inline-block;float: right; margin-right: 5px;">
                <a tabindex="6" class="btn-custom button btn-primary" href="{{ route('users_create_notmember') }}">Thêm mới (Không phải hội viên)</a>
            </div> -->
            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="2%">STT</td>
                        <td width="8%">Mã</td>
                        <td width="12%">Tên</td>
                        <td width="7%">Thời gian bắt đầu</td>
                        <td width="7%">Thời gian kết thúc</td>
                        <td width="8%">Mức ưu đãi</td>
                        <td width="8%">Phạm vi áp dụng</td>
                        <td width="10%">Đối tượng áp dụng</td>
                        <td width="12%">Chi Hội</td>
                        <td width="8%">Chức năng</td>
                        <td width="5%">Trạng thái</td>
                        <td width="15%">Ghi chú</td>
                        
                    </tr>
                    </thead>
                    <tbody>
                        @if (count($objFeePolicy) == 0)
                            <tr>
                                <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                            </tr>
                        @else
                            @foreach($objFeePolicy as $key => $objFeePolicies)
                                <tr>
                                    <td class="text-center"> {{ $current_cursor + $key + 1 }} </td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;">{{ $objFeePolicies->code }}</td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;">{{ $objFeePolicies->name }}</td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;"> {{ empty($objFeePolicies->start_date) ? '' : date('d/m/Y', strtotime($objFeePolicies->start_date)) }} </td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;"> {{ empty($objFeePolicies->end_date) ? '' : date('d/m/Y', strtotime($objFeePolicies->end_date)) }} </td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;"> {{ $objFeePolicies->money }} VNĐ</td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;">{{ $objFeePolicies->typeOp == 1 ? "Toàn quốc" : "Chi hội" }}</td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;">
                                        {{ $objFeePolicies->perfect_fee_policy_name }}
                                    </td>
                                    <td>{{ $objFeePolicies->list_name }}</td>
                                    <td class="text-center">
                                        <a class="btn-function add-modal"
                                        id="btnAddPayment"
                                        href="#"
                                        data-toggle="tooltip"
                                        data-placement="left"
                                        title="Cập nhập phạm vi áp dụng"
                                        onclick="showPopup({{$objFeePolicies->id }},{{$objFeePolicies->typeOp }},'{{$objFeePolicies->list_id }}','{{$objFeePolicies->perfect_fee_policy_code }}')"
                                        >
                                            <span class="glyphicon glyphicon-plus" style="font-size: 16px; color: #0000ff; "></span>
                                        </a>
                                        <a title="Cập nhật chính sách phí" href="{{ url('/officesys/fee-policy/' . $objFeePolicies->id) }}">
                                            <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                        </a>

                                        <a title="Xóa chính sách phí" href="javascript:void(0)" style="padding-left: 10px"
                                            onclick="delete_user({{ $objFeePolicies->id }})">
                                            <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                        </a>
                                    </td>
                                    <td>{{$objFeePolicies->status == 0 ? "Lưu nháp" : ($objFeePolicies->status == 1 ? "Đang áp dụng" : "Đã hết hạn")}}</td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;"> {{ $objFeePolicies->note }}</td>


                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <form id="delete_user" action="" method="post">
                    {{ csrf_field() }}
                </form>
            </div>
        @include('admin.layouts.pagination')
        </div>
    </div>
    @include('admin.user.modal.reset')
    @include('admin.fee-policy.modal.detail')
</div>
@endsection

@section('footer_embed')
<script>
    function delete_user(id) {
        if (confirm("Bạn có thực sự muốn xóa chính sách phí này")) {
            $('#delete_user').attr('action', '/officesys/fee-policy/' + id + '/delete');
            $('#delete_user').submit();
        } else {
            $("form").submit(function (e) {
                e.preventDefault();
            });
        }
    }

    function showPopup(id,typeOp,list_id,perfect_fee_policy_code) {
            var title, canEdit = false;
            var modalId = '#_showModal';

            $('.modal-title').text(title);

            $(modalId).modal('show');

           $("input[name=id_update]").val(id);
           $('input[name="typeOp"]').filter("[value='" + typeOp + "']").attr('checked', true);

           if(typeOp == 0 && list_id) {
                let arrId = list_id.split(",");
                for(let i = 0; i < arrId.length; i++) {
                    $('#province > option[value=' + arrId[i] + ']').attr("selected","selected");
                }
           }

           $('#province').select2();

            $("select[name=perfect_fee_policy_code]").val(perfect_fee_policy_code);

            changeTypeOp();
        }

    
   
</script>
@endsection
