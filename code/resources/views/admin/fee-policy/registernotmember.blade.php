@extends('admin.layouts.app')
@section('title', 'Tạo mới Người Dùng không phải hội viên')
@section('content')
    @include('admin.layouts.message')
    <div class="page-wrap">
		<link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
		<script src="{{asset('admin/select2/js/i18n/vi.js')}}"></script>
		<style>
			.color-required{
				color: red
			}
			.error_border {
				border-color: #a94442 !important;
				box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
			}
			.help-block strong {
				color: red
			}
			.has-error .control-label {
				color: #3D3A3A !important
			}
			.select2-search__field{
                width:100% !important;
            }
		</style>
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Tạo mới người dùng không phải hội viên</div>

            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('users_store_notmember') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                        <label for="fullname" class="col-md-4 text-right">Họ và tên <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="fullname" type="text" class="form-control" name="fullname" value="{{ old('fullname') }}"  autofocus>

                            @if ($errors->has('fullname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fullname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 text-right">Tên đăng nhập <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}"  autofocus>

                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('first_mobile') ? ' has-error' : '' }}">
                        <label for="first_mobile" class="col-md-4 text-right">Số điện thoại <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="first_mobile" type="text" class="form-control" name="first_mobile" value="{{ old('first_mobile') }}" >

                            @if ($errors->has('first_mobile'))
                                <span class="help-block">
                            <strong>{{ $errors->first('first_mobile') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 text-right">Email <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >

                            @if ($errors->has('email'))
                                <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 text-right">Mật khẩu <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="password" type="text" class="form-control" name="password" readonly="readonly">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-primary" id="gen-password" value="">Tạo mới</button>
                        </div>
                    </div>

                    <div class="group-branches">
                        <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-4 text-right">Chi Hội<span class="color-required">*</span></label>
                            <div class="col-md-6">
                                <select id="branch_id" class="form-control" name="branch_id">
                                    <option value="">Chọn chi hội</option>
                                    @if (!empty($listBranches))
                                        @foreach($listBranches as $key => $branches)
                                            <option value="{{ $key }}" {{ !empty(old('branch_id')) ? 'selected' : '' }}> {{ $branches }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('branch_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Tạo mới
                            </button>
                            <a href="{{ URL::route('users.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="10">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('footer_embed')
<script>
	$(document).ready(function(){
		$('#branch_id').select2({
			placeholder: "Chọn Chi Hội",
			width: '100%',
		});
	});

    $('#gen-password').click(function (e) {
        var length = 6,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.+*?[^]$(){}=!<>|:-",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.random() * n);
        }
        console.log($(this).parent().parent());
        $(this).parent().parent().children('.col-md-6').children('#password').val(retVal);
        e.preventDefault();
    })
</script>
@endsection
