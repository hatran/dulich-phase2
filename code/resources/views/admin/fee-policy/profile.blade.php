@extends('admin.layouts.app')
@section('title', 'Thông tin Người Dùng')
@section('content')
    <style>
        .list-group{
            width: 100%;
            box-shadow: none;
        }
        .list-group .list-group-item {
            border: none;
        }
    </style>
<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
    <div class="block-heading">Thông tin người dùng</div>
    <div class="block-content">
        <ul class="list-group">
            <li class="list-group-item">
                <span>Tên người dùng:</span> {{$user->fullname}}
            </li>
            <li class="list-group-item">
                <span>Tên đăng nhập:</span> {{$user->username}}
            </li>
            <li class="list-group-item">
                <span>Email:</span> {{$user->email}}
            </li>
            <li class="list-group-item">
                <span>Quyền truy cập:</span> {{array_get($user->group, 'name', '')}}
            </li>
        </ul>
    </div>
</div>
@endsection