@extends('admin.layouts.app')
@section('title', 'Chỉnh sửa thông tin Chính sách phí' . $objFeePolicy->name)
@section('content')
    <link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('admin/select2/js/i18n/vi.js')}}"></script>
    <script src="{{asset('admin/js/fee-policy.js')}}"></script>
    <style>
        .color-required{
            color: red
        }
        .error_border {
            border-color: #a94442 !important;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        }
        .help-block strong {
            color: red
        }
        .has-error .control-label {
            color: #3D3A3A !important
        }
        .select2-search__field{
            width:100% !important;
        }
        .d-flex {
            display: flex;
            gap: 30px;
        }
        
    </style>
    <div class="page-wrap">
        <style>
            #closeButton {margin-top: 20px}
            .select2-search__field{
                width:100% !important;
            }
        </style>
        @include('admin.layouts.message')
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Thông tin chi tiết hội phí: {{ $objFeePolicy->name }}</div>

            <div class="panel-body">
                <form id="userForm" class="form-horizontal" method="post" action="{{ route('fee_policy.update', [$objFeePolicy->id]) }}">
                    {{ csrf_field() }}
                    {{--{{ method_field('PUT') }}--}}
                    <input type="hidden" value="{{$objFeePolicy->list_id}}" name="list_id"/>

                     <!-- Mã chính Sách Start -->
                     <div id="form-code" class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                        <label for="code" class="col-md-4 text-right">Mã hội phí <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="code" type="hidden" class="form-control" name="code" value="{{ $objFeePolicy->code }}" maxlength="255" autofocus>
                            {{ $objFeePolicy->code }}
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <!-- Mã chính Sách End -->

                    <!-- Tên chính Sách Start -->
                    <div id="form-name" class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 text-right">Tên hội phí <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ $objFeePolicy->name }}" maxlength="255" autofocus>

                            <span class="help-block"></span>
                        </div>
                    </div>
                    <!-- Tên chính Sách End -->

                    <!-- Ngày bắt đầu Start -->
                    <div id="form-start-date" class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 text-right">Ngày bắt đầu <span class="color-required">*</span></label>

                        <div class="col-md-6">
                        <input class="date datetime-input position-date form-control" name="start_date" type="text" id="start_date" 
                        tabindex="202" maxlength="10" style="max-width: 230px" value="{{ $objFeePolicy->start_date }}">
                            <span class="errorDatePayment text-left error-msg"></span>

                            <span class="help-block"></span>
                        </div>
                    </div>
                    <!-- Ngày bắt đầu End -->

                    <!-- Ngày kết thúc Start -->
                    <div id="form-end-date" class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 text-right">Ngày kết thúc <span class="color-required">*</span></label>

                        <div class="col-md-6">
                        <input class="date datetime-input position-date form-control" name="end_date" type="text" id="end_date" 
                        tabindex="202" maxlength="10" style="max-width: 230px" value="{{ $objFeePolicy->end_date }}">
                            <span class="errorDatePayment text-left error-msg"></span>

                            <span class="help-block"></span>
                        </div>
                    </div>
                    <!-- Ngày kết thúc End -->

                    <!-- Mức ưu đãi Start -->
                    <div id="form-money" class="form-group{{ $errors->has('money') ? ' has-error' : '' }}">
                        <label for="money" class="col-md-4 text-right">Mức đóng hội phí <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <div style="display: flex; align-items: center;">
                                <input type="text"
                                    name="money"
                                    id="money"
                                    tabindex="204"
                                    style="max-width: 230px; width: 100%;"
                                    onkeypress="return isNumber()"
                                    class="position-currency form-control"
                                    maxlength="9"
                                    value="{{ $objFeePolicy->money }}"
                                > VNĐ
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <!-- Mức ưu đãi End -->

                    <!-- Phạm vi áp dụng Start -->
                    <div id="form-note" class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                        <label for="note" class="col-md-4 text-right">Phạp vi áp dụng </label>

                        <div class="col-md-6 d-flex">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="typeOp" type="radio" id="inlineRadio2" value="1"  {{ $objFeePolicy->typeOp ==  1 ? "checked" : ""}} />
                                <label class="form-check-label" for="inlineRadio2">Toàn quốc</label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="typeOp" type="radio" id="inlineRadio1" value="0"  {{ $objFeePolicy->typeOp ==  0 ? "checked" : ""}} />
                                <label class="form-check-label" for="inlineRadio1">Chi hội</label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <!-- Phạm vi áp dụng End -->

                    <!-- Danh sách chi hội Start -->
                     <div id="form-list-police" class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                        <label for="note" class="col-md-4 text-right">Chi hội </label>

                        <div class="col-md-6 d-flex">
                        <select id="province" name="province" multiple="multiple" class="form-control">
                            @foreach($offices as $key => $office)
                                <option value="{{$office->id}}" >{{$office->name}}</option>
                            @endforeach
                        </select>
                            <span class="help-block"></span>
                            <input type="hidden" name="list_id">
                        <input type="hidden" name="list_name">
                        </div>
                    </div>
                    <!-- Danh sách chi hội End -->

                    <!-- Đối tượng áp dụng Start -->
                    <!-- <div id="form-option" class="form-group{{ $errors->has('perfect_fee_policy_code') ? ' has-error' : '' }}">
                        <label for="note" class="col-md-4 text-right">Đối tượng áp dụng </label>

                        <div class="col-md-6 d-flex">
                        <select id="perfect_fee_policy_code" name="perfect_fee_policy_code" class="form-control">
                            @foreach($option as $key => $option)
                                <option value="{{$option->code}}" {{ $objFeePolicy->perfect_fee_policy_code ==  $option->code ? "selected" : ""}}>{{$option->value}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="perfect_fee_policy_name">
                            <span class="help-block"></span>
                        </div>
                    </div> -->
                    <!-- Danh sách chi hội End -->

                    <!-- Trạng thái Start -->
                    <div id="form-status" class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="note" class="col-md-4 text-right">Trạng thái </label>

                        <div class="col-md-6 d-flex">
                        <select id="status" name="status" class="form-control">
                            <option value="0" {{ $objFeePolicy->status ==  0 ? "selected" : ""}}>{{$option->value}}>Lưu nháp</option>
                            <option value="1" {{ $objFeePolicy->status ==  1 ? "selected" : ""}}>Đang áp dụng</option>
                            <option value="2" {{ $objFeePolicy->status ==  2 ? "selected" : ""}}>Đã hét hạn</option>
                        </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <!-- Trạng tháii End -->

                    <!-- Ghi chú Start -->
                    <div id="form-note" class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                        <label for="note" class="col-md-4 text-right">Ghi chú </label>

                        <div class="col-md-6">
                            <textarea  id="note" class="form-control" name="note" rows="10" value="{{ $objFeePolicy->note }}" maxlength="5000"  autofocus></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            @if (auth()->user()->canSee(['fee_policy.update']))
                                <button class="btn btn-primary" type="button"  onclick="submitForm()">
                                    Lưu
                                </button>
                            @endif
                            <a href="{{ url('/officesys/fee-policy') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="8">Thoát</a>
                                {{--@if (auth()->user()->canSee(['fee_policy.destroy']))
                                <button class="btn btn-danger" onclick="delete_user()">
                                    Xóa
                                </button>
                            @endif--}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer_embed')
    <script>
    $(document).ready(function() {
        
        let list_id = $("input[name=list_id]").val();
        if(list_id) {
            let arrId = list_id.split(",");
            for(let i = 0; i < arrId.length; i++) {
                $('#province > option[value=' + arrId[i] + ']').attr("selected","selected");
            }
        }

        $('#province').select2({
            maximumSelectionLength: 1
        });

        changeTypeOp();

        $( "input[name=typeOp]" ).change(function() {
            changeTypeOp();
        });
    });

    function changeTypeOp() {
        let val = $("input[name=typeOp]:checked").val();
        if(val == 0) {
            $("#form-list-police").show();
        } else {
            $("#form-list-police").hide();
        }
    }
    function submitForm()  {
        if($("input[name=typeOp]:checked").val() == 0) {

            // Set data list_id and list_name
            let province_list_id = $('#province').val();
            $("input[name=list_id]").val(province_list_id.join(","));
            if(province_list_id) {
                let arr_province_name = [];
                for(let i = 0; i < province_list_id.length; i++) {
                    arr_province_name.push($('#province > option[value=' + province_list_id[i] + ']').html());
                }
                $("input[name=list_name]").val(arr_province_name.join(","));
            }         
        }
        //Set data perfect_fee_policy_name
        $("input[name=perfect_fee_policy_name]").val($('#perfect_fee_policy_code > option[value=' + $("#perfect_fee_policy_code").val() + ']').html());
        !validate("edit") && !isValidate && $(".form-horizontal").submit();
    }

        $( document ).ready(function() {
            convertStartDateInit();
            convertEndDateInit();
        });

        function convertStartDateInit() {
            const dateInit = "{{ $objFeePolicy->start_date }}";
            if(dateInit) {
                const dateInitArr = dateInit.split("-");
                const dateStr =  dateInitArr[2].split(" ")[0] + "/" + dateInitArr[1] + "/" + dateInitArr[0];
                $("input[name=start_date]").val(dateStr);
            }
        }

        function convertEndDateInit() {
            const dateInit = "{{ $objFeePolicy->end_date }}";
            if(dateInit) {
                const dateInitArr = dateInit.split("-");
                const dateStr = dateInitArr[2].split(" ")[0] + "/" + dateInitArr[1] + "/" + dateInitArr[0];
                $("input[name=end_date]").val(dateStr);
            }
        }

        function delete_user() {
            if(confirm("Bạn có thực sự muốn xóa user này")) {
                // $('input[name="_method"]').val('DELETE')
                $('#userForm').attr('action', '{{ route('users.destroy', [$objFeePolicy->id]) }}');
                document.forms[0].submit();
            } else {
                $("form").submit(function(e){
                    e.preventDefault();
                });
            }
        }

        function showPermission(__this) {
            if (__this.is(':checked')) {
                $('.group-permission').slideDown();
            } else {
                $('.group-permission').slideUp();
            }
        }
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            $('#branch_id').select2({
                placeholder: "Chọn Chi Hội",
                width: '100%',
            });
            $('#club_id').select2({
                width: '100%',
                placeholder: 'Chọn CLB'
            });
        });
        $('body').on('change', 'select[name="role"]', function() {
            if ($(this).val() == 4) {
                $('.group-branches').removeClass('hidden');
            } else {
                $('.group-branches').addClass('hidden');
            }
        });
    </script>
@endsection


