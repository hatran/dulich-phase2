<div id="_showModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="min-width: 600px;margin: 100px auto">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="table-line modal-body">
                <form  id="form-modal" method="POST" action="{{ route('fee_policy.storeupdate') }}">
                {{ csrf_field() }}
                {{--{{ method_field('PUT') }}--}}

                <input type="hidden" name="id_update" />
                <div class="line row">
                    <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                        <span class="font-all-popup-title">Phạm vi áp dụng</span>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-8 dialog-content d-flex">

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="typeOp" type="radio" id="inlineRadio2" value="1" checked />
                            <label class="form-check-label" for="inlineRadio2">Toàn quốc</label>
                        </div>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="typeOp" type="radio" id="inlineRadio1" value="0" />
                            <label class="form-check-label" for="inlineRadio1">Chi hội</label>
                        </div>
                    </div>

                </div>

				<div class="line row" id="form-list-police">
                    <div class="col-md-3 col-sm-3 col-xs-4 no-break field-wrap">
                        <span class="font-all-popup-title">Danh sách chi hội</span>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-8 dialog-content field-wrap">
                    <select id="province" name="province" multiple="multiple" class="form-control">
                            @foreach($offices as $key => $office)
                                <option value="{{$office->id}}">{{$office->name}}</option>
                            @endforeach
                        </select>
                            <span class="help-block"></span>
                            <input type="hidden" name="list_id">
                        <input type="hidden" name="list_name">
                    
                    </div>
                </div>

                <div class="line row">
                    <div class="col-md-3 col-sm-3 col-xs-4 no-break field-wrap">
                        <span class="font-all-popup-title">Đối tượng áp dụng</span>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-8 dialog-content field-wrap">
                    <select id="perfect_fee_policy_code" name="perfect_fee_policy_code" class="form-control">
                            @foreach($option as $key => $option)
                                <option value="{{$option->code}}">{{$option->value}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="perfect_fee_policy_name">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add" id="btnsubmit" tabindex="207" onclick="submitForm()">
                        <span class="glyphicon glyphicon-plus"></span> Lưu
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Thoát
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            $( "input[name=typeOp]" ).change(function() {
                                changeTypeOp();
                            });
                        });
                        function changeTypeOp() {
                            let val = $("input[name=typeOp]:checked").val();
                            if(val == 0) {
                                $("#form-list-police").show();
                            } else {
                                $("#form-list-police").hide();
                            }
                        }
                        function submitForm() {

                            if($("input[name=typeOp]:checked").val() == 0) {

                                // Set data list_id and list_name
                                let province_list_id = $('#province').val();
                                $("input[name=list_id]").val(province_list_id.join(","));
                                if(province_list_id) {
                                    let arr_province_name = [];
                                    for(let i = 0; i < province_list_id.length; i++) {
                                        arr_province_name.push($('#province > option[value=' + province_list_id[i] + ']').html());
                                    }
                                    $("input[name=list_name]").val(arr_province_name.join(","));
                                }         
                            }
                            //Set data perfect_fee_policy_name
                            $("input[name=perfect_fee_policy_name]").val($('#perfect_fee_policy_code > option[value=' + $("#perfect_fee_policy_code").val() + ']').html());

                           $("#form-modal").submit();
                        }
                    </script>
<style>
    .table_payment {
        width: 100%;
    }

    .table_payment thead tr {
        background-color: #EDF1F6;
        text-align: center;
    }

    .table_payment thead tr td {
        color: #144a8b;
    }

    .table_payment tbody tr {
        text-align: center;
    }
    .d-flex {
        display: flex;
        gap: 15px;
    }
    .table-line.modal-body {
        padding: 15px 25px;
    }
    .btn-group, .multiselect , .multiselect-container.dropdown-menu,.multiselect.dropdown-toggle {
        width: 100%;
    }
    /* .multiselect.dropdown-toggle {
        position: relative;
    } */
    .multiselect.dropdown-toggle:after {
        display: none;
    }
    /* .multiselect.dropdown-toggle {
        margin-left: 0;
        position: absolute;
        right: 10px;
        top: 14px;
    } */
    .multiselect-container.dropdown-menu input {
        height: unset;
    }
    .multiselect-container.dropdown-menu {
        height: 400px;
        overflow: auto;
    }
    .multiselect-container>li>a>label {
        height: unset;
    }
    .select2.select2-container {
        width: 100% !important;
    }
</style>