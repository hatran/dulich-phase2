<style>
    @media (min-width: 768px) {
        .modal-dialog {
            width: 500px;
        }
    }
</style>

<!-- Modal -->
<div class="modal fade" id="createnewModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -35px">
                    <span aria-hidden="true"
                          style="font-size: 39px; width: 30%;float: right;text-align: right;padding-right: 10px;">&times;</span>
                </button>
            </div>
            <form id="resetPasswordUser" class="form-horizontal" role="form" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="container-fluid shadow">
                                <div class="row">
                                    <div id="valOk" class="row viewerror clearfix hidden">
                                        <div class="alert alert-success">Mật khẩu đã được reset!</div>
                                    </div>
                                    {{--Mật khẩu--}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="display: block;">
                                                <label class="control-label control-label-left col-sm-4"
                                                       for="field1">Mật khẩu mới<a
                                                            style="color: red">*</a></label>
                                                <div class="controls col-sm-5" style="margin-left: 5px;">
                                                    <div contenteditable="false" class="form-control" id="reset-password" data-pass=""></div>
                                                    <input type="hidden" name="input-password" id="input-password" value="">
                                                     <span id="error-password" class="error" style="color: red;">Mật khẩu mới chưa được tạo</span>
                                                </div>
                                                <div class="controls col-sm-2" style="margin-left: 5px;">
                                                    <button class="btn btn-primary" id="generate-password" value="">Tạo mới</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer edit">
                    <button type="submit" class="btn btn-primary" style="width: 60px; height: 30px"
                            id="submitCreate"><?= Lang::get('offices.save') ?></button>
                    <button type="button" class="btn btn-warning" style="width: 60px; height: 30px"
                            data-dismiss="modal"><?= Lang::get('offices.close') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    var inputValidate = true;
    jQuery('#resetPasswordUser').submit(function (e) {
        inputValidate = true;
        if (jQuery('#reset-password').text() == "") {
            inputValidate = false;
            jQuery('#reset-password').addClass('is-invalid');
            jQuery('.error').show();
        }
        else {
            jQuery('#reset-password').removeClass('is-invalid');
            jQuery('.error').hide();
        }
        
        if (inputValidate == false) e.preventDefault();
        jQuery("#resetPasswordUser button[type=submit]").attr("disabled", "disabled");

        if (inputValidate == true) jQuery('#resetPasswordUser').submit();
        jQuery("#resetPasswordUser button[type=submit]").prop('disabled', false);
    });

    function resetPassword(id) {
        jQuery('.error').hide();
        jQuery('.is-invalid').removeClass('is-invalid');
        jQuery("#exampleModalLabel").text("Cập nhật mật khẩu");
        jQuery("#exampleModalLabel").css("font-weight", "bold");
        jQuery("#reset-password").text('');
        jQuery("#reset-password").attr('data-pass', '');
        jQuery("#input-password").val('');
        jQuery("#valOk").attr('style', 'display: none !important');
        jQuery('.hidden').attr('style', 'display: none !important');
        jQuery('#resetPasswordUser').attr('action', '<?php echo '/officesys/users/reset-password/'?>' + id);
    }

    function printSuccessfulMsg() {
        jQuery("#valOk").attr('style', 'display: block !important');
        jQuery('.hidden').attr('style', 'display: block !important');
    }

    $('#generate-password').click(function (e) {
        jQuery('.error').hide();
        jQuery('#reset-password').removeClass('is-invalid');
        var length = 6,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.+*?[^]$(){}=!<>|:-",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.random() * n);
        }
        $(this).parent().siblings('.col-sm-5').children('#reset-password').attr('data-pass', retVal);
        $(this).parent().siblings('.col-sm-5').children('#reset-password').text(retVal);
        $(this).parent().siblings('.col-sm-5').children('#input-password').val(retVal);
        printSuccessfulMsg();
        e.preventDefault();
    })

</script>
