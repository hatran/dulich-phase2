<form class="page-wrap" name="filter" method="get" action="{{ url ('/officesys/fee-policy') }}">
    <div class="row" style="width: 100%">
        {{ csrf_field() }}
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Mã hội phí
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="code" name="code" value="{{ array_get($response_view, 'code', '') }}" tabindex="1" placeholder="Nhập mã chính sách muốn tìm kiếm">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Áp dụng từ ngày
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="date datetime-input" type="text" name="start_date" id="start_date"
                       value="{{ array_get($response_view, 'start_date', '') }}" placeholder="Từ ngày">
                <span class="datetime-icon fa fa-calendar"></span>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tên hội phí
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="name" name="name" value="{{ array_get($response_view, 'name', '') }}" tabindex="1" placeholder="Nhập tên chính sách muốn tìm kiếm">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Áp dụng đến ngày
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="date datetime-input" type="text" name="end_date" id="end_date"
                       value="{{ array_get($response_view, 'end_date', '') }}" placeholder="Đến ngày">
                <span class="datetime-icon fa fa-calendar"></span>
        </div>
        <!-- <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Email
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="email" name="email" value="{{ array_get($response_view, 'email', '') }}" placeholder="Địa chỉ email" tabindex="2">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select id="status" name="status" tabindex="4">
                <option value="">Trạng thái</option>
                @foreach(\App\Constants\UserConstants::$arrUserStatus as $key => $status)
                    <option value="{{ $key }}" @if (array_get($response_view, 'status', '') == $key && array_get($response_view, 'status', '') != '') selected @endif>{{ $status }}</option>
                @endforeach
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Vai trò
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select id="role" class="form-control" name="role" >
                <option value="">Chọn vai trò</option>
                @foreach($groups as $key => $userRole)
                    <option value="{{ $key }}" @if (array_get($response_view, 'role', '') == $key) selected @endif> {{ $userRole }} </option>
                @endforeach
            </select>
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Khu vực
        </div> -->
        <!-- <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <select id="area" name="province_type">
                <option value="">Khu vực</option>
                {{--@foreach(\App\Constants\BranchConstants::$arrProvinceType as $key => $provinceType)
                    <option value="{{ $key }}" @if (array_get($response_view, 'province_type', '') == $key) selected @endif>{{ $provinceType }}</option>
                @endforeach
--}}
                @if (!empty($branchesVpdd))
                    @foreach($branchesVpdd as $key => $val)
                        <option value="{{ $key }}" {{ (old('province_type') == $key || $key == array_get($response_view, 'province_type', '')) ? 'selected' : '' }}>{{ $val }}</option>
                    @endforeach
                @endif
            </select>
        </div> -->
        <div class="field-wrap col-md-3 col-sm-3 col-xs-12" style="margin-left: 45%;margin-bottom: 30px;margin-top: 30px;">
            <input type="submit" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;"s>
        </div>
    </div>
</form>
