<?php

use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
?>
<style>
    body .field-wrap.dropfield:after {
        top: 10px;
    }
    body .field-wrap .datetime-icon {
        top: 10px;
        right: 35px;
    }
</style>
<form class="page-wrap" name="filter" method="get" id="formSearch" style="margin-bottom: 10px;">
    {{ csrf_field() }}
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
			Mã số hồ sơ
		</div>
		<div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
			<input id="file_code" tabindex="1" type="text" name="file_code" value="{{ htmlentities(array_get($response_view, 'file_code', '')) }}" maxlength="6">
		</div>
		<div class="field-wrap col-md-2 col-sm-6 col-xs-12">
			Số thẻ hội viên
		</div>
		<div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
			<input id="member_code" tabindex="1" type="text" name="member_code" value="{{ htmlentities(array_get($response_view, 'member_code', '')) }}" maxlength="6">
		</div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số thẻ HDV
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="idNumber" type="text" name="touristGuideCode" tabindex="3"
                   value="{{ htmlentities(array_get($response_view, 'touristGuideCode', '')) }}" maxlength="9">
        </div>
         <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Họ và tên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="name" type="text" tabindex="4" name="fullName" value="{{ htmlentities(array_get($response_view, 'fullName', '')) }}" maxlength="50">
        </div>
        @if($step != 3)
            {{--<div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Ngày chứng từ
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="from_date" id="from_date" tabindex="5"
                       value="{{ array_get($response_view, 'from_date', '') }}" placeholder="Từ ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="to_date" id="to_date" tabindex="6"
                       value="{{ array_get($response_view, 'to_date', '') }}" placeholder="Đến ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>--}}
        @endif
        @if($step == 3)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Số chứng từ
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input id="number_payment" type="text" name="number_payment" value="{{ htmlentities(array_get($response_view, 'number_payment', '')) }}" maxlength="20">
            </div>

            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Ngày chứng từ
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="from_date" value="{{ array_get($response_view, 'from_date', '') }}" id="from_date" placeholder="Từ ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="to_date" value="{{ array_get($response_view, 'to_date', '') }}" id="to_date" placeholder="Đến ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
        @endif
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái hội phí
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select id="status" name="status" tabindex="9">
                {{--<option value="-1"></option>--}}
                @foreach($arrFile as $key => $file)
                    <option value="{{ $key }}"
                            @if (array_get($response_view, 'status') == $key) selected="selected" @endif>{{ $file }}
                    </option>
                @endforeach
            </select>
        </div>

        @if($step == 4)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12"></div>
            <div class="dropfield col-md-4 col-sm-6 col-xs-12"></div>
        @endif

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Năm nộp hội phí
        </div>
        <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input id="from-year" tabindex="10" type="text" name="from-year" value="{{ htmlentities(array_get($response_view, 'from-year', '')) }}" maxlength="4">
        </div>
        <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input id="to-year" tabindex="11" type="text" name="to-year" value="{{ htmlentities(array_get($response_view, 'to-year', '')) }}" maxlength="4">
        </div>
       {{-- <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Văn phòng đại diện
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select class="select_vpdd" id="province_type" name="province_type" onchange="changeProvinceCode()" tabindex="11">
                <option value="" selected></option>
                @foreach($offices as $value)
                    <option value="{{ $value->id }}" @if (array_get($response_view, 'province_type', '') == $value->id) selected="selected" @endif>{{ $value->name }}</option>
                @endforeach
            </select>
        </div>--}}

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Nơi đăng ký sinh hoạt
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select class="province_code" id="province_code" name="province_code" tabindex="12">
                <option value="" selected></option>
            </select>
            <input type="hidden" id="province-code-selected" value="{{ array_get($response_view, 'province_code', '') }}">
            <input type="hidden" id="filter-by-status" value="<?php echo isset($statusFilter) ? $statusFilter : -1 ?>">
        </div>

        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;">
            <input type="button" class="button btn-primary" style="width: 100px" name="search" id="search" onclick="submitForm();" value="Tìm kiếm" tabindex="13">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm hồ sơ</h4>
            </div>
            <div class="modal-body">
                @if($step == 3)
                    <h4 class="text-center">Ngày chứng từ không hợp lệ</h4>
                @else
                    <h4 class="text-center">Ngày tạo hồ sơ không hợp lệ</h4>
                @endif
                <br/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showAlertYear" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Năm nộp hội phí</h4>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Năm đầu không được lớn hơn năm sau</h4>
                <br/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showAlertChooseOne" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm hội phí</h4>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Vui lòng nhập Mã số hồ sơ hoặc Số thẻ hội viên</h4>
                <br/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });

    function parseDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }

    function submitForm() {
        var isValild = true;
        var fromDate = $('#from-year').val();
        var toDate = $('#to-year').val();
        var fileCode = $('#file_code').val();
        var memberCode = $('#member_code').val();
        
        if (fromDate != '' || toDate != '') {
            if (fileCode == '' && memberCode == '') {
                $("#showAlertChooseOne").modal('show');
                return;
            }
        }

        if (fromDate != '' && toDate != '') {
            if (fromDate > toDate)
            {
               isValild = false;
            } else {
                isValild = true;
            }
     
            if(isValild == false)
            {
               $("#showAlertYear").modal('show');
               return;
            }
        }

        $('#formSearch').submit();
    };

    var changeProvinceCode = function(){
        var val = jQuery('#province_type').val();
        var sta = jQuery('#filter-by-status').val();
        changeProvinceType(val, sta);
    }

    function changeProvinceType(parent_id, province_code_selected = '', status) {
        var province_type_array = [];
        $('#province_type option').each(function() {
            if ($(this).val() != "") {
                province_type_array.push($(this).val());
            }
        });

        $.ajax({
            type: "POST",
            url: '/searchactivityregistrationplace',
            data: { parent_id: parent_id, status: status, province_type: province_type_array, _token: '{{csrf_token()}}' },
            success: function (data) {
              $("#province_code option").remove();
              var option = "";
              option += "<option value='' selected></option>";
              $.each(data.value, function (i, value) {
                if (value.id == province_code_selected) {
                    option += "<option value="+ value.id +" data-parent="+ value.parent_id +" Selected>"+ value.name +"</option>";
                }
                else {
                    option += "<option value="+ value.id +" data-parent="+ value.parent_id +">"+ value.name +"</option>";
                }
              })
              $("#province_code").append(option);
            },
            error: function (data, textStatus, errorThrown) {
                console.log(data);
            },
        });
    };
    var parent_id = $("#province_type option:selected").val();
    var province_code_selected = $("#province-code-selected").val();
    var filter_by_status = $("#filter-by-status").val();

    // console.log(province_code_selected);
    changeProvinceType(parent_id, province_code_selected, filter_by_status);
</script>
