@extends('admin.layouts.app')
@section('title', 'Cập nhật hội phí đã nộp qua VietComBank')
@section('content')
<div class="page-wrap">
    <div class="heading">Cập nhật hội phí đã nộp qua VietComBank</div>
    <div class="table-wrap" style="padding: 10px 20px;">
        <p>1. Nhập chuột để chọn file excel (.xls hoặc .xlsx) chứa thông tin khách hàng đã nộp lệ phí qua Ngân Hàng
            VietComBank</p>
        <p>2. Click vào nút "Cập nhật" để thực hiện cập nhật dữ liệu lên hệ thống</p>
        <div>
            <form action="" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" name="fileToUpload" id="fileToUpload">
                <div style="margin-top: 10px;">
                    <input type="submit" value="Cập nhật" name="submit">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
