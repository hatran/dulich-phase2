<div id="_deleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                        <span class="font-all-popup-title">Số thẻ HDV</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                        <span id="code_delete"/>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                        <span class="font-all-popup-title">Họ và tên</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                        <span id="name_delete" style="word-wrap: break-word;"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span class="font-all-popup-title">Ngày sinh</span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                        <span id="birthday_delete"/>
                    </div>
                </div>

                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                        <span class="font-all-popup-title">Số chứng từ</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                        <span id="number_payment_delete" style="word-wrap: break-word;"/>
                    </div>
                    
                    <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                        <span class="font-all-popup-title">Ngày chứng từ</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                        <span id="date_payment_delete"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span class="font-all-popup-title">Hội phí năm</span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                        <span id="year_delete"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span class="font-all-popup-title">Số tiền</span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                        <span id="currency_delete"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span class="font-all-popup-title">Nội dung nộp tiền</span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                        <span id="content_delete" style="word-wrap: break-word;"/>
                    </div>
                </div>
                <div class="line row">
                    <div class="col-md-2 col-sm-2 col-xs-4">
                        <span class="font-all-popup-title">Ghi chú</span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                        <span id="note_delete" style="word-wrap: break-word;"/>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                        <span class='glyphicon glyphicon-trash'></span> Xoá
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
