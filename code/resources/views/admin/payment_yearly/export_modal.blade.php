<div id="exportAll" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="" style="line-height: 1.42857; margin: 0; font-size: 18px">Thông báo</h4>
            </div>
            <div class="table-line modal-body">

                <div class="line row">
                    <div class="col-md-12 col-sm-10 col-xs-8 dialog-content">
                        <span>Bạn chưa chọn năm xuất danh sách</span>
                    </div>
                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
