<table>
    <tbody>
    <tr>
        <td colspan="9" style="text-align: center; font-weight: 900">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align: center; font-weight: 900">
            @if ($type == 3)
                DANH SÁCH HỘI VIÊN ĐÃ NỘP PHÍ THƯỜNG NIÊN
            @elseif ($type == 2)
                DANH SÁCH HỘI VIÊN CHƯA NỘP PHÍ THƯỜNG NIÊN
            @else
                DANH SÁCH HỘI VIÊN CHƯA NỘP HỘI PHÍ LẦN ĐẦU
            @endif
                
            @if (!empty($fromYear) && empty($toYear))
                {{ $fromYear }}
            @elseif (empty($fromYear) && !empty($toYear))
                {{ $toYear }}
            @elseif (!empty($fromYear) && !empty($toYear))
                {{ $fromYear }} - {{ $toYear }}
            @endif
            :
            @if ($check == 0) TOÀN QUỐC
            @elseif ($check == 1) {{ $provinCode }}
            @endif
        </td>
    </tr>
    @if ($total != 0)
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">
                TỔNG SỐ TIỀN : {{ number_format($total, 0, ',', '.')  }} vnđ 
            </td>
        </tr>
    @endif
    <tr>
        <td colspan="9" style="text-align: center; font-weight: 900">&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
    </tbody>
</table>
