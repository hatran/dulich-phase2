<div id="_showModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="line row">
                    <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                        <span class="font-all-popup-title">Họ và tên</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                        <span id="name_show" style="word-wrap: break-word;"/>
                    </div>
                    <!-- <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                        <span class="font-all-popup-title">CMTND</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                        <span id="identitycard_show" style="word-wrap: break-word;"/>
                    </div> -->
                    <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                        <span class="font-all-popup-title">Số thẻ hội viên</span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                        <span id="member_code_show"/>
                    </div>
                </div>
				
				<div class="line row">
                    
                    <div class="col-md-3 col-sm-3 col-xs-4">
                        <span class="font-all-popup-title">Số thẻ HDV</span>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-8 dialog-content">
                        <span id="code_show"/>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-4">
                        <span class="font-all-popup-title">Hạn thẻ</span>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-8 dialog-content">
                        <span id="member_code_expiration"/>
                    </div>
                </div>
                

                <div class="line row">
                    <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                        <span class="font-all-popup-title">Chi hội</span>
                    </div>
                    <div class="col-md-9 col-sm-3 col-xs-8 dialog-content">
                        <span id="provincecode_show" style="word-wrap: break-word;"/>
                    </div>
                </div>
                <!-- <div class="line row">
                    <div class="col-md-3 col-sm-3 col-xs-4">
                        <span class="font-all-popup-title">Gia nhập VTGA từ</span>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-8 dialog-content">
                        <span id="joining_show"/>
                    </div>
                </div> -->
                <div class="line row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div>
                            <table class="table_payment" border="1px">
                                <thead>
                                    <tr>
                                        <td width="7%">Số TT</td>
                                        <td width="15%">Số chứng từ</td>
                                        <td width="20%">Ngày chứng từ<br >(dd/mm/yyyy)</td>
                                        <!-- <td width="13%">Hội phí năm</td> -->
                                        <td width="15%">Số tiền nộp(VNĐ)</td>
                                        <td width="15%">Hội phí đến ngày<br >(dd/mm/yyyy)</td>
                                        <td width="20%">Nội dung nộp tiền</td>
                                        <td width="10%">Ghi chú</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .table_payment {
        width: 100%;
    }

    .table_payment thead tr {
        background-color: #EDF1F6;
        text-align: center;
    }

    .table_payment thead tr td {
        color: #144a8b;
    }

    .table_payment tbody tr {
        text-align: center;
    }
</style>