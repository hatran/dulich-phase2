<div id="_addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="table-line">
                    <div class="line row">
                        <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                            <span class="font-all-popup-title">Số thẻ HDV</span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <span id="code_add"/>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <span class="font-all-popup-title">Ngày sinh</span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <span id="birthday_add"/>
                        </div>

                    </div>
                    <div class="line row">
                        <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                            <span class="font-all-popup-title">Họ và tên</span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <span id="name_add"/>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                            <span class="font-all-popup-title">Chi hội</span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <span id="province_name_add"/>
                        </div>
                    </div>

                    <div class="line row">
                        <div class="col-md-2 col-sm-2 col-xs-4 no-break">
                            <span class="font-all-popup-title">Số chứng từ <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <input class="position-number-payment" type="text" id="number_payment_add" tabindex="201" maxlength="30" style="max-width: 120px"><br/>
                            <span class="errorNumberPayment text-left hidden error-msg"></span>
                        </div>
                        <div class="dropfield col-md-2 col-sm-2 col-xs-4 no-break">
                            <span class="font-all-popup-title">Ngày chứng từ <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <input class="date datetime-input position-date" type="text" id="date_payment_add" onblur="updateExpirationDate(this)" tabindex="202" maxlength="10" style="max-width: 120px">
                            <span class="errorDatePayment text-left error-msg"></span>
                        </div>
                    </div>
                    <div class="line row">
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <span class="font-all-popup-title">Số tiền <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <input type="text"
                                   name="currency_add"
                                   id="currency_add"
                                   tabindex="204"
                                   style="max-width: 90px; width: 100%;"
                                   placeholder="500000"
                                   onkeypress="return isNumber()"
                                   class="position-currency"
                                   maxlength="9"
                            >
                            <label for="currency_add" style="font-size: 12px; font-weight: 0; width: 5px;">VNĐ</label>
                            <br/>
                            <label id="transform_currency_add" style="display: none;"></label>
                            <span class="errorCurrency text-left hidden error-msg"></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <span class="font-all-popup-title">Ngày hết hạn</span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <input class="date datetime-input position-date" type="text" id="expiration_add" tabindex="205" maxlength="10" style="max-width: 120px">
                            <span class="errorDateExpired text-left error-msg"></span>
                        </div>
                    </div>
                    <div class="line row fee_policy" style="display: none">
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <span class="font-all-popup-title">Hội phí</span>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-8 dialog-content">
                            <span class="list_fee_police">
                                <select class="form-control fee_policy_select" onchange="changeFeePolicy(this)"></select>
                            </span>
                        </div>
                        <!-- <div class="col-md-4 col-sm-4 col-xs-8 dialog-content">
                            <span class="money" style="display: inline-block;"></span>
                            VNĐ
                        </div> -->
                    </div>

                    <!-- <div class="line row fee_policy_end" style="display: none">
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <span class="font-all-popup-title">Số tiền phải đóng</span>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-8 dialog-content">
                         <span class="money"></span> VNĐ
                        </div>
                    </div> -->

                    <!-- <div class="line row">
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <span class="font-all-popup-title">Loại phí</span>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-8 dialog-content">
                            <span class="list_fee_police">
                                <select class="form-control" name="flag">
                                    <option value="0">Phí gia nhập</option>
                                    <option value="1">Phí thường niên</option>
                                </select>
                            </span>
                        </div>
                    </div> -->

                    <div class="line row">
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <span class="font-all-popup-title">Nội dung nộp tiền</span>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                            <textarea class="form-control position-content" id="content_add" rows="5" tabindex="205" maxlength="200"></textarea><br/>
                            <span class="errorContent text-left hidden error-msg"></span>
                        </div>
                    </div>

                    <div class="line row">
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <span class="font-all-popup-title">Ghi chú</span>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-8 dialog-content">
                            <textarea class="form-control" id="note_add" rows="5" tabindex="206" maxlength="200"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary add" id="btnsubmit" tabindex="207">
                            <span class='glyphicon glyphicon-plus'></span> Tạo mới
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal" tabindex="208">
                            <span class='glyphicon glyphicon-remove'></span> Thoát
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/display-currency.js') }}"></script>
<style>
    @media (min-width: 768px) {
        .modal-dialog {
            width: 900px;
            margin: 30px auto;
        }
    }

    @media (min-width: 992px) {
        #_addModal .col-md-2 {
            width: 20%;
        }

        #_addModal .col-md-4 {
            width: 30%;
        }

        .col-md-10 {
            width: 80%;
        }
    }


</style>
