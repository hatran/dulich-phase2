<?php if($type == 3){ //da nop phi ?>
@include('admin.payment_yearly.header_excel')
<table>
    <tbody>
        <tr class="header">
            <td>TT</td>
            <td>Họ và tên</td>
            <td>Mã hồ sơ</td>
            <td>Số thẻ hội viên</td>
            <td>Số tham chiếu</td>
            <td>Ngày nộp tiền</td>
            <td>Số tiền</td>
            <td>Loại hội phí</td>
            <td>Số điện thoại</td>
            <td>Email</td>
            <td>Chi hội HDV</td>
        </tr>
        @if (!empty($listMember))
            @foreach ($listMember as $key => $item)
                @php
                $mobile = $item->firstMobile . (!empty($item->secondMobile) ? ',' . $item->secondMobile : '' );
                $emailS = $item->firstEmail . (!empty($item->secondEmail) ? ','. $item->secondEmail : '' );
                @endphp
            <tr class="main-content">
                <td>{{ $key + 1 }}</td>
                <td>{{ htmlentities($item->fullName) }}</td>
                <td>{{ htmlentities($item->file_code) }}</td>
                <td>{{ !empty($item->member_code) ? $item->member_code : ''  }}</td>
                <td style="text-align: left">{{ htmlentities($item->number_payment) }}</td>
                <td>{{ $item->date_payment != '' ? date('d/m/Y', strtotime($item->date_payment)) : '' }}</td>
                <td style="text-align: right">
                    @php $money = 0; @endphp
                    @if (!empty($item->date_payment))
                        @if (strtotime($item->date_payment) >= strtotime($paymentYearlyPoint))
                            @php $money = $item->currency / $item->number_of_year @endphp
                        @else
                            @if ($item->flag == 1) 
                                @php 
                                    $money = $item->currency / $item->number_of_year 
                                @endphp
                            @else 
                                @php 
                                    $money = ($item->currency - 500000) / $item->number_of_year 
                                @endphp
                            @endif
                        @endif
                    @endif
                
                    @if ($money != 0) {{ number_format($money, 0, ',', '.')  }} vnđ @endif
                </td>
                <td>
                    @if ($item->flag == 1) Gia hạn
                    @else Đóng mới
                    @endif
                </td>
                <td>{{ $mobile }}</td>
                <td>{{ $emailS }}</td>
                <td>{{ !empty($branchList[$item->province_code]) ? $branchList[$item->province_code] : '' }}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>

<?php }elseif($type == 2){ // chua nop phi ?>
@include('admin.payment_yearly.header_excel')
<table>
    <tbody>
    <tr class="header">
        <td>TT</td>
        <td>Số thẻ <br/> Hội viên</td>
        <td>Họ và tên</td>
        <td>Ngày sinh</td>
        <td>Ngôn ngữ <br/> hướng dẫn</td>
        <td>Hội viên <br/> VTGA từ</td>
        <td>ĐTDĐ</td>
        <td>Email</td>
        <td>Chi hội HDV</td>
    </tr>
    @if (!empty($listMember))
        @foreach ($listMember as $key => $item)
            @php
            $mobile = $item->firstMobile . (!empty($item->secondMobile) ? ',' . $item->secondMobile : '' );
            $emailS = $item->firstEmail . (!empty($item->secondEmail) ? ','. $item->secondEmail : '' );
            @endphp
            <tr class="main-content">
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->member_code }}</td>
                <td style="position: relative;word-wrap: break-word; white-space: normal;">{{ htmlentities($item->fullName) }}</td>
                <td>{{ $item->birthday != '' ? date('d/m/Y', strtotime($item->birthday)) : '' }}</td>
                <td>{{ isset($listLanguage[$item->guideLanguage]) ? $listLanguage[$item->guideLanguage] : '' }}</td>
                <td>{{ $item->member_from }}</td>
                <td style="position: relative;word-wrap: break-word; white-space: normal;">{{ $mobile }}</td>
                <td style="position: relative;word-wrap: break-word; white-space: normal;">{{ $emailS }}</td>
                <td style="position: relative;word-wrap: break-word; white-space: normal;">{{ !empty($branchList[$item->province_code]) ? $branchList[$item->province_code] : '' }}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

<?php }elseif($type == 1){ // chua nop phi lan dau ?>
@include('admin.payment_yearly.header_excel')
<table>
    <tbody>
    <tr class="header">
        <td>TT</td>
        <td>Số thẻ <br/> Hội viên</td>
        <td>Họ và tên</td>
        <td>Ngày sinh</td>
        <td>Ngôn ngữ <br/> hướng dẫn</td>
        <td>Hội viên <br/> VTGA từ</td>
        <td>ĐTDĐ</td>
        <td>Email</td>
        <td>Chi hội HDV</td>
    </tr>
    @if (!empty($listMember))
        @foreach ($listMember as $key => $item)
            @php
            $mobile = $item->firstMobile . (!empty($item->secondMobile) ? ',' . $item->secondMobile : '' );
            $emailS = $item->firstEmail . (!empty($item->secondEmail) ? ','. $item->secondEmail : '' );
            @endphp
            <tr class="main-content">
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->member_code }}</td>
                <td style="position: relative;word-wrap: break-word; white-space: normal;">{{ htmlentities($item->fullName) }}</td>
                <td>{{ $item->birthday != '' ? date('d/m/Y', strtotime($item->birthday)) : '' }}</td>
                <td>{{ isset($listLanguage[$item->guideLanguage]) ? $listLanguage[$item->guideLanguage] : '' }}</td>
                <td>{{ $item->member_from }}</td>
                <td style="position: relative;word-wrap: break-word; white-space: normal;">{{ $mobile }}</td>
                <td style="position: relative;word-wrap: break-word; white-space: normal;">{{ $emailS }}</td>
                <td style="position: relative;word-wrap: break-word; white-space: normal;">{{ !empty($branchList[$item->province_code]) ? $branchList[$item->province_code] : '' }}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

<?php } ?>

<style>
    .header td {
        font-weight: bold;
        text-align: center;
        border: 1px solid #000;
    }
    .main-content td {
        border: 1px solid #000;
    }
</style>
