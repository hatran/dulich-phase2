<div id="_editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="table-line">
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="font-all-popup-title">Số thẻ HDV</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <span id="code_edit"/>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Ngày sinh</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <span id="birthday_edit"/>
                        </div>
                    </div>
                    <div class="line row">
                        <div class="col-md-3 col-sm-4 col-xs-4 no-break">
                            <span class="font-all-popup-title">Họ và tên</span>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-8 dialog-content">
                            <span id="name_edit"/>
                        </div>

                    </div>

                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="font-all-popup-title">Số chứng từ <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <input class="position-number-payment" type="text" id="number_payment_edit" tabindex="101" maxlength="20" style="max-width: 120px"><br/>
                            <span class="errorNumberPayment text-left hidden error-msg"></span>
                        </div>
                        <div class="dropfield col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="font-all-popup-title">Ngày chứng từ <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <input class="date datetime-input position-date" type="text" id="date_payment_edit" onblur="updateExpirationDate(this, '_edit')" tabindex="102" maxlength="10" style="max-width: 120px">
                            <span class="errorDatePayment text-left error-msg"></span>
                        </div>
                    </div>
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Số tiền <span class="color-required">*</span></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <input type="text"
                                   name="currency_edit"
                                   id="currency_edit"
                                   tabindex="204"
                                   style="max-width: 90px; width: 100%;"
                                   placeholder="500000"
                                   onkeypress="return isNumber(event)"
                                   class="position-currency"
                                   maxlength="9"
                            >
                            <label for="currency_edit" style="font-size: 12px; font-weight: 0; width: 5px;">VNĐ</label>
                            <br/>
                            <label id="transform_currency_edit" style="display: none;"></label>
                            <span class="errorCurrency text-left hidden error-msg"></span>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Ngày hết hạn</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <input class="date datetime-input position-date" type="text" id="expiration_edit" tabindex="205" maxlength="10" style="max-width: 120px">
                            <span class="errorDateExpired text-left error-msg"></span>
                        </div>
                    </div>
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Nội dung nộp tiền</span>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-8 dialog-content">
                            <textarea class="form-control position-content" id="content_edit" rows="5" tabindex="105" maxlength="200"></textarea><br/>
                            <span class="errorContent text-left hidden error-msg"></span>
                        </div>
                    </div>

                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4">
                            <span class="font-all-popup-title">Ghi chú</span>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-8 dialog-content">
                            <textarea class="form-control" id="note_edit" rows="5" tabindex="106" maxlength="200"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-info edit" id="btnsubmit" tabindex="107">
                            <span class='glyphicon glyphicon-check'></span> Lưu
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal" tabindex="108">
                            <span class='glyphicon glyphicon-remove'></span> Thoát
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>