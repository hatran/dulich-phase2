@extends('admin.layouts.app')
@section('title', 'Thêm mới thông tin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .expire_date{
        margin-bottom: 10px;
        padding-top: 7px;
        font-weight: bold;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }
    .col-md-1{
        width: 10%;
    }
    .has-error .control-label {
        color: #3D3A3A !important
    }

    span.error-msg {
        color: #a94442;
    }
</style>
    <div class="page-wrap">
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Thêm mới thông tin</div>

            <div class="panel-body">
                <form id="infomation-form" enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{ URL::route('infomation.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('option_code_parent') ? ' has-error' : '' }}">
                        <label for="option_code_parent" class="col-md-2 text-right">Danh mục thông tin cấp 1<span class="color-required">*</span></label>
                        <div class="col-md-4">
                            <select id="option_code_parent" class="form-control" tabindex="1" name="option_code_parent">
                                <option value="">Chọn danh mục</option>
                                @foreach($listInfomationParent as $key => $val)
                                    <option value="{{ $key }}" {{ (old('option_code_parent') == $key) ? 'selected' : '' }}> {{ $val }} </option>
                                @endforeach
                            </select>
                            @if ($errors->has('option_code_parent'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('option_code_parent') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('option_code') ? ' has-error' : '' }}">
                        <label for="option_code" class="col-md-2 text-right">Danh mục thông tin cấp 2</label>
                        <div class="col-md-4">
                            <select id="option_code" class="form-control" name="option_code" tabindex="2">
                                <option value="">Chọn danh mục</option>
                               <!--  @foreach($listInfomation as $key => $val)
                                    <option value="{{ $key }}" {{ (old('option_code') == $key) ? 'selected' : '' }}> {{ $val }} </option>
                                @endforeach -->
                            </select>
                            @if ($errors->has('option_code'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('option_code') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-2 text-right">Tiêu đề <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}"  tabindex="3" maxlength="500" >

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="start_time" class="col-md-2 control-label">Phát hành từ ngày<span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" id="errorDate" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <input type="text" style="" class="date datetime-input" name="start_time" value="{{ old('start_time') }}" id="start_time" placeholder="" tabindex="4">
                                <span class="datetime-icon fa fa-calendar"></span>
                                @if ($errors->has('start_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('start_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12 expire_date" style="">
                                Đến ngày
                            </div>
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" style="margin-bottom: 10px; padding-right: 0px !important; " >
                                <input type="text" style="" class="date datetime-input" name="end_time" value="{{ old('end_time') }}" id="end_time" placeholder="" tabindex="5">
                                <span class="datetime-icon fa fa-calendar" style="right: 10px !important;"></span>
                                @if ($errors->has('end_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('end_time') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="" class="col-md-2 control-label">Trạng thái <span class="color-required">*</span></label>
                        <div class="col-md-6" style="padding-top: 5px">
                            <label class="radio-inline" for="statushd">
                                <input id="statushd" type="radio" name="status" value="1" tabindex="6" {{ $statusDefault == 1 ? 'checked' : '' }} >
                                Hoạt động
                            </label>
                            <label class="radio-inline" for="statusln" >
                                <input id="statusln" type="radio" name="status" value="0" tabindex="7" {{ $statusDefault == 0 ? 'checked' : '' }}>
                                Lưu nháp
                            </label>
                            @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 text-right" for="pdfAll_add" style="padding-top: 7px">File thông tin đính kèm</label>
                        <div class="col-md-9">
                            <span><i class="fa fa-upload" aria-hidden="true"></i>Tải lên file pdf</span>
                            <input type="file" name="pdfAll_add" id="pdf" tabindex="8">
                            <span class="errorPdfAll_add text-left hidden error-msg"></span>
                        </div>
                    </div>
                           
                    <div class="form-group">
                        <label for="thumnail_image" class="col-md-2 text-right">Hình ảnh tóm tắt<span class="color-required">*</span></label>
                        <div class="col-md-10" style="padding-top: 10px">
                            <input tabindex="9" onchange="return fileValidation(event);" type="file" class="form-control-file" name="thumnail_image" id="thumnail_image">
                            @if ($errors->has('thumnail_image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('thumnail_image') }}</strong>
                                </span>
                            @endif
                            <div id="image-preview" style="margin-top: 10px;float: left;"></div>
                        </div>
                      </div>
                    <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                        <label for="short_description" class="col-md-2 text-right">Nội dung tóm tắt <span class="color-required">*</span></label>
                        <div class="col-md-10" style="padding-top: 10px">
                            <textarea maxlength="500" tabindex="10" rows="5" id="short_description" name="short_description" class="form-control" style="resize: none;">{{ old('short_description') }}</textarea>
                            @if ($errors->has('short_description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('short_description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-2 text-right">Nội dung đầy đủ <span class="color-required">*</span></label>
                        <div class="col-md-10" style="padding-top: 10px">
                            <textarea tabindex="11" rows="15" id="content_rg" name="content" class="form-control position-content tinymce">{{ old('content') }}</textarea>
                            @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button type="button" class="btn btn-primary" id="submitBtn" onclick="return submitForm();" tabindex="12">Lưu</button>
                            <a href="{{ URL::route('infomation.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="13">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript" src="{{ asset("js/ajaxloadoptioncode.js") }}"></script>
<script>
    var inputGetOptionCode = "<?php echo old('option_code'); ?>";
    var urlRoute = "<?php echo URL::route('ajax_add_sub_info'); ?>";
    var selector = $("#option_code");
    var selectorParent = $("#option_code_parent");
    // Enter to submit form registerForm
    document.body.addEventListener('keydown', function(e) {
        var key = e.which;
        if (key == 13) {
            $("#registerInfo").click();
        }
    });
    var flagValidate = true;
    function fileValidation(event) {
        event.preventDefault();
        var fileInput = event.target;
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        $(fileInput).parent().find('.help-block').remove();
        $('#image-preview').html('');
        if(!allowedExtensions.exec(filePath)){
            $(fileInput).parent().append('<span class="help-block"><strong>Ảnh đại diện phải là một tập tin có định dạng: jpeg, jpg, png, gif.</strong></span>');
            fileInput.value = '';
            return false;
        } else if (fileInput.files[0].size/1024/1024 > 10) {
            mess = 'Dung lượng ảnh đại diện quá lớn: ' + fileInput.files[0].size/1024/1024 + "MB";
            $(fileInput).parent().remove('.help-block').append('<span class="help-block"><strong>'+mess+'</strong></span>');
            fileInput.value = '';
            return false;
        } else {
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image-preview').html('<img src="'+e.target.result+'" width="100" height="80"/>');
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
            return false;
        }
        
        return false;
    }
    
    $(document).ready(function () {
        document.getElementById("option_code_parent").focus();
    });

    function submitForm() {
        var myfile="";
        var flg = 0;
        myfile= $("#pdf").val();
        var title       = $("#title").val();
        if(title.trim() == ''){
        
        }else{
            $("#title").removeClass('error_border');
            var titleConver = convertStr(title);
            if(!(/^[a-zA-Z0-9\-\.\/]*$/i).test(titleConver)){
                flg = 1;
                $("#title").addClass("error_border");
                $( '<span class="help-block"><strong>Tiêu đề chỉ được nhập các ký tự a-z A-Z 0-9 -  . /</strong></span>' ).insertAfter( "#title" );//
            }else {
                $("#title").removeClass("error_border");
            }
        }
        if (myfile != "") {
            var ext = myfile.split('.').pop();
            if(ext != "pdf"){
                $("#pdfAll_add").addClass('error_border');
                $(".errorPdfAll_add").html('Tải lên file có định dạng pdf');
                $(".errorPdfAll_add").removeClass('hidden');
                flg = 1;
            }
            else{
                $(".errorPdfAll_add").html('');
                $(".errorPdfAll_add").addClass('hidden');
                $("#pdfAll_add").removeClass('error_border');
            }
        }
        if (flg != 1) {
            $('#submitBtn').prop('disabled', true);
            $('#infomation-form').submit();
        }
    
        return false;
    }

    function convertStr(txt){

        var str = txt;
        str= str.toLowerCase();
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str= str.replace(/đ/g,"d");
        str= str.replace(/\s/g, '');
        //str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
        //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
        //str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
        return str;
    }

    $(document).ready(function () {
        var itemVal = selectorParent.children("option:selected").val();
        selector.children('option:not(:first)').remove();
        if (itemVal != '') {
            ajaxSearchOptionCode(urlRoute, itemVal, selector, inputGetOptionCode);
        }
    });

    selectorParent.change(function () {
        var __this = $(this);
        var itemVal = $(this).val();
        selector.children('option:not(:first)').remove();

        if (itemVal != '') {
            ajaxSearchOptionCode(urlRoute, itemVal, selector)
        }
    });
</script>
@endsection
