<div class="filter">
<form class="page-wrap" name="filter" method="get" id="formSearch" style="display: block">
    {{ csrf_field() }}
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Họ tên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="fullname-search" class="form-control" tabindex="1" type="text" name="fullname" placeholder="Họ tên thành viên" value="{{ htmlentities(array_get($response_view, 'fullname', '')) }}" maxlength="50"/>
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trực thuộc Ban
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select class="form-control department" id="department" name="department" style="-webkit-appearance: menulist" tabindex="2">
                <option value="" selected=""></option>
                @if(!empty($departments))
                    @foreach($departments as $value)
                        <?php if ($value->code == 'DEPARTMENT03'): ?>
                        <optgroup label="{{ $value->value }}">
                            @foreach($departments as $value2)
                                <?php if($value2->code != 'DEPARTMENT03' && strpos($value2->code, $value->code) !== false): ?>
                                <option value="{{ $value2->code }}" {{ (array_get($response_view, 'department', '') === $value2->code) ? 'selected' : '' }}>{{ $value2->value }}</option>
                                <?php endif; ?>
                            @endforeach
                        </optgroup>
                            <?php elseif(!(strpos($value->code, 'DEPARTMENT03') !== false)): ?>
                            <option value="{{ $value->code }}" {{ (array_get($response_view, 'department', '') === $value->code) ? 'selected' : '' }}>{{ $value->value }}</option>
                            <?php endif; ?>
                    @endforeach
                @endif
            </select>
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số điện thoại
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="phone" class="form-control" type="text" name="phone" tabindex="3" placeholder="Số điện thoại" value="{{ htmlentities(array_get($response_view, 'phone', '')) }}" maxlength="20">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Email
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="email" class="form-control" type="text" name="email" tabindex="4" placeholder="Địa chỉ Email" value="{{ htmlentities(array_get($response_view, 'email', '')) }}" maxlength="50">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Chức vụ
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select class="form-control position" id="position" name="position" style="-webkit-appearance: menulist" tabindex="5">
                <option value="" selected=""></option>
                <option value="">Tất cả</option>
                @if(!empty($positions))
                @foreach($positions as $value)
                    <option value="{{ $value->code }}" {{ (array_get($response_view, 'position', '') === $value->code) ? 'selected' : '' }}>{{ $value->value }}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="6" id="search_status" name="search_status" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($statusOption))
                    @foreach($statusOption as $key => $val)
                        @php $selected = is_numeric(Input::get('search_status', '')) && Input::get('search_status', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 30px;margin-top: 30px;">
            <input type="submit" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 140px; margin: auto;" tabindex="7"/>
        </div>
    </div>
</form>
</div>
<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
    $(function () {
        $("#fullname-search").focus();
    });
</script>