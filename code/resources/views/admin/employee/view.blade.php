<div class="modal-body">
    <div class="container-fluid">
        <div class="row">
            <div class="container-fluid shadow">
                <div class="row col-md-6" style="padding-left: 0">
                    <div class="row" style="padding-left: 15px">
                    {{--Họ tên--}}
                   
                        <div class="col-md-12">
                            <div class="row form-group" style="display: block;">
                                <label class="col-sm-4"
                                       for="field1">Họ tên</label>
                                <div class="controls col-sm-8">
                                    <p><?= htmlentities($model->fullname) ?></p>
                                </div>
                            </div>
                        </div>
                   

                    {{--Ngày sinh / Giới tính--}}
                  
                        <div class="col-md-12">
                            <div class="row form-group" style="display: block;">
                                <label class="col-sm-4"
                                       for="birthday">Ngày sinh</label>
                                <div class="controls col-sm-8">
                                    <p><?= $model->birthday ?></p>
                                </div>

                            </div>
                        </div>
                   
               
                        <div class="col-md-12">
                            <div class="row form-group" style="display: block;">
                                <label class="col-sm-4"
                                       for="gender1">Giới tính</label>
                                <div class="controls col-sm-8">
                                    <p><?php echo ($model->gender == 1) ? 'Nam' : 'Nữ';  ?></p>
                                </div>

                            </div>
                        </div>
             

                    {{--phone--}}
                    
                        <div class="col-md-12">
                            <div class="row form-group" style="display: block;">
                                <label class="col-sm-4"
                                       for="phone">Số điện thoại</label>
                                <div class="controls col-sm-8">
                                    <p><?= $model->phone ?></p>
                                </div>

                            </div>

                            {{--email--}}
                            <div class="row form-group" style="display: block;">
                                <label class="col-sm-4"
                                       for="email">Email</label>
                                <div class="controls col-sm-8">
                                    <p><?= $model->email ?></p>
                                </div>

                            </div>

                            {{--Branch--}}
                            <div class="row form-group" style="display: block;">
                                <label class="col-sm-4"
                                       for="branch">Trực thuộc ban</label>
                                <div class="controls col-sm-8">
                                    <p><?= $department ?></p>
                                </div>

                            </div>

                            {{--Chức vụ--}}
                            <div class="row form-group" style="display: block;">
                                <label class="col-sm-4"
                                       for="option">Chức vụ</label>
                                <div class="controls col-sm-8">
                                    <p><?= $position ?></p>
                                </div>

                            </div>             
                        </div>
                    </div>
                </div>

                <div class="row col-md-6" style="padding-left: 0">
                    {{--Image--}}
                    <div class="form-group" style="display: block;">
                        <label class="col-sm-4"
                               for="option">Ảnh đại diện</label>
                        <div class="controls col-sm-8">
                            <span id="upload-image-error" class="error"></span>
                        </div>
                    </div>
                    <div class="preview-img text-center" style="padding-bottom: 20px">
                        <a href="{{url('/files'.$model->profile_image)}}">
                            <img src="{{url('/files'.$model->profile_image)}}"
                                 alt="no image" width="80%"/>
                        </a>
                    </div>
                </div>
                <div class="row col-md-12" style="padding-left: 0">
                   
                        {{--Address--}}
                        <div class="row form-group" style="display: block; margin-left: 0; margin-right: 0">
                            <label class="col-sm-2"
                                   for="address">Địa chỉ</label>
                            <div class="controls col-sm-10" style="text-align: justify;">
                                <p><?= htmlentities($model->address) ?></p>
                            </div>
                        </div>
                        
                        {{--Company--}}
                        <div class="row form-group" style="display: block; margin-left: 0; margin-right: 0">
                            <label class="col-sm-2"
                                   for="company">Nơi công tác</label>
                            <div class="controls col-sm-10" style="text-align: justify;">
                                <p><?= htmlentities($model->company) ?></p>
                            </div>
                        </div>

                        {{--Status--}}
                        <div class="row form-group" style="display: block; margin-left: 0; margin-right: 0">
                            <label class="col-sm-2"
                                   for="status">Trạng thái</label>
                            <div class="controls col-sm-10">
                                <p><?php echo ($model->status == 1) ? 'Hoạt động' : 'Không hoạt động'; ?></p>
                            </div>
                        </div>               
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer view">
    <button type="button" class="btn btn-warning" tabindex="1"
            data-dismiss="modal"><?= Lang::get('offices.close') ?></button>
</div>
