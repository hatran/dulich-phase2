@extends('admin.layouts.app')
@section('title', 'Quản trị Ban chấp hành')
@section('header_embed')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/bootstrap-table/dist/bootstrap-table.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vita.css') }}"/>
@endsection
@section('content')

    <?php // Dump data for debug ?>
    <?php // var_dump($arrayEmployees); ?>

    <div id="content" style="margin-top: 10px;">
        <div class="main-content" style="padding-bottom: 75px;">
            @if (session()->get('error'))
                <div class="alert alert-danger">
                    <div>{!! session()->get('error') !!}</div>
                </div>
            @endif
            @if(\Illuminate\Support\Facades\Session::has('success_message'))
                <div class="alert alert-success">
                    <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
                </div>
            @endif

            @include('admin.employee.search')

            <div class="top-navigation row">
                <div class="col-sm-6 col-xs-12 total">
                    Tống số: <?= $total ?>
                </div>
                <div class="col-md-6 col-xs-12 right">
                    <button type="button" class="btn btn-primary col-xs-2 add-modal" style="float:right"
                            data-toggle="modal" data-target="#createnewModal" onclick="addnewOffice()">
                        <?= Lang::get('offices.addnew') ?>
                    </button>
                </div>
            </div>

            <div class="table-wrap">
                <table id="table" data-escape="false" data-search="true" data-show-columns="true" data-pagination="true"
                       data-pagination-first-text="Trang đầu"
                       data-pagination-last-text="Trang cuối"
                >
                    <thead>
                    <tr>
			<th data-width="5%" data-field="stt" data-sortable="false" data-align="center" >STT</th>
                        <th data-width="12%" data-field="fullname" data-sortable="false" data-align="left"><?= Lang::get('employee.fullname'); ?></th>
                        <th data-width="9%" data-field="department" data-sortable="false" data-align="left"><?= Lang::get('employee.department'); ?></th>
                        <th data-width="9%" data-field="position" data-sortable="false" data-align="left"><?= Lang::get('employee.position'); ?></th>
                        <th data-width="15%" data-field="company2" data-sortable="false" data-align="left"><?= Lang::get('employee.company'); ?></th>
                        <th data-width="15%" data-field="address2" data-sortable="false" data-align="left"><?= Lang::get('employee.address'); ?></th>
                        <th data-width="8%" data-field="phone" data-sortable="false" data-align="right"><?= Lang::get('employee.phone'); ?></th>
                        <th data-width="10%" data-field="email" data-sortable="false" data-align="left"><?= Lang::get('employee.email'); ?></th>
                        <th data-width="10%" data-field="status_render" data-sortable="false" data-align="left">Trạng thái</th>
                        <th data-width="7%" data-field="action" data-sortable="false" data-align="center"><?= Lang::get('employee.action'); ?></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    @include('admin.employee.modal.addnew')
    @include('admin.employee.modal.delete')
@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/bootstrap-table-1.9.1/dist/bootstrap-table.min.js') }}"></script>
    <script src="{{ asset('admin/js/offices.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/bootstrap-table-en-US.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        var collectionData = <?= $arrayEmployees?>;
        jQuery('#table').bootstrapTable({
            data: collectionData,
            pageNumber: <?= $paginator ?>,
            search: false,
            showColumns: false,
        });

        jQuery('#table').bootstrapTable('selectPage', 1);

        jQuery(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            setTimeout(function () {
                jQuery('#fullname').focus();
            }, 500);
        });

        jQuery(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            setTimeout(function () {
                jQuery('#fullname').focus();
            }, 500);
        });
    </script>
@endsection
