<style>
    /*[hidden] {*/
    /*display: none !important;*/
    /*}*/
    .bar {
        height: 18px;
        background: green;
    }
</style>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel" style="font-size: 20px;">Xóa Thành Viên</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true"
                          style="font-size: 39px; width: 30%;float: right;text-align: right;padding-right: 15px;">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Bạn chắc chắn muốn xóa <span id="id-em" style="font-size:  20px;color:  blue;"></span> ra khỏi danh sách?</h4>
                <br/>
            </div>
            <form action="{{url('officesys/employee/delete')}}" id="officesaddnew" class="form-horizontal" role="form"
                  enctype="multipart/form-data" method="get">
                {{ csrf_field() }}
                <input type="hidden" id="inputid-to-delete" name="id" class="form-control" value="-1"/>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" tabindex="1"><?= Lang::get('offices.delete') ?></button>
                    <button type="button" class="btn btn-warning" tabindex="2"
                            data-dismiss="modal"><?= Lang::get('offices.close') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>