<style>
    @media (min-width: 768px) {
        .modal-dialog {
            width: 800px;
        }
    }
</style>

<!-- Modal -->
<div class="modal fade" id="createnewModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="font-size: 20px;"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"
                          style="font-size: 39px; width: 30%;float: right;text-align: right;padding-right: 15px;">&times;</span>
                </button>
            </div>
            <form action="{{url('/officesys/employee/create')}}" id="officesaddnew" class="form-horizontal" role="form"
                  enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id" class="form-control" value="-1"/>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="container-fluid shadow">
                                <div class="row">
                                    <div id="valErr" class="row viewerror clearfix hidden">
                                        <div class="alert alert-danger">Oops! Seems there are some errors..</div>
                                    </div>
                                    <div id="valOk" class="row viewerror clearfix hidden">
                                        <div class="alert alert-success">Yay! ..</div>
                                    </div>
                                    {{--Họ tên--}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="display: block;">
                                                <label class="control-label control-label-left col-sm-2"
                                                       for="field1">Họ tên<a
                                                            style="color: red">*</a></label>
                                                <div class="controls col-sm-9" style="padding-left: 19px;">
                                                    <input id="fullname" name="fullname" type="text"
                                                           class="form-control k-textbox required" tabindex="100"
                                                           data-role="text" data-parsley-errors-container="#errId1" pattern="^[a-zA-Z1-9].*" maxlength="50" title="Vui lòng không đẻ trống. Vui lòng nhập đúng định dạng dư liệu" >
                                                    <span id="error-fullname" class="error">Họ tên không được bỏ trống</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    {{--Ngày sinh / Giới tính--}}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group" style="display: block;">
                                                <label class="control-label control-label-left col-sm-4"
                                                       for="birthday">Ngày sinh<a
                                                            style="color: red">*</a></label>
                                                <div class="controls col-sm-8" style="padding-left: 19px;">

                                                    <input id="birthday" name="birthday" type="text"
                                                           class="form-control k-textbox date required"
                                                           data-role="text" tabindex="101"
                                                           data-parsley-errors-container="#error-birthday">
                                                    <span id="error-birthday" class="error">Ngày sinh không được bỏ trống</span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" style="display: block;">
                                                <label class="control-label control-label-left col-sm-3"
                                                       for="gender1">Giới tính<a
                                                            style="color: red">*</a></label>
                                                <div class="controls col-sm-9">

                                                    <label class="radio-inline" for="gender1">
                                                        <input type="radio" value="1"
                                                               id="gender1" name="gender" tabindex="102"
                                                               data-parsley-errors-container="#error-gender1" checked>Nam</label>
                                                    <label class="radio-inline" for="gender2"
                                                           style="display: inline-block">
                                                        <input type="radio" value="0" id="gender2" name="gender" tabindex="103"
                                                               data-parsley-errors-container="#error-gender2">Nữ</label>
                                                    <span id="error-gender" class="error">Giới tính không được bỏ trống</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    {{--phone--}}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group" style="display: block;">
                                                    <label class="control-label control-label-left col-sm-4"
                                                           for="phone">Số điện thoại<a
                                                                style="color: red">*</a></label>
                                                    <div class="controls col-sm-8">

                                                        <input id="phone" name="phone" type="tel" max="15"
                                                               maxlength="15" tabindex="104"
                                                               class="form-control k-textbox required"
                                                               data-role="text" onkeypress="return isNumber(event)"
                                                               data-parsley-errors-container="#error-phone">
                                                        <span id="error-phone" class="error">Số điện thoại không được bỏ trống</span>
                                                    </div>

                                                </div>
                                            </div>

                                            {{--email--}}
                                            <div class="col-md-12">
                                                <div class="form-group" style="display: block;">
                                                    <label class="control-label control-label-left col-sm-4"
                                                           for="email">Email<a
                                                                style="color: red">*</a></label>
                                                    <div class="controls col-sm-8">

                                                        <input id="email" name="email" type="email"
                                                               class="form-control k-textbox required"
                                                               data-role="text" maxlength="50" tabindex="105"
                                                               data-parsley-errors-container="#error-email"
                                                               pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/>
                                                        <span id="error-email" class="error">Địa chỉ email không được bỏ trống</span>
                                                    </div>

                                                </div>
                                            </div>

                                            {{--Branch--}}
                                            <div class="col-md-12">
                                                <div class="form-group" style="display: block;">
                                                    <label class="control-label control-label-left col-sm-4"
                                                           for="branch">Trực thuộc ban<a
                                                                style="color: red">*</a></label>
                                                    <div class="controls col-sm-8">
                                                        <select class="form-control selectpicker required" id="branch" tabindex="106"
                                                                name="branch">
                                                            @foreach($departments as $value)
                                                                <?php if ($value->code == 'DEPARTMENT03'): ?>
                                                                <optgroup label="{{ $value->value }}">
                                                                    @foreach($departments as $value2)
                                                                        <?php if($value2->code != 'DEPARTMENT03' && strpos($value2->code, $value->code) !== false): ?>
                                                                        <option value="{{ $value2->code }}">{{ $value2->value }}</option>
                                                                        <?php endif; ?>
                                                                    @endforeach
                                                                </optgroup>
                                                                <?php elseif(!(strpos($value->code, 'DEPARTMENT03') !== false)): ?>
                                                                <option value="{{ $value->code }}">{{ $value->value }}</option>
                                                                <?php endif; ?>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>

                                            {{--Chức vụ--}}
                                            <div class="col-md-12">
                                                <div class="form-group" style="display: block;">
                                                    <label class="control-label control-label-left col-sm-4"
                                                           for="option">Chức vụ<a
                                                                style="color: red">*</a></label>
                                                    <div class="controls col-sm-8">
                                                        <select class="form-control selectpicker required" id="option" tabindex="107"
                                                                name="option">
                                                            @foreach($positions as $value)
                                                                <option value="{{ $value->code }}">{{ $value->value }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>

                                            {{--Image--}}
                                            <div class="col-md-12">
                                                <div class="form-group" style="display: block;">
                                                    <label class="control-label control-label-left col-sm-4"
                                                           for="option">Ảnh đại diện</label>
                                                    <div class="controls col-sm-8">
                                                        <input id="upload-image" name="profile_image"
                                                               type="file" value="" tabindex="108"
                                                               onchange="readURL(this);"
								style="display: none;">
							<div style="width:100%">
                                                        	<input type="button" value="Browse..." onclick="document.getElementById('upload-image').click();" />
                                                            <input type="hidden" id="hidden-image" name="hidden_image" value="" />
                                                        </div>
                                                        <span id="upload-image-error" class="error">Ảnh đại diện không được bỏ trống</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="preview-img text-center" style="padding-bottom: 20px">
                                                <img id="blah" src="{{ asset('admin/images/no_image.png') }}"
                                                     alt="no image" width="80%"/>

                                            </div>
                                        </div>
                                    </div>

                                    {{--Address--}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="display: block;">
                                                <label class="control-label control-label-left col-sm-2"
                                                       for="address">Địa chỉ<a
                                                            style="color: red">*</a></label>
                                                <div class="controls col-sm-10">

                                                    <textarea id="address" name="address" rows="3" tabindex="109"
                                                           class="form-control k-textbox required"
                                                           data-role="text"
                                                           data-parsley-errors-container="#address"
                                                           pattern="^[a-zA-Z1-9].*" maxlength="500"></textarea>
                                                    <span id="error-address" class="error">Địa chỉ không được bỏ trống</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    {{--Company--}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="display: block;">
                                                <label class="control-label control-label-left col-sm-2"
                                                       for="company">Nơi công tác<a
                                                            style="color: red">*</a></label>
                                                <div class="controls col-sm-10">

                                                    <textarea id="company" name="company"
                                                           class="form-control k-textbox required"
                                                           data-role="text" tabindex="110"
                                                           data-parsley-errors-container="#company"
                                                              pattern="^[a-zA-Z1-9].*" maxlength="500"></textarea>
                                                    <span id="error-company" class="error">Đơn vị công tác không được bỏ trống</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    {{--Status--}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group" style="display: block;">
                                                <label class="control-label control-label-left col-sm-2"
                                                       for="status">Trạng thái</label>
                                                <div class="controls col-sm-10">

                                                    <label class="radio-inline" for="status1">
                                                        <input type="radio" value="1" tabindex="111"
                                                               id="status1" name="status"
                                                               data-parsley-errors-container="#error-status1">Hoạt
                                                        động</label>
                                                    <label class="radio-inline" for="status2"
                                                           style="display: inline-block">
                                                        <input type="radio" value="0" id="status2" name="status" tabindex="112"
                                                               data-parsley-errors-container="#error-status2">Không
                                                        hoạt động</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer edit">
                    <button type="submit" class="btn btn-primary" tabindex="113"
                            id="submitCreate"><?= Lang::get('offices.save') ?></button>
                    <button type="button" class="btn btn-warning"
                            data-dismiss="modal"><?= Lang::get('offices.close') ?></button>
                </div>
                <div class="modal-footer view">
                    <button type="button" class="btn btn-warning" tabindex="114"
                            data-dismiss="modal"><?= Lang::get('offices.close') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="font-size: 20px;">Xem thông tin Thành Viên</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"
                          style="font-size: 39px; width: 30%;float: right;text-align: right;padding-right: 15px;">&times;</span>
                </button>
            </div>
            <div class="content">

            </div>
        </div>
    </div>
</div>

<script>
    jQuery(window).ready(function () {
        jQuery("#upload-image").change(function () {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if (jQuery.inArray(jQuery(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                jQuery('#upload-image-error').text("Only formats are allowed : " + fileExtension.join(', '));
            }
        });
    });

    var inputValidate = true;
    jQuery('#officesaddnew').submit(function (e) {
        inputValidate = true;
        jQuery('.form-control.required').each(function () {
            var input = jQuery(this);
            if (input.val() == '') {
                inputValidate = false;
                input.addClass('is-invalid');
                input.parent().find('.error').show();
            } else {
                input.removeClass('is-invalid');
                input.parent().find('.error').hide();
            }
        });


        if (inputValidate == false) e.preventDefault();
        jQuery("#officesaddnew button[type=submit]").attr("disabled", "disabled");

        if (inputValidate == true) jQuery('#officesaddnew').submit();
        jQuery("#officesaddnew button[type=submit]").prop('disabled', false);
    });
    function readURL(input) {
        if (validateImg() == true) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        } else {
            jQuery('#blah').attr('src', '{{ asset('admin/images/no_image.png') }}');
        }
    }

    function editOffices(key) {
        jQuery('.error').hide();
        jQuery('.is-invalid').removeClass('is-invalid');
        jQuery('#upload-image-error').hide();
        jQuery('#upload-image').val('');
        jQuery("#createnewModal").modal();
        jQuery("#exampleModalLabel").text("Thay đổi thông tin Thành Viên");
        jQuery('form-control').each(function () {
            jQuery(this).val('');
        });
        jQuery('#branch').val(jQuery('#branch option:first').val());
        jQuery('#option').val(jQuery('#option option:first').val());
        jQuery('input[type="radio"]').each(function () {
            jQuery(this).prop('checked', false);
        });
        jQuery('.form-control').each(function () {
            var inputForm = jQuery(this);
            var nameForm = jQuery(this).attr('name');
            var dataByKey = collectionData[key];
            $.each(dataByKey, function (k, v) {
                if (nameForm == k) inputForm.val(v);
                if (k == 'profile_image') {
                    jQuery('#blah').attr('src', '{{url('/files')}}' + v);
                    jQuery('#hidden-image').attr('value', v);
                } 
                if (inputForm.hasClass('selectpicker') && nameForm == 'branch' && k == 'branch' && v != '') inputForm.val(v);
                if (inputForm.hasClass('selectpicker') && nameForm == 'option' && k == 'option_code' && v != '') inputForm.val(v);
            });
        });
        jQuery('input[type="radio"]').each(function () {
            var inputForm = jQuery(this);
            var dataByKey = collectionData[key];
            $.each(dataByKey, function (k, v) {
                if (k == 'status' && v == inputForm.val()) inputForm.prop('checked', true);
                if (k == 'gender') {
                    if (v == 0) {
                        $('#gender2').prop('checked', true);
                    }
                    else {
                        $('#gender1').prop('checked', true);
                    }
                }
            });
        });
        isViewModal(0);
    }

    function viewOffices(key) {
        editOffices(key);
        jQuery("#exampleModalLabel").text("Xem thông tin Thành Viên");
        isViewModal(1);
    }

    function addnewOffice() {
        jQuery('.error').hide();
        jQuery('.is-invalid').removeClass('is-invalid');
        jQuery('#upload-image-error').hide();
        jQuery('#upload-image').val('');
        jQuery("#exampleModalLabel").text("Thêm mới Thành Viên");
        jQuery('.form-control').each(function () {
            jQuery(this).val('');
        });
        jQuery('input[type="radio"]').each(function () {
            jQuery(this).prop('checked', false);
        });
        jQuery('#gender2').prop('checked', true);
        jQuery('#status2').prop('checked', true);
        jQuery('#branch').val(jQuery('#branch option:first').val());
        jQuery('#option').val(jQuery('#option option:first').val());
        jQuery('#blah').attr('src', '{{ asset('admin/images/no_image.png') }}');
        jQuery("input[type='text']")[0].focus();
        isViewModal(0);
    }


    function deleteOffices(key) {
        jQuery("#deleteModal").modal();
        var dataByKey = collectionData[key];
        var nameKey = dataByKey['fullname'];
        var idKey = dataByKey['id'];
        jQuery('#inputid-to-delete').val(idKey);
        jQuery('#id-em').text(nameKey);
    }

    function printErrorMsg(msg) {
        jQuery(".print-error-msg").find("ul").html('');
        jQuery(".print-error-msg").css('display', 'block');
        jQuery.each(msg, function (key, value) {
            jQuery(".print-error-msg").find("ul").append('<li>' + value + '</li>');
        });
    }

</script>
