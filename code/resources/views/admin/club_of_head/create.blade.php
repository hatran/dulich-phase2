<form method="POST" id="branchs-form-create" class="form-create form-horizontal" action="{{ URL::route('club_of_head_store') }}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="code" class="col-md-4 control-label">Tên CLB<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <select name="code" id="code" class="form-control" tabindex="100"></select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="phone" class="col-md-4 control-label">Số điện thoại</label>
                <div class="col-md-8">
                    <input class="form-control" onkeypress="return isNumber(event)" name="phone" tabindex="101" type="text" id="phone" onpaste="return onPasteNumber(event)" maxlength="20">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-8">
                    <input class="form-control" name="email" type="text" id="email" maxlength="100" tabindex="102">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-4 control-label">Trạng thái</label>
                <div class="col-md-4">
                    <label class="radio-inline">
                        <input checked="" value="{{\App\Models\Branches::BRANCHES_ACTIVE}}" type="radio" name="status" tabindex="103"> Hoạt động
                    </label>
                </div>
                <div class="col-md-4">
                    <label class="radio-inline">
                        <input value="{{\App\Models\Branches::BRANCHES_DEACTIVE}}" type="radio" name="status" tabindex="104"> Không hoạt động
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-12">
                    <label for="address" class="control-label">Địa chỉ</label>
                    <textarea class="form-control" rows="2" name="address" id="address" maxlength="350" style="resize: none;" tabindex="105"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                <label for="note" class="control-label">Ghi chú</label>
                <textarea class="form-control" rows="2" name="note" id="note" maxlength="500" style="resize: none;" tabindex="106"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <input type="button" id="branchs-form-save" value="Lưu" class="btn btn-primary" tabindex="107">
                        <input type="button" data-dismiss="modal" value="Thoát" class="btn btn-warning" tabindex="108">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
renderSelect2($('#code'), 'Tên CLB');
    function renderSelect2(_element, _name) {
        var listPerson;
        setTimeout(function () {
            _element.select2({
                placeholder: _name,
                ajax: {
                    url: '{{URL::route("club_of_head_ajax_get_clb")}}',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                          search: params.term,
                          unuse: 'true'
                        }
                        return query;
                      },
                    processResults: function (data) {
                        listPerson = data;
                        var objs = [];
                        let i = 0;
                        $.each(data, function (key, value) {
                            objs[i] = {'id': key, 'text': value};
                            i++;
                        });
                        return {
                            results: objs
                        };
                    }
                },
				language : 'vi'
            });
        }, 1);
    }
</script>
