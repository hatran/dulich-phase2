<form method="POST" id="branchs-form-create" class="form-horizontal" action="{{ URL::route('club_of_head_update', ['id' => $detail->id]) }}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="code" class="col-md-4 control-label">Tên CLB<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <select name="code" id="code" class="form-control" tabindex="200">
                        <option value="{{$detail->code}}">{{$detail->name}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="phone" class="col-md-4 control-label">Điện thoại</label>
                <div class="col-md-8">
                    <input value="{{$detail->phone}}" class="form-control" tabindex="201" name="phone" type="text" id="phone" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="20">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-8">
                    <input value="{{$detail->email}}" class="form-control" name="email" tabindex="202" type="text" id="email" maxlength="100">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-4 control-label">Trạng thái</label>
                <div class="col-md-4">
                    <label class="radio-inline">
                        <input tabindex="203" <?= $detail->status == \App\Models\Branches::BRANCHES_ACTIVE ? 'checked="checked"' : '' ?> value="{{\App\Models\Branches::BRANCHES_ACTIVE}}" type="radio" name="status"> Hoạt động
                    </label>
                </div>
                <div class="col-md-4">
                    <label class="radio-inline">
                        <input tabindex="204" <?= $detail->status == \App\Models\Branches::BRANCHES_DEACTIVE ? 'checked="checked"' : '' ?> value="{{\App\Models\Branches::BRANCHES_DEACTIVE}}" type="radio" name="status"> Không hoạt động
                    </label>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="pull-right alert alert-danger error-mess-branche hidden"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-12">
                    <label for="address" class="control-label">Địa chỉ</label>
                    <textarea class="form-control" rows="2" name="address" id="address" maxlength="350" tabindex="205">{{ htmlentities($detail->address) }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="note" class="control-label">Ghi chú</label>
                    <textarea class="form-control" rows="2" name="note" id="note" maxlength="500" tabindex="206">{{ htmlentities($detail->note) }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <input type="button" id="branchs-form-save" value="Lưu" class="btn btn-primary" tabindex="207">
                        <input type="button" data-dismiss="modal" value="Thoát" class="btn btn-warning" tabindex="208">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    renderSelect2($('#code'), 'Tên CLB');
    function renderSelect2(_element, _name) {
        var listPerson;
        setTimeout(function () {
            _element.select2({
                placeholder: _name,
                ajax: {
                    url: '{{URL::route("club_of_head_ajax_get_clb")}}',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            unuse: 'true'
                        }
                        return query;
                    },
                    processResults: function (data) {
                        listPerson = data;
                        var objs = [];
                        let i = 0;
                        $.each(data, function (key, value) {
                            objs[i] = {'id': key, 'text': value};
                            i++;
                        });
                        return {
                            results: objs
                        };
                    }
                },
				language : 'vi'
            });
        }, 400);
    }
    setTimeout(function () {
        $('#code').focus();
    }, 500);
</script>
