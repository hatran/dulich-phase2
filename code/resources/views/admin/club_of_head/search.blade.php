<form class="page-wrap" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tên CLB
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <select tabindex="1" id="search_option_code" name="search_option_code" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($listCLB))
                @foreach($listCLB as $key => $val)
                @php $selected = Input::get('search_option_code', '') == $key ? 'selected="selected"' : ''; @endphp
                <option {{$selected}} value="{{$key}}">{{$val}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Địa chỉ
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="2" id="search_address" placeholder="Địa chỉ CLB thuộc Hội" type="text" name="search_address" value="{{ Input::get('search_address', '') }}" maxlength="50">
        </div>
        
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số điện thoại
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="3" id="search_phone" type="text" placeholder="Số điện thoại" name="search_phone" value="{{ Input::get('search_phone') }}" maxlength="20" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Email
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="4" id="search_email" type="text" placeholder="Địa chỉ email" name="search_email" value="{{ Input::get('search_email') }}" maxlength="100">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="5" id="search_status" name="search_status" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($statusOption))
                    @foreach($statusOption as $key => $val)
                        @php $selected = is_numeric(Input::get('search_status', '')) && Input::get('search_status', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="6" type="submit" onclick="return submitForm();" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<script>
    $(document).keypress(function (e) {
        if($('#formSearch input:focus, #formSearch button:focus').length != 0) {
            if (e.which === 13) {
                submitForm();
            }
        }
    });
    $(function () {
        $("#search_option_code").focus();
    });
    function submitForm() {
        $('#formSearch').submit();
    }
    ;
</script>