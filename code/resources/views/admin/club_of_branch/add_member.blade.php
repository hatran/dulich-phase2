<form method="POST" id="branchs-form-create" class="form-horizontal" action="{{ URL::route('club_of_branch_member_create', ['id' => $id]) }}">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tr>
                    <td class="tablebg">Họ tên</td>
                    <td colspan="3">
                        <select class="form-control" name="member_id" id="member_id" tabindex="1"></select>
                    </td>
                </tr>
                <tr>
                    <td class="tablebg">Ngày sinh</td>
                    <td><span id="birthday_label"></span></td>
                    <td class="tablebg">Số điện thoại</td>
                    <td><span id="firstMobile_label"></span></td>
                </tr>
                <tr>
                    <td class="tablebg">Email</td>
                    <td><span id="firstEmail_label"></span></td>
                    <td class="tablebg">Số thẻ HDV</td>
                    <td><span id="touristGuideCode_label"></span></td>
                </tr>
                <tr>
                    <td class="tablebg">Số thẻ hội viên</td>
                    <td><span id="member_code_label"></span></td>
                    <td class="tablebg">Ngôn ngữ hướng dẫn</td>
                    <td><span id="guideLanguage_label"></span></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <input type="button" id="branchs-form-save" value="Lưu" class="btn btn-primary" tabindex="2">
                        <input type="button" data-dismiss="modal" value="Thoát" class="btn btn-warning" tabindex="3">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
<script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
<script src="{{asset('admin/select2/js/i18n/vi.js')}}"></script>
<script>
var listPerson = [];
$(document).ready(function () {
    $("#member_id").select2({
        placeholder: "Chọn hội viên",
        minimumInputLength: 3,
        ajax: {
            url: "{{URL::route('club_of_branch_member_ajax_search')}}",
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    branchId: '{{$id}}'
                }
                return query;
            },
            processResults: function (data) {
                console.log(data);
                listPerson = data;
                var objs = [];
                $.each(data, function (key, value) {
                    objs[key] = {'id': value.id, 'text': value.fullName};
                    listPerson[value.id] = value;
                });
                return {
                    results: objs
                };
            },
        },
		language : 'vi'
    });
});
$("#member_id").on('select2:select', function (e) {
    let data = e.params.data;
    var secondMobie = '';
    $.each(listPerson[data.id], function (key, value) {
        if ($('span#' + key + '_label').length > 0) {
            if (key == 'birthday') {
                date = new Date(value);
                value = getFormattedDate(date);
            }
            $('span#' + key + '_label').html(value);
        }
        if (key == 'secondMobile' && value != '' && value != null) {
            secondMobie = ', '+ value;
        }

        if (key == 'secondMobile' && value == null) {
            secondMobile = '';
        }
    });
    var phonehmtl = $('span#firstMobile_label').html();
    $('span#firstMobile_label').html(phonehmtl + secondMobie);
});
function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return day + '/' + month + '/' + year;
}
</script>