<form class="page-wrap" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">Tên hội viên</div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="search_member_name" type="text" name="search_member_name" value="{{ Input::get('search_member_name') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">Số thẻ Hội Viên</div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="2" id="search_member_code" type="text" name="search_member_code" value="{{ Input::get('search_member_code') }}" maxlength="20">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số thẻ HDV
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="3" id="search_touristGuideCode" type="text" name="search_touristGuideCode" value="{{ Input::get('search_touristGuideCode') }}" maxlength="20" onkeypress="return isNumber(event)">
        </div>
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="4" type="submit" onclick="return submitForm();" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<script>
    $(document).keypress(function (e) {
        if($('#formSearch input:focus, #formSearch button:focus').length != 0) {
            if (e.which === 13) {
                submitForm();
            }
        }
    });
    $(function () {
        $("#search_member_name").focus();
    });
    function submitForm() {
        $('#formSearch').submit();
    }
    ;
</script>