<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="{{ asset('vendors/jQuery/dist/jquery.min.js') }}"></script>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .modal-frame-content {
                overflow-x: hidden;
            }
            .tableUser td.ajaxTitle {
                color: #144a8b;
                text-align: center;
            }
            .tableUser tr td {
                position: relative;
                word-wrap: break-word;
                white-space: normal !important;
            }
            .ajaxReq .error {
                border: 1px solid #fb0000;
            }
            .tableUser > tr {
                background-color: #EDF1F6;
            }
            .table-user-title{
                font-size: 14px;
                color: #144a8b;
                font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;
            }
            .table-bordered.tableUser > tbody > tr:hover {
                background-color: #EDF1F6;
            }
            .l_birthday {text-align: center;}
            .l_phone {text-align: right;}
            .error_border {-webkit-box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);}

            @media screen and (min-width: 768px) and (max-width: 992px) {
                .fixSpace {
                    position: relative !important;
                    right: 50px !important;
                }
            }
        </style>
    </head>
    <body>
        <div class="modal-frame-content">
            <div class="row">
                <div class="col-sm-3">Mã CLB thuộc Chi Hội</div>
                <div class="col-sm-9 fixSpace">{{ $detailBranch->code }}</div>
                <div class="col-sm-3">Tên CLB thuộc Chi Hội</div>
                <div class="col-sm-9 fixSpace">{{ $detailBranch->name }}</div>
                <hr>
            </div>
            <h4>Danh sách Ban chấp hành CLB thuộc Chi hội</h4>
            <div class="hidden ajax-message alert alert-success"></div>
            <table border="0" class="tableUser table table-bordered">
                <tr class="ajaxTitle">
                    <th class="text-center table-user-title" width="5%">STT</th>
                    <th class="text-center table-user-title" width="20%">Họ tên</th>
                    <th class="text-center table-user-title" width="18%">Chức vụ</th>
                    <th class="text-center table-user-title" width="13%">Ngày sinh</th>
                    <th class="text-center table-user-title" width="14%">Số điện thoại</th>
                    <th class="text-center table-user-title" width="20%">Email</th>
                    <th class="text-center table-user-title" width="10%">Chức năng</th>
                </tr>
                @if (!$listManager->count())
                <tr class="no-item">
                    <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                </tr>
                @else
                @foreach ($listManager as $key => $item)
                @php if ($item->employees == "") continue;@endphp
                <tr id="{{$item->id}}">
                    <td class="text-center"> {{$key + 1 }} </td>
                    <td class="l_fullname">{{ $item->employees->fullname }}</td>
                    <td class="l_position">{{ isset($listOption[$item->option_code]) ? $listOption[$item->option_code] : '' }}</td>
                    <td class="l_birthday">{{ htmlentities($item->employees->birthday) }}</td>
                    <td class="l_phone">{{ $item->employees->phone }}</td>
                    <td class="l_email">{{ $item->employees->email }}</td>
                    <td class="text-center">
                        <a href="javascript:;" id="{{$item->id}}" class="ajaxEdit">
                            <i class="fa fa-edit" style="font-size: 18px; color: #ff6600;"></i>
                        </a>
                        <a href="javascript:;" id="{{$item->id}}" class="ajaxDelete">
                            <i class="fa fa-times" style="font-size: 18px;"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>
        </div>
<script>
    var currentBranchId = '{{$detailBranch->id}}';
    var X_CSRF_TOKEN = '{{ csrf_token() }}';
    var ajaxSaveData = "{{ URL::route('club_of_branches_ajax_save_member') }}";
</script>
<script src="{{asset('admin/js/table_script.js')}}"></script>
<script>
    var columns = new Array("l_fullname", "l_position", "l_birthday", "l_phone", "l_email");
    var inputType = new Array("text", "select", "text", "text", "text");
    var maxLength = new Array('50', '', '20', '20', '100');
    var placeholder = new Array('Họ tên', 'Chức vụ', 'Ngày sinh', 'Số điện thoại', 'Email');
    var table = "tableUser";
    var selectOpt = @json($listOption);
// Set button class names 
            var savebutton = "ajaxSave";
    var deletebutton = "ajaxDelete";
    var editbutton = "ajaxEdit";
    var updatebutton = "ajaxUpdate";
    var cancelbutton = "cancel";

// Set highlight animation delay (higher the value longer will be the animation)
    var saveAnimationDelay = 3000;
    var deleteAnimationDelay = 1000;

// 2 effects available available 1) slide 2) flash
    var effect = "slide";
</script>
</body>
</html>
