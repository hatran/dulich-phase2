<form method="POST" id="branchs-form-create" class="form-horizontal" action="{{ URL::route('club_of_branch_update', ['id' => $detail->id]) }}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="code" class="col-md-4 control-label">Chi hội<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <select name="parent_id" class="form-control" tabindex="200" id="branchParentId">
                        <option value="">Chọn Chi hội</option>
                        @if (!empty($listBranches))
                        @foreach($listBranches as $key => $val)
                        <option @php if ($detail->parent_id == $key) {echo 'selected="selected"';} @endphp value="{{$key}}">{{$val}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="code" class="col-md-4 control-label">Tên CLB<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <select name="code" id="code" class="form-control" tabindex="201">
                        <option value="{{isset($optionDetail->code) ? $optionDetail->code : '' }}">{{isset($optionDetail->code) ? $optionDetail->value : '' }}</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="phone" class="col-md-4 control-label">Số điện thoại</label>
                <div class="col-md-8">
                    <input value="{{$detail->phone}}" class="form-control" name="phone" tabindex="202" type="text" id="phone" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="20">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="email" class="col-md-2 control-label">Email</label>
                <div class="col-md-4">
                    <input value="{{$detail->email}}" class="form-control" tabindex="203" name="email" type="text" id="email" maxlength="100">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-2 control-label">Trạng thái</label>
                <div class="col-md-2">
                    <label class="radio-inline">
                        <input <?= $detail->status == \App\Models\Branches::BRANCHES_ACTIVE ? 'checked="checked"' : '' ?> value="{{\App\Models\Branches::BRANCHES_ACTIVE}}" type="radio" name="status" tabindex="204"> Hoạt động
                    </label>
                </div>
                <div class="col-md-2">
                    <label class="radio-inline">
                        <input <?= $detail->status == \App\Models\Branches::BRANCHES_DEACTIVE ? 'checked="checked"' : '' ?> value="{{\App\Models\Branches::BRANCHES_DEACTIVE}}" type="radio" name="status" tabindex="205"> Không hoạt động
                    </label>
                </div>
                <div class="col-md-6">
                    <div class="alert alert-danger error-mess-branche hidden"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="address" class="control-label">Địa chỉ</label>
                    <textarea class="form-control" rows="3" name="address" id="address" tabindex="206" maxlength="350" style="resize: none;">{{ htmlentities($detail->address) }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                <label for="note" class="control-label">Ghi chú</label>
                <textarea class="form-control" rows="3" name="note" id="note" maxlength="500" tabindex="207" style="resize: none;">{{ htmlentities($detail->note) }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <input type="button" id="branchs-form-save" value="Lưu" class="btn btn-primary" tabindex="208">
                        <input type="button" data-dismiss="modal" value="Thoát" class="btn btn-warning" tabindex="209">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    renderSelect2($('#code'), 'Tên CLB');
    function renderSelect2(_element, _name) {
        var listPerson;
        setTimeout(function () {
            _element.select2({
                placeholder: _name,
                ajax: {
                    url: '{{URL::route("club_of_branch_ajax_get_clb")}}',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            unuse: 'true'
                        }
                        return query;
                    },
                    processResults: function (data) {
                        listPerson = data;
                        var objs = [];
                        let i = 0;
                        $.each(data, function (key, value) {
                            objs[i] = {'id': key, 'text': value};
                            i++;
                        });
                        return {
                            results: objs
                        };
                    }
                },
				language : 'vi'
            });
        }, 400);
    }
    setTimeout(function () {
        $('#branchParentId').focus();
    }, 500);
</script>
