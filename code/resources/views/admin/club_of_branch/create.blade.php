<form method="POST" id="branchs-form-create" class="form-create form-horizontal" action="{{ URL::route('club_of_branch_create_store') }}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="code" class="col-md-4 control-label">Chi hội<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <select name="parent_id" class="form-control" tabindex="100" id="branchParentId">
                        <option value="">Chọn Chi hội</option>
                        @if (!empty($listBranches))
                        @foreach($listBranches as $key => $val)
                        <option @php echo Input::get('parent_id', '') == $key ? 'selected' : '' @endphp value="{{$key}}">{{$val}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group group-clb">
                <label for="code" class="col-md-4 control-label">Tên CLB<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <select name="code" id="code" class="form-control" tabindex="101"></select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="phone" class="col-md-4 control-label">Số điện thoại</label>
                <div class="col-md-8">
                    <input class="form-control" name="phone" type="text" tabindex="102" id="phone" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="20">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-8">
                    <input class="form-control" name="email" type="text" id="email" maxlength="100" tabindex="103">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-2 control-label">Trạng thái</label>
                <div class="col-md-2">
                    <label class="radio-inline">
                        <input checked="" value="{{\App\Models\Branches::BRANCHES_ACTIVE}}" type="radio" name="status" tabindex="104"> Hoạt động
                    </label>
                </div>
                <div class="col-md-2">
                    <label class="radio-inline">
                        <input value="{{\App\Models\Branches::BRANCHES_DEACTIVE}}" type="radio" name="status" tabindex="105"> Không hoạt động
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="address" class="control-label">Địa chỉ</label>
                    <textarea class="form-control" rows="3" name="address" id="address" maxlength="350" tabindex="106" style="resize: none;"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                <label for="note" class="control-label">Ghi chú</label>
                <textarea class="form-control" rows="3" name="note" id="note" maxlength="500"  style="resize: none;" tabindex="107"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <input type="button" id="branchs-form-save" value="Lưu" class="btn btn-primary" tabindex="108">
                        <input type="button" data-dismiss="modal" value="Thoát" class="btn btn-warning" tabindex="109">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
var searchParentId = '';
renderSelect2($('#code'), '', parent);
function renderSelect2(_element, _name) {
    var listPerson;
    setTimeout(function () {
        _element.select2({
            disabled: true,
            placeholder: _name,
            ajax: {
                url: '{{URL::route("club_of_branch_ajax_get_clb")}}',
                dataType: 'json',
                data: function (params) {
                    var query = {
                        search: params.term,
                        unuse: 'true',
                        searchParentId: searchParentId
                    }
                    return query;
                  },
                processResults: function (data) {
                    listPerson = data;
                    var objs = [];
                    let i = 0;
                    $.each(data, function (key, value) {
                        objs[i] = {'id': key, 'text': value};
                        i++;
                    });
                    return {
                        results: objs
                    };
                }
            },
			language : 'vi'
        });
    }, 1);
}
$(document).ready(function() {
	if ($('select[name="parent_id"]').val() != '') {
        searchParentId = $('select[name="parent_id"]').val();
        $("#code").prop("disabled",false);
    } else {
        $("#code").prop("disabled",true);
    }
});
$('body').on('change', 'select[name="parent_id"]', function () {
    if ($("#code").find('option').length) {
        $("#code").find('option').remove();
    }
    if ($(this).val() != '') {
        searchParentId = $(this).val();
        $("#code").prop("disabled",false);
    } else {
        $("#code").prop("disabled",true);
    }
});
</script>
