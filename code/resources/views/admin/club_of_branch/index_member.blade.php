@extends('admin.layouts.app')
@section('title', 'Cập nhật Hội viên CLB')
@section('content')

<style>
    .error_border {-webkit-box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);}
    .total {line-height: 30px;padding: 6px 10px;color: #0000F0 !important;border-radius: 3px;font-size: 16px;border: 0;display: inline-block;}
    .form-horizontal .control-label {padding-top: 7px;margin-bottom: 0;text-align: left;}
    .fa{cursor: pointer}
    .fa-2x {font-size: 1.2em;}
    .select2{min-width:250px;}
    .tablebg{background-color: #EDF1F6;width: 20%}
    .error-member_id{display:inline-block;width:100%}
    .modal-title {
        margin: 0;
        font-weight: bold;
        float: left;
        font-size: 20px;
    }
	.select2-container--open {
		z-index: 9999;
	}
</style>
<div class="main-content">
    @include('admin.club_of_branch.search_member')
    <div class="clearfix"></div>
    <div class="page-wrap">
        @include('admin.layouts.message')
        <div class="total">Tổng số: {{$totalItem}}</div>
        <div class="" style="display:inline-block; float: right; height: 35px; line-height: 30px; max-width: 100%; border-radius: 2px;">
            <input type="button" data-href="{{ URL::route('club_of_branch_member_create', ['id' => $detailBranch->id]) }}" class="openPopup button btn-primary add-modal-member" value="Thêm mới" style="border: 1px solid #BBBABA">
            <input type="button" data-href="{{ URL::route('club_of_branch_index') }}" class="button btn-info back-to-parent-screen" value="CLB thuộc Chi Hội" style="border: 1px solid #BBBABA">
        </div>
        <div class="table-wrap">
            <table>
                <thead>
                    <tr class="header">
                        <td width="5%" class="text-center">STT</td>
                        <td width="25%" class="text-left">Họ tên</td>
                        <td width="8%" class="text-center">Ngày sinh</td>
                        <td width="7%" class="text-center">Số thẻ HDV</td>
                        <td width="5%" class="text-center">Số thẻ hội viên</td>
                        <td width="10%" class="text-center">Ngôn ngữ</td>
                        <td width="7%" class="text-center">Số điện thoại</td>
                        <td width="12%" class="text-center">Địa chỉ email</td>
                        <td width="30%" class="text-center">Địa chỉ liên hệ</td>
                        <td width="3%" class="text-center">Chức năng</td>
                    </tr>
                </thead>
                <tbody>
                    @if(!$listMember->count())
                    <tr>
                        <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                    </tr>
                    @else
                    @foreach ($listMember as $key => $item)
                    @php
                    $mobile = $item->firstMobile . (!empty($item->secondMobile) ? ',' . $item->secondMobile : '' );
                    $emailS = $item->firstEmail . (!empty($item->secondEmail) ? ','. $item->secondEmail : '' );
                    @endphp
                    <tr class="main-content">
                        <td class="text-center">{{$offsets + $key + 1 }}</td>
                        <td>{{ htmlentities($item->fullName) }}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($item->birthday)->format('d/m/Y') }}</td>
                        <td class="text-center">{{ $item->touristGuideCode }}</td>
                        <td class="text-center">{{ $item->member_code }}</td>
                        <td>
                            @php
                                $language = $item->guideLanguage;
                            @endphp
                            <?php $languageName = ""; ?>
                            @foreach(explode(",", $language) as $key => $lang)
                                @foreach($listLanguage as $data => $val)
                                    @if($data == $lang)
                                        <?php $languageName .= $val ."," ?>
                                    @endif
                                @endforeach
                            @endforeach
                        {{ rtrim($languageName, ",") }}
                        </td>
                        <td class="text-right">{{ $mobile }}</td>
                        <td class="text-left">{{ $emailS }}</td>
                        <td class="text-left">{{ htmlentities($item->address) }}</td>
                        <td class="text-center">
                            <a data-id="{{$detailBranch->id}}" title="Xóa Hội viên" data-title="{{ htmlentities($item->fullName) }}" href="javascript:;" data-href="{{ URL::route('club_of_branch_member_delete', array($item->id)) }}" style="padding-left: 5px" onclick="showDeleteModal($(this))">
                                <i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @include('admin.layouts.pagination')
    </div>
    <!-- Modal form to add a post -->
    <div id="branchesModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div id="branchesManagerModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Xóa Hội viên</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Bạn chắc chắn muốn xóa hội viên <span id="id-em" style="font-size:  20px;color:  blue;"></span> ra khỏi danh sách?</h4>
                    <br/>
                    <form id="delete_member" action="" method="post">
                        <input type="hidden" value="" name="branchesId" id="branchesId" />
                        {{ csrf_field() }}
                    </form>
                </div>
                <input type="hidden" id="id_delete" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="delete_member();">Xóa</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Thoát</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_embed')

<script>
    $(document).on('click', '.add-modal-member', function (event) {
        event.preventDefault();
        setTimeout(function () {
            $('#member_id').focus();
        }, 500);
    });
    $(document).on('click', '.back-to-parent-screen', function (event) {
        event.preventDefault();
        var url = $(this).attr("data-href");
        window.location.href = url;
    });
    var errorValidateForm = function (errors) {
        $('.status-message').remove();
        $('.text-danger').remove();
        $('.form-control').removeClass('error_border');
        for (var errorType in errors) {
            for (var i in errors[errorType]) {
                $('[name="' + errorType + '"]').addClass('error_border').parent().append('<span class=" text-danger error-' + errorType + '">' + errors[errorType][i] + '</span>');
            }
        }
    };
    $('body').on('click', '.openPopup', function () {
        var dataURL = $(this).attr('data-href');
        $('#branchesModal .modal-body').load(dataURL, function () {
            $('#branchesModal .modal-title').html('Thêm mới hội viên');
            $('#branchesModal').modal({show: true});
        });
    }).on('click', '#branchs-form-save', function (e)
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            cache: false,
            "type": $('#branchs-form-create').attr('method'),
            "url": $('#branchs-form-create').attr('action'),
            "data": $('#branchs-form-create').serialize(),
            "dataType": "json"
        }).done(function (result) {
            if (result.savedData === false) {
                if (typeof result.errorMessages !== 'undefined')
                {
                    errorValidateForm(result.errorMessages);
                }
            } else {
                $('#branchesModal').modal('hide');
                toastr.success('Thành công!', 'Thêm mới hội viên', {timeOut: 2000});
                setTimeout(function () {// wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 2000);
            }
        });

        return false;
    });
    function showDeleteModal(__this) {
        var dataURL = __this.attr('data-href');
        $('#id-em').text(__this.attr('data-title'));
        $('#delete_member').attr('action', dataURL);
        $('#branchesId').val(__this.attr('data-id'));
        $('#deleteModal').modal({show: true});
        return false;
    }
    function delete_member() {
        $('#delete_member').submit();
    }
    
</script>
@endsection
