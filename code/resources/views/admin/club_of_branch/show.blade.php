<style>
    .div-break {
        position: relative;word-wrap: break-word;word-break:break-all;white-space: normal !important;
        max-width: 260px;
    }
</style>
<table class="table table-bordered">
    <tr>
        <td>
            <label for="code" class="col-md-4 control-label">Mã</label>
            <div class="col-md-8">
                {{$detail->code}}
            </div>
        </td>
        <td>
            <label for="code" class="col-md-4 control-label">Chi hội</label>
            <div class="col-md-8">
                <?= isset($listBranches[$detail->parent_id]) ? $listBranches[$detail->parent_id] : '' ?>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <label for="name" class="col-md-4 control-label">Tên CLB thuộc Chi Hội</label>
            <div class="col-md-8">
                {{$detail->name}}
            </div>
        </td>
        <td>
            <label for="phone" class="col-md-4 control-label">Số điện thoại</label>
            <div class="col-md-8">
                {{$detail->phone}}
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <label for="email" class="col-md-4 control-label">Email</label>
            <div class="col-md-4">
                {{$detail->email}}
            </div>
        </td>
        <td>
            <label class="col-md-4 control-label">Trạng thái</label>
            <div class="col-md-8">
                <?= isset(\App\Models\Branches::$statusLabel[$detail->status]) ? \App\Models\Branches::$statusLabel[$detail->status] : ''; ?>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <label for="address" class="col-md-4 control-label">Địa chỉ</label>
            <div class="col-md-8">
            <div class="div-break">{{htmlentities($detail->address)}}</div>
            </div>
        </td>
        <td>
            <label for="note" class="col-md-4 control-label">Ghi chú</label>
            <div class="col-md-8">
                <div class="div-break">{{htmlentities($detail->note)}}</div>
            </div>
        </td>
    </tr>
</table>
<div class="row">
    <div class="form-group">
        <div class="col-sm-12">
            <div class="pull-right">
                <input type="button" data-dismiss="modal" value="Thoát" class="btn btn-warning" tabindex="1">
            </div>
        </div>
    </div>
</div>