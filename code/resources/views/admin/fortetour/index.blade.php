
@extends('admin.layouts.app')
@section('title', 'Quản trị Danh mục')

@section('content')
    <div class="page-wrap">
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.message')
            @include('admin.fortetour.search')
            <div class="total" style="margin-bottom: 10px">Tổng số : {{$count_news}}<input class="import_view-modal button btn-primary btn-function add-modal" value="Thêm mới" id="btnAddForte" style="float:  right;width: 120px; height: 37px; line-height: 24px; padding: 6px 12px !important; cursor: pointer" data-file-id="-1"></div>

            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%"><div class="th-inner sortable both">STT</div></td>
                        <td width="35%"><div class="th-inner sortable both">Tên tuyến</div></td>
                        <td width="35%"><div class="th-inner sortable both">Loại tuyến</div></td>
                        <td width="10%"><div class="th-inner sortable both">Trạng thái</div></td>
                        <td><div class="th-inner sortable both">Chức năng</div></td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objNews) == 0)
                        <tr>
                            <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objNews as $key => $value)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                  
                                <td class="text-left">
                                    <p title="{{$value->name}}">
                                        {{ mb_strlen($value->name, 'UTF-8') > 100 ? mb_substr($value->name, 0, 100, 'utf-8') .'...' :  $value->name  }}
                                    </p>
                                </td>

                                <td class="text-left">
                                    @foreach($options as $k => $v)
                                        @if ($v->id == array_get($response_view, 'id', '')){{$v->name}}
                                        @endif
                                    @endforeach
                                </td>
                                <td class="text-left"> {{ $value->status == 0 ? 'Lưu nháp' : 'Hoạt động' }} </td>

                                <td class="text-center">
                                    {{--<a class="btn-function show-modal" href="{{ url('/officesys/categories/detail/' . $value->id) }}" data-toggle="tooltip" data-placement="left" title="Xem" data-id="{{ $value->id }}">
                                        <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                    </a>
                                    <a class="btn-function edit-modal" href="{{ url('/officesys/categories/' . $value->id) }}" data-toggle="tooltip" data-placement="left" title="Sửa" data-id="{{ $value->id }}">
                                        <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                    </a>
                                    @if (Auth::user()->role == $admin_user)
                                        <a class="btn-function delete-modal" href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Xoá" onclick="delete_intro({{ $value->id }})">
                                            <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                        </a>
                                    @endif--}}

                                    {{--<a class="btn-function add-modal" id="btnAddForte" href="#" data-toggle="tooltip" data-placement="left" title="Thêm tuyến" data-id="{{ $value->id }}" data-status="{{ $value->status }}" data-approved="">
                                        <span class="glyphicon glyphicon-plus" style="font-size: 16px; color: #0000ff; "></span>
                                    </a>--}}
                                    <a class="btn-function show-modal" href="#" data-toggle="tooltip" data-placement="left" title="Xem danh mục" data-id="{{ $value->id }}" data-option="{{array_get($response_view, 'id', '')}}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                    </a>
                                    <a class="btn-function edit-modal" href="#" data-toggle="tooltip" data-placement="left" title="Sửa danh mục" data-id="{{ $value->id }}" data-option="{{array_get($response_view, 'id', '')}}" data-status="{{ $value->status }}" data-approved="">
                                        <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                    </a>
                                    <a class="btn-function" href="javascript:void(0);" data-toggle="tooltip" data-placement="left" title="Xoá danh mục" onclick="delete_intro({{ $value->id }}, '{{array_get($response_view, 'id', '')}}', '{{strip_tags($value->name)}}')" data-id="{{ $value->id }}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>

    @include('admin.fortetour.detail')
    @include('admin.fortetour.register')
    @include('admin.fortetour.edit')
    @include('admin.fortetour.delete')


@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }
        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .sortable{
            color: #144a8b;
            font-weight: bold;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }
        #closeButton ul {
            padding-left: 0px !important;
        }
        #id{
            -moz-appearance: textfield;     height: 35px;     line-height: 35px;     padding-right: 30px;     position: relative;     width: 100%;border: 1px solid #bbbaba;border-radius: 2px;display: block;max-width: 100%; margin-bottom: 10px;padding-left: 10px;
        }
        .rq-star{
            color: red
        }
        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }
        span.error-msg {
            color: #a94442;
        }
        span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }
        .total {
            border: 0 none;
            border-radius: 3px;
            color: #0000f0 !important;
            font-size: 16px;
            line-height: 30px;
            padding: 6px 10px;
        }


        .modal-title {
            margin: 0;
            line-height: 1.42857143;
            font-weight: bold;
            width: 50%;
            float: left;
            font-size: 20px;
        }
    </style>
    <script>
        function delete_intro(id, option, txt) {
            jQuery("#deleteModal").modal();
            jQuery('#id-em').text(txt);
            jQuery('#id_delete').val(id);
            jQuery('#option_delete').val(option);
        }
        function deleteCat() {
            window.location = '/officesys/fortetour/deleteall/'+jQuery('#id_delete').val()+'/'+jQuery('#option_delete').val();
        }
        function convertStr(txt){
            var str = txt;
            str= str.toLowerCase();
            str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str= str.replace(/đ/g,"d");
            str= str.replace(/\s/g, '');
            //str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
            //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
            //str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
            return str;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#btnsubmit").click();
                }
            }
        });
        function clearErrorFields(postfix) {
            $('#nameAll'+postfix).removeClass('error_border');
            $('#code'+postfix).removeClass('error_border');
            $('#optionAll'+postfix).removeClass('error_border');
            $('.errorNameAll'+postfix).addClass('hidden');
            $('.errorOptionAll'+postfix).addClass('hidden');
            $('.errorcode'+postfix).addClass('hidden');
        }

        var url = "{{ url('/officesys/fortetour') }}"
        function htmlEntities(str) {
            return str;
        }
        function showPopup(dmId, option, postfix) {
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';

            switch (postfix) {
                case '_add':
                    title = 'Thêm mới tuyến';
                    canEdit = true;
                    break;

                case '_edit':
                    title = 'Cập nhật thông tin tuyến';
                    $("#id_edit").val(dmId);
                    $("#option_edit").val(option);
                    canEdit = true;
                    break;

                default:
                    title = 'Chi tiết thông tin tuyến';
            }
            clearErrorFields(postfix);
            $('.modal-title'+postfix).text(title);

            var procode_all = $("#procode_all").val();
            if(postfix != '_add'){
//                $(modalId).modal('show');
//                $(modalId + ' .modal-dialog').loader('show');
                $.ajax({
                    url: url + "/" + dmId + "/" + option,
                    type: 'GET',
                    success: function (response) {
//                        console.log(response);
                        var optionEditShow = '';
                        var nameAll = '';
                        if(response.deleted == 'error'){
                            document.location.reload(true);
                        }else{
                            $(modalId).modal('show');
                            //$(modalId + ' .modal-dialog').loader('show');
                            $.each(response.objOptions, function( index, value ) {
                                if(value.id == procode_all){
                                    optionEditShow = value.name;
                                }
                            });
                            //$(modalId + ' .modal-dialog').loader('hide');
                            nameAll = response.objData.name;
                            if (canEdit === true) {
                                $('#nameAll'+postfix).val(nameAll);
                                $('#optionEditShow'+postfix).text(optionEditShow);
                            } else {
                                $('#nameAll'+postfix).text(nameAll);
                                $('#optionEditShow'+postfix).text(optionEditShow);
                            }
                            if (postfix == '_edit') {
                                setTimeout(function () {
                                    $("#nameAll"+postfix).focus();
                                }, 500);
                            }
                        }
                    }
                });
            }else{
                $(modalId).modal('show');
                $("#nameAll"+postfix).val('');
                $("#optionAll"+postfix).val('');
                setTimeout(function () {
                    $("#nameAll"+postfix).focus();
                }, 500);
            }
        }

        $(document).on('click', '.show-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            var option = $(this).data('option');
            showPopup(dmId, option, '_show');
        });

        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            showPopup('','', '_add');
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            var option = $(this).data('option');
            $('input[type=hidden]#dm-id').val(dmId);
            showPopup(dmId, option, '_edit');
        });


        function doAction(dmId, postfix) {
            var procode_all = $("#procode_all").val();
            var action = 'POST';
            var modalId = '#' + postfix + 'Modal';

            var nameAll = $("#nameAll" + postfix);
            var optionAll = $('#optionAll' + postfix);
            if (postfix == '_add')  dmId = 'new';
            if (postfix == '_add' || postfix == '_edit') {
                clearErrorFields(postfix)
            }
            var flg = 0;
            if (postfix == '_add' || postfix == '_edit') {
                if (postfix == '_add') {
                    if (optionAll.val() == '') {
                        $("#optionAll" + postfix).addClass('error_border');
                        $(".errorOptionAll" + postfix).html('Loại tuyến là trường bắt buộc phải chọn');
                        $(".errorOptionAll" + postfix).removeClass('hidden');
                        flg = 1;
                    } else {
                        $(".errorOptionAll" + postfix).html('');
                        $(".errorOptionAll" + postfix).addClass('hidden');
                        $("#optionAll" + postfix).removeClass('error_border');
                    }
                }

                if (nameAll.val().trim() == '') {
                    $("#nameAll"+postfix).addClass('error_border');
                    $(".errorNameAll"+postfix).html('Tên tuyến là trường bắt buộc phải điền');
                    $(".errorNameAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorNameAll"+postfix).html('');
                    $(".errorNameAll"+postfix).addClass('hidden');
                    $("#nameAll"+postfix).removeClass('error_border');
                }
            }

            if (flg == 0) {
                $("#forteForm"+postfix).submit();
                var modalId = '#' + postfix + 'Modal';
                $(modalId).modal('hide');
            }
        }

        $('.modal-footer').on('click', '.add', function () {
            doAction('', '_add');
        });

        $('.modal-footer').on('click', '.edit', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_edit');
        });

        $('.modal-footer').on('click', '.delete', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_delete');
        });
        $(document).ready(function () {
            var nameall = $("#nameall").val();
            $("#nameall").val('');
            setTimeout(function() {
                $('#nameall').focus() ;
                $("#nameall").val(nameall);
            }, 100);
        });
    </script>
@endsection