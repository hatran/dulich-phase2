<div id="_editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_edit"></h4>
            </div>
            <form action="{{url('/officesys/fortetour/check_forte')}}" id="forteForm_edit" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id_edit" id="id_edit" class="form-control" value=""/>
                <input type="hidden" name="option_edit" id="option_edit" class="form-control" value=""/>
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label" style="padding-top: 7px">Tên tuyến<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="nameAll_edit" id="nameAll_edit" maxlength="191" tabindex="201">
                                        <span class="errorNameAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row" style="margin-bottom: 30px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="code">Loại tuyến</label>
                                    <div class="col-md-9">
                                        <span id="optionEditShow_edit"></span>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary edit" type="button" tabindex="202">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="203">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
