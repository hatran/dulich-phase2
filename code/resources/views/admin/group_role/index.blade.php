@extends('admin.layouts.app')
@section('title', 'Quản trị Quyền truy cập')
@section('content')
<style>
    .error_border {-webkit-box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);}
    .total {line-height: 30px;padding: 6px 10px;color: #0000F0 !important;border-radius: 3px;font-size: 16px;border: 0;display: inline-block;}
    .form-horizontal .control-label {padding-top: 7px;margin-bottom: 0;text-align: left;}
    .fa{cursor: pointer}
    .fa-2x {font-size: 1.2em;}
    .table-wrap table tr td {position: relative;word-wrap: break-word;white-space: normal !important;}
    /*.table-wrap table span {font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;}*/
    .btn-primary.btn-custom{
        color: #fff;
        height: 35px;
        line-height: 35px;
        border: 1px solid #BBBABA;
        padding: 0 10px;
        max-width: 100%;
        display: block;
        border-radius: 2px;
    }
    .rq-star {color: #ff0000;}
</style>
<div class="main-content">
    @include('admin.group_role.search')
    <div class="clearfix"></div>
    <div class="page-wrap">
        <!-- <div class="heading">Kết quả tìm kiếm</div> -->
        @include('admin.layouts.message')
        <div class="total">Tổng số: {{$groups->total()}}</div>
        <div class="field-wrap" style="display:inline-block;float: right">
            <a tabindex="6" class="btn-custom button btn-primary" href="{{ URL::route('group_role.create') }}">Thêm mới</a>
        </div>
        <div class="table-wrap">
            <table>
                <thead>
                <tr>
                    <td width="5%">STT</td>
                    <td width="25%">Tên Quyền truy cập</td>
                    <td width="33%">Mô tả</td>
                    <td width="10%">Ngày tạo</td>
                    <td width="7%">Chức năng</td>
                </tr>
                </thead>
                <tbody>
                @if (!$groups->count())
                    <tr>
                        <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                    </tr>
                @else
                    @foreach($groups as $key => $group)
                        <tr>
                            <td class="text-center"> {{ $offsets + $key + 1 }} </td>
                            <td class="text-left">{{ htmlentities($group->name) }}</td>
                            <td class="text-left"> {{ htmlentities($group->description) }} </td>

                            <td class="text-center"> {{ date('d/m/Y', strtotime($group->created_at)) }} </td>
                            <td class="text-center">
                                <a title="Cập nhật Quyền truy cập" href="{{ URL::route('group_role.edit', ['id' => $group->id]) }}">
                                    <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                </a>
                                <a title="Xóa Quyền truy cập" href="javascript:;" data-href="{{ URL::route('group_role.destroy', ['id' => $group->id]) }}" style="padding-left: 10px" onclick="delete_groups($(this))">
                                    <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        @include('admin.layouts.pagination')
    </div>
    <form id="delete_groups" action="" method="post">
        {{ method_field('post') }}
        {{ csrf_field() }}
    </form>
</div>
@endsection

@section('footer_embed')
<script>
    function delete_groups(__this) {
        var dataURL = __this.attr('data-href');
        if (confirm("Bạn có thực sự muốn xóa quyền này")) {
            $('#delete_groups').attr('action', dataURL);
            $('#delete_groups').submit();
        } else {
            $("form").submit(function (e) {
                e.preventDefault();
            });
        }
    }
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endsection
