@extends('admin.layouts.app')
@section('title', 'Thêm mới quyền')
@section('content')
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .expire_date{
        margin-bottom: 10px;
        padding-top: 7px;
        font-weight: bold;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }
    .col-md-1{width: 10%;}
    .has-error .control-label {
        color: #3D3A3A !important
    }
    .checkitem{padding-left: 30px}
    .group-parent{margin-top: 10px}
</style>
    <div class="page-wrap">
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Thêm mới Quyền truy cập</div>

            <div class="panel-body">
                <form id="infomation-form" class="form-horizontal" method="POST" action="{{ URL::route('group_role.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-2 control-label">Tên Quyền truy cập<span class="color-required">*</span></label>
                        <div class="col-md-6" style="">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  tabindex="1" maxlength="100" >
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description" class="col-md-2 control-label">Mô tả <span class="color-required">*</span></label>
                        <div class="col-md-6">
                            <textarea id="description" type="text" class="form-control" name="description" tabindex="2" maxlength="500">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('permissions') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="permissions" class="col-md-2 control-label">Chức năng <span class="color-required">*</span></label>

                        <div class="col-md-10" style="padding-top: 10px">
                            <div class="row">
                                @if ($errors->has('permissions'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('permissions') }}</strong>
                                </span>
                                @endif
                                <div class="col-md-12">
                                    <label><input type="checkbox" class="checkAll"> Chọn tất cả</label>
                                </div>

                                @php $k = 0; @endphp
                                @foreach ($listTree as $key => $val)
                                    @php if ($key == 0) : @endphp
                                    @foreach ($val as $v)
                                        @php if (isset($listTree[$v['id']])) continue; @endphp
                                         <?php if ($k % 3 == 0 && $k == 0) { ?>        
                                            <div class="col-md-12">
                                        <?php } ?>
                                         <?php if ($k % 3 == 0 && $k != 0 && $k != count($listTree)) { ?>
                                            </div>
                                            <div class="col-md-12">
                                        <?php } ?>
                                        <?php if ($k == count($listTree)) { ?>
                                            </div>
                                        <?php } ?>
                                        <div class="col-md-4">
                                            <label style="font-weight:normal">
                                                <input class="checkbox-pr" @php echo (isset($oldPermission) && in_array($v['route_name'], $oldPermission)) ? 'checked' : ''; @endphp type="checkbox" name="permissions[]" value="{{$v['route_name']}}" /> {{$v['name']}}
                                            </label>
                                        </div>
                                     <?php $k++; ?>
                                    @endforeach
                                    <div class="col-md-12"></div>
                                    <div class="clearfix" style="margin-bottom: 15px"></div>
                                    @else
                                        <?php if ($k % 3 == 0 && $k == 0) { ?>        
                                            <div class="col-md-12">
                                        <?php } ?>
                                        <?php if ($k % 3 == 0 && $k != 0 && $k != count($listTree)) { ?>
                                            </div>
                                            <div class="col-md-12">
                                        <?php } ?>
                                        <?php if ($k == count($listTree)) { ?>
                                            </div>
                                        <?php } ?>
                                        <div class="group-parent col-md-4">
                                            <label class="text-danger" id="text-danger-{{ $k++ }}">
                                                <input @php echo (isset($oldPermission) && in_array($listTree[0][$key]['route_name'], $oldPermission)) ? 'checked' : ''; @endphp class="checkbox-pr" type="checkbox" onclick="checkallChild($(this))"> {{$listTree[0][$key]['name']}}
                                            </label>
                                            @foreach ($val as $v)
                                                <div class="checkitem">
                                                    <label style="font-weight:normal">
                                                        <input class="checkbox-pr" @php echo (isset($oldPermission) && in_array($v['route_name'], $oldPermission)) ? 'checked' : '11'; @endphp type="checkbox" name="permissions[]" value="{{$v['route_name']}}" /> &nbsp;&nbsp;&nbsp;&nbsp;{{$v['name']}}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                @endforeach
                                <?php $k++; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button id="submitBtn" type="button" class="btn btn-primary" onclick="return submitForm();" tabindex="9">Lưu</button>
                            <a href="{{ URL::route('group_role.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="10">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>
    // Enter to submit form registerForm
    document.body.addEventListener('keydown', function(e) {
        var key = e.which;
        if (key == 13) {
            submitForm();
        }
    });
    
    $(document).ready(function () {
        document.getElementById("name").focus();
    });
    function submitForm() {
		$('#submitBtn').prop('disabled', true);
        $('#infomation-form').submit();
        return false;
    }
    function checkallChild(_this) {
        if (_this.is(':checked')) {
            _this.parent().parent().find('input[type="checkbox"]').prop('checked', true);
            _this.parent().parent().find('input[type="checkbox"]').attr('data-check', 'yes');
        } else {
            _this.parent().parent().find('input[type="checkbox"]').prop('checked', false);
            _this.parent().parent().find('input[type="checkbox"]').attr('data-check', 'no')
        }
    }
    $('body').on('change', '.checkAll', function() {
        $(".checkbox-pr").prop('checked',$(this).is(":checked"));
        if ($(this).is(":checked") == true) {
            $(".checkbox-pr").attr('data-check', 'yes');
        }
        else {
            $(".checkbox-pr").attr('data-check', 'no');
        }
    }).on('change', '.checkbox-pr', function () {
        var __this = $(this);
        var anyChecked = false;
        var arr = [];
        var allColLength = __this.parent().parent().parent().parent().parent().find(".col-md-12 .checkbox-pr").length;
        var allRowLength = __this.parent().parent().parent().parent().parent().find('input[data-check="yes"]').length;
        var eachColLength = __this.parent().parent().parent().find('.checkitem').length;
        var rowLength = __this.parent().parent().parent().find('.checkitem input[data-check="yes"]').length;
        if (__this.is(':checked')) {
            __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                if ($(this).is(':checked')){
                    anyChecked = true;
                } else {
                    anyChecked = false;
                }
            });
            if (anyChecked == true) {
                __this.attr("data-check", "yes");
                
                if (eachColLength == rowLength + 1) {
                   __this.parent().parent().parent().children('.text-danger').children('input[type="checkbox"]').prop('checked', true);
                    __this.parent().parent().parent().children('.text-danger').children('input[type="checkbox"]').attr("data-check", "yes");
                }
                var checkBoxLength = $('.text-danger > .checkbox-pr').length;
                var checkBoxCheckedLength = $('.text-danger').children('input[data-check="yes"]').length;
                console.log(allRowLength);
                console.log(allColLength);
                if (checkBoxLength == checkBoxCheckedLength) {
                    if (allRowLength + 2 == allColLength || allRowLength == allColLength) {
                        $('.checkAll').prop('checked', true);
                        $('.checkAll').attr('data-check', 'yes');
                    }
                }
            } else {
                // __this.attr("data-check", "no");
                // __this.parent().parent().parent().children('.text-danger').children('input[type="checkbox"]').prop('checked', false);
                // __this.parent().parent().parent().children('.text-danger').children('input[type="checkbox"]').attr("data-check", "no");
                
            }           
        } else {
            __this.parent().parent().parent().parent().parent().children('.col-md-12:first-child').children().children('.checkAll').prop('checked', false);
            __this.parent().parent().parent().children('.text-danger').children('input[type="checkbox"]').prop('checked', false);
            __this.parent().parent().children('.text-danger').children('input[type="checkbox"]').prop('checked', false);

            __this.parent().parent().parent().parent().parent().children('.col-md-12:first-child').children().children('.checkAll').attr('data-check', "no");
            __this.parent().parent().parent().children('.text-danger').children('input[type="checkbox"]').attr('data-check', "no");
            __this.parent().parent().children('.text-danger').children('input[type="checkbox"]').attr('data-check', "no");
            __this.attr('data-check', 'no');
            if (allRowLength + 2 != allColLength || allRowLength != allColLength) {
                $('.checkAll').prop('checked', false);
                $('.checkAll').attr('data-check', 'no');
            }
        }
    });
    $(".checkbox-pr-a").click(function() {
        if ($(this).is(':checked')){
            anyChecked = true;
        } else {
            anyChecked = false;
        }

        if (anyChecked == true) {
            $(".checkbox-pr-a").prop("checked", false);
            $(this).prop("checked", true);
        }
        else {
            $(".checkbox-pr-a").prop("checked", false);
        }
    })
</script>
@endsection
