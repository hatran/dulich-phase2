<form class="page-wrap" name="" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tên Quyền truy cập
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="search_name" type="text" placeholder="Tên Quyền truy cập" name="search_name" value="{{ Input::get('search_name', '') }}" maxlength="100">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Mô tả
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="2" id="search_description" type="text" placeholder="Mô tả" name="search_description" value="{{ Input::get('search_description') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="3" type="submit" onclick="return submitForm();" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm tin tức</h4>
            </div>
            <div class="modal-body">
                <h4>Phát hành từ ngày phải nhỏ hơn ngày kết thúc</h4>
            </div>
            <div class="modal-footer">                   
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    Thoát
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    function parseDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    $(document).keypress(function (e) {
        if ($('#formSearch input:focus, #formSearch button:focus').length != 0) {
            if (e.which === 13) {
                submitForm();
            }
        }
    });
    $(function () {
        $("#search_name").focus();
    });
    function submitForm() {
        $('#formSearch').submit();
        return false;
    }
</script>