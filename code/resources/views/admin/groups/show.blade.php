@extends('admin.layouts.app')
@section('title', 'Xem chi tiết vai trò - ' . $detail->name)
@section('content')
    <style>
        .color-required{
            color: red
        }
        .error_border {
            border-color: #a94442 !important;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        }
        .help-block strong {
            color: red
        }
        .expire_date{
            margin-bottom: 10px;
            padding-top: 7px;
            font-weight: bold;
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
        .col-md-1{
            width: 10%;
        }
    </style>
    <div class="page-wrap">
        @include('admin.layouts.message')
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Xem chi tiết vai trò</div>

            <div class="panel-body">
                <div class="row form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-1 control-label">Tên <span class="color-required">*</span></label>
                    <div class="col-md-6" style="">
                        {{ htmlentities($detail->name) }}
                    </div>
                </div>
                <div class="row form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label for="" class="col-md-1 control-label">Trạng thái <span class="color-required">*</span></label>

                    <div class="col-md-6" style="padding-top: 5px">
                        {{ $detail->status == 1 ? 'Hoạt động' : 'Không hoạt động' }}
                    </div>
                </div>
                <div class="row form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-1 control-label">Mô tả <span class="color-required">*</span></label>
                    <div class="col-md-6">
                        {{ htmlentities($detail->description) }}
                    </div>
                </div>
                <div class="row form-group{{ $errors->has('group_role_id') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-1 control-label">Quyền truy cập <span class="color-required">*</span></label>
                    <div class="col-md-10">
                        <div class="row">
                            @if (!empty($oldGroupRole))
                            @foreach ($oldGroupRole as $v)
                                <div class="col-md-3">
                                    {{ isset($listGroupRole[$v]) ? $listGroupRole[$v] : '' }}
                                </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-6 col-md-offset-5">
                        <a href="{{ URL::route('groups.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="10">Thoát</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
