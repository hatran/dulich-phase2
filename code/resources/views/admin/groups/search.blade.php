<form class="page-wrap" name="" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Mã vai trò
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="search_code" placeholder="Mã vai trò" type="text" name="search_code" value="{{ Input::get('search_code', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tên vai trò
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="2" id="search_name"  placeholder="Tên vai trò" type="text" name="search_name" value="{{ Input::get('search_name', '') }}" maxlength="100">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Mô tả
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="3" id="search_description" placeholder="Mô tả" type="text" name="search_description" value="{{ Input::get('search_description') }}" maxlength="50">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="5" id="search_status" name="search_status" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($statusList))
                    @foreach($statusList as $key => $val)
                        @php $selected = is_numeric(Input::get('search_status', '')) && Input::get('search_status', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="5" type="submit" onclick="return submitForm();" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm tin tức</h4>
            </div>
            <div class="modal-body">
                <h4>Phát hành từ ngày phải nhỏ hơn ngày kết thúc</h4>
            </div>
            <div class="modal-footer">                   
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    Thoát
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    function parseDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    $(document).keypress(function (e) {
        if ($('#formSearch input:focus, #formSearch button:focus').length != 0) {
            if (e.which === 13) {
                submitForm();
            }
        }
    });
    $(function () {
        $("#search_name").focus();
    });
    function submitForm() {
        $('#formSearch').submit();
        return false;
    }
</script>