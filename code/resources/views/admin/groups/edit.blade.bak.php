@extends('admin.layouts.app')
@section('title', 'Sửa quyền - ' . $detail->name)
@section('content')
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .expire_date{
        margin-bottom: 10px;
        padding-top: 7px;
        font-weight: bold;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }
    .col-md-1{
        width: 10%;
    }
</style>
    <div class="page-wrap">
        @include('admin.layouts.message')
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Sửa quyền</div>

            <div class="panel-body">
                <form id="infomation-form" class="form-horizontal" method="POST" action="{{ URL::route('groups.update',['id' => $detail->id]) }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-1 control-label">Tên <span class="color-required">*</span></label>
                        <div class="col-md-6" style="">
                            <input id="name" type="text" class="form-control" name="name" value="{{ empty(old('name')) ? $detail->name : old('name') }}"  tabindex="1" maxlength="100" >
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="" class="col-md-1 control-label">Trạng thái <span class="color-required">*</span></label>

                        <div class="col-md-6" style="padding-top: 5px">
                            <div class="field-wrap col-md-12 radio-list choices" style="padding-left: 0px !important;">
                                <div class="choice {{ empty(old('status')) ? ($detail->status == 1 ?  'chosen' : '') : (old('status') == 1 ? 'chosen' : '') }}" style="padding-left: 25px !important;">
                                    <label for="statushd" tabindex="5" >Hoạt động </label>
                                    <input id="statushd" type="radio" name="status" value="1" {{ empty(old('status')) ? ($detail->status == 1 ?  'checked' : '') :  (old('status') == 1 ? 'checked' : '') }}>
                                </div>
                                <div class="choice {{ empty(old('status')) ? ($detail->status == 0 ?  'chosen' : '') : (old('status') == 0 ? 'chosen' : '') }}">
                                    <label for="statusln" tabindex="6" >Bị khóa </label>
                                    <input id="statusln" type="radio" name="status" value="0" {{ empty(old('status')) ? ($detail->status == 0 ?  'checked' : '') :  (old('status') == 0 ? 'checked' : '') }}>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description" class="col-md-1 control-label">Mô tả quyền <span class="color-required">*</span></label>
                        <div class="col-md-6">
                            <textarea id="description" type="text" class="form-control" name="description" tabindex="2" maxlength="500">{{ empty(old('description')) ? $detail->description : old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('permissions') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="permissions" class="col-md-1 control-label">Quyền hạn <span class="color-required">*</span></label>

                        <div class="col-md-10" style="padding-top: 10px">
                            <div class="row">
                                <div class="col-md-12">
                                    <label><input type="checkbox" class="checkAll"> Chọn tất cả</label>
                                </div>
                                @foreach ($listTree as $key => $val)
                                    @php if ($key == 0) : @endphp
                                        @foreach ($val as $v)
                                            @php if (isset($listTree[$v['id']])) continue; @endphp
                                            <div class="col-md-4">
                                                <label>
                                                    <input class="checkbox-pr" @php echo (isset($oldPermission) && in_array($v['route_name'], $oldPermission)) ? 'checked' : ''; @endphp type="checkbox" name="permissions[]" value="{{$v['route_name']}}" /> {{$v['name']}}
                                                </label>
                                            </div>
                                        @endforeach
                                        <div class="col-md-12"></div>
                                        <div class="clearfix" style="margin-bottom: 15px"></div>
                                    @else
                                        <div class="group-parent col-md-4">
                                            <label class="text-danger">{{$listTree[0][$key]['name']}}</label>
                                            @foreach ($val as $v)
                                            <div class="">
                                                <label>
                                                    <input class="checkbox-pr" @php echo (isset($oldPermission) && in_array($v['route_name'], $oldPermission)) ? 'checked' : '11'; @endphp type="checkbox" name="permissions[]" value="{{$v['route_name']}}" /> ----{{$v['name']}}
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            @if ($errors->has('permissions'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('permissions') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button type="button" class="btn btn-primary" onclick="return submitForm();" tabindex="9">Lưu</button>
                            <a href="{{ URL::route('groups.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="10">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>
    // Enter to submit form registerForm
    document.body.addEventListener('keydown', function(e) {
        var key = e.which;
        if (key == 13) {
            $("#registerInfo").click();
        }
    });
    
    $(document).ready(function () {
        document.getElementById("name").focus();
    });
    function submitForm() {
        $('#infomation-form').submit();
        return false;
    }
    $('body').on('change', '.checkAll', function() {
        $(".checkbox-pr").prop('checked',$(this).is(":checked"));
    });
</script>
@endsection
