<form class="page-wrap row" name="filter" method="get" id="formSearch" action="{{ url ('/officesys/subjectknowledge') }}">
    {{ csrf_field() }}
    <div class="row" style="width: 100%">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
           Tên danh mục câu hỏi
        </div>

        <div style="margin-bottom: 10px;" class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <input type="text" max="255" id="nameall" name="nameall" value="{{ array_get($response_view, 'nameall', '') }}" placeholder="Nhập từ khóa muốn tìm kiếm" tabindex="1">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
            Loại kiến thức
        </div>
        <div style="" class="field-wrap dropfield col-md-4 col-sm-6 col-xs-12" >
            <select name="id" id="id" tabindex="2">
                <option value="">Tất cả</option>
                @foreach($options as $key => $value)
                <option value="{{ $key }}"
                        @if (array_get($response_view, 'id', '') == $key) selected="selected" @endif>{{ $value }}</option>
                @endforeach
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
           Trạng thái
        </div>

        <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <select name="status" id="status" tabindex="3">
                <option value="">Tất cả</option>
                <option value="0" @if (array_get($response_view, 'status', '') == "0") selected="selected" @endif>Không hoạt động</option>
                <option value="1" @if (array_get($response_view, 'status', '') == "1") selected="selected" @endif>Hoạt động</option>
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2">
            <input type="submit" class="button btn-primary" name="search" value="Tìm kiếm" tabindex="4" style="width: 140px">
        </div>
    </div>
</form>
<input type="hidden" value="{{array_get($response_view, 'id', '')}}" id="procode_all" name="procode_all"/>
<input type="hidden" value="" id="dm-id" name="dm-id"/>

<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
</script>