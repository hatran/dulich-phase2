
@extends('admin.layouts.app')
@section('title', 'Quản trị danh mục câu hỏi kiến thức')

@section('content')
    <div class="page-wrap">
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.message')
            @include('admin.subject_knowledge.search')
            <div class="total" style="margin-bottom: 10px">Tổng số : {{$count_subjects}}<input class="import_view-modal button btn-primary btn-function add-modal" value="Thêm mới" id="btnAddSubject" style="float:  right;width: 120px; height: 37px; line-height: 24px; padding: 6px 12px !important; cursor: pointer" data-file-id="-1"></div>

            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%"><div class="th-inner sortable both">STT</div></td>
                        <td width="25%"><div class="th-inner sortable both">Tên danh mục câu hỏi</div></td>
                        <td width="20%"><div class="th-inner sortable both">Loại kiến thức</div></td>
                        <td width="10%"><div class="th-inner sortable both">Ngày tạo</div></td>
                        <td width="10%"><div class="th-inner sortable both">Nguời tạo</div></td>
                        <td width="10%"><div class="th-inner sortable both">Trạng thái</div></td>
                        <td width="10%"><div class="th-inner sortable both">Câu hỏi kiến thức</div></td>
                        <td><div class="th-inner sortable both">Chức năng</div></td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objSubjects) == 0)
                        <tr>
                            <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objSubjects as $key => $value)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                  
                                <td class="text-left">
                                    <p title="{{$value->name}}">
                                        {{ mb_strlen($value->name, 'UTF-8') > 100 ? mb_substr($value->name, 0, 100, 'utf-8') .'...' :  $value->name  }}
                                    </p>
                                </td>

                                <td class="text-left">
                                    {{ isset($value->knowledge_id) ? $options[$value->knowledge_id] : '' }}
                                </td>
                                <td class="text-center">{{ !empty($value->created_at) ? date('d.m.Y', strtotime($value->created_at)) : '' }}</td>
                                <td>{{ !empty($value->created_author) ? $value->created_author : '' }}</td>
                                <td class="text-left"> {{ $value->status == 0 ? 'Không hoạt động' : 'Hoạt động' }} </td>
                                <td class="text-center">
                                    <a class="btn-function" href="{{ url("/officesys/question?knowledge_id={$value->knowledge_id}&subject_knowledge_id={$value->id}") }}" title="Thêm câu hỏi kiến thức">
                                        <span class="glyphicon glyphicon-plus" style="font-size: 16px; color: #009933;"></span>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <a class="btn-function show-modal" href="#" data-toggle="tooltip" data-placement="left" title="Xem danh mục" data-id="{{ $value->id }}" data-option="{{array_get($response_view, 'knowledge_id', '')}}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                    </a>
                                    <a class="btn-function edit-modal" href="#" data-toggle="tooltip" data-placement="left" title="Sửa danh mục" data-id="{{ $value->id }}" data-option="{{array_get($response_view, 'knowledge_id', '')}}" data-status="{{ $value->status }}" data-approved="">
                                        <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                    </a>
                                    <a class="btn-function" href="javascript:void(0);" data-toggle="tooltip" data-placement="left" title="Xoá danh mục" onclick="delete_intro({{ $value->id }}, '{{array_get($response_view, 'knowledge_id', '')}}', '{{strip_tags($value->name)}}')" data-id="{{ $value->id }}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>

    @include('admin.subject_knowledge.detail')
    @include('admin.subject_knowledge.register')
    @include('admin.subject_knowledge.edit')
    @include('admin.subject_knowledge.delete')


@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }
        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .sortable{
            color: #144a8b;
            font-weight: bold;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }
        #closeButton ul {
            padding-left: 0px !important;
        }
        #id{
            -moz-appearance: textfield;     height: 35px;     line-height: 35px;     padding-right: 30px;     position: relative;     width: 100%;border: 1px solid #bbbaba;border-radius: 2px;display: block;max-width: 100%; margin-bottom: 10px;padding-left: 10px;
        }
        .rq-star{
            color: red
        }
        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }
        span.error-msg {
            color: #a94442;
        }
        span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }
        .total {
            border: 0 none;
            border-radius: 3px;
            color: #0000f0 !important;
            font-size: 16px;
            line-height: 30px;
            padding: 6px 10px;
        }


        .modal-title {
            margin: 0;
            line-height: 1.42857143;
            font-weight: bold;
            width: 50%;
            float: left;
            font-size: 20px;
        }
    </style>
    <script>
        var knowledgeIndex = "<?php echo $knowledge; ?>";
        var areaIndex = "<?php echo $area; ?>";
        function delete_intro(id, option, txt) {
            jQuery("#deleteModal").modal();
            jQuery('#id-em').text(txt);
            jQuery('#id_delete').val(id);
            jQuery('#option_delete').val(option);
        }
        function deleteCat() {
            window.location = '/officesys/subjectknowledge/deleteall/'+jQuery('#id_delete').val();
        }
        function convertStr(txt){
            var str = txt;
            str= str.toLowerCase();
            str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str= str.replace(/đ/g,"d");
            str= str.replace(/\s/g, '');
            //str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
            //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
            //str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
            return str;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#btnsubmit").click();
                }
            }
        });
        function clearErrorFields(postfix) {
            $('#nameAll'+postfix).removeClass('error_border');
            $('#code'+postfix).removeClass('error_border');
            $('#optionAll'+postfix).removeClass('error_border');
            $('.errorNameAll'+postfix).addClass('hidden');
            $('.errorOptionAll'+postfix).addClass('hidden');
            $('.errorcode'+postfix).addClass('hidden');
        }

        var url = "{{ url('/officesys/subjectknowledge') }}"
        function htmlEntities(str) {
            return str;
        }
        function showPopup(dmId, option, postfix) {
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';

            switch (postfix) {
                case '_add':
                    title = 'Thêm mới danh mục câu hỏi';
                    canEdit = true;
                    break;

                case '_edit':
                    title = 'Cập nhật danh mục câu hỏi';
                    $("#id_edit").val(dmId);
                    $("#option_edit").val(option);
                    canEdit = true;
                    break;

                default:
                    title = 'Chi tiết danh mục câu hỏi';
            }
            clearErrorFields(postfix);
            $('.modal-title'+postfix).text(title);

            var procode_all = $("#procode_all").val();
            if(postfix != '_add'){
//                $(modalId).modal('show');
//                $(modalId + ' .modal-dialog').loader('show');
                $.ajax({
                    url: url + "/" + dmId,
                    type: 'GET',
                    success: function (response) {
//                        console.log(response);
                        var optionEditShow = '';
                        var nameAll = '';
                        if(response.deleted == 'error'){
                            document.location.reload(true);
                        }else{
                            $(modalId).modal('show');
                            //$(modalId + ' .modal-dialog').loader('show');
                            $.each(response.objOptions, function( index, value ) {
                                if(index == response.objData.knowledge_id){
                                    optionEditShow = value;
                                }
                            });
                            //$(modalId + ' .modal-dialog').loader('hide');
                            nameAll = response.objData.name;
                            optionAll = response.objData.knowledge_id;
                            statusAll = response.objData.status;
                            pdfAll = response.objData.file_name;
                            areaAll = response.objData.area;
                            if (canEdit === true) {
                                var radio = $("input[name='optionAll_edit']");
                                radio.each(function () {
                                    $(this).prop('checked', false);
                                });
                                radio.each(function () {
                                    if (optionAll == $(this).val()) {
                                        $(this).prop('checked', 'checked');
                                    }
                                });

                                $('input[name="areaLocalAll_edit"]').prop('checked', false);
                                $('input[name="areaInternationalAll_edit"]').prop('checked', false);

                                if (areaAll == 3) {
                                    $('input[name="areaLocalAll_edit"]').prop('checked', 'checked');
                                    $('input[name="areaInternationalAll_edit"]').prop('checked', 'checked');
                                }
                                else {
                                    if (areaAll == 1) {
                                        $('input[name="areaLocalAll_edit"]').prop('checked', 'checked');
                                    }

                                    if (areaAll == 2) {
                                        $('input[name="areaInternationalAll_edit"]').prop('checked', 'checked');
                                    }
                                }

                                var statusRadio = $("input[name='statusAll_edit']");
                                statusRadio.each(function () {
                                    if (statusAll == $(this).val()) {
                                        $(this).prop('checked', 'checked');
                                    }
                                });

                                $('#nameAll'+postfix).val(nameAll);
                         
                                $('#pdf_name').text(pdfAll);
                            } else {
                                $('#nameAll_show').text(nameAll);
                                $('#optionEditShow_show').text(optionEditShow);
                                var messStatus = '';
                                if (statusAll == 1) {
                                    messStatus = 'Họat động';
                                }
                                else {
                                    messStatus = 'Không họat động';
                                }
                                $('#statusAll_show').text(messStatus);
                                $('#pdfAll_show').text(pdfAll);
                                

                                if (optionAll == knowledgeIndex && areaAll == areaIndex) {
                                    $('#area-open-show').show();
                                }
                                else {
                                    $('#area-open-show').hide();
                                }
                            }
                            if (postfix == '_edit') {
                                setTimeout(function () {
                                    $("#nameAll"+postfix).focus();
                                }, 500);
                            }
                        }
                    }
                });
            }else{
                $(modalId).modal('show');
                $("#nameAll"+postfix).val('');
                $("#optionAll"+postfix).val('');
                setTimeout(function () {
                    $("#nameAll"+postfix).focus();
                }, 500);
            }
        }

        $(document).on('click', '.show-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            var option = $(this).data('option');
            showPopup(dmId, option, '_show');
        });

        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            showPopup('','', '_add');
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            var option = $(this).data('option');
            $('input[type=hidden]#dm-id').val(dmId);
            showPopup(dmId, option, '_edit');
        });


        function doAction(dmId, postfix) {
            var procode_all = $("#procode_all").val();
            var action = 'POST';
            var modalId = '#' + postfix + 'Modal';

            var nameAll = $("#nameAll" + postfix);
            var optionAll = $('input[name="optionAll' + postfix + '"]:checked');
            if (postfix == '_add')  dmId = 'new';
            if (postfix == '_add' || postfix == '_edit') {
                clearErrorFields(postfix)
            }
            var flg = 0;
            if (postfix == '_add' || postfix == '_edit') {
                if (postfix == '_add') {
                    if (optionAll.val() == '') {
                        $("#optionAll" + postfix).addClass('error_border');
                        $(".errorOptionAll" + postfix).html('Loại kiến thức là trường bắt buộc phải chọn');
                        $(".errorOptionAll" + postfix).removeClass('hidden');
                        flg = 1;
                    } else {
                        $(".errorOptionAll" + postfix).html('');
                        $(".errorOptionAll" + postfix).addClass('hidden');
                        $("#optionAll" + postfix).removeClass('error_border');
                    }

                    var myfile="";

                    myfile= $("#pdf").val();
                    if (myfile != "") {
                        var ext = myfile.split('.').pop();
                        if(ext != "pdf"){
                            $("#pdfAll"+postfix).addClass('error_border');
                            $(".errorPdfAll"+postfix).html('Tải lên file có định dạng pdf');
                            $(".errorPdfAll"+postfix).removeClass('hidden');
                            flg = 1;
                        }
                        else{
                            $(".errorPdfAll"+postfix).html('');
                            $(".errorPdfAll"+postfix).addClass('hidden');
                            $("#pdfAll"+postfix).removeClass('error_border');
                        }
                    }
                }

                if (nameAll.val().trim() == '') {
                    $("#nameAll"+postfix).addClass('error_border');
                    $(".errorNameAll"+postfix).html('Tên danh mục câu hỏi là trường bắt buộc phải điền');
                    $(".errorNameAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorNameAll"+postfix).html('');
                    $(".errorNameAll"+postfix).addClass('hidden');
                    $("#nameAll"+postfix).removeClass('error_border');
                }

                if (postfix == '_edit') {
                    var myfileEdit ="";

                    myfileEdit = $("#pdf1").val();
                    if (myfileEdit != "") {
                        var ext = myfileEdit.split('.').pop();
                        if(ext != "pdf"){
                            $("#pdfAll"+postfix).addClass('error_border');
                            $(".errorPdfAll"+postfix).html('Tải lên file có định dạng pdf');
                            $(".errorPdfAll"+postfix).removeClass('hidden');
                            flg = 1;
                        }
                        else{
                            $(".errorPdfAll"+postfix).html('');
                            $(".errorPdfAll"+postfix).addClass('hidden');
                            $("#pdfAll"+postfix).removeClass('error_border');
                        }
                    }
                }

                
            }

            if (flg == 0) {
                $("#subjectForm"+postfix).submit();
                var modalId = '#' + postfix + 'Modal';
                $(modalId).modal('hide');
            }
        }

        $('.modal-footer').on('click', '.add', function () {
            doAction('', '_add');
        });

        $('.modal-footer').on('click', '.edit', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_edit');
        });

        $('.modal-footer').on('click', '.delete', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_delete');
        });
        $(document).ready(function () {
            var nameall = $("#nameall").val();
            $("#nameall").val('');
            setTimeout(function() {
                $('#nameall').focus() ;
                $("#nameall").val(nameall);
            }, 100);
        });
    </script>
@endsection