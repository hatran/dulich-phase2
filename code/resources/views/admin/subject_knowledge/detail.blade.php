<div id="_showModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_show"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="table-line">
                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone" class="col-md-3 control-label">Tên danh mục câu hỏi</label>
                                <div class="col-md-9">
                                    <span id="nameAll_show"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-bottom: 30px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="code">Loại kiến thức</label>
                                <div class="col-md-9">
                                    <span id="optionEditShow_show"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" id="area-open-show" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="areaAll_show" style="padding-top: 7px">Quốc tế</label>
                                    <div class="col-md-9">
                                        HDV Quốc Tế
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="statusAll_show" style="padding-top: 7px">Trạng thái</label>
                                    <div class="col-md-9">
                                         <span id="statusAll_show"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label pdf-label" for="pdfAll_add" style="padding-top: 7px">File danh sách câu hỏi</label>
                                    <div class="col-md-9">
                                        <span id="pdfAll_show"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="40">
                            Thoát
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
