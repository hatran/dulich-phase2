<div id="_editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_edit"></h4>
            </div>
            <form action="{{url('/officesys/subjectknowledge/check_subject')}}" enctype="multipart/form-data" id="subjectForm_edit" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id_edit" id="id_edit" class="form-control" value=""/>
                <input type="hidden" name="option_edit" id="option_edit" class="form-control" value=""/>
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label" style="padding-top: 7px">Tên danh mục câu hỏi<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="nameAll_edit" id="nameAll_edit" maxlength="191" tabindex="201">
                                        <span class="errorNameAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="code">Loại kiến thức</label>
                                    <div class="col-md-9">
                                        <?php $i = 202; ?>
                                        @foreach($options as $key => $value)
                                            <input type="radio" name="optionAll_edit" value="{{ $key }}" tabindex="{{ $i }}"> {{ $value }}<br />
                                            <?php $i++; ?>
                                        @endforeach
                                        <span class="errorOptionAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3" style="margin-left: 30% !important;">
                                        <input type="checkbox" name="areaInternationalAll_edit" value="2" tabindex="204" style="margin-left: 10px;margin-right: 5px;"> <span style="font-weight: bold; padding-right: 30px;">Quốc Tế</span>
                                        <span class="errorAreaLocalAll_edit text-left hidden error-msg"></span>
                                        <input type="checkbox" name="areaLocalAll_edit" value="1" tabindex="205" style="margin-right: 5px;"> <span style="font-weight: bold;">Nội địa</span>
                                        <span class="errorAreaInternationalAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="statusAll_edit" style="padding-top: 7px">Trạng thái<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="radio" name="statusAll_edit" value="1" tabindex="206" checked> Hoạt động
                                        <input type="radio" name="statusAll_edit" value="0" tabindex="207" style="margin-left: 10px;"> Không hoạt động<br>
                                        <span class="errorStatusAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label pdf-label" for="pdfAll_add" style="padding-top: 7px">File danh sách câu hỏi</label>
                                    <div class="col-md-9">
                                        <span id="pdf_name1" style="display: none;"><i class="fa fa-upload" aria-hidden="true"></i>Tải lên file pdf</span>
                                        <label id="pdf_name"><i class="fa fa-upload" aria-hidden="true"></i>Tải lên file pdf</label>
                                        <input type="file" name="pdfAll_edit" id="pdf1" tabindex="208" style="color:transparent">
                                        <span class="errorPdfAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary edit" type="button" tabindex="209">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="210">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('input[type="file"]').change(function(e){
            $(this).removeAttr('style');
            $('#pdf_name').hide();
            $('#pdf_name1').show();
        });
    });
</script>
