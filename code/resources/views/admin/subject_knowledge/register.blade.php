<div id="_addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_add"></h4>
            </div>
            <form action="{{url('/officesys/subjectknowledge/check_subject')}}" enctype="multipart/form-data" id="subjectForm_add" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nameAll_add" class="col-md-3 control-label" style="padding-top: 7px">Tên danh mục câu hỏi<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="nameAll_add" id="nameAll_add" maxlength="191" tabindex="101">
                                        <span class="errorNameAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="optionAll_add" style="padding-top: 7px">Loại kiến thức<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        @foreach($options as $key => $value)
                                            @if ($key == 1)
                                                <input type="radio" name="optionAll_add" value="{{ $key }}" tabindex="102" checked> {{ $value }}<br />
                                            @else
                                                <input type="radio" name="optionAll_add" value="{{ $key }}" tabindex="103"> {{ $value }}<br />
                                            @endif
                                            
                                        @endforeach
                                        <span class="errorOptionAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-9 col-md-offset-3" style="margin-left: 30% !important;">
                                        <input type="checkbox" name="areaInternationalAll_add" value="2" tabindex="104" checked="checked" style="margin-left: 10px;margin-right: 5px;"> <span style="font-weight: bold; padding-right: 30px;">Quốc Tế</span>
                                        <input type="checkbox" name="areaLocalAll_add" value="1" tabindex="105" checked="checked" style="margin-right: 5px;"> <span style="font-weight: bold;">Nội địa</span>
                                        <span class="errorAreaInternationalAll_add text-left hidden error-msg"></span>
                                        <span class="errorAreaLocalAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="statusAll_add" style="padding-top: 7px">Trạng thái<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="radio" name="statusAll_add" value="1" tabindex="106" checked> Hoạt động
                                        <input type="radio" name="statusAll_add" value="0" tabindex="107" style="margin-left: 10px;"> Không hoạt động<br>
                                        <span class="errorStatusAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="pdfAll_add" style="padding-top: 7px">File danh sách câu hỏi</label>
                                    <div class="col-md-9">
                                        <span><i class="fa fa-upload" aria-hidden="true"></i>Tải lên file pdf</span>
                                        <input type="file" name="pdfAll_add" id="pdf" tabindex="108">
                                        <span class="errorPdfAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary add" type="button" tabindex="109">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="110">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>