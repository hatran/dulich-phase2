@extends('admin.layouts.app')
@section('title', 'Quản trị Chi Hội - Danh sách Chi hội')
@section('content')
<style>
    .error_border {-webkit-box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);}
    .total {line-height: 30px;padding: 6px 10px;color: #0000F0 !important;border-radius: 3px;font-size: 16px;border: 0;display: inline-block;}
    .form-horizontal .control-label {padding-top: 7px;margin-bottom: 0;text-align: left;}
    .fa{cursor: pointer}
    .fa-2x {font-size: 1.2em;}
    .table-wrap table.tbl-custom > tbody > tr > td {position: relative;word-wrap: break-word;white-space: normal !important;}

    .table-wrap table.tbl-custom > tr > td {position: relative;word-wrap: break-word;white-space: normal !important;}
    .rq-star{color:red}
    .modal-title {
        margin: 0;
        font-weight: bold;
        float: left;
        font-size: 20px;
    }
    .error-mess-branche{
        margin-bottom: 0;
        padding: 2px;
    }
</style>
<div class="main-content">
    @include('admin.branches.search')
    <div class="clearfix"></div>
    <div class="page-wrap">
        <!-- <div class="heading">Kết quả tìm kiếm</div> -->
        @include('admin.layouts.message')
        <div class="total">Tổng số: {{$totalItems}}</div>
        <div class="field-wrap" style="display:inline-block;float: right">
            <!-- <input tabindex="9" type="button" data-href="{{ URL::route('branches.create') }}" class="openPopup button btn-primary add-modal" value="Tạo mới Chi hội"> -->
        </div>
        <div class="table-wrap">
            <table class="tbl-custom">
                <thead>
                    <tr>
                        <td width="5%">STT</td>
                        <td width="5%">Mã <br/>Chi Hội</td>
                        <td width="14%">Tên Chi Hội</td>
                        <td width="12%">VPĐD</td>
                        <td width="15%">Địa chỉ</td>
                        <td width="9%">Số điện thoại</td>
                        <td width="12%">Email</td>
                        <td width="9%">Trạng thái</td>
                        <td width="4%">Ban <br/>chấp<br/>hành</td>
                        <td width="4%">Hội <br/> viên</td>
                        <td width="4%">CLB <br>thuộc<br>Chi <br>Hội</td>
                        <td width="7%">Chức năng</td>
                    </tr>
                </thead>
                <tbody>
                    @if(!$branches->count())
                    <tr>
                        <td colspan="8" class="text-center"><p>Không có Chi hội nào phù hợp theo điều kiện tìm kiếm</p></td>
                    </tr>
                    @else
                    @foreach($branches as $key => $branch)
                    <tr>
                        <td class="text-center"> {{$offsets + $key + 1 }} </td>
                        <td class="text-center">{{ $branch->code }}</td>
                        <td class="text-left"> {{ htmlentities($branch->name) }} </td>
                        <td class="text-left">
                            {{ isset($representative_office[$branch->parent_id]) ? $representative_office[$branch->parent_id] : '' }}
                        </td>
                        <td class="text-left"><span data-toggle="tooltip" data-placement="top" title="{{htmlentities($branch->address)}}">{{ htmlentities(str_limit($branch->address, 100)) }}</span></td>
                        <td class="text-right"> {{ $branch->phone }} </td>
                        <td class="text-left"> {{ $branch->email }} </td>
                        <td class="text-left"> {{ $branch->getStatusLabel() }} </td>
                        <td class="text-center">
                            @if ($branch->status)
                            <i title="Cập nhật Ban chấp hành Chi hội" data-href="{{ URL::route('branches_manager', ['id' => $branch->id]) }}" class="openPopupIframe fa fa-plus fa-2x" style="font-size: 20px; color: #009933;"></i>
                            @endif
                        </td>
                        <td class="text-center">
                            <a title="Xuất danh sách hội viên Chi hội" href="{{ URL::route('branches_export_excel', ['id' => $branch->id]) }}">
                                <i class="fa fa-download fa-2x" style="font-size: 18px; color: #009933;"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            @if ($branch->status)
                            <a title="Tạo mới CLB thuộc Chi hội" href="{{ URL::route('club_of_branch_index') }}?search_parent_id={{$branch->id}}">
                                <i class=" fa fa-plus fa-2x" style="font-size: 18px; color: #009933;"></i>
                            </a>
                            @endif
                        </td>
                        <td class="text-center">
                            <a title="Xem thông tin Chi hội" style="padding-right:5px" data-type="show" href="javascript:;" data-name="{{$branch->name}}" class="edit_link" data-href="{{ route('branches.show',['id' => $branch->id]) }}">
                                <i class="fa fa-eye fa-2x" aria-hidden="true" style="font-size: 18px; color: #009933;"></i>
                            </a>
                            <a title="Cập nhật thông tin Chi hội" href="javascript:;" data-name="{{$branch->name}}" class="edit_link edit-modal" data-href="{{ route('branches.edit',['id' => $branch->id]) }}">
                                <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true" style="font-size: 18px; color: #ff6600;"></i>
                            </a>
                            <a title="Xóa Chi hội" data-title="{{ htmlentities($branch->name) }}" href="javascript:;" data-href="" style="padding-left: 5px" onclick="showDeleteModal($(this))">
								<i class="fa fa-trash-o fa-2x" aria-hidden="true" style="font-size: 18px;"></i>
							</a>
                           <!--  <a title="Xóa Chi hội" data-title="{{ htmlentities($branch->name) }}" href="javascript:;" data-href="{{ URL::route('branch_destroy', array($branch->id)) }}" style="padding-left: 5px" onclick="showDeleteModal($(this))">

                                <i class="fa fa-trash-o fa-2x" aria-hidden="true" style="font-size: 18px;"></i>
                            </a> -->
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @include('admin.layouts.pagination')
    </div>
    <div id="branchesModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div id="branchesManagerModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Xóa Chi Hội</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Bạn chắc chắn muốn xóa Chi hội <span id="id-em" style="font-size:  20px;color:  blue;"></span> ra khỏi danh sách?</h4>
                    <br/>
                    <form id="delete_branches" action="" method="post">
                        {{ method_field('post') }}
                        {{ csrf_field() }}
                    </form>
                </div>
                <input type="hidden" id="id_delete" value=""/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="delete_branches();">Xóa</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Thoát</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_embed')
<script>
    $(document).on('click', '.add-modal', function (event) {
        event.preventDefault();
        setTimeout(function () {
            $('#code').focus();
        }, 500);
    });
    
    var errorValidateForm = function (errors) {
        $('.status-message').remove();
        $('.text-danger').remove();
        $('.form-control').removeClass('error_border');
        for (var errorType in errors) {
            for (var i in errors[errorType]) {
                $('[name="' + errorType + '"]').addClass('error_border').parent().append('<span class=" text-danger error-' + errorType + '">' + errors[errorType][i] + '</span>');
            }
        }
    };
    $('body').on('click', '.openPopup', function () {
        var dataURL = $(this).attr('data-href');
        $('#branchesModal .modal-body').load(dataURL, function () {
            $('#branchesModal .modal-title').html('Tạo mới Chi hội');
            $('#branchesModal').modal({show: true});
        });
    }).on('click', '.openPopupIframe', function () {
        var dataURL = $(this).attr('data-href');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            cache: false,
            url: dataURL,
            success:function (res) {
                if (res.status != false ) {
                    $('#branchesManagerModal .modal-title').html('Cập nhật Ban chấp hành Chi hội');
                    $('#branchesManagerModal').modal({show: true});

                    let IframeEmbed = '<iframe style="position:relative;min-height:400px" src="' + dataURL + '" width="100%" height="300px" frameBorder="0"></iframe>';

                    $('#branchesManagerModal .modal-body').html(IframeEmbed);
                } else {
                    toastr.warning('Không thể cập nhật Ban chấp hành Chi hội ở trạng thái không hoạt động', {timeOut: 2000});
                }
            }
        });
    }).on('click', '.edit_link', function (e) {
        e.preventDefault();
        var dataURL = $(this).attr('data-href');
        var dataName = $(this).attr('data-name');
        var titleModal = $(this).attr('data-type') == 'show' ? 'Chi tiết' : 'Cập nhật';
        $('#branchesModal .modal-body').load(dataURL, function () {
            $('#branchesModal .modal-title').html(titleModal + ' Chi hội: ' + dataName);

            $('#branchesModal').modal({show: true});
        });
    }).on('click', '#branchs-form-save', function (e)
    {
        if ($('.error-mess-branche').length > 0) {
            $('.error-mess-branche').addClass('hidden');
        }
        $(this).prop("disabled", true);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            cache: false,
            "type": $('#branchs-form-create').attr('method'),
            "url": $('#branchs-form-create').attr('action'),
            "data": $('#branchs-form-create').serialize(),
            "dataType": "json"
        }).done(function (result) {
            $('#branchs-form-save').prop("disabled", false);
            if (result.savedData === false) {
                if (typeof result.errorMessages !== 'undefined')
                {
                    errorValidateForm(result.errorMessages);
                    if (result.errorMessages.updated == false && result.errorMessages.errorsUpdateDisable !== 'undefined') {
                        $('.error-mess-branche').removeClass('hidden').html(result.errorMessages.errorsUpdateDisable);
                        return;
                    }
                    if (result.errorMessages.updated == false && result.errorMessages.nullBranch !== 'undefined') {
                        toastr.warning(result.errorMessages.nullBranch);
                    }
                    
                    if (result.errorMessages.reload == true) {
                        setTimeout(function () {// wait for 5 secs(2)
                            location.reload(); // then reload the page.(3)
                        }, 2000);
                    }
                }
            } else {
                $('#branchesModal').modal('hide');
                var messageNotice = $('#branchs-form-create').hasClass('form-create') ? 'Tạo mới thành công' : 'Cập nhật thành công';
                toastr.success('Thành công!', messageNotice, {timeOut: 2000});
                setTimeout(function () {// wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 2000);
            }
        });

        return false;
    });

    function showDeleteModal(__this) {
        var dataURL = __this.attr('data-href');
        $('#id-em').text(__this.attr('data-title'));
        $('#delete_branches').attr('action', dataURL);
        $('#deleteModal').modal({show: true});
        return false;
    }
    function delete_branches() {
        $('#delete_branches').submit();
    }
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endsection
