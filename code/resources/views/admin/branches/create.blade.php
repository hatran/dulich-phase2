<form method="POST" id="branchs-form-create" class="form-create form-horizontal" action="{{ URL::route('branches.store') }}">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="code" class="col-md-4 control-label">Mã Chi Hội<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <input class="form-control" name="code" type="text" id="code" maxlength="10" autocomplete="off" tabindex="100">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="phone" class="col-md-4 control-label">Số điện thoại</label>
                <div class="col-md-8">
                    <input class="form-control" name="phone" type="text" id="phone" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="20" tabindex="101">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">Tên Chi Hội<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <input class="form-control" name="name" type="text" id="name" maxlength="50" tabindex="102">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-8">
                    <input class="form-control" name="email" type="text" id="email" maxlength="100" tabindex="103">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="parent_id" class="col-md-4 control-label">VPĐD<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <select name="parent_id" class="form-control" tabindex="104">
                        <option value="">Chọn VPĐD</option>
                        @if (!empty($representative_office))
                        @foreach($representative_office as $key => $val)
                        <option value="{{$key}}">{{$val}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-2 control-label">Trạng thái</label>
                <div class="col-md-2">
                    <label class="radio-inline">
                        <input checked="" value="{{\App\Models\Branches::BRANCHES_ACTIVE}}" type="radio" name="status" tabindex="105"> Hoạt động
                    </label>
                </div>
                <div class="col-md-2">
                    <label class="radio-inline">
                        <input value="{{\App\Models\Branches::BRANCHES_DEACTIVE}}" type="radio" name="status" tabindex="106"> Không hoạt động
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="address" class="control-label">Địa chỉ</label>
                    <textarea class="form-control" rows="3" name="address" id="address" maxlength="350" style="resize: none;" tabindex="107"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                <label for="note" class="control-label">Ghi chú</label>
                <textarea class="form-control" rows="3" name="note" id="note" maxlength="500" style="resize: none;" tabindex="108"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <input type="button" id="branchs-form-save" value="Lưu" class="btn btn-primary" tabindex="109">
                        <input type="button" data-dismiss="modal" value="Thoát" class="btn btn-warning" tabindex="110">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
