<form method="POST" id="branchs-form-create" class="form-horizontal" action="{{ route('branches_update',['id' => $detail->id]) }}">
	{{ csrf_field() }}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="code" class="col-md-4 control-label">Mã Chi Hội<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <input value="{{$detail->code}}" class="form-control" name="code" type="text" id="code" maxlength="20" tabindex="200">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="phone" class="col-md-4 control-label">Số điện thoại</label>
                <div class="col-md-8">
                    <input value="{{$detail->phone}}" class="form-control" name="phone" tabindex="201" type="text" id="phone" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="20">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="name" class="col-md-4 control-label">Tên Chi Hội<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <input value="{{ htmlentities($detail->name) }}" class="form-control" name="name" type="text" id="name" maxlength="50" tabindex="202">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-8">
                    <input value="{{$detail->email}}" class="form-control" name="email" type="text" id="email" maxlength="100" tabindex="203">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="parent_id" class="col-md-4 control-label">VPĐD<span class="rq-star">*</span></label>
                <div class="col-md-8">
                    <select name="parent_id" class="form-control" tabindex="204">
                        <option value="">Chọn VPĐD</option>
                        @if (!empty($representative_office))
                        @foreach($representative_office as $key => $val)
                        <option @php if ($detail->parent_id == $key) {echo 'selected="selected"';} @endphp value="{{$key}}">{{$val}}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-2 control-label">Trạng thái</label>
                <div class="col-md-2">
                    <label class="radio-inline">
                        <input <?= $detail->status == \App\Models\Branches::BRANCHES_ACTIVE ? 'checked="checked"' : '' ?> tabindex="205" value="{{\App\Models\Branches::BRANCHES_ACTIVE}}" type="radio" name="status"> Hoạt động
                    </label>
                </div>
                <div class="col-md-2">
                    <label class="radio-inline">
                        <input <?= $detail->status == \App\Models\Branches::BRANCHES_DEACTIVE ? 'checked="checked"' : '' ?> tabindex="206" value="{{\App\Models\Branches::BRANCHES_DEACTIVE}}" type="radio" name="status"> Không hoạt động
                    </label>
                </div>
                <div class="col-md-6">
                    <div class="alert alert-danger error-mess-branche hidden"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="address" class="control-label">Địa chỉ</label>
                    <textarea class="form-control" rows="3" name="address" id="address" maxlength="350" tabindex="207" style="resize: none;">{{ htmlentities($detail->address) }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="note" class="control-label">Ghi chú</label>
                    <textarea class="form-control" rows="3" name="note" id="note" maxlength="500" style="resize: none;" tabindex="208">{{ htmlentities($detail->note) }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <input type="button" id="branchs-form-save" value="Lưu" class="btn btn-primary" tabindex="209">
                        <input type="button" value="Thoát" data-dismiss="modal" class="btn btn-warning" tabindex="210">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    setTimeout(function () {
        $('#code').focus();
    }, 500);
</script>