<table>
    <tbody>
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">Danh sách hội viên</td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">Chi hội {{ $detailBranch->name }}</td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: left; font-weight: 900">I. Ban chấp hành</td>
        </tr>
        <tr class="header">
            <td >TT</td>
            <td>Họ tên</td>
            <td>Chức vụ</td>
            <td>Ngày sinh</td>
            <td>Điện thoại</td>
            <td>Email</td>
        </tr>

        @foreach ($listManager as $key => $item)
            @php if ($item->employees == "") continue;@endphp
        <tr class="main-content">
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->employees->fullname }}</td>
            <td>{{ isset($listOption[$item['option_code']]) ? $listOption[$item['option_code']] : '' }}</td>
            <td>{{ $item->employees->birthday }}</td>
            <td>{{ $item->employees->phone }}</td>
            <td>{{ $item->employees->email }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td colspan="9" style="text-align: left; font-weight: 900">II. Hội viên</td>
        </tr>
        <tr class="header">
            <td >TT</td>
            <td>Họ tên</td>
            <td>Ngày sinh</td>
            <td>Số thẻ HDV</td>
            <td>Số thẻ hội viên</td>
            <td>Ngôn ngữ hướng dẫn</td>
            <td>Hội viên VTGA từ</td>
            <td>Số điện thoại</td>
            <td>Địa chỉ email</td>
            <td>Địa chỉ liên hệ</td>
        </tr>

        @foreach ($listMember as $key => $item)
        @php
        $mobile = $item->firstMobile . (!empty($item->secondMobile) ? ',' . $item->secondMobile : '' );
        $emailS = $item->firstEmail . (!empty($item->secondEmail) ? ','. $item->secondEmail : '' );
        @endphp
        <tr class="main-content">
            <td>{{ $key + 1 }}</td>
            <td>{{ htmlentities($item->fullName) }}</td>
            <td>{{ date('d/m/Y', strtotime($item->birthday)) }}</td>
            <td>{{ $item->touristGuideCode }}</td>
            <td>{{ $item->member_code }}</td>
            @php
                $language = $item->guideLanguage;
            @endphp
            <?php $languageName = ""; ?>
            @foreach(explode(",", $language) as $key => $lang)
                @if (isset($listLanguage[$lang]))
                    <?php $languageName .= $listLanguage[$lang] ."," ?>
                @endif
            @endforeach
            <td>{{ rtrim($languageName, ",") }}</td>
            <td>{{ $item->member_from }}</td>
            <td>{{ $mobile }}</td>
            <td>{{ $emailS }}</td>
            <td>{{ htmlentities($item->address) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<style>
    .header td {
        background: #FFFF00;
        border: 1px solid #000;
    }
    .main-content td {
        border: 1px solid #000;
    }
</style>
