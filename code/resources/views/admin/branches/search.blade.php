<form class="page-wrap" name="" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row">
        @if (empty(auth()->user()->province_type))
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                VPĐD
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select tabindex="1" id="search_parent_id" name="search_parent_id" class="form-control">
                    <option value="">Tất cả</option>
                    @if (!empty($representative_office))
                    @foreach($representative_office as $key => $val)
                    @php $selected = Input::get('search_parent_id', '') == $key ? 'selected="selected"' : ''; @endphp
                    <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Mã Chi hội
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input class="form-control" tabindex="2" id="search_code" type="text" placeholder="Mã Chi hội" name="search_code" value="{{ Input::get('search_code', '') }}" maxlength="50">
            </div>
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Tên Chi hội
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input class="form-control" tabindex="3" id="search_name" type="text" placeholder="Tên Chi hội" name="search_name" value="{{ Input::get('search_name', '') }}" maxlength="50">
            </div>
        @endif
        
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Địa chỉ
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="4" id="search_address" type="text" placeholder="Địa Chỉ Chi hội" name="search_address" value="{{ Input::get('search_address') }}" maxlength="50">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số điện thoại
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="5" id="search_phone"  placeholder="Số điện thoại" type="text" name="search_phone" value="{{ Input::get('search_phone') }}" maxlength="20" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Email
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="6" id="search_email"  placeholder="Địa chỉ email" type="text" name="search_email" value="{{ Input::get('search_email') }}" maxlength="100">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="7" id="search_status" name="search_status" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($statusOption))
                    @foreach($statusOption as $key => $val)
                        @php $selected = is_numeric(Input::get('search_status', '')) && Input::get('search_status', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="8" type="submit" onclick="return submitForm();" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<script>
    $(document).keypress(function (e) {
        if($('#formSearch input:focus, #formSearch button:focus').length != 0) {
            if (e.which === 13) {
                submitForm();
            }
        }
    });
    $(function () {
        $("#search_parent_id").focus();
    });
    function submitForm() {
        $('#formSearch').submit();
        return false;
    }
</script>