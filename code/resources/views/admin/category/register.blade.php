<div id="_addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_add"></h4>
            </div>
            <form action="{{url('/officesys/categories/check_category')}}" id="categoryForm_add" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="code_add" class="col-md-3 control-label" style="padding-top: 7px">Mã danh mục<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="code_add" id="code_add" maxlength="10" tabindex="100">
                                        <span class="errorcode_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nameAll_add" class="col-md-3 control-label" style="padding-top: 7px">Tên danh mục<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="nameAll_add" id="nameAll_add" maxlength="191" tabindex="101">
                                        <span class="errorNameAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row" style="margin-bottom: 30px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="optionAll_add" style="padding-top: 7px">Loại danh mục<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="optionAll_add" id="optionAll_add" tabindex="102">
                                            <option value="">Chọn danh mục</option>
                                            @foreach($options as $key => $value)
                                                <option value="{{ $value->code }}"
                                                        @if (array_get($response_view, 'option_code', '') == $value->code) selected="selected" @endif>{{ $value->value }}</option>
                                            @endforeach

                                        </select>
                                        <span class="errorOptionAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary add" type="button" tabindex="103">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="104">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
