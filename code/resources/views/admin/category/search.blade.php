<form class="page-wrap row" name="filter" method="get" id="formSearch" action="{{ url ('/officesys/categories') }}">
    {{ csrf_field() }}
    <div class="row" style="width: 100%">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
           Tên danh mục
        </div>

        <div style="margin-bottom: 10px;" class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <input type="text" max="255" id="nameall" name="nameall" value="{{ array_get($response_view, 'nameall', '') }}" placeholder="Nhập từ khóa muốn tìm kiếm" tabindex="1">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
            Loại danh mục
        </div>
        <div style="" class="field-wrap dropfield col-md-4 col-sm-6 col-xs-12" >
            <select name="option_code" id="option_code" tabindex="2">
                {{--<option value="">Chọn danh mục muốn tìm kiếm</option>--}}
                @foreach($options as $key => $value)
                <option value="{{ $value->code }}"
                        @if (array_get($response_view, 'option_code', '') == $value->code) selected="selected" @endif>{{ $value->value }}</option>
                @endforeach
            </select>
        </div>
        <div style="margin-left: 45%;" class="field-wrap col-md-2 col-sm-3 col-xs-2">
            <input type="submit" class="button btn-primary" name="search" value="Tìm kiếm" tabindex="3" style="width: 140px">
        </div>
    </div>
</form>
<input type="hidden" value="{{array_get($response_view, 'option_code', '')}}" id="procode_all" name="procode_all"/>
<input type="hidden" value="" id="dm-id" name="dm-id"/>

<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
</script>