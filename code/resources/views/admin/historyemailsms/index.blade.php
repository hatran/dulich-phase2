@extends('admin.layouts.app')
@section('title', 'Danh sách lịch sử gửi SMS và Email')
@section('header_embed')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/bootstrap-table/dist/bootstrap-table.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vita.css') }}"/>
@endsection
@section('content')
    <div class="main-content" style="padding-bottom: 75px;">
        @if (session()->get('error'))
            <div class="alert alert-danger">
                <div>{!! session()->get('error') !!}</div>
            </div>
        @endif
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <div class="alert alert-success">
                <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
            </div>
        @endif
        @include('admin.historyemailsms.search')
        <div class="clearfix"></div>
        <div class="page-wrap">
            <div class="top-navigation row">
                <div class="col-sm-6 col-xs-12 total">
                    Tống số: <?= $total ?>
                </div>
            </div>
            @php
                use App\Constants\UserConstants;
                $showDownload = false;
                $contentWidth = 53;
                if ((int) auth()->user()->role !== UserConstants::CONTENT_MANAGER_SUB_GROUP_USER) {
                    $showDownload = true;
                    $contentWidth = 46;
                }
            @endphp
            <div class="table-wrap">
                <table id="table"
                       data-toggle="table"
                       data-search="false"
                       data-show-columns="true"
                       data-pagination="true"
                       data-page-list="[5, 10, 20, 50, 100, 200]"
                       data-side-pagination="server"
                       data-url="{{ $ajaxFliterUrl }}"
                       data-pagination-first-text="Trang đầu"
                       data-pagination-last-text="Trang cuối"
                       data-flat="true">
                    <thead>
                    <tr>
                        <th data-sortable="false" data-width="5%" data-field="order_number" data-sortable="true" data-align="center">STT</th>
                        <th data-sortable="false" data-width="5%" data-field="type" data-sortable="true" data-cell-style="cellStyle">Email / SMS</th>
                        <th data-sortable="false" data-width="15%" data-field="email_sms_to" data-sortable="true" data-cell-style="cellStyle">Địa chỉ nhận</th>
                        <th data-sortable="false" data-width="15%" data-field="created_at" data-sortable="true" data-cell-style="cellStyle">Thời gian gửi</th>
                        <th data-sortable="false" data-width="{{ $contentWidth }}%" data-field="content" data-sortable="true" data-cell-style="cellStyle">Nội dung</th>
                        <th data-sortable="false" data-width="7%" data-field="action" data-sortable="true" data-align="center">Gửi lại</th>
                        @if ($showDownload)
                            <th data-sortable="false" data-width="7%" data-field="download" data-sortable="true" data-align="center">Chức năng</th>
                        @endif
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/bootstrap-table-1.9.1/dist/bootstrap-table.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-table-en-US.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/vita.js') }}" type="text/javascript"></script>
@endsection
