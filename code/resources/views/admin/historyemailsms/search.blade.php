<form class="page-wrap" name="" method="get" id="formSearch">
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Email/SMS
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select class="type" id="type" name="type" style="-webkit-appearance: menulist">
                <option value="" selected=""></option>
                <option value="{{ \App\Models\HistoryEmailSMS::SMS_LOG_TYPE }}" {{ (array_get($response_view, 'type', '') === '2') ? 'selected' : '' }}>SMS</option>
                <option value="{{ \App\Models\HistoryEmailSMS::EMAIL_LOG_TYPE }}" {{ (array_get($response_view, 'type', '') === '1') ? 'selected' : '' }}>Email</option>
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Địa chỉ nhận
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="email_sms_to" name="email_sms_to" placeholder="Địa chỉ nhận"
                   value="{{ htmlentities(array_get($response_view, 'email_sms_to', '')) }}" maxlength="100">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Ngày gửi Từ ngày
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="start_date" name="start_date" placeholder="Từ ngày" class="date"
                   value="{{ htmlentities(array_get($response_view, 'start_date', '')) }}" maxlength="10">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Đến ngày
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="end_date" name="end_date" placeholder="Đến ngày" class="date"
                   value="{{ htmlentities(array_get($response_view, 'end_date', '')) }}" maxlength="100">
        </div>
        {{ csrf_field() }}
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="6" type="submit" onclick="return submitForm();" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
    function submitForm() {
        if ($('#start_date').val() != "" && $('#end_date').val() != "") {
            var startDate = convertDate($('#start_date').val());
            var endDate = convertDate($('#end_date').val());
          
            if(startDate > endDate) {
                alert("Ngày gửi bắt đầu không được lớn hơn ngày kết thúc");
            } else {
                $('#formSearch').submit();
            }
            return false;
        }
    }

    function convertDate(date) {
        var convertFormat = date.split("/");
        var firstString = convertFormat[2];
        var lastString = convertFormat[0];
        var completeString = firstString+"-"+convertFormat[1]+"-"+lastString;
        return completeString;
    }
</script>