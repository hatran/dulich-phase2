@extends('admin.layouts.app')
@section('title', 'Danh sách lịch sử hệ thống')
@section('header_embed')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/bootstrap-table/dist/bootstrap-table.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vita.css') }}"/>
@endsection
@section('content')
    <div class="main-content" style="padding-bottom: 75px;">
        @if (session()->get('error'))
            <div class="alert alert-danger">
                <div>{!! session()->get('error') !!}</div>
            </div>
        @endif
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <div class="alert alert-success">
                <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
            </div>
        @endif
        @include('admin.history.search')
        <div class="clearfix"></div>
        <div class="page-wrap">
            <div class="top-navigation row">
                <div class="col-sm-6 col-xs-12 total">
                    Tống số: <?= $total ?>
                </div>
            </div>
            <div class="table-wrap">
                <table id="table"
                       data-toggle="table"
                       data-search="false"
                       data-show-columns="true"
                       data-pagination="true"
                       data-page-list="[5, 10, 20, 50, 100, 200]"
                       data-side-pagination="server"
                       data-url="{{ $ajaxFliterUrl }}"
                       data-pagination-first-text="Trang đầu"
                       data-pagination-last-text="Trang cuối"
                       data-flat="true">
                    <thead>
                    <tr>
                        <th data-sortable="false" data-width="5%" data-field="order_number" data-align="center"
                            data-cell-style="cellStyle">STT</th>
                        <th data-sortable="false" data-width="15%" data-field="name" data-sortable="true"
                            data-cell-style="cellStyle">Chức năng</th>
                        <th data-sortable="false" data-width="10%" data-field="username" data-sortable="true"
                            data-cell-style="cellStyle">Tên đăng nhập</th>
                        <th data-sortable="false" data-width="10%" data-field="fullname" data-sortable="true"
                            data-cell-style="cellStyle">Họ và tên</th>
                        <th data-sortable="false" data-width="10%" data-field="ip" data-sortable="true"
                            data-cell-style="cellStyle" data-align="center">IP</th>
                        <th data-sortable="false" data-width="10%" data-field="created_at" data-sortable="true"
                            data-cell-style="cellStyle" data-align="center">Ngày thao tác</th>
                        <th data-sortable="false" data-width="15%" data-field="description_short" data-sortable="true"
                            data-cell-style="cellStyle">Mô Tả</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/bootstrap-table-1.9.1/dist/bootstrap-table.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap-table-en-US.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/vita.js') }}" type="text/javascript"></script>
@endsection
