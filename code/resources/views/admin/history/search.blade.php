<div class="filter">
    <form class="page-wrap" name="filter" method="get" id="formSearch" action="" _lpchecked="1">
        {{ csrf_field() }}
        <div class="row">
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Họ và tên
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                <input type="text" id="fullname" name="fullname" placeholder="Tên người thao tác"
                       value="{{ htmlentities(array_get($response_view, 'fullname', '')) }}" maxlength="10"/>
            </div>
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Chức năng
            </div>

            <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                <input type="text" id="title" name="title" placeholder="Tên chức năng"
                       value="{{ htmlentities(array_get($response_view, 'title', '')) }}" maxlength="100"/>
            </div>
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Ngày xử lý Từ ngày
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                <input type="text" id="start_date" name="start_date" placeholder="Từ ngày" class="date"
                       value="{{ htmlentities(array_get($response_view, 'start_date', '')) }}" maxlength="10"/>
            </div>
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Đến ngày
            </div>

            <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                <input type="text" id="end_date" name="end_date" placeholder="Đến ngày" class="date"
                       value="{{ htmlentities(array_get($response_view, 'end_date', '')) }}" maxlength="100"/>
            </div>


            <div class="field-wrap col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 30px;margin-top: 30px;text-align:  center;">
                <input type="submit" class="button btn-primary" name="search" value="Tìm kiếm"
                       style="width: 140px; margin: auto;"/>
            </div>
        </div>
    </form>
</div>