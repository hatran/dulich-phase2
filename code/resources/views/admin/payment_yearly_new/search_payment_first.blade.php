<?php

use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
?>
<style>
    body .field-wrap.dropfield:after {
        top: 10px;
    }
    body .field-wrap .datetime-icon {
        top: 10px;
        right: 35px;
    }

    body #content .main-content form[name=filter] {
        display: block !important;
    }
</style>
<form class="page-wrap" name="filter" method="get" id="formSearch" style="margin-bottom: 10px;">
    {{ csrf_field() }}
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
			Mã số hồ sơ
		</div>
		<div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
			<input id="file_code" tabindex="1" type="text" name="file_code" value="{{ htmlentities(array_get($response_view, 'file_code', '')) }}" maxlength="6">
		</div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số thẻ HDV
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="idNumber" type="text" name="touristGuideCode" tabindex="3"
                   value="{{ htmlentities(array_get($response_view, 'touristGuideCode', '')) }}" maxlength="9">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Họ và tên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="name" type="text" tabindex="4" name="fullName" value="{{ htmlentities(array_get($response_view, 'fullName', '')) }}" maxlength="50">
        </div>
        @if (empty(auth()->user()->branch_id))
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Nơi đăng ký sinh hoạt
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select class="province_code" id="province_code" name="province_code" tabindex="12">
                    <option value="" selected></option>
                </select>
                <input type="hidden" id="province-code-selected" value="{{ array_get($response_view, 'province_code', '') }}">
                <input type="hidden" id="filter-by-status" value="<?php echo isset($statusFilter) ? $statusFilter : -1 ?>">
            </div>
        @endif

        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;">
            <input type="button" class="button btn-primary" style="width: 100px" name="search" id="search" onclick="submitForm();" value="Tìm kiếm" tabindex="13">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm hồ sơ</h4>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Ngày chứng từ không hợp lệ</h4>
                <br/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showAlertYear" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Năm nộp hội phí</h4>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Năm đầu không được lớn hơn năm sau</h4>
                <br/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showAlertChooseOne" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm hội phí</h4>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Vui lòng nhập Mã số hồ sơ hoặc Số thẻ hội viên</h4>
                <br/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });

    function parseDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }

    function submitForm() {
        $('#formSearch').submit();
    };

    var changeProvinceCode = function(){
        var val = jQuery('#province_type').val();
        var sta = jQuery('#filter-by-status').val();
        changeProvinceType(val, sta);
    }

    function changeProvinceType(parent_id, province_code_selected = '', status) {
        var province_type_array = [];
        $('#province_type option').each(function() {
            if ($(this).val() != "") {
                province_type_array.push($(this).val());
            }
        });

        $.ajax({
            type: "POST",
            url: '/searchactivityregistrationplace',
            data: { parent_id: parent_id, status: status, province_type: province_type_array, _token: '{{csrf_token()}}' },
            success: function (data) {
              $("#province_code option").remove();
              var option = "";
              option += "<option value='' selected></option>";
              $.each(data.value, function (i, value) {
                if (value.id == province_code_selected) {
                    option += "<option value="+ value.id +" data-parent="+ value.parent_id +" Selected>"+ value.name +"</option>";
                }
                else {
                    option += "<option value="+ value.id +" data-parent="+ value.parent_id +">"+ value.name +"</option>";
                }
              })
              $("#province_code").append(option);
            },
            error: function (data, textStatus, errorThrown) {
                console.log(data);
            },
        });
    };
    var parent_id = $("#province_type option:selected").val();
    var province_code_selected = $("#province-code-selected").val();
    var filter_by_status = $("#filter-by-status").val();

    changeProvinceType(parent_id, province_code_selected, filter_by_status);
</script>
