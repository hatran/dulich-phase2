<?php
setlocale(LC_MONETARY,"en_US");
?>

@extends('admin.layouts.app')
@section('title', 'Nộp hội phí lần đầu')

@section('header_embed')
    <link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <style>
        a.button {
            padding-left: 20px;
            padding-right: 20px;
            margin-left: 5px;
            margin-right: 5px;
        }

        a.button > span.glyphicon {
            color: white;
        }

        .no-break {
            white-space: nowrap;
        }

        .modal-body .line {
            margin-bottom: 10px;
        }

        .dialog-content > span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }

        .dialog-content > span.error-msg {
            color: #a94442;
        }

        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            /*background-color: #337ab7;*/
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }

        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }

        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }
        .modal-title {
            float: left;
            font-size: 20px;
            font-weight: bold;
            line-height: 1.42857;
            margin: 0;
            width: 90%;
        }
        .font-all-popup-title{
            font-weight: bold;
        }

        body .choices.checkbox-list .choice.chosen label:after, body .choices.checkbox-list .choice label:after {
            margin-top: -8px
        }
        body .checkall {
            padding-left: 25px !important;
        }
        body .checkall .choice {
            padding-left: 35px;
            padding-right: 10px;
            cursor: pointer;
        }
        body .checkall .choice input {
            display: none !important;
        }
        body .checkall .choice label {
            position: relative;
            cursor: pointer;
            margin-bottom: 0;
        }
        body .checkall .choice label:before {
            content: "";
            width: 17px;
            height: 17px;
            margin-top: -9px;
            border: 1px solid #6C6B6B;
            position: absolute;
            left: -25px;
            top: 50%;
            border-radius: 2px;
        }
        body .checkall .choice label:after {
            margin-top: -9px;
            content: "\F00C";
            font-family: 'FontAwesome';
            font-size: 12px;
            color: #BBBABA;
            left: -22px;
            position: absolute;
            top: 50%;
            opacity: 0;
            transition: 0.3s;
            -moz-transition: 0.3s;
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
            -ms-transition: 0.3s;
        }
        body .checkall .choice.chosen label:after {
            opacity: 1;
            color: #2B95CB;
        }
        body .checkall .choice:hover label:after {
            opacity: 1;
        }

        input[type=checkbox]
        {
            width: 18px;
            height: 18px;
            text-align: center; /* center checkbox horizontally */
            vertical-align: middle; /* center checkbox vertically */
        }

        body #content .table-wrap table tr td {
            padding-right: 0px;
        }
    </style>
@endsection

@section('content')
    <div class="page-wrap">

        <div class="main-content" style="padding-bottom: 75px;">
        @include('admin.payment_yearly_new.search_payment_first')
        @include('admin.layouts.message')
            <input id="exportHdd" type="hidden" value="{{ url('/officesys/payment_yearly/excel/export_excel') }}{{$query_string != '' ? '?'.$query_string : ''}}"/>

            <a class="resend-notification-sms-first button btn-primary" style="float:  right;width: 200px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Gửi SMS Hội phí lần đầu</a>
            <a class="resend-notification-email-first button btn-primary" style="float:  right;width: 200px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Gửi Email Hội phí lần đầu</a>
            <div class="total">Tổng số : {{$count_members}}</div>

            <div class="table-wrap" style="margin-top: 5px">
                <table>
                    <thead>
                    <tr>
                        <td width="5%">Số TT</td>
                        <td width="5%">Mã hồ sơ</td>
                        <td width="7%">Số thẻ HDV</td>
                        <td width="15%">Họ và tên</td>
                        <td width="12%">Số điện thoại</td>
                        <td width="13%">Ngày phê duyệt</td>
                        <td width="15%">Nơi đăng ký sinh hoạt</td>
                        <td width="15%">Trạng thái</td>
                        <td width="7%">Chức năng</td>
                        <td width="5%" style="">TB Nộp Phí<br>
							<input type="checkbox" class="checkAll" name="member">
							<label>&nbsp;</label>
						</td>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($objMembers->count() == 0)
                        <tr>
                            <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            <tr>
                                <td class="text-center"> {{ array_get($response_view, 'status') != -1 ? $current_paginator + $key + 1 : $key + 1 }} </td>
                                <td class="text-center">
                                    {{ $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('admin_list_member_detail_view' , !empty($objMember->mid) ? $objMember->mid : (!empty($objMember->id) ? $objMember->id : '')) }}">{{ $objMember->touristGuideCode }} </a>
                                </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }}</td>
                                <td class="text-center"> {{ $objMember->firstMobile }} </td>
                                <td class="text-center"> {{ empty($objMember->approved_at) ? '' : date('d/m/Y', strtotime($objMember->approved_at)) }} </td>
                                <td class="text-center"> {{ array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '') }} </td>
                                <td class="text-center">
                                    Chờ đóng hội phí lần đầu
                                </td>
                                <td align="center" class="text-center">
                                    <a class="btn-function add-modal"
                                       id="btnAddPayment"
                                       href="#"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="Nộp hội phí"
                                       data-id="{{ $objMember->id }}?mpuid=null"
                                       data-status="{{ $objMember->status }}"
                                       data-approved="{{ empty($objMember->approved_at) ? '' :  date('d/m/Y', strtotime($objMember->approved_at)) }}">
                                        <span class="glyphicon glyphicon-plus" style="font-size: 16px; color: #0000ff; "></span>
                                    </a>
                                </td>
								<td class="text-center">
									<span class="checkitem">
										<input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{ $objMember->id }}" class="checkbox-pr">
										<label for="member {{ $key + 1 }}">&nbsp;</label>
									</span>
								</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            @include('admin.layouts.pagination')
            <div style="margin-top: 17px; float: left;">
            <a class="button btn-primary" style="float:  right;width: 150px; height: 37px; line-height: 24px; margin-bottom: 5px; padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)" onclick="createExcel('1');" data-toggle="tooltip" title="Xuất danh sách hội viên chưa đóng hội phí lần đầu">Xuất excel</a>
            </div>
        </div>
    </div>
    @if (session('error_message'))
        <div type="hidden" id="error_message">
            {{ session('error_message') }}
        </div>
    @endif

    <input type="hidden" id="member-id"/>
    <input type="hidden" id="current-status"/>
    <input type="hidden" id="approved_at"/>
    <input type="hidden" id="verified_at"/>

    @include('admin.payment_yearly.detail')
    @include('admin.payment_yearly.create')
    @include('admin.payment_yearly.edit')
    @include('admin.payment_yearly.delete')
    @include('admin.payment_yearly.export_modal')

@endsection

@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>

    <script>
        function createExcel(flg){
            //flg = 1 Hội viên chưa nộp hội phí lần đầu
            window.location = $("#exportHdd").val()+'&type='+flg;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#btnsubmit").click();
                }
            }
        });
        function clearErrorFields() {
            $('.errorNumberPayment').addClass('hidden');
            $('.errorDatePayment').addClass('hidden');
            $('.errorCurrency').addClass('hidden');
            $('.errorContent').addClass('hidden');
        }

        var url = "{{ url('/officesys/payment_yearly') }}"
        function htmlEntities(str) {
            return str;
        }

        var listFeePolicy = [];

        function showPopup(memberId, postfix) {
            listFeePolicy = [];
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';

            switch (postfix) {
                case '_add':
                    title = 'Thêm mới thông tin hội phí';
                    canEdit = true;
                    break;
            }

            $('.position-number-payment').removeClass('error_border');
            $('.position-date').removeClass('error_border');
            $('.position-currency').removeClass('error_border');
            $('.position-content').removeClass('error_border');

            $('.modal-title').text(title);
            if (canEdit === true) {
                clearErrorFields();
            }

            var numberOfYear = $('.select2-selection--multiple');
            $('.position_numberofyear').removeClass('error_border');
            $('.errorNumberOfYear').addClass('hidden');
            numberOfYear.removeClass('error_border');

            $(modalId).modal('show');
            $(modalId + ' .modal-dialog').loader('show');

            $('#number_payment' + postfix).val('');
            $('#date_payment' + postfix).val('');
            $('#currency' + postfix).val('');
            $("#transform_currency" + postfix).text('');
            $("#content_add").val('');
            $("#note_add").val('');

            $.ajax({
                url: url + "/" + memberId,
                type: 'GET',
                success: function (response) {
                    $(modalId + ' .modal-dialog').loader('hide');
                    var objMember = response.objMember;
                    $('#code' + postfix).text(objMember.touristGuideCode);
                    $('#name' + postfix).text(objMember.fullName);
                    $('#birthday' + postfix).text(objMember.formattedBirthday);
                    $("#verified_at").val(toTimestamp(objMember.verified_at));
                    $("#member_code" + postfix).text(objMember.member_code);
                    $("#identitycard" + postfix).text(objMember.cmtCccd);
                    $("#joining" + postfix).text(objMember.member_from);
                    $("#provincecode" + postfix).text(objMember.provinceName);
                    $("#last_year" + postfix).val(response.lastYear);

                    //add fee policy
                    if(response.objFeePolicy) {
                        response.objFeePolicy[0].forEach(e=>{
                            if(e.typeOp == 1) {
                                // if(e.perfect_fee_policy_code == "fee-policy-001") {
                                //     listFeePolicy.push(e);
                                // } else if(e.perfect_fee_policy_code == "fee-policy-002") {
                                //     let approved_at = new Date(objMember.approved_at);
                                //     approved_at.setFullYear(approved_at.getFullYear() + 3);
                                //     if(approved_at.getTime() <= new Date().getDate()) {
                                //         listFeePolicy.push(e);
                                //     }
                                // } else if(e.perfect_fee_policy_code == "fee-policy-003") {
                                //     let approved_at = new Date(objMember.approved_at);
                                //     approved_at.setFullYear(approved_at.getFullYear() + 5);
                                //     if(approved_at.getTime() <= new Date().getDate()) {
                                //         listFeePolicy.push(e);
                                //     }
                                // } else if(e.perfect_fee_policy_code == "fee-policy-006") {
                                //     listFeePolicy.push(e);
                                // }
                                listFeePolicy.push(e);
                            } else {
                                let listProvince = e.list_id.split(",");
                                if(listProvince.includes(objMember.province_code.toString())) {
                                    listFeePolicy.push(e);
                                }
                            }
                        })

                        if(listFeePolicy.length > 0) {
                            $(".fee_policy").show();
                            let maxMoney = listFeePolicy[0];
                            $(".fee_policy_select").html("");
                            listFeePolicy.forEach(e=>{
                                $(".fee_policy_select").append(new Option(e.name,e.code));
                                if(maxMoney.money < e.money) {
                                    maxMoney = e;
                                }
                            })
                            $(".fee_policy_select").val(maxMoney.code);
                            $(".fee_policy .dialog-content .money").html(maxMoney.money);
                        }
                    }
                }
            });
        }

        function changeFeePolicy(e) {
            listFeePolicy.forEach(item=>{
                if(e.value == item.code) {
                    $(".fee_policy .dialog-content .money").html(item.money);
                    getMoney();
                }
            })
        }

        function updateExpirationDate(ele) {
            var dt = $(ele).val();
            var arrDt = dt.split('/');
            var newDt = arrDt[2] + '-' + arrDt[1] + '-' + arrDt[0];
            var date = new Date(newDt);
            $("#expiration_add").val(new Date(date.setFullYear(date.getFullYear() + 1)).toLocaleDateString('en-GB'));
        }

        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            var memberId = $(this).data('id');
            $('input[type=hidden]#member-id').val(memberId);
            $("#current-status").val($(this).data("status"));
            $("#approved_at").val($(this).data("approved"));
            $("#expiration_add").val(new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toLocaleDateString('en-GB'));
            showPopup(memberId, '_add');
        });

        function doAction(memberId, postfix) {
            var action = 'POST';
            var modalId = '#' + postfix + 'Modal';
            var data = {
                'number_payment': $('#number_payment' + postfix).val(),
                'currency': $(".fee_policy_end .money").html() ? $(".fee_policy_end .money").html() : $('#currency' + postfix).val(),
                'date_payment': $('#date_payment' + postfix).val(),
                'content': $('#content' + postfix).val(),
                'note': $('#note' + postfix).val(),
                'status': $("#current-status").val(),
                'date_expiration': $("#expiration" + postfix).val()
            };
            //validation  Ngày chứng từ
            var date = new Date();
            var today = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            var date_payment = $("#date_payment" + postfix);
            var number_payment = $('#number_payment' + postfix);
            var currency = $('#currency' + postfix);
            var content = $('#content' + postfix);
            var note = $('#note' + postfix);
            var apprved_date = parseDate($('#approved_at').val()).getTime()/1000;


            if (postfix == '_add') {
                $('.position-number-payment').removeClass('error_border');
                $('.errorNumberPayment').addClass('hidden');
                $('.position-date').removeClass('error_border');
                $('.errorDatePayment').addClass('hidden');
                $('.position-currency').removeClass('error_border');
                $('.errorCurrency').addClass('hidden');
                $('.position-content').removeClass('error_border');
                $('.errorContent').addClass('hidden');
            }

            var flg = 0;
            if (postfix == '_add') {
                if (date_payment.val() == '') {
                    $(".errorDatePayment").html('Ngày chứng từ không được bỏ trống');
                    $(".errorDatePayment").removeClass('hidden');
                    date_payment.addClass('error_border');
                    flg = 1;
                } else {
                    today = parseDate(today).getTime();
                    var verifiedDate = $('#verified_at').val();
                    var paymentDate = parseDate(date_payment.val()).getTime();
                    if (today < paymentDate) {
                        setTimeout(function () {
                            $(".errorDatePayment").html('Ngày chứng từ không được lớn hơn ngày hiện tại');
                            $(".errorDatePayment").removeClass('hidden');
                            date_payment.addClass('error_border');
                        }, 500);

                        flg = 1;
                    } else if(paymentDate < apprved_date){
                        $(".errorDatePayment").html('Ngày chứng từ không được nhỏ hơn ngày phê duyệt');
                        $(".errorDatePayment").removeClass('hidden');
                        date_payment.addClass('error_border');
                        flg = 1;
                    } else {
                        $(".errorDatePayment").addClass('hidden');
                        $(".errorDatePayment").html('');
                        date_payment.removeClass('error_border');
                    }
                }

                //validation so chung tu

                if (number_payment.val() == '') {
                    $(".errorNumberPayment").html('Số chứng từ không được bỏ trống');
                    $(".errorNumberPayment").removeClass('hidden');
                    number_payment.addClass('error_border');
                    flg = 1;
                } else {
                    $(".errorNumberPayment").html('');
                    $(".errorNumberPayment").addClass('hidden');
                    number_payment.removeClass('error_border');

                }
                //validation so tien
                if (currency.val() == '') {
                    $(".errorCurrency").html('Số tiền không được bỏ trống');
                    $(".errorCurrency").removeClass('hidden');
                    currency.addClass('error_border');
                    flg = 1;
                } else {
                    var regexSt = /^([0-9])+$/;
                    if (!regexSt.test(currency.val())) {
                        $(".errorCurrency").html('Số tiền phải là số nguyên dương > 0');
                        $(".errorCurrency").removeClass('hidden');
                        currency.addClass('error_border');
                        flg = 1;
                    } else if(currency.val() < 0) {
                        $(".errorCurrency").html('Số tiền phải là số nguyên dương > 0');
                        $(".errorCurrency").removeClass('hidden');
                        currency.addClass('error_border');
                        flg = 1;
                    } else {
                        $(".errorCurrency").html('');
                        $(".errorCurrency").addClass('hidden');
                        currency.removeClass('error_border');
                    }
                }
            }

            if (flg == 1) {
                setTimeout(function () {
                    $(modalId).modal('show');
                }, 500);
                return false;
            }
			$('#btnsubmit').text("Đang xử lý ...").attr('disabled', 'disabled');

            action = 'POST';
            $.ajax({
                type: action,
                url: url + '/' + memberId,
                data: data,
                success: function (data) {
                    clearErrorFields();
                    if (data.errors) {
                        var errorsPayment = data.errors;
                        if (errorsPayment.memberInvalid) {
                            toastr.warning(errorsPayment.memberInvalid, 'Thông báo', {timeOut: 2000});
                            $(modalId).modal('hide');
                            document.location.reload();
                        }
                        if (errorsPayment.uniqueNumberPayment) {
                            $(modalId).modal('hide');
                            alert(errorsPayment.uniqueNumberPayment);
                            document.location.reload();
                        }
                        setTimeout(function () {
                            $(modalId).modal('show');
                        }, 500);
                        if (errorsPayment.invalid) {
                            toastr.warning(errorsPayment.number_payment, '', {timeOut: 2000});
                        }
                        if (errorsPayment.number_payment) {
                            $('.position-number-payment').addClass('error_border');
                            $('.errorNumberPayment').removeClass('hidden');
                            $('.errorNumberPayment').text(errorsPayment.number_payment);
                        }
                        if (errorsPayment.date_payment) {
                            $('.position-date').addClass('error_border');
                            $('.errorDatePayment').removeClass('hidden');
                            $('.errorDatePayment').text(errorsPayment.date_payment);
                        }
                        if (errorsPayment.currency) {
                            $('.position-currency').addClass('error_border');
                            $('.errorCurrency').removeClass('hidden');
                            $('.errorCurrency').text(errorsPayment.currency);
                        }
                        if (errorsPayment.content) {
                            $('.position-content').addClass('error_border');
                            $('.errorContent').removeClass('hidden');
                            $('.errorContent').text(errorsPayment.content);
                        }
                    } else {
                        toastr.success(data.success, 'Thông báo', {timeOut: 2000});
                        $(modalId).modal('hide');
                        document.location.reload(true);
                    }
                }
            });
        }

        $('.modal-footer').on('click', '.add', function () {
            var memberId = $('input[type=hidden]#member-id').val();
            doAction(memberId, '_add');
        });

        function sendNotification(memberIds) {
            resetCheckboxes();

            $.ajax({
                type: 'POST',
                url: url + '/notifications/send',
                data: {
                    'member-ids': memberIds
                },
                success: function (data) {
                    if (data.errors) {
                        toastr.warning('Có lỗi hệ thống xảy ra. Vui lòng thử lại trong ít phút nữa', '', {timeOut: 2000});
                        console.log(data.errors);
                    } else {
                        toastr.success(data.success, 'Hệ thống đã gửi thông báo thành công', {timeOut: 1000});
                    }
                }
            });
        }

        function checkedIds() {
            return $("input[id='chk-inform']:checked").map(function () {
                return $(this).data('id');
            }).get();
        }

        function resetCheckboxes() {
            $("input[id='chk-inform']:checked").each(function () {
                $(this).prop('checked', false);
            });
        }

        $(document).on('click', '#send-notification', function (event) {
            event.preventDefault();
            isCheck = true;
            var checkIds = checkedIds();
            if (checkIds.length > 0) {
                toastr.clear()
                $("#send-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function () {
                    $("#send-notification").css('pointer-events', 'auto');
                }
                toastr.warning('Đang tiến hành gửi thông báo. Vui lòng đợi trong giây lát.', '', {timeOut: 2000});
                sendNotification(checkIds);
            } else {
                toastr.clear()
                $("#send-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function () {
                    $("#send-notification").css('pointer-events', 'auto');
                }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $(document).ready(function () {
            document.getElementById("file_code").focus();
        });
        function toTimestamp(strDate){
            var datum = Date.parse(strDate);
            return datum/1000;
        }

        //print
        function checkedNames() {
            check = 0;
            $("input[name=member]:checked").each( function () {
                check = 1;
            });
            return check;
        }

        $(".resend-notification-sms-first").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('admin_list_member_send_notification_sms_remind_completing_fee') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $(".resend-notification-email-first").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }

                window.location.href = '{{ route('admin_list_member_send_notification_email_remind_completing_fee') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $('body').on('change', '.checkAll', function() {
            $(".checkbox-pr").prop('checked',$(this).is(":checked"));
            if ($(this).is(":checked") == true) {
                $(".checkbox-pr").attr('data-check', 'yes');
            }
            else {
                $(".checkbox-pr").attr('data-check', 'no');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;

            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                    anyChecked = !!$(this).is(':checked');
                });
                if (anyChecked == true) {
                    __this.attr("data-check", "yes");

                    if (allRowLength == allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");

                __this.attr('data-check', 'no');
            }
        });
    </script>
@endsection
