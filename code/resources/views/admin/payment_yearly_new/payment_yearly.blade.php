<?php
use App\Constants\MemberConstants;
setlocale(LC_MONETARY,"en_US");
?>

@extends('admin.layouts.app')
@section('title', 'Nộp hội phí thường niên')

@section('header_embed')
    <!-- CSFR token for ajax call -->
    <link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <style>
        a.button {
            padding-left: 20px;
            padding-right: 20px;
            margin-left: 5px;
            margin-right: 5px;
        }

        a.button > span.glyphicon {
            color: white;
        }

        .no-break {
            white-space: nowrap;
        }

        .modal-body .line {
            margin-bottom: 10px;
        }

        .dialog-content > span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }

        .dialog-content > span.error-msg {
            color: #a94442;
        }

        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            /*background-color: #337ab7;*/
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }

        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }

        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }
        .modal-title {
            float: left;
            font-size: 20px;
            font-weight: bold;
            line-height: 1.42857;
            margin: 0;
            width: 90%;
        }
        .font-all-popup-title{
            font-weight: bold;
        }

        body .choices.checkbox-list .choice.chosen label:after, body .choices.checkbox-list .choice label:after {
            margin-top: -8px
        }
        body .checkall {
            padding-left: 25px !important;
        }
        body .checkall .choice {
            padding-left: 35px;
            padding-right: 10px;
            cursor: pointer;
        }
        body .checkall .choice input {
            display: none !important;
        }
        body .checkall .choice label {
            position: relative;
            cursor: pointer;
            margin-bottom: 0;
        }
        body .checkall .choice label:before {
            content: "";
            width: 17px;
            height: 17px;
            margin-top: -9px;
            border: 1px solid #6C6B6B;
            position: absolute;
            left: -25px;
            top: 50%;
            border-radius: 2px;
        }
        body .checkall .choice label:after {
            margin-top: -9px;
            content: "\F00C";
            font-family: 'FontAwesome';
            font-size: 12px;
            color: #BBBABA;
            left: -22px;
            position: absolute;
            top: 50%;
            opacity: 0;
            transition: 0.3s;
            -moz-transition: 0.3s;
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
            -ms-transition: 0.3s;
        }
        body .checkall .choice.chosen label:after {
            opacity: 1;
            color: #2B95CB;
        }
        body .checkall .choice:hover label:after {
            opacity: 1;
        }

        input[type=checkbox]
        {
            width: 18px;
            height: 18px;
            text-align: center; /* center checkbox horizontally */
            vertical-align: middle; /* center checkbox vertically */
        }

        body #content .table-wrap table tr td {
            padding-right: 0px;
        }
    </style>
@endsection

@section('content')
    <div class="page-wrap">

        <div class="main-content" style="padding-bottom: 75px;">
        @include('admin.payment_yearly_new.search_payment_yearly')
        @include('admin.layouts.message')

            <input id="exportHdd" type="hidden" value="{{ url('/officesys/payment_yearly/excel/export_excel') }}{{$query_string != '' ? '?'.$query_string : ''}}"/>
            {{-- <a class="resend-notification button btn-primary" style="float:  right;width: 200px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Gửi SMS Hội phí thường niên</a> --}}
            <a class="resend-notification-email button btn-primary" style="float:  right;width: 200px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Gửi Email Hội phí thường niên</a>
            <div class="total">Tổng số : {{$count_members}}</div>

            <div class="table-wrap" style="margin-top: 5px">
                <table>
                    <thead>
                    <tr>
                        <td width="5%">Số TT</td>
                        <td width="5%">Mã hồ sơ</td>
                        <td width="7%">Số thẻ HDV</td>
                        <td width="12%">Họ và tên</td>
                        <td width="7%">Số điện thoại</td>
                        <td width="7%">Số thẻ hội viên</td>
                        
                        <!-- <td width="10%">Thời gian có hiệu lực</td> -->
                        <td width="10%">Hạn thẻ hội viên</td>
                        <td width="15%">Chi hội</td>
                        <td width="10%">Trạng thái HV</td>
                        <td width="10%">Số chứng từ</td>
                        <td width="7%">Ngày chứng từ</td>
                        <td width="7%">Số tiền (VNĐ)</td>
                        <!-- <td width="6%">Trạng thái</td> -->
                        <td width="10%">Chức năng</td>
                        <td width="5%" style="">TB Nộp Phí<br>
                            <input type="checkbox" class="checkAll" name="member">
                            <label>&nbsp;</label>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($objMembers->count() == 0)
                        <tr>
                            <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            <tr>
                                <td class="text-center"> {{ array_get($response_view, 'status') != -1 ? $current_paginator + $key + 1 : $key + 1 }} </td>
                                <td class="text-center">
                                    {{ $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('admin_list_member_detail_view' , !empty($objMember->mid) ? $objMember->mid : (!empty($objMember->id) ? $objMember->id : '')) }}">{{ $objMember->touristGuideCode }} </a>
                                </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }}</td>
                                <td class="text-center">{{$objMember->firstMobile}}</td> 
                                <td class="text-center">
                                    {{ !is_null($objMember->member_code) ? $objMember->member_code : '' }}
                                </td>
                                
                                <!-- <td>{{ isset($objMember->year) ? $objMember->year : '' }}</td> -->
                                <!-- <td> {{ empty($objMember->date_payment) ? '' : date('d/m/Y', strtotime($objMember->date_payment)) .'-'. $objMember->member_code_expiration}} </td> -->
                                <td> {{ $objMember->member_code_expiration}} </td>
                                <td> {{ $objMember->province_name}} </td>
                                @if(empty($objMember->status))
                                    <td class="text-left wrap-text"></td>
                                @else
                                    <td class="text-left wrap-text">
                                        {{ array_get(MemberConstants::$file_const_display_status, $objMember->status, '') }}
                                    </td>
                                @endif
                                <td class="text-left wrap-text"> {{ $objMember->number_payment }} </td>
                                <td class="text-center"> {{ empty($objMember->date_payment) ? '' : date('d/m/Y', strtotime($objMember->date_payment)) }} </td>
                                <td class="text-right">
                                    @if (strpos($objMember->currency, '.') !== false || strpos($objMember->currency, ',') !== false)
                                        {{ $objMember->currency }}
                                    @else
                                        {{ number_format($objMember->currency, 0, ',', '.') }}
                                    @endif
                                </td>
                                <!-- <td>
                                    @if ($objMember->status === 5)
                                        Chờ ký quyết định
                                    @elseif ($objMember->status === 6)
                                        Chờ cấp mã hội viên
                                    @elseif ($objMember->status === 13)
                                        Hội viên chính thức
                                    @endif
                                </td> -->
                                <td align="center" class="text-center">
                                    @if (!empty($objMember->date_payment))
                                        <a class="btn-function show-modal"
                                           href="#"
                                           data-toggle="tooltip"
                                           data-placement="left"
                                           title="Xem chứng từ"
                                           data-id="{{ $objMember->mpuid }}"
                                           data-status="{{ $objMember->status }}">
                                            <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                        </a>

                                        <a class="btn-function edit-modal"
                                           href="#" data-toggle="tooltip"
                                           data-placement="left"
                                           title="Sửa chứng từ"
                                           data-id="{{ $objMember->mpuid }}"
                                           data-status="{{ $objMember->status }}"

                                           data-expiration="{{ date('d/m/Y', strtotime($objMember->member_code_expiration)) }}"
                                           data-approved="{{ empty($objMember->approved_at) ? '' :  date('d/m/Y', strtotime($objMember->approved_at)) }}">
                                        <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                        </a>
                                        <a href="{{ route('admin_member_profile_edit', ['id' => $objMember->mid]) }}"
    										class="update-profile-member-modal btn-function"
    										data-toggle="tooltip"
    										data-placement="left"
    										title="Cập nhật thông tin hồ sơ hội viên"
    										data-id="{{$objMember->mid}}"
    										data-status="{{$objMember->status}}"
    										data-membercode="{{$objMember->member_code}}">
    										<span class="glyphicon glyphicon-edit" style="color: #ff0000;"></span>
    									</a>
                                        @if (date('Y', strtotime($objMember->created_at)) == date('Y'))
											<a class="btn-function delete-modal"
											   href="#"
											   data-toggle="tooltip"
											   data-placement="left"
											   title="Xoá chứng từ"
											   data-id="{{ $objMember->mpuid }}"
											   data-status="{{ $objMember->status }}">
												<span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
											</a>
                                        @endif
                                    @endif
                                    <a class="btn-function add-modal"
                                       id="btnAddPayment"
                                       href="#"
                                       data-toggle="tooltip"
                                       data-placement="left"
                                       title="Nộp hội phí thường niên"
                                       data-id="{{ !empty($objMember->mid) ? $objMember->mid . '?mpuid=null' : '' }}"
                                       data-status="{{ $objMember->status }}"
                                       data-approved="{{ empty($objMember->approved_at) ? '' :  date('d/m/Y', strtotime($objMember->approved_at)) }}">
                                        <span class="glyphicon glyphicon-plus" style="font-size: 16px; color: #0000ff; "></span>
                                    </a>
                                </td>
								<td class="text-center">
									<span class="checkitem">
										<input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{ $objMember->mid }}" class="checkbox-pr">
										<label for="member {{ $key + 1 }}">&nbsp;</label>
									</span>
								</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            @include('admin.layouts.pagination')
            <div style="margin-top: 17px; float: left;">
            @if (array_get($response_view, 'status') == 0)
                <a class="button btn-primary" style="float:  right;width: 150px; height: 37px; line-height: 24px; margin-bottom: 5px; padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)" onclick="createExcel('2');" data-toggle="tooltip" title="Xuất danh sách hội viên chưa nộp hội phí thường niên" >Xuất excel</a>
            @elseif (array_get($response_view, 'status') == 1)
                <a class="button btn-primary" style="float:  right;width: 150px; height: 37px; line-height: 24px; margin-bottom: 5px; padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)" onclick="createExcel('3');" data-toggle="tooltip" title="Xuất danh sách hội viên đã nộp hội phí thường niên">Xuất excel</a>
            @endif
            </div>
        </div>
    </div>
    @if (session('error_message'))
        <div type="hidden" id="error_message">
            {{ session('error_message') }}
        </div>
    @endif

    <input type="hidden" id="member-id"/>
    <input type="hidden" id="current-status"/>
    <input type="hidden" id="approved_at"/>
    <input type="hidden" id="verified_at"/>

    @include('admin.payment_yearly.detail')
    @include('admin.payment_yearly.create')
    @include('admin.payment_yearly.edit')
    @include('admin.payment_yearly.delete')
    @include('admin.payment_yearly.import_modal')
    @include('admin.payment_yearly.export_modal')
@endsection

@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/display-currency.js') }}"></script>

    <script>
        function createExcel(flg){
            //flg = 1 Hội viên chưa nộp hội phí lần đầu
            //flg = 2 Hội viên chưa nộp hội phí thường niên
            //flg = 3 Hội viên đã nộp hội phí thường niên
            window.location = $("#exportHdd").val()+'&type='+flg;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#btnsubmit").click();
                }
            }
        });
        function clearErrorFields() {
            $('.errorNumberPayment').addClass('hidden');
            $('.errorDatePayment').addClass('hidden');
            $('.errorCurrency').addClass('hidden');
            $('.errorContent').addClass('hidden');
        }

        var url = "{{ url('/officesys/payment_yearly') }}"
        function htmlEntities(str) {
            return str;
        }

        var listFeePolicy = [];
        function showPopup(memberId, postfix) {
            listFeePolicy = [];
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';

            switch (postfix) {
                case '_add':
                    title = 'Thêm mới thông tin hội phí';
                    canEdit = true;
                    var numberOfYear = $('.select2-selection--multiple');
                    $('.position_numberofyear').removeClass('error_border');
                    $('.errorNumberOfYear').addClass('hidden');
                    numberOfYear.removeClass('error_border');
                    $('#content' + postfix).val('');
                    $('#note' + postfix).val('');
                    $("#transform_currency" + postfix).text('');
                    break;

                case '_edit':
                    title = 'Cập nhật thông tin hội phí';
                    canEdit = true;
                    var numberOfYear = $('.select2-selection--multiple');
                    $('.position_numberofyear').removeClass('error_border');
                    $('.errorNumberOfYear').addClass('hidden');
                    numberOfYear.removeClass('error_border');
                    break;

                case '_delete':
                    title = 'Bạn có chắc chắn muốn xóa thông tin hội phí không?';
                    break;
                default:
                    title = 'Chi tiết thông tin hội phí';
            }

            $('.position-number-payment').removeClass('error_border');
            $('.position-date').removeClass('error_border');
            $('.position-currency').removeClass('error_border');
            $('.position-content').removeClass('error_border');

            $('.modal-title').text(title);
            if (canEdit === true) {
                clearErrorFields();
            }

            $(modalId).modal('show');
            $(modalId + ' .modal-dialog').loader('show');

            $.ajax({
                url: url + "/" + memberId,
                type: 'GET',
                success: function (response) {
                    $(modalId + ' .modal-dialog').loader('hide');

                    var objMember = response.objMember;
                    var objMemberPayment = response.objMemberPayment;
                    var objMemberPaymentEdit = response.objMemberPaymentEdit;
                    $('#code' + postfix).text(objMember.touristGuideCode);
                    $('#name' + postfix).text(objMember.fullName);
                    $('#province_name' + postfix).text(objMember.provinceName);
                    $('#birthday' + postfix).text(objMember.formattedBirthday);
                    $("#verified_at").val(toTimestamp(objMember.verified_at));
                    $("#member_code" + postfix).text(objMember.member_code);
                    $("#identitycard" + postfix).text(objMember.cmtCccd);
                    $("#joining" + postfix).text(objMember.member_from);
                    $("#provincecode" + postfix).text(objMember.provinceName);
                    $("#last_year" + postfix).val(response.lastYear);
                    $("#member_code_expiration").text(objMember.member_code_expiration);
                    var table_payment = $('.table_payment tbody');
                    $('.table_payment tbody tr').remove();
                    if (objMemberPayment !== null) {
                        objMemberPayment.forEach(function (payment, index) {
                            var incre = index + 1;
                            var paymentNote = payment.note != null ? payment.note : '';
                            var paymentContent = payment.payment_content != null ? payment.payment_content : '';
                            table_payment.append(
                                "<tr>" +
                                "<td>" + incre + "</td>" +
                                "<td>" + payment.number_payment + "</td>" +
                                "<td>" + payment.date_payment + "</td>" +
                                "<td>" + payment.currency + "</td>" +
                                "<td>" + payment.member_code_expiration.substr(0,10).replaceAll('-', '/') + "</td>" +
                                "<td>" + paymentContent + "</td>" +
                                "<td>" + paymentNote + "</td>" +
                                "</tr>");
                        });
                    }
                    var numberPayment = '';
                    var datePayment = '';
                    var currency = '';
                    var content = '';
                    var year = '';
                    var note = '';
                    if (objMemberPaymentEdit) {
                        numberPayment = objMemberPaymentEdit.number_payment;
                        datePayment = objMemberPaymentEdit.date_payment;
                        //a chua hieu sao lai nhu nay nen tam comment lai: currency = canEdit === true ? objMemberPayment.currency : objMemberPayment.formattedCurrency;
                        currency = htmlEntities(objMemberPaymentEdit.currency);
                        content = htmlEntities(objMemberPaymentEdit.payment_content);
                        note = htmlEntities(objMemberPaymentEdit.note);
                        year = htmlEntities(objMemberPaymentEdit.year);
                    }
                    if (canEdit === true) {
                        $('#number_payment' + postfix).val(numberPayment);
                        $('#date_payment' + postfix).val(datePayment);
                        $('#currency' + postfix).val(currency).change();
                        displayTxt($("#transform_currency_edit"), currency);
                        if (datePayment != "") {
                          var arrDt = datePayment.split('/');
                          var newDt = arrDt[2] + '-' + arrDt[1] + '-' + arrDt[0];
                          var date = new Date(newDt);
                          $('#expiration' + postfix).val(new Date(date.setFullYear(date.getFullYear() + 1)).toLocaleDateString('en-GB'));
                        }
                    } else {
                        $('#number_payment' + postfix).text(numberPayment);
                        $('#date_payment' + postfix).text(datePayment);
                        $('#currency' + postfix).text(currency);
                        $('#year' + postfix).text(year);
                    }
                    //focus number payment
                    if (postfix == '_add' || postfix == '_edit') {
                        var number_payment = $("#number_payment" + postfix);
                        setTimeout(function () {
                            number_payment.focus();
                        }, 500);
                    }

                    $('#content' + postfix).text(content);
                    $('#note' + postfix).text(note);

                    //add fee policy
                    if(response.objFeePolicy) {
                        response.objFeePolicy[0].forEach(e=>{
                            if(e.typeOp == 1) {
                                // if(e.perfect_fee_policy_code == "fee-policy-001") {
                                //     //listFeePolicy.push(e);
                                // } else if(e.perfect_fee_policy_code == "fee-policy-002") {
                                //     let approved_at = new Date(objMember.approved_at);
                                //     approved_at.setFullYear(approved_at.getFullYear() + 3);
                                //     if(approved_at.getTime() <= new Date().getDate()) {
                                //         listFeePolicy.push(e);
                                //     }
                                // } else if(e.perfect_fee_policy_code == "fee-policy-003") {
                                //     let approved_at = new Date(objMember.approved_at);
                                //     approved_at.setFullYear(approved_at.getFullYear() + 5);
                                //     if(approved_at.getTime() <= new Date().getDate()) {
                                //         listFeePolicy.push(e);
                                //     }
                                // } else if(e.perfect_fee_policy_code == "fee-policy-006") {
                                //     listFeePolicy.push(e);
                                // }
                                listFeePolicy.push(e);
                            } else {
                                let listProvince = e.list_id.split(",");
                                if(listProvince.includes(objMember.province_code.toString())) {
                                    listFeePolicy.push(e);
                                }
                            }
                        })

                        if(listFeePolicy.length > 0) {
                            $(".fee_policy").show();
                            let maxMoney = listFeePolicy[0];
                            $(".fee_policy_select").html("");
                            listFeePolicy.forEach(e=>{
                                $(".fee_policy_select").append(new Option(e.name,e.code));
                                if(maxMoney.money < e.money) {
                                    maxMoney = e;
                                }
                            })
                            $("#currency_add").val(maxMoney.money);

                            // $(".fee_policy_select").val(maxMoney.code);
                            // $(".fee_policy .dialog-content .money").html(maxMoney.money);
                        }
                    }
                }
            });

        }

        function changeFeePolicy(e) {
            listFeePolicy.forEach(item=>{
                if(e.value == item.code) {
                    // $(".fee_policy .dialog-content .money").html(item.money);
                    $("#currency_add").val(item.money);

                    getMoney();
                }
            })
        }

        $(document).on('click', '.show-modal', function (event) {
            event.preventDefault();
            var memberId = $(this).data('id');
            showPopup(memberId, '_show');
        });

        function updateExpirationDate(ele, postfix = '_add') {
          var dt = $(ele).val();
          var exp_val = $('#expiration' + postfix).val();
          if (dt != "" && exp_val == "") {
            var arrDt = dt.split('/');
            var newDt = arrDt[2] + '-' + arrDt[1] + '-' + arrDt[0];
            var date = new Date(newDt);
            $('#expiration' + postfix).val(new Date(date.setFullYear(date.getFullYear() + 1)).toLocaleDateString('en-GB'));
          }
        }

        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            var memberId = $(this).data('id');
            $('input[type=hidden]#member-id').val(memberId);
            $("#current-status").val($(this).data("status"));
            $("#approved_at").val($(this).data("approved"));
            $("#expiration_add").val(new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toLocaleDateString('en-GB'));
            showPopup(memberId, '_add');
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            var memberId = $(this).data('id');
            $('input[type=hidden]#member-id').val(memberId);
            $("#current-status").val($(this).data("status"));
            $("#approved_at").val($(this).data("approved"));
            showPopup(memberId, '_edit');
        });

        $(document).on('click', '.delete-modal', function (event) {
            event.preventDefault();
            var memberId = $(this).data('id');
            $('input[type=hidden]#member-id').val(memberId);
            $("#current-status").val($(this).data("status"));
            showPopup(memberId, '_delete');
        });

        function doAction(memberId, postfix) {
            var action = 'POST';
            var modalId = '#' + postfix + 'Modal';
            var data = {
                'number_payment': $('#number_payment' + postfix).val(),
                'currency': $(".fee_policy_end .money").html() ? $(".fee_policy_end .money").html() : $('#currency' + postfix).val(),
                'date_payment': $('#date_payment' + postfix).val(),
                'content': $('#content' + postfix).val(),
                'note': $('#note' + postfix).val(),
                'status': $("#current-status").val(),
                'date_expiration': $('#expiration' + postfix).val(),
            };
            //validation  Ngày chứng từ
            var date = new Date();
            var today = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            var date_payment = $("#date_payment" + postfix);
            var number_payment = $('#number_payment' + postfix);
            var currency = $('#currency' + postfix);
            var content = $('#content' + postfix);
            var note = $('#note' + postfix);
            var apprved_date = parseDate($('#approved_at').val()).getTime()/1000;


            if (postfix == '_add' || postfix == '_edit') {
                $('.position-number-payment').removeClass('error_border');
                $('.errorNumberPayment').addClass('hidden');
                $('.position-date').removeClass('error_border');
                $('.errorDatePayment').addClass('hidden');
                $('.position-currency').removeClass('error_border');
                $('.errorCurrency').addClass('hidden');
                $('.position-content').removeClass('error_border');
                $('.errorContent').addClass('hidden');
            }

            var flg = 0;
            if (postfix == '_add' || postfix == '_edit') {
                if (date_payment.val() == '') {
                    $(".errorDatePayment").html('Ngày chứng từ không được bỏ trống');
                    $(".errorDatePayment").removeClass('hidden');
                    date_payment.addClass('error_border');
                    flg = 1;
                } else {
                    today = parseDate(today).getTime();
                    var verifiedDate = $('#verified_at').val();
                    var paymentDate = parseDate(date_payment.val()).getTime();
                    if (today < paymentDate) {
                        setTimeout(function () {
                            $(".errorDatePayment").html('Ngày chứng từ không được lớn hơn ngày hiện tại');
                            $(".errorDatePayment").removeClass('hidden');
                            date_payment.addClass('error_border');
                        }, 500);

                        flg = 1;
                    } else if(paymentDate < apprved_date){
                        $(".errorDatePayment").html('Ngày chứng từ không được nhỏ hơn ngày phê duyệt');
                        $(".errorDatePayment").removeClass('hidden');
                        date_payment.addClass('error_border');
                        flg = 1;
                    } else {
                        $(".errorDatePayment").addClass('hidden');
                        $(".errorDatePayment").html('');
                        date_payment.removeClass('error_border');
                    }
                }

                //validation so chung tu

                if (number_payment.val() == '') {
                    $(".errorNumberPayment").html('Số chứng từ không được bỏ trống');
                    $(".errorNumberPayment").removeClass('hidden');
                    number_payment.addClass('error_border');
                    flg = 1;
                } else {
                    $(".errorNumberPayment").html('');
                    $(".errorNumberPayment").addClass('hidden');
                    number_payment.removeClass('error_border');

                }
                //validation so tien
                if (currency.val() == '') {
                    $(".errorCurrency").html('Số tiền không được bỏ trống');
                    $(".errorCurrency").removeClass('hidden');
                    currency.addClass('error_border');
                    flg = 1;
                } else {
                    var regexSt = /^([0-9])+$/;
                    if (!regexSt.test(currency.val())) {
                        $(".errorCurrency").html('Số tiền phải là số nguyên dương > 0');
                        $(".errorCurrency").removeClass('hidden');
                        currency.addClass('error_border');
                        flg = 1;
                    } else if(currency.val() < 0) {
                        $(".errorCurrency").html('Số tiền phải là số nguyên dương > 0');
                        $(".errorCurrency").removeClass('hidden');
                        currency.addClass('error_border');
                        flg = 1;
                    } else {
                        $(".errorCurrency").html('');
                        $(".errorCurrency").addClass('hidden');
                        currency.removeClass('error_border');
                    }
                }
            }

            if (flg == 1) {
                setTimeout(function () {
                    $(modalId).modal('show');
                }, 500);
                return false;
            }
			$('#btnsubmit').text("Đang xử lý ...").attr('disabled', 'disabled');

            switch (postfix) {
                case '_edit':
                    action = 'POST';
					url = url + "/update";
                    break;

                case '_delete':
                    action = 'POST';
					url = url + "/destroy";
                    data = '';
                    break;
                default:
                    action = 'POST';
            }

            $.ajax({
                type: action,
                url: url + '/' + memberId,
                data: data,
                success: function (data) {
                    clearErrorFields();
                    if (data.errors) {
                        var errorsPayment = data.errors;
                        if (errorsPayment.memberInvalid) {
                            toastr.warning(errorsPayment.memberInvalid, 'Thông báo', {timeOut: 2000});
                            $(modalId).modal('hide');
                            document.location.reload();
                        }
                        if (errorsPayment.uniqueNumberPayment) {
                            $(modalId).modal('hide');
                            alert(errorsPayment.uniqueNumberPayment);
                            //document.location.reload();
                        }
                        setTimeout(function () {
                            $(modalId).modal('show');
                        }, 500);
                        if (errorsPayment.invalid) {
                            toastr.warning(errorsPayment.number_payment, '', {timeOut: 2000});
                        }
                        if (errorsPayment.number_payment) {
                            $('.position-number-payment').addClass('error_border');
                            $('.errorNumberPayment').removeClass('hidden');
                            $('.errorNumberPayment').text(errorsPayment.number_payment);
                        }
                        if (errorsPayment.date_payment) {
                            $('.position-date').addClass('error_border');
                            $('.errorDatePayment').removeClass('hidden');
                            $('.errorDatePayment').text(errorsPayment.date_payment);
                        }
                        if (errorsPayment.currency) {
                            $('.position-currency').addClass('error_border');
                            $('.errorCurrency').removeClass('hidden');
                            $('.errorCurrency').text(errorsPayment.currency);
                        }
                        if (errorsPayment.content) {
                            $('.position-content').addClass('error_border');
                            $('.errorContent').removeClass('hidden');
                            $('.errorContent').text(errorsPayment.content);
                        }
                    } else {
                        toastr.success(data.success, 'Thông báo', {timeOut: 2000});
                        $(modalId).modal('hide');
                        document.location.reload(true);
                    }
                }
            });
        }

        $('.modal-footer').on('click', '.add', function () {
            var memberId = $('input[type=hidden]#member-id').val();
            doAction(memberId, '_add');
        });

        $('.modal-footer').on('click', '.edit', function () {
            var memberId = $('input[type=hidden]#member-id').val();
            doAction(memberId, '_edit');
        });

        $('.modal-footer').on('click', '.delete', function () {
            var memberId = $('input[type=hidden]#member-id').val();
            doAction(memberId, '_delete');
        });

        function sendNotification(memberIds) {
            console.log(memberIds);
            resetCheckboxes();

            $.ajax({
                type: 'POST',
                url: url + '/notifications/send',
                data: {
                    'member-ids': memberIds
                },
                success: function (data) {
                    if (data.errors) {
                        toastr.warning('Có lỗi hệ thống xảy ra. Vui lòng thử lại trong ít phút nữa', '', {timeOut: 2000});
                        console.log(data.errors);
                    } else {
                        toastr.success(data.success, 'Hệ thống đã gửi thông báo thành công', {timeOut: 1000});
                    }
                }
            });
        }

        function checkedIds() {
            return $("input[id='chk-inform']:checked").map(function () {
                return $(this).data('id');
            }).get();
        }

        function resetCheckboxes() {
            $("input[id='chk-inform']:checked").each(function () {
                $(this).prop('checked', false);
            });
        }

        $(document).on('click', '#send-notification', function (event) {
            event.preventDefault();
            isCheck = true;
            var checkIds = checkedIds();
            if (checkIds.length > 0) {
                toastr.clear()
                $("#send-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function () {
                    $("#send-notification").css('pointer-events', 'auto');
                }
                toastr.warning('Đang tiến hành gửi thông báo. Vui lòng đợi trong giây lát.', '', {timeOut: 2000});
                sendNotification(checkIds);
            } else {
                toastr.clear()
                $("#send-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function () {
                    $("#send-notification").css('pointer-events', 'auto');
                }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $(document).on('click', '.import_view-modal', function () {
            $('#importViewModal').modal('show');
        });

        var error_message = $("#error_message").text();
        if (error_message) {
            toastr.clear()
            $("#send-notification").css('pointer-events', 'none');
            toastr.options.onHidden = function () {
                $("#send-notification").css('pointer-events', 'auto');
            }
            toastr.error("File import Lệ phí, hội phí không hợp lệ", 'Lỗi', {timeOut: 2000});
        }

        $(document).ready(function () {
            document.getElementById("file_code").focus();
        });
        function toTimestamp(strDate){
            var datum = Date.parse(strDate);
            return datum/1000;
        }

        $("#import_excel").on('click', '.import_excel_submit', function () {
            validateFileUpload();
        });

        function validateFileUpload() {
            var file;
            var FileSize;
            var extension;

            if ($('#fileToUpload').val() != '') {
                file      = $('#fileToUpload')[0].files[0];
                extension = file.name.substr((file.name.lastIndexOf('.') +1));
                FileSize  = file.size / 1024 / 1024; // in MB
            } else {
                FileSize = 0;
                extension = '';
            }

            if (FileSize == 0) {
                alert('Không có file!');
            } else if (FileSize > 10){
                alert('File quá lớn, dung lượng file cho phép tối đa là 10Mb!');
            } else if (extension != 'xlsx' && extension != '') {
                alert('Bạn chỉ được phép upload file excel xlsx, vui lòng thực hiện lại');
            } else {
                $('#import_excel').get(0).submit();
            }
        }

        //print
        function checkedNames() {
            check = 0;
            $("input[name=member]:checked").each( function () {
                check = 1;
            });
            return check;
        }

        $(".resend-notification").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('payment_yearly.resend_notification') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $(".resend-notification-email").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('payment_yearly.resend_notification_email') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $('body').on('change', '.checkAll', function() {
            $(".checkbox-pr").prop('checked',$(this).is(":checked"));
            if ($(this).is(":checked") == true) {
                $(".checkbox-pr").attr('data-check', 'yes');
            }
            else {
                $(".checkbox-pr").attr('data-check', 'no');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;

            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                    if ($(this).is(':checked')){
                        anyChecked = true;
                    } else {
                        anyChecked = false;
                    }
                });
                if (anyChecked == true) {
                    __this.attr("data-check", "yes");

                    if (allRowLength == allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");

                __this.attr('data-check', 'no');
            }
        });
    </script>
@endsection
