@extends('admin.layouts.app')
@section('title', 'Cập nhật Mật Khẩu Người Dùng')
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .has-error .control-label {
        color: #3D3A3A !important
    }
</style>
@section('content')
    <div class="page-wrap">
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel panel-heading">Thay đổi mật khẩu</div>
            <div class="panel-body">
                @if(session('successes'))
                    <div id="closeButton" class="alert alert-success">
                        <button type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach (session('successes') as $success)
                                <li>{{ $success }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal reset-form" method="POST" action="{{ route('admin_user_change_password') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Mật khẩu cũ <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="old_password" type="password" class="form-control" name="old_password">

                            @if ($errors->has('old_password'))
                                <span class="help-block"> {{ $errors->first('old_password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Mật khẩu mới <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block"> {{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-4 control-label">Xác nhận mật
                            khẩu mới <span class="color-required">*</span></label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Đổi mật khẩu
                            </button>
                            <a href="{{URL::route('admin_user_profile')}}" class="btn btn-warning" style="color: #FFFFFF;">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
