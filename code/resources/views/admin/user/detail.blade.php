@extends('admin.layouts.app')
@section('title', 'Chỉnh sửa thông tin Người Dùng' . $objUser->fullname)
@section('content')
    <link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('admin/select2/js/i18n/vi.js')}}"></script>
    <style>
        .color-required{
            color: red
        }
        .error_border {
            border-color: #a94442 !important;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        }
        .help-block strong {
            color: red
        }
        .has-error .control-label {
            color: #3D3A3A !important
        }
        .select2-search__field{
            width:100% !important;
        }
    </style>
    <div class="page-wrap">
        <style>
            #closeButton {margin-top: 20px}
            .select2-search__field{
                width:100% !important;
            }
        </style>
        @include('admin.layouts.message')
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Thông tin chi tiết người dùng: {{ $objUser->fullname }}</div>

            <div class="panel-body">
                <form id="userForm" class="form-horizontal" method="post" action="{{ route('users.update', [$objUser->id]) }}">
                    {{ csrf_field() }}
                    {{--{{ method_field('PUT') }}--}}

                    <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                        <label for="fullname" class="col-md-4 control-label">Họ và tên <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="fullname" type="text" class="form-control" name="fullname" value="{{ empty(old('fullname')) ? $objUser->fullname : old('fullname') }}" autofocus>

                            @if ($errors->has('fullname'))
                                <span class="help-block">
                                <strong>{{ $errors->first('fullname') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 control-label">Tên đăng nhập <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="username" type="text" class="form-control" name="username" value="{{ empty(old('username')) ? $objUser->username : old('username') }}" required autofocus>

                            @if ($errors->has('username'))
                                <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Email <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ empty(old('email')) ? $objUser->email : old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Mật khẩu </label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Xác nhận mật khẩu</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status" class="col-md-4 control-label">Trạng thái người dùng <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <select id="status" class="form-control" name="status" required>
                                @foreach(\App\Constants\UserConstants::$arrUserStatus as $key => $userStatus)
                                    <option value="{{ $key }}" {{ ((old('status') == $key && old('status') != '') || $objUser->status == $key) ? 'selected' : '' }}> {{ $userStatus }} </option>
                                @endforeach
                            </select>
                            @if ($errors->has('status'))
                                <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('province_type') ? ' has-error' : '' }}">
                        <label for="province_type" class="col-md-4 control-label">Khu vực</label>

                        <div class="col-md-6" style="display: flex;">
                            @php $newArea = ['66' => 'Hà Nội', '67' => 'Đà Nẵng', '68' => 'Hồ Chí Minh']; @endphp
                            <div class="radio" style="margin-right: 25px;">
                                <label><input name="province_type" value="all" onclick="filterBranch(this)" type="radio" {{ (!old('province_type') || old('province_type') === 'all') ? 'checked' : '' }}>Toàn Quốc</label>
                            </div>
                            @foreach($branchesVpdd as $key => $val)
                                <div class="radio" style="margin-right: 25px;">
                                    <label><input name="province_type" onclick="filterBranch(this)" type="radio" value="{{ $key }}" {{ (old('province_type') === $key || $objUser->province_type == $key) ? 'checked' : '' }}>{{ $newArea[$key] ?? '' }}</label>
                                </div>
                            @endforeach
                            <input type="hidden" id="filter-by-status" value="-1">

                            @if ($errors->has('province_type'))
                                <span class="help-block">
                            <strong>{{ $errors->first('province_type') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="group-branches">
                        <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-4 text-right">Chi Hội</label>
                            <div class="col-md-6">
                                <select id="branch_id" class="form-control" name="branch_id">
                                    <option value="">Chọn chi hội</option>
                                    @if (!empty($listBranches))
                                        @foreach($listBranches as $key => $branch)
                                            <option value="{{ $key }}" {{ $key == $oldBranchId ? 'selected' : '' }}> {{ $branch }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('branch_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('club_id') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-4 text-right">CLB Thuộc Hội</label>
                            <div class="col-md-6">
                                <select id="club_id" class="form-control" name="club_id">
                                    <option value="">Chọn CLB</option>
                                    @if (!empty($listClub))
                                        @foreach($listClub as $key => $club)
                                            <option value="{{ $key }}" {{ $key == $oldClubId ? 'selected' : '' }}> {{ $club }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('club_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('club_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                        <label for="role" class="col-md-4 control-label">Vai trò <span class="color-required">*</span></label>
                        <div class="col-md-6">
                            <select id="role" class="form-control" name="role" >
                                <option value="">Chọn vai trò</option>
                                @foreach($groups as $key => $userRole)
                                    <option value="{{ $key }}" {{ $oldRole == $key ? 'selected' : '' }}> {{ $userRole }} </option>
                                @endforeach
                            </select>
                            @if ($errors->has('role'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            @if (auth()->user()->canSee(['users.update']))
                                <button class="btn btn-primary" type="submit">
                                    Lưu
                                </button>
                            @endif
                            <a href="{{ url('/officesys/users') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="8">Thoát</a>
                                {{--@if (auth()->user()->canSee(['users.destroy']))
                                <button class="btn btn-danger" onclick="delete_user()">
                                    Xóa
                                </button>
                            @endif--}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer_embed')
    <script>
        function delete_user() {
            if(confirm("Bạn có thực sự muốn xóa user này")) {
                // $('input[name="_method"]').val('DELETE')
                $('#userForm').attr('action', '{{ route('users.destroy', [$objUser->id]) }}');
                document.forms[0].submit();
            } else {
                $("form").submit(function(e){
                    e.preventDefault();
                });
            }
        }

        function showPermission(__this) {
            if (__this.is(':checked')) {
                $('.group-permission').slideDown();
            } else {
                $('.group-permission').slideUp();
            }
        }

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            $('#branch_id').select2({
                placeholder: "Chọn Chi Hội",
                width: '100%',
            });
            $('#club_id').select2({
                width: '100%',
                placeholder: 'Chọn CLB'
            });

            var parent_id = $('input[name="province_type"]:checked').val();
            if (parent_id == 'all') {
                $("#branch_id option").remove();
            } else {
                var province_code_selected = $("#branch_id option:selected").val();
                changeProvinceType(parent_id, province_code_selected);
            }
        });

        function filterBranch(ele) {
          var val = $(ele).val();
          var sta = jQuery('#filter-by-status').val();
          if (val == 'all') {
            $("#branch_id option").remove();
            return;
          }
          changeProvinceType(val, sta);
        }

        function changeProvinceType(parent_id, province_code_selected = '', status) {
          var province_type_array = [];

          $.ajax({
            type: "POST",
            url: '/searchactivityregistrationplace',
            data: { parent_id: parent_id, status: status, province_type: province_type_array, _token: '{{csrf_token()}}' },
            success: function (data) {
              $("#branch_id option").remove();
              var option = "";
              option += "<option value='' selected></option>";
              $.each(data.value, function (i, value) {
                if (value.id == province_code_selected) {
                  option += "<option value="+ value.id +" data-parent="+ value.parent_id +" Selected>"+ value.name +"</option>";
                }
                else {
                  option += "<option value="+ value.id +" data-parent="+ value.parent_id +">"+ value.name +"</option>";
                }
              })
              $("#branch_id").append(option);
            },
            error: function (data) {
              console.log(data);
            },
          });
        }
    </script>
@endsection


