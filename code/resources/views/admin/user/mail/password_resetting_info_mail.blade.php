<p>Xin chào ông/bà  {{ htmlentities($objUser->fullname) }},
<p>Mật khẩu mới của ông bà là <b>{{ $objUser->resetPassword }}</b></p>
<p>Xin vui lòng truy cập đường link sau:  <a href="{{ url('/login') }}">Đăng nhập</a>
<p><b>Mọi thông tin chi tiết xin liên hệ:</b></p>
<p>Hội Hướng dẫn viên Du lịch Việt Nam ĐT: 024.37835120, 024.37835122</p>
