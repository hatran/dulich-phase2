@extends('admin.layouts.app')
@section('title', 'Thông tin Người Dùng')
@section('content')
    <style>
        .list-group{
            width: 100%;
            box-shadow: none;
        }
        .list-group .list-group-item {
            border: none;
        }
    </style>
<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
    <div class="block-heading">Thông tin người dùng</div>
    <div class="block-content">
        <ul class="list-group">
            <li class="list-group-item">
                <span>Tên người dùng:</span> {{$user->fullname}}
            </li>
            <li class="list-group-item">
                <span>Tên đăng nhập:</span> {{$user->username}}
            </li>
            <li class="list-group-item">
                <span>Email:</span> {{$user->email}}
            </li>
            <li class="list-group-item">
                <span>Quyền truy cập:</span> {{ mb_convert_case(array_get($user->group, 'name', ''), MB_CASE_TITLE, "UTF-8") }}
            </li>
            @php
                $branches = new \App\Repositories\Branches\BranchesRepository(new App\Models\Branches());
                $branchesVpdd = $branches->getVPDD();

                $branchesRepo = new \App\Repositories\ClubOfBranch\ClubOfBranchRepository(new App\Models\Branches());
                $listBranches = $branchesRepo->getListBranch();
                $areaName = '';
                $areaTitle = '';
                if ($user->branch_id) {
                    $areaName = $listBranches[$user->branch_id] ?? '';
                    if (! empty($areaName)) {
                        $areaName = 'Chi hội '.$areaName;
                        $areaName = str_replace(array('Du Lịch', 'du lịch'), '', $areaName);
                    }
                    $areaTitle = 'Chi hội:';
                } else if ($user->province_type) {
                    $areaName = $branchesVpdd[$user->province_type] ?? '';
                    if (\Illuminate\Support\Str::contains($areaName, 'Đà Nẵng')) {
                        $areaName = 'Đà Nẵng';
                    } else if (\Illuminate\Support\Str::contains($areaName, 'Hồ Chí Minh')) {
                        $areaName = 'Hồ Chí Minh';
                    } else if (\Illuminate\Support\Str::contains($areaName, 'Việt Nam')) {
                        $areaName = 'Hà Nội';
                    }
                    $areaTitle = 'Khu vực:';
                } else {
                    $areaName = 'Toàn Quốc';
                    $areaTitle = 'Khu vực:';
                }
            @endphp
            <li class="list-group-item">
                <span>{{ $areaTitle }}</span> {{ $areaName }}
            </li>
        </ul>
    </div>
</div>
@endsection