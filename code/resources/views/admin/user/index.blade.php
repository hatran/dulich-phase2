@extends('admin.layouts.app')
@section('title', 'Danh sách Người Dùng')
<style>
    .total {line-height: 30px;padding: 6px 10px;color: #0000F0 !important;border-radius: 3px;font-size: 16px;border: 0;display: inline-block;}
    .btn-primary.btn-custom{
        color: #fff;
        line-height: 21px;
        height: 35px;
        line-height: 35px;
        border: 1px solid #BBBABA;
        padding: 0 10px;
        max-width: 100%;
        display: block;
        border-radius: 2px;
    }
</style>
@section('content')
    <div class="main-content">
    @include('admin.layouts.message')
    @include('admin.user.search')
    <!-- <div class="heading">Kết quả tìm kiếm</div> -->
        <div class="main-content">

            <div class="total">Tổng số: {{ $objUsers->total() }}</div>
            <div class="field-wrap" style="display:inline-block;float: right">
                <a tabindex="6" class="btn-custom button btn-primary" href="{{ url('/officesys/users/create') }}">Thêm mới</a>
            </div>
            <div class="field-wrap" style="display:inline-block;float: right; margin-right: 5px;">
                <a tabindex="6" class="btn-custom button btn-primary" href="{{ route('users_create_notmember') }}">Thêm mới (Không phải hội viên)</a>
            </div>
            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%">STT</td>
                        <td width="15%">Tên đăng nhập</td>
                        <td width="25%">Họ và tên</td>
                        <td width="25%">Email</td>

                        <td width="25%">Vai trò</td>
                        <td width="5%">Trạng thái</td>
                        <td width="15%">Khu vực</td>
                        <td width="15%">Quản lý Chi hội</td>
                        <td>Chức năng</td>
                    </tr>
                    </thead>
                    <tbody>
                        @if (count($objUsers) == 0)
                            <tr>
                                <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                            </tr>
                        @else
                            @foreach($objUsers as $key => $objUser)
                                @php
                                    $areaName = '';
                                    if (empty($objUser->province_type)) {
                                        $areaName = 'Toàn Quốc';
                                    } else {
                                        $area = array_get($branchesVpdd, $objUser->province_type, '');
                                        if (\Illuminate\Support\Str::contains($area, 'Đà Nẵng')) {
                                            $areaName = 'Đà Nẵng';
                                        } else if (\Illuminate\Support\Str::contains($area, 'Hồ Chí Minh')) {
                                            $areaName = 'Hồ Chí Minh';
                                        } else if (\Illuminate\Support\Str::contains($area, 'Việt Nam')) {
                                            $areaName = 'Hà Nội';
                                        }
                                    }
                                @endphp
                                <tr>
                                    <td class="text-center"> {{ $current_cursor + $key + 1 }} </td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;"><a href="{{ url('/officesys/users/' . $objUser->id) }}">{{ $objUser->username }} </a>
                                    </td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;"> {{ $objUser->fullname }} </td>
                                    <td class="text-left" style="white-space: normal; word-break: break-word;"> {{ $objUser->email }} </td>

                                    <td class="text-left" style="white-space: normal; word-break: break-word;"> {{ array_get($objUser, 'group.name', '') }} </td>
                                    <td class="text-left"> {{ !isset($objUser->status) ? '' : array_get(\App\Constants\UserConstants::$arrUserStatus, $objUser->status, '') }} </td>
                                    {{--<td class="text-left"> {{ !isset($objUser->province_type) ? '' : array_get(\App\Constants\BranchConstants::$arrProvinceType, $objUser->province_type, '') }} </td>--}}
                                    <td class="text-left"> {{ $areaName }} </td>
                                    <td class="text-left"> {{ !isset($objUser->branch_id) ? '' : array_get($branch_chihoi, $objUser->branch_id, '') }} </td>
                                    <td class="text-center">
                                        @if (auth()->user()->canSee(['users.edit']))
                                            <a title="Cập nhật người dùng" href="{{ url('/officesys/users/' . $objUser->id) }}">
                                                <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                            </a>
                                        @endif
                                        {{--@if (Auth::user()->role == \App\Constants\UserConstants::ADMIN_USER)--}}
                                        @if (auth()->user()->canSee(['users.destroy']))
                                            <a title="Xóa người dùng" href="javascript:void(0)" style="padding-left: 10px"
                                               onclick="delete_user({{ $objUser->id }})">
                                                <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                            </a>
                                        @endif
                                        <a title="Cập nhật mật khẩu" href="#" data-toggle="modal" data-target="#createnewModal" onclick="resetPassword({{ $objUser->id }})" style="padding-left: 10px">
                                            <span class="glyphicon glyphicon-refresh" style="font-size: 16px; color: blue;"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <form id="delete_user" action="" method="post">
                    {{ csrf_field() }}
                </form>
            </div>
        @include('admin.layouts.pagination')
        </div>
    </div>
    @include('admin.user.modal.reset')
@endsection

@section('footer_embed')
<script>
    function delete_user(id) {
        if (confirm("Bạn có thực sự muốn xóa user này")) {
            $('#delete_user').attr('action', '/officesys/users/' + id + '/delete');
            $('#delete_user').submit();
        } else {
            $("form").submit(function (e) {
                e.preventDefault();
            });
        }
    }
    $( document ).ready(function() {
        document.getElementById("fullname").focus();
    });
</script>
@endsection
