@extends('admin.layouts.app')
@section('title', 'Tạo mới Người Dùng')
@section('content')
    <div class="page-wrap">
    @include('admin.layouts.message')
		<link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
		<script src="{{asset('admin/select2/js/i18n/vi.js')}}"></script>
		<style>
			.color-required{
				color: red
			}
			.error_border {
				border-color: #a94442 !important;
				box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
			}
			.help-block strong {
				color: red
			}
			.has-error .control-label {
				color: #3D3A3A !important
			}
			.select2-search__field{
                width:100% !important;
            }
		</style>
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Tạo mới người dùng</div>

            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('users.store') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                        <label for="fullname" class="col-md-4 text-right">Họ và tên <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="fullname" type="text" class="form-control" name="fullname" value="{{ old('fullname') }}"  autofocus>

                            @if ($errors->has('fullname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fullname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-4 text-right">Tên đăng nhập <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}"  autofocus>

                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 text-right">Email <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >

                            @if ($errors->has('email'))
                                <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 text-right">Mật khẩu <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" >

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-4 text-right">Xác nhận mật khẩu <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
							@if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status" class="col-md-4 text-right">Trạng thái người dùng <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <select id="status" class="form-control" name="status" >
                                @foreach(\App\Constants\UserConstants::$arrUserStatus as $key => $userStatus)
                                    <option value="{{ $key }}" {{ (old('status') === $key) ? 'selected' : '' }}> {{ $userStatus }} </option>
                                @endforeach
                            </select>
                            @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('province_type') ? ' has-error' : '' }}">
                        <label for="province_type" class="col-md-4 control-label">Khu vực</label>

                        <div class="col-md-6" style="display: flex;">
                            @php $newArea = ['66' => 'Hà Nội', '67' => 'Đà Nẵng', '68' => 'Hồ Chí Minh']; @endphp
                            <div class="radio" style="margin-right: 25px;">
                                <label><input name="province_type" value="all" onclick="filterBranch(this)" type="radio" {{ (!old('province_type') || old('province_type') === 'all') ? 'checked' : '' }}>Toàn Quốc</label>
                            </div>
                            @foreach($branchesVpdd as $key => $val)
                                <div class="radio" style="margin-right: 25px;">
                                    <label><input name="province_type" onclick="filterBranch(this)" type="radio" value="{{ $key }}" {{ (old('province_type') === $key) ? 'checked' : '' }}>{{ $newArea[$key] ?? '' }}</label>
                                </div>
                            @endforeach
                            <input type="hidden" id="filter-by-status" value="-1">

                            @if ($errors->has('province_type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('province_type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="group-branches">
                        <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-4 text-right">Chi Hội</label>
                            <div class="col-md-6">
                                <select id="branch_id" class="form-control" name="branch_id">
                                    <option value="">Chọn chi hội</option>
                                    @if (!empty($listBranches))
                                        @foreach($listBranches as $key => $branches)
                                            <option value="{{ $key }}" {{ (!empty(old('branch_id')) && in_array($key, old('branch_id'))) ? 'selected' : '' }}> {{ $branches }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('branch_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('club_id') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-4 text-right">CLB Thuộc Hội</label>
                            <div class="col-md-6">
                                <select id="club_id" class="form-control" name="club_id">
                                    <option value="">Chọn CLB</option>
                                    @if (!empty($listClub))
                                        @foreach($listClub as $key => $branches)
                                            <option value="{{ $key }}" {{ (!empty(old('club_id')) && in_array($key, old('club_id'))) ? 'selected' : '' }}> {{ $branches }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('club_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('club_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                        <label for="role" class="col-md-4 text-right">Vai trò <span class="color-required">*</span></label>
                        <div class="col-md-6">
                            <select id="role" class="form-control" name="role" >
                                <option value="">Chọn vai trò</option>
                                @foreach($groups as $key => $userRole)
                                    <option value="{{ $key }}" {{ (old('role') == $key) ? 'selected' : '' }}> {{ $userRole }} </option>
                                @endforeach
                            </select>
                            @if ($errors->has('role'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Tạo mới
                            </button>
                            <a href="{{ URL::route('users.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="10">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('footer_embed')
<script>
	$(document).ready(function(){
		$('#branch_id').select2({
			placeholder: "Chọn Chi Hội",
			width: '100%'
		});
		$('#club_id').select2({
			width: '100%',
			placeholder: 'Chọn CLB'
		});

      var parent_id = $('input[name="province_type"]:checked').val();
      if (parent_id == 'all') {
        $("#branch_id option").remove();
      } else {
        var province_code_selected = $("#branch_id option:selected").val();
        changeProvinceType(parent_id, province_code_selected);
      }
	});

    function filterBranch(ele) {
      var val = $(ele).val();
      var sta = jQuery('#filter-by-status').val();
      if (val == 'all') {
        $("#branch_id option").remove();
        return;
      }
      changeProvinceType(val, sta);
    }

    function changeProvinceType(parent_id, province_code_selected = '', status) {
      var province_type_array = [];

      $.ajax({
        type: "POST",
        url: '/searchactivityregistrationplace',
        data: { parent_id: parent_id, status: status, province_type: province_type_array, _token: '{{csrf_token()}}' },
        success: function (data) {
          $("#branch_id option").remove();
          var option = "";
          option += "<option value='' selected></option>";
          $.each(data.value, function (i, value) {
            if (value.id == province_code_selected) {
              option += "<option value="+ value.id +" data-parent="+ value.parent_id +" Selected>"+ value.name +"</option>";
            }
            else {
              option += "<option value="+ value.id +" data-parent="+ value.parent_id +">"+ value.name +"</option>";
            }
          })
          $("#branch_id").append(option);
        },
        error: function (data) {
          console.log(data);
        },
      });
    }
</script>
@endsection
