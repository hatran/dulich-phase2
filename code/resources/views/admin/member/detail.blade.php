<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
use App\Constants\OtherConstants;
?>

@extends('admin.layouts.app')
@section('title', 'Chi tiết thành viên')
@section('content')
    <div class="page-wrap">
        @include('admin.layouts.steps')
        @include('admin.layouts.detail')
        @include('admin.layouts.history')
        <div class="block" style="margin-top:  10px; padding-bottom: 10px;">
            @if ( $objMember->status == MemberConstants::APPROVAL_WAITING)
                <div class="block-heading">Thông tin phê duyệt:</div>
            @endif
            @if ( $objMember->status == MemberConstants::VERIFICATION_WAITING || $objMember->status == MemberConstants::VERIFICATION_REJECTED)
                <div class="block-heading">Thông tin thẩm định:</div>
            @endif

            <form id="approve" name="approve" method="post">
                {{ csrf_field() }}
                <table style="border: none;">
                    <input type="hidden" id="type_of_action" name="type_of_action" value="{{ $typeOfAction }}">
                    <input type="hidden" id="status_old" name="status_old" value="{{ $objMember->status }}">
                    <input type="hidden" id="id" name="id" value="{{ $objMember->id }}">
                    <input id="rank_value" type="hidden" name="rank" value="">
                    <tr>
                        <td colspan="2">
                        @if ( $objMember->status == MemberConstants::VERIFICATION_WAITING || $objMember->status == MemberConstants::VERIFICATION_REJECTED)
                                <div class="radio-list choices field-wrap">
                                    <div class="choice {{ $objMember->status == MemberConstants::VERIFICATION_REJECTED ? 'chosen' : '' }}" style="margin-right: 80px;" data-msgerror="Từ chối hồ sơ">
                                        <label for="opt_reject">Từ chối</label>
                                        <label for="msg_reject" class="hidden">Bạn có chắc chắn từ chối hồ sơ này không?</label>
                                        <input id="opt_reject" type="radio" value="submit_verification_reject">
                                    </div>
                                    <div class="choice {{ $objMember->status == MemberConstants::UPDATE_INFO_1 ? 'chosen' : '' }}" style="margin-right: 80px;" data-msgerror="Yêu cầu bổ sung hồ sơ">
                                        <label for="opt_update">Yêu cầu bổ sung</label>
                                        <label for="msg_update" class="hidden">Bạn có chắc chắn yêu cầu bổ sung hồ sơ này không?</label>
                                        <input id="opt_update" type="radio" value="submit_verification_update">
                                    </div>
                                    <div class="choice {{ $objMember->status == MemberConstants::VERIFICATION_WAITING ? 'chosen' : '' }}">
                                        <label for="opt_success">Thẩm định</label>
                                        <label for="msg_success" class="hidden">Bạn có chắc chắn thẩm định hồ sơ này không?</label>
                                        <input id="opt_success" type="radio" value="submit_verification_success">
                                    </div>
                                </div>
                            @endif
                            @if ( $objMember->status == MemberConstants::APPROVAL_WAITING)
                                <div class="radio-list choices field-wrap">
                                    <div class="choice {{ $objMember->status == MemberConstants::APPROVAL_REJECTED ? 'chosen' : '' }}" style="margin-right: 80px;" data-msgerror="Từ chối hồ sơ">
                                        <label for="opt_reject">Từ chối</label>
                                        <label for="msg_reject" class="hidden">Bạn có chắc chắn từ chối hồ sơ này không?</label>
                                        <input id="opt_reject" type="radio" value="leader_submit_verification_reject">
                                    </div>
                                    <div class="choice {{ $objMember->status == MemberConstants::APPROVE_WAITING_UPDATE ? 'chosen' : '' }}" style="margin-right: 80px;" data-msgerror="Yêu cầu bổ sung hồ sơ">
                                        <label for="opt_update">Yêu cầu bổ sung</label>
                                        <label for="msg_update" class="hidden">Bạn có chắc chắn yêu cầu bổ sung hồ sơ này không?</label>
                                        <input id="opt_update" type="radio" value="leader_submit_verification_update">
                                    </div>
                                    <div class="choice {{ $objMember->status == MemberConstants::APPROVAL_WAITING ? 'chosen' : '' }}">
                                        <label for="opt_success">Phê duyệt</label>
                                        <label for="msg_success" class="hidden">Bạn có chắc chắn phê duyệt hồ sơ này không?</label>
                                        <input id="opt_success" type="radio" value="leader_submit_verification_success">
                                    </div>
                                </div>
                            @endif
                        </td>
                    </tr>
                    @if ($objMember->status == $memberOfficial && Route::currentRouteName() == 'admin_member_detail_view')
                        <tr id="notes" style="display: none;">
                            <td style="width: 14%; padding-left: 12px;vertical-align: middle; display: none;">Ghi chú</td>
                            <td>
                                <textarea name="idea" rows="5" id="note-msg" autofocus maxlength="200" style="display: none;">Phần ghi chú xếp hạng</textarea>
                            </td>
                        </tr>   
                    @else
                        <tr id="notes">
                            <td style="width: 14%; padding-left: 12px;vertical-align: middle;">Ghi chú</td>
                            <td>
                                <textarea name="idea" rows="5" id="note-msg" autofocus maxlength="200">{{ htmlentities($last_note) }}</textarea>
                                <span id="note-error" class="help-block" style="display: none; color: #a94442;"><!-- Bạn cần phải nhập Ghi chú khi Từ chối hồ sơ hoặc Yêu cầu bổ sung hồ sơ --></span>
                            </td>
                        </tr>   
                    @endif                   
                                    
                </table>
                <div class="form-btns container">
                    <div class="row justify-content-center">                        
						@if ($objMember->status == $memberOfficial)
							<div class="choice {{ $objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER ? 'chosen' : '' }} hidden">
								<label for="opt_rank">Xếp hạng HDV</label>
								<label for="msg_rank" class="hidden">Bạn có chắc chắn xếp hạng HDV hồ sơ này không?</label>
								<input id="opt_rank" type="radio" value="rank_official_member">
							</div>
							<button id="save-button" class="submit_button btn btn-primary"> Xếp hạng </button>
						@else
							<button id="save-button" class="submit_button btn btn-primary"> Lưu</button>
						@endif
						
                        <button class="submit_button btn btn-warning" onclick="window.history.back();" type="button">Thoát</button>
                    </div>
                </div>
                <?php if (session()->has('memberInvalid')) {
                ?>
                    <span id="invalid-error" style="color: red;">{{ session()->get('memberInvalid') }}</span>
                <?php
                } ?>
                @include('admin.member.confirm_modal')

            </form>
        </div>
    </div>
@endsection
<style>
    body .btn-view {
        margin-top: 15px;
    }
    .submit_button {
        background-color: #2b95cb;
        color: #fff;
        border: 0;
        text-transform: uppercase;
        height: 30px;
        line-height: 30px;
        padding: 0 15px;
        border-radius: 2px;
    }
    .help-block {
        color: #a94442;
        margin-top: 5px;
        margin-bottom: 10px;
    }
</style>
@section('header_embed')
    <script type="text/javascript" src="{{ asset('admin/js/jquery.min.js') }}"></script>
@endsection
@section('footer_embed')
    <!-- Bootstrap JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            var inValidFlag = false;

            // Enter to submit form
            document.body.addEventListener('keydown', function(e) {
                var key = e.which;
                if (key == 13) {
                    $("#save-button").click();
                    if($("#confirmModal").css('display') == 'block'){
                        $("#confirm-save-button").click();
                    }
                }
            });

            $("input[name='majors']").click(function() {
                $('#rank_value').val($(this).val());
            });

            $('.choice').click(function() {
                var class_name = $(this).find('input').val().split('_');
                var test = class_name[class_name.length - 1];
                var msg = "Bạn cần phải nhập Ghi chú khi "; 
                msg = msg  + $(this).data('msgerror');
                if(['reject', 'update'].indexOf(test) != -1) {
                    $("#note-error").html(msg);
                    $('#notes').removeClass("hidden");
                } else {
                    $("#note-error").html("");
                    $('#notes').addClass("hidden");
                    $("#note-error").hide();
                }
                console.log($(this));
                $('#type_of_action').val($(this).find('input').val());

            });

            $('#save-button').click(function (e) {
                e.preventDefault();
                // var rank_checked = $("input[name='majors']:checked").val();
                // if (typeof rank_checked == 'undefined') {
                //     $("#rank-error").html('Bạn cần nhập hạng HDV');
                //     $("#rank-error").show();
                //     return false;
                // }
                // else {
                //     $("#rank-error").html("");
                //     $("#rank-error").hide();
                // }
                var msg = $("#note-msg").val();
                var determined = $('#type_of_action').val();
                var idVal = determined.split('_').slice(-1)[0];
                var qd = $("label[for='opt_"+idVal+"']").text();
                var msgqd = $("label[for='msg_"+idVal+"']").text();
                var submit_type = ['leader_submit_verification_reject', 'leader_submit_verification_update',
                                    'submit_verification_reject', 'submit_verification_update', 'member_official_rank'];
                if(submit_type.indexOf(determined) != -1 && msg == "") {
                    $("#note-error").show();
                    return false;
                } else {
                    $("#note-error").hide();
                    //kiểm tra xem hồ so đã được xử lý ?
                    var id = $("#id").val();
                    var status_old = $("#status_old").val();
                    $.ajax({
                        url: '/officesys/member/check_member?id='+id+'&status_old='+status_old,
                        type: 'GET',
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        processData: false,
                        success: function (data) {
                            status = data.status;
                            if(status == 1){
                                //hồ sơ đã xử lý
                                $(".modal-title").html('Xác nhận hồ sơ xử lý');
                                $("#msg_confirm").html('Hồ sơ này đã được xử lý');
                                $("#confirm-save-button").hide();
                                inValidFlag = true;
                            }else{
                                $('.modal-title').text(qd);
                                $('#msg_confirm').text(msgqd);
                            }
                            $('#confirmModal').modal('show');
                        }
                    });
                    $("button#save-button").text("Đang xử lý ...").attr('disabled', 'disabled');
                    return false;
                }
            });

            $('#confirm-save-button').click(function (e) {
                $("#note-error").hide();
                $('#approve').submit();
                $("button#confirm-save-button").text("Đang xử lý ...").attr('disabled', 'disabled');
            });
            
            $('#confirm-cancel-button').click(function (e) {
                e.preventDefault();
                $("button#save-button").text("Lưu").removeAttr('disabled');
                $('#confirmModal').modal('hide');
                $('#msg_confirm').text('');
                if (inValidFlag) {
                    @if (UserServiceProvider::isLeaderRole())
                        window.location.href = "{{ url('/officesys/member') }}?status={{\App\Constants\MemberConstants::APPROVAL_WAITING}}";
                    @endif
                    @if (UserServiceProvider::isExpertRole())
                        window.location.href = "{{ url('/officesys/member') }}?status={{\App\Constants\MemberConstants::VERIFICATION_WAITING}}";
                    @endif
                }
            });

            $("#confirmModal").on("hidden.bs.modal", function () {
                // put your default event here
                $("button#save-button").text("Lưu").removeAttr('disabled');
            });
        });
    </script>
@endsection
