<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Providers\UserServiceProvider;
?>

@extends('admin.layouts.app')
@section('title', 'Quản trị gia hạn thẻ hội viên')
@section('content')
    <style>
        .button {
            width: 30px;
            height: 30px;
            line-height: 30px;
            padding: 6px 15px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            margin: 0 0 10px 10px;
            border: 0;
        }

        .is_fee > .text-center > a > .glyphicon-plus {
            color: #fff !important;
        }
    </style>
    <div class="page-wrap">
        @include('admin.layouts.steps')
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.search')
            <div class="total" style="margin-bottom: 10px">Tổng số : {{ $count_members }}</div>
            <div class="table-wrap">
            <table>
                <thead>
                <tr>
                    <td width="5%">STT</td>
					<td width="5%">Mã hồ sơ</td>
                    <td width="7%">Số thẻ HDV</td>
                    <td width="7%">Số thẻ <br/> hội viên</td>
                    <td width="20%">Họ và tên</td>
                    <td width="7%">Ngày sinh</td>
                    <td width="7%">Ngày <br/>tạo hồ sơ</td>
                    <td width="7%">Ngày <br/>thẩm định</td>
                    <td width="7%">Ngày <br/>phê duyệt</td>
                    <td width="7%">Ngày <br/>nộp Lệ phí <br/> Hội phí</td>
                    <td width="13%">Trạng thái</td>
                    <td width="15%">Chức năng</td>
                </tr>
                </thead>
                <tbody>
                    @if (count($objMembers) == 0)
                        <tr>
                            <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            <tr class="">
                                <td class="text-center">{{ $current_paginator + $key + 1 }}</td>
								<td class="text-left">{{ empty($objMember->file_code) ? '' : $objMember->file_code }}</td>
                                <td class="text-center"> <a href="{{ route('admin_list_member_detail_view' , $objMember->id ) }}">{{ $objMember->touristGuideCode }} </a></td>
                                <td class="text-center"> {{ $objMember->member_code }} </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-center"> {{ empty($objMember->createdAt) ? '' : date('d/m/Y', strtotime($objMember->createdAt)) }} </td>
                                <td class="text-center"> {{ empty($objMember->verified_at) ? '' :  date('d/m/Y', strtotime($objMember->verified_at)) }} </td>
                                <td class="text-center"> {{ empty($objMember->approved_at) ? '' :  date('d/m/Y', strtotime($objMember->approved_at)) }} </td>
                                <td class="text-center"> {{ empty($objMember->date_payment) ? '' : $objMember->date_payment }} </td>
                                @if (array_key_exists($objMember->status, MemberConstants::$file_const_display_status))
                                    <td class="text-left">
                                        {{ array_get(MemberConstants::$file_const_display_status, $objMember->status, '') }}
                                    </td>
                                @else
                                    <td class="text-left"></td>
                                @endif

                                <td class="text-center">
                                    @if (UserServiceProvider::isMemberShipCardIssuer() && $objMember->status == 132)
                                        <a class="btn-function" href="javascript:void(0);" onclick="openPopupRenewCard('{{ $objMember->id }}','creatd-card', '{{array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '')}}');" data-toggle="tooltip" data-placement="left" title="Gia hạn thẻ hội viên">
                                             <span class="glyphicon glyphicon-plus" style="font-size: 16px; color: #009933;"></span> 
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
            <!-- Modal form to add a post -->
            <div id="addModal" class="modal fade" role="dialog">

                <div class="modal-dialog" id="id_show">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Cấp mã hội viên</h4>
                        </div>

                        <div class="modal-body">
                            {{csrf_field() }}
                            <form id="uploaddiamond" class="form-horizontal form-label-left" method="get"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="" id="idMember"/>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Số HDV: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="touristGuideCode"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Họ và tên: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="fullName" style="word-wrap: break-word;"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Ngày sinh: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="formattedBirthday"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Hội viên VTGA từ: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="hv_member_from"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Chi hội sinh hoạt: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="province_code_value"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Mã số thẻ hội viên: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="member_code"></label>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button btn-primary" class="btn btn-success savedecision" id="save-decision" onclick="changeStatus(event);" tabindex="1">
                                        Gia hạn
                                    </button>
                                    <button type="button btn-danger" class="btn btn-warning" data-dismiss="modal" tabindex="2">
                                        Thoát
                                    </button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>

            <div id="addModalCreateCardNew" class="modal fade" role="dialog">

                <div class="modal-dialog" id="id_show">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Cấp thẻ hội viên</h4>
                        </div>

                        <div class="modal-body">
                            <form id="uploaddiamondnew" class="form-horizontal form-label-left" method="post"
                                  enctype="multipart/form-data">
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                <input type="hidden" value="" id="idMemberNew"/>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Số HDV: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="touristGuideCodeNew"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Họ và tên: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="fullNameNew" style="word-wrap: break-word;"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Ngày sinh: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="formattedBirthdayNew"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Hội viên VTGA từ: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="hv_member_from_new"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Chi hội sinh hoạt: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="province_code_value_new"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Mã số thẻ hội viên: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="member_code_new"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Ngày cấp thẻ: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="member_from_new"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Ngày hết hạn: </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="date datetime-input" id="end_expire_new" value="" style="width: 50%;">
                                    </div>
                                </div>


                                <div class="modal-footer">
                                    <button type="button btn-primary" class="btn btn-success savedecisionnew" id="save-decision_new" onclick="changeStatusRenewCard(event);" tabindex="1">
                                        Cấp thẻ
                                    </button>
                                    <button type="button btn-danger" class="btn btn-warning" data-dismiss="modal" tabindex="2">
                                        Thoát
                                    </button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
            <!-- End Modal form to add a post -->
            @include('admin.layouts.pagination')
        </div>
    </div>
@endsection
<style>
    .is_fee, .is_fee:hover {
        background: #2b95cb !important;
    }
    .is_fee td, .is_fee a {
        color: #fff;
    }
</style>
@section('footer_embed')
    <style>

        .btn btn-delete {
            line-height: 30px;
            padding: 6px 6px;
            text-align: center;
            background-color: #fff;
            color: #000 !important;
            border-radius: 3px;
            margin: 0 0 10px 10px;
            border: 0;
        }

        .form-horizontal .control-label {
            padding-top: 7px;
            margin-bottom: 0;
            text-align: left;
        }

        #addModal .btn-upload {
            margin-bottom: 20px;
        }

        #addModal .btn-upload label {
            font-family: "Times New Roman", Times, serif;
            font-size: 14px;
            color: #2481bf;
            text-decoration: underline;
            cursor: pointer;
        }

        #addModal .btn-upload input[type=file] {
            visibility: hidden;
            pointer-events: none;
            height: 0;
        }

        #addModal .btn-upload .spli {
            cursor: pointer;
            font-size: 13px;
            color: #2481bf;
        }
    </style>

    <script type="text/javascript" src="{{ asset('admin/js/jquery.min.js') }}"></script>
     <script type="text/javascript" src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- Bootstrap JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- toastr notifications -->
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById("file_code").focus();
        });

        var getDaysInMonth = function(month,year) {
            return new Date(year, month, 0).getDate();
        };

        function getCurrentDate() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
              dd = '0' + dd;
            } 
            if (mm < 10) {
              mm = '0' + mm;
            } 
            var today = dd + '/' + mm + '/' + yyyy;
            return today;
        }

        function openPopupRenewCard (id, url, province_code) {
            if(url == 'nocard'){
                $("#save-decision_new").hide();
            }else{
                $("#save-decision_new").show();
            }
            $("#idMemberNew").val(id);
            var data = {};
            data.id = id;
            $.ajax({
                url: 'detailUserCreateCard?id='+id,
                type: 'GET',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (data) {
                    var d = new Date();
                    var year = d.getFullYear();
                    birthday = data.birthday;
                    if(birthday != '' && birthday != null){
                        birthday = birthday.substr(0, 10);
                        arr = birthday.split("-");
                        birthday = arr[2] + '/' + arr[1] + '/' + arr[0] ;
                    }

                    var date = data.expirationDate;
                    if(date != '' && date != null){
                        date = date.substr(0, 10);
                        arr = date.split("-");
                        date = arr[2] + '/' + arr[1] + '/' + arr[0] ;
                    }                    
                
                    province_code_value = province_code != '' ? ('HDV Du Lịch ' + province_code) : '';
                    $("#touristGuideCodeNew").html(data.touristGuideCode);
                    $("#fullNameNew").text(data.fullName);
                    $("#formattedBirthdayNew").html(birthday);
                    $("#hv_member_from_new").html(data.member_from);
                    $("#province_code_value_new").html(province_code_value);
                    $("#member_code_new").html(data.member_code);
                    $("#member_from_new").html(data.member_from);
                    $("#end_expire_new").val(date);
                    $('#addModalCreateCardNew').modal('show');
                }
            });
        }

        function changeStatusRenewCard(event){
            $('#save-decision_new').text("Đang xử lý ...").attr('disabled', 'disabled');
            var idMember = $("#idMemberNew").val();
            var dateExpiration = $("#end_expire_new").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'changeStatusRenewCard',
                type: 'POST',
                data: {
                    id: idMember,
                    expirationDate : dateExpiration
                },
                success: function (data) {
                    if (data.status == 0) {                        
                        $('#addModalCreateCardNew').modal('hide');
                        toastr.clear();
                        $('#save-decision_new').text("Gia hạn thẻ").removeAttr('disabled');
                        if(data.errors) {
                            toastr.warning(data.errors, 'Thông báo', {timeOut: 0});
                        }                        
                    } else {
                        toastr.success('Gia hạn thẻ hội viên thành công!', 'Thông báo', {timeOut: 500});
                        setTimeout(function () {
                            location.reload(true);
                        }, 500);
                    }
                }
            });
            event.preventDefault();
        }
    </script>
@endsection
