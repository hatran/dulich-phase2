@extends('admin.layouts.app')
@section('title', 'Gửi thông báo')
@section('content')
    <div class="page-wrap">
		<link href="{{asset('admin/select2/css/select2.min.css')}}" rel="stylesheet" />
		<script src="{{asset('admin/select2/js/select2.min.js')}}"></script>
		<script src="{{asset('admin/select2/js/i18n/vi.js')}}"></script>
		<style>
			.color-required{
				color: red
			}
			.error_border {
				border-color: #a94442 !important;
				box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
			}
			.help-block strong {
				color: red
			}
			.has-error .control-label {
				color: #3D3A3A !important
			}
			.select2-search__field{
                width:100% !important;
            }
		</style>
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Gửi thông báo</div>

            <div class="panel-body">
                @if(session('successes'))
                    <div id="closeButton" class="alert alert-success">
                        <button type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach (session('successes') as $success)
                                <li>{{ $success }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($errors->any())
                    <div id="closeButton" class="alert alert-danger">
                        <button type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal" method="POST" action="{{ route('admin.sendnoti_post') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('member') ? ' has-error' : '' }}">
                        <label for="member" class="col-md-4 text-right">Hội Viên<span class="color-required">*</span></label>

                        <div class="col-md-8">                            
							<div class="dropfield field-wrap col-md-8 col-sm-8 col-xs-12" style="margin-bottom: 10px; padding-left: 0px;">
								<select class="select_vpdd" id="province_type" name="province_type" onchange="changeProvinceCode()">
									<option value="" selected>Toàn Quốc</option>
									@foreach($offices as $value)
										<option value="{{ $value->id }}" @if (old('province_type') == $value->id) selected="selected" @endif>{{ $value->name }}</option>
									@endforeach
								</select>
							</div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('province_code') ? ' has-error' : '' }}">
                        <label for="province_code" class="col-md-4 text-right">Nơi đăng ký sinh hoạt</label>

                        <div class="col-md-8">                            
                            <div class="dropfield field-wrap col-md-8 col-sm-8 col-xs-12" style="margin-bottom: 10px; padding-left: 0px;">
                                <select class="province_code" id="province_code" name="province_code">
                                    <option value="" selected></option>
                                </select>
                                <input type="hidden" id="province-code-selected" value="<?php echo array_get($response_view, 'province_code', ''); ?>">
                                <input type="hidden" id="filter-by-status" value="<?php echo isset($statusFilter) ? $statusFilter : -1 ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                        <label for="type" class="col-md-4 text-right">Thể loại <span class="color-required">*</span></label>

                        <div class="col-md-2">
                            <label for="email">Email </label>
                            <input id="email" type="radio" name="type" value="1" style="margin-right: 15px;" @php
                                    if (!is_null(old('type'))) {
                                        if (old('type') == 1) {
                                            echo 'checked="checked"';
                                        }
                                    } else {
                                        echo 'checked="checked"';
                                    } 
                                @endphp>
                            <label for="sms">Sms </label>
                            <input id="sms" type="radio" name="type" value="2" @php
                                    if (!is_null(old('type'))) {
                                        if (old('type') == 2) {
                                            echo 'checked="checked"';
                                        }
                                    }
                                @endphp>
                        </div>

                        <div class="col-md-6">
                            <label for="attach" class="col-md-4 text-right">File đính kèm</label>

                            <div class="col-md-6">
                                <input type="file"name="upload" accept="application/pdf">
                            </div>
                        </div>
                    </div>
                    @if (isset($alert))
                        <span class="help-block">
                            <strong>{{ $alert }}</strong>
                        </span>
                    @endif
					<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                        <label for="type" class="col-md-4 text-right">Tiêu đề <span class="color-required">*</span></label>

                        <div class="col-md-6">                           
                            <input id="subject" type="input" name="subject" class="form-control col-md-12" value="{{ old('subject') }}" style="margin-right: 15px;" >                            
                        </div>
                    </div>
                    <div class="form-group{{ isset($text) ? ' has-error' : '' }}">
                        <label for="content" class="col-md-4 text-right">Nội dung <span class="color-required">*</span></label>
                        <div class="col-md-6">
                            <p style="color: red; font-weight: bold;">Sms: Viết không dấu và dưới 150 chữ</p>
                            <textarea rows="15" id="content_rg" name="text" class="form-control position-content tinymce">{{ old('text') }}</textarea>
                            @if (isset($content))
                                <span class="help-block">
                                    <strong>{{ $content }}</strong>
                                </span>
                            @endif
                            <input type="hidden" id="count_text" name="count">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary" id="btnSend">
                                Gửi
                            </button>
                            <a href="{{ URL::route('admin.sendnoti') }}" class="btn btn-warning" style="color: #FFFFFF;">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('footer_embed')
    <script>
        $('form').submit(function (e) {
            $('#count_text').val($('.mce-wordcount').text());
            return;  
        });

        var changeProvinceCode = function(){
            var val = jQuery('#province_type').val();
            var sta = jQuery('#filter-by-status').val();
            changeProvinceType(val, sta);      
        }

        function changeProvinceType(parent_id, province_code_selected = '', status) {
            var province_type_array = [];
            $('#province_type option').each(function() {
                if ($(this).val() != "") {
                    province_type_array.push($(this).val()); 
                }
            });
        
            $.ajax({
                type: "POST",
                url: '/searchactivityregistrationplace',
                data: { parent_id: parent_id, status: status, province_type: province_type_array, _token: '{{csrf_token()}}' },
                success: function (data) {
                  $("#province_code option").remove();
                  var option = "";
                  option += "<option value='' selected></option>";
                  $.each(data.value, function (i, value) {
                    if (value.id == province_code_selected) {
                        option += "<option value="+ value.id +" data-parent="+ value.parent_id +" Selected>"+ value.name +"</option>";
                    }
                    else {
                        option += "<option value="+ value.id +" data-parent="+ value.parent_id +">"+ value.name +"</option>";
                    }
                  })
                  $("#province_code").append(option);
                  // console.log(option);
                },
                error: function (data, textStatus, errorThrown) {
                    console.log(data);

                },
            });
        };

        var parent_id = $("#province_type option:selected").val();
        var province_code_selected = $("#province-code-selected").val();
        var filter_by_status = $("#filter-by-status").val();
        // console.log(province_code_selected);
        changeProvinceType(parent_id, province_code_selected, filter_by_status);

    </script>
@endsection


