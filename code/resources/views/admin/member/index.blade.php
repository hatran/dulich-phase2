<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Providers\UserServiceProvider;

$isExpertRole = (\App\Providers\UserServiceProvider::isExpertRole() || \App\Providers\UserServiceProvider::isAdmin()) && (isset($status) && in_array($status, [1, 2, 7]));
$isLeaderRole = (\App\Providers\UserServiceProvider::isLeaderRole() || \App\Providers\UserServiceProvider::isAdmin()) && (isset($status) && in_array($status, [3, 8, 22]));
?>

@extends('admin.layouts.app')
@if($isExpertRole)
    @section('title', 'Thẩm định hồ sơ')
@elseif ($isLeaderRole)
    @section('title', 'Phê duyệt hồ sơ')
@endif

@section('content')
    <div class="page-wrap">
    @include('admin.layouts.steps')
    <!-- <div class="heading">Danh Sách Hồ Sơ {{ $isExpertRole ? 'Thẩm Định' : ($isLeaderRole ? 'Phê Duyệt' : '') }}</div> -->
        <div class="main-content" style="padding-bottom: 75px;">
            @if (session('successes'))
                <div class="alert alert-success">
                    <p>{{ session('successes') }}</p>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <button id="closeButton" type="button" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @include('admin.layouts.search')
            <div class="total" style="margin-bottom: 10px">Tổng số : {{$count_members}}</div>
            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%">STT</td>
                        <td width="10%">Số hồ sơ</td>
                        <td width="10%">Số thẻ HDV</td>
                        <td width="20%">Họ và tên</td>
                        <!-- <td width="10%">Ngày sinh</td> -->
                        <td width="10%">Số điện thoại</td>
                        <td width="10%">Ngày tạo hồ sơ</td>
                        <td width="15%">Nơi đăng ký sinh hoạt</td>
                        <!-- @if ($isExpertRole)
                        <td width="10%">Ngày thẩm định</td>
                        @endif
                        @if ($isLeaderRole)
                            <td width="10%">Ngày phê duyệt</td>
                        @endif -->
                        <td width="15%">Trạng thái</td>
                        <td width="15%">Chức năng</td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objMembers) == 0)
                        <tr>
                            <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">{{$objMember->file_code}}</td>
                                <td class="text-center"><a href="{{ url('/officesys/member/detail/' . $objMember->id) }}">{{ $objMember->touristGuideCode }} </a>
                                </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center">{{$objMember->firstMobile}}</td> 
                                <!-- <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td> -->
                                <td class="text-center"> {{ empty($objMember->createdAt) ? '' : date('d/m/Y', strtotime($objMember->createdAt)) }} </td>
                                <td class="text-left"> {{ array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '') }} </td>
                                <!-- @if($isExpertRole)
                                    <td class="text-center">
                                    @if ($objMember->status == MemberConstants::VERIFICATION_REJECTED)
                                        {{ date('d/m/Y', strtotime($objMember->updatedAt)) }}
                                    @elseif ($objMember->status == MemberConstants::UPDATE_INFO_1)
                                        {{ date('d/m/Y', strtotime($objMember->updatedAt)) }}
                                    @elseif (!empty($objMember->verified_at))
                                        {{ date('d/m/Y', strtotime($objMember->verified_at)) }}
                                    @else

                                    @endif
                                    </td>
                                    {{--<td class="text-center"> {{ ($objMember->status == MemberConstants::VERIFICATION_REJECTED || $objMember->status == MemberConstants::UPDATE_INFO_1) ? date('d/m/Y', strtotime($objMember->updatedAt)) : '' }} </td>--}}
                                @endif
                                @if ($isLeaderRole)
                                    <td class="text-center">
                                        @if ($objMember->status == MemberConstants::APPROVAL_REJECTED)
                                            {{ date('d/m/Y', strtotime($objMember->updatedAt)) }}
                                        @elseif ($objMember->status == MemberConstants::UPDATE_INFO_1)
                                            {{ date('d/m/Y', strtotime($objMember->updatedAt)) }}
                                        @elseif (!empty($objMember->approved_at))
                                            {{ date('d/m/Y', strtotime($objMember->approved_at)) }}
                                        @else

                                        @endif
                                    </td>
                                    {{--<td class="text-center"> {{ empty($objMember->verified_at) ? '' : date('d/m/Y', strtotime($objMember->verified_at)) }} </td>
                                    <td class="text-center"> {{ ($objMember->status == MemberConstants::APPROVAL_REJECTED || $objMember->status == MemberConstants::UPDATE_INFO_1) ? date('d/m/Y', strtotime($objMember->updatedAt)) : (empty($objMember->verified_at) ? '' : date('d/m/Y', strtotime($objMember->verified_at)) ) }} </td>--}}
                                @endif -->
                                @if(empty($objMember->status))
                                    <td><td>
                                @else
                                    <td class="{{ array_get(MemberConstants::$file_const_flag_css, $objMember->status, '') }} text-center">
                                        {{ array_get(MemberConstants::$file_const_display_status, $objMember->status, '') }}
                                    </td>
                                @endif

                                <td class="text-center">
                                    @if($isExpertRole)
                                        <a class="btn-function text-center" data-toggle="tooltip" data-placement="left" title="Thẩm định hồ sơ"
                                           href="{{ url('/officesys/member/detail/' . $objMember->id) }}">
                                            <span class="glyphicon glyphicon-check"  style="color: #0000ff; font-size: 18px;"></span>
                                        </a>                                        
                                    @elseif ($isLeaderRole)
                                        <a class="btn-function text-center" data-toggle="tooltip" data-placement="left" title="Phê duyệt hồ sơ"
                                           href="{{ url('/officesys/member/detail/' . $objMember->id) }}">
                                            <span class="glyphicon glyphicon-check"  style="color: #0000ff; font-size: 18px;"></span>
                                        </a>
                                        {{--@elseif (Auth::user()->role == UserConstants::LEADER_USER && $objMember->status == MemberConstants::FEE_WAITING)
                                            <a class="button create_card" href="{{ url('/officesys/member/create_card/' . $objMember->id) }}">Tạo mã số thẻ</a>--}}
                                    @endif
									<a href="{{ route('admin_member_profile_edit', ['id' => $objMember->id]) }}"
										class="update-profile-member-modal btn-function"
										data-toggle="tooltip"
										data-placement="left"
										title="Cập nhật thông tin hồ sơ hội viên"
										data-id="{{$objMember->id}}"
										data-status="{{$objMember->status}}"
										data-membercode="{{$objMember->member_code}}">
										<span class="glyphicon glyphicon-edit" style="color: #ff0000;"></span>
									</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>
@endsection
@section('footer_embed')
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }

        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }
    </style>    
@endsection
