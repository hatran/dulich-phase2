<table>
    <tbody>
        <tr>
            <td colspan="7" style="text-align: center; font-weight: 900">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</td>
        </tr>
        <tr>
            <td colspan="7" style="text-align: center; font-weight: 900">DANH SÁCH IN THẺ NGÀY {{ date('d/m/Y') }}</td>
        </tr>

        <tr class="header">
            <td>STT</td>
            <td>Số thẻ Hội viên</td>
            <td>Họ và tên</td>
            <td>Sinh ngày</td>
            <td>Hội viên VTGA từ</td>
            <td>Chi hội</td>
            <td>Hạn sử dụng</td>
        </tr>
        @if(!empty($arrMemberInfo))
        @foreach($arrMemberInfo as $key => $objMember)
            <tr class="main-content">
                <td>{{ ++$key }}</td>
                <td>{{ empty($objMember->member_code) ? '' : ''.$objMember->member_code }}</td>
                <td>{{ empty($objMember->fullName) ? '' : htmlentities($objMember->fullName) }}</td>
                <td>{{ $objMember->formattedBirthday }}</td>
                <td>{{ empty($objMember->member_from) ? '' : $objMember->member_from }}</td>
                <td style="text-transform: uppercase">
                    <?php
                        $chihoi = $objMember->typeOfPlace == 1 ? 'Chi hội ' . array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '') : '';
                        $clb = $objMember->typeOfPlace == 2 ? 'CLB thuộc hội: ' . array_get(\App\Models\Branches::getBranches03(), $objMember->province_code, '') : '';
                    ?>
                    {{ mb_strtoupper($chihoi, 'UTF-8') }}
                    {{ mb_strtoupper($clb, 'UTF-8') }}
                </td>
                <td>{{ empty($objMember->member_code_expiration) ? '' : date('d/m/Y', strtotime($objMember->member_code_expiration)) }}</td>
            </tr>
        @endforeach
        @endif
        <tr style="border: none">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="border: none">
            <td></td>
            <td></td>
            <td style="font-weight: 900">Người giao</td>
            <td></td>
            <td></td>
            <td style="font-weight: 900">Người nhận</td>
            <td></td>
        </tr>
    </tbody>
</table>
<style>
    .header td {
        background: #FFFF00;
        border: 1px solid #000;
    }
    .main-content td {
        border: 1px solid #000;
    }
</style>
