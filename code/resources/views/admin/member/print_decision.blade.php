<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Providers\UserServiceProvider;
?>

<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>In quyết định</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/arcana.css ') }}" />

    <script type="text/javascript" src="{{ asset('vendors/jQuery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tinymce/jquery.tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('images/favicon.ico') }}" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}" />

    <!-- Bootstrap CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{asset('admin/css/jquery.loader.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/offices.css')}}"/>
<style type="text/css">
	.h3, h3 {
		font-size: 18px !important;
		margin-top: 0 !important;
		margin-bottom: 3 !important;
	}
	.line {
		height: 1px;
		background: #000;
		margin: auto;
	}
	@font-face {
	  font-family: TimeNewRoman;
	  src: url(http://aic-daklib.tk/DemoMap/fonts/font-times-new-roman.ttf);
	}

	body {
		font-family: TimeNewRoman;
		padding: 30px 30px;
		font-size: 20px;
	}
	h3 {
		font-size:18px;
	}
	li {
		text-indent: 30px;
	}
	.no-list li {
		list-style: none;
	}
	.no-list li b {
		text-decoration: underline;
	}
</style>
</head>
<body style="color: #000 !important;">
	<header class="header">
		<div class="container">
			<div class="row">
				<div class="header-left text-center" style="width: 45%; float: left;">
					<h3 style="font-weight: 400">HIỆP HỘI DU LỊCH VIỆT NAM</h3>
					<h3><b style="font-size:18px;">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</b></h3>
					<!-- <div class="line" style="width: 200px;"></div>-->
					 <img src="{{ asset('images/line.png') }}" style="width: 200px;"/>
					<p style="margin-top: 10px;">Số:  {{  !empty($objDecision->number_decisive) ? htmlentities($objDecision->number_decisive) : '' }}</p>
				</div>
				<div class="header-right text-center"  style="width: 55%; float: right;">
					<h3>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h3>
					<h3 style="font-weight: 400;"><i>Độc lập – Tự do – Hạnh phúc</i></h3>
					<!-- <div class="line" style="width: 300px;"></div>-->
					<img src="{{ asset('images/line.png') }}" style="width: 150px;"/>
					<p style="margin-top: 10px;"><i>Hà Nội, ngày {{ empty($objDecision->sign_date) ? '' : date('d', strtotime($objDecision->sign_date)) }} tháng {{ empty($objDecision->sign_date) ? '' : date('m', strtotime($objDecision->sign_date)) }}  năm {{ empty($objDecision->sign_date) ? '' : date('Y', strtotime($objDecision->sign_date)) }}</i></p>
				</div>
			</div>
		</div>
	</header>
	<div class="main" style="margin-top: 50px;">
		<div class="container">
			<div class="row">
				<section class="about text-center" style="width: 100%">
					<div class="about_box">
						<h3 style="color: #000; font-weight: bold; font-size:18px !important;">QUYẾT ĐỊNH CỦA<br>CHỦ TỊCH HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</h3>
						<p><b  style="color: #000; font-size: 16px;">V/v công nhận hội viên<br>Hội Hướng dẫn viên Du lịch Việt Nam </b></p>
						<!-- <div class="line" style="width: 500px;margin-top: 20px;"></div> -->
						<img src="{{ asset('images/line.png') }}" style="width: 350px;"/>
					</div>
				</section>
				<!-- END ABOUT -->
				<!-- START SERVICE -->
				<section class="service" style="margin-top: 10px;width: 100%;">
					<div class="service_title text-center">
						<h3  style="color: #000; font-weight: bold; font-size:18px !important;">CHỦ TỊCH HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</h3>
					</div>
					<ul style="margin-top: 30px; list-style-type: none">
						<li>- Căn cứ Quyết định số 66/QĐ- HHDLVN ngày 10 tháng 10 năm 2017 của Hiệp hội Du lịch Việt Nam về việc thành lập Hội Hướng dẫn viên Du lịch Việt Nam;</li>
						<li>- Căn cứ vào Quy chế hoạt động của Hội Hướng dẫn viên Du lịch Việt Nam;</li>
						<li>- Căn cứ vào đơn đăng ký gia nhập Hội Hướng dẫn viên Du lịch Việt Nam của ông/bà {{ !empty($objMember->fullName) ? htmlentities($objMember->fullName) : '' }};</li>
						<li>- Theo đề nghị của Trưởng Ban Hội viên Hội Hướng dẫn viên Du lịch Việt Nam,</li>
					</ul>
					<div class="service_title text-center" style="margin-top: 40px;">
						<h3 style="color: #000; font-weight: bold;">QUYẾT ĐỊNH</h3>
					</div>
					<ul style="margin-top: 30px;" class="no-list">
						<li><b>Điều 1:</b> Công nhận ông/bà {{ !empty($objMember->fullName) ? htmlentities($objMember->fullName) : '' }} sinh ngày {{ !empty($objMember->birthday) ? date('d/m/Y', strtotime($objMember->birthday)) : '' }}, số thẻ HDV: {{ !empty($objMember->touristGuideCode) ? $objMember->touristGuideCode : ''}} là Hội viên chính thức của Hội Hướng dẫn viên Du lịch Việt Nam.</li>
						<li><b>Điều 2:</b> Ông/bà {{ !empty($objMember->fullName) ? htmlentities($objMember->fullName) : '' }} có trách nhiệm thực hiện đúng Quy chế hoạt động và các quy định của Hội Hướng dẫn viên Du lịch Việt Nam.</li>
						<li><b>Điều 3:</b> Các ông/bà Tổng thư ký, Trưởng các ban của Hội Hướng dẫn viên Du lịch Việt Nam và ông/bà {{ !empty($objMember->fullName) ? htmlentities($objMember->fullName) : ''}} chịu trách nhiệm thi hành quyết định này.</li>
					</ul>
				</section>
			</div>
		</div>
	</div>
	<footer style="margin-top: 20px;padding-bottom: 50px;">
		<div class="container">
			<div class="row">
				<div class="footr-left text-center" style="width: 35%; float: left">
					<h3 style="text-decoration: underline;"><i>Nơi nhận:</i></h3>
					<ul style="list-style-type: none">
						<li><i>- Như điều 3;</i></li>
						<li><i>- Lưu VP Hội.</i></li>
					</ul>
					
				</div>
				<div class="footr-left text-center" style="width: 60%; float: right">
					<h3 style="color: #000; font-weight: bold; font-size: 16px !important">TM. BCH HỘI HDV DU LỊCH VIỆT NAM</h3>
					<h3 style="color: #000; font-weight: bold; font-size: 16px !important">CHỦ TỊCH</h3>
					<!--<p style="margin-top: 90px;"><i style=" font-size: 18px !important">{{ !empty($objSigner->fullname) ? htmlentities($objSigner->fullname) : '' }}</i></p>-->
				</div>
			</div>
		</div>
	</footer>
	</body>
</html>
