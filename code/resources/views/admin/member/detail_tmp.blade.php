<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
use App\Constants\OtherConstants;
?>

@extends('admin.layouts.app')
@section('title', 'Chi tiết thành viên')
@section('content')
    <div class="page-wrap">
        @include('admin.layouts.steps')
        @include('admin.layouts.detail')
        <div class="block">
        <div class="block" style="margin-top:  10px; padding-bottom: 10px;">
            <form id="approve" name="approve" method="post">
                {{ csrf_field() }}
                <table style="border: none;">
                    <input type="hidden" id="type_of_action" name="type_of_action" value="manager_reject">
                    <tr>
                        <td colspan="2">
                            @if (UserServiceProvider::isLeaderManager())
                                <div class="radio-list choices field-wrap">
                                    <div class="choice chosen">
                                        <label for="opt_reject">Từ chối</label>
                                        <input id="opt_reject" type="radio" value="manager_reject">
                                    </div>
                                    <div class="choice">
                                        <label for="opt_verify">Phê duyệt</label>
                                        <input id="opt_verify" type="radio" value="manager_success">
                                    </div>
                                </div>
                            @endif
                        </td>
                    </tr>
                        <tr id="notes" class="">
                            <td style="width: 14%; padding-left: 12px;vertical-align: middle;">Ghi chú</td>
                            <td>
                                <textarea name="idea" rows="5" id="note-msg"></textarea>
                                <span id="note-error" class="help-block" style="display: none; color: #a94442;">Bạn cần phải nhập Ghi chú khi Từ chối hồ sơ</span>
                            </td>
                        </tr>
                </table>
                <div class="form-btns container">
                    <div class="row justify-content-center">                            
                        <input class="submit_button btn-primary" type="button" onclick="submitForm()" value="Lưu" style="margin-right: 50px;">
                        <input class="submit_button btn-warning" type="button" onclick="window.history.back()" value="Thoát">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
<style>
    body .btn-view {
        margin-top: 15px;
    }
    .submit_button {
        background-color: #2b95cb;
        color: #fff;
        border: 0;
        text-transform: uppercase;
        height: 30px;
        line-height: 30px;
        padding: 0 15px;
        border-radius: 2px;
    }
    .help-block {
        color: #a94442;
        margin-top: 5px;
        margin-bottom: 10px;
    }
</style>
@section('header_embed')
    <script type="text/javascript" src="{{ asset('admin/js/jquery.min.js') }}"></script>
@endsection
@section('footer_embed')
    <script>
        $(document).ready(function() {
            $('#opt_reject').click(function() {
                $('#notes').removeClass("hidden");
                $(this).parent().addClass("chosen");
                $('#opt_update').parent().removeClass("chosen");
                $('#opt_verify').parent().removeClass("chosen");

                $('#type_of_action').val($(this).val());
            });

            $('#opt_verify').click(function() {
                $('#notes').addClass("hidden");
                $(this).parent().addClass("chosen");
                $('#opt_reject').parent().removeClass("chosen");
                $('#opt_update').parent().removeClass("chosen");
                $("#note-error").hide();
                $('#type_of_action').val($(this).val());
            });
        });

        function submitForm() {
            var msg = $("#note-msg").val();
            var determined = $('#type_of_action').val();
            if((determined == "manager_reject") &&
                msg == "") {
                $("#note-error").show();
            } else {
                $("#note-error").hide();
                $('#approve').submit();
            }
        }
    </script>
@endsection
