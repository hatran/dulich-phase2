<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Providers\UserServiceProvider;
?>

@extends('admin.layouts.app')
@section('title', 'Quản trị cấp mã số thẻ hội viên')
@section('content')
    <style>
        .button {
            width: 30px;
            height: 30px;
            line-height: 30px;
            padding: 6px 15px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            margin: 0 0 10px 10px;
            border: 0;
        }

        .is_fee > .text-center > a > .glyphicon-plus {
            color: #fff !important;
        }
    </style>
    <div class="page-wrap">
        @include('admin.layouts.steps')
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.search')
            @include('admin.layouts.message')
            @if(array_get($response_view, 'status', '') == \App\Constants\MemberConstants::CODE_PROVIDING_WAITING )
                <a class="providing-code-notification button btn-primary" style="float:  right;width: 130px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important; margin-top: -12px;" data-file-id="-1" href="javascript:void(0)">Cấp mã hội viên</a>
            @endif
            <div class="total" style="margin-bottom: 10px">Tổng số : {{ $count_members }}</div>
            <div class="table-wrap">
            <table>
                <thead>
                <tr>
                    <td width="5%">STT</td>
					<td width="7%">Mã hồ sơ</td>
                    <td width="7%">Số thẻ HDV</td>
                    <td width="10%">Số thẻ <br/> hội viên</td>
                    <td width="15%">Họ và tên</td>
                    <td width="7%">Ngày sinh</td>
                    <td width="7%">Ngày <br/>tạo hồ sơ</td>
                    <td width="7%">Ngày <br/>thẩm định</td>
                    <td width="7%">Ngày <br/>phê duyệt</td>
                    <td width="13%">Trạng thái</td>
                    <td width="15%">Chức năng</td>
                    @if(array_get($response_view, 'status', '') == \App\Constants\MemberConstants::CODE_PROVIDING_WAITING )
                      <td width="5%" style="position: relative; top:0px; color: #144a8b;">Cấp mã<br>
                          <input type="checkbox" class="checkAll">
                          <label>&nbsp;</label>
                      </td>
                    @endif
                </tr>
                </thead>
                <tbody>
                    @if (count($objMembers) == 0)
                        <tr>
                            <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            <!-- <tr class="{{ ($objMember->is_fee == MemberConstants::IS_FEE && $objMember->status != MemberConstants::FEE_WAITING && UserServiceProvider::isAccountant()) ? 'is_fee' : '' }}"> -->
                            <tr class="">
                                <td class="text-center">{{ $current_paginator + $key + 1 }}</td>
								<td class="text-left">{{ empty($objMember->file_code) ? '' : $objMember->file_code }}</td>
                                <td class="text-center"> <a href="{{ route('admin_list_member_detail_view' , $objMember->id ) }}">{{ $objMember->touristGuideCode }} </a></td>
                                <td class="text-center"> {{ $objMember->member_code }} </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-center"> {{ empty($objMember->createdAt) ? '' : date('d/m/Y', strtotime($objMember->createdAt)) }} </td>
                                <td class="text-center"> {{ empty($objMember->verified_at) ? '' :  date('d/m/Y', strtotime($objMember->verified_at)) }} </td>
                                <td class="text-center"> {{ empty($objMember->approved_at) ? '' :  date('d/m/Y', strtotime($objMember->approved_at)) }} </td>
                                @if (array_key_exists($objMember->status, MemberConstants::$file_const_display_status))
                                    <td class="text-left">
                                        @if ($objMember->cardType == '1')
                                            {{ 'Chờ in thẻ gia hạn' }}
                                        @elseif ($objMember->cardType == '2')
                                            {{ 'Chờ in thẻ cấp lại' }}
                                        @elseif ($objMember->cardType == '0')
                                            {{ 'Chờ in thẻ lần đầu' }}
                                        @else
                                            {{ array_get(MemberConstants::$file_const_display_status, $objMember->status, '') }}
                                        @endif
                                    </td>
                                @else
                                    <td class="text-left"></td>
                                @endif

                                <td class="text-center">
                                    {{--Button Cấp mã hội viên chỉ hiển thị khi trạng thái là Chờ cấp mã hội viên, popup có save. update 20180191--}}
                                    @if (UserServiceProvider::isMemberShipCardIssuer() && ($objMember->status == MemberConstants::CODE_PROVIDING_WAITING || $objMember->status == 61))
                                        <a class="btn-function" href="javascript:void(0);" onclick="openPopup('{{ $objMember->id }}','{{ route('admin_member_do_create_card_view',[$objMember->id, 'create_card' => 1]) }}', '{{array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '')}}');" data-toggle="tooltip" data-placement="left" title="{{ $objMember->status == 61 ? 'Cấp lại thẻ hội viên' : 'Cấp mã hội viên' }}">
                                             <span class="glyphicon glyphicon-plus" style="font-size: 16px; color: #009933;"></span> 
                                        </a>
                                    @endif

                                    {{--Button Cấp mã hội viên chỉ hiển thị khi trạng thái là Chờ in thẻ, popup chỉ xem ko save. update 20180191--}}
                                    @if (UserServiceProvider::isMemberShipCardIssuer() && $objMember->status == 13)
                                        <a class="btn-function" href="javascript:void(0);" onclick="openPopup('{{ $objMember->id }}', 'nocard', '{{array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '')}}');" data-toggle="tooltip" data-placement="left" title="Xem mã hội viên">
                                            <span class="glyphicon glyphicon-eye-open" style="font-size: 16px;"></span>
                                        </a>
                                    @endif
                                    <a class="btn-function text-center show-modal-payment" data-toggle="tooltip" data-placement="left" title="Xem chi tiết hội phí"
                                           href="#"
                                           data-id="{{ $objMember->id }}?mpuid=null">
                                        <span class="glyphicon glyphicon-usd"  style="color: #0000ff; font-size: 14px;"></span>
                                    </a>
                                </td>
                                @if(array_get($response_view, 'status', '') == \App\Constants\MemberConstants::CODE_PROVIDING_WAITING )
                                    <td class="text-center">
                                        <span class="checkitem">
                                            <input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{ $objMember->id }}" class="checkbox-pr">
                                            <label for="member {{ $key + 1 }}">&nbsp;</label>
                                        </span>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
            <!-- Modal form to add a post -->
            <div id="addModal" class="modal fade" role="dialog">

                <div class="modal-dialog" id="id_show">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Xem mã thẻ hội viên</h4>
                        </div>

                        <div class="modal-body">
                            {{csrf_field() }}
                            <form id="uploaddiamond" class="form-horizontal form-label-left" method="get"
                                  enctype="multipart/form-data">
                                <input type="hidden" value="" id="idMember"/>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Số HDV: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="touristGuideCode"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Họ và tên: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="fullName" style="word-wrap: break-word;"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Ngày sinh: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="formattedBirthday"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Hội viên VTGA từ: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="hv_member_from"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Chi hội sinh hoạt: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="province_code_value"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Mã số thẻ hội viên: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="member_code"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Ngày cấp thẻ: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="member_from"></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Ngày hết hạn: </label>
                                    <div class="col-sm-9">
                                        <label class="control-label align_text" id="endexpire"></label>
                                    </div>
                                </div>


                                <div class="modal-footer">
                                    <button type="button btn-primary" class="btn btn-success savedecision" id="save-decision" onclick="changeStatus(event);" tabindex="1">
                                        {{ array_get($response_view, 'status', '') == 6 ? 'Cấp mã' : 'Cấp lại thẻ'}}
                                    </button>
                                    <button type="button btn-danger" class="btn btn-warning" data-dismiss="modal" tabindex="2">
                                        Thoát
                                    </button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
            <!-- End Modal form to add a post -->
            @include('admin.layouts.pagination')
        </div>
    </div>
    @include('admin.payment_yearly.detail')
@endsection
<style>
    .is_fee, .is_fee:hover {
        background: #2b95cb !important;
    }
    .is_fee td, .is_fee a {
        color: #fff;
    }
</style>
@section('footer_embed')
    <style>

        .btn btn-delete {
            line-height: 30px;
            padding: 6px 6px;
            text-align: center;
            background-color: #fff;
            color: #000 !important;
            border-radius: 3px;
            margin: 0 0 10px 10px;
            border: 0;
        }

        .form-horizontal .control-label {
            padding-top: 7px;
            margin-bottom: 0;
            text-align: left;
        }

        #addModal .btn-upload {
            margin-bottom: 20px;
        }

        #addModal .btn-upload label {
            font-family: "Times New Roman", Times, serif;
            font-size: 14px;
            color: #2481bf;
            text-decoration: underline;
            cursor: pointer;
        }

        #addModal .btn-upload input[type=file] {
            visibility: hidden;
            pointer-events: none;
            height: 0;
        }

        #addModal .btn-upload .spli {
            cursor: pointer;
            font-size: 13px;
            color: #2481bf;
        }
    </style>

    <script type="text/javascript" src="{{ asset('admin/js/jquery.min.js') }}"></script>
     <script type="text/javascript" src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- Bootstrap JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- toastr notifications -->
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById("file_code").focus();
        });

        var getDaysInMonth = function(month,year) {
            return new Date(year, month, 0).getDate();
        };

        function openPopup (id, url, province_code) {
            if(url == 'nocard'){
                $("#save-decision").hide();
            }else{
                $("#save-decision").show();
            }
            $("#idMember").val(id);
            var data = {};
            data.id = id;
            $.ajax({
                url: 'detailUser?id='+id,
                type: 'GET',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (data) {
                    var d = new Date();
                    var year = d.getFullYear();
                    var birthday = data.birthday;
                    var mce = data.member_code_expiration
                    if(birthday != '' && birthday != null){
                        birthday = birthday.substr(0, 10);
                        arr = birthday.split("-");
                        birthday = arr[2] + '/' + arr[1] + '/' + arr[0] ;
                    }

                    if(mce != '' && mce != null){
                        mce = mce.substr(0, 10);
                        arr = mce.split("-");
                        mce = arr[2] + '/' + arr[1] + '/' + arr[0] ;
                    }


                    member_from = data.member_from;
                    // if(member_from != '' && member_from != null){
                    //     member_from = member_from.substr(0, 10);
                    //     arr = member_from.split("-");
                    //     member_from = arr[2] + '/' + arr[1] + '/' + arr[0] ;
                    // }
                    province_code_value = province_code != '' ? ('HDV Du Lịch ' + province_code) : '';
                    $("#touristGuideCode").html(data.touristGuideCode);
                    $("#fullName").text(data.fullName);
                    $("#formattedBirthday").html(birthday);
                    $("#hv_member_from").html(member_from);
                    $("#province_code_value").html(province_code_value);
                    $("#member_code").html('');
                    $("#member_from").html(member_from);
                    $("#endexpire").html('');
                    $('#addModal').modal('show');
                }
            });

        }

        function changeStatus(event){
            $('#save-decision').text("Đang xử lý ...").attr('disabled', 'disabled');
            idMember = $("#idMember").val();
            $.ajax({
                url: 'changeStatus?id='+idMember,
                type: 'GET',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                /*beforeSend: function(){
                    // Show image container
                    toastr.warning('Đang tiến hành gửi thông báo. Vui lòng đợi trong giây lát.', {timeOut: 2000});
                },*/
                success: function (data) {
                    if (data.status == 0) {                        
                        $('#addModal').modal('hide');
                        toastr.clear();
                        $('#save-decision').text("Cấp mã").removeAttr('disabled');
                        if(data.errors) {
                            toastr.warning(data.errors, 'Thông báo', {timeOut: 0});
                        }                        
                    } else {
                        toastr.success('Cấp mã thẻ hội viên thành công!', 'Thông báo', {timeOut: 500});
                        setTimeout(function () {
                            location.reload(true);
                        }, 500);
                    }
                }
            });
            event.preventDefault();
        }

        $('body').on('change', '.checkAll', function() {
            $(".checkbox-pr").prop('checked',$(this).is(":checked"));
            if ($(this).is(":checked") == true) {
                $(".checkbox-pr").attr('data-check', 'yes');
            }
            else {
                $(".checkbox-pr").attr('data-check', 'no');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;
            
            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                    if ($(this).is(':checked')){
                        anyChecked = true;
                    } else {
                        anyChecked = false;
                    }
                });
                if (anyChecked == true) {
                    __this.attr("data-check", "yes");
                    
                    if (allRowLength == allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }   
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");
                
                __this.attr('data-check', 'no');
            }
        });

        function checkedNames() {
            check = 0;
            $("input[name=member]:checked").each( function () {
                check = 1;
            });
            return check;
        }

        $(".providing-code-notification").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('admin_member_providing_code_many') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        var urlPayment = "{{ url('/officesys/payment_yearly') }}";
    </script>
    <script type="text/javascript" src="/admin/js/show_payment_modal.js"></script>
@endsection
