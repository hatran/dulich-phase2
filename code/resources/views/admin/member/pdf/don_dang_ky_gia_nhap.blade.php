<html>
<head>
    <meta charset="utf-8">
    <style>
        body {
            font-family: DejaVu Sans, sans-serif;
        }
    </style>
</head>
<body>
<div style="width: 100%; text-align: center;"><strong><span style=" font-size: medium;">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</span></strong></div>
<div style="width: 100%; text-align: center;"><span style=" font-size: medium;">Độc lập - Tự do - Hạnh phúc</span></div>
<div style="width: 100%; text-align: center;">--------------------------------------</div>
<div style="width: 100%; text-align: center;">
    <strong><span style=" font-size: medium;">ĐƠN ĐĂNG KÝ </span></strong>
</div>
<div style="width: 100%; text-align: center;">
    <strong><span style=" font-size: medium;">GIA NHẬP HỘI VIÊN </span></strong>
</div>

<div style="margin: 0 10px 10px 30px">
<strong><p style="text-align: center; font-size: 14px;" dir="ltr">Kính gửi: Hội Hướng dẫn viên Du lịch Việt Nam</p></strong>
<strong>
    <p style="text-align: center; font-size: 14px;" dir="ltr">
        {{ $typeOfPlace == 1 ? 'Chi hội ' . array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '') : '' }}
        {{ $typeOfPlace == 2 ? 'CLB thuộc hội ' . array_get(\App\Models\Branches::getBranches03(), $objMember->province_code, '') : '' }}
    </p>
</strong>
<p dir="ltr">
    <span style=" font-size: 13px;">Sau khi đọc kỹ Quy chế hoạt động và Quy định của Hội, Chi hội hướng dẫn viên, tôi đồng ý và tự nguyện làm đơn này để đăng ký gia nhập Hội viên</span>
</p>

<p style="font-size: 13px; line-height: 10px;">
    1. Họ và tên (viết in hoa): <span style="text-transform: uppercase">{{ htmlentities($objMember->fullName) }}</span>
</p>
<p style="font-size: 13px;">
    <span style=" font-size: 13px;">@if(!empty($objMember->birthday))2. Sinh ngày: {{ $objMember->birthday->format('d') }}
        tháng {{ $objMember->birthday->format('m') }} năm {{ $objMember->birthday->format('Y') }}@endif - Giới tính:
        {{ $objMember->gender == 1 ? 'Nam' : 'Nữ' }}
    </span>
</p>
<p dir="ltr" style="line-height: 12px;"><span style=" font-size: 13px;">3. Số thẻ HDV du lịch: {{ $objMember->touristGuideCode }}

    @if($typeOfTravelGuide == 1 || $typeOfTravelGuide == 2)
        -Ng&agrave;y hết hạn: {{ empty($objMember->expirationDate) ? '' : $objMember->expirationDate->format('d-m-Y') }}
    @endif
    </span>
</p>
<p dir="ltr" style="line-height: 12px;"><span style=" font-size: 13px;">4. Số CMT/CCCD: {{ htmlentities($objMember->cmtCccd) }} - Cấp ngày: {{ $objMember->dateIssued->format('d-m-Y') }} - Tại: {{ htmlentities($objMember->issuedBy) }}</span></p>
<p dir="ltr" style="line-height: 12px;"><span style=" font-size: 13px;">5. Địa chỉ thường trú: {{ htmlentities($objMember->permanentAddress) }}</span></p>
<p dir="ltr" style="line-height: 12px;"><span style=" font-size: 13px;"> </span></p>
<p dir="ltr" style="line-height: 12px;"><span style=" font-size: 13px;">6. Địa chỉ liên hệ: {{ htmlentities($objMember->address) }}</span></p>
<p dir="ltr" style="line-height: 12px;"><span style=" font-size: 13px;">7. Tỉnh thành: {{ $objMember->province ? array_get($provincial, $objMember->province, '') : ''}}</span></p>
<p dir="ltr" style="line-height: 12px;"><span style="font-size: 13px;"></span></p>
<p dir="ltr" style="line-height: 12px;"><span style=" font-size: 13px;">8. Điện thoại di động 1: {{ $objMember->firstMobile }}</span></p>
<p dir="ltr" style="line-height: 12px;">
<span style=" font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;Điện thoại di động 2: {{ empty($objMember->secondMobile) ? '' : htmlentities($objMember->secondMobile) }}</span></p>
<p dir="ltr" style="line-height: 12px;"><span style=" font-size: 13px;">9. Email : {{ $objMember->firstEmail }}</span></p>
<p dir="ltr" style="line-height: 10px;">
    <span style=" font-size: 13px;">10. Đăng ký sinh hoạt tại:
        {{ $typeOfPlace == 1 ? 'Chi hội ' . array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '') : '' }}
        {{ $typeOfPlace == 2 ? 'CLB thuộc hội ' . array_get(\App\Models\Branches::getBranches03(), $objMember->province_code, '') : '' }}
    </span>
</p>
<p dir="ltr" style="line-height: 16px;"><span style=" font-size: 13px;">Tr&acirc;n trọng đề nghị &nbsp;Hội, Chi hội hướng dẫn viên xem x&eacute;t, chấp nhận để t&ocirc;i tham gia l&agrave;m hội vi&ecirc;n ch&iacute;nh thức. T&ocirc;i xin cam đoan sẽ thực hiện đầy đủ Quy chế hoạt động v&agrave; c&aacute;c quy định của Hội, Chi hội hướng dẫn viên ban hành</span>
</p>

<div style="width: 350px; float: right; text-align: center; clear: both;"><span style=" font-size: 13px;"> <em>..............., ngày {{ empty($objMember->createdAt) ? '' : date('d', strtotime($objMember->createdAt)) }} tháng {{ empty($objMember->createdAt) ? '' : date('m', strtotime($objMember->createdAt)) }} năm {{ empty($objMember->createdAt) ? '' : date('Y', strtotime($objMember->createdAt)) }}</em>.</span>
</div>
<br/>
<div style="width: 350px; float: right; text-align: center; clear: both;"><strong><span style="font-size: 14px;">NGƯỜI VIẾT ĐƠN</span></strong></div><br/>
<div style="width: 350px; float: right; text-align: center; clear: both;"><em>(K&yacute; v&agrave; ghi rõ họ tên)</em></div>
</p>

<br/><br/><br/><br/><br/><br/><br/><br/>
<div style="width: 350px; float: right; text-align: center;"><span style=" font-size: 13px;">{{ htmlentities($objMember->fullName) }}</span></div>
</div>
</body>
</html>
