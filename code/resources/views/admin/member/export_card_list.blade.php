<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
?>

@extends('admin.layouts.app')
@section('title', 'In thẻ hội viên')
@section('header_embed')
    <style>
        body .choices.checkbox-list .choice.chosen label:after, body .choices.checkbox-list .choice label:after {
            margin-top: -8px
        }
        body .checkall {
            padding-left: 25px !important;
        }
        body .checkall .choice {
            padding-left: 35px;
            padding-right: 10px;
            cursor: pointer;
        }
        body .checkall .choice input {
            display: none !important;
        }
        body .checkall .choice label {
            position: relative;
            cursor: pointer;
            margin-bottom: 0;
        }
        body .checkall .choice label:before {
            content: "";
            width: 17px;
            height: 17px;
            margin-top: -9px;
            border: 1px solid #6C6B6B;
            position: absolute;
            left: -25px;
            top: 50%;
            border-radius: 2px;
        }
        body .checkall .choice label:after {
            margin-top: -9px;
            content: "\F00C";
            font-family: 'FontAwesome';
            font-size: 12px;
            color: #BBBABA;
            left: -22px;
            position: absolute;
            top: 50%;
            opacity: 0;
            transition: 0.3s;
            -moz-transition: 0.3s;
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
            -ms-transition: 0.3s;
        }
        body .checkall .choice.chosen label:after {
            opacity: 1;
            color: #2B95CB;
        }
        body .checkall .choice:hover label:after {
            opacity: 1;
        }

        input[type=checkbox]
        {
            width: 20px;
            height: 20px;
            text-align: center; /* center checkbox horizontally */
            vertical-align: middle; /* center checkbox vertically */
        }

    </style>
@endsection
@section('content')
    <div class="page-wrap">
        <!-- <div class="heading">In thẻ hội viên</div> -->
        @include('admin.layouts.steps')
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.search')
            <a class="flex heading"
               style="display:block;"
               href="{{ route('admin_member_print_card_selected_view', ['print_card_type' => $arrPrintCardType[$response_view->input('is_printed_card')] ?? null]) }}">
                <button class="fa fa-print" type="button" style="float: right;">Danh sách in thẻ hội viên được chọn</button>
            </a>
            <div class="total" style="margin-bottom: 10px">Tổng số: {{ $count_members }}</div>
            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%">STT</td>
                        <td width="5%">Mã hồ sơ</td>
                        <td width="7%">Số thẻ HDV</td>
                        <td width="7%">Số thẻ <br/>hội viên</td>
                        <td width="20%">Họ và tên</td>
                        <td width="7%">Ngày sinh</td>                    
                        <td width="7%">Ngày <br/>gia nhập VTGA</td>
                        <td width="7%">Ngày <br/>in thẻ</td>
                        <td width="7%">Ngày <br/>hết hạn</td>
                        <td width="13%">Trạng thái <br/> in thẻ</td>
                        <td width="14%">Chức năng<br>
                            <span style="vertical-align: middle; color: #144a8b">Lưu vào danh sách</span>
                            <input type="checkbox" class="checkAll" name="member">
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objMembers) == 0)
                        <tr>
                            <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @php $printCardStatus = 0; @endphp
                        @foreach($objMembers as $key => $objMember)
                            <tr>
                                <td class="text-center">{{ \Request::input('is_printed_card') ? $current_paginator + $key + 1 : $key + 1 }}</td>
                                <td class="text-center">{{ empty($objMember->file_code) ? '' : $objMember->file_code }}</td>
                                <td class="text-center"> <a href="{{ route('admin_list_member_detail_view',$objMember->id) }}">{{ $objMember->touristGuideCode }} </a></td>
                                <td class="text-center"> {{ $objMember->member_code }} </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-center"> {{ empty($objMember->member_from) ? '' : date('d/m/Y', strtotime($objMember->member_from)) }} </td>
                                <td class="text-center"> {{ empty($objMember->printDate) ? '' :  date('d/m/Y', strtotime($objMember->printDate)) }} </td>
                                <td class="text-center"> {{ empty($objMember->member_code_expiration) ? '' :  date('d/m/Y', strtotime($objMember->member_code_expiration)) }} </td>
                                @if (array_key_exists($objMember->status, MemberConstants::$printed_card_status))
                                    <td class="text-left">
                                        @if ($objMember->wCardType == '1')
                                            {{ 'Chờ in thẻ gia hạn' }}
                                            @php $printCardStatus = 91; @endphp
                                        @elseif ($objMember->wCardType == '2')
                                            {{ 'Chờ in thẻ cấp lại' }}
                                            @php $printCardStatus = 92; @endphp
                                        @elseif ($objMember->wCardType == '0')
                                            {{ 'Chờ in thẻ lần đầu' }}
                                            @php $printCardStatus = 90; @endphp
                                        @elseif ($objMember->pCardType == '1')
                                            {{ 'Đã in thẻ gia hạn' }}
                                            @php $printCardStatus = 131; @endphp
                                        @elseif ($objMember->pCardType == '2')
                                            {{ 'Đã in thẻ cấp lại' }}
                                            @php $printCardStatus = 132; @endphp
                                        @elseif ($objMember->pCardType == '0')
                                            {{ 'Đã in thẻ' }}
                                            @php $printCardStatus = 130; @endphp
                                        @else
                                            {{ array_get(MemberConstants::$printed_card_status, $objMember->status, '') }}
                                        @endif
                                    </td>
                                @else
                                    <td></td>
                                @endif
                                <td class="text-center">
                                    <span class="checkitem">
                                        <input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{
                                         $objMember->id }}" class="checkbox-pr" {{ \App\Providers\MemberServiceProvider::checkSelected($objMember->id, $printCardStatus, $listPrintCardSelected) ? 'checked' : '' }}>
                                        <label for="member {{ $key + 1 }}">&nbsp;</label>
                                        <input type="hidden" class="printCardStatus" style="display: none" value="{{ $printCardStatus }}"></input>
                                    </span>
									<a class="btn-function" href="{{ route('member_decision_do_create_card_view',[$objMember->id, 'is_print' => 1]) }}"
									   target="_blank" data-toggle="tooltip" data-placement="left" title="In quyết định">
										<span class="glyphicon glyphicon-print" style="font-size: 16px; color: blue;"></span> 
									</a>
									<a href="{{ route('admin_member_profile_edit', ['id' => $objMember->id]) }}"
										class="update-profile-member-modal btn-function"
										data-toggle="tooltip"
										data-placement="left"
										title="Cập nhật thông tin hồ sơ hội viên"
										data-id="{{$objMember->id}}"
										data-status="{{$objMember->status}}"
										data-membercode="{{$objMember->member_code}}">
										<span class="glyphicon glyphicon-edit" style="color: #ff0000;"></span>
									</a>
                                    <a class="btn-function text-center show-modal-payment" data-toggle="tooltip" data-placement="left" title="Xem chi tiết hội phí"
                                           href="#"
                                           data-id="{{ $objMember->id }}?mpuid=null">
                                        <span class="glyphicon glyphicon-usd"  style="color: #0000ff; font-size: 14px;"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>
    @include('admin.payment_yearly.detail')
@endsection
@section('footer_embed')
    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById("file_code").focus();
            var checkboxCheckedLength = $('.checkbox-pr:checkbox:checked').length;
            var numberOfItems = {{ count($objMembers) }} ?? 0;
            if (numberOfItems > 0 && checkboxCheckedLength === numberOfItems) {
                $(".checkAll").attr('checked', true);
            }
        });
        
        $('body').on('change', '.checkAll', function() {
            var checkboxPr = $(".checkbox-pr");
            var isChecked = $(this).is(":checked");
            checkboxPr.prop('checked', isChecked);
            if (isChecked) {
                checkboxPr.attr('data-check', 'yes');
                handlePrintCardSelected(true, null, null, 'add');
            }
            else {
                checkboxPr.attr('data-check', 'no');
                handlePrintCardSelected(true, null, null, 'delete');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;
            var printCardType = __this.parent().children('.printCardStatus').val();
            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function () {
                    anyChecked = !!$(this).is(':checked');
                });
                if (anyChecked) {
                    __this.attr("data-check", "yes");
                    
                    if (allRowLength === allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }
                handlePrintCardSelected(false, __this.val(), printCardType, "add");
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");
                
                __this.attr('data-check', 'no');
              handlePrintCardSelected(false, __this.val(), printCardType, "delete");
            }
        });

        function handlePrintCardSelected(isSelectAll, memberId, printCardType, action) {
            var checkboxPr = $('.checkbox-pr');
            var printButton = $('#print_button');
            checkboxPr.attr('disabled', true);
            printButton.attr('disabled', true);
            var printData = [];
            if (isSelectAll) {
                var memberChecked = action === 'add' ? $("input[name='member']:not(input.checkAll):checked") : $("input[name='member']:not(input.checkAll)");
                var printCardStatus = $(".printCardStatus");
                for (var i = 0; i < memberChecked.length; i++) {
                    printData.push(
                        {
                            'member_id': memberChecked[i].value,
                            'type': printCardStatus[i].value
                        }
                    );
                }
            } else {
                printData.push(
                    {
                        'member_id': memberId,
                        'type': printCardType
                    }
                )
            }
            handleAjax(action, printData);
            checkboxPr.attr('disabled', false);
            printButton.attr('disabled', false);
        }

        function handleAjax(action, attributes) {
          $.ajax({
            type: 'POST',
            url: action === "add" ? "{{ route('admin_member_add_print_card_selected') }}" : "{{ route('admin_member_delete_print_card_selected') }}",
            data: {
              '_token': '{{ csrf_token() }}',
              'attributes': attributes
            },
            success: function (data) {
              var messageError = action === "add" ? 'lưu' : 'xóa';
              var messageSuccess = action === "add" ? 'Lưu vào' : 'Xóa';
              if (data.message === "ERROR") {
                toastr.warning('Lỗi không ' + messageError + ' được', 'Thông báo', {timeOut: 2000});
              } else {
                toastr.success(messageSuccess + ' danh sách thành công!', 'Thông báo', {timeOut: 2000});
              }
            }
          });
        }

        var urlPayment = "{{ url('/officesys/payment_yearly') }}";
    </script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script type="text/javascript" src="/admin/js/show_payment_modal.js"></script>
@endsection