<p>Kính gửi Quý Hội viên Hội Hướng dẫn viên Du lịch Việt Nam</p>
<p>Đề nghị ông/ bà đóng 500.000 VNĐ hội phí năm 2019 về tài khoản:</p>
<p>Hiệp hội Du lịch Việt Nam</p>
<p>Số tài khoản: 0011004369919</p>
<p>Ngân hàng: Sở Giao dịch-Ngân hàng Thương mại Cổ phần Ngoại thương Việt Nam (SGD-Vietcombank)</p>
<p><strong>Nội dung thanh toán:</strong> Đóng Hội phí Hội HDV năm 2019, số thẻ hội viên:{{ empty($objMember->member_code) ? '' : $objMember->member_code}}, hoặc số HDV: {{ empty($objMember->touristGuideCode) ? '' : $objMember->touristGuideCode}}</p>
<p><strong>Lưu ý:</strong> Để tránh nhầm lẫn, trong nội dung thanh toán nhất thiết phải ghi số thẻ hội viên</p>
<p><strong>Mọi thông tin chi tiết xin liên hệ:</strong></p>
<p>Hội Hướng dẫn viên Du lịch Việt Nam ĐT: 024.37835120, 37835122</p>
