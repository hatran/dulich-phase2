<p>Kính gửi ông bà {{ htmlentities($objMember->fullName) }}</p>
<p>Hồ sơ đăng ký gia nhập Hội Hướng dẫn viên Du lịch Việt Nam của ông/bà đã bị yêu cầu bổ sung thông tin:  <a href="{{ UrlSigner::sign(url('/member/update/' . $objMember->id . '/edit')) }}">(Chi tiết hồ sơ)</a></p>
<p>Lý do yêu cầu bổ sung: {{ empty($objMember->notes->last()->note) ? '' : $objMember->notes->last()->note }}</p>
<p>Nếu cần trao đổi xin ông/ bà liên hệ:</p>
<p>Hội Hướng dẫn viên Du lịch Việt Nam: ĐT: 024.37835120, 37835122</p>
{{--<p>VPĐD Hội Hướng dẫn viên Du lịch Việt Nam tại Đà Nẵng: ĐT: 0236.3886977, 3886933</p>--}}
{{--<p>VPĐD Hội Hướng dẫn viên Du lịch Việt Nam tại TP. HCM: ĐT: 028.22535139</p>--}}
<p>Thư điện tử này được gửi ra từ hệ thống tự động. Vui lòng không trả lời thư điện tử này.</p>