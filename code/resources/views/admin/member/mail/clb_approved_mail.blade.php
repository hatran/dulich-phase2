<p>Xin chào ông/bà  {{ htmlentities($objMember->fullName) }},
<p>Cảm ơn ông/bà đã đăng ký gia nhập Hội Hướng dẫn viên Du lịch Việt Nam</p>
<p>Số hồ sơ số <b>{{ empty($objMember->file_code) ? '' : $objMember->file_code }} </b> đã được Hội Hướng dẫn viên Du lịch Việt Nam phê duyệt</p>
<p>Để hoàn thành thủ tục đăng ký gia nhập, xin vui lòng truy cập đường link sau <a href="{{ action('MemberController@downloadContractFile', ['fileName' => md5($objMember->id) . '_detail.pdf', 'fileNameSaved' => 'Phiếu thông tin']) }}">Phiếu thông tin hội viên</a> và  <a href="{{ action('MemberController@downloadContractFile', ['fileName' => md5($objMember->id) . '.pdf', 'fileNameSaved' => 'Đơn đăng ký']) }}">Đơn đăng kí gia nhập Hội HDVDL VN</a> để tải đơn đăng ký gia nhập và phiếu thông tin hội viên chính thức.</p>
<p>Hồ sơ gia nhập Hội Hướng dẫn viên Du lịch Việt Nam gồm các giấy tờ sau:</p>
<p>
	<ol style="list-style-type:decimal;">
		<li>Đơn đăng ký gia nhập Hội (bản gốc)</li>
		<li>Phiếu thông tin Hội viên có dán ảnh chân dung mầu cỡ 3cm x 4 cm (bản gốc)</li>
		<li>Thẻ hướng dẫn viên Du lịch (bản sao)</li>
		<li>Chứng minh thư/căn cước công dân (bản sao)</li>
	</ol>
</p>
<p><b>Hồ sơ gia nhập xin gửi về:</b></p>
<p>Hội Hướng dẫn viên Du lịch Việt Nam</p>
<p>Địa chỉ: Tầng 5, số 211 Giảng Võ, Quận Đống Đa, Hà Nội</p>
<p>Đề nghị ông/ bà đóng 500.000 VNĐ lệ phí gia nhập Hội và 500.000 VNĐ hội phí năm 2018 về tài khoản:</p>
<div style="margin-left: 20px;">
	<p>Hiệp hội Du lịch Việt Nam</p>
	<p>Số tài khoản: 0011004369919</p>
	<p>Ngân hàng: Sở Giao dịch-Ngân hàng Thương mại Cổ phần Ngoại thương Việt Nam( SGD-Vietcombank)</p>
</div>
<p><b>Nội dung thanh toán:</b>  đóng Lệ phí gia nhập và Hội phí năm 2018, số hồ sơ: {{ empty($objMember->file_code) ? '' : $objMember->file_code }}</p>
<p><b>Mọi thông tin chi tiết xin liên hệ:</b></p>
<p>Hội Hướng dẫn viên Du lịch Việt Nam ĐT: 024.37835120, 024.37835122</p>
