<p>Kính gửi ông bà {{ htmlentities($objMember->fullName) }},</p>
<p>Hồ sơ đăng ký gia nhập Hội Hướng dẫn viên Du lịch Việt Nam của ông/bà đã bị từ chối thẩm định: <a href="{{ UrlSigner::sign(url('/member/rejected/' . $objMember->id)) }}">(Chi tiết hồ sơ)</a></p>
<p>Lý do từ chối: {{ $objMember->notes->last()->note }}</p>
<p>Nếu cần trao đổi xin ông/ bà liên hệ:</p>
<p>Hội Hướng dẫn viên Du lịch Việt Nam: ĐT: 024.37835120, 37835122</p>

<p>Thư điện tử này được gửi ra từ hệ thống tự động. Vui lòng không trả lời thư điện tử này.</p>