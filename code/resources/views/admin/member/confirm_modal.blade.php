<!-- Modal form to delete a form -->
<div id="confirmModal" class="modal fade" role="dialog" tabindex='-1'>
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"></h3>
            </div>
            <div class="modal-body">
                <h4 class="text-center" id="msg_confirm"></h4>
                <br/>

                <div class="modal-footer">
                    <button id="confirm-save-button" class="submit_button btn btn-primary" tabindex="1"> Đồng ý</button>
                    <button id="confirm-cancel-button" class="submit_button btn btn-warning" tabindex="2">Thoát</button>
                </div>
            </div>
        </div>
    </div>
</div>