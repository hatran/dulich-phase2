<form class="page-wrap" name="filter" method="get" id="formSearch" action="{{ url ('/officesys/introductions') }}" style="margin-top: 10px;">
    {{ csrf_field() }}
    <div class="row" style="max-width:100%; width: 100%; margin-top: 10px;">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="line-height: 34px;">
           Tiêu đề bài viết
        </div>

        <div style="margin-bottom: 10px;" class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <input type="text" max="255" id="title" name="title" value="{{ array_get($response_view, 'title', '') }}" placeholder="Nhập từ khóa muốn tìm kiếm" tabindex="1">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="line-height: 34px;">
            Loại bài viết
        </div>
        <div style="" class="dropfield col-md-4 col-sm-6 col-xs-12" >
            <select name="option_code" id="option_code" tabindex="2">
                <option value="">Chọn mục muốn tìm kiếm</option>
                @foreach($options as $key => $value)
                <option value="{{ $value->code }}"
                        @if (array_get($response_view, 'option_code', '') == $value->code) selected="selected" @endif>{{ $value->value }}</option>
                @endforeach
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="3" id="search_status" name="search_status" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($statusOption))
                    @foreach($statusOption as $key => $val)
                        @php $selected = is_numeric(Input::get('search_status', '')) && Input::get('search_status', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="line-height: 34px;">
            Ngày tạo
        </div>

        <div style="margin-bottom: 10px;; margin-top: 10px" class="dropfield field-wrap col-md-4 col-sm-3 col-xs-12">
            <input type="text" placeholder="Ngày tạo" tabindex="4" value="{{ array_get($response_view, 'created_at', '') }}" id="created_at" name="created_at" class="date datetime-input" style="">
            <span class="datetime-icon fa fa-calendar"></span>
        </div>

        <div style="margin-left: 45%;" class="field-wrap col-md-2 col-sm-3 col-xs-2">
            <input type="submit" class="button btn-primary" name="search" value="Tìm kiếm" tabindex="5" style="width: 140px">
        </div>
    </div>
</form>
<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
</script>