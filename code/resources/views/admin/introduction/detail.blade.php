@extends('admin.layouts.app')
@section('title', 'Xem thông tin bài giới thiệu ')
@section('content')
    <style>
        .color-required{
            color: red
        }
        .error_border {
            border-color: #a94442;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        }
        .help-block strong {
            color: red
        }

        .expire_date{
            margin-bottom: 10px;
            padding-top: 7px;
            font-weight: bold;
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
        .col-md-6{
            padding-top: 7px
        }
        .col-md-1{
            width: 10%;
        }
    </style>
    <div class="page-wrap">
        <style>
            #closeButton {margin-top: 20px}
        </style>
        @include('admin.layouts.message')
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Thông tin chi tiết bài viết</div>

            <div class="panel-body">
                <form class="form-horizontal" method="post" action="" id="updateForm">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('option_code') ? ' has-error' : '' }}">
                        <label for="option_code" class="col-md-1 control-label" style="padding-right: 15px !important;">Loại giới thiệu</label>

                        <div class="col-md-6">

                        @foreach($arrOptions as $key => $v)
                            {{ ($objIntro->option_code == $v->code) ? $v->value : '' }}
                        @endforeach

                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-1 control-label">Tiêu đề</label>

                        <div class="col-md-6">
                            {{$objIntro->title}}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="start_time" class="col-md-1 control-label">Ngày bắt đầu</label>

                        <div class="col-md-6">
                            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" id="errorDate" style="margin-bottom: 10px;padding-left: 0px !important; width: 80px; padding-top: 1px">
                                {{$objIntro->start_time != '' ? (str_replace('-', '/', date('d-m-Y', strtotime($objIntro->start_time)))) : ''}}
                            </div>
                            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style=" padding-right: 0px !important; font-weight: bold; width: 130px;">
                                Ngày kết thúc
                            </div>
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" style=" padding-left: 0px !important; padding-top: 1px">
                                {{$objIntro->end_time != '' ? (str_replace('-', '/', date('d-m-Y', strtotime($objIntro->end_time)))) : ''}}
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status" class="col-md-1 control-label">Trạng thái</label>
                        <div class="col-md-6">
                            {{$objIntro->status == 0 ?  'Khóa' : 'Hoạt động'}}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-1 control-label">Nội dung</label>

                        <div class="col-md-10" style="padding-top: 7px; word-wrap: break-word;overflow-x: auto;max-height: 700px;line-height: 24px; ">
                            {{--<textarea  rows="15" id="content_rg" name="content" class="form-control position-content tinymce" >{{ empty(old('content')) ? htmlentities($objIntro->content) : old('content') }}</textarea>--}}
                            {!! ($objIntro->content) !!}
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5" style="padding-left: 100px">
                            <a href="{{ url('/officesys/introductions') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="8">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer_embed')
    <script>

        // Enter to submit form updateForm
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                $("#updateInfo").click();
            }
        });

        // Chuyển chuỗi kí tự (string) sang đối tượng Date()
        function parseDate(str) {
            var mdy = str.split('/');
            return new Date(mdy[2], mdy[1], mdy[0]);
        }
        $(document).on('click', '#updateInfo', function (event) {
            var title       = $("#title").val();
            var option_code   = $("#option_code").val();

            var start_time  = $("#start_time").val();
            var end_time    = $("#end_time").val();
            //var content     = $("#content_rg").val();
            console.log($("#tinymce").val());
            $(".help-block").remove();
            var flg = 0;
            if(option_code == ''){
                flg = 1;
                $("#option_code").addClass("error_border");
                $( '<span class="help-block"><strong>Loại giới thiệu là trường bắt buộc phải chọn</strong></span>' ).insertAfter( "#option_code" );
            }else{
                $("#option_code").removeClass("error_border");
            }
            if(title == ''){
                flg = 1;
                $("#title").addClass("error_border");
                $( '<span class="help-block"><strong>Tiêu đề là trường bắt buộc phải điền</strong></span>' ).insertAfter( "#title" );
            }else{
                $("#title").removeClass("error_border");
            }

            var d = new Date();
            var nowDate = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
            var startDate = parseDate(nowDate).getTime();
            if(start_time != '' && end_time == '') {
                //check format
                if(ValidateCustomDate(start_time)){
                    //validation
                    var startTime = parseDate(start_time).getTime();
                    if (startDate > startTime) {
                        flg = 1;
                        $("#start_time").addClass("error_border");
                        $( '<span class="help-block"><strong>Ngày bắt đầu phải lớn hơn ngày hiện tại</strong></span>' ).insertAfter( "#start_time" );
                    }else{
                        $("#start_time").removeClass("error_border");
                    }
                }else{
                    flg = 1;
                    $("#start_time").addClass("error_border");
                    $( '<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>' ).insertAfter( "#start_time" );
                }

            }

            if(end_time != '' && start_time == '') {
                if(ValidateCustomDate(end_time)) {
                    var endTime = parseDate(end_time).getTime();
                    if (startDate > endTime) {
                        flg = 1;
                        $("#end_time").addClass("error_border");
                        $( '<span class="help-block"><strong>Ngày kết thúc phải lớn hơn ngày hiện tại</strong></span>' ).insertAfter( "#end_time" );
                    }else{
                        $("#end_time").removeClass("error_border");
                    }
                }else{
                    flg = 1;
                    $("#end_time").addClass("error_border");
                    $('<span class="help-block"><strong>Định dạng của ngày kết thúc là dd/mm/yyyy</strong></span>').insertAfter("#end_time");
                }

            }

            if(end_time != '' && start_time != '') {
                if(ValidateCustomDate(end_time) && ValidateCustomDate(start_time)) {
                    //validation
                    var startTime = parseDate(start_time).getTime();
                    var endTime = parseDate(end_time).getTime();
                    if (startDate > startTime) {
                        flg = 1;
                        $("#start_time").addClass("error_border");
                        $( '<span class="help-block"><strong>Ngày bắt đầu phải lớn hơn ngày hiện tại</strong></span>' ).insertAfter( "#start_time" );
                    }else{
                        $("#start_time").removeClass("error_border");
                    }
                    if (startDate > endTime) {
                        flg = 1;
                        $("#end_time").addClass("error_border");
                        $('<span class="help-block"><strong>Ngày kết thúc phải lớn hơn ngày hiện tại</strong></span>').insertAfter("#end_time");
                    } else {
                        $("#end_time").removeClass("error_border");
                    }
                    if(flg == 0) {
                        if (startTime > endTime) {
                            flg = 1;
                            $("#end_time").addClass("error_border");
                            $("#start_time").addClass("error_border");
                            $('<span class="help-block"><strong>Ngày bắt đầu phải nhỏ hơn ngày kết thúc</strong></span>').insertAfter("#start_time");
                        } else {
                            $("#end_time").removeClass("error_border");
                            $("#start_time").removeClass("error_border");
                        }
                    }
                }else{
                    if(!ValidateCustomDate(end_time)) {
                        flg = 1;
                        $("#end_time").addClass("error_border");
                        $('<span class="help-block"><strong>Định dạng của ngày kết thúc là dd/mm/yyyy</strong></span>').insertAfter("#end_time");
                    }else{
                        $("#end_time").removeClass("error_border");
                    }
                    if(!ValidateCustomDate(start_time)) {
                        flg = 1;
                        $("#start_time").addClass("error_border");
                        $('<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>').insertAfter("#start_time");
                    }else{
                        $("#start_time").removeClass("error_border");
                    }
                }
            }

            if($('#content_rg_ifr').contents().find('#tinymce').html() == '<p><br data-mce-bogus="1"></p>'){
                flg = 1;
                $( '<span class="help-block"><strong>Nội dung là trường bắt buộc phải điền</strong></span>' ).insertBefore( "#mceu_15" );
            }

            if(flg == 0){
                $('input[name="_method"]').val('PUT')
                document.forms[0].submit();
                $("#updateForm").get(0).submit();
            }
        });

        function ValidateCustomDate(d) {
            var match = /^(\d{2})\/(\d{2})\/(\d{4})$/.exec(d);
            if (!match) {
                // pattern matching failed hence the date is syntactically incorrect
                return false;
            }
            var day     = parseInt(match[1], 10); // months are 0-11, not 1-12
            var month   = parseInt(match[2], 10) - 1;
            var year    = parseInt(match[3], 10);
            var date    = new Date(year, month, day);
            // now, Date() will happily accept invalid values and convert them to valid ones
            // therefore you should compare input month/day/year with generated month/day/year
            return date.getDate() == day && date.getMonth() == month && date.getFullYear() == year;
        }

        $(document).ready(function () {
            //document.getElementById("option_code").focus();
        });
    </script>
@endsection

