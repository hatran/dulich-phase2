@extends('admin.layouts.app')
@section('title', 'Thêm mới danh mục giới thiệu')
@section('content')
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .expire_date{
        margin-bottom: 10px;
        padding-top: 7px;
        font-weight: bold;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }
    .col-md-1{
        width: 10%;
    }
</style>
    <div class="page-wrap">
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Thêm mới danh mục giới thiệu</div>

            <div class="panel-body">
                <form class="form-horizontal" method="POST" action="{{ route('introductions.store') }}" id="registerForm">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('option_code') ? ' has-error' : '' }}">
                        <label for="option_code" class="col-md-1 control-label" style="padding-right: 0px !important;">Loại giới thiệu <span class="color-required">*</span></label>
                        <div class="col-md-6" style="">
                            <div class="col-md-5" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <select id="option_code" class="form-control" name="option_code" tabindex="1">
                                    <option value="">Chọn loại giới thiệu</option>
                                    @foreach($arrOptions as $key => $v)
                                        <option value="{{ $v->code }}" {{ (old('option_code') == $v->code) ? 'selected' : '' }}> {{ $v->value }} </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('option_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-1 control-label">Tiêu đề <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}"  tabindex="2" maxlength="150" >

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="start_time" class="col-md-1 control-label">Ngày bắt đầu<span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" id="errorDate" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <input type="text" style="" class="date datetime-input" name="start_time" value="{{ old('start_time') }}" id="start_time" placeholder="" tabindex="3">
                                <span class="datetime-icon fa fa-calendar"></span>
                            </div>
                            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12 expire_date" style="">
                                Ngày kết thúc
                            </div>
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" style="margin-bottom: 10px; padding-right: 0px !important; " >
                                <input type="text" style="" class="date datetime-input" name="end_time" value="{{ old('end_time') }}" id="end_time" placeholder="" tabindex="4">
                                <span class="datetime-icon fa fa-calendar" style="right: 10px !important;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="" class="col-md-1 control-label">Trạng thái <span class="color-required">*</span></label>

                        {{--<div class="col-md-6" style="padding-top: 5px">
                            <div class="field-wrap col-md-12 radio-list choices" style="padding-left: 0px !important;" id="statusCheck">
                                <div class="choice {{ old('status') == 1 ? 'chosen' : '' }}" style="padding-left: 25px !important;">
                                    <label for="statushd" tabindex="5">Hoạt động </label>
                                    <input id="statushd" type="radio" name="status" value="1" {{ old('status') == 1 ? 'checked' : '' }} >
                                </div>
                                <div class="choice {{ old('status') == 0 ? 'chosen' : '' }}">
                                    <label for="statusln" tabindex="6" >Khóa </label>
                                    <input id="statusln" type="radio" name="status" value="0" {{ old('status') == 0 ? 'checked' : '' }}>
                                </div>
                            </div>
                        </div>--}}


                        <div class="col-md-6" style="padding-top: 5px;">
                            <div class="" style="padding-left: 0px !important;" id="statusCheck">
                                <label class="radio-inline" for="statushd" tabindex="5">
                                    <input id="statushd" type="radio" name="status" value="1" {{ old('status') == 1 ? 'checked' : '' }}> Hoạt động
                                </label>
                                <label class="radio-inline" for="statusln" tabindex="6" >
                                    <input id="statusln" type="radio" name="status" value="0" {{ old('status') == 0 ? 'checked' : '' }}>
                                    Lưu nháp
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-1 control-label">Nội dung <span class="color-required">*</span></label>

                        <div class="col-md-10" style="padding-top: 10px">
                            <textarea  rows="15" id="content_rg" name="content" class="form-control position-content tinymce"></textarea>
                            @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button type="button" class="btn btn-primary" id="registerInfo" tabindex="7">
                                Lưu
                            </button>
                            <a href="{{ url('/officesys/introductions') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="8">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>

    // Enter to submit form registerForm
    document.body.addEventListener('keydown', function(e) {
        var key = e.which;
        if (key == 13) {
            $("#registerInfo").click();
        }
    });
    // Chuyển chuỗi kí tự (string) sang đối tượng Date()
    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[1], mdy[0]);
    }
    $(document).on('click', '#registerInfo', function (event) {
        var title       = $("#title").val();
        var option_code   = $("#option_code").val();
        var start_time  = $("#start_time").val();
        var end_time    = $("#end_time").val();

        $(".help-block").remove();
        $("#end_time").removeClass("error_border");
        $("#start_time").removeClass("error_border");
        var statusCheck = $("input:radio[name=status]:checked").val();
        var flg = 0;
        if(option_code == ''){
            flg = 1;
            $("#option_code").addClass("error_border");
            $( '<span class="help-block"><strong>Loại giới thiệu là trường bắt buộc phải chọn</strong></span>' ).insertAfter( "#option_code" );
        }else{
            $("#option_code").removeClass("error_border");
        }
        if(title.trim() == ''){
            flg = 1;
            $("#title").addClass("error_border");
            $( '<span class="help-block"><strong>Tiêu đề là trường bắt buộc phải điền</strong></span>' ).insertAfter( "#title" );
        }else{
            $("#title").removeClass("error_border");
        }
        /*else{
            var titleConver = convertStr(title);
            if(!(/^[a-zA-Z0-9\-\.\/]*$/i).test(titleConver)){
                flg = 1;
                $("#title").addClass("error_border");
                $( '<span class="help-block"><strong>Tiêu đề chỉ được nhập các ký tự a-z A-Z 0-9 -  . /</strong></span>' ).insertAfter( "#title" );//
            }else {
                $("#title").removeClass("error_border");
            }
        }*/
        if(statusCheck == 1){
            var d = new Date();
            var nowDate = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
            var startDate = parseDate(nowDate).getTime();

            if(start_time.trim() != '' && end_time.trim() == '') {
                //check format
                if(ValidateCustomDate(start_time)){
                    //validation
                    var startTime = parseDate(start_time).getTime();
                    if (startDate > startTime) {
                        flg = 1;
                        $("#start_time").addClass("error_border");
                        $( '<span class="help-block"><strong>Ngày bắt đầu không được nhỏ hơn ngày hiện tại</strong></span>' ).insertAfter( "#start_time" );
                    }else{
                        $("#start_time").removeClass("error_border");
                    }
                }else{
                    flg = 1;
                    $("#start_time").addClass("error_border");
                    $( '<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>' ).insertAfter( "#start_time" );
                }

            }

            if(start_time.trim() == '') {
                flg = 1;
                $("#start_time").addClass("error_border");
                $('<span class="help-block"><strong>Ngày bắt đầu là trường bắt buộc phải điền</strong></span>').insertAfter("#start_time");
            } else {
                $("#start_time").removeClass("error_border");
            }

            if(end_time.trim() != '' && start_time.trim() != '') {
                if(ValidateCustomDate(end_time) && ValidateCustomDate(start_time)) {

                    //validation
                    var startTime = parseDate(start_time).getTime();
                    var endTime = parseDate(end_time).getTime();

                    if (startDate > startTime) {
                        flg = 1;
                        $("#start_time").addClass("error_border");
                        $( '<span class="help-block"><strong>Ngày bắt đầu không được nhỏ hơn ngày hiện tại</strong></span>' ).insertAfter( "#start_time" );
                    }else{
                        $("#start_time").removeClass("error_border");
                    }
                    if (startDate > endTime) {
                        flg = 1;
                        $("#end_time").addClass("error_border");
                        $('<span class="help-block"><strong>Ngày kết thúc phải lớn hơn ngày hiện tại</strong></span>').insertAfter("#end_time");
                    } else {
                        $("#end_time").removeClass("error_border");
                    }
                    if(flg == 0){
                        if (startTime > endTime) {
                            flg = 1;
                            $("#end_time").addClass("error_border");
                            $("#start_time").addClass("error_border");
                            $('<span class="help-block"><strong>Ngày bắt đầu phải nhỏ hơn ngày kết thúc</strong></span>').insertAfter("#start_time");
                        } else {
                            $("#end_time").removeClass("error_border");
                            $("#start_time").removeClass("error_border");
                        }
                    }
                }else{
                    if(!ValidateCustomDate(end_time)) {
                        flg = 1;
                        $("#end_time").addClass("error_border");
                        $('<span class="help-block"><strong>Định dạng của ngày kết thúc là dd/mm/yyyy</strong></span>').insertAfter("#end_time");
                    }else{
                        $("#end_time").removeClass("error_border");
                    }
                    if(!ValidateCustomDate(start_time)) {
                        flg = 1;
                        $("#start_time").addClass("error_border");
                        $('<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>').insertAfter("#start_time");
                    }else{
                        $("#start_time").removeClass("error_border");
                    }
                }

            }
        }
        if($('#content_rg_ifr').contents().find('#tinymce').html() == '<p><br data-mce-bogus="1"></p>'){
            flg = 1;
            $( '<span class="help-block"><strong>Nội dung là trường bắt buộc phải điền</strong></span>' ).insertBefore( "#mceu_15" );
        }

        if(flg == 0){

            var url = "{{ url('/officesys/introductions') }}"
            //check status
            var idCheck = null;
            var statusCheck = $("input:radio[name=status]:checked").val();
            var optionCode = $("#option_code").val();
            if(statusCheck == 1){
                $.ajax({
                    url: url + "/check_status/" + idCheck +'/' + statusCheck +'/' + optionCode,
                    type: 'GET',
                    success: function (response) {
                        if(response.error == ''){
                            $("#registerForm").get(0).submit();
                        }else{
                            $( '<span class="help-block"><strong>'+response.content+'</strong></span>' ).insertAfter( "#statusCheck" );
                        }
                    }
                });
            }else{
                $("#registerForm").get(0).submit();
            }
        }
    });

    function ValidateCustomDate(d) {
        var match = /^(\d{2})\/(\d{2})\/(\d{4})$/.exec(d);
        if (!match) {
            // pattern matching failed hence the date is syntactically incorrect
            return false;
        }
        var day     = parseInt(match[1], 10); // months are 0-11, not 1-12
        var month   = parseInt(match[2], 10) - 1;
        var year    = parseInt(match[3], 10);
        var date    = new Date(year, month, day);
        // now, Date() will happily accept invalid values and convert them to valid ones
        // therefore you should compare input month/day/year with generated month/day/year
        return date.getDate() == day && date.getMonth() == month && date.getFullYear() == year;
    }
    function convertStr(txt){

        var str = txt;
        str= str.toLowerCase();
        str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str= str.replace(/đ/g,"d");
        str= str.replace(/\s/g, '');
        //str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
        //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
        //str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
        return str;
    }

    $(document).ready(function () {
        document.getElementById("option_code").focus();
    });
</script>
@endsection
