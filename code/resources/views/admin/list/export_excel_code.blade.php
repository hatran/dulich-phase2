@if($status == 'all')

<table>
    <tbody>
    <tr>
        <td colspan="9" style="text-align: center; font-weight: 900">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align: center; font-weight: 900">SỐ LƯỢNG HỒ SƠ ĐĂNG KÝ MỚI</td>
    </tr>
    @if($fromDate != '' || $toDate != '')
    <tr>
        <td colspan="9" style="text-align: center; font-weight: 900">Thời gian tạo hồ sơ từ ngày {{$fromDate != '' ? date('d/m/Y', strtotime($fromDate)) : '...'}} đến ngày {{$toDate != '' ? date('d/m/Y', strtotime($toDate)) : '...'}}</td>
    </tr>
    @else
    <tr>
        <td colspan="9" style="text-align: center; font-weight: 900">&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
    @endif
    </tbody>
</table>
<table>
    <tbody>
        <tr class="header">
            <td>STT</td>
            <td width="50">NỘI DUNG</td>
            <td width="10">SỐ LƯỢNG</td>
            <td width="50">CỤ THỂ TỪNG CHI HỘI</td>
            <td>&nbsp;</td>
        </tr>
        @if (!empty($arrData))
            @foreach ($arrData as $key => $item)
                @php
                $i = 0;
                @endphp

                @if (!empty($item['all']))
                    @foreach ($item['all'] as $k => $v)
                        @if($i == 0)
                            <tr class="main-content">
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['count'] }}</td>
                                <td>{{ $v['name'] }}</td>
                                <td>{{ $v['count'] }}</td>
                            </tr>
                        @else
                            <tr class="main-content">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td width="50">{{ $v['name'] }}</td>
                                <td width="10">{{ $v['count'] }}</td>
                            </tr>
                        @endif
                        @php
                        $i = $i + 1;
                        @endphp

                    @endforeach
                @else
                    <tr class="main-content">
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item['name'] }}</td>
                        <td>{{ $item['count'] }}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endif
            @endforeach
        @endif
    </tbody>
</table>

@else

<table>
    <tbody>
    <tr>
        <td colspan="4" style="text-align: center; font-weight: 900">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM </td>
    </tr>

    <tr>
        <td colspan="4" style="text-align: center; font-weight: 900">SỐ LƯỢNG HỒ SƠ ĐĂNG KÝ MỚI</td>
    </tr>

    <tr>
        <td colspan="4" style="text-align: center; font-weight: 900">CHI HỘI: {{$status}}</td>
    </tr>

    @if($fromDate != '' || $toDate != '')
    <tr>
        <td colspan="4" style="text-align: center; font-weight: 900">Thời gian tạo hồ sơ từ ngày {{$fromDate != '' ? date('d/m/Y', strtotime($fromDate)) : '...'}} đến ngày {{$toDate != '' ? date('d/m/Y', strtotime($toDate)) : '...'}}</td>
    </tr>
    @endif

    <tr>
        <td colspan="4" style="text-align: center; font-weight: 900">&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr class="header">
        <td >STT</td>
        <td>NỘI DUNG</td>
        <td>SỐ LƯỢNG</td>
    </tr>
    @if (!empty($arrData))
        @foreach ($arrData as $key => $item)
            <tr class="main-content">
                <td>{{ $key + 1 }}</td>
                <td>{{ $item['name'] }}</td>
                <td>{{ $item['count'] }}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>


@endif



<style>
    .header td {
        font-weight: bold;
        text-align: center;
        border: 1px solid #000;
    }
    .main-content td {
        border: 1px solid #000;
        text-align: center;
        vertical-align: middle;
    }
</style>
