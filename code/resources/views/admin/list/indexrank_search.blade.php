<?php

use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
?>
<style>
    body .field-wrap.dropfield:after {
        top: 10px;
    }
    body .field-wrap .datetime-icon {
        top: 10px;
        right: 35px;
    }
</style>
<form class="page-wrap" name="filter" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row">
        @if($step != 1 && $step != 2)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Mã số hồ sơ
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input id="file_code" type="text" name="file_code" value="{{ htmlentities(array_get($response_view, 'file_code', '')) }}" maxlength="6" tabindex="1">
            </div>
        @endif
        @if($step == 0 || $step == 5 || $step == 6)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Số thẻ hội viên
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input id="idMemberCode" type="text" name="member_code"
                       value="{{ htmlentities(array_get($response_view, 'member_code', '')) }}" maxlength="6" tabindex="2">
            </div>
        @endif
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số thẻ HDV
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="idNumber" type="text" name="touristGuideCode"
                   value="{{ htmlentities(array_get($response_view, 'touristGuideCode', '')) }}" maxlength="9" tabindex="3">
        </div>
         <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Họ tên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="name" type="text" name="fullName" value="{{ htmlentities(array_get($response_view, 'fullName', '')) }}" maxlength="50" tabindex="4">
        </div>
        @if($step != 6)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Trạng thái xếp hạng
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select id="rank_status" name="rank_status" tabindex="5">
                   
                    <option value=""></option>
                    @foreach($rankStatus as $key => $val)
                        <option value="{{ $key }}"
                                @if (array_get($response_view, 'rank_status', '') == $key) selected="selected" @endif>{{ $val }}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif
        @foreach($arrFile as $key => $file)
            <input type="hidden" name="status" value="{{$key}}" >
        @endforeach
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Văn phòng đại diện
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select class="select_vpdd" id="province_type" name="province_type" onchange="changeProvinceCode()" tabindex="6">
                @if (count(\App\Models\Branches::getBranchesNameByUserProvinceType(auth()->user()->province_type)) != 0)
                    <option value="" selected></option>
                    <option value="{{ auth()->user()->province_type }}" @if (array_get($response_view, 'province_type', '') == auth()->user()->province_type) selected="selected" @endif>{{ \App\Models\Branches::getBranchesNameByUserProvinceType(auth()->user()->province_type)[0] }}</option>
                @else
                    <option value="" selected></option>
                    @foreach($offices as $value)
                        <option value="{{ $value->id }}" @if (array_get($response_view, 'province_type', '') == $value->id) selected="selected" @endif>{{ $value->name }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        @if($step == 4)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12"></div>
            <div class="dropfield col-md-4 col-sm-6 col-xs-12"></div>
        @endif
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Nơi đăng ký sinh hoạt
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select class="province_code" id="province_code" name="province_code" tabindex="7">
                <option value="" selected></option>
               <!--  @foreach(\App\Models\Offices::whereNotNull('parent_id')->where('option_code','BRANCHES01')->where('status',1)->whereNull('deleted_at')->get() as $key => $val)
                    <option value="{{ $val->id }}" data-parent="{{$val->parent_id}}"
                            @if (array_get($response_view, 'province_code', '') == $val->id) selected="selected" @endif>{{ $val->name }}</option>
                @endforeach -->
            </select>
            <input type="hidden" id="province-code-selected" value="<?php echo array_get($response_view, 'province_code', ''); ?>">
            <input type="hidden" id="filter-by-status" value="<?php echo isset($statusFilter) ? $statusFilter : -1 ?>">
        </div>
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;">
            <input type="button" class="button btn-primary" style="width: 100px" name="search" id="search" onclick="submitForm();" value="Tìm kiếm" tabindex="8">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm hồ sơ</h4>
            </div>
            <div class="modal-body">
                @if($step == 3)
                    <h4 class="text-center">Ngày chứng từ không hợp lệ</h4>
                @else
                    <h4 class="text-center">Ngày tạo hồ sơ không hợp lệ</h4>
                @endif
                <br/>

                <div class="modal-footer">                   
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
    var isValild = true;
    $('#from_date').on('dp.change', function(e){ 
        fromDate = parseDate($(this).val());
        toDate = parseDate($('#to_date').val());
        if(fromDate > toDate)
        {
           isValild = false;
        } else {
            isValild = true;
        }
    });

    $('#to_date').on('dp.change', function(e){ 
        toDate = parseDate($(this).val());
        fromDate = parseDate($('#from_date').val());
        if(fromDate > toDate)
        {
           isValild = false;
        } else {
            isValild = true;
        }
    });
    function parseDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    function submitForm() { 
        if(isValild == false)
        {
           $("#showAlert").modal('show');
           return;
        }
        else {
            $('#formSearch').submit();
        }
    };
</script>
