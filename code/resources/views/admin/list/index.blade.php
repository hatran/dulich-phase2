<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\MemberPay;
use App\Providers\UserServiceProvider;
?>

@extends('admin.layouts.app')
@section('title', 'Danh sách hồ sơ')

@section('content')
    <div class="main-content">
        @include('admin.layouts.steps')
        <meta name="_token" content="{{ csrf_token() }}"/>
        <!-- <div class="heading">Danh sách hồ sơ</div> -->
        <div class="main-content">
            @include('admin.layouts.message')
            @if ($errors->has('error_message'))
                <div class="alert alert-danger">
                    <div>{{ $errors->first('error_message') }}</div>
                </div>
            @endif
            @if(\Illuminate\Support\Facades\Session::has('success_message'))
                <div class="alert alert-success">
                    <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
                </div>
                @endif
            @include('admin.layouts.search')
            <input id="exportHdd" type="hidden" value="{{ url('/officesys/list_members/export-excel') }}{{$query_string != '' ? '?'.$query_string : ''}}"/>
            <div class="total" style="margin-bottom: 15px">
                <a class="send-notification-expiration-date button btn-primary" style="float: right;width: 150px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)" title="Sắp hết hạn thẻ hội viên">Gửi thông báo</a>
                Tổng số : {{$count_members}}<input class="import_view-modal button btn-primary btn-function add-modal" id="" style="cursor: pointer;float:  right;width: 120px; height: 37px; line-height: 24px;" data-file-id="-1" value="Xuất danh sách" onclick="createExcel();"/>
                <br>
            </div>
            <div class="table-wrap">
                    <table>
                        <thead>
                        <tr>
                            <td width="3%">STT</td>
                            <td width="5%">Mã hồ sơ</td>
                            <td width="6%">Số thẻ <br/>HDV</td>
                            <td width="11%">Họ và tên</td>
                            <td width="7%">Số ĐT</td>
                            <td width="10%">Nơi đăng ký <br/> sinh hoạt</td>
                            <td width="5%">Số thẻ <br/>hội viên</td>
                            <td width="7%">Ngày tạo <br/> hồ sơ <br />(dd/mm/yyyy)</td>
                            <td width="7%">Hạn thẻ <br/>hội viên<br />(dd/mm/yyyy)</td>
                            <td width="7%">Hội viên VTGA từ<br />(dd/mm/yyyy)</td>
                            <td width="10%">Trạng thái <br/> hội viên</td>
							<!-- <td width="10%">Trạng thái <br/> in thẻ</td> -->
							<!-- <td width="10%">Hạng HDV</td> -->
                            <td width="10%"> Chức năng</td>
                            <td width="5%" style="">TB Nộp Phí<br>
                                <input type="checkbox" class="checkAll" name="member">
                                <label>&nbsp;</label>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($objMembers) == 0)
                            <tr>
                                <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                            </tr>
                        @else
                        @foreach($objMembers as $key => $objMember)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">

                                    {{ empty($objMember->file_code) ? '' : $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    @if (Route::currentRouteName() == 'admin_member_rank_view')
                                        <a href="{{ url('/officesys/member/detail/' . $objMember->id) }}">{{ $objMember->touristGuideCode }}</a>
                                    @else 
                                    <a href="{{ route('admin_list_member_detail_view',$objMember->id) }}">{{ $objMember->touristGuideCode }} </a>
                                    @endif
                                </td>
                                <td class="text-left wrap-text-name"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ $objMember->firstMobile }} </td>
                                <td class="text-left"> {{ array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '') }} </td>
                                <td class="text-center">
                                    {{ empty($objMember->member_code) ? '' : $objMember->member_code }}
                                </td>
                                <td class="text-center"> {{ empty($objMember->createdAt) ? '' : date('d/m/Y', strtotime($objMember->createdAt)) }} </td>
                                <td class="text-center"> {{ empty($objMember->member_code_expiration) ? '' : date('d/m/Y', strtotime($objMember->member_code_expiration)) }} </td>
                                <td class="text-center"> {{ empty($objMember->member_from) ? '' : date('d/m/Y', strtotime($objMember->member_from)) }} </td>
                                @if(empty($objMember->status))
                                    <td class="text-left wrap-text"></td>
                                @else
                                    <td class="text-left wrap-text">
                                        {{ array_get(MemberConstants::$file_const_display_status, $objMember->status, '') }}
                                    </td>
                                @endif
                                <!-- <td>
                                    @if (is_null($objMember->wCardType))
                                        @if ($objMember->pCardType == '1')
                                            {{ 'Đã in thẻ gia hạn' }}
                                        @elseif ($objMember->pCardType == '2')
                                            {{ 'Đã in thẻ cấp lại' }}
                                        @elseif ($objMember->pCardType == '0')
                                            {{ 'Đã in thẻ lần đầu' }}
                                        @endif
                                    @else
                                        @if ($objMember->wCardType == '1')
                                            {{ 'Chờ in thẻ gia hạn' }}
                                        @elseif ($objMember->wCardType == '2')
                                            {{ 'Chờ in thẻ cấp lại' }}
                                        @elseif ($objMember->wCardType == '0')
                                            {{ 'Chờ in thẻ lần đầu' }}
                                        @endif
                                    @endif
                                </td> -->

                                <td class="text-left" align="left">
                                    
									@if (Route::currentRouteName() == 'admin_member_rank_view')
                                        <a class="btn-function text-center" data-toggle="tooltip" data-placement="left" title="Xếp hạng HDV"
                                           href="{{ url('/officesys/member/detail/' . $objMember->id) }}">
                                            <span class="glyphicon glyphicon-check"  style="color: #0000ff; font-size: 18px;"></span>
                                        </a>
                                    @else 
                                        <a class="btn-function" data-toggle="tooltip" data-placement="left" title="Xem hồ sơ"
                                       href="{{ route('admin_list_member_detail_view',$objMember->id) }}" >
                                             <span class="glyphicon glyphicon-eye-open" style="color: green;"></span>
                                        </a>
                                        @if(!array_key_exists($objMember->status, MemberConstants::$file_download_const))
                                            @if((int) auth()->user()->role !== UserConstants::CONTENT_MANAGER_SUB_GROUP_USER)
                                                @php $hashMemberId = md5($objMember->id) @endphp
                                                <a href="{{ action('MemberController@downloadZipFolder', ['fileName' => [$hashMemberId . '_detail.pdf', $hashMemberId . '.pdf'], 'fileNameSaved' => $objMember->file_code]) }}"
                                                    class="btn-function"
                                                    target="_blank"
                                                    data-toggle="tooltip"
                                                    data-placement="left"
                                                    title="In hồ sơ">
                                                    <span class="glyphicon glyphicon-print"></span><!-- In hồ sơ -->
                                                </a>
                                                <a href="{{ action('MemberController@regenerateAndDownloadZipFolder', [
                                                    'memberId' => $objMember->id,
                                                    'fileCode' => $objMember->file_code
                                                ]) }}"
                                                   class="btn-function"
                                                   target="_blank"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Cho phép gen lại file hồ sơ và download xuống">
                                                    <span class="glyphicon glyphicon-download" style="color: green;"></span><!-- Cho phép gen lại file hồ sơ và download xuống -->
                                                </a>
                                            @endif
                                        @endif
                                        @if(\App\Providers\UserServiceProvider::isLeaderManager() || \App\Providers\UserServiceProvider::isLeaderRole() || \App\Providers\UserServiceProvider::isExpertRole() || \App\Providers\UserServiceProvider::isAccountant() || \App\Providers\UserServiceProvider::isMemberShipCardIssuer())
                                            <a href="{{ route('admin_member_profile_edit', ['id' => $objMember->id]) }}"
                                                class="update-profile-member-modal btn-function"
                                                data-toggle="tooltip"
                                                data-placement="left"
                                                title="Cập nhật thông tin hồ sơ hội viên"
                                                data-id="{{$objMember->id}}"
                                                data-status="{{$objMember->status}}"
                                                data-membercode="{{$objMember->member_code}}">
                                                <span class="glyphicon glyphicon-edit" style="color: #ff0000;"></span>
                                            </a>
                                        @endif
                                         
                                        @if(\App\Providers\UserServiceProvider::isLeaderManager())
                                            @if($objMember->status == \App\Constants\MemberConstants::UPDATE_INFO_2)
                                                <a class="btn-function"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Phê duyệt cập nhật hồ sơ"
                                                   href="{{ route('admin_member_detail_tmp_view' , $objMember->id) }}">
                                                    <span class="glyphicon glyphicon glyphicon-check" style="color: #0000ff;"></span>
                                                </a>
                                            @endif

                                            @if($objMember->status == \App\Constants\MemberConstants::MEMBER_STORED)
                                                <a href="javascript:void(0)"
                                                   class="save-storage-member-modal btn-function"
                                                   data-id="{{$objMember->id}}"
                                                   data-type=2
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Khôi phục hồ sơ lưu kho">
                                                    <span class="glyphicon glyphicon-floppy-saved" style="color: #0000ff;"></span>
                                                </a>
                                            @elseif(($objMember->status !=  \App\Constants\MemberConstants::MEMBER_OFFICIAL_MEMBER) && $objMember->status != \App\Constants\MemberConstants::UPDATE_INFO_2)
                                                <a href="javascript:void(0)" class="save-storage-member-modal btn-function"
                                                   data-id="{{$objMember->id}}"
                                                   data-isfee="{{$objMember->is_fee}}"
                                                   data-type=1
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Lưu kho hồ sơ">
                                                    <span class="glyphicon glyphicon-floppy-disk"  style="color: #ff6a00;"></span>
                                                </a>
                                            @endif

                                            @if($objMember->status == \App\Constants\MemberConstants::MEMBER_OFFICIAL_MEMBER || $objMember->status == \App\Constants\MemberConstants::UPDATE_INFO_2)
                                                <a href="javascript:void(0)"
                                                   class="save-storage-member-modal btn-function"
                                                   data-id="{{$objMember->id}}"
                                                   data-type=3
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Chấm dứt tư cách hội viên">
                                                    <span class="glyphicon glyphicon-ban-circle" style="color: #ff0000;"></span>
                                                </a>
                                                <a href="javascript:void(0)"
                                                   class="save-storage-member-modal btn-function"
                                                   data-id="{{$objMember->id}}"
                                                   data-type=4
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Tạm dừng tư cách hội viên">
                                                    <span class="glyphicon glyphicon-warning-sign" style="color: #ff6a00;"></span>
                                                </a>
                                            @elseif($objMember->status ==  \App\Constants\MemberConstants::MEMBER_PENDDING)
                                                <a href="javascript:void(0)"
                                                   class="save-storage-member-modal btn-function"
                                                   data-id="{{$objMember->id}}"
                                                   data-type=5
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Khội phục tư cách hội viên">
                                                    <span class="glyphicon glyphicon-user" style="color: #0000ff;"></span>
                                                </a>
                                            @endif
                                            
                                        @endif
                                        @if(\App\Providers\UserServiceProvider::isAdmin())
                                            @if(($objMember->status !=  \App\Constants\MemberConstants::MEMBER_OFFICIAL_MEMBER) && ($objMember->status != \App\Constants\MemberConstants::UPDATE_INFO_2))
                                                <a href="javascript:void(0)"
                                                   class="delete-member-modal btn-function"
                                                   data-toggle="tooltip"
                                                   data-placement="left"
                                                   title="Xóa hồ sơ"
                                                   data-id="{{$objMember->id}}"
                                                   data-status="{{$objMember->status}}"
                                                   data-membercode="{{$objMember->member_code}}">
                                                    <span class="glyphicon glyphicon-floppy-remove" style="color: #ff0000;"></span>
                                                </a>
                                            @endif
                                        @endif
                                    @endif
                                    @if ($objMember->status > 4 && auth()->user()->role != UserConstants::CONTENT_MANAGER_SUB_GROUP_USER)
                                    <a class="btn-function text-center show-modal-payment" data-toggle="tooltip" data-placement="left" title="Xem chi tiết hội phí"
                                           href="#"
                                           data-id="{{ $objMember->id }}?mpuid=null"
                                           data-status="{{ $objMember->status }}">
                                        <span class="glyphicon glyphicon-usd"  style="color: #0000ff; font-size: 14px;"></span>
                                    </a>
                                    @endif
                                    @if(($objMember->status == \App\Constants\MemberConstants::CODE_PROVIDING_WAITING ||  $objMember->status == \App\Constants\MemberConstants::MEMBER_OFFICIAL_MEMBER) && App\Providers\UserServiceProvider::isMemberShipCardIssuer())
                                        <a class="btn-function" href="{{ route('member_decision_do_create_card_view',[$objMember->id, 'is_print' => 1]) }}"
                                           target="_blank" data-toggle="tooltip" data-placement="left" title="In quyết định">
                                            <span class="glyphicon glyphicon-print" style="font-size: 16px; color: blue;"></span> 
                                        </a>
                                    @endif
									<a href="javascript:void(0)"
										class="sendMail-modal btn-function"
										data-toggle="tooltip"
										data-placement="left"
										title="Gửi lại email phê duyệt"
										data-id="{{$objMember->id}}"
										data-name="{{$objMember->fullName}}"
										data-url="{{ route('fakeSendMailApprove',$objMember->id) }}">
										<span class="glyphicon glyphicon-envelope" style="color: #039BE5;"></span>
									</a>
                                    @if($objMember->status ==  \App\Constants\MemberConstants::MEMBER_STORED || $objMember->status ==  \App\Constants\MemberConstants::MEMBER_DISTROY || $objMember->status ==  \App\Constants\MemberConstants::MEMBER_PENDDING)
                                        <a href="javascript:void(0)"
                                            class="save-not-member-modal btn-function"
                                            data-id="{{$objMember->id}}"
                                            onclick="openModalNote({{$objMember->id}})"
                                            title="Lý do" style="color: #0000ff;">
                                            <span class="glyphicon glyphicon-plus" style="color: #039BE5;"></span></span>
                                        </a>
                                    @endif
									<!--<a class="btn-function" href="{{ route('fakeSendMailApprove',$objMember->id) }}"
									   target="_blank" data-toggle="tooltip" data-placement="left" title="Gửi lại email phê duyệt">
										<span class="glyphicon glyphicon-envelope" style="font-size: 14px; color: #D81BG60;"></span> 
									</a>-->
                                </td>
                                <td class="text-center">
									<span class="checkitem">
										<input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{ $objMember->id }}" class="checkbox-pr">
										<label for="member {{ $key + 1 }}">&nbsp;</label>
									</span>
                                </td>
                            </tr>
                        @endforeach
                @endif
                        </tbody>
                    </table>
            </div>

            @include('admin.layouts.pagination')
        </div>
    </div>


    <!-- Modal form to delete a form -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="id_delete">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Bạn có chắn chắn muốn xóa hồ sơ của hội viên này?</h3>
                    <br/>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            Có
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Không
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- gửi email phê duyệt -->
	<div id="sendEmailModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="isSendMailModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center msgContent"></h4>
                    <br/>

                    <div class="modal-footer" id="sendmail-footer">
                        <button type="button" class="btn btn-danger resendMail" data-dismiss="modal">
                            Có
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Không
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- alert form-->
    <div id="notifyModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="id_delete">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="content"></h3>
                    <br/>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Đóng
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- alert form-->
    <div id="confirmModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="id_update">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="content"></h3>
                    <br/>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger savemember" data-dismiss="modal">
                            Có
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Không
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- note form-->
    <div id="noteModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="id_update">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lý do</h4>
                </div>
                <div class="modal-body">
                    <textarea name="note_content" class="form-control position-content" row="15" maxlength="1000" style="height: 150px"></textarea>
                    <br/>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger savenote" data-dismiss="modal">
                            Lưu
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Đóng
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.payment_yearly.detail')
@endsection
@section('footer_embed')
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 10px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
            margin: 3px;
        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }

        table {
            width: 100%;
        }

        tbody > tr > td {
            white-space: normal !important;
        }
    </style>

    <!-- Bootstrap JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- toastr notifications -->
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        function createExcel(){
            window.location = $("#exportHdd").val();
        }
        // in hồ sơ
        function openInNewTab(url) {
            var a = document.createElement("a");
            a.target = "_blank";
            a.href = url;
            a.click();
        }
        $("#printed").click(function() {
            $("#print_profile").click();
            $("#regist_profile").click();
        });

        // delete a member
        $(document).on('click', '.delete-member-modal', function () {
            $status = $(this).data('status');
            $member_code = $(this).data('membercode');
            if ($status == 13 && $member_code != null) {
                $('.modal-title').text('Thông báo');
                $('.content').text('Bạn không thể xóa hội viên này');
                $('#notifyModal').modal('show');
            } else {
                $('.modal-title').text('Xoá hồ sơ');
                $('#id_delete').val($(this).data('id'));
                $('#deleteModal').modal('show');
                id = $('#id_delete').val();
            }

        });

        // note a member
     
        var id_member_note;
        function openModalNote(id) {
            id_member_note = id;
            $.ajax({
                type: 'GET',
                url: 'list_members/note/' + id_member_note,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    if(data) {
                        $('textarea[name=note_content]').val(data.note || '')
                    } else {
                        $('textarea[name=note_content]').val('')
                    }
                }
            });
            $('#noteModal').modal('show');
        }

        $('.modal-footer').on('click', '.savenote', function () {
            $.ajax({
                type: 'POST',
                url: 'list_members/note/' + id_member_note,
                data: {
                    '_note': $('textarea[name=note_content]').val(),
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    console.log(data);
                }
            });
        });

		$('.modal-footer').on('click', '.delete', function () {
            $.ajax({
                type: 'POST',
                url: 'list_members/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    if (data.errors) {
                        toastr.warning(data.errors.memberInvalid, 'Thông báo', {timeOut: 2000});   
                    } else {
                        toastr.success('Xóa hồ sơ thành công!', 'Xóa hồ sơ', {timeOut: 1000});
                    }
                    
                    setTimeout(function () {// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 300);
                }
            });
        });

		// gửi lại email phê duyệt - sendMail-modal
		$(document).on('click', '.sendMail-modal', function () {
            $name = $(this).data('name');
            $memberId = $(this).data('id');
			$sendmailUrl = $(this).data('url');
            $('.modal-title').text('Xác nhận gửi lại email phê duyệt');
			$('.msgContent').text('Bạn chắc chắn gửi lại email phê duyệt cho hội viên: ' + $name);			
			$('#sendEmailModal').modal('show');
			
        });
		$('#sendmail-footer').on('click', '.resendMail', function () {
            
			$.ajax({
                type: 'POST',
                url: $sendmailUrl,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    if (data.errors) {
                        toastr.warning(data.errors.memberInvalid, 'Thông báo', {timeOut: 2000});   
                    } else {
                        toastr.success('Gửi lại email phê duyệt thành công!', 'Gửi email', {timeOut: 2000});
                    }
                }
            });
        });
		
        // Các chức năng khác
        $(document).on('click', '.save-storage-member-modal', function () {
            $isfee = $(this).data('isfee');
            $type = $(this).data('type');
            if ($type == 1) {
                /*if ($isfee == 1) {
                    $('.modal-title').text('Thông báo');
                    $('.content').text('Hội viên này đã đóng hội phí');
                    $('#notifyModal').modal('show');
                } else {*/
                    $('#id_update').val($(this).data('id'));
                    $('.modal-title').text('Lưu kho hồ sơ');
                    $('.content').text('Bạn có chắc chắn lưu kho hồ sơ này ? ');
                    $('#confirmModal').modal('show');
                    id = $('#id_update').val();
                /*}*/
            } else if ($type == 2) {
                $('#id_update').val($(this).data('id'));
                $('.modal-title').text('Khôi phục hồ sơ lưu kho');
                $('.content').text('Bạn có chắc chắn khôi phục hồ sơ lưu kho này ? ');
                $('#confirmModal').modal('show');
                id = $('#id_update').val();
            }
            else if ($type == 3) {
                $('#id_update').val($(this).data('id'));
                $('.modal-title').text('Chấm dứt tư cách hội viên');
                $('.content').text('Bạn có chắc chắn chấm dứt tư cách hội viên này ? ');
                $('#confirmModal').modal('show');
                id = $('#id_update').val();
            }
            else if ($type == 4) {
                $('#id_update').val($(this).data('id'));
                $('.modal-title').text('Tạm dừng tư cách hội viên');
                $('.content').text('Bạn có chắc chắn tạm dừng tư cách hội viên này ? ');
                $('#confirmModal').modal('show');
                id = $('#id_update').val();
            }
            else if ($type == 5) {
                $('#id_update').val($(this).data('id'));
                $('.modal-title').text('Khôi phục tư cách hội viên');
                $('.content').text('Bạn có chắc chắn khôi phục tư cách hội viên này? ');
                $('#confirmModal').modal('show');
                id = $('#id_update').val();
            }
        });

        $('.modal-footer').on('click', '.savemember', function () {            
			$.ajax({
                type: 'POST',
                url: 'list_members/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'type': $type,
                },
                success: function (data) {
                    if (data.errors) {
                        toastr.warning(data.errors.memberInvalid, 'Thông báo', {timeOut: 2000});   
                    } else {
                        if ($type == 1) {
                            toastr.success('Lưu kho hồ sơ thành công!', 'Lưu kho hồ sơ', {timeOut: 1000});
                        }
                        else if ($type == 2) {
                            toastr.success('Khôi phục hồ sơ thành công!', 'Khôi phục hồ sơ', {timeOut: 1000});
                        }
                        else if ($type == 3) {
                            toastr.success('Chấm dứt tư cách hội viên thành công!', 'Chấm dứt tư cách hội viên', {timeOut: 1000});
                        }
                        else if ($type == 4) {
                            toastr.success('Tạm dừng tư cách hội viên thành công!', 'Tạm dừng tư cách hội viên', {timeOut: 1000});
                        }
                        else if ($type == 5) {
                            toastr.success('Khôi phục tư cách hội viên thành công!', 'Khôi phục tư cách hội viên', {timeOut: 1000});
                        }
                    }
                    
                    setTimeout(function () {// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 300);
                }
            });
        });
        $( document ).ready(function() {
            document.getElementById("file_code").focus();
        });

        var urlPayment = "{{ url('/officesys/payment_yearly') }}";
        //print
        function checkedNames() {
            check = 0;
            $("input[name=member]:checked").each( function () {
                check = 1;
            });
            return check;
        }

        $(".send-notification-expiration-date").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('member_card_expiration_email') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".send-notification-expiration-date").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".send-notification-expiration-date").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $('body').on('change', '.checkAll', function() {
            $(".checkbox-pr").prop('checked',$(this).is(":checked"));
            if ($(this).is(":checked") == true) {
                $(".checkbox-pr").attr('data-check', 'yes');
            }
            else {
                $(".checkbox-pr").attr('data-check', 'no');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;

            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                    if ($(this).is(':checked')) {
                        anyChecked = true;
                    } else {
                        anyChecked = false;
                    }
                });
                if (anyChecked == true) {
                    __this.attr("data-check", "yes");

                    if (allRowLength == allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");

                __this.attr('data-check', 'no');
            }
        });
    </script>
    <script type="text/javascript" src="/admin/js/show_payment_modal.js"></script>
@endsection
