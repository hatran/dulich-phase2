@extends('admin.layouts.app')
@section('title', 'Báo cáo tổng hợp')

@section('content')
    <div class="main-content">
        <meta name="_token" content="{{ csrf_token() }}"/>
        <div class="main-content">
            @include('admin.layouts.searchexport')
            <input id="exportHdd" type="hidden" value="{{ url('/officesys/list_members/export_general_report') }}{{$query_string != '' ? '?'.$query_string : ''}}"/>
                Tổng số hồ sơ : {{$count_members}}<input readonly class="import_view-modal button btn-primary btn-function add-modal" id="" style="cursor: pointer;float:  right;width: 135px; height: 37px; line-height: 24px; padding: 6px 12px !important;" data-file-id="-1"  value="Xuất danh sách" onclick="createExcel();" />
                <br>
            </div>
            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="3%">STT</td>
                        <td width="5%">Mã hồ sơ</td>
                        <td width="6%">Số thẻ HDV</td>
                        <td width="5%">Số thẻ <br/>hội viên</td>
                        <td width="13%">Họ và tên</td>
                        <td width="7%">Hội viên VTGA từ</td>
                        <td width="7%">Ngày sinh</td>
                        <td width="7%">Số điện thoại</td>
                        <td width="7%">Email</td>
                        <td width="15%">Trạng thái</td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objMembers) == 0)
                        <tr>
                            <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">
                                    {{ empty($objMember->file_code) ? '' : $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('admin_list_member_detail_view',$objMember->id) }}">{{ $objMember->touristGuideCode }} </a>
                                </td>
                                <td class="text-center">

                                    {{ empty($objMember->member_code) ? '' : $objMember->member_code }}
                                </td>
                                <td class="text-left wrap-text-name"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->member_from) ? '' : date('d/m/Y', strtotime($objMember->member_from)) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-center"> {{ empty($objMember->firstMobile) ? '' : $objMember->firstMobile }} </td>
                                <td class="text-center"> {{ empty($objMember->firstEmail) ? '' : $objMember->firstEmail }} </td>
                                @if(empty($objMember->status))
                                    <td class="text-left wrap-text"></td>
                                @else
                                    <td class="text-left wrap-text">
                                        @if ($objMember->status == 13 && is_null($objMember->cardType))
                                            Hội viên chính thức
                                        @elseif (!is_null($objMember->cardType))
                                            @if ($objMember->cardType == 0)
                                                Hội viên in thẻ lần đầu
                                            @elseif ($objMember->cardType == 1)
                                                Hội viên in thẻ gia hạn
                                            @else
                                                Hội viên in thẻ cấp lại
                                            @endif
                                        @else
                                            {{ array_get(\App\Constants\MemberConstants::$file_const_display_status, $objMember->status, '') }}
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

            @include('admin.layouts.pagination')
        </div>
    </div>
@endsection
@section('footer_embed')
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 10px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
            margin: 3px;
        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }
    </style>

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        function createExcel(){
            window.location = $("#exportHdd").val();
        }
        // in hồ sơ
        function openInNewTab(url) {
            var a = document.createElement("a");
            a.target = "_blank";
            a.href = url;
            a.click();
        }
        $("#printed").click(function() {
            $("#print_profile").click();
            $("#regist_profile").click();
        });
    </script>
@endsection
