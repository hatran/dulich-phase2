<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\MemberPay;
use App\Providers\UserServiceProvider;
?>

@extends('admin.layouts.app')
@section('title', 'Danh sách hồ sơ')

@section('content')
    <div class="main-content">
        @include('admin.layouts.steps')
        <meta name="_token" content="{{ csrf_token() }}"/>
        <div class="main-content">
            @include('admin.layouts.message')
            @if ($errors->has('error_message'))
                <div class="alert alert-danger">
                    <div>{{ $errors->first('error_message') }}</div>
                </div>
            @endif
            @if(\Illuminate\Support\Facades\Session::has('success_message'))
                <div class="alert alert-success">
                    <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
                </div>
                @endif
            @include('admin.list.indexrank_search')
            <input id="exportHdd" type="hidden" value="{{ url('/officesys/list_members/export-excel') }}{{$query_string != '' ? '?'.$query_string : ''}}"/>
            <div class="total" style="margin-bottom: 15px">
                Tổng số : {{ $count_members }}
                <br>
            </div>
            <div class="table-wrap">
                    <table>
                        <thead>
                        <tr>
                            <td width="3%">STT</td>
                            <td width="5%">Mã hồ sơ</td>
                            <td width="6%">Số thẻ HDV</td>
                            <td width="5%">Số thẻ <br/>hội viên</td>
                            <td width="20%">Họ và tên</td>
                            <td width="7%">Ngày sinh</td>
                            <td width="7%">Ngày đăng ký  <br/> xếp hạng HDV</td>
                            <td width="7%">Ngày thi <br/> kiến thức</td>
                            <td width="7%">Điểm thi <br/> kiến thức</td>
                            <td width="7%">Điểm đánh giá  <br/>của Cty  <br/>Lữ hành</td>
                            <td width="7%">Điểm  <br/> hồ sơ</td>
                            <td width="7%">Điểm  <br/> giải thưởng</td>
                            <td width="15%">Trạng thái</td>
                            <td width="15%"> Chức năng</td>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($objMembers) == 0)
                            <tr>
                                <td colspan="14" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                            </tr>
                        @else
                        @foreach($objMembers as $key => $objMember)
                            
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">

                                    {{ empty($objMember->file_code) ? '' : $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    @if (Route::currentRouteName() == 'admin_member_rank_view')
                                        <a href="{{ url('/officesys/rank_members/detail/' . $objMember->id) }}">{{ $objMember->touristGuideCode }}</a>
                                    @else 
                                    <a href="{{ route('admin_list_member_detail_view',$objMember->id) }}">{{ $objMember->touristGuideCode }} </a>
                                    @endif
                                </td>
                                <td class="text-center">

                                    {{ empty($objMember->member_code) ? '' : $objMember->member_code }}
                                </td>
                                <td class="text-left wrap-text-name"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-center"> {{ empty($objMember->registerday) ? '' : date('d/m/Y', strtotime($objMember->registerday)) }} </td>
                                <td class="text-center"> {{ empty($objMember->join_date) ? '' : date('d/m/Y', strtotime($objMember->join_date)) }} </td>
                                <td class="text-center">  {{ empty($objMember->score) ? '0' : $objMember->score }} </td>
                                 <?php $score = 0; ?>
                                @foreach ($criteria as $key => $value)
                                    @if ($key == $objMember->criteria1_code)
                                        <?php $score += $objMember->ct1 * $value / 100; ?>
                                    @elseif ($key == $objMember->criteria2_code)
                                        <?php $score += $objMember->ct2 * $value / 100; ?>
                                    @elseif ($key == $objMember->criteria3_code)
                                        <?php $score += $objMember->ct3 * $value / 100; ?>
                                    @elseif ($key == $objMember->criteria4_code)
                                        <?php $score += $objMember->ct4 * $value / 100; ?>
                                    @elseif ($key == $objMember->criteria5_code)
                                        <?php $score += $objMember->ct5 * $value / 100; ?>
                                    @else
                                    @endif
                                @endforeach
                                <td class="text-center"> {{ round($score, 2) }} </td>
                                <td class="text-center"> {{ empty($objMember->score_file) ? '0' : $objMember->score_file }} </td>
                                <td class="text-center"> {{ empty($objMember->score_award) ? '0' : $objMember->score_award }} </td>
                                @if(empty($objMember->rank_status))
                                    <td class="text-left wrap-text">Chưa đăng ký</td>
                                @else
                                    <td class="text-left wrap-text">
                                        {{ array_get(MemberConstants::$rank_status, $objMember->rank_status, '') . ' ' . $ranksName[$objMember->mrrid] }}
                                    </td>
                                @endif


                                <td class="text-center" align="center">
                                    @if (Route::currentRouteName() == 'admin_member_rank_view' && !empty($objMember->mrid))
                                        <a class="btn-function text-center" title="Xem chi tiết điểm thi, điểm đánh giá của các công ty lữ hành"
                                           href="{{ url('/rankinfo?memberId=' . $objMember->id) }}">
                                            <span class="glyphicon glyphicon-eye-open"  style="color: green;"></span>
                                        </a>
                                        <a class="btn-function text-center" data-toggle="tooltip" data-placement="left" title="Xếp hạng HDV"
                                           href="{{ url('/officesys/member/detail/' . $objMember->id) }}">
                                            <span class="glyphicon glyphicon-check"  style="color: #0000ff;"></span>
                                        </a>
                                        <a href="#"
                                            class="btn-function edit-modal"
                                            data-toggle="tooltip"
                                            data-placement="left"
                                            title="Cập nhật điểm hồ sơ, điểm giải thưởng"
                                            data-rank-id="{{$objMember->mrrid}}"
                                            data-mem-id="{{$objMember->id}}">
                                            <span class="glyphicon glyphicon-plus" style="color: green;"></span>
                                        </a>
                                        <a href="javascript:void(0);"
                                            class="btn-function"
                                            data-toggle="tooltip"
                                            data-placement="left"
                                            title="Xóa đăng ký HDV"
                                            onclick="delete_intro({{ $objMember->mrid }})"
                                            data-id="{{$objMember->mrid}}">
                                            <span class="glyphicon glyphicon-trash" style="color: #ff0000;"></span>
                                        </a>
                                        <a href="{{ action('Admin\MemberRankController@downloadfile', ['memberId' => $objMember->id]) }}"
                                           class="btn-function"
                                           target="_blank"
                                           data-toggle="tooltip"
                                           data-placement="left"
                                           title="Xuất file báo cáo xếp hạng">
                                            <span class="glyphicon glyphicon-print"></span>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                           
                        @endforeach
                @endif
                        </tbody>
                    </table>
            </div>

            @include('admin.layouts.pagination')
        </div>
    </div>

    @include('admin.list.indexrank_edit')
    @include('admin.list.indexrank_delete')
@endsection
@section('footer_embed')
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 10px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
            margin: 3px;
        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }
    </style>

    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }
        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .sortable{
            color: #144a8b;
            font-weight: bold;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;
        }

        #closeButton ul {
            padding-left: 0px !important;
        }
        span.error-msg {
            color: #a94442;
        }
        span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }
        .total {
            border: 0 none;
            border-radius: 3px;
            color: #0000f0 !important;
            font-size: 16px;
            line-height: 30px;
            padding: 6px 10px;
        }

        #option_code{
            -moz-appearance: textfield;     height: 35px;     line-height: 35px;     padding-right: 30px;     position: relative;     width: 100%;border: 1px solid #bbbaba;border-radius: 2px;display: block;max-width: 100%; margin-bottom: 10px;padding-left: 10px;
        }
        .rq-star{
            color: red
        }
        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }

        .modal-title {
            margin: 0;
            line-height: 1.42857143;
            font-weight: bold;
            width: 100%;
            float: left;
            font-size: 20px;
        }
    </style>
    <script>
        function delete_intro(id) {
            jQuery("#deleteModal").modal();
            jQuery('#id_delete').val(id);
        }
        function deleteCat() {
            window.location = '/officesys/rank_members/delete/'+jQuery('#id_delete').val();
        }

        function update_intro(id) {
            jQuery("#updateModal").modal();
            jQuery('#id_update').val(id);
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        function clearErrorFields(postfix) {
            $('#scoreFile'+postfix).removeClass('error_border');
            $('#scoreAward'+postfix).removeClass('error_border');
            $('.errorScoreFile'+postfix).addClass('hidden');
            $('.errorScoreAward'+postfix).addClass('hidden');
        }

        var url = "{{ url('/officesys/rank_members') }}"
        function htmlEntities(str) {
            return str;
        }
        function showPopup(memberId, rankId, postfix) {
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';

            switch (postfix) {
                case '_edit':
                    title = 'Cập nhật điểm hồ sơ và điểm giải thưởng cho HDV';
                    $("#memberIdEdit").val(memberId);
                    $("#rankIdEdit").val(rankId);
                    canEdit = true;
                    break;

                default:
                    title = 'Chi tiết điểm hồ sơ và điểm giải thưởng cho HDV';
            }
            clearErrorFields(postfix);
            $('.modal-title'+postfix).text(title);

            if(postfix != '_add'){
                $.ajax({
                    url: url + "/fileaward/" + memberId + "/" + rankId,
                    type: 'GET',
                    success: function (response) {
                        if (response.deleted == 'error'){
                            document.location.reload(true);
                        } else {
                            $(modalId).modal('show');
                            if (canEdit === true) {
                                $('#scoreFile'+postfix).val(response.score_file == null ? '0' : response.score_file);
                                $('#scoreAward'+postfix).val(response.score_award == null ? '0' : response.score_award);
                                $('#name'+postfix).text(response.member_name);
                            } 
                            //focus number payment
                            if (postfix == '_edit') {
                                setTimeout(function () {
                                    $("#scoreFile"+postfix).focus();
                                }, 500);
                            }
                        }
                    }
                });
            }
        }

        function doAction(memberId, rankId, postfix) {
            var scoreFileAll = $("#scoreFile" + postfix);
            var scoreAwardAll = $("#scoreAward" + postfix);

            if (postfix == '_edit') {
                clearErrorFields(postfix)
            }
            var flg = 0;
            if (postfix == '_edit') {
                if (scoreFileAll.val().trim() == '') {
                    console.log(scoreFileAll.val());
                    $("#scoreFile"+postfix).addClass('error_border');
                    $(".errorScoreFile"+postfix).html('Điểm hồ sơ là trường bắt buộc phải điền');
                    $(".errorScoreFile"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    var scoreFileAll = scoreFileAll.val();
                    console.log(scoreFileAll);
                    if(!(/^[0-9]*$/i).test(scoreFileAll)){
                        flg = 1;
                        $("#scoreFile"+postfix).addClass('error_border');
                        $(".errorScoreFile"+postfix).html('Điểm hồ sơ chỉ được nhập ký tự số');
                        $(".errorScoreFile"+postfix).removeClass('hidden');
                    }else {
                        $(".errorScoreFile"+postfix).html('');
                        $(".errorScoreFile"+postfix).addClass('hidden');
                        $("#scoreFile"+postfix).removeClass('error_border');
                    }
                }

                if (scoreAwardAll.val().trim() == '') {
                    $("#scoreAward"+postfix).addClass('error_border');
                    $(".errorScoreAward"+postfix).html('Điểm giải thưởng là trường bắt buộc phải điền');
                    $(".errorScoreAward"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    var scoreAwardAll = scoreAwardAll.val();

                    if(!(/^[0-9]*$/i).test(scoreAwardAll)){
                        flg = 1;
                        $("#scoreAward"+postfix).addClass('error_border');
                        $(".errorScoreAward"+postfix).html('Điểm giải thưởng chỉ được nhập ký tự số');
                        $(".errorScoreAward"+postfix).removeClass('hidden');
                    }else {
                        $(".errorScoreAward"+postfix).html('');
                        $(".errorScoreAward"+postfix).addClass('hidden');
                        $("#scoreAward"+postfix).removeClass('error_border');
                    }
                }
            }

            if (flg == 0) {
                $(".errorScoreFile"+postfix).html('');
                $(".errorScoreAward"+postfix).addClass('hidden');
                $("#scoreFile"+postfix).removeClass('error_border');
                $("#scoreAward"+postfix).removeClass('error_border');
                $("#indexRank"+postfix).submit();
                var modalId = '#' + postfix + 'Modal';
                $(modalId).modal('hide');
            }
        }

        $('.modal-footer').on('click', '.edit', function () {
            var memberId = $('input[type=hidden]#dataMemberId').val();
            var rankId = $('input[type=hidden]#dataRankId').val();
            doAction(memberId, rankId, '_edit');
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            var dataMemberId = $(this).data('mem-id');
            var dataRankId = $(this).data('rank-id');
            $('input[type=hidden]#dataMemberId').val(dataMemberId);
            $('input[type=hidden]#dataRankId').val(dataRankId);
            showPopup(dataMemberId, dataRankId, '_edit');
        });

        $(document).ready(function () {
            var nameall = $("#file_code").val();
            $("#file_code").val('');
            setTimeout(function() {
                $('#file_code').focus() ;
                $("#file_code").val(nameall);
            }, 100);
        });
    </script>
@endsection
