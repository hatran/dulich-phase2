<div id="_editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_edit"></h4>
            </div>
            <form action="{{ route('admin_member_rank_update_score') }}" id="indexRank_edit" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <input type="hidden" name="member_id_edit" id="memberIdEdit" class="form-control" value=""/>
                <input type="hidden" name="rank_id_edit" id="rankIdEdit" class="form-control" value=""/>
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="name">Họ và tên</label>
                                    <div class="col-md-9" style="margin-top: 9px;">
                                        <span id="name_edit"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="scorefile_edit" class="col-md-3 control-label" style="padding-top: 7px">Điểm hồ sơ<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="scorefile_edit" id="scoreFile_edit" tabindex="100">
                                        <span class="errorScoreFile_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="scoreaward_edit" class="col-md-3 control-label" style="padding-top: 7px">Điểm giải thưởng<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="scoreaward_edit" id="scoreAward_edit" tabindex="201">
                                        <span class="errorScoreAward_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary edit" type="button" tabindex="202">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="203">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
