@extends('admin.layouts.app')
@section('title', 'Chi tiết thành viên')
@section('content')
    <?php
    $is_note = request()->query("is_note")
    ?>
    <div class="page-wrap">
        @include('admin.layouts.steps')
        <!-- <div class="heading">Chi tiết hồ sơ hội viên</div> -->
        <div class="block" style="margin-top: 10px;">
            <ul class="nav nav-tabs">
                <li class="{{ (!$is_note == 1) ? 'active' : '' }}"><a data-toggle="tab" href="#home" style="font-size: 20px;">Thông tin chung</a></li>
				<li><a data-toggle="tab" href="#menu2" style="font-size: 20px;">Thông tin hội viên</a></li>
                <li><a data-toggle="tab" href="#menu1" style="font-size: 20px;">Lịch sử tác động</a></li>
                <li class="{{ ($is_note == 1) ? 'active' : '' }}"><a data-toggle="tab" href="#menu3" style="font-size: 20px;">Ghi chú</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div id="home" class="tab-pane fade {{ (!$is_note == 1) ? 'in active' : '' }}">
                @include('admin.layouts.detail')
                @include('admin.layouts.editTouristGuideCode')
            </div>
			<div id="menu2" class="tab-pane fade">
                @include('admin.layouts.fileinfo')
            </div>
            <div id="menu1" class="tab-pane fade">
                @include('admin.layouts.history')
                @include('admin.layouts.payment')
				
            </div>
            <div id="menu3" class="tab-pane fade {{ ($is_note == 1) ? 'in active' : '' }}">
                @include('admin.layouts.member_note')
            </div>
        </div>
        <div class="form-btns container">
            <div class="row justify-content-center">                    
                <div class="col-md-4">
                    <input class="submit_button btn-warning" type="button" onclick="window.history.back()" value="Thoát">
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="member-id"/>
@endsection
<style>
    body .btn-view {
        margin-top: 15px;
    }
    .submit_button {
        background-color: #2b95cb;
        color: #fff;
        border: 0;
        text-transform: uppercase;
        height: 30px;
        line-height: 30px;
        padding: 0 15px;
        border-radius: 2px;
    }
    .help-block {
        color: #a94442;
        margin-top: 5px;
        margin-bottom: 10px;
    }

    body a:hover {
        color: #337ab7 !important;
    }

    .error_border {
        box-shadow: rgb(206, 63, 56) 0px 0px 10px;
    }

    .dialog-content > span {
        font-family: "Times New Roman", Times, serif;
        word-break: break-word;
    }

    .dialog-content > span.error-msg {
        color: #a94442;
    }
	.nav-tabs>li>a {
		border: 1px solid #ccc !important;
	}
</style>
@section('footer_embed')    
<script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
<script>
    $(document).on('click', '#btnEditTouristGuideCode', function (event) {
        event.preventDefault();
        var memberId = $(this).data('id');
        $('input[type=hidden]#member-id').val(memberId);
        showPopup(memberId);
    });  

    function clearErrorFields() {
        $('.errorTouristGuideType').addClass('hidden');
    }
   
    function showPopup(memberId) {
        var title;
        var postfix = '_edit';
        var modalId = '#' + postfix + 'Modal';
        title = 'Cập nhật số thẻ HDV du lịch';

        $('.modal-title').text(title);
        $('#name' + postfix).text('...');
        $('#identitycard' + postfix).text('...');
        $('#tourist_guide_type option').remove();
        $(modalId).modal('show');
        $(modalId).modal('show');
        $(modalId + ' .modal-dialog').loader('show');
        var url = "/officesys/list_members/get_member_for_list";
        $.ajax({
            url: url + "/" + memberId,
            type: 'GET',
            success: function (response) {
                $(modalId + ' .modal-dialog').loader('hide');
                var objMember = response.data;
                $('#expireddate' + postfix).val(objMember.expirationDateFormat);
                $('#name' + postfix).text(objMember.fullName);
                $('#identitycard' + postfix).text(objMember.cmtCccd);
                $('#touristcode' + postfix).val(objMember.touristGuideCode);
                localStorage.setItem('typeOfTravelGuide', JSON.stringify(objMember.typeOfTravelGuide));
                $.each(objMember.typeOfTravelGuideName, function (i, item) {
                    $('#tourist_guide_type').append($('<option>', { 
                        value: i,
                        text : item,
                        selected: i == objMember.typeOfTravelGuide ? true : false 
                    }));
                });
            }
        });
    }

    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[1], mdy[0]);
    }

    function doAction(memberId) {
        var action = 'POST';
        var url = "/officesys/list_members/updatetouristcode";
        var postfix = '_edit';
        var modalId = '#' + postfix + 'Modal';
        var data = {
            'typeOfTravelGuide': $('#tourist_guide_type').val(),
            'touristGuideCode':  $('#touristcode_edit').val(),
            'expirationDate' : $('#expireddate_edit').val()
        };
        var flg = 0;
        var tourist_guide_type = $("#tourist_guide_type");
        tourist_guide_type.removeClass('error_border');
        $('.errorTouristGuideType').addClass('hidden');

        var tourist_code = $("#touristcode_edit");
        tourist_code.removeClass('error_border');
        $('.errorTouristCode').addClass('hidden');

        var expired_date = $("#expireddate_edit");
        expired_date.removeClass('error_border');
        $('.errorExpiredDate').addClass('hidden');

        if (expired_date.val() == '') {
            $(".errorExpiredDate").html('Ngày hết hạn không được bỏ trống');
            $(".errorExpiredDate").removeClass('hidden');
            expired_date.addClass('error_border');
            flg = 1;
        } else {
            var date = new Date();
            var today = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
            today = parseDate(today).getTime();
            var paymentDate = parseDate(expired_date.val()).getTime();
            if (today > paymentDate) {
                setTimeout(function () {
                    $(".errorExpiredDate").html('Ngày hết hạn không được nhỏ hơn ngày hiện tại');
                    $(".errorExpiredDate").removeClass('hidden');
                    expired_date.addClass('error_border');
                }, 500);
                flg = 1;
            } else {
                $(".errorExpiredDate").addClass('hidden');
                $(".errorExpiredDate").html('');
                expired_date.removeClass('error_border');
            }
        }
        
        if (tourist_code.val() == '') {
            $(".errorTouristCode").html('Số thẻ HDV du lịch là trường bắt buộc');
            $(".errorTouristCode").removeClass('hidden');
            tourist_code.addClass('error_border');
            flg = 1;
        } else {
            var regexSt = /^([0-9])+$/;
            var regexNumber = /^\d{9}$/;
            if (!regexSt.test(tourist_code.val())) {
                $(".errorTouristCode").html('Số thẻ HDV du lịch phải là số');
                $(".errorTouristCode").removeClass('hidden');
                tourist_code.addClass('error_border');
                flg = 1;
            } else if(!regexNumber.test(tourist_code.val())) {
                $(".errorTouristCode").html('Số thẻ HDV du lịch phải là số có 9 chữ số');
                $(".errorTouristCode").removeClass('hidden');
                tourist_code.addClass('error_border');
                flg = 1;
            } else {
                $(".errorTouristCode").html('');
                $(".errorTouristCode").addClass('hidden');
                tourist_code.removeClass('error_border');
            }
        }
        
        if (flg == 0) {
            var type_old = JSON.parse(localStorage.getItem('typeOfTravelGuide'));
            var type_new = $("#tourist_guide_type").val();
            if (type_old == type_new) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: action,
                    url: url + '/' + memberId,
                    data: data,
                    success: function (data) {
                        if (data.error) {
                            toastr.error(data.error, 'Thông báo', {timeOut: 2000});
                        }
                        else {
                            toastr.success(data.success, 'Thông báo', {timeOut: 2000});
                        }
                        setTimeout(function () {
                            $(modalId).modal('hide');
                            document.location.reload(true);
                        }, 2000);
                        
                    }
                });
            }
            else {
                var ok = confirm('Hội viên cần cấp mới số thẻ hội viên. Bạn có chắc chắn thay đổi thông tin?');
                if (ok) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: action,
                        url: url + '/' + memberId,
                        data: data,
                        success: function (data) {
                            if (data.error) {
                                toastr.error(data.error, 'Thông báo', {timeOut: 5000});
                            }
                            else {
                                toastr.success(data.success, 'Thông báo', {timeOut: 2000});
                            }
                            setTimeout(function () {
                            $(modalId).modal('hide');
                                document.location.reload(true);
                            }, 2000);
                        }
                    });
                }
            }
        }
    }


    $('.modal-footer').on('click', '.edit', function () {
        var memberId = $('input[type=hidden]#member-id').val();
        doAction(memberId, '_edit');
    });
</script>
@endsection
