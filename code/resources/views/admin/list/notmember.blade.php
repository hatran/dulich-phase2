<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\MemberPay;
use App\Providers\UserServiceProvider;
?>

@extends('admin.layouts.app')
@section('title', 'Danh sách hồ sơ')

@section('content')
    <div class="main-content">
        @include('admin.layouts.steps')
        <meta name="_token" content="{{ csrf_token() }}"/>
        <!-- <div class="heading">Danh sách hồ sơ</div> -->
        <div class="main-content">
            @include('admin.layouts.message')
            @if ($errors->has('error_message'))
                <div class="alert alert-danger">
                    <div>{{ $errors->first('error_message') }}</div>
                </div>
            @endif
            @if(\Illuminate\Support\Facades\Session::has('success_message'))
                <div class="alert alert-success">
                    <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
                </div>
                @endif
            @include('admin.layouts.searchnotmember')
            <div class="total" style="margin-bottom: 15px">
                Tổng số : {{$count_members}}
                <a class="resend-notification-sms button btn-primary" style="float:  right;width: 100px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Gửi SMS</a>
                <a class="resend-notification-email button btn-primary" style="float:  right;width: 100px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Gửi Email</a>
                <br>
            </div>
            <div class="table-wrap">
                    <table>
                        <thead>
                        <tr>
                            <td width="5%">STT</td>
                            <td width="10%">Số thẻ HDV</td>
                            <td width="20%">Họ và tên</td>
                            <td width="10%">Ngày sinh</td>
                            <td width="15%">Số điện thoại</td>
                            <td width="15%">Email</td>
                            <td width="10%"> Chức năng</td>
                            <td width="5%" style="position: relative; top:0px; color: #144a8b;">TB Mời Gia Nhập Hội<br>
                                <input type="checkbox" class="checkAll">
                                <label>&nbsp;</label>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($objMembers) == 0)
                            <tr>
                                <td colspan="6" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                            </tr>
                        @else
                        @foreach($objMembers as $key => $objMember)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">{{ empty($objMember->touristGuideCode) ? '' : $objMember->touristGuideCode }}</td>
                                <td class="text-left wrap-text-name"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-center"> {{ $objMember->firstMobile }}</td>
                                <td class="text-left"> {{ $objMember->firstEmail }}</td>
                                <td class="text-center" align="center">
                                        @if(\App\Providers\UserServiceProvider::isLeaderManager() || \App\Providers\UserServiceProvider::isLeaderRole() || \App\Providers\UserServiceProvider::isExpertRole())
                                            <a href="{{ route('admin_member_profile_edit', ['id' => $objMember->id]) }}"
                                                class="update-profile-member-modal btn-function"
                                                data-toggle="tooltip"
                                                data-placement="left"
                                                title="Cập nhật hồ sơ thông tin"
                                                data-id="{{$objMember->id}}"
                                                data-status="{{$objMember->status}}"
                                                data-membercode="{{$objMember->member_code}}">
                                                <span class="glyphicon glyphicon-edit" style="color: #ff0000;"></span>
                                            </a>
                                        @endif
                                </td>
                                <td class="text-center">
                                    <span class="checkitem">
                                        <input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{ $objMember->id }}" class="checkbox-pr">
                                        <label for="member {{ $key + 1 }}">&nbsp;</label>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                @endif
                        </tbody>
                    </table>
            </div>

            @include('admin.layouts.pagination')
        </div>
    </div>
@endsection
@section('footer_embed')
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 10px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
            margin: 3px;
        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }
    </style>

    <!-- Bootstrap JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script type="text/javascript">
        $('body').on('change', '.checkAll', function() {
            $(".checkbox-pr").prop('checked',$(this).is(":checked"));
            if ($(this).is(":checked") == true) {
                $(".checkbox-pr").attr('data-check', 'yes');
            }
            else {
                $(".checkbox-pr").attr('data-check', 'no');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;
            
            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                    if ($(this).is(':checked')){
                        anyChecked = true;
                    } else {
                        anyChecked = false;
                    }
                });
                if (anyChecked == true) {
                    __this.attr("data-check", "yes");
                    
                    if (allRowLength == allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }   
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");
                
                __this.attr('data-check', 'no');
            }
        });

        function checkedNames() {
            check = 0;
            $("input[name=member]:checked").each( function () {
                check = 1;
            });
            return check;
        }

        $(".resend-notification-email").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('admin_list_member_send_notification_email_to_not_member') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        $(".resend-notification-sms").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('admin_list_member_send_notification_sms_to_not_member') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });
    </script>
@endsection
