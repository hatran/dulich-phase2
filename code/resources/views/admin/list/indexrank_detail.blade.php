<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
use App\Constants\OtherConstants;
?>

@extends('admin.layouts.app')
@section('title', 'Chi tiết thành viên')
@section('content')
    <div class="page-wrap">
        @include('admin.layouts.steps')
        @include('admin.layouts.indexrank_detail')
        <div class="block" style="margin-top:  10px; padding-bottom: 10px;">
           
                <div class="form-btns container">
                    <div class="row justify-content-center">
                        <button class="submit_button btn btn-warning" onclick="window.history.back();" type="button">Thoát</button>
                    </div>
                </div>
                <?php if (session()->has('memberInvalid')) {
                ?>
                    <span id="invalid-error" style="color: red;">{{ session()->get('memberInvalid') }}</span>
                <?php
                } ?>

           
        </div>
    </div>
@endsection
<style>
    body .btn-view {
        margin-top: 15px;
    }
    .submit_button {
        background-color: #2b95cb;
        color: #fff;
        border: 0;
        text-transform: uppercase;
        height: 30px;
        line-height: 30px;
        padding: 0 15px;
        border-radius: 2px;
    }
    .help-block {
        color: #a94442;
        margin-top: 5px;
        margin-bottom: 10px;
    }
</style>

