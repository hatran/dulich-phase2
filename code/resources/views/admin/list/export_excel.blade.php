<?php
    use App\Constants\MemberConstants;
?>
<table>
    <tbody>
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">DANH SÁCH HỒ SƠ</td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
    <tr class="header">
        <td>TT</td>
        <td>Số thẻ HDV</td>
        <td>Mã số hồ sơ</td>
        <td>Số thẻ hội viên</td>
        <td>Họ và tên</td>
        <td>Hội viên VTGA từ</td>
        <td>Hạn thẻ hội viên</td>
        <td>ĐTDĐ</td>
        <td>Email</td>
        <td>Trạng thái</td>
        <td>Chi hội HDV</td>
    </tr>
    @if (!empty($objMembers))
        @foreach ($objMembers as $key => $item)
            @php
            $mobile = $item->firstMobile . (!empty($item->secondMobile) ? ',' . $item->secondMobile : '' );
            $emailS = $item->firstEmail . (!empty($item->secondEmail) ? ','. $item->secondEmail : '' );
            @endphp
            <tr class="main-content">
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->touristGuideCode }}</td>
                <td>{{ empty($item->file_code) ? '' : $item->file_code }}</td>
                <td>{{ empty($item->member_code) ? '' : $item->member_code }}</td>
                <td>{{ htmlentities($item->fullName) }}</td>
                <td>{{ !empty($item->member_from) ? date('d/m/Y', strtotime($item->member_from)) : '' }}</td>
                <td>{{ !empty($item->member_code_expiration) ? date('d/m/Y', strtotime($item->member_code_expiration)) : '' }}</td>
                <td>{{ $mobile }}</td>
                <td>{{ $emailS }}</td>
                <td>
                    @if ($item->status == 13)
                        @if ($status == 13 && empty($print_status))
                            {{ 'Hội viên chính thức' }}
                        @else
                            @if (is_null($item->wCardType))
                                @if ($item->pCardType == '1')
                                    {{ 'Đã in thẻ gia hạn' }}
                                @elseif ($item->pCardType == '2')
                                    {{ 'Đã in thẻ cấp lại' }}
                                @elseif ($item->pCardType == '0')
                                    {{ 'Đã in thẻ lần đầu' }}
                                @endif
                            @else
                                @if ($item->wCardType == '1')
                                    {{ 'Chờ in thẻ gia hạn' }}
                                @elseif ($item->wCardType == '2')
                                    {{ 'Chờ in thẻ cấp lại' }}
                                @elseif ($item->wCardType == '0')
                                    {{ 'Chờ in thẻ lần đầu' }}
                                @endif
                            @endif
                        @endif
                    @else
                        {{ array_get(MemberConstants::$file_const_display_status, $item->status, '') }}
                    @endif
                </td>
                <td>{{ !empty($item->typeOfTravelGuide) ? $tourist_guide_type[$item->typeOfTravelGuide] : '' }}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<style>
    .header td {
        font-weight: bold;
        text-align: center;
        border: 1px solid #000;
    }
    .main-content td {
        border: 1px solid #000;
    }
</style>
