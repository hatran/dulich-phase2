<table>
    <tbody>
    <tr>
        <td colspan="13" style="text-align: center; font-weight: 900">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</td>
    </tr>
    <tr>
        <td colspan="13" style="text-align: center; font-weight: 900">
            @if ($check == 0) TOÀN QUỐC
            @elseif ($check == 1) {{ $provinceCode }}
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="13" style="text-align: center; font-weight: 900">&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
    </tbody>
</table>

<table>
    <thead>
    <tr class="header">
        <td>STT</td>
        <td>Mã hồ sơ</td>
        <td>Số thẻ HDV</td>
        <td>Số thẻ <br/>hội viên</td>
        <td>Họ và tên</td>
        <td>Hội viên VTGA từ</td>
        <td>Ngày sinh</td>
        <td>Số điện thoại</td>
        <td>Email</td>
        <td>Địa chỉ liên hệ</td>
        <td>Nơi đăng ký sinh hoạt</td>
        <td>Hạng HDV</td>
        <td>Trạng thái</td>
    </tr>
    </thead>
    <tbody>
    @if (count($objMembers) == 0)
        <tr class="main-content">
            <td colspan="13" class="text-center"><p>Không có bản ghi</p></td>
        </tr>
    @else
        @foreach($objMembers as $key => $objMember)
            <tr class="main-content">
                <td class="text-center"> {{ $key + 1 }} </td>
                <td class="text-center">
                    {{ empty($objMember->file_code) ? '' : $objMember->file_code }}
                </td>
                <td class="text-center">
                    {{ $objMember->touristGuideCode }}
                </td>
                <td class="text-center">
                    {{ empty($objMember->member_code) ? '' : $objMember->member_code }}
                </td>
                <td class="text-left wrap-text-name"> {{ htmlentities($objMember->fullName) }} </td>
                <td class="text-center"> {{ empty($objMember->member_from) ? '' : date('d/m/Y', strtotime($objMember->member_from)) }} </td>
                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                <td class="text-center"> {{ empty($objMember->firstMobile) ? '' : $objMember->firstMobile }} </td>
                <td class="text-center"> {{ empty($objMember->firstEmail) ? '' : $objMember->firstEmail }} </td>
                <td class="text-center"> {{ empty($objMember->address) ? '' : $objMember->address }} </td>
                <td class="text-center"> {{ empty($objMember->province_code) ? '' : array_get(\App\Models\Branches::getBranches01(), $objMember->province_code, '') }} </td>
                <td class="text-center">
                    @if ($objMember->rank_status == 4)
                        @if (! empty ($memberRank[$objMember->id]))
                            <?php $rankId = $memberRank[$objMember->id]; ?>
                            @if (! empty($ranks[$rankId]))
                                {{ $ranks[$rankId] }}
                            @endif
                        @endif
                    @endif
                </td>
                @if(empty($objMember->status))
                    <td class="text-left wrap-text"></td>
                @else
                    <td class="text-left wrap-text">
                        @if ($objMember->status == 13 && is_null($objMember->cardType))
                            Hội viên chính thức
                        @elseif (!is_null($objMember->cardType))
                            @if ($objMember->cardType == 0)
                                Hội viên in thẻ lần đầu
                            @elseif ($objMember->cardType == 1)
                                Hội viên in thẻ gia hạn
                            @else
                                Hội viên in thẻ cấp lại
                            @endif
                        @else
                            {{ array_get(\App\Constants\MemberConstants::$file_const_display_status, $objMember->status, '') }}
                        @endif
                    </td>
                @endif
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<style>
    .header td {
        font-weight: bold;
        text-align: center;
        border: 1px solid #000;
    }
    .main-content td {
        border: 1px solid #000;
    }
</style>
