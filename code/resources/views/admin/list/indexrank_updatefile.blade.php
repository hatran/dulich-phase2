<style>
    .bar {
        height: 18px;
        background: green;
    }
</style>

<!-- Modal -->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateModalLabel">Xác nhận đã nhận đủ hồ sơ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Bạn chắc chắn muốn cập nhật trạng thái HDV<span id="id-em" style="font-size:  20px;color:  blue;"></span> đủ điều kiện tham gia xếp hạng HDV?</h4>
                <br/>
            </div>
            <input type="hidden" id="id_update" value=""/>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateCat();">Cập nhật</button>
                <button type="button" class="btn btn-warning"
                        data-dismiss="modal">Thoát</button>
            </div>
        </div>
    </div>
</div>