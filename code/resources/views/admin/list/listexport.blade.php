<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\MemberPay;
use App\Providers\UserServiceProvider;
?>

@extends('admin.layouts.app')
@section('title', 'Danh sách hồ sơ')

@section('content')
    <div class="main-content">
        <meta name="_token" content="{{ csrf_token() }}"/>
        <!-- <div class="heading">Danh sách hồ sơ</div> -->
        <div class="main-content">
            @if ($errors->has('error_message'))
                <div class="alert alert-danger">
                    <div>{{ $errors->first('error_message') }}</div>
                </div>
            @endif
            @if(\Illuminate\Support\Facades\Session::has('success_message'))
                <div class="alert alert-success">
                    <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
                </div>
                @endif
            @include('admin.layouts.searchexport')
            <input id="exportHdd" type="hidden" value="{{ url('/officesys/list_members/export-excel-code') }}{{$query_string != '' ? '?'.$query_string : ''}}"/>
            <div class="total" style="margin-bottom: 15px">
                Tổng số hồ sơ : {{$count_members}}<input class="import_view-modal button btn-primary btn-function add-modal" id="" style="cursor: pointer;float:  right;width: 135px; height: 37px; line-height: 24px; padding: 6px 12px !important;" data-file-id="-1"  value="Xuất danh sách" onclick="createExcel();" />
                <br>
            </div>
            <div class="table-wrap">
                    <table>
                        <thead>
                        <tr>
                            <td width="3%">STT</td>
                            <td width="5%">Mã hồ sơ</td>
                            <td width="6%">Số thẻ HDV</td>
                            <td width="5%">Số thẻ <br/>hội viên</td>
                            <td width="20%">Họ và tên</td>
                            <td width="7%">Ngày sinh</td>
                            <td width="7%">Ngày tạo <br/> hồ sơ</td>
                            <td width="7%">Ngày <br/> thẩm định</td>
                            <td width="7%">Ngày <br/>phê duyệt</td>
                            <td width="10%">Loại HDV</td>
                            <td width="15%">Trạng thái</td>
                            {{--<td width="15%"> Chức năng</td>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($objMembers) == 0)
                            <tr>
                                <td colspan="12" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                            </tr>
                        @else
                        @foreach($objMembers as $key => $objMember)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">

                                    {{ empty($objMember->file_code) ? '' : $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('admin_list_member_detail_view',$objMember->id) }}">{{ $objMember->touristGuideCode }} </a>
                                </td>
                                <td class="text-center">

                                    {{ empty($objMember->member_code) ? '' : $objMember->member_code }}
                                </td>
                                <td class="text-left wrap-text-name"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-center"> {{ empty($objMember->createdAt) ? '' : date('d/m/Y', strtotime($objMember->createdAt)) }} </td>
                                <td class="text-center"> {{ empty($objMember->name) ? '' : $objMember->name }} </td>
                                <td class="text-center"> {{ empty($objMember->approved_at) ? '' : date('d/m/Y', strtotime($objMember->approved_at)) }} </td>
                               <td class="text-left"> {{ !empty($objMember->typeOfTravelGuide) ? $tourist_guide_type[$objMember->typeOfTravelGuide] : '' }} </td>
                                @if(empty($objMember->status))
                                    <td class="text-left wrap-text"></td>
                                @else
                                    <td class="text-left wrap-text">
                                        {{ array_get(MemberConstants::$file_const_display_status, $objMember->status, '') }}
                                    </td>
                                @endif


                                {{--<td class="text-center" align="center">

                                    <a class="btn-function" data-toggle="tooltip" data-placement="left" title="Xem hồ sơ"
                                       href="{{ route('admin_list_member_detail_view',$objMember->id) }}" >
                                         <span class="glyphicon glyphicon-eye-open" style="color: green;"></span>
                                    </a>
                                    @if(!in_array($objMember->status, array_keys(MemberConstants::$file_download_const)))
                                        <a href="{{ action('MemberController@downloadZipFolder', ['fileName' => [md5($objMember->id) . '_detail.pdf', md5($objMember->id) . '.pdf'], 'fileNameSaved' => $objMember->file_code]) }}"
                                        class="btn-function" target="_blank" data-toggle="tooltip" data-placement="left" title="In hồ sơ">  <span class="glyphicon glyphicon-print"></span><!-- In hồ sơ --></a>
                                    @endif
                                    
                                </td>--}}
                            </tr>
                        @endforeach
                @endif
                        </tbody>
                    </table>
            </div>

            @include('admin.layouts.pagination')
        </div>
    </div>


    <!-- Modal form to delete a form -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="id_delete">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">Bạn có chắn chắn muốn xóa hồ sơ của hội viên này?</h3>
                    <br/>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            Có
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Không
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- alert form-->
    <div id="notifyModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="id_delete">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="content"></h3>
                    <br/>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Đóng
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- alert form-->
    <div id="confirmModal" class="modal fade" role="dialog">
        <div class="modal-dialog" id="id_update">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3 class="content"></h3>
                    <br/>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger savemember" data-dismiss="modal">
                            Có
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            Không
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_embed')
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 10px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
            margin: 3px;
        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }
    </style>

    <!-- Bootstrap JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- toastr notifications -->
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        function createExcel(){
            window.location = $("#exportHdd").val();
        }
        // in hồ sơ
        function openInNewTab(url) {
            var a = document.createElement("a");
            a.target = "_blank";
            a.href = url;
            a.click();
        }
        $("#printed").click(function() {
            $("#print_profile").click();
            $("#regist_profile").click();
        });

        // delete a member
        $(document).on('click', '.delete-member-modal', function () {
            $status = $(this).data('status');
            $member_code = $(this).data('membercode');
            if ($status == 13 && $member_code != null) {
                $('.modal-title').text('Thông báo');
                $('.content').text('Bạn không thể xóa hội viên này');
                $('#notifyModal').modal('show');
            } else {
                $('.modal-title').text('Xoá hồ sơ');
                $('#id_delete').val($(this).data('id'));
                $('#deleteModal').modal('show');
                id = $('#id_delete').val();
            }

        });
        $('.modal-footer').on('click', '.delete', function () {
            $.ajax({
                type: 'DELETE',
                url: 'list_members/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    if (data.errors) {
                        toastr.warning(data.errors.memberInvalid, 'Thông báo', {timeOut: 2000});   
                    } else {
                        toastr.success('Xóa hồ sơ thành công!', 'Xóa hồ sơ', {timeOut: 1000});
                    }
                    
                    setTimeout(function () {// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 300);
                }
            });
        });


        $(document).on('click', '.save-storage-member-modal', function () {
            $isfee = $(this).data('isfee');
            $type = $(this).data('type');
            if ($type == 1) {
                /*if ($isfee == 1) {
                    $('.modal-title').text('Thông báo');
                    $('.content').text('Hội viên này đã đóng hội phí');
                    $('#notifyModal').modal('show');
                } else {*/
                    $('#id_update').val($(this).data('id'));
                    $('.modal-title').text('Lưu kho ');
                    $('.content').text('Bạn có muốn lưu kho hội viên này ? ');
                    $('#confirmModal').modal('show');
                    id = $('#id_update').val();
                /*}*/
            } else if ($type == 2) {
                $('#id_update').val($(this).data('id'));
                $('.modal-title').text('Khôi phục thành viên');
                $('.content').text('Bạn có muốn khôi phục hội viên này ? ');
                $('#confirmModal').modal('show');
                id = $('#id_update').val();
            }

        });

        $('.modal-footer').on('click', '.savemember', function () {
            $.ajax({
                type: 'POST',
                url: 'list_members/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'type': $type,
                },
                success: function (data) {
                    if (data.errors) {
                        toastr.warning(data.errors.memberInvalid, 'Thông báo', {timeOut: 2000});   
                    } else {
                        if ($type == 2) {
                            toastr.success('Khôi phục hồ sơ thành công!', 'Lưu hồ sơ', {timeOut: 1000});
                        }
                        else {
                            toastr.success('Lưu kho hồ sơ thành công!', 'Lưu hồ sơ', {timeOut: 1000});
                        }
                    }
                    
                    setTimeout(function () {// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 300);
                }
            });
        });
        $( document ).ready(function() {
            //document.getElementById("from_date").focus();
        });
    </script>
@endsection
