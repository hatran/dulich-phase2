<form class="page-wrap row" name="filter" method="get" id="formSearch" action="{{ url ('/officesys/banners') }}">
    {{ csrf_field() }}
    <div class="row" style="width: 100%">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px;">
           Tên Hình Ảnh
        </div>

        <div style="margin-bottom: 10px;" class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <input type="text" max="255" id="name" name="name" value="{{ array_get($response_view, 'name', '') }}" placeholder="Nhập từ khóa muốn tìm kiếm" tabindex="1">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px;">
            Trang hiển thị
        </div>
        <div style="" class="field-wrap dropfield col-md-4 col-sm-6 col-xs-12" >
            <select name="page_code" id="page_code" tabindex="2">
                <option value="">Chọn trang hiển thị muốn tìm kiếm</option>
                @foreach($pages as $key => $value)
                <option value="{{ $value->page_code }}"
                        @if (array_get($response_view, 'page_code', '') == $value->page_code) selected="selected" @endif>{{ $value->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px;">
            Phát hành từ ngày
        </div>

        <div style="margin-bottom: 10px;" class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12">
            <input type="text" placeholder="Từ ngày" tabindex="3" value="{{ array_get($response_view, 'start_time', '') }}" id="start_time" name="start_time" class="date datetime-input" style="">
            <span class="datetime-icon fa fa-calendar"></span>
        </div>
        <div style="margin-bottom: 10px;" class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12">
            <input type="text" placeholder="Đến ngày" tabindex="4" value="{{ array_get($response_view, 'end_time', '') }}" id="end_time" name="end_time" class="date datetime-input" style="">
            <span class="datetime-icon fa fa-calendar"></span>
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px;">
            Trạng thái
        </div>
        <div style="" class="field-wrap dropfield col-md-4 col-sm-6 col-xs-12" >
            <select name="status" id="status" tabindex="5">
                <option value="">Chọn trạng thái</option>
                <option value="1" @if (array_get($response_view, 'status', '') == 1) selected="selected" @endif>Lưu nháp</option>
                <option value="2" @if (array_get($response_view, 'status', '') == 2) selected="selected" @endif>Hoạt động</option>
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px; margin-top: 10px">
            Ngày tạo
        </div>

        <div style="margin-bottom: 10px;; margin-top: 10px" class="dropfield field-wrap col-md-4 col-sm-3 col-xs-12">
            <input type="text" placeholder="Ngày tạo" tabindex="6" value="{{ array_get($response_view, 'created_at', '') }}" id="created_at" name="created_at" class="date datetime-input" style="">
            <span class="datetime-icon fa fa-calendar"></span>
        </div>

        <div style="margin-left: 45%;" class="field-wrap col-md-2 col-sm-3 col-xs-2">
            <input type="submit" class="button btn-primary"  onclick="return submitForm();" name="search" value="Tìm kiếm" tabindex="7" style="width: 140px">
        </div>
    </div>
</form>
{{--<input type="hidden" value="{{array_get($response_view, 'page_code', '')}}" id="page_code" name="page_code"/>--}}
<input type="hidden" value="" id="dm-id" name="dm-id"/>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm hình ảnh quảng cáo</h4>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Phát hành từ ngày phải nhỏ hơn ngày kết thúc</h4>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
    function submitForm() {
        startDate = parseDate($('#start_time').val());
        endDate = parseDate($('#end_time').val());
        if(startDate != '' && endDate != '' && (startDate > endDate)) {
            $("#showAlert").modal('show');
        } else {
            $('#formSearch').submit();
        }
        return false;
    }
</script>