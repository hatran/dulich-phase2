
@extends('admin.layouts.app')
@section('title', 'Quản trị Hình Ảnh')

@section('content')
    <div class="page-wrap" id="">
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.message')
            @include('admin.banner.search')
            <div class="total" style="margin-bottom: 10px">Tổng số : {{$count_news}}<input class="import_view-modal button btn-primary btn-function add-modal" tabindex="7" id="btnAddPayment" style="cursor: pointer;float:  right;width: 120px; height: 37px; line-height: 24px; padding: 6px 12px !important;" data-file-id="-1" value="Thêm mới"></div>

            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%"><div class="th-inner sortable both">STT</div></td>
                        <td width="39%"><div class="th-inner sortable both">Tên Hình Ảnh</div></td>
                        <td width="39%"><div class="th-inner sortable both">Trang hiển thị</div></td>
                        <td width="8%"><div class="th-inner sortable both">Phát hành <br/> từ ngày</div></td>
                        <td width="8%"><div class="th-inner sortable both">Đến ngày</div></td>
                        <td width="8%"><div class="th-inner sortable both">Ngày tạo</div></td>
                        <td width="8%"><div class="th-inner sortable both">Trạng thái</div></td>
                        <td><div class="th-inner sortable both">Chức năng</div></td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objNews) == 0)
                        <tr>
                            <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objNews as $key => $value)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-left"><p title="{{$value->name}}">{{ mb_strlen($value->name, 'UTF-8') > 100 ? mb_substr($value->name, 0, 100, 'utf-8') .'...' :  $value->name  }}</p></td>
                                <td class="text-left">
                                    @foreach($pages as $k => $v)
                                        @if ($v->page_code == $value->page_code){{$v->name}}
                                        @endif
                                    @endforeach
                                </td>

                                <td class="text-center"> {{ empty($value->start_time) ? '' : date('d/m/Y', strtotime($value->start_time))}} </td>
                                <td class="text-center"> {{ empty($value->end_time) ? '' : date('d/m/Y', strtotime($value->end_time))}} </td>
                                <td class="text-center"> {{ empty($value->created_at) ? '' : date('d/m/Y', strtotime($value->created_at))}} </td>
                                <td class="text-left"> {{ $value->status == 1 ? 'Lưu nháp' : 'Hoạt động' }} </td>
                                <td class="text-center">
                                    <a class="btn-function show-modal" href="#" data-toggle="tooltip" data-placement="left" title="Xem Hình Ảnh" data-id="{{ $value->id }}" data-option="{{array_get($response_view, 'page_code', '')}}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                    </a>
                                    <a class="btn-function edit-modal" href="#" data-toggle="tooltip" data-placement="left" title="Sửa Hình Ảnh" data-id="{{ $value->id }}" data-option="{{array_get($response_view, 'page_code', '')}}" data-status="{{ $value->status }}" data-approved="">
                                        <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                    </a>
                                    <a class="btn-function" href="javascript:void(0);" data-toggle="tooltip" data-placement="left" title="Xoá Hình Ảnh" onclick="delete_intro({{ $value->id }}, '{{strip_tags($value->name)}}')" data-id="{{ $value->id }}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>

    @include('admin.banner.detail')
    @include('admin.banner.register')
    @include('admin.banner.edit')
    @include('admin.banner.delete')


@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }
        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .sortable{
            color: #144a8b;
            font-weight: bold;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }
        #closeButton ul {
            padding-left: 0px !important;
        }
        #page_code{
            -moz-appearance: textfield;     height: 35px;     line-height: 35px;     padding-right: 30px;     position: relative;     width: 100%;border: 1px solid #bbbaba;border-radius: 2px;display: block;max-width: 100%; margin-bottom: 10px;padding-left: 10px;
        }
        .rq-star{
            color: red
        }
        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }
        span.error-msg {
            color: #a94442;
        }
        span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }
        .total {
            border: 0 none;
            border-radius: 3px;
            color: #0000f0 !important;
            font-size: 16px;
            line-height: 30px;
            padding: 6px 10px;
        }


        .modal-title {
            margin: 0;
            line-height: 1.42857143;
            font-weight: bold;
            width: 50%;
            float: left;
            font-size: 20px;
        }
        .control-label{
            text-align: left !important;
        }
        .help-block strong{
            font-weight: normal;
            color: #a94442
        }
    </style>
    <script>
        var ValidateSize = function (file) {
            var zFileSize = file.files[0].size / 1024 / 1024; // in MB
            if (zFileSize > 10) {
                return false;
            } else {
                return true;
            }
        };
     
        function readURL(input, postfix) {

            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if (ValidateSize(document.getElementById("upload-image"+postfix)) == false) {
                jQuery('#check_img'+postfix).val('0');
                jQuery('#upload-image-error'+postfix).text("Chỉ được upload file tối đa 10MB ");
                jQuery('#upload-image-error'+postfix).css("display", "block");
            } else if (jQuery("#upload-image"+postfix).val() != '' && jQuery.inArray(jQuery("#upload-image"+postfix).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                jQuery('#check_img'+postfix).val('0');
                jQuery('#upload-image-error'+postfix).text("Các định dạng cho phép : " + fileExtension.join(', '));
                jQuery('#upload-image-error'+postfix).css("display", "block");
            }else{
                jQuery('#check_img'+postfix).val('1');
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        jQuery('#upload-image-error'+postfix).css("display", "none");
                        if(postfix == '_edit'){
                            $('#img_edit').attr('src', e.target.result);
                        }else{
                            $('#blah').attr('src', e.target.result);
                        }
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

        }
        function delete_intro(id, txt) {
            jQuery("#deleteModal").modal();
            jQuery('#id-em').text(txt);
            jQuery('#id_delete').val(id);
        }
        function deleteBanner() {
            window.location = '/officesys/banners/deleteall/'+jQuery('#id_delete').val();
        }
        function convertStr(txt){
            var str = txt;
            str= str.toLowerCase();
            str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str= str.replace(/đ/g,"d");
            str= str.replace(/\s/g, '');
            //str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
            //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
            //str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
            return str;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#btnsubmit").click();
                }
            }
        });
        function clearErrorFields(postfix) {
            $('#name'+postfix).removeClass('error_border');
            $('#start_time'+postfix).removeClass('error_border');
            $('#end_time'+postfix).removeClass('error_border');
            $('#page_code'+postfix).removeClass('error_border');
            $('.errorname'+postfix).addClass('hidden');
            $('.errorpage_code'+postfix).addClass('hidden');
            $('.help-block').remove();
        }

        var url = "{{ url('/officesys/banners') }}"
        function htmlEntities(str) {
            return str;
        }
        function showPopup(dmId, postfix) {
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';
            switch (postfix) {
                case '_add':
                    title = 'Thêm mới Hình Ảnh';
                    canEdit = true;
                    break;

                case '_edit':
                    title = 'Cập nhật thông tin Hình Ảnh';
                    canEdit = true;
                    break;

                default:
                    title = 'Chi tiết thông tin Hình Ảnh';
            }

            clearErrorFields(postfix);
            $('.modal-title').text(title);

            if(postfix != '_add'){
                $.ajax({
                    url: url + "/" + dmId,
                    type: 'GET',
                    success: function (response) {
//                        console.log(response);

                        var optionEditShow = '';
                        var nameAll = '';
                        if(response.deleted == 'error'){
                            document.location.reload(true);
                        }else{
                            $(modalId).modal('show');
                            if (canEdit === true) {
                                /*var page_code = '';
                                $.each(response.objPages, function( index, value ) {
                                    if(value.page_code == response.objData.page_code){
                                        page_code = value.name;
                                    }
                                });*/
                                $('#name'+postfix).val(response.objData.name);
                                $('#id'+postfix).val(response.objData.id);
                                $('#start_time'+postfix).val(response.objData.start_time);
                                $('#end_time'+postfix).val(response.objData.end_time);
                                $('#page_code'+postfix).val(response.objData.page_code);
                                $('#img'+postfix).attr('src', '/images/banners/'+response.objData.profile_image);
                                $('#img_old').val(response.objData.profile_image);
                                if(response.objData.status == 2){
                                    $('#statushd'+postfix).attr('checked', ' ');
                                    $('#statushd'+postfix).parent().addClass("chosen");
                                    $('#statusln'+postfix).removeAttr('checked');
                                    $('#statusln'+postfix).parent().removeClass("chosen");
                                }else{
                                    $('#statusln'+postfix).attr('checked', ' ');
                                    $('#statusln'+postfix).parent().addClass("chosen");
                                    $('#statushd'+postfix).removeAttr('checked');
                                    $('#statushd'+postfix).parent().removeClass("chosen");
                                }
                                $('#content'+postfix).val(response.objData.content);
                                $('#content2'+postfix).val(response.objData.content2);
                            } else {
                                var page_code = '';
                                $.each(response.objPages, function( index, value ) {
                                    if(value.page_code == response.objData.page_code){
                                        page_code = value.name;
                                    }
                                });
                                $('#name'+postfix).text(response.objData.name);
                                $('#images_show').attr('src', '/images/banners/'+response.objData.profile_image);
                                $('#start_time'+postfix).text(response.objData.start_time);
                                $('#end_time'+postfix).text(response.objData.end_time);
                                $('#page_code'+postfix).text(page_code);
                                $('#status'+postfix).text(response.objData.status == 2 ? 'Hoạt động' : 'Lưu nháp');
                                $('#content'+postfix).text(response.objData.content);
                                $('#content2'+postfix).text(response.objData.content2);
                            }
                            //focus number payment
                            if (postfix == '_edit') {
                                setTimeout(function () {
                                    $("#name"+postfix).focus();
                                }, 500);
                            }
                        }
                    }
                });
            }else{
                $(modalId).modal('show');
                $("#name"+postfix).val('');
                $("#start_time"+postfix).val('');
                $("#end_time"+postfix).val('');
                $("#page_code"+postfix).val('');
                $("#content"+postfix).val('');
                $("#content2"+postfix).val('');
                setTimeout(function () {
                    $("#name"+postfix).focus();
                }, 500);
            }
        }

        $(document).on('click', '.show-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            showPopup(dmId, '_show');
        });

        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            showPopup('', '_add');
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            $('input[type=hidden]#dm-id').val(dmId);
            showPopup(dmId, '_edit');
        });


        function doAction(dmId, postfix) {

            var name        = $("#name"+postfix);
            var start_time_obj  = $('#start_time'+postfix);
            var end_time_obj    = $('#end_time'+postfix);
            var page_code   = $('#page_code'+postfix);
            if (postfix == '_add' || postfix == '_edit') {
                clearErrorFields(postfix)
            }
            $(".help-block").remove();
            var flg = 0;
            if (postfix == '_add' || postfix == '_edit') {

                if (name.val().trim() == '') {
                    $("#name"+postfix).addClass('error_border');
                    $(".errorname"+postfix).html('Tên hình ảnh là trường bắt buộc phải điền');
                    $(".errorname"+postfix).removeClass('hidden');
                    flg = 1;
                } /*else {
                    var titleConver = convertStr(name.val());
                    if(!(/^[a-zA-Z0-9\-\.\/]*$/i).test(titleConver)){
                        flg = 1;
                        $("#name"+postfix).addClass('error_border');
                        $(".errorname"+postfix).html('Tên banner chỉ được nhập các ký tự a-z A-Z 0-9 -  . /');
                        $(".errorname"+postfix).removeClass('hidden');
                    }else {
                        $(".errorname"+postfix).html('');
                        $(".errorname"+postfix).addClass('hidden');
                        $("#name"+postfix).removeClass('error_border');
                    }
                }*/

                if (page_code.val() == '') {
                    $("#page_code"+postfix).addClass('error_border');
                    $(".errorpage_code"+postfix).html('Trang hiển thị là trường bắt buộc phải chọn');
                    $(".errorpage_code"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorpage_code"+postfix).html('');
                    $(".errorpage_code"+postfix).addClass('hidden');
                    $("#page_code"+postfix).removeClass('error_border');
                }
                var d = new Date();
                var nowDate = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
                var startDate = parseDate(nowDate).getTime();
                if(start_time_obj.val().trim() != '' && end_time_obj.val().trim() == '') {
                    //check format
                    if(ValidateCustomDate(start_time_obj.val())){
                        //validation
                        var startTime = parseDate(start_time_obj.val()).getTime();
                        if (startDate > startTime) {
                            flg = 1;
                            start_time_obj.addClass("error_border");
                            $( '<span class="help-block"><strong>Ngày bắt đầu không được nhỏ hơn ngày hiện tại</strong></span>' ).insertAfter( "#start_time"+postfix );
                        }else{
                            start_time_obj.removeClass("error_border");
                        }
                    }else{
                        flg = 1;
                        start_time_obj.addClass("error_border");
                        $( '<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>' ).insertAfter( "#start_time"+postfix );
                    }

                }

                if(end_time_obj.val().trim() != '' && start_time_obj.val().trim() == '') {
                    if(ValidateCustomDate(end_time_obj.val())) {
                        var endTime = parseDate(end_time_obj.val()).getTime();
                        if (startDate > endTime) {
                            flg = 1;
                            end_time_obj.addClass("error_border");
                            $('<span class="help-block"><strong>Ngày kết thúc phải lớn hơn ngày hiện tại</strong></span>').insertAfter("#end_time"+postfix);
                        } else {
                            end_time_obj.removeClass("error_border");
                        }
                    }else{
                        flg = 1;
                        end_time_obj.addClass("error_border");
                        $('<span class="help-block"><strong>Định dạng của ngày kết thúc là dd/mm/yyyy</strong></span>').insertAfter("#end_time"+postfix);
                    }

                }

                if(end_time_obj.val().trim() != '' && start_time_obj.val().trim() != '') {
                    if(ValidateCustomDate(end_time_obj.val()) && ValidateCustomDate(start_time_obj.val())) {

                        //validation
                        var startTime = parseDate(start_time_obj.val()).getTime();
                        var endTime = parseDate(end_time_obj.val()).getTime();

                        if (startDate > startTime) {
                            flg = 1;
                            start_time_obj.addClass("error_border");
                            $( '<span class="help-block"><strong>Ngày bắt đầu không được nhỏ hơn ngày hiện tại</strong></span>' ).insertAfter( "#start_time"+postfix );
                        }else{
                            start_time_obj.removeClass("error_border");
                        }
                        if (startDate > endTime) {
                            flg = 1;
                            end_time_obj.addClass("error_border");
                            $('<span class="help-block"><strong>Ngày kết thúc phải lớn hơn ngày hiện tại</strong></span>').insertAfter("#end_time"+postfix);
                        } else {
                            end_time_obj.removeClass("error_border");
                        }
                        if(flg == 0){
                            if (startTime > endTime) {
                                flg = 1;
                                end_time_obj.addClass("error_border");
                                start_time_obj.addClass("error_border");
                                $('<span class="help-block"><strong>Ngày bắt đầu phải nhỏ hơn ngày kết thúc</strong></span>').insertAfter("#start_time"+postfix);
                            } else {
                                end_time_obj.removeClass("error_border");
                                start_time_obj.removeClass("error_border");
                            }
                        }
                    }else{
                        if(!ValidateCustomDate(end_time_obj.val())) {
                            flg = 1;
                            end_time_obj.addClass("error_border");
                            $('<span class="help-block"><strong>Định dạng của ngày kết thúc là dd/mm/yyyy</strong></span>').insertAfter("#end_time"+postfix);
                        }else{
                            end_time_obj.removeClass("error_border");
                        }
                        if(!ValidateCustomDate(start_time_obj.val())) {
                            flg = 1;
                            start_time_obj.addClass("error_border");
                            $('<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>').insertAfter("#start_time"+postfix);
                        }else{
                            start_time_obj.removeClass("error_border");
                        }
                    }

                }
                if (postfix == '_add') {
                    if (jQuery("#upload-image" + postfix).val() == '') {
                        $("#upload-image-error" + postfix).css('display', 'block');
                    }
                }
            }

            if (flg == 0 && jQuery('#check_img'+postfix).val() == '1') {
                $("#bannerForm"+postfix).submit();
                var modalId = '#' + postfix + 'Modal';
                $(modalId).modal('hide');
            }
        }
        // Chuyển chuỗi kí tự (string) sang đối tượng Date()
        function parseDate(str) {
            var mdy = str.split('/');
            return new Date(mdy[2], mdy[1], mdy[0]);
        }
        function ValidateCustomDate(d) {
            var match = /^(\d{2})\/(\d{2})\/(\d{4})$/.exec(d);
            if (!match) {
                // pattern matching failed hence the date is syntactically incorrect
                return false;
            }
            var day     = parseInt(match[1], 10); // months are 0-11, not 1-12
            var month   = parseInt(match[2], 10) - 1;
            var year    = parseInt(match[3], 10);
            var date    = new Date(year, month, day);
            // now, Date() will happily accept invalid values and convert them to valid ones
            // therefore you should compare input month/day/year with generated month/day/year
            return date.getDate() == day && date.getMonth() == month && date.getFullYear() == year;
        }
        $('.modal-footer').on('click', '.add', function () {
            doAction('', '_add');
        });

        $('.modal-footer').on('click', '.edit', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_edit');
        });

        $('.modal-footer').on('click', '.delete', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_delete');
        });
        $(document).ready(function () {
            var name = $("#name").val();
            $("#name").val('');
            setTimeout(function() {
                $('#name').focus() ;
                $("#name").val(name);
            }, 100);
        });
    </script>
@endsection