<style>
    #_showModal .form-group{
        margin-bottom: 35px !important;
    }
</style>
<div class="modal fade" id="_showModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"
                          style="font-size: 39px; width: 30%;float: right;text-align: right;padding-right: 10px;">&times;</span>
                </button>
            </div>
                {{ csrf_field() }}
                <input type="hidden" name="id" class="form-control" value="-1"/>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row" style="padding: 1rem;">
                            <div class="container-fluid shadow">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="col-md-12" style="padding-left: 0px !important;">
                                                        <div class="form-group">
                                                            <label class="control-label control-label-left col-sm-4" for="field1"  style="padding-left: 0px !important;">Tên hình ảnh</label>
                                                            <div class="controls col-sm-8">
                                                                <span id="name_show"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="padding-left: 0px !important;">
                                                        <div class="form-group">
                                                            <label class="control-label control-label-left col-sm-4" for="field2" style="padding-left: 0px !important;">Thời gian bắt đầu</label>
                                                            <div class="dropfield field-wrap col-md-8 col-sm-3 col-xs-12">
                                                                <span id="start_time_show"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="padding-left: 0px !important;">
                                                        <div class="form-group">
                                                            <label class="control-label control-label-left col-sm-4" for="field2" style="padding-left: 0px !important;">Thời gian kết thúc</label>
                                                            <div class="dropfield field-wrap col-md-8 col-sm-3 col-xs-12">
                                                                <span id="end_time_show"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12" style="padding-left: 0px !important;">
                                                        <div class="form-group">
                                                            <label class="control-label control-label-left col-sm-4" for="field2" style="padding-left: 0px !important;">Trang hiển thị</label>
                                                            <div class="dropfield field-wrap col-md-8 col-sm-3 col-xs-12">
                                                                <span id="page_code_show"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{--<div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4" for="status">Ảnh đại diện</label>
                                                        <div class="controls col-sm-8">
                                                            <input id="upload-image" tabindex="5" name="profile_image" type="file" value="" onchange="readURL(this);">
                                                            <span id="upload-image-error" class="error" style="display: none;">Ảnh đại diện không được bỏ trống</span>
                                                            <span id="error-status" class="error"></span>
                                                        </div>
                                                    </div>--}}
                                                    <div class="col-md-12" style="padding-left: 0px !important;">
                                                        <div class="form-group">
                                                            <label class="control-label control-label-left col-sm-4" for="status" style="padding-left: 0px !important;">Trạng thái</label>
                                                            <div class="col-md-6" style="padding-top: 5px">
                                                                <div class="field-wrap col-md-12 radio-list choices" style="padding-left: 0px !important;">
                                                                    <span id="status_show"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <div class="controls col-sm-9">
                                                                        <img id="images_show" src="{{ asset('admin/images/no_image.png') }}" alt="your image" width="100%"/>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left  col-sm-2"  style="padding-left: 0px !important;">Nội dung hiển thị 1</label>
                                                        <div class="controls col-sm-10" style="padding-left: 0px  !important;">
                                                            <p id="content_show" class="" data-role="" style="width: 90%; margin-left: 30px"  tabindex="8" name="content" ></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left  col-sm-2"  style="padding-left: 0px !important;">Nội dung hiển thị 2</label>
                                                        <div class="controls col-sm-10" style="padding-left: 0px  !important;">
                                                            <p id="content2_show" class="" data-role="" style="width: 90%; margin-left: 30px"  tabindex="8" name="content2" ></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer edit">
                    <button type="button" class="btn btn-warning"  tabindex="1" data-dismiss="modal">Thoát</button>
                </div>
        </div>
    </div>
</div>
