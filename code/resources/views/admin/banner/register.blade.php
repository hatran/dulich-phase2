<div class="modal fade" id="_addModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"
                          style="font-size: 39px; width: 30%;float: right;text-align: right;padding-right: 10px;">&times;</span>
                </button>
            </div>
            <form action="{{url('/officesys/banners/create_banner')}}" id="bannerForm_add" class="form-horizontal" role="form" enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id" class="form-control" value="-1"/>
                <input type="hidden" id="check_img_add" name="check_img" value="0"/>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row" style="padding: 1rem;">
                            <div class="container-fluid shadow">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4" for="field1" style="padding-left: 0px !important;">Tên hình ảnh<span style="color: red"> *</span></label>
                                                        <div class="controls col-sm-8">
                                                            <input id="name_add" type="text" class="form-control k-textbox " data-role="text" maxlength="191" name="name_add" tabindex="100">
                                                            <span class="errorname_add text-left hidden error-msg"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4" for="field2" style="padding-left: 0px !important;">Thời gian bắt đầu</label>
                                                        <div class="dropfield field-wrap col-md-8 col-sm-3 col-xs-12">

                                                            <input type="text" tabindex="101" placeholder="" id="start_time_add" value="" name="start_time" class="date datetime-input" style="">
                                                            <span class="datetime-icon fa fa-calendar"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4" for="field2" style="padding-left: 0px !important;">Thời gian kết thúc</label>
                                                        <div class="dropfield field-wrap col-md-8 col-sm-3 col-xs-12">

                                                            <input type="text" tabindex="102" placeholder="" id="end_time_add" value="" name="end_time" class="date datetime-input" style="">
                                                            <span class="datetime-icon fa fa-calendar"></span>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4" for="field2" style="padding-left: 0px !important;">Trang hiển thị<span style="color: red"> *</span></label>
                                                        <div class="dropfield field-wrap col-md-8 col-sm-3 col-xs-12">

                                                            <select name="page_code" id="page_code_add" tabindex="103">
                                                                <option value="">Chọn trang hiển thị</option>
                                                                @foreach($pages as $key => $value)
                                                                    <option value="{{ $value->page_code }}">{{ $value->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="errorpage_code_add text-left hidden error-msg"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4" for="status" style="padding-left: 0px !important;">Ảnh đại diện<span style="color: red"> *</span></label>
                                                        <div class="controls col-sm-8">
                                                            <input id="upload-image_add" tabindex="104" name="profile_image" type="file" value="" onchange="readURL(this, '_add');">
                                                            <span id="upload-image-error_add" class="error" style="display: none; color: #a94442">Ảnh đại diện không được bỏ trống</span>
                                                            <span id="error-status" class="error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4" for="status" style="padding-left: 0px !important;">Trạng thái</label>
                                                        <div class="col-md-6" style="padding-top: 5px">
                                                            <div class="field-wrap col-md-12 radio-list choices" style="padding-left: 0px !important;">
                                                                <div class="choice" style="padding-left: 25px !important;">
                                                                    <label for="statushd" tabindex="105">Hoạt động </label>
                                                                    <input id="statushd" type="radio" name="status" value="2" >
                                                                </div>
                                                                <div class="choice chosen">
                                                                    <label for="statusln" tabindex="106" >Lưu nháp </label>
                                                                    <input id="statusln" type="radio" name="status" value="1" checked="checked">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <div class="controls col-sm-9">
                                                                        <img id="blah" src="{{ asset('admin/images/no_image.png') }}" alt="your image" width="100%"/>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left  col-sm-2" for="field5" style="padding-left: 0px !important;">Nội dung hiển thị 1</label>
                                                        <div class="controls col-sm-10">
                                                            <textarea id="content_add" rows="3" class="form-control k-textbox required" data-role="textarea" style="width: 90%; margin-left: 30px"  tabindex="107" name="content" maxlength="500"  ></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left  col-sm-2" for="field5" style="padding-left: 0px !important;">Nội dung hiển thị 2</label>
                                                        <div class="controls col-sm-10">
                                                            <textarea id="content2_add" rows="3" class="form-control k-textbox required" data-role="textarea" style="width: 90%; margin-left: 30px"  tabindex="108" name="content2" maxlength="500"  ></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer edit">
                    <button type="button" class="btn btn-primary add" tabindex="109">Lưu</button>
                    <button type="button" class="btn btn-warning"  tabindex="110" data-dismiss="modal">Thoát</button>
                </div>
            </form>
        </div>
    </div>
</div>
