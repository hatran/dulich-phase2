<div id="_editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_edit"></h4>
            </div>
            <form action="{{url('/officesys/evaluationcriteria/check_evaluation')}}" id="evaluationForm_edit" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id_edit" id="id_edit" class="form-control" value=""/>
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-md-3 control-label" style="padding-top: 7px">Tên tiêu chí<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="nameAll_edit" id="nameAll_edit" maxlength="191" tabindex="201">
                                        <span class="errorNameAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="code" class="col-md-3 control-label" style="padding-top: 7px">Mã tiêu chí<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="codeAll_edit" id="codeAll_edit" maxlength="191" tabindex="201">
                                        <span class="errorCodeAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="percentage" class="col-md-3 control-label" style="padding-top: 7px">Trọng số<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="percentageAll_edit" id="percentageAll_edit" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="2" tabindex="201">
                                        <span class="errorPercentageAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="statusAll_edit" style="padding-top: 7px">Trạng thái<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="radio" name="statusAll_edit" value="1" checked> Hoạt động
                                        <input type="radio" name="statusAll_edit" value="0" style="margin-left: 50px;"> Không hoạt động<br>
                                        <span class="errorStatusAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary edit" type="button" tabindex="202">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="203">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
