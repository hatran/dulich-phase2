<div id="_addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_add"></h4>
            </div>
            <form action="{{url('/officesys/evaluationcriteria/check_evaluation')}}" id="evaluationForm_add" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="nameAll_add" class="col-md-3 control-label" style="padding-top: 7px">Tên tiêu chí<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="nameAll_add" id="nameAll_add" maxlength="191" tabindex="101">
                                        <span class="errorNameAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="codeAll_add" class="col-md-3 control-label" style="padding-top: 7px">Mã tiêu chí<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="codeAll_add" id="codeAll_add" maxlength="191" tabindex="101">
                                        <span class="errorCodeAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="percentageAll_add" class="col-md-3 control-label" style="padding-top: 7px">Trọng số<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="percentageAll_add" id="percentageAll_add" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="2" tabindex="101">
                                        <span class="errorPercentageAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="statusAll_add" style="padding-top: 7px">Trạng thái<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="radio" name="statusAll_add" value="1" checked> Hoạt động
                                        <input type="radio" name="statusAll_add" value="0" style="margin-left: 50px;"> Không hoạt động<br>
                                        <span class="errorStatusAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary add" type="button" tabindex="103">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="104">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>