
@extends('admin.layouts.app')
@section('title', 'Quản trị danh mục tiêu chí đánh giá')

@section('content')
    <div class="page-wrap">
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.message')
            @include('admin.evaluation_criteria.search')
            <div class="total" style="margin-bottom: 10px">Tổng số : {{$count_evaluations}}<input class="import_view-modal button btn-primary btn-function add-modal" value="Thêm mới" id="btnAddEvaluation" style="float:  right;width: 120px; height: 37px; line-height: 24px; padding: 6px 12px !important; cursor: pointer" data-file-id="-1"></div>

            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%"><div class="th-inner sortable both">STT</div></td>
                        <td width="15%"><div class="th-inner sortable both">Mã tiêu chí</div></td>
                        <td width="25%"><div class="th-inner sortable both">Tên tiêu chí</div></td>
                        <td width="10%"><div class="th-inner sortable both">Trọng số (%)</div></td>
                        <td width="15%"><div class="th-inner sortable both">Người tạo</div></td>
                        <td width="10%"><div class="th-inner sortable both">Ngày tạo</div></td>
                        <td width="10%"><div class="th-inner sortable both">Trạng thái</div></td>
                        <td width="10%"><div class="th-inner sortable both">Chức năng</div></td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objEvaluations) == 0)
                        <tr>
                            <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objEvaluations as $key => $value)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center"> {{ $value->code }} </td>
                                <td class="text-left">
                                    {{ mb_strlen($value->name, 'UTF-8') > 100 ? mb_substr($value->name, 0, 100, 'utf-8') .'...' :  $value->name  }}
                                </td>

                                <td class="text-center">
                                    {{ $value->percentage }}
                                </td>
                                 <td>{{ !empty($value->created_author) ? $value->created_author : '' }}</td>
                                <td class="text-center">{{ !empty($value->created_at) ? date('d.m.Y', strtotime($value->created_at)) : '' }}</td>
                                <td class="text-left"> {{ $value->status == 0 ? 'Không hoạt động' : 'Hoạt động' }} </td>
                                <td class="text-center">
                                    <a class="btn-function show-modal" href="#" data-toggle="tooltip" data-placement="left" title="Xem tiêu chí đánh giá" data-id="{{ $value->id }}"  data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                    </a>
                                    <a class="btn-function edit-modal" href="#" data-toggle="tooltip" data-placement="left" title="Sửa tiêu chí đánh giá" data-id="{{ $value->id }}"  data-status="{{ $value->status }}" data-approved="">
                                        <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                    </a>
                                    <a class="btn-function" href="javascript:void(0);" data-toggle="tooltip" data-placement="left" title="Xoá tiêu chí đánh giá" onclick="delete_evaluation({{ $value->id }}, '{{strip_tags($value->name)}}')" data-id="{{ $value->id }}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>

    @include('admin.evaluation_criteria.detail')
    @include('admin.evaluation_criteria.register')
    @include('admin.evaluation_criteria.edit')
    @include('admin.evaluation_criteria.delete')


@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }
        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .sortable{
            color: #144a8b;
            font-weight: bold;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }
        #closeButton ul {
            padding-left: 0px !important;
        }
        #id{
            -moz-appearance: textfield;     height: 35px;     line-height: 35px;     padding-right: 30px;     position: relative;     width: 100%;border: 1px solid #bbbaba;border-radius: 2px;display: block;max-width: 100%; margin-bottom: 10px;padding-left: 10px;
        }
        .rq-star{
            color: red
        }
        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }
        span.error-msg {
            color: #a94442;
        }
        span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }
        .total {
            border: 0 none;
            border-radius: 3px;
            color: #0000f0 !important;
            font-size: 16px;
            line-height: 30px;
            padding: 6px 10px;
        }


        .modal-title {
            margin: 0;
            line-height: 1.42857143;
            font-weight: bold;
            width: 50%;
            float: left;
            font-size: 20px;
        }
    </style>
    <script>
        function delete_evaluation(id, txt) {
            jQuery("#deleteModal").modal();
            jQuery('#id-em').text(txt);
            jQuery('#id_delete').val(id);
        }
        function deleteCat() {
            window.location = '/officesys/evaluationcriteria/deleteall/'+jQuery('#id_delete').val();
        }
        function convertStr(txt){
            var str = txt;
            str= str.toLowerCase();
            str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str= str.replace(/đ/g,"d");
            str= str.replace(/\s/g, '');
            //str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
            //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
            //str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
            return str;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#btnsubmit").click();
                }
            }
        });
        function clearErrorFields(postfix) {
            $('#nameAll'+postfix).removeClass('error_border');
            $('#codeAll'+postfix).removeClass('error_border');
            $('#percentageAll'+postfix).removeClass('error_border');
            $('.errorNameAll'+postfix).addClass('hidden');
            $('.errorCodeAll'+postfix).addClass('hidden');
            $('.errorPercentageAll'+postfix).addClass('hidden');
        }

        var url = "{{ url('/officesys/evaluationcriteria') }}"
        function htmlEntities(str) {
            return str;
        }
        function showPopup(dmId, postfix) {
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';

            switch (postfix) {
                case '_add':
                    title = 'Thêm mới tiêu chí đánh giá';
                    canEdit = true;
                    break;

                case '_edit':
                    title = 'Cập nhật tiêu chí đánh giá';
                    $("#id_edit").val(dmId);
                    canEdit = true;
                    break;

                default:
                    title = 'Chi tiết tiêu chí đánh giá';
            }
            clearErrorFields(postfix);
            $('.modal-title'+postfix).text(title);
            if(postfix != '_add'){
//                $(modalId).modal('show');
//                $(modalId + ' .modal-dialog').loader('show');
                $.ajax({
                    url: url + "/" + dmId,
                    type: 'GET',
                    success: function (response) {
//                        console.log(response);
                        var nameAll = '';
                        var codeAll = '';
                        var percentageAll = '';
                        var statusAll = '';
                        if(response.deleted == 'error'){
                            document.location.reload(true);
                        }else{
                            $(modalId).modal('show');
                           
                            nameAll = response.objData.name;
                            codeAll = response.objData.code;
                            statusAll = response.objData.status;
                            percentageAll = response.objData.percentage;
                            if (canEdit === true) {
                                $('#nameAll'+postfix).val(nameAll);
                                $('#codeAll'+postfix).val(codeAll);
                                $('#percentageAll'+postfix).val(percentageAll);
                                var statusRadio = $("input[name='statusAll_edit']");
                                statusRadio.each(function () {
                                    if (statusAll == $(this).val()) {
                                        $(this).prop('checked', 'checked');
                                    }
                                });
                            } else {
                                $('#nameAll_show').text(nameAll);
                                $('#codeAll_show').text(codeAll);
                                $('#percentageAll_show').text(percentageAll);
                                var messStatus = '';
                                if (statusAll == 1) {
                                    messStatus = 'Họat động';
                                }
                                else {
                                    messStatus = 'Không họat động';
                                }
                                $('#statusAll_show').text(messStatus);
                                
                            }
                            if (postfix == '_edit') {
                                setTimeout(function () {
                                    $("#nameAll"+postfix).focus();
                                }, 500);
                            }
                        }
                    }
                });
            }else{
                $(modalId).modal('show');
                $("#nameAll"+postfix).val('');
                $("#codeAll"+postfix).val('');
                $("#percentageAll"+postfix).val('');
                setTimeout(function () {
                    $("#nameAll"+postfix).focus();
                }, 500);
            }
        }

        $(document).on('click', '.show-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            showPopup(dmId, '_show');
        });

        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            showPopup('', '_add');
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            $('input[type=hidden]#dm-id').val(dmId);
            showPopup(dmId, '_edit');
        });


        function doAction(dmId, postfix) {
            var action = 'POST';
            var modalId = '#' + postfix + 'Modal';

            var nameAll = $("#nameAll" + postfix);
            var codeAll = $("#codeAll" + postfix);
            var percentageAll = $("#percentageAll" + postfix);
            var statusAll = $("#statusAll" + postfix);

            if (postfix == '_add')  dmId = 'new';
            if (postfix == '_add' || postfix == '_edit') {
                clearErrorFields(postfix)
            }
            var flg = 0;
            if (postfix == '_add' || postfix == '_edit') {
                if (nameAll.val().trim() == '') {
                    $("#nameAll"+postfix).addClass('error_border');
                    $(".errorNameAll"+postfix).html('Tên danh mục đánh giá là trường bắt buộc phải điền');
                    $(".errorNameAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorNameAll"+postfix).html('');
                    $(".errorNameAll"+postfix).addClass('hidden');
                    $("#nameAll"+postfix).removeClass('error_border');
                }

                if (codeAll.val().trim() == '') {
                    $("#codeAll"+postfix).addClass('error_border');
                    $(".errorCodeAll"+postfix).html('Mã danh mục là trường bắt buộc phải điền');
                    $(".errorCodeAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorCodeAll"+postfix).html('');
                    $(".errorCodeAll"+postfix).addClass('hidden');
                    $("#codeAll"+postfix).removeClass('error_border');
                }

                if (percentageAll.val().trim() == '') {
                    $("#percentageAll"+postfix).addClass('error_border');
                    $(".errorPercentageAll"+postfix).html('Trọng số là trường bắt buộc phải điền');
                    $(".errorPercentageAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorPercentageAll"+postfix).html('');
                    $(".errorPercentageAll"+postfix).addClass('hidden');
                    $("#percentageAll"+postfix).removeClass('error_border');
                }
            }

            if (flg == 0) {
                $("#evaluationForm"+postfix).submit();
                var modalId = '#' + postfix + 'Modal';
                $(modalId).modal('hide');
            }
        }

        $('.modal-footer').on('click', '.add', function () {
            doAction('', '_add');
        });

        $('.modal-footer').on('click', '.edit', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_edit');
        });

        $('.modal-footer').on('click', '.delete', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_delete');
        });
        $(document).ready(function () {
            var nameall = $("#nameAll").val();
            $("#nameAll").val('');
            setTimeout(function() {
                $('#nameAll').focus() ;
                $("#nameAll").val(nameall);
            }, 100);
        });
    </script>
@endsection