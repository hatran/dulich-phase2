<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Providers\UserServiceProvider;
?>

@extends('admin.layouts.app')
@section('title', 'Quản trị nội dung Email')
<style>
    .table-wrap table tr td {position: relative;word-wrap: break-word;white-space: normal !important;}
</style>
@section('content')
<div class="main-content" style="padding-bottom: 75px; margin-top: -20px;">
        @include('admin.layouts.message')
        @include('admin.email.search')
        <div class="total" style="margin-bottom: 10px">Tổng số : {{$count_emails}}<input class="import_view-modal button btn-primary btn-function add-modal" tabindex="8" id="btnAddPayment" style="float:  right;width: 120px; height: 37px; line-height: 24px; padding: 6px 12px !important; cursor: pointer" data-file-id="-1" value="Thêm mới" onclick="createIntro();"></div>
        <div class="clearfix"></div>
            <div class="page-wrap">
                <div class="table-wrap">
                    <table>
                        <thead>
                        <tr>
                            <td width="5%"><div class="th-inner sortable both">STT</div></td>
                            <td width="23%"><div class="th-inner sortable both">Tiêu đề e-mail</div></td>
                            <td width="15%"><div class="th-inner sortable both">Loại email</div></td>
                            <td width="15%"><div class="th-inner sortable both">Phạm vi áp dụng</div></td>
                            <td width="8%"><div class="th-inner sortable both">Ngày bắt đầu</div></td>
                            <td width="8%"><div class="th-inner sortable both">Ngày kết thúc</div></td>
                            <td width="8%"><div class="th-inner sortable both">Ngày tạo</div></td>
                            <td width="8%"><div class="th-inner sortable both">Trạng thái</div></td>
                            <td width="8%"><div class="th-inner sortable both">Chức năng</div></td>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($objEmails) == 0)
                            <tr>
                                <td colspan="8" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                            </tr>
                        @else
                            @foreach($objEmails as $key => $value) {{--htmlentities($value->content)--}}
                                <tr>
                                    <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                    <td class="text-left"><p title="{{$value->title}}">{{ mb_strlen($value->title, 'UTF-8') > 100 ? mb_substr(htmlentities($value->title), 0, 100, 'utf-8') .'...' :  htmlentities($value->title)  }}</p>
                                    </td>
                                    <td class="text-left">
                                        @foreach($options as $k => $v)
                                            @if ($v->code == $value->option_code){{$v->value}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        @if (0 === $value->branch_id)
                                            {{'Toàn Quốc'}}
                                        @endif
                                        @if (0 !== $value->branch_id)
                                            @foreach($offices as $key => $val)
                                                @if ($val->id === $value->branch_id)
                                                    {{ $val->name }}
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="text-center"> {{ empty($value->start_time) ? '' : date('d/m/Y', strtotime($value->start_time)) }} </td>
                                    <td class="text-center"> {{ empty($value->end_time) ? '' : date('d/m/Y', strtotime($value->end_time)) }} </td>
                                    <td class="text-center"> {{ empty($value->created_at) ? '' : date('d/m/Y', strtotime($value->created_at)) }} </td>
                                    <td class="text-left"> {{ $value->status == 0 ? 'Khóa' : 'Hoạt động' }} </td>

                                    <td class="text-center">
                                        <a class="btn-function show-modal" href="{{ url('/officesys/emails/detail/' . $value->id) }}" data-toggle="tooltip" data-placement="left" title="Xem" data-id="{{ $value->id }}">
                                            <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                        </a>
                                        <a class="btn-function edit-modal" href="{{ url('/officesys/emails/' . $value->id) }}" data-toggle="tooltip" data-placement="left" title="Sửa" data-id="{{ $value->id }}">
                                            <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                        </a>
                                        @if (Auth::user()->role == $admin_user)
                                            <a class="btn-function delete-modal" href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Xoá" onclick="delete_intro({{ $value->id }}, '{{strip_tags($value->title)}}')">
                                                <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    @include('admin.email.delete')
                    <form id="delete_intro" action="" method="post">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                    </form>
                </div>
                @include('admin.layouts.pagination')
        </div>
    </div>
@endsection
@section('footer_embed')
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }
        .sortable{
            color: #144a8b;
            font-weight: bold;
        }
        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }
        #closeButton ul {
            padding-left: 0px !important;
        }
        #option_code{
            -moz-appearance: textfield;     height: 35px;     line-height: 35px;     padding-right: 30px;     position: relative;     width: 100%;border: 1px solid #bbbaba;border-radius: 2px;display: block;max-width: 100%; margin-bottom: 10px;padding-left: 10px;
        }

        .total {
            border: 0 none;
            border-radius: 3px;
            color: #0000f0 !important;
            font-size: 16px;
            line-height: 30px;
            padding: 6px 10px;
        }

        .modal-title {
            margin: 0;
            line-height: 1.42857143;
            font-weight: bold;
            width: 50%;
            float: left;
            font-size: 20px;
        }
    </style>
    <script>
        function createIntro(){
            window.location = "{{ url('/officesys/emails/create') }}";
        }
        function deleteCat() {
            $('#delete_intro').attr('action', '/officesys/emails/' + jQuery('#id_delete').val());
            $('#delete_intro').get(0).submit();
        }
        function delete_intro(id, txt) {
            jQuery("#deleteModal").modal();
            jQuery('#id-em').text(txt);
            jQuery('#id_delete').val(id);
        }
        $( document ).ready(function() {
            var title = $("#title").val();
            $("#title").val('');
            setTimeout(function() {
                $('#title').focus() ;
                $("#title").val(title);
            }, 100);
        });
    </script>
@endsection