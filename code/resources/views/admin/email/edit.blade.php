@extends('admin.layouts.app')
@section('title', 'Chỉnh sửa thông tin bài giới thiệu ')
@section('content')
    <style>
        .color-required{
            color: red
        }
        .error_border {
            border-color: #a94442 !important;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset !important;
        }
        .help-block strong {
            color: red
        }

        .expire_date{
            margin-bottom: 10px;
            padding-top: 7px;
            font-weight: bold;
            padding-left: 0px;
            padding-right: 0px;
            text-align: center;
        }
        .col-md-1{
            width: 10%;
        }
    </style>
    <div class="page-wrap">
        <style>
            #closeButton {margin-top: 20px}
        </style>
        @include('admin.layouts.message')
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Thông tin chi tiết nội dung email</div>

            <div class="panel-body">
                <form class="form-horizontal" method="post" action="{{ route('emails.update', [$objEmail->id]) }}" id="updateForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <input type="hidden" id="idCheck" value="{{ $objEmail->id }}"/>
                    <!-- <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}">
                        <label for="branch_id" class="col-md-1 control-label">Văn phòng đại diện <span class="color-required">*</span></label>
                        <div class="col-md-6" style="">
                            <div class="col-md-5" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <select id="branch_id" class="form-control" name="branch_id" tabindex="1">
                                     <option value="">Chọn văn phòng đại diện</option>
                                    @foreach($arrBranches as $key => $v)
                                        <option value="{{ $v->id }}" {{ (empty(old('branch_id')) ? ($objEmail->branch_id == $v->id) : (old('branch_id') == $v->id)) ? 'selected' : '' }}> {{ $v->name }} </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('branch_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div> -->

                    <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}">
                        <label for="branch_id" class="col-md-1 control-label">Phạm vi <span class="color-required">*</span></label>
                        <div class="col-md-6" style="">
                            <div class="col-md-5" style="margin-bottom: 10px;padding-left: 0px !important;display: flex; gap: 15px">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="typeOp" {{ ($objEmail->list_id) == '0' ? 'checked' : ''  }} type="radio" id="inlineRadio2" value="1" checked />
                                    <label class="form-check-label" for="inlineRadio2">Toàn quốc</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" {{ ($objEmail->list_id != '0') ? 'checked' : ''  }} name="typeOp" type="radio" id="inlineRadio1" value="0" />
                                    <label class="form-check-label" for="inlineRadio1">Chi hội</label>
                                </div>
                                @if ($errors->has('option_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}" id="form-list-police">
                        <label for="branch_id" class="col-md-1 control-label">Chi hội <span class="color-required">*</span></label>
                        <div class="col-md-6" style="">
                            <div class="col-md-5" style="margin-bottom: 10px;padding-left: 0px !important;display: flex; gap: 15px">
                            <select id="branch_id" name="branch_id" class="form-control">
                                @foreach($offices as $key => $office)
                                    <option {{ ($objEmail->branch_id == $office->id) ? 'selected' : '' }} value="{{$office->id}}">{{$office->name}}</option>
                                @endforeach
                            </select>
                                @if ($errors->has('option_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('option_code') ? ' has-error' : '' }}" id="form-list-police-fee">
                        <label for="option_code" class="col-md-1 control-label">Hội phí <span class="color-required">*</span></label>
                        <div class="col-md-6" style="">
                            <select id="hoiphi" name="hoiphi" class="form-control">
                                @foreach($list_fees as $key => $fee)
                                    <option {{ $fee_selected == $fee->id ? 'selected' : '' }} value="{{$fee->id}}">{{$fee->name ? $fee->name : 'Tổng hội'}} - {{ $fee->name }} - {{ $fee->money }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('option_code'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('option_code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('option_code') ? ' has-error' : '' }}">
                        <label for="option_code" class="col-md-1 control-label">Loại email <span class="color-required">*</span></label>
                        <div class="col-md-6" style="">
                            <div class="col-md-5" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <select id="option_code" class="form-control" name="option_code" tabindex="2">
                                    <option value="">Chọn loại giới thiệu</option>
                                    @foreach($arrOptions as $key => $v)
                                        <option value="{{ $v->code }}" {{ (empty(old('option_code')) ? ($objEmail->option_code == $v->code) : (old('option_code') == $v->code)) ? 'selected' : '' }}> {{ $v->value }} </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('option_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('option_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-1 control-label">Tiêu đề email <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ empty(old('title')) ? $objEmail->title : old('title') }}" tabindex="3" maxlength="150" >

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="start_time" class="col-md-1 control-label">Ngày bắt đầu<span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" id="errorDate" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <input type="text" style="" class="date datetime-input" name="start_time" value="{{ empty(old('start_time')) ? ($objEmail->start_time != '' ? (str_replace('-', '/', date('d-m-Y', strtotime($objEmail->start_time)))) : '') : old('start_time') }}" id="start_time" placeholder="" tabindex="4">
                                <span class="datetime-icon fa fa-calendar"></span>
                            </div>
                            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12 expire_date" style=" ">
                                Ngày kết thúc
                            </div>
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" style="margin-bottom: 10px; padding-right: 0px !important; ">
                                <input type="text" style="" class="date datetime-input" name="end_time" value="{{ empty(old('end_time')) ? ($objEmail->end_time != '' ? (str_replace('-', '/', date('d-m-Y', strtotime($objEmail->end_time)))) : '') : old('end_time') }}" id="end_time" placeholder="" tabindex="5">
                                <span class="datetime-icon fa fa-calendar" style="right: 10px !important;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('auto_send_time') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="auto_send_time" class="col-md-1 control-label">Gửi tự động</label>

                        <div class="col-md-6">
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" id="errorAutoSendTime" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <input type="text" style="" class="date datetime-input" name="auto_send_time" value="{{ empty(old('auto_send_time')) ? ($objEmail->auto_send_time != '' ? (str_replace('-', '/', date('d-m-Y', strtotime($objEmail->auto_send_time)))) : '') : old('auto_send_time') }}" id="auto_send_time" placeholder="" tabindex="6">
                                <span class="datetime-icon fa fa-calendar"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('attach') ? ' has-error' : '' }}" style="margin-bottom: 5px !important;">
                        <label for="attach" class="col-md-1 control-label">File đính kèm</label>
                        <div class="col-md-6">
                            <div>
                                <label class="radio-inline" for="attach" style="padding-left: 0px;">
                                    <span id="pdf_name1" style="display: none;"><i class="fa fa-upload" aria-hidden="true"></i>Tải lên file pdf</span>
                                    <label id="pdf_name"><i class="fa fa-upload" aria-hidden="true"></i>Tải lên file pdf</label>
                                    <input type="file" name="attach" id="attach" style="color:transparent;">
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status" class="col-md-1 control-label">Trạng thái <span class="color-required">*</span></label>
                        <div class="col-md-6" style="padding-top: 5px">
                            <div class="col-md-6" style="padding-top: 5px; padding-left: 0px !important;">
                                <div class="" style="padding-left: 0px !important;" id="statusCheck">
                                    <label class="radio-inline" for="statushd">
                                        <input id="statushd" type="radio" tabindex="7" name="status" value="1" {{ empty(old('status')) ? ($objEmail->status == 1 ?  'checked' : '') :  (old('status') == 1 ? 'checked' : '') }}> Hoạt động
                                    </label>
                                    <label class="radio-inline" for="statusln" >
                                        <input id="statusln" type="radio" tabindex="8" name="status" value="0" {{ empty(old('status')) ? ($objEmail->status == 0 ?  'checked' : '') :  (old('status') == 0 ? 'checked' : '') }}>
                                        Lưu nháp
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="col-md-1 control-label"></label>

                        <div class="col-md-10" style="padding-top: 5px">
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Tên thay bằng $PLACEHOLDER_TEN$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Mã hồ sơ thay bằng $PLACEHOLDER_MAHOSO$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Mã hội viên thay bằng $PLACEHOLDER_MAHOIVIEN$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Link phiếu thông tin thay bằng $PLACEHOLDER_URL_PHIEUTHONGTIN$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Link đơn đăng ký thay bằng $PLACEHOLDER_URL_DONDANGKY$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Tên đăng nhập thay bằng $PLACEHOLDER_USERNAME$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Mật khẩu thay bằng $PLACEHOLDER_PASSWORD$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Trang chủ thay bằng $PLACEHOLDER_URL_TRANGCHU$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Hội phí thay bằng $PLACEHOLDER_HOIPHI$</label>
                            </div>
                            <div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Hội phí đã được nộp thay bằng $PLACEHOLDER_HOIPHI_DANOP$</label>
                            </div>
							<div class="col-md-12" style="padding-top: 5px; padding-left: 0px !important;">
                                <label style="color: red;">*Tải về đơn đăng ký gia nhập thay bằng: $PLACEHOLDER_TAI_DK$  -
								Tải về phiếu thông tin hội viên thay bằng: $PLACEHOLDER_TAI_PHIEU$</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-1 control-label">Nội dung <span class="color-required">*</span></label>

                        <div class="col-md-10" style="padding-top: 10px">
                            <textarea  rows="15" id="content_rg" name="content" tabindex="9" class="form-control position-content tinymce" >{{ empty(old('content')) ? htmlentities($objEmail->content) : old('content') }}</textarea>
                            @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button class="btn btn-primary" type="button" id="updateInfo" tabindex="10">
                                Lưu
                            </button>
                            <a href="{{ url('/officesys/emails') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="11">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer_embed')
    <script>
        $(document).ready(function() {

            changeTypeOp();

            $( "input[name=typeOp]" ).change(function() {
                changeTypeOp();
            });
        });

        function changeTypeOp() {
            let val = $("input[name=typeOp]:checked").val();
            if(val == 0) {
                $("#form-list-police").show();
                $("#form-list-police-fee").show();
            } else {
                $("#form-list-police").hide();
                $("#form-list-police-fee").hide();
            }
        }
        var pdfName = '{{ !empty($objEmail->attach) ? substr($objEmail->attach, strpos($objEmail->attach, 'pdf\\') + 4) : 'Chưa có file đính kèm' }}';

        // Enter to submit form updateForm
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                $("#updateInfo").click();
            }
        });

        // Chuyển chuỗi kí tự (string) sang đối tượng Date()
        function parseDate(str) {
            var mdy = str.split('/');
            return new Date(mdy[2], mdy[1], mdy[0]);
        }
        $(document).on('click', '#updateInfo', function (event) {
            var title       = $("#title").val();
            var option_code   = $("#option_code").val();

            var start_time  = $("#start_time").val();
            var end_time    = $("#end_time").val();
            var auto_send_time = $('#auto_send_time').val();
            //var content     = $("#content_rg").val();
            $(".help-block").remove();
            $("#end_time").removeClass("error_border");
            $("#start_time").removeClass("error_border");
            var flg = 0;
            if(option_code == ''){
                flg = 1;
                $("#option_code").addClass("error_border");
                $( '<span class="help-block"><strong>Loại giới thiệu là trường bắt buộc phải chọn</strong></span>' ).insertAfter( "#option_code" );
            }else{
                $("#option_code").removeClass("error_border");
            }
            if(title.trim() == ''){
                flg = 1;
                $("#title").addClass("error_border");
                $( '<span class="help-block"><strong>Tiêu đề là trường bắt buộc phải điền</strong></span>' ).insertAfter( "#title" );
            }else{
                var titleConver = convertStr(title);
                if(!(/^[a-zA-Z0-9\-\.\/]*$/i).test(titleConver)){
                    flg = 1;
                    $("#title").addClass("error_border");
                    $( '<span class="help-block"><strong>Tiêu đề chỉ được nhập các ký tự a-z A-Z 0-9 -  . /</strong></span>' ).insertAfter( "#title" );//
                }else {
                    $("#title").removeClass("error_border");
                }
            }
            var statusCheck = $("input:radio[name=status]:checked").val();
            var d = new Date();
            var nowDate = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
            var startDate = parseDate(nowDate).getTime();

            if(statusCheck == 1) {
                if (auto_send_time != '') {
                    if(ValidateCustomDate(auto_send_time)){
                        //validation
                        var autoSendTime = parseDate(auto_send_time).getTime();
                        if (startDate > autoSendTime) {
                            flg = 1;
                            $("#auto_send_time").addClass("error_border");
                            $( '<span class="help-block"><strong>Ngày gửi tự động phải lớn hơn ngày hiện tại</strong></span>' ).insertAfter( "#auto_send_time" );
                        }else{
                            $("#auto_send_time").removeClass("error_border");
                        }
                    }else{
                        flg = 1;
                        $("#auto_send_time").addClass("error_border");
                        $( '<span class="help-block"><strong>Định dạng của ngày gửi tự động là dd/mm/yyyy</strong></span>' ).insertAfter( "#auto_send_time" );
                    }
                }
                if (start_time.trim() == '') {
                    flg = 1;
                    // $("#start_time").addClass("error_border");
                    // $("#start_time").attr("style", "border-color: #a94442 !important");
                    // $('<span class="help-block"><strong>Ngày bắt đầu là trường bắt buộc phải điền</strong></span>').insertAfter("#start_time");
                }
                else {
                    $("#start_time").removeClass("error_border");
                }

                if (start_time.trim() != '' && end_time.trim() == '') {
                    //check format
                    if (ValidateCustomDate(start_time)) {
                        //validation
                        var startTime = parseDate(start_time).getTime();
                        if (startDate > startTime) {
                            flg = 1;
                            $("#start_time").addClass("error_border");
                            $('<span class="help-block"><strong>Ngày bắt đầu không được nhỏ hơn ngày hiện tại</strong></span>').insertAfter("#start_time");
                        } else {
                            $("#start_time").removeClass("error_border");
                        }
                    } else {
                        flg = 1;
                        $("#start_time").addClass("error_border");
                        $('<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>').insertAfter("#start_time");
                    }

                }
                else {
                    if (start_time.trim() == '') {
                        flg = 1;
                        $("#start_time").addClass("error_border");
                        $("#start_time").attr("style", "border-color: #a94442 !important");
                        $('<span class="help-block"><strong>Ngày bắt đầu là trường bắt buộc phải điền</strong></span>').insertAfter("#start_time");
                    }
                    else {
                        $("#start_time").removeClass("error_border");
                    }

                    if (start_time.trim() != '' && end_time.trim() == '') {
                        //check format
                        if (ValidateCustomDate(start_time)) {
                            //validation
                            var startTime = parseDate(start_time).getTime();
                            if (startDate > startTime) {
                                flg = 1;
                                $("#start_time").addClass("error_border");
                                $('<span class="help-block"><strong>Ngày bắt đầu không được nhỏ hơn ngày hiện tại</strong></span>').insertAfter("#start_time");
                            } else {
                                $("#start_time").removeClass("error_border");
                            }
                        } else {
                            flg = 1;
                            $("#start_time").addClass("error_border");
                            $('<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>').insertAfter("#start_time");
                        }

                    }
                }

                /*if(end_time.trim() != '' && start_time.trim() == '') {
                 if(ValidateCustomDate(end_time)) {
                 var endTime = parseDate(end_time).getTime();
                 if (startDate > endTime) {
                 flg = 1;
                 $("#end_time").addClass("error_border");
                 $( '<span class="help-block"><strong>Ngày kết thúc phải lớn hơn ngày hiện tại</strong></span>' ).insertAfter( "#end_time" );
                 }else{
                 $("#end_time").removeClass("error_border");
                 }
                 }else{
                 flg = 1;
                 $("#end_time").addClass("error_border");
                 $('<span class="help-block"><strong>Định dạng của ngày kết thúc là dd/mm/yyyy</strong></span>').insertAfter("#end_time");
                 }

                 }*/
                if (end_time.trim() != '' && start_time.trim() != '') {
                    if (ValidateCustomDate(end_time) && ValidateCustomDate(start_time)) {
                        //validation
                        var startTime = parseDate(start_time).getTime();
                        var endTime = parseDate(end_time).getTime();
                        if (startDate > startTime) {
                            flg = 1;
                            $("#start_time").addClass("error_border");
                            $('<span class="help-block"><strong>Ngày bắt đầu không được nhỏ hơn ngày hiện tại</strong></span>').insertAfter("#start_time");
                        } else {
                            $("#start_time").removeClass("error_border");
                        }
                        if (startDate > endTime) {
                            flg = 1;
                            $("#end_time").addClass("error_border");
                            $('<span class="help-block"><strong>Ngày kết thúc phải lớn hơn ngày hiện tại</strong></span>').insertAfter("#end_time");
                        } else {
                            $("#end_time").removeClass("error_border");
                        }
                        if (flg == 0) {
                            if (startTime > endTime) {
                                flg = 1;
                                $("#end_time").addClass("error_border");
                                $("#start_time").addClass("error_border");
                                $('<span class="help-block"><strong>Ngày bắt đầu phải nhỏ hơn ngày kết thúc</strong></span>').insertAfter("#start_time");
                            } else {
                                $("#end_time").removeClass("error_border");
                                $("#start_time").removeClass("error_border");
                            }
                        }
                    } else {
                        if (!ValidateCustomDate(end_time)) {
                            flg = 1;
                            $("#end_time").addClass("error_border");
                            $('<span class="help-block"><strong>Định dạng của ngày kết thúc là dd/mm/yyyy</strong></span>').insertAfter("#end_time");
                        } else {
                            $("#end_time").removeClass("error_border");
                        }
                        if (!ValidateCustomDate(start_time)) {
                            flg = 1;
                            $("#start_time").addClass("error_border");
                            $('<span class="help-block"><strong>Định dạng của ngày bắt đầu là dd/mm/yyyy</strong></span>').insertAfter("#start_time");
                        } else {
                            $("#start_time").removeClass("error_border");
                        }
                    }
                }
            }
            if($('#content_rg_ifr').contents().find('#tinymce').html() == '<p><br data-mce-bogus="1"></p>'){
                flg = 1;
                $( '<span class="help-block"><strong>Nội dung là trường bắt buộc phải điền</strong></span>' ).insertBefore( "#mceu_15" );
            }
            if (flg == 0) {
                $("#updateForm").get(0).submit();
            } else {
                $('.help-block:first').prev().focus();
            }
        });



        function ValidateCustomDate(d) {
            var match = /^(\d{2})\/(\d{2})\/(\d{4})$/.exec(d);
            if (!match) {
                // pattern matching failed hence the date is syntactically incorrect
                return false;
            }
            var day     = parseInt(match[1], 10); // months are 0-11, not 1-12
            var month   = parseInt(match[2], 10) - 1;
            var year    = parseInt(match[3], 10);
            var date    = new Date(year, month, day);
            // now, Date() will happily accept invalid values and convert them to valid ones
            // therefore you should compare input month/day/year with generated month/day/year
            return date.getDate() == day && date.getMonth() == month && date.getFullYear() == year;
        }

        function convertStr(txt){

            var str = txt;
            str= str.toLowerCase();
            str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str= str.replace(/đ/g,"d");
            str= str.replace(/\s/g, '');
            //str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
            //str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
            //str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi
            return str;
        }

        $(document).ready(function () {
            document.getElementById("branch_id").focus();
            $('#pdf_name').text(pdfName);
            $('input[type="file"]').change(function(e){
                $(this).removeAttr('style');
                $('#pdf_name').hide();
                $('#pdf_name1').show();
            });
        });
    </script>
@endsection
