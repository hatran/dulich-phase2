<form class="page-wrap" name="filter" method="get" id="formSearch" action="{{ url ('/officesys/emails') }}" style="margin-top: 10px;">
    {{ csrf_field() }}
    <div class="row" style="max-width:100%; width: 100%; margin-top: 10px;">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="line-height: 34px;">
           Tiêu đề e-mail
        </div>

        <div style="margin-bottom: 10px;" class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <input type="text" max="255" id="title" name="title" value="{{ array_get($response_view, 'title', '') }}" placeholder="Nhập từ khóa muốn tìm kiếm" tabindex="1">
        </div>

        <!-- <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="line-height: 34px;">
            Văn phòng đại diện
        </div>
        <div style="" class="dropfield col-md-4 col-sm-6 col-xs-12" >
            <select name="branch_id" id="option_code" tabindex="2">
                <option value="">Chọn mục muốn tìm kiếm</option>
                @foreach($branches as $key => $value)
                <option value="{{ $value->id }}"
                        @if (array_get($response_view, 'branch_id', '') == $value->id) selected="selected" @endif>{{ $value->name }}</option>
                @endforeach
            </select>
        </div> -->
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="3" id="search_status" name="search_status" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($statusOption))
                    @foreach($statusOption as $key => $val)
                        @php $selected = is_numeric(Input::get('search_status', '')) && Input::get('search_status', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="line-height: 34px;">
            Thời gian áp dụng
        </div>

        <div style="margin-bottom: 10px;" class="field-wrap col-md-4 col-sm-3 col-xs-12">
            <input type="text" placeholder="Ngày tạo" tabindex="4" value="{{ array_get($response_view, 'created_at', '') }}" id="created_at" name="created_at" class="date datetime-input" style="">
            <span class="datetime-icon fa fa-calendar"></span>
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="line-height: 34px;">
            Phạm vi áp dụng
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="3" id="typeOp" name="typeOp" class="form-control">
           
                <option value="">Tất cả</option>
                @php $selected1 = is_numeric(Input::get('typeOp', '')) && Input::get('typeOp', '') == '1' ? 'selected="selected"' : ''; @endphp
                <option  {{$selected1}} value="1">Toàn Quốc</option>
                @php $selected0 = is_numeric(Input::get('typeOp', '')) && Input::get('typeOp', '') == '0' ? 'selected="selected"' : ''; @endphp
                <option {{$selected0}} value="0">Chi Hội</option>
            </select>
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12 form-list-police" style="line-height: 34px;">
            Chi Hội
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12 form-list-police" style="margin-bottom: 10px;">
            <select tabindex="3" id="branch_id" name="branch_id" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($offices))
                    @foreach($offices as $key => $office)
                    @php $selected = is_numeric(Input::get('branch_id', '')) && Input::get('branch_id', '') == $office->id ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$office->id}}">{{$office->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="line-height: 34px;">
            Loại email
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select id="option_code" class="form-control" name="option_code" >
                <option value="">Chọn loại email</option>
                @foreach($arrOptions as $key => $v)
                    <option value="{{ $v->code }}" {{ (Input::get('option_code', '') == $v->code) ? 'selected' : '' }}> {{ $v->value }} </option>
                @endforeach
            </select>
        </div>

        <div style="margin-left: 45%;" class="field-wrap col-md-2 col-sm-3 col-xs-2">
            <input type="submit" class="button btn-primary" name="search" value="Tìm kiếm" tabindex="5" style="width: 140px">
        </div>
    </div>
</form>
<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });

    $(document).ready(function() {

        changeTypeOp();

        $( "select[name=typeOp]" ).change(function() {
            changeTypeOp();
        });
    });

    function changeTypeOp() {
        let val = $("select[name=typeOp]").val();
        if(val == 0) {
            $(".form-list-police").show();
        } else {
            $(".form-list-police").hide();
        }
    }
</script>