<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\MemberPay;
?>

@extends('admin.layouts.app')
@section('title', 'Ký quyết định Hội Viên')
@section('content')
    <div class="page-wrap">
    @include('admin.layouts.steps')
    <!-- CSFR token for ajax call -->
        <meta name="_token" content="{{ csrf_token() }}"/>

        <!-- <div class="heading"> Ký quyết định Hội Viên </div> -->
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.search')
            @include('admin.layouts.message')
            @if(array_get($response_view, 'status', '') == \App\Constants\MemberConstants::SIGNING_WAITING )
              <a class="providing-decision-notification button btn-primary" style="float:  right;width: 120px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)">Cấp quyết định</a>
            @endif
            <div class="total" style="color: #3d3a3a !important;">Tổng số : {{$count_members}}</div>

            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%">STT</td>
                        <td width="5%">Mã hồ sơ</td>
                        <td width="7%">Số thẻ HDV</td>
                        <td width="15%">Họ và tên</td>
                        <td width="7%">Ngày sinh</td>
                        <td width="7%">Ngày <br/>tạo hồ sơ</td>
                        <td width="7%">Ngày <br/>thẩm định</td>
                        <td width="7%">Ngày <br/>phê duyệt</td>
                        <td width="7%">Ngày <br/>nộp Lệ phí <br/> Hội phí</td>
                        <td width="10%">Trạng thái</td>
                        <td width="15%">Chức năng</td>
                        @if(array_get($response_view, 'status', '') == \App\Constants\MemberConstants::SIGNING_WAITING )
                          <td width="5%" style="position: relative; top:0px; color: #144a8b;">Cấp quyết định<br>
                              <input type="checkbox" class="checkAll">
                              <label>&nbsp;</label>
                          </td>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objMembers) == 0)
                        <tr>
                            <td colspan="11" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            <tr class="item{{$objMember->id}}" data-datePayment="{{strtotime(str_replace('/', '-',$objMember->date_payment))}}">
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">
                                    {{ empty($objMember->file_code) ? '' : $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('admin_list_member_detail_view', $objMember->id) }}">{{ $objMember->touristGuideCode }} </a>
                                </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-center"> {{ empty($objMember->createdAt) ? '' : date('d/m/Y', strtotime($objMember->createdAt)) }} </td>
                                <td class="text-center"> {{ empty($objMember->verified_at) ? '' : date('d/m/Y', strtotime($objMember->verified_at)) }} </td>
                                <td class="text-center"> {{ empty($objMember->approved_at) ? '' : date('d/m/Y', strtotime($objMember->approved_at)) }} </td>
                                <td class="text-center"> {{ empty($objMember->date_payment) ? '' : $objMember->date_payment }}</td>
                                @if(empty($objMember->status))
                                    <td class="text-left wrap-text"></td>
                                @else
                                    <td class="text-left">
                                        {{ array_get(MemberConstants::$file_const, $objMember->status, '') }}
                                    </td>
                                @endif

                                <td align="center" class="text-center">
                                    @if($objMember->status == \App\Constants\MemberConstants::SIGNING_WAITING )
                                        <!--<a class="print-decision-modal btn-function" data-id="{{$key + 1}}" data-member="{{ $objMember->id }}"
                                           href="{{ route('member_decision_do_create_card_view',[$objMember->id, 'is_print' => 1]) }}"
                                           target="_blank" data-toggle="tooltip" data-placement="left" title="In quyết định">
                                            <span class="glyphicon glyphicon-print" style="font-size: 16px;"></span>
                                        </a>-->
										<!--style="{{ empty($objMember->is_signed) ? 'display: none;' :  'display: bock;' }}"-->
                                        <a class="item{{$key + 1}} add-decision-modal btn-function"
                                           data-toggle="tooltip"
                                           data-placement="left"
                                           title="Tạo quyết Định"
                                           data-id="{{$objMember->id}}"
                                           data-member-status="{{$objMember->status}}"
                                           data-file-id="-1"
                                           href="javascript:void(0)">
                                            <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #0000ff;"></span> </a>

                                    @elseif($objMember->status == \App\Constants\MemberConstants::CODE_PROVIDING_WAITING)
                                        <a class="detail-decision-modal btn-function"
										   data-id="{{$objMember->id}}"
										   data-member-status="{{$objMember->status}}"
										   data-toggle="tooltip" data-placement="left" title="Xem quyết Định"
										   @if (!empty($objMember->decision))
										   data-action="{{ route('decision',$objMember->id) }}"
										   data-number-decison="{{ array_get($objMember->decision, '0.number_decisive', '') }}"
										   data-leader-id="{{ array_get($objMember->decision, '0.leaderId', '') }}"
										   data-date-sign="{{ (empty(array_get($objMember->decision, '0.sign_date', ''))) ? '' : date('d/m/Y',strtotime(array_get($objMember->decision, '0.sign_date', ''))) }}"
										   data-file-name="{{ array_get($objMember->decision, '0.file.0.originalName', '') }}"
										   data-file-id="{{ array_get($objMember->decision, '0.file.0.id', '') }}"
										   @endif
										   data-index="{{$key + 1}}"
										   href="javascript:void(0)">
											<span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
										</a>
										<a class="btn-function" data-toggle="tooltip" data-placement="left" title="In quyết định"
                                           href="{{ route('member_decision_do_create_card_view',[$objMember->id, 'is_print' => 1]) }}"
                                           target="_blank">
                                            <span class="glyphicon glyphicon-print" style="font-size: 16px;"></span>
                                        </a>

                                        <a class="add-decision-modal btn-function"
                                           data-id="{{$objMember->id}}"
                                           data-member-status="{{$objMember->status}}"
                                           data-toggle="tooltip" data-placement="left" title="Sửa quyết Định"
                                           @if (!empty($objMember->decision))
                                           data-action="{{ route('decision',$objMember->id) }}"
                                           data-number-decison="{{ array_get($objMember->decision, '0.number_decisive', '') }}"
                                           data-leader-id="{{ array_get($objMember->decision, '0.leaderId', '') }}"
                                           data-date-sign="{{ (empty(array_get($objMember->decision, '0.sign_date', ''))) ? '' : date('d/m/Y',strtotime(array_get($objMember->decision, '0.sign_date', ''))) }}"
                                           data-file-name="{{ array_get($objMember->decision, '0.file.0.originalName', '') }}"
                                           data-file-id="{{ array_get($objMember->decision, '0.file.0.id', '') }}"
                                           @endif
                                           data-index="{{$key + 1}}"
                                           href="javascript:void(0)">
                                            <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #009933;"></span>
                                        </a>

                                        <!--<a class="delete-decision-modal btn-function" data-toggle="tooltip"
                                           data-placement="left" title="Hủy Quyết Định"
                                           data-id="{{$objMember->id}}" data-title="{{$objMember->fullName}}"
                                           data-content="{{$objMember->fullName}}" data-leader="{{$objLeaders}}"
                                           href="javascript:void(0)">
                                            <span class="glyphicon glyphicon-trash" style="font-size: 16px; color: #ff0000;"></span>
                                        </a>-->
                                    @endif
                                </td>
                                @if(array_get($response_view, 'status', '') == \App\Constants\MemberConstants::SIGNING_WAITING )
                                  <td class="text-center">
                                      <span class="checkitem">
                                          <input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{ $objMember->id }}" class="checkbox-pr">
                                          <label for="member {{ $key + 1 }}">&nbsp;</label>
                                      </span>
                                  </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>

            <!-- Modal form to delete a form -->
            <div id="deleteModal" class="modal fade" role="dialog">
                <div class="modal-dialog" id="id_delete">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <h4 class="text-center">Bạn có chắn chắn muốn hủy quyết định?</h4>
                            <br/>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                                    Có
                                </button>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">
                                    Không
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal form to add a post -->
            <div id="addModal" class="modal fade" role="dialog">
                <div class="modal-dialog" id="id_show">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"></h4>
                        </div>

                        <div class="modal-body">
                            {{csrf_field() }}
                            <form id="uploaddiamond" class="form-horizontal form-label-left" method="post"
                                  enctype="multipart/form-data">
                                <input type="hidden" id="member-status" />
                                <div class="form-group">

                                    <label class="control-label col-sm-3 align_text" for="number_decision"
                                           id="title_add">Số quyết định
                                        <span class="color-required">*</span> </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="number_decision" value="{{ sprintf('%03d', $decisionNumber->current_code + 1) }}"
                                               maxlength="20" tabindex="1" style="width: 30%; float: left; margin-right: 20px;" disabled="disabled">
                                        <label style="line-height: 2.5; font-weight: normal;"> /QĐ-HHDVDLVN</label>
                                        <span id="name-error" class="help-block text-left errorNumberDecision"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text">Người ký <span class="color-required">*</span></label>
                                    {{--<div class="col-sm-9">--}}
                                    {{--<input type="text" class="form-control" id="leader_sign" autofocus>--}}
                                    {{--</div>--}}
                                    {{--comment dropbox--}}
                                    <div class="col-sm-9 dropdown">
                                        <select class="form-control" id="leader_sign" name="leader_sign" tabindex="2" disabled="disabled" style="padding-left: 7px;">
                                            @if (count($objLeaders) > 0)
                                                @foreach($objLeaders as $key => $objLeader)
                                                    <option value="{{ $objLeader->id }}" selected>{{ $objLeader->fullname }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <span id="name-error" class="help-block text-left errorPersionDecision"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3 align_text" for="id">Ngày ký <span class="color-required">*</span></label>
                                    <div class="col-sm-9">
                                        <input class="date datetime-input" id="date_sign" type="text" value="{{ date('d/m/Y') }}" tabindex="3" disabled="disabled" style="padding-left: 8px;">
                                        <span class="datetime-icon fa fa-calendar"></span>
                                        <span id="name-error" class="help-block text-left errorDateDecision"></span>
                                    </div>
                                </div>

                               <!--  <div class="form-group">
                                    <label class="control-label col-sm-3" for="file_path">File quyết định</label>
                                    <div class="col-sm-9">
                                        <div class="btn-upload">

                                            <div class="btn-upload" id="file-id">

                                                <label id="file_name" class="fa fa-upload" for="result_file" tabindex="4"> Tải lên
                                                    file PDF hoặc
                                                    file ảnh.</label>

                                                <input type="file" name="result_file" id="result_file"
                                                       accept="application/pdf,image/x-png,image/gif,image/jpeg"
                                                       value="">
                                            </div>
                                        </div>
                                        <span id="name-error" class="help-block text-left errorFilePath"></span>
                                    </div>
                                </div> -->

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success savedecision" id="save-decision" tabindex="5">
                                        Lưu
                                    </button>
                                    <button type="button" class="btn btn-warning" data-dismiss="modal" tabindex="6">
                                        Thoát
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            @include('admin.decision.detail')
            @include('admin.layouts.pagination')
        </div>
    </div>
@endsection
@section('footer_embed')
    <style>
        body .help-block {
            color: #a94442;
        }

        .button {
            width: 30px;
            height: 30px;
            line-height: 30px;
            padding: 6px 15px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            margin: 0 0 10px 10px;
            border: 0;
        }

        .btn btn-delete {
            line-height: 30px;
            padding: 6px 6px;
            text-align: center;
            background-color: #fff;
            color: #000 !important;
            border-radius: 3px;
            margin: 0 0 10px 10px;
            border: 0;
        }

        .form-horizontal .control-label {
            padding-top: 7px;
            margin-bottom: 0;
            text-align: left;
        }

        #addModal .btn-upload {
            margin-bottom: 20px;
        }

        #addModal .btn-upload label {
            font-family: "Times New Roman", Times, serif;
            font-size: 14px;
            color: #2481bf;
            text-decoration: underline;
            cursor: pointer;
        }

        #addModal .btn-upload input[type=file] {
            visibility: hidden;
            pointer-events: none;
            height: 0;
        }

        #addModal .btn-upload .spli {
            cursor: pointer;
            font-size: 13px;
            color: #2481bf;
        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }
        .box-shado-error{
            box-shadow: 0px 0px 10px rgb(206, 63, 56);
        }
        .color-required{
            color: red
        }
    </style>

    <script type="text/javascript" src="{{ asset('admin/js/jquery.min.js') }}"></script>
    <!-- Bootstrap JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- toastr notifications -->
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- Delay table load until everything else is loaded -->
    <script type="text/javascript" src="{{ asset('admin/js/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- Delay table load until everything else is loaded -->

    <!-- AJAX CRUD operations -->
    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById("file_code").focus();
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#save-decision").click();
                }
            }
        });
        $(document).on('click', '.show-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            var option = $(this).data('option');
            showPopup(dmId, option, '_show');
        });

        function formatDate(value)
        {
            var resultDate = '';
            if(value != ''){
                var tmpV = value.substr(0, 10);
                var arrval = tmpV.split('-');
                resultDate = arrval['2']+'/'+arrval['1']+'/'+arrval['0'];
            }
            return resultDate;
        }
        var url = "{{ url('/officesys/decision') }}"
        function showPopup(dmId) {
            var modalId = '#_showModal';

            title = 'Chi tiết hội viên';
            $('.modal-title').text(title);

            $.ajax({
                url: url + "/get_detail/" + dmId,
                type: 'GET',
                success: function (response) {
                        console.log(response);
                    if(response.deleted == 'error'){
                        document.location.reload(true);
                    }else{
                        $(modalId).modal('show');
                        $('#file_code_show').text(response.objData.file_code);
                        $('#touristGuideCode_show').text(response.objData.touristGuideCode);
                        $('#fullName_show').text(response.objData.fullName);
                        $('#birthday_show').text(formatDate(response.objData.birthday));
                        $('#createdAt_show').text(formatDate(response.objData.createdAt));
                        $('#verified_at_show').text(formatDate(response.objData.verified_at));
                        $('#approved_at_show').text(formatDate(response.objData.approved_at));
                        $('#date_payment_show').text(response.objData.date_payment);
                    }
                }
            });

        }

        $(document).on('click', '.detail-decision-modal', function () {

            $('.modal-title-detail').text('Xem quyết định hội viên');
            $('#id_show_detail').val($(this).data('id'));
            $('#number_decision_detail').text($(this).data('number-decison'));

            $('#leader_sign_detail').val($(this).data('leader-id'));
            $('#leader_sign_detail_show').text($("#leader_sign_detail option:selected").text());

            // document.getElementById("file_name_detail").innerHTML = ($(this).data('file-name')) ? $(this).data('file-name')
            //         : '';
            $('#date_sign_detail').text($(this).data('date-sign'));
            $('#detailModal').modal('show');
        });

        $(document).on('click', '.add-decision-modal', function () {

            $("#date_sign").removeClass('box-shado-error');
            $("#leader_sign").removeClass('box-shado-error');
            $("#number_decision").removeClass('box-shado-error');
            // $('#file-id').removeClass('box-shado-error');
            $('#member-status').val($(this).attr('data-member-status'));
            $('.modal-title').text('Nhập quyết định hội viên');
            $('#id_show').val($(this).data('id'));
            $('#index').val($(this).data('index'));

            if (typeof $(this).data('number-decison') !== "undefined") {
              var dataDecision = ""+$(this).data('number-decison');
              var spl = dataDecision.split("/");
              $('#number_decision').val(spl[0]);
              $('#number_decision').attr('disabled', false);
            }

            if (typeof $(this).data('leader-id') !== "undefined") {
              $('#leader_sign').val($(this).data('leader-id'));
              $('#leader_sign').attr('disabled', false);
            }

            // $('#file_name').val('1213');
            // document.getElementById("file_name").innerHTML = ($(this).data('file-name')) ? $(this).data('file-name')
            //     : 'Tải lên file PDF hoặc file ảnh.';
            if (typeof $(this).data('date-sign') !== "undefined") {
              $('#date_sign').val($(this).data('date-sign'));
              $('#date_sign').attr('disabled', false);
            }
            id = $('#id_show').val();
            //$('#file-id').val($(this).data('file-id'));
            $('#addModal').modal('show');
            $('.help-block').html('');

            //var number_decision = $("#number_decision").val();
            // $("#number_decision").val('');
            // setTimeout(function() {
            //     $('#number_decision').focus() ;
            //     /$("#number_decision").val(number_decision);
            // }, 300);
        });


        $("#uploaddiamond").on('click', '.savedecision', function (e) {
            e.preventDefault();

            //var file_data = $('#result_file').get(0).files[0];
            //validation ngay ky
            $(".errorDateDecision").html('');
            var d = new Date();
            str = d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear();
            var date_sign = $("#date_sign").val();
            var flg = 0;
            if(date_sign == ''){
                $(".errorDateDecision").removeClass('hidden').html('Ngày ký không được bỏ trống');
                $("#date_sign").addClass('box-shado-error');
                flg = 1;
            }else{
                var startDate = parseDate(str).getTime();
                var endDate = parseDate(date_sign).getTime()/1000;
                var keyU = $('#id_show').val();
                var datePayment = $('tr.item'+keyU).attr('data-datepayment');
                if (startDate < endDate * 1000) {
                    $(".errorDateDecision").removeClass('hidden').html('Ngày ký không được lớn hơn ngày hiện tại.');
                    $("#date_sign").addClass('box-shado-error');
                    flg = 1;
                }

                // else if (datePayment > endDate) {
                    /*$(".errorDateDecision").removeClass('hidden').html('Ngày ký không được nhỏ hơn ngày nộp lệ phí.');
                    $("#date_sign").addClass('box-shado-error');
                    flg = 1;*/
                //}
                else {
                    $(".errorDateDecision").addClass('hidden').html('');
                    $("#date_sign").removeClass('box-shado-error');
                }
            }

            // if(flg == 1){
            //     return false;
            // }

            if ($("#number_decision").val() == '') {
                $(".errorNumberDecision").removeClass('hidden').html('Số quyết định không được bỏ trống');
                $("#number_decision").addClass('box-shado-error');
                flg = 1;
            } else {
                $(".errorNumberDecision").addClass('hidden').html('');
                $("#number_decision").removeClass('box-shado-error');
            }
            if ($('#leader_sign').val() == '' || $('#leader_sign').val() == null) {
                $('#leader_sign').addClass('box-shado-error');
                $('.errorPersionDecision').removeClass('hidden').text("Người ký quyết định không được bỏ trống");
            } else {
                $(".errorPersionDecision").addClass('hidden').html('');
                $("#leader_sign").removeClass('box-shado-error');
            }
            // người ký quyết định

            if(flg == 1){
                return false;
            }

            // //validation file image
            // var fi = document.getElementById('result_file'); // GET THE FILE INPUT.
            // // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
            // $(".errorFilePath").html('');
            // var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'];
            // if (fi.files.length > 0) {
            //     $(".errorFilePath").html('');
            //     $(".errorFilePath").addClass('hidden');
            //     $("#file-id").removeClass('box-shado-error');
            //     // RUN A LOOP TO CHECK EACH SELECTED FILE.
            //     for (var i = 0; i <= fi.files.length - 1; i++) {
            //         var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.
            //         if(Math.round((fsize / 1024)) > 10*1024){
            //             $(".errorFilePath").html('File quyết định phải nhỏ hơn 10M');
            //             flg = 1;
            //         }
            //         if($.inArray($(fi).val().split('.').pop().toLowerCase(), fileExtension) == -1){
            //             $(".errorFilePath").html('File quyết định phải là ảnh hoặc file pdf');
            //             flg = 1;
            //         }
            //     }
            // } else {
            //     // if (!$('label#file_name').html().trim().length) {
            //     //     $('#file-id').addClass('box-shado-error');
            //     //     flag = 1;
            //     //     $('.errorFilePath').removeClass('hidden').text("File quyết định không được bỏ trống");
            //     // } else {
            //     //     flg = 0;
            //     // }
            // }
            if(flg == 1){
                return false;
            } else {
                $(this).text("Đang xử lý ...").attr('disabled', 'disabled');
                var supplier_name = $('#supplier_name').val();
                var form_data = new FormData();
                // if (file_data != undefined) {
                //     form_data.append('file', file_data);
                // }
                if (supplier_name != undefined) {
                    form_data.append('supplier_name', supplier_name);
                }

                form_data.append('number_decision', $('#number_decision').val());
                form_data.append('leader_sign', $('#leader_sign').val());

                form_data.append('date_sign', $('#date_sign').val());
                // form_data.append('file-id', $('#file-id').val());
                //form_data.append('file_name', document.getElementById("file_name").innerHTML);
                form_data.append('member_status', $('#member-status').val());
                $('.savedecision').attr('disabled', 'disabled');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name=_token]').attr('content')
                    }
                });

                $.ajax({
                    url: 'decision/' + id,
                    type: 'POST',
                    data: form_data,
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    success: function (data) {
                        $('.errorNumberDecision').addClass('hidden');
                        // $('.errorFilePath').addClass('hidden');
                        $('.errorPersionDecision').addClass('hidden');
                        $('.errorDateDecision').addClass('hidden');
                        // $('#file-id').removeClass("box-shado-error");
                        if ((data.errors)) {
                            $(this).text("Lưu").removeAttr('disabled');
                            if (data.errors.number_decision) {
                                $('#number_decision').addClass('box-shado-error');
                                $('.errorNumberDecision').removeClass('hidden');
                                $('.errorNumberDecision').text(data.errors.number_decision);
                            } else {
                                $('#number_decision').css("box-shadow", "none");
                            }
                            if (data.errors.leader_sign) {
                                $('#leader_sign').addClass('box-shado-error');
                                $('.errorPersionDecision').removeClass('hidden');
                                $('.errorPersionDecision').text(data.errors.leader_sign);
                            } else {
                                $('#leader_sign').css("box-shadow", "none");
                            }

                            if (data.errors.date_sign) {
                                $('#date-sign').addClass('box-shado-error');
                                $('.errorDateDecision').removeClass('hidden');
                                $('.errorDateDecision').text(data.errors.date_sign);
                            } else {
                                $('#date-sign').css("box-shadow", "none");
                            }

                            // if (data.errors.result_file) {
                            //     $('#file-id').addClass('box-shado-error');
                            //     $('.errorFilePath').removeClass('hidden');
                            //     $('.errorFilePath').text(data.errors.result_file);
                            // } else {
                            //     $('#file-id').removeClass("box-shado-error");
                            // }
                            var timeOut = 2000;
                            var messageError = '';
                            if (data.errors.memberDelete) {
                                messageError = data.errors.memberDelete;
                            } else if(data.errors.memberExecuted) {
                                messageError = data.errors.memberExecuted;
                            }
                            if (messageError.length > 0) {
                                toastr.error(messageError, 'Thông báo', {timeOut: 1000});
                                setTimeout(function () {
                                    location.reload(true);
                                }, timeOut);
                            }else {
                                toastr.success('Quyết định thành công!', 'Thông báo', {timeOut: 1000});
                                setTimeout(function () {// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                                }, 300);

                            }
                        } else {
                            toastr.success('Quyết định thành công!', 'Thông báo', {timeOut: 1000});
                            setTimeout(function () {// wait for 5 secs(2)
                                location.reload(); // then reload the page.(3)
                            }, 300);

                        }
                    }
                });
            }
        });

        $(document).on('click', '.print-decision-modal', function (e) {
            e.preventDefault();
            var memberId = $(this).data('member');
            var flg = 1;
            var _this = $(this);
            var _print = this;
            $.ajax({
                url: url + "/checkprovidingdecision/" + memberId,
                type: 'GET',
                success: function (response) {
                    if(response.data == 'error'){
                        alert('Bạn cần ký quyết định trước khi in');
                    }
                    else {
                      window.open(_print.href);
                      id = _this.data('id');
                      $('.item' + id).show();
                      _this.find('.item' + id).show();
                    }
                }
            });
        });


        // $(document).on('click', '.clear-path-modal', function () {
        //     // document.getElementById("result_file").value = "";
        //     $('#result_file').val('abc.png');

        // });

        // delete a decision
        $(document).on('click', '.delete-decision-modal', function () {

            $('.modal-title').text('Hủy quyết định');
            $('#id_delete').val($(this).data('id'));
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();

        });
        $('.modal-footer').on('click', '.delete', function () {
            $.ajax({
                type: 'DELETE',
                url: 'decision/' + id,
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function (data) {
                    toastr.success('Hủy quyết định thành công!', 'Hủy quyết đinh', {timeOut: 300});
                    location.reload();
                }
            });
        });

        $('body').on('change', '.checkAll', function() {
            $(".checkbox-pr").prop('checked',$(this).is(":checked"));
            if ($(this).is(":checked") == true) {
                $(".checkbox-pr").attr('data-check', 'yes');
            }
            else {
                $(".checkbox-pr").attr('data-check', 'no');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;

            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                    if ($(this).is(':checked')){
                        anyChecked = true;
                    } else {
                        anyChecked = false;
                    }
                });
                if (anyChecked == true) {
                    __this.attr("data-check", "yes");

                    if (allRowLength == allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");

                __this.attr('data-check', 'no');
            }
        });

        function checkedNames() {
            check = 0;
            $("input[name=member]:checked").each( function () {
                check = 1;
            });
            return check;
        }

        $(".providing-decision-notification").click(function() {
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('admin_member_providing_decision_many') }}' + '?list_id=' + printData;
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        //upload file
        // $(document).on('change', 'input[type=file]', function () {
        //     var filename = $(this)[0].files[0].name,
        //         $parent = $(this).parent(),
        //         /*autoRemove = $parent.attr('data-auto-remove'),*/
        //         $label = $parent.find("label");
        //     $label.removeClass("fa fa-upload");
        //     var currentText = $label.text();
        //     $label.text(filename);
        //     if ($parent.find(".spli.icon-close").length === 0) {
        //         $parent.prepend("<span class='spli icon-close'></span>")
        //             .find(".spli.icon-close")
        //             .click(function () {
        //                 $parent.find("input[type=file]").val('');
        //                 $parent.find("label")
        //                     .addClass("fa fa-upload")
        //                     .text(currentText);
        //                 $(this).remove();
        //                 /*if (autoRemove) {
        //                     $(this).parent().remove();
        //                 }
        //                 else {
        //                     $(this).remove();
        //                 }*/
        //             });
        //     }
        // });
    </script>
@endsection
