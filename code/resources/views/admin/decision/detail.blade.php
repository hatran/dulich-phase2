<!-- Modal form to show a post -->
<div id="detailModal" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_show_detail">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title-detail"></h4>
            </div>

            <div class="modal-body">
                {{csrf_field() }}
                <form id="uploaddiamond" class="form-horizontal form-label-left" method="post"
                      enctype="multipart/form-data">
                    <div class="form-group">

                        <label class="control-label col-sm-3 align_text" for="number_decision_detail"
                               id="title_add">Số quyết định </label>
                        <div class="col-sm-9" style="padding-top: 7px">
                            <span id="number_decision_detail"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3 align_text">Người ký</label>

                        <div class="col-sm-9 dropdown" style="padding-top: 7px">
                            <select class="form-control" id="leader_sign_detail" name="leader_sign_detail"  style="display: none">
                                @if (count($objLeaders) > 0)
                                    @foreach($objLeaders as $key => $objLeader)
                                        <option value="{{ $objLeader->id }}">{{ $objLeader->fullname }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <span id="leader_sign_detail_show"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3 align_text" for="id">Ngày ký</label>
                        <div class="col-sm-9" style="padding-top: 7px">

                            <span id="date_sign_detail"></span>
                        </div>
                    </div>

                   <!--  <div class="form-group">
                        <label class="control-label col-sm-3" for="file_path">File quyết định</label>
                        <div class="col-sm-9" style="padding-top: 7px">
                            <div class="btn-upload">

                                <div class="btn-upload" id="file-id-detail">

                                    <label id="file_name_detail" class="" for="result_file" style="font-weight: normal"></label>

                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal" >
                            Thoát
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>