
@extends('admin.layouts.app')
@section('title', 'Quản trị công ty lữ hành tham gia đánh giá, xếp hạng HDV')

@section('content')
    <div class="page-wrap">
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.message')
            @include('admin.traveler_company_rank.search')
            <div class="total" style="margin-bottom: 10px">Tổng số : {{ $count_travelers }}<input class="import_view-modal button btn-primary btn-function add-modal" value="Thêm mới" id="btnAddTraveler" style="float:  right;width: 120px; height: 37px; line-height: 24px; padding: 6px 12px !important; cursor: pointer" data-file-id="-1" tabindex="5"></div>

            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%"><div class="th-inner sortable both">STT</div></td>
                        <td width="10%"><div class="th-inner sortable both">Mã số thuế</div></td>
                        <td width="20%"><div class="th-inner sortable both">Tên công ty</div></td>
                        <td width="15%"><div class="th-inner sortable both">Địa chỉ</div></td>
                        <td width="10%"><div class="th-inner sortable both">Email</div></td>
                        <td width="10%"><div class="th-inner sortable both">SĐT</div></td>
                        <td width="10%"><div class="th-inner sortable both">GPKD</div></td>
                        <td width="10%"><div class="th-inner sortable both">Người tạo</div></td>
                        <td width="10%"><div class="th-inner sortable both">Ngày tạo</div></td>
                        <td width="10%"><div class="th-inner sortable both">Trạng thái</div></td>
                        <td><div class="th-inner sortable both">Chức năng</div></td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objTravelers) == 0)
                        <tr>
                            <td colspan="11" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objTravelers as $key => $value)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center"> {{ $value->tax_code }} </td>
                                <td class="text-left">
                                    {{ mb_strlen($value->company_name, 'UTF-8') > 100 ? mb_substr($value->company_name, 0, 100, 'utf-8') .'...' :  $value->company_name  }}
                                </td>

                                <td class="text-left">
                                    {{ $value->address }}
                                </td>
                                <td class="text-left">
                                    {{ $value->email }}
                                </td>
                                <td class="text-left">
                                    {{ $value->phone }}
                                </td>
                                <td class="text-center">
                                    {{ $value->business_certificate }}
                                </td>
                                 <td>{{ !empty($value->created_author) ? $value->created_author : '' }}</td>
                                <td class="text-center">{{ !empty($value->created_at) ? date('d.m.Y', strtotime($value->created_at)) : '' }}</td>
                                <td class="text-left"> {{ $value->status == 0 ? 'Không hoạt động' : 'Hoạt động' }} </td>
                                <td class="text-center">
                                    <a class="btn-function show-modal" href="#" data-toggle="tooltip" data-placement="left" title="Xem công ty lữ hành" data-id="{{ $value->id }}"  data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                    </a>
                                    <a class="btn-function edit-modal" href="#" data-toggle="tooltip" data-placement="left" title="Sửa công ty lữ hành" data-id="{{ $value->id }}"  data-status="{{ $value->status }}" data-approved="">
                                        <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                    </a>
                                    <a class="btn-function" href="javascript:void(0);" data-toggle="tooltip" data-placement="left" title="Xoá công ty lữ hành" onclick="delete_traveler('{{ $value->id }}', '{{strip_tags($value->company_name)}}')" data-id="{{ $value->id }}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                    </a>
                                    <a class="btn-function" href="javascript:void(0);" title="Gửi email quên mật khẩu" onclick="send_forgot_password('{{ $value->email }}')">
                                        <span class="glyphicon glyphicon-send" style="font-size: 16px;"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>

    @include('admin.traveler_company_rank.detail')
    @include('admin.traveler_company_rank.register')
    @include('admin.traveler_company_rank.edit')
    @include('admin.traveler_company_rank.delete')
    @include('admin.traveler_company_rank.forgot_password')

@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }
        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .sortable{
            color: #144a8b;
            font-weight: bold;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }

        #closeButton ul {
            padding-left: 0px !important;
        }

        #id {
            -moz-appearance: textfield;
            height: 35px;
            line-height: 35px;
            padding-right: 30px;
            position: relative;
            width: 100%;
            border: 1px solid #bbbaba;
            border-radius: 2px;
            display: block;
            max-width: 100%;
            margin-bottom: 10px;
            padding-left: 10px;
        }
        .rq-star{
            color: red
        }
        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }
        span.error-msg {
            color: #a94442;
        }
        span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }
        .total {
            border: 0 none;
            border-radius: 3px;
            color: #0000f0 !important;
            font-size: 16px;
            line-height: 30px;
            padding: 6px 10px;
        }

        .modal-title {
            margin: 0;
            line-height: 1.42857143;
            font-weight: bold;
            width: 50%;
            float: left;
            font-size: 20px;
        }
    </style>
    <script>
        function delete_traveler(id, txt) {
            jQuery("#deleteModal").modal();
            jQuery('#id-em').text(txt);
            jQuery('#id_delete').val(id);
        }
        function send_forgot_password(email) {
            jQuery("#forgotPasswordModal").modal();
            jQuery('#sendEmail').val(email);
        }
        function deleteCat() {
            window.location = '/officesys/travelercompanyrank/deleteall/'+jQuery('#id_delete').val();
        }
        function convertStr(txt){
            var str = txt;
            str= str.toLowerCase();
            str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str= str.replace(/đ/g,"d");
            str= str.replace(/\s/g, '');
            return str;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Enter to submit form
        document.body.addEventListener('keydown', function(e) {
            var key = e.which;
            if (key == 13) {
                if($("#addModal").css('display') == 'block'){
                    $("#btnsubmit").click();
                }
            }
        });
        function clearErrorFields(postfix) {
            $('#companyNameAll'+postfix).removeClass('error_border');
            $('#taxCodeAll'+postfix).removeClass('error_border');
            $('#addressAll'+postfix).removeClass('error_border');
            $('#emailAll'+postfix).removeClass('error_border');
            $('#phoneAll'+postfix).removeClass('error_border');
            $('.errorCompanyNameAll'+postfix).addClass('hidden');
            $('.errorTaxCodeAll'+postfix).addClass('hidden');
            $('.errorAddressAll'+postfix).addClass('hidden');
            $('.errorEmailAll'+postfix).addClass('hidden');
            $('.errorPhoneAll'+postfix).addClass('hidden');
        }

        var url = "{{ url('/officesys/travelercompanyrank') }}"
        function htmlEntities(str) {
            return str;
        }
        function showPopup(dmId, postfix) {
            var title, canEdit = false;
            var modalId = '#' + postfix + 'Modal';

            switch (postfix) {
                case '_add':
                    console.log($('#companyNameAll'+postfix).focus());
                    title = 'Thêm mới công ty lữ hành';
                    canEdit = true;
                    break;

                case '_edit':
                    title = 'Cập nhật công ty lữ hành';
                    $("#id_edit").val(dmId);
                    canEdit = true;
                    break;

                default:
                    title = 'Chi tiết công ty lữ hành';
            }
            clearErrorFields(postfix);
            $('.modal-title'+postfix).text(title);
            if(postfix != '_add'){
                $.ajax({
                    url: url + "/" + dmId,
                    type: 'GET',
                    success: function (response) {
                        var companyNameAll = '';
                        var taxCodeAll = '';
                        var addressAll = '';
                        var phoneAll = '';
                        var emailAll = '';
                        var businessCertificateAll = '';
                        var statusAll = '';
                        if(response.deleted == 'error'){
                            document.location.reload(true);
                        }else{
                            $(modalId).modal('show');
                           
                            companyNameAll = response.objData.company_name;
                            taxCodeAll = response.objData.tax_code;
                            statusAll = response.objData.status;
                            addressAll = response.objData.address;
                            phoneAll = response.objData.phone;
                            emailAll = response.objData.email;
                            businessCertificateAll = response.objData.business_certificate;
                            if (canEdit === true) {
                                $('#companyNameAll'+postfix).val(companyNameAll);
                                $('#taxCodeAll'+postfix).val(taxCodeAll);
                                $('#taxCode'+postfix).text(taxCodeAll);
                                $('#addressAll'+postfix).val(addressAll);
                                $('#emailAll'+postfix).val(emailAll);
                                $('#phoneAll'+postfix).val(phoneAll);
                                $('#businessCertificateAll'+postfix).val(businessCertificateAll);
                                var statusRadio = $("input[name='statusAll_edit']");
                                statusRadio.each(function () {
                                    if (statusAll == $(this).val()) {
                                        $(this).prop('checked', 'checked');
                                    }
                                });
                            } else {
                                $('#companyNameAll_show').text(companyNameAll);
                                $('#taxCodeAll_show').text(taxCodeAll);
                                $('#addressAll_show').text(addressAll);
                                $('#phoneAll_show').text(phoneAll);
                                $('#emailAll_show').text(emailAll);
                                $('#businessCertificateAll_show').text(businessCertificateAll);
                                var messStatus = '';
                                if (statusAll == 1) {
                                    messStatus = 'Họat động';
                                }
                                else {
                                    messStatus = 'Không họat động';
                                }
                                $('#statusAll_show').text(messStatus);
                                
                            }
                            if (postfix == '_edit') {
                                setTimeout(function () {
                                    $("#companyNameAll"+postfix).focus();
                                }, 500);
                            }
                        }
                    }
                });
            }else{
                $(modalId).modal('show');
                $("#companyNameAll"+postfix).val('');
                $("#taxCodeAll"+postfix).val('');
                $("#addressAll"+postfix).val('');
                $("#phoneAll"+postfix).val('');
                $("#emailAll"+postfix).val('');
                $("#businessCertificateAll"+postfix).val('');
                setTimeout(function () {
                    $("#companyNameAll"+postfix).focus();
                }, 500);
            }
        }

        $(document).on('click', '.show-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            showPopup(dmId, '_show');
        });

        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            showPopup('', '_add');
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            var dmId = $(this).data('id');
            $('input[type=hidden]#dm-id').val(dmId);
            showPopup(dmId, '_edit');
        });


        function doAction(dmId, postfix) {
            var action = 'POST';
            var modalId = '#' + postfix + 'Modal';

            var companyNameAll = $("#companyNameAll" + postfix);
            var taxCodeAll = $("#taxCodeAll" + postfix);
            var addressAll = $("#addressAll" + postfix);
            var phoneAll = $("#phoneAll" + postfix);
            var emailAll = $("#emailAll" + postfix);

            if (postfix == '_add')  dmId = 'new';
            if (postfix == '_add' || postfix == '_edit') {
                clearErrorFields(postfix)
            }
            var flg = 0;
            if (postfix == '_add' || postfix == '_edit') {
                if (companyNameAll.val().trim() == '') {
                    $("#companyNameAll"+postfix).addClass('error_border');
                    $(".errorCompanyNameAll"+postfix).html('Tên công ty lữ hành là trường bắt buộc phải điền');
                    $(".errorCompanyNameAll"+postfix).removeClass('hidden');
                    flg = 1;
                }

                if (taxCodeAll.val().trim() == '') {
                    $("#taxCodeAll"+postfix).addClass('error_border');
                    $(".errorTaxCodeAll"+postfix).html('Mã số thuế là trường bắt buộc phải điền');
                    $(".errorTaxCodeAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorTaxCodeAll"+postfix).html('');
                    $(".errorTaxCodeAll"+postfix).addClass('hidden');
                    $("#taxCodeAll"+postfix).removeClass('error_border');
                }

                if (addressAll.val().trim() == '') {
                    $("#addressAll"+postfix).addClass('error_border');
                    $(".errorAddressAll"+postfix).html('Địa chỉ là trường bắt buộc phải điền');
                    $(".errorAddressAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorAddressAll"+postfix).html('');
                    $(".errorAddressAll"+postfix).addClass('hidden');
                    $("#addressAll"+postfix).removeClass('error_border');
                }

                if (phoneAll.val().trim() == '') {
                    $("#phoneAll"+postfix).addClass('error_border');
                    $(".errorPhoneAll"+postfix).html('Số điện thoại là trường bắt buộc phải điền');
                    $(".errorPhoneAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    $(".errorPhoneAll"+postfix).html('');
                    $(".errorPhoneAll"+postfix).addClass('hidden');
                    $("#phoneAll"+postfix).removeClass('error_border');
                }

                if (emailAll.val().trim() == '') {
                    $("#emailAll"+postfix).addClass('error_border');
                    $(".errorEmailAll"+postfix).html('Email là trường bắt buộc phải điền');
                    $(".errorEmailAll"+postfix).removeClass('hidden');
                    flg = 1;
                } else {
                    var emailAll = emailAll.val();
                    if(!(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(emailAll)){
                        flg = 1;
                        $("#emailAll"+postfix).addClass('error_border');
                        $(".errorEmailAll"+postfix).html('Email không hợp lệ');
                        $(".errorEmailAll"+postfix).removeClass('hidden');
                    }else {
                        $(".errorEmailAll"+postfix).html('');
                        $(".errorEmailAll"+postfix).addClass('hidden');
                        $("#emailAll"+postfix).removeClass('error_border');

                    }
                }
            }

            if (flg == 0) {
                $("#travelerForm"+postfix).submit();
                var modalId = '#' + postfix + 'Modal';
                $(modalId).modal('hide');
            }
        }

        $('.modal-footer').on('click', '.add', function () {
            doAction('', '_add');
        });

        $('.modal-footer').on('click', '.edit', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_edit');
        });

        $('.modal-footer').on('click', '.delete', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_delete');
        });
        $(document).ready(function () {
            var nameall = $("#companyNameAll").val();
            $("#companyNameAll").val('');
            setTimeout(function() {
                $('#companyNameAll').focus() ;
                $("#companyNameAll").val(nameall);
            }, 100);
        });
    </script>
@endsection