<!-- Modal -->
<div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="forgotPasswordModalLabel">Gửi email quên mật khẩu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Bạn có đồng ý gửi lại mật khẩu hay không?</h4>
                <br/>
            </div>
            <form action="{{ url('/forgot-password') }}" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <input type="hidden" id="sendEmail" name="email" value=""/>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Gửi</button>
                    <button type="button" class="btn btn-warning"
                            data-dismiss="modal">Thoát</button>
                </div>
            </form>
        </div>
    </div>
</div>