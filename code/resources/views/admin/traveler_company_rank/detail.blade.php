<div id="_showModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_show"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="table-line">
                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone" class="col-md-3 control-label">Tên công ty</label>
                                <div class="col-md-9">
                                    <span id="companyNameAll_show"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone" class="col-md-3 control-label">Mã số thuế</label>
                                <div class="col-md-9">
                                    <span id="taxCodeAll_show"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone" class="col-md-3 control-label">Địa chỉ</label>
                                <div class="col-md-9">
                                    <span id="addressAll_show"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone" class="col-md-3 control-label">Số điện thoại</label>
                                <div class="col-md-9">
                                    <span id="phoneAll_show"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone" class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">
                                    <span id="emailAll_show"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone" class="col-md-3 control-label">GPKD Lữ hành Quốc tế</label>
                                <div class="col-md-9">
                                    <span id="businessCertificateAll_show"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="statusAll_show" style="padding-top: 7px">Trạng thái</label>
                                <div class="col-md-9">
                                     <span id="statusAll_show"></span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="40">
                            Thoát
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
