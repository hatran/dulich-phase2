<div id="_editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_edit"></h4>
            </div>
            <form action="{{url('/officesys/travelercompanyrank/check_traveler')}}" id="travelerForm_edit" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id_edit" id="id_edit" class="form-control" value=""/>
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label" style="padding-top: 7px">Tên công ty<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="companyNameAll_edit" id="companyNameAll_edit" maxlength="191" tabindex="201">
                                        <span class="errorCompanyNameAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label" style="padding-top: 7px">Mã số thuế<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <span id="taxCode_edit"></span>
                                        <input type="hidden" name="taxCodeAll_edit" id="taxCodeAll_edit">
                                        <span class="errorTaxCodeAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label" style="padding-top: 7px">Địa chỉ<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="addressAll_edit" id="addressAll_edit" maxlength="191" tabindex="202">
                                        <span class="errorAddressAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label" style="padding-top: 7px">Số điện thoại<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="phoneAll_edit" id="phoneAll_edit" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="191" tabindex="203">
                                        <span class="errorPhoneAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label" style="padding-top: 7px">Email<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="emailAll_edit" id="emailAll_edit" maxlength="191" tabindex="204">
                                        <span class="errorEmailAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 15px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label" style="padding-top: 7px">GPKD Lữ hành Quốc tế</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="businessCertificateAll_edit" id="businessCertificateAll_edit" maxlength="191" tabindex="205">
                                        <span class="errorBusinessCertificateAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="statusAll_edit" style="padding-top: 7px">Trạng thái<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="radio" name="statusAll_edit" value="1" tabindex="206" checked> Hoạt động
                                        <input type="radio" name="statusAll_edit" value="0" tabindex="207" style="margin-left: 50px;"> Không hoạt động<br>
                                        <span class="errorStatusAll_edit text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary edit" type="button" tabindex="208">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="209">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
