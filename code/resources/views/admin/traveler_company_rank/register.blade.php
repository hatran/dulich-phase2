<div id="_addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-title_add"></h4>
            </div>
            <form action="{{url('/officesys/travelercompanyrank/check_traveler')}}" id="travelerForm_add" class="form-horizontal"  method="post">
                {{ csrf_field() }}
                <div class="table-line modal-body">
                    <div class="table-line">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="companyNameAll_add" class="col-md-3 control-label" style="padding-top: 7px">Tên công ty<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="companyNameAll_add" id="companyNameAll_add" maxlength="100" tabindex="101">
                                        <span class="errorCompanyNameAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="taxCodeAll_add" class="col-md-3 control-label" style="padding-top: 7px">Mã số thuế<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="taxCodeAll_add" id="taxCodeAll_add" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="10" tabindex="102">
                                        <span class="errorTaxCodeAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="addressAll_add" class="col-md-3 control-label" style="padding-top: 7px">Địa chỉ<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="addressAll_add" id="addressAll_add" maxlength="100" tabindex="103">
                                        <span class="errorAddressAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phoneAll_add" class="col-md-3 control-label" style="padding-top: 7px">Số điện thoại<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="phoneAll_add" id="phoneAll_add" onkeypress="return isNumber(event)" onpaste="return onPasteNumber(event)" maxlength="20" tabindex="104">
                                        <span class="errorPhoneAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="emailAll_add" class="col-md-3 control-label" style="padding-top: 7px">Email<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="emailAll_add" id="emailAll_add" maxlength="191" tabindex="105">
                                        <span class="errorEmailAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="businessCertificateAll_add" class="col-md-3 control-label" style="padding-top: 7px">GPKD Lữ hành Quốc tế</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="businessCertificateAll_add" id="businessCertificateAll_add" maxlength="191" tabindex="106">
                                        <span class="errorBusinessCertificateAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="statusAll_add" style="padding-top: 7px">Trạng thái<span class="rq-star"> *</span></label>
                                    <div class="col-md-9">
                                        <input type="radio" name="statusAll_add" value="1" tabindex="107" checked> Hoạt động
                                        <input type="radio" name="statusAll_add" value="0" tabindex="108" style="margin-left: 50px;"> Không hoạt động<br>
                                        <span class="errorStatusAll_add text-left hidden error-msg"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button id="btnsubmit" class="btn btn-primary add" type="button" tabindex="109">
                                Lưu
                            </button>
                            <button data-dismiss="modal" class="btn btn-warning" type="button" tabindex="110">
                                Thoát
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>