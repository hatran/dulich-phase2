<div class="modal fade" id="createnewModal" role="dialog" aria-hidden="true" style="z-index: 9999">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="font-size: 20px;">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -10px">
                    <span aria-hidden="true"
                          style="font-size: 39px; width: 30%;float: right;text-align: right;padding-right: 20px;">&times;</span>
                </button>
            </div>
            <form action="" id="officesaddnew" class="form-horizontal" role="form"
                  enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id" class="form-control" value="-1"/>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row" style="padding: 1rem;">
                            <div class="container-fluid shadow">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4"
                                                               for="field1"><?= Lang::get('offices.code'); ?><a
                                                                    style="color: red">*</a></label>
                                                        <div class="controls col-sm-8">
                                                            <input id="field1" type="text"
                                                                   class="form-control k-textbox required"
                                                                   data-role="text" maxlength="10"
                                                                   data-parsley-errors-container="#errId1"
                                                                   name="code" tabindex="100">
                                                            <span class="error">Mã VPĐD không được bỏ trống</span>
                                                            <span class="error1" style="color: #992100">Thêm mới VPĐD không thành công, mã văn phòng đại diện đã tồn tại</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4"
                                                               for="field2"><?= Lang::get('offices.name'); ?><a
                                                                    style="color: red">*</a></label>
                                                        <div class="controls col-sm-8">

                                                            <input id="field2" type="text"
                                                                   class="form-control k-textbox required"
                                                                   data-role="text"
                                                                   data-parsley-errors-container="#errId2"
                                                                   name="name" maxlength="150" tabindex="101"
                                                            />
                                                            <span class="error">Tên VPĐD không được bỏ trống</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4"
                                                               for="field3"><?= Lang::get('offices.phone'); ?><a
                                                                    style="color: red">*</a></label>
                                                        <div class="controls col-sm-8">

                                                            <input id="field3" type="tel"
                                                                   class="form-control k-textbox required"
                                                                   data-role="text" maxlength="50" tabindex="102"
                                                                   data-parsley-errors-container="#errId3"
                                                                   name="phone"
                                                            />
                                                            <span class="error">Số điện thoại VPĐD không được bỏ trống</span>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4"
                                                               for="field4"><?= Lang::get('offices.email'); ?><a
                                                                    style="color: red">*</a></label>
                                                        <div class="controls col-sm-8">
                                                            <input id="field4" type="email" maxlength="100"
                                                                   class="form-control k-textbox required"
                                                                   data-role="text"
                                                                   data-parsley-errors-container="#errId4"
                                                                   name="email"
                                                                   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
                                                                   tabindex="103"
                                                            />
                                                            <span class="error">Email VPĐD không được bỏ trống</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4"
                                                               for="status">Trụ sở hội</label>
                                                        <div class="controls col-sm-8">
                                                            <input class="form-check-input" type="checkbox" tabindex="104"
                                                                   name="main_office" value="1" id="main_office">
                                                            <label class="form-check-label" for="main_office">
                                                            </label>
                                                            <p class="error1" style="color: #992100">Thêm mới VPĐD không thành công. Đã tồn tại 1 văn phòng khác là trụ sở</p>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4"
                                                               for="status">Khu Vực</label>
                                                        <div class="controls col-sm-8">
                                                            <select id="area" name="area" class="form-control selectpicker" tabindex="105">
                                                                @if (!empty($area))
                                                                    @foreach($area as $key => $val)
                                                                        <option value="{{$key}}">{{$val}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-4"
                                                               for="status">Trạng thái</label>
                                                        <div class="controls col-sm-8">

                                                            <label class="radio-inline" for="status1">
                                                                <input type="radio" value="1" id="status1" name="status" tabindex="106"
                                                                       data-parsley-errors-container="#error-status1"
                                                                       checked>Hoạt
                                                                động</label>
                                                            <label class="radio-inline" for="status2"
                                                                   style="display: inline-block">
                                                                <input type="radio" value="0" id="status2" name="status" tabindex="107"
                                                                       data-parsley-errors-container="#error-status2">Không
                                                                hoạt động</label>
                                                            <span id="error-status" class="error"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-6">

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="control-label control-label-left col-sm-3"
                                                                       for="field5"><?= Lang::get('offices.upload_image'); ?>
                                                                    <a
                                                                            style="color: red">*</a></label>
                                                                <div class="controls col-sm-9">
                                                                    <input id="upload-image" name="images"
                                                                           type="file" value=""
                                                                           onchange="readURL(this);"
                                                                           style="display: none;">
                                                                    <div style="width:100%">
                                                                        <input type="button" value="Browse..." tabindex="108" onclick="document.getElementById('upload-image').click();"/>
                                                                    </div>
                                                                    <span id="upload-image-error" class="error"
                                                                          style="display: none;">
                                                                        Ảnh đại diện VPĐD không được bỏ trống
                                                                    </span>
                                                                    <img id="blah"
                                                                         src="{{ asset('admin/images/no_image.png') }}"
                                                                         alt="no image" width="100%"/>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left  col-sm-2"
                                                               for="field5"><?= Lang::get('offices.addr'); ?><a
                                                                    style="color: red">*</a></label>

                                                        <div class="controls col-sm-10">
                                                    <textarea id="field5" rows="3"
                                                              class="form-control k-textbox required"
                                                              data-role="textarea" tabindex="109"
                                                              data-parsley-errors-container="#errId5"
                                                              name="address" maxlength="500"
                                                    ></textarea>
                                                            <span class="error">Địa chỉ VPĐD không được bỏ trống</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-2"
                                                               for="field8">
                                                            <?= Lang::get('offices.payment_info'); ?>
                                                        </label>
                                                        <div class="controls col-sm-10">
                                                    <textarea id="field8" rows="3" maxlength="500"
                                                              class="form-control k-textbox"
                                                              data-role="textarea" tabindex="110"
                                                              data-parsley-errors-container="#errId6"
                                                              name="payment_info"
                                                    ></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label control-label-left col-sm-2"
                                                               for="field9"><?= Lang::get('offices.note'); ?></label>
                                                        <div class="controls col-sm-10">
                                                     <textarea id="field9" rows="3" tabindex="111"
                                                               class="form-control k-textbox"
                                                               data-role="textarea" maxlength="500"
                                                               data-parsley-errors-container="#errId7"
                                                               name="note"
                                                     ></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer edit">
                    <button type="submit" class="btn btn-primary" tabindex="112" id="officesaddnewButton"><?= Lang::get('offices.save') ?></button>
                    <button type="button" class="btn btn-warning" tabindex="113"
                            data-dismiss="modal"><?= Lang::get('offices.close') ?></button>
                </div>
                <div class="modal-footer view">
                    <button type="button" class="btn btn-warning"
                            data-dismiss="modal"><?= Lang::get('offices.close') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 9999">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="font-size: 20px;">Xem Thông tin Văn Phòng Đại Diện</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"
                          style="font-size: 39px; width: 30%;float: right;text-align: right;padding-right: 10px;">&times;</span>
                </button>
            </div>
            <div class="content">

            </div>
        </div>
    </div>
</div>

<script>
    function readURL(input) {
        if (validateImg() == true) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        } else {
            jQuery('#blah').attr('src', '{{ asset('admin/images/no_image.png') }}');
        }
    }

    function editOffices(key) {
        jQuery('.error').hide();
        jQuery('.error1').hide();
        jQuery('.is-invalid').removeClass('is-invalid');
        jQuery('#upload-image-error').hide();
        jQuery('#upload-image').val('');
        jQuery("#createnewModal").modal();
        jQuery("#exampleModalLabel").text("<?= Lang::get('offices.editoffices'); ?>");
        jQuery('.form-control').each(function () {
            jQuery(this).val('');
        });
        jQuery('input[type="radio"]').each(function () {
            jQuery(this).prop('checked', false);
        });
        jQuery('input[type="checkbox"]').each(function () {
            jQuery(this).prop('checked', false);
        });
        jQuery('#area').val(jQuery('#area option:first').val());
        jQuery('.form-control').each(function () {
            var inputForm = jQuery(this);
            var nameForm = jQuery(this).attr('name');
            var dataByKey = officesData[key];
            $.each(dataByKey, function (k, v) {
                if (nameForm == k) {
                    inputForm.val(v);
                }
                if (k == 'images') jQuery('#blah').attr('src', '{{url('/files/offices')}}' + v);
                if (inputForm.hasClass('selectpicker') && nameForm == 'area' && k == 'area' && v != '') inputForm.val(v);
            });
        });
        jQuery('input[type="radio"]').each(function () {
            var inputForm = jQuery(this);
            var dataByKey = officesData[key];
            $.each(dataByKey, function (k, v) {
                if (k == 'status' && v == inputForm.val()) inputForm.prop('checked', true);
            });
        });

        jQuery('input[type="checkbox"]').each(function () {
            var inputForm = jQuery(this);
            var dataByKey = officesData[key];
            $.each(dataByKey, function (k, v) {
                console.log(dataByKey);
                if (k == 'option_code' && v == 'HEAD') inputForm.prop('checked', true);
            });
        });

        isViewModal(0);
    }

    function addnewOffice() {
        jQuery('.error').hide();
        jQuery('.error1').hide();
        jQuery('.is-invalid').removeClass('is-invalid');
        jQuery('#upload-image-error').hide();
        jQuery('#upload-image').val('');
        jQuery("#exampleModalLabel").text("<?= Lang::get('offices.addoffice'); ?>");
        jQuery('.form-control').each(function () {
            jQuery(this).val('');
        });
        jQuery('input[type="radio"]').each(function () {
            jQuery(this).prop('checked', false);
        });
        jQuery('input[type="checkbox"]').each(function () {
            jQuery(this).prop('checked', false);
        });
        jQuery('#status1').prop('checked', true);
        jQuery('#area').val(jQuery('#area option:first').val());
        jQuery('#blah').attr('src', '{{ asset('admin/images/no_image.png') }}');
        isViewModal(0);
    }

    function deleteOffices(key) {
        jQuery("#deleteModal").modal();
        var dataByKey = officesData[key];
        var idKey = dataByKey['id'];
        var nameKey = dataByKey['name'];
        jQuery('#inputid-to-delete').val(idKey);
        jQuery('#id-em').text(nameKey);
    }

    jQuery('#officesaddnewButton').click(function (e) {
        e.preventDefault();
        var inputValidate = true;
        jQuery('#upload-image-error').hide();
        if (validateForm() == false) {
            inputValidate = false;
        }

        if (jQuery('#blah').attr('src') == '{{ asset('admin/images/no_image.png') }}') {
            jQuery('#upload-image-error').show();
            jQuery('#upload-image-error').text('Ảnh đại diện VPĐD không được bỏ trống');
            inputValidate = false;
        } else if (validateImg() == false) {
            inputValidate = false;
        }

        if ($("#main_office").is(":checked") == false) {
          console.log("abc");
            $("#main_office").parent().find('.error1').hide();
        }

        var inputs = $("#officesaddnew").serialize();

        $.ajax({
          url: '{{ route('admin_representative_office_check_valid_add_new_office') }}',
          type: 'POST',
          dataType: 'json',
          data: inputs,
          success: function(response) {
            var input = jQuery("#"+response.column);
            if (response.error == 1) {
                input.parent().find('.error').hide();
                $('#main_office').parent().find('.error1').hide();           
                input.addClass('is-invalid');
                input.parent().find('.error1').show();
                input.focus();
                return false;
            }
            else if (response.error == 2) {
                input.parent().find('.error').hide();
                $('#field1').parent().find('.error1').hide();           
                input.addClass('is-invalid');
                input.parent().find('.error1').show();
                input.focus();
                return false;
            }
            else if ($("#field1").val() == "") {
                $("#field1").parent().find('.error1').hide();
                $('#main_office').parent().find('.error1').hide();
                $("#field1").addClass('is-invalid');
                $("#field1").parent().find('.error').show();
                return false;
            }
            else {
                input.removeClass('is-invalid');
                input.parent().find('.error').hide();
                input.parent().find('.error1').hide();
            }

          },
          error: function() { 
            alert('Server connection failed! Retry or check your device.'); 
          },

        });
        
        if (inputValidate == false) {
            e.preventDefault();
            return;
        }
        jQuery("#officesaddnew button[type=submit]").attr("disabled", "disabled");

        if (inputValidate == true) jQuery('#officesaddnew').submit();
        jQuery("#officesaddnew button[type=submit]").prop('disabled', false);
    });
    function ajaxCallBack(retString){
      retVal = retString;
  }
    // jQuery('.form-control.required').keypress(function() {
    //     validateForm();
    // });
</script>
