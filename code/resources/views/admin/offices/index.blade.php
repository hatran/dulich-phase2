@extends('admin.layouts.app')
@section('title', 'Quản trị Văn Phòng Đại Diện')
@section('header_embed')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/bootstrap-table/dist/bootstrap-table.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vita.css') }}"/>
@endsection
@section('content')
    <div class="main-content" style="padding-bottom: 75px;">
        @if (session()->get('error'))
            <div class="alert alert-danger">
                <div>{!! session()->get('error') !!}</div>
            </div>
        @endif
        @if(\Illuminate\Support\Facades\Session::has('success_message'))
            <div class="alert alert-success">
                <div>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</div>
            </div>
        @endif
        @include('admin.offices.search')
        <div class="clearfix"></div>
        <div class="page-wrap">
            <div class="top-navigation row">
            <div class="col-sm-6 col-xs-12 total">
                Tổng số: <?= $total ?>
            </div>
            <div class="col-sm-6 col-xs-12 right">
                <button tabindex="8" type="button" class="button btn-primary add-modal"
                        data-toggle="modal" data-target="#createnewModal" onclick="addnewOffice()">
                    <?= Lang::get('offices.addnew') ?>
                </button>
            </div>
        </div>

            <div class="table-wrap">
            <table id="tableOffice"
                   data-search="true"
                   data-show-columns="true"
                   data-pagination="true"
                   data-pagination-first-text="Trang đầu"
                   data-pagination-last-text="Trang cuối"
            >
                <thead>
                <tr>
                    <th data-sortable="false" data-width="5%" data-field="stt" data-align="center">STT</th>
                    <th data-sortable="false" data-width="7%" data-field="code" data-align="center"><?= Lang::get('offices.code'); ?></th>
                    <th data-sortable="false" data-width="15%" data-field="name" data-align="left"><?= Lang::get('offices.name'); ?></th>
                    <th data-sortable="false" data-width="7%" data-field="area2"  data-align="left">Khu vực</th>
                    <th data-sortable="false" data-width="15%" data-field="address2" data-align="left"><?= Lang::get('offices.addr'); ?></th>
                    <th data-sortable="false" data-width="10%" data-field="phone" data-align="right"><?= Lang::get('offices.phone'); ?></th>
                    <th data-sortable="false" data-width="17%" data-field="email" data-align="left"><?= Lang::get('offices.email'); ?></th>
                    <th data-sortable="false" data-width="20%" data-field="payment_info2" data-align="left" ><?= Lang::get('offices.payment_info'); ?></th>
                    <th data-sortable="false" data-width="18%" data-field="note2"  data-align="left"><?= Lang::get('offices.note'); ?></th>
                    <th data-sortable="false" data-width="8%" data-field="status2"  data-align="left"><?= Lang::get('offices.status'); ?></th>
                    <th data-sortable="false" data-width="5%" data-field="employee"  data-align="center"><?= Lang::get('offices.employee'); ?></th>
                    <th data-sortable="false" data-width="10%" data-field="action"  data-align="center"><?= Lang::get('offices.action'); ?></th>
                </tr>
                </thead>
            </table>
        </div>
        </div>
    </div>
    <div id="branchesManagerModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="font-size: 20px;"></h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    @include('admin.offices.modal.addnew')
    @include('admin.offices.modal.delete')
@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/bootstrap-table-1.9.1/dist/bootstrap-table.min.js') }}"></script>
    <script src="{{ asset('admin/js/offices.js') }}" type="text/javascript"></script>
    <script src="{{ asset('admin/js/bootstrap-table-en-US.min.js') }}" type="text/javascript"></script>
    <script>
        $(document).on('click', '.add-modal', function (event) {
            event.preventDefault();
            setTimeout(function () {
                $('#field1').focus();
            }, 500);
        });

        $(document).on('click', '.edit-modal', function (event) {
            event.preventDefault();
            setTimeout(function () {
                $('#field1').focus();
            }, 500);
        });

        $(document).ready(function () {
            var codec = $("#code").val();
            $("#code").val('');
            setTimeout(function() {
                $('#code').focus() ;
                $("#code").val(codec);
            }, 100);
        });

        var officesData = <?= $arrayOffices?>;
        jQuery('#tableOffice').bootstrapTable({
            data: officesData,
            pageNumber: <?= $paginator ?>,
            search: false,
            showColumns: false,
            selectPage: 1,
        });

        jQuery('#tableOffice').bootstrapTable('selectPage', 1);


        $('body').on('click', '.openPopupIframe', function () {
            var dataURL = $(this).attr('data-href');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                cache: false,
                url: dataURL,
                success:function (res) {
                    if (res.status != false ) {
                        $('#branchesManagerModal .modal-title').html('Cập nhật Ban chấp hành VPĐD');
                        $('#branchesManagerModal').modal({show: true});

                        let IframeEmbed = '<iframe style="position:relative;min-height:400px" src="' + dataURL + '" width="100%" height="300px" frameBorder="0"></iframe>';

                        $('#branchesManagerModal .modal-body').html(IframeEmbed);
                    } else {
                        toastr.warning('Không thể cập nhật Ban chấp hành cho VPĐD ở trạng thái không hoạt động', {timeOut: 2000});
                    }
                }
            });
        });

    </script>
@endsection
