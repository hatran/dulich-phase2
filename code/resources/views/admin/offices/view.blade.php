<div class="modal-body">
    <div class="container-fluid">
        <div class="row" style="padding: 1rem;">
            <div class="container-fluid shadow">
                <div class="row">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-4"
                                               for="field1"><?= Lang::get('offices.code'); ?>
                                        </label>
                                        <div class="controls col-sm-8">
                                            <p><?=$model->code?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-4"
                                               for="field2"><?= Lang::get('offices.name'); ?>
                                        </label>
                                        <div class="controls col-sm-8">
                                            <p><?=$model->name?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-4"
                                               for="field3"><?= Lang::get('offices.phone'); ?>
                                        </label>
                                        <div class="controls col-sm-8">
                                            <p><?=$model->phone?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-4"
                                               for="field4"><?= Lang::get('offices.email'); ?>
                                        </label>
                                        <div class="controls col-sm-8">
                                            <p><?=$model->email?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-4"
                                               for="status">Trụ sở hội</label>
                                        <div class="controls col-sm-8">
                                            <p><?php echo ($model->option_code == 'HEAD') ? 'Có' : 'Không'; ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-4"
                                               for="status">Khu Vực</label>
                                        <div class="controls col-sm-8">
                                            <p><?php echo $area; ?></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-4"
                                               for="status">Trạng thái</label>
                                        <div class="controls col-sm-8">
                                            <p><?php echo ($model->status == 1) ? 'Hoạt động' : 'Không hoạt động'; ?></p>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label control-label-left col-sm-3"
                                                       for="field5">Hình ảnh</label>
                                                <div class="controls col-sm-9">
                                                    <a href="{{url('/files/offices/'.$model->images)}}">
                                                        <img src="{{url('/files/offices/'.$model->images)}}"
                                                             alt="no image" width="100%" style="width: 186px"/>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label control-label-left  col-sm-2"
                                               for="field5"><?= Lang::get('offices.addr'); ?></label>

                                        <div class="controls col-sm-10">
                                            <p><?=$model->address?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-2"
                                               for="field8">
                                            <?= Lang::get('offices.payment_info'); ?>
                                        </label>
                                        <div class="controls col-sm-10">
                                            <p><?=$model->payment_info?></p>
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label control-label-left col-sm-2"
                                               for="field9"><?= Lang::get('offices.note'); ?></label>
                                        <div class="controls col-sm-10">
                                            <p><?=$model->note?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer view">
    <button type="button" class="btn btn-warning"
            data-dismiss="modal"><?= Lang::get('offices.close') ?></button>
</div>
