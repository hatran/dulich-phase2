<form class="page-wrap" name="filter" method="get"  id="formSearch" action="" _lpchecked="1">
    {{ csrf_field() }}
    <div class="row" style="width:100%">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Mã VPĐD
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="code" name="code" placeholder="<?= Lang::get('offices.code') ?>" value="<?= (!empty($code)) ? $code : '' ?>" maxlength="10" tabindex="1" class="form-control" >
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tên VPĐD
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="name" name="name" placeholder="<?= Lang::get('offices.name') ?>" value="<?= (!empty($name)) ? $name : '' ?>" maxlength="100" tabindex="2" class="form-control">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số điện thoại
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="phone" name="phone" placeholder="<?= Lang::get('offices.phone') ?>" value="<?= (!empty($phone)) ? $phone : '' ?>" maxlength="15" tabindex="3" class="form-control">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Email
        </div>
        <div class="dropfield field-wrap col-md-4 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
            <input type="text" id="email" name="email" placeholder="<?= Lang::get('offices.email') ?>" value="<?= (!empty($email)) ? $email : '' ?>" maxlength="100" tabindex="4" class="form-control">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Trạng thái
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="5" id="search_status" name="search_status" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($statusOption))
                    @foreach($statusOption as $key => $val)
                        @php $selected = is_numeric(Input::get('search_status', '')) && Input::get('search_status', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Khu vực
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select tabindex="6" id="search_area" name="search_area" class="form-control">
                <option value="">Tất cả</option>
                @if (!empty($area))
                    @foreach($area as $key => $val)
                        @php $selected = Input::get('search_area', '') == $key ? 'selected="selected"' : ''; @endphp
                        <option {{$selected}} value="{{$key}}">{{$val}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        {{ csrf_field() }}
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="7" type="submit" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>
<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
</script>