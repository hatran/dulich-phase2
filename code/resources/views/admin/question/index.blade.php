
@extends('admin.layouts.app')
@section('title', 'Quản trị câu hỏi kiến thức')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .table-wrap table tr td {position: relative;word-wrap: break-word;white-space: normal !important;}
</style>
@section('content')
    <div class="page-wrap">
        <div class="main-content" style="padding-bottom: 75px;">
            @include('admin.layouts.message')
            @include('admin.question.search')
            <input id="exportHdd" type="hidden" value="{{ url('/officesys/question/exportexcel') }}{{$query_string != '' ? '?'.$query_string : ''}}"/>
            <div class="total" style="margin-bottom: 10px">Tổng số : {{$count_questions}}
                <a href="{{ route('question.create') }}" class="button btn-primary btn-function" style="float:  right;width: 120px; height: 37px; line-height: 24px; padding: 6px 12px !important; cursor: pointer" tabindex="8">Thêm mới</a>
                <a class="import_view-modal button btn-primary" style="float:  right;width: 130px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important; margin-left: 5px;" data-file-id="-1" href="javascript:void(0)" tabindex="7">Nhập câu hỏi</a>
                <a class="button btn-primary" style="float:  right;width: 130px; height: 37px; line-height: 24px; margin-bottom: 5px; padding-right: 5px !important; padding-left: 5px !important;" data-file-id="-1" href="javascript:void(0)" onclick="createExcel();" tabindex="6">Xuất câu hỏi</a>
            </div>
            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%"><div class="th-inner sortable both">STT</div></td>
                        <td width="10%"><div class="th-inner sortable both">Mã câu hỏi</div></td>
                        <td width="25%"><div class="th-inner sortable both">Nội dung câu hỏi</div></td>
                        <td width="15%"><div class="th-inner sortable both">Danh mục câu hỏi</div></td>
                        <td width="10%"><div class="th-inner sortable both">Loại câu hỏi</div></td>
                        <td width="10%"><div class="th-inner sortable both">Ngày tạo</div></td>
                        <td width="10%"><div class="th-inner sortable both">Nguời tạo</div></td>
                        <td width="7%"><div class="th-inner sortable both">Trạng thái</div></td>
                        <td width="8%"><div class="th-inner sortable both">Chức năng</div></td>
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objQuestions) == 0)
                        <tr>
                            <td colspan="10" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objQuestions as $key => $value)
                            <tr>
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">
                                    {{ $value->code }}
                                </td>
                                <td class="text-left">
                                    <p title="{{$value->content}}">
                                        {{ mb_strlen($value->content, 'UTF-8') > 50 ? mb_substr($value->content, 0, 50, 'utf-8') .'...' :  $value->content }}
                                    </p>
                                </td>

                                <td class="text-left">
                                    {{ isset($value->subject_knowledge_id) ? $subjectKnowledge[$value->subject_knowledge_id] : '' }}
                                </td>
                                <td class="text-left">
                                    {{ $rank_type[$value->question_rank] }}
                                </td>
                                <td class="text-center">{{ !empty($value->created_at) ? date('d.m.Y', strtotime($value->created_at)) : '' }}</td>
                                <td>{{ !empty($value->created_author) ? $value->created_author : '' }}</td>
                                <td class="text-left"> {{ $value->status == 0 ? 'Không hoạt động' : 'Hoạt động' }} </td>
                                <td class="text-center">
                                    <a class="btn-function" href="{{ route('question.show', ['id' => $value->id]) }}">
                                        <span class="glyphicon glyphicon-eye-open" style="font-size: 16px; color: #009933;"></span>
                                    </a>
                                    <a class="btn-function" href="{{ route('question.edit', ['id' => $value->id]) }}">
                                        <span class="glyphicon glyphicon-edit" style="font-size: 16px; color: #ff6600;"></span>
                                    </a>
                                    <a class="btn-function" href="javascript:void(0);" data-toggle="tooltip" data-placement="left" title="Xoá danh mục" onclick="delete_question({{ $value->id }}, '{{strip_tags($value->code)}}')" data-id="{{ $value->id }}" data-status="{{ $value->status }}">
                                        <span class="glyphicon glyphicon-trash" style="font-size: 16px;"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>
    @include('admin.question.delete')
    @include('admin.question.import_excel')

@endsection
@section('footer_embed')
    <script src="{{ asset('admin/js/jquery.loader.min.js') }}"></script>
    <style>
        .button {
            width: 130px;
            height: 30px;
            line-height: 30px;
            padding: 6px 32px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            border: 0;
        }
        .create_card {
            background-color: #2481bf;
            padding: 6px 18px;
        }

        .sortable{
            color: #144a8b;
            font-weight: bold;
        }

        .checkbox {
            align: center;
        }

        .sent_notify {
            margin-top: 20px;

        }
        #closeButton ul {
            padding-left: 0px !important;
        }
        #id{
            -moz-appearance: textfield;     height: 35px;     line-height: 35px;     padding-right: 30px;     position: relative;     width: 100%;border: 1px solid #bbbaba;border-radius: 2px;display: block;max-width: 100%; margin-bottom: 10px;padding-left: 10px;
        }
        .rq-star{
            color: red
        }
        .error_border {
            box-shadow: rgb(206, 63, 56) 0px 0px 10px;
        }

        .color-required {
            color: red
        }
        span.error-msg {
            color: #a94442;
        }
        span {
            font-family: "Times New Roman", Times, serif;
            word-break: break-word;
        }
        .total {
            border: 0 none;
            border-radius: 3px;
            color: #0000f0 !important;
            font-size: 16px;
            line-height: 30px;
            padding: 6px 10px;
        }


        .modal-title {
            margin: 0;
            line-height: 1.42857143;
            font-weight: bold;
            width: 50%;
            float: left;
            font-size: 20px;
        }
    </style>
    <script>

        function createExcel(){
            window.location = $("#exportHdd").val();
        }

        function delete_question(id, txt) {
            jQuery("#deleteModal").modal();
            jQuery('#id-em').text(txt);
            jQuery('#id_delete').val(id);
            jQuery('#option_delete').val(option);
        }
        function deleteCat() {
            window.location = '/officesys/question/deleteall/'+jQuery('#id_delete').val();
        }
       
        var url = "{{ url('/officesys/question') }}"
        function htmlEntities(str) {
            return str;
        }

        function doAction(dmId, postfix) {
            $("#questionForm"+postfix).submit();
            var modalId = '#' + postfix + 'Modal';
            $(modalId).modal('hide');
        }

        $('.modal-footer').on('click', '.delete', function () {
            var dmId = $('input[type=hidden]#dm-id').val();
            doAction(dmId, '_delete');
        });

        $(document).on('click', '.import_view-modal', function () {
            $('#importViewModal').modal('show');
        });

        var error_message = $("#error_message").text();
        if (error_message) {
            toastr.clear()
            $("#send-notification").css('pointer-events', 'none');
            toastr.options.onHidden = function () {
                $("#send-notification").css('pointer-events', 'auto');
            }
            toastr.error("File import Lệ phí, hội phí không hợp lệ", 'Lỗi', {timeOut: 2000});
        }

        function toTimestamp(strDate){
            var datum = Date.parse(strDate);
            return datum/1000;
        }

        $("#import_excel").on('click', '.import_excel_submit', function () {
            validateFileUpload();
        });

        function validateFileUpload() {
            var file;
            var FileSize;
            var extension;

            if ($('#fileToUpload').val() != '') {
                file      = $('#fileToUpload')[0].files[0];
                extension = file.name.substr((file.name.lastIndexOf('.') +1));
                FileSize  = file.size / 1024 / 1024; // in MB
            } else {
                FileSize = 0;
                extension = '';
            }

            if (FileSize == 0) {
                alert('Không có file!');
            } else if (FileSize > 10){
                alert('File quá lớn, dung lượng file cho phép tối đa là 10Mb!');
            } else if (extension != 'xlsx' && extension != '') {
                alert('Bạn chỉ được phép upload file excel xlsx, vui lòng thực hiện lại');
            } else {
                $('#import_excel').get(0).submit();
            }
        }
        $(document).ready(function () {
            var nameall = $("#contentall").val();
            $("#contentall").val('');
            setTimeout(function() {
                $('#contentall').focus() ;
                $("#contentall").val(nameall);
            }, 100);
        });
    </script>
@endsection