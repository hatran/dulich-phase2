<form class="page-wrap row" name="filter" method="get" id="formSearch" action="{{ url ('/officesys/question') }}">
    {{ csrf_field() }}
    <div class="row" style="width: 100%">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
           Từ khóa câu hỏi
        </div>

        <div style="margin-bottom: 10px;" class="field-wrap col-md-4 col-sm-6 col-xs-12">
            <input type="text" max="255" id="contentall" name="contentall" value="{{ array_get($response_view, 'contentall', '') }}" placeholder="Nhập từ khóa muốn tìm kiếm" tabindex="1">
        </div>

        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px;">
            Loại kiến thức
        </div>
        <div style="margin-bottom: 10px;" class="field-wrap dropfield col-md-4 col-sm-6 col-xs-12" >
            <select id="knowledge_id" tabindex="2" name="knowledge_id">
                <option value="">Tất cả</option>
                @foreach($knowledge as $key => $value)
                <option value="{{ $key }}"
                        @if (array_get($response_view, 'knowledge_id', '') == $key) selected="selected" @endif>{{ $value }}</option>
                @endforeach
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
           Loại câu hỏi
        </div>

        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select name="question_rank" id="question_rank" tabindex="3">
                <option value="">Tất cả</option>
                @foreach($rank_type as $key => $value)
                <option value="{{ $key }}"
                        @if (array_get($response_view, 'question_rank', '') == $key) selected="selected" @endif>{{ $value }}</option>
                @endforeach
            </select>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
           Danh mục câu hỏi
        </div>

        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <select name="subject_knowledge_id" id="subject_knowledge_id" tabindex="4">
                <option value="">Tất cả</option>
            </select>
        </div>
        <div style="margin-left: 45%;" class="field-wrap col-md-2 col-sm-3 col-xs-2">
            <input type="submit" class="button btn-primary" name="search" value="Tìm kiếm" tabindex="5" style="width: 140px">
        </div>
    </div>
</form>
<input type="hidden" value="" id="dm-id" name="dm-id"/>
<script type="text/javascript" src="{{ asset("js/ajaxloadoptioncode.js") }}"></script>
<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });

    var inputGetOptionCode = "<?php echo Input::get('subject_knowledge_id'); ?>";
    var urlRoute = "<?php echo URL::route('ajax_question_add_subject_knowledge'); ?>";
    var selector = $("#subject_knowledge_id");
    var selectorParent = $("#knowledge_id");

    $(document).ready(function () {
        console.log($('meta[name="csrf-token"]'));
        var itemVal = selectorParent.children("option:selected").val();
        selector.children('option:not(:first)').remove();
        if (itemVal != '') {
            ajaxSearchOptionCode(urlRoute, itemVal, selector, inputGetOptionCode);
        }
    });

    selectorParent.change(function () {
        var __this = $(this);
        var itemVal = $(this).val();
        selector.children('option:not(:first)').remove();

        if (itemVal != '') {
            ajaxSearchOptionCode(urlRoute, itemVal, selector)
        }
    });

</script>