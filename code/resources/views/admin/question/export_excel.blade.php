
<table>
    <tbody>
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">
                DANH SÁCH CÂU HỎI KIẾN THỨC
            </td>
        </tr>
        <tr>
            <td colspan="9" style="text-align: center; font-weight: 900">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr class="header">
            <td>TT</td>
            <td>Mã câu hỏi</td>
            <td>Nội dung câu hỏi</td>
            <td>Danh mục câu hỏi</td>
            <td>Loại kiến thức</td>
            <td>Loại câu hỏi</td>
            <td>Đáp án 1</td>
            <td>Đáp án 2</td>
            <td>Đáp án 3</td>
            <td>Đáp án 4</td>
            <td>Đáp án đúng</td>
            <td>Ngày tạo</td>
            <td>Nguời tạo</td>
            <td>Trạng thái</td>
        </tr>
        @if (!empty($objQuestions))
            @foreach ($objQuestions as $key => $item)
            <tr class="main-content">
                <td>{{ $key + 1 }}</td>
                <td>{{ htmlentities($item->code) }}</td>
                <td>{{ htmlentities($item->content) }}</td>
                <td>{{ htmlentities($item->skname) }}</td>
                <td>{{ $item->kname }}</td>
                <td>{{ $rank_type[$item->question_rank] }}</td>
                <td>{{ htmlentities($item->answer1) }}</td>
                <td>{{ htmlentities($item->answer2) }}</td>
                <td>{{ htmlentities($item->answer3) }}</td>
                <td>{{ htmlentities($item->answer4) }}</td>
                <td>{{ htmlentities($item->answer_correct) }}</td>
                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                <td>{{ htmlentities($item->created_author) }}</td>
                <td>{{ $item->status == 1 ? 'Sử dụng' : 'Không sử dụng' }}</td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>


<style>
    .header td {
        font-weight: bold;
        text-align: center;
        border: 1px solid #000;
    }
    .main-content td {
        border: 1px solid #000;
    }
</style>
