@extends('admin.layouts.app')
@section('title', 'Sửa câu hỏi kiến thức - ' . $question->code)
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .expire_date{
        margin-bottom: 10px;
        padding-top: 7px;
        font-weight: bold;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }
    .col-md-1{
        width: 10%;
    }
    .has-error .control-label {
        color: #3D3A3A !important
    }
</style>
    <div class="page-wrap">
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Chi tiết câu hỏi kiến thức - {{ $question->code }}</div>

            <div class="panel-body">
                <form id="infomation-form" class="form-horizontal">
                    <div class="form-group{{ $errors->has('knowledge_id') ? ' has-error' : '' }}">
                        <label for="knowledge_id" class="col-md-2 text-right">Loại kiến thức</label>
                        <div class="col-md-4">
                            {{ $knowledge[$subjectKnowledge->knowledge_id] }}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('subject_knowledge_id') ? ' has-error' : '' }}">
                        <label for="subject_knowledge_id" class="col-md-2 text-right">Danh mục câu hỏi</label>
                        <div class="col-md-4">
                            {{ $subjectKnowledgeName[$question->subject_knowledge_id] }}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('question_rank') ? ' has-error' : '' }}">
                        <label for="" class="col-md-2 control-label">Loại câu hỏi</label>
                        <div class="col-md-6" style="padding-top: 5px">
                            {{ $rank_type[$question->question_rank] }}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                        <label for="code" class="col-md-2 text-right">Mã câu hỏi</label>

                        <div class="col-md-6">
                            {{ $question->code }}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-2 text-right">Nội dung câu hỏi</label>
                        <div class="col-md-10" style="padding-top: 10px">
                           {{ $question->content }}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('answer1') ? ' has-error' : '' }}">
                        <label for="answer1" class="col-md-2 text-right">Đáp án 1 </label>

                        <div class="col-md-6">
                            {{ $question->answer1 }}&nbsp
                            <?php echo $question->answer_correct == 1 ? 'Đáp án đúng' : ''; ?> 
                        </div>

                
                            
              
                    </div>
                    <div class="form-group{{ $errors->has('answer2') ? ' has-error' : '' }}">
                        <label for="answer2" class="col-md-2 text-right">Đáp án 2 </label>

                        <div class="col-md-6">
                           {{ $question->answer2 }}&nbsp
                           <?php echo $question->answer_correct == 2 ? 'Đáp án đúng' : ''; ?>

                        </div>

                     
                            
                        
                    
                    </div>
                    <div class="form-group{{ $errors->has('answer3') ? ' has-error' : '' }}">
                        <label for="answer3" class="col-md-2 text-right">Đáp án 3</label>

                        <div class="col-md-6">
                            {{ $question->answer3 }}&nbsp
                            <?php echo $question->answer_correct == 3 ? 'Đáp án đúng' : ''; ?>
                           
                        </div>

                
                            
                           
                 
                    </div>
                    <div class="form-group{{ $errors->has('answer4') ? ' has-error' : '' }}">
                        <label for="answer4" class="col-md-2 text-right">Đáp án 4</label>

                        <div class="col-md-6">
                            {{ $question->answer4 }}&nbsp
                            <?php echo $question->answer_correct == 4 ? 'Đáp án đúng' : ''; ?>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="" class="col-md-2 control-label">Trạng thái <span class="color-required">*</span></label>
                        <div class="col-md-6" style="padding-top: 5px">
                          
                                <?php echo $question->status == 1 ? '  Hoạt động' : ''; ?>
                         
                      
                                <?php echo $question->status == 0 ? 'Lưu nháp' : ''; ?>
                       
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <a href="{{ URL::route('question.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="10">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
