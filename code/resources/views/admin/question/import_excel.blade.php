<div id="importViewModal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="id_show">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Nhập danh sách câu hỏi</h4>
          </div>

          <div class="modal-body">
              {{csrf_field() }}
              <div class="page-wrap">
                  <div class="table-wrap" style="padding: 10px 20px; margin-bottom: 25px;">
                      <p>Import thông tin danh sách câu hỏi (.xlsx)</p>
                      <div>
                          <form id="import_excel" name="import_excel" action="{{ route('admin.question_importexcel') }}" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                              <input type="file" name="fileToUpload" id="fileToUpload">
                              <div class="modal-footer">
                                  <div style="margin: -10px -12px; float: left; text-decoration: underline;">
                                      <a href="{{ asset('docs/bieu_mau_excel_import_danh_sach_cau_hoi.xlsx') }}" target="_blank">Tải file biểu mẫu</a>
                                  </div>
                                  <input type="button" value="Cập nhật" class="btn btn-success import_excel_submit">
                                  <button type="button" class="btn btn-warning" data-dismiss="modal" style="margin-left: 2px">Không</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
