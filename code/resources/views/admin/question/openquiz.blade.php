@extends('admin.layouts.app')
@section('title', 'Đóng/mở thi xếp hạng HDV')
@section('content')
    <div class="page-wrap">
        <meta name="_token" content="{{ csrf_token() }}"/>

        <div class="main-content" style="padding-bottom: 75px;">
            <form class="page-wrap" name="filter" method="get" id="formSearch">
                {{ csrf_field() }}
                <div class="row">
                        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                            Mã số hồ sơ
                        </div>
                        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                            <input id="file_code" type="text" name="file_code" value="{{ htmlentities(array_get($response_view, 'file_code', '')) }}" maxlength="6">
                        </div>
                        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                            Số thẻ hội viên
                        </div>
                        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                            <input id="idMemberCode" type="text" name="member_code"
                                   value="{{ htmlentities(array_get($response_view, 'member_code', '')) }}" maxlength="6">
                        </div>
                    <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                        Số thẻ HDV
                    </div>
                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                        <input id="idNumber" type="text" name="touristGuideCode"
                               value="{{ htmlentities(array_get($response_view, 'touristGuideCode', '')) }}" maxlength="9">
                    </div>
                    <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                        Họ tên
                    </div>
                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                        <input id="name" type="text" name="fullName" value="{{ htmlentities(array_get($response_view, 'fullName', '')) }}" maxlength="50">
                    </div>
                    <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                        Trạng thái
                    </div>
                    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                        <select id="status" name="status">
                            <option></option>
                            @foreach ($status as $key => $value)
                                <option value="{{ $key }}" {{ array_get($response_view, 'status', '') == $key ? 'selected="selected"' : '' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                   
                    <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;">
                        <input type="button" class="button btn-primary" style="width: 100px" name="search" id="search" onclick="submitForm();" value="Tìm kiếm">
                    </div>
                </div>
            </form>
            @include('admin.layouts.message')
            <a class="checked-button button btn-primary" style="float:  right;width: 120px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-status="1" href="javascript:void(0)">Mở thi</a>
            <a class="checked-button button btn-danger" style="float:  right;width: 120px; height: 37px; line-height: 24px; margin-bottom: 5px;  padding-right: 5px !important; padding-left: 5px !important;" data-status="2" href="javascript:void(0)">Đóng thi</a>
            
            <div class="total" style="color: #3d3a3a !important;">Tổng số : {{ $count_members }}</div>
            <div class="table-wrap">
                <table>
                    <thead>
                    <tr>
                        <td width="5%">STT</td>
                        <td width="15%">Mã hồ sơ</td>
                        <td width="10%">Số thẻ hội viên</td>
                        <td width="15%">Số thẻ HDV</td>
                        <td width="20%">Họ và tên</td>
                        <td width="10%">Ngày sinh</td>
                        <td width="15%">Trạng thái</td>
                        <td width="10%" style="position: relative; top:0px; color: #144a8b;">Chọn tất cả<br>
                            <input type="checkbox" class="checkAll">
                            <label>&nbsp;</label>
                        </td>
                        
                    </tr>
                    </thead>
                    <tbody>
                    @if (count($objMembers) == 0)
                        <tr>
                            <td colspan="7" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                        </tr>
                    @else
                        @foreach($objMembers as $key => $objMember)
                            <tr class="item{{$objMember->id}}" data-datePayment="{{strtotime(str_replace('/', '-',$objMember->date_payment))}}">
                                <td class="text-center"> {{ $current_paginator + $key + 1 }} </td>
                                <td class="text-center">
                                    {{ empty($objMember->file_code) ? '' : $objMember->file_code }}
                                </td>
                                <td class="text-center">
                                    {{ empty($objMember->member_code) ? '' : $objMember->member_code }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('admin_list_member_detail_view', $objMember->id) }}">{{ $objMember->touristGuideCode }} </a>
                                </td>
                                <td class="text-left"> {{ htmlentities($objMember->fullName) }} </td>
                                <td class="text-center"> {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }} </td>
                                <td class="text-left wrap-text">
                                    {{  $status[$objMember->open] }}
                                </td>
                                <td class="text-center">
                                    <span class="checkitem">
                                        <input id="member{{ $key + 1 }}" type="checkbox" name="member" value="{{ $objMember->id }}" class="checkbox-pr">
                                        <label for="member {{ $key + 1 }}">&nbsp;</label>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            @include('admin.layouts.pagination')
        </div>
    </div>
@endsection
@section('footer_embed')
    <style>
        body .help-block {
            color: #a94442;
        }

        .button {
            width: 30px;
            height: 30px;
            line-height: 30px;
            padding: 6px 15px;
            text-align: center;
            background-color: #f35d23;
            color: #fff !important;
            border-radius: 3px;
            margin: 0 0 10px 10px;
            border: 0;
        }

        .btn btn-delete {
            line-height: 30px;
            padding: 6px 6px;
            text-align: center;
            background-color: #fff;
            color: #000 !important;
            border-radius: 3px;
            margin: 0 0 10px 10px;
            border: 0;
        }

        .form-horizontal .control-label {
            padding-top: 7px;
            margin-bottom: 0;
            text-align: left;
        }

        .total {
            line-height: 30px;
            padding: 6px 10px;
            color: #0000F0 !important;
            border-radius: 3px;
            font-size: 16px;
            border: 0;
        }
        .box-shado-error{
            box-shadow: 0px 0px 10px rgb(206, 63, 56);
        }
    </style>

    <script type="text/javascript" src="{{ asset('admin/js/jquery.min.js') }}"></script>
    <!-- Bootstrap JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <!-- toastr notifications -->
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            document.getElementById("file_code").focus();
        });
        
        $('body').on('change', '.checkAll', function() {
            $(".checkbox-pr").prop('checked',$(this).is(":checked"));
            if ($(this).is(":checked") == true) {
                $(".checkbox-pr").attr('data-check', 'yes');
            }
            else {
                $(".checkbox-pr").attr('data-check', 'no');
            }
        }).on('change', '.checkbox-pr', function () {
            var __this = $(this);
            var anyChecked = false;
            var allDataCheckLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').find('input[data-check="yes"]').length;
            var allRowLength = __this.parent().parent().parent().parent().children('tr').children('.text-center').children('.checkitem').length;

            if (__this.is(':checked')) {
                __this.parent().parent().find('input[type="checkbox"]').each(function (index, value) {
                    if ($(this).is(':checked')){
                        anyChecked = true;
                    } else {
                        anyChecked = false;
                    }
                });
                if (anyChecked == true) {
                    __this.attr("data-check", "yes");

                    if (allRowLength == allDataCheckLength + 1) {
                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', true);

                        __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "yes");
                    }
                }
            } else {
                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').prop('checked', false);

                __this.parent().parent().parent().parent().parent().children('thead').children('tr').children('td').children('.checkAll').attr('data-check', "no");

                __this.attr('data-check', 'no');
            }
        });

        function checkedNames() {
            check = 0;
            $("input[name=member]:checked").each( function () {
                check = 1;
            });
            return check;
        }

        $(".checked-button").click(function() {
            var status = $(this).attr('data-status');
            if (checkedNames() == 1) {
                var printData = [];
                for (var i = 0; i < $("input[name='member']:checked").length; i++) {
                    printData.push($("input[name='member']:checked")[i].value);
                }
                window.location.href = '{{ route('admin.updatequiz') }}' + '?list_id=' + printData + '&status=' + status; 
            }else{
                toastr.clear();
                $(".resend-notification").css('pointer-events', 'none');
                toastr.options.onHidden = function() { $(".resend-notification").css('pointer-events', 'auto'); }
                toastr.warning('Vui lòng chọn ít nhất 1 thành viên.', 'Cảnh báo', {timeOut: 2000});
            }
        });

        function submitForm() { 
            $('#formSearch').submit();
        };
    </script>
@endsection