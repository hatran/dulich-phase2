@extends('admin.layouts.app')
@section('title', 'Sửa câu hỏi kiến thức - ' . $question->code)
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .expire_date{
        margin-bottom: 10px;
        padding-top: 7px;
        font-weight: bold;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }
    .col-md-1{
        width: 10%;
    }
    .has-error .control-label {
        color: #3D3A3A !important
    }
</style>
    <div class="page-wrap">
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel-heading">Sửa câu hỏi kiến thức - {{ $question->code }}</div>

            <div class="panel-body">
                @if(session('successes'))
                    <div id="closeButton" class="alert alert-success">
                        <button type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach (session('successes') as $success)
                                <li>{{ $success }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($errors->any())
                    <div id="closeButton" class="alert alert-danger">
                        <button type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form id="infomation-form" enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{ URL::route('admin_question_savedeletdit_detail') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_edit" value="{{ $question->id }}">
                    <div class="form-group{{ $errors->has('knowledge_id') ? ' has-error' : '' }}">
                        <label for="knowledge_id" class="col-md-2 text-right">Loại kiến thức<span class="color-required">*</span></label>
                        <div class="col-md-4">
                            <select id="knowledge_id" class="form-control" tabindex="1" name="knowledge_id">
                                @foreach($knowledge as $key => $val)
                                    <option value="{{ $key }}" {{ ($subjectKnowledge->knowledge_id == $key) ? 'selected' : '' }}> {{ $val }} </option>
                                @endforeach
                            </select>
                            @if ($errors->has('knowledge_id'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('knowledge_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('subject_knowledge_id') ? ' has-error' : '' }}">
                        <label for="subject_knowledge_id" class="col-md-2 text-right">Danh mục câu hỏi<span class="color-required">*</span></label>
                        <div class="col-md-4">
                            <select id="subject_knowledge_id" class="form-control" name="subjectKnowledgeIDAll_edit" tabindex="2">
                            </select>
                            @if ($errors->has('subject_knowledge_id'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('subject_knowledge_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('question_rank') ? ' has-error' : '' }}">
                        <label for="" class="col-md-2 control-label">Loại câu hỏi <span class="color-required">*</span></label>
                        <div class="col-md-6" style="padding-top: 5px">
                            <?php $n = 3; ?>
                            @foreach ($rank_type as $key => $value)
                            <label class="radio-inline" for="question_rank" style="margin-right: 20px;">
                                <input type="radio" name="questionRankAll_edit" value="{{ $key }}" tabindex="{{ $n }}" {{ $key == $question->question_rank ? 'checked' : '' }} >
                                    {{ $value }}
                            </label>
                            <?php $n++; ?>
                            @endforeach
                            @if ($errors->has('question_rank'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('question_rank') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                        <label for="code" class="col-md-2 text-right">Mã câu hỏi <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            {{ $question->code }}
                            <input type="hidden" name="codeAll_edit" value="{{ $question->code }}" tabindex="6">
                            @if ($errors->has('code'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-2 text-right">Nội dung câu hỏi <span class="color-required">*</span></label>
                        <div class="col-md-10" style="padding-top: 10px">
                            <textarea tabindex="5" rows="15" id="content_rg" name="contentAll_edit" class="form-control position-content tinymce">{{ $question->content }}</textarea>
                            @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('answer1') ? ' has-error' : '' }}">
                        <label for="answer1" class="col-md-2 text-right">Đáp án 1 <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="answer1" type="text" class="form-control" name="answer1All_edit" value="{{ $question->answer1 }}"  tabindex="6" maxlength="500" >

                            @if ($errors->has('answer1'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('answer1') }}</strong>
                                </span>
                            @endif
                        </div>

                        <label class="radio-inline" for="correct1">
                            <input id="correct1" type="radio" name="answerCorrectAll_edit" value="1" tabindex="7" <?php echo $question->answer_correct == 1 ? 'checked' : ''; ?> >
                            Đáp án đúng
                        </label>
                    </div>
                    <div class="form-group{{ $errors->has('answer2') ? ' has-error' : '' }}">
                        <label for="answer2" class="col-md-2 text-right">Đáp án 2 <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="answer2" type="text" class="form-control" name="answer2All_edit" value="{{ $question->answer2 }}"  tabindex="8" maxlength="500">

                            @if ($errors->has('answer2'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('answer2') }}</strong>
                                </span>
                            @endif
                        </div>

                        <label class="radio-inline" for="correct2">
                            <input id="correct2" type="radio" name="answerCorrectAll_edit" value="2" tabindex="9" <?php echo $question->answer_correct == 2 ? 'checked' : ''; ?>>
                            Đáp án đúng
                        </label>
                    </div>
                    <div class="form-group{{ $errors->has('answer3') ? ' has-error' : '' }}">
                        <label for="answer3" class="col-md-2 text-right">Đáp án 3 <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="answer3" type="text" class="form-control" name="answer3All_edit" value="{{ $question->answer3 }}"  tabindex="10" maxlength="500">

                            @if ($errors->has('answer3'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('answer3') }}</strong>
                                </span>
                            @endif
                        </div>

                         <label class="radio-inline" for="correct3">
                            <input id="correct3" type="radio" name="answerCorrectAll_edit" value="3" tabindex="11" <?php echo $question->answer_correct == 3 ? 'checked' : ''; ?>>
                            Đáp án đúng
                        </label>
                    </div>
                    <div class="form-group{{ $errors->has('answer4') ? ' has-error' : '' }}">
                        <label for="answer4" class="col-md-2 text-right">Đáp án 4 <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="answer4" type="text" class="form-control" name="answer4All_edit" value="{{ $question->answer4 }}"  tabindex="12" maxlength="500" >

                            @if ($errors->has('answer4'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('answer4') }}</strong>
                                </span>
                            @endif
                        </div>

                         <label class="radio-inline" for="correct4">
                            <input id="correct4" type="radio" name="answerCorrectAll_edit" value="4" tabindex="13" <?php echo $question->answer_correct == 4 ? 'checked' : ''; ?>>
                            Đáp án đúng
                        </label>
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="" class="col-md-2 control-label">Trạng thái <span class="color-required">*</span></label>
                        <div class="col-md-6" style="padding-top: 5px">
                            <label class="radio-inline" for="statushd">
                                <input id="statushd" type="radio" name="statusAll_edit" value="1" tabindex="14" <?php echo $question->status == 1 ? 'checked' : ''; ?> >
                                Hoạt động
                            </label>
                            <label class="radio-inline" for="statusln" >
                                <input id="statusln" type="radio" name="statusAll_edit" value="0" tabindex="15" <?php echo $question->status == 0 ? 'checked' : ''; ?> >
                                Lưu nháp
                            </label>
                            @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <button type="button" class="btn btn-primary" id="submitBtn" onclick="return submitForm();" tabindex="16">Lưu</button>
                            <a href="{{ URL::route('question.index') }}" class="btn btn-warning" style="color: #FFFFFF;" tabindex="17">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript" src="{{ asset("js/ajaxloadoptioncode.js") }}"></script>
<script>
    var inputGetOptionCode = "<?php echo $question->subject_knowledge_id; ?>";
    var urlRoute = "<?php echo URL::route('ajax_question_add_subject_knowledge'); ?>";
    var selector = $("#subject_knowledge_id");
    var selectorParent = $("#knowledge_id");
    // Enter to submit form registerForm
    document.body.addEventListener('keydown', function(e) {
        var key = e.which;
        if (key == 13) {
            $("#registerInfo").click();
        }
    });
    var flagValidate = true;

    function submitForm() {
        $('#submitBtn').prop('disabled', true);
        $('#infomation-form').submit();
        return false;
    }

    $(document).ready(function () {
        var itemVal = selectorParent.children("option:selected").val();
        selector.children().remove();
        if (itemVal != '') {
            ajaxSearchOptionCode(urlRoute, itemVal, selector, inputGetOptionCode);
        }

        $("#knowledge_id").focus();
    });

    selectorParent.change(function () {
        var __this = $(this);
        var itemVal = $(this).val();
        selector.children().remove();

        if (itemVal != '') {
            ajaxSearchOptionCode(urlRoute, itemVal, selector)
        }
    });
</script>
@endsection
