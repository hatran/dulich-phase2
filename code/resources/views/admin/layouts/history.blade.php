<?php
    use App\Providers\UserServiceProvider;
    use App\Constants\MemberConstants;
?>
<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
	<div class="block-heading">Lịch sử thẩm định:</div>
	<div class="block-content" style="margin-top:  10px;">
		<div class="table-line"> 
			@if (count($notes) > 0)
			<div class="table-wrap">
				<table>
				<thead>
				<tr>
					<td width="50">Số TT</td>
					<td width="250">Người thẩm định</td>
					<td width="150">Ngày thẩm định</td>
					<td width="150">Kết quả thẩm định</td>
					<td width="800">Ghi chú</td>
				</tr>
				</thead>
				<tbody>
					@foreach ($notes as $key => $note)
						<tr>
						@if (!empty($note) && $note->function_type == MemberConstants::IS_VERIFIED_ACTION)
							<td class="text-right">{{ ++$key }}</td>
							<td class="text-left">{{ $note->fullname }}</td>
							<td class="text-center">{{ !empty($note->created_at) ? $note->created_at->format('d/m/Y') : '' }}</td>
							<td class="text-left">{{ array_get(MemberConstants::$file_const_history, $note->action_type, '') }}</td>
							<td class="text-left">
								<div style="width: 500px; height: auto; white-space: pre-wrap;">{{  htmlentities($note->note) }}</div></td>
						@endif
						</tr>
					@endforeach
				</tbody>
				</table>
			</div>
			@endif
		</div>
	</div>
</div>

<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
	<div class="block-heading">Lịch sử phê duyệt:</div>
	<div class="block-content" style="margin-top:  10px;">
		<div class="table-line"> 
			@if (count($notes) > 0)
			<div class="table-wrap">
				<table>
				<thead>
				<tr>
					<td width="50">Số TT</td>
					<td width="250">Người phê duyệt</td>
					<td width="150">Ngày phê duyệt</td>
					<td width="150">Kết quả phê duyệt</td>
					<td width="800">Ghi chú</td>
				</tr>
				</thead>
				<tbody>
					@foreach ($notes as $pkey => $note)
					   <tr>
						@if (!empty($note) && $note->function_type == MemberConstants::IS_APPROVED_ACTION)

							<td class="text-right">{{ ++$pkey }}</td>
							<td class="text-left">{{ $note->fullname }}</td>
							<td class="text-center">{{ !empty($note->created_at) ? $note->created_at->format('d/m/Y') : '' }}</td>
							<td class="text-left">{{ array_get(MemberConstants::$file_const_history, $note->action_type, '') }}</td>
							<td class="text-left">
								<div style="width: 500px; height: auto; white-space: pre-wrap;">{{  htmlentities($note->note) }}</div></td>
						@endif
						</tr>
					@endforeach
				</tbody>
				</table>
			</div>
			@endif
		</div>
	</div>
</div> 

<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
	<div class="block-heading">Lịch sử tác động:</div>
	<div class="block-content" style="margin-top:  10px;">
		<div class="table-line"> 			
			<div class="table-wrap">
				<table>
				<thead>
				<tr>
					<td width="50">Số TT</td>
					<td width="250">Người tác động</td>
					<td width="150">Ngày tác động</td>
					<td width="150">Nội dung tác động</td>
					<td width="800">Ghi chú</td>
				</tr>
				</thead>
				<tbody>
					@foreach ($notes as $key => $note)
						<tr>		
							@if (!empty($note) && $note->function_type == MemberConstants::IS_NUMBER_TOUR_GUIDE_CHANGE )
							<td class="text-right">{{ ++$key }}</td>
							<td class="text-left">{{ $note->fullname }}</td>
							<td class="text-center">{{ !empty($note->created_at) ? $note->created_at->format('d/m/Y') : '' }}</td>
							<td class="text-left">{{  htmlentities($note->note) }}</td>							
							<td class="text-left">
								<div style="width: 500px; height: auto; white-space: pre-wrap;">Số thẻ HDV cũ: <b> {{  htmlentities($note->value_old) }}</b><br/>Số thẻ HDV mới: <b> {{  htmlentities($note->value_new) }} </b></div></td>							
							@endif
							@if (!empty($note) && $note->function_type == MemberConstants::IS_PAID_ACTION )
							<td class="text-right">{{ ++$key }}</td>
							<td class="text-left">{{ $note->fullname }}</td>
							<td class="text-center">{{ !empty($note->created_at) ? $note->created_at->format('d/m/Y') : '' }}</td>
							<td class="text-left">{{  htmlentities($note->note) }}</td>							
							<td class="text-left"></td>
							@endif
						</tr>
					@endforeach
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
