
<?php
    use App\Providers\UserServiceProvider;
    use App\Constants\MemberConstants;	
?>
<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
    <div class="block-heading">Thông tin nộp Lệ phí và Hội phí:</div>
    <div class="block-content">
        <div class="table-line"> 
            <div class="table-wrap">
                <table style="width:100%">
                    <thead>                    
                    <tr>
                        <td width="10%">Số chứng từ</td>
                        <td width="10%">Ngày chứng từ</td>
                        <td width="10%">Số tiền</td>
                        <td width="50%">Nội dung nộp tiền</td>
                        <td>Ghi chú</td>
                    </tr></thead>                    
                    <?php
                        if (!empty($paymentYearly)) { 
                    ?>
                    @foreach ($paymentYearly as $item) 
                    <tr>
                        <td class="text-center">{{ empty($item->number_payment) ? '' : $item->number_payment}}</td>
                        <td class="text-center">{{ empty($item->date_payment) ? '' : $item->date_payment }}</td>
                        <td class="text-center">{{ empty($item->currency) ? '' : $item->currency }}</td>
                        <td style="width: 300px; height: auto; white-space: pre-wrap;word-break: break-word;">{{ empty($item->payment_content) ? '' : htmlentities($item->payment_content) }}</td>
                        <td style="white-space: pre-wrap;word-break: break-word;"><?php if ($item->flag == 0) { echo "Đã đóng hội phí năm đầu";} else if ($item->flag == 1) { echo "Đã đóng hội phí gia hạn";} ?></td>
                    </tr>
                    @endforeach 
                    <?php
                    }  else { 
                    ?>
                        <tr>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    <?php }?>
                </table>
            </div>            
        </div>               
    </div>    
</div>