<div id="_editModal" class="modal fade" role="dialog">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="modal-dialog" id="fix-modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="table-line modal-body">
                <div class="table-line">
                    <div class="line row">
                        <div class="col-md-12 col-sm-12 col-xs-12 no-break">
                            <span style="font-size: 18px;">Thông tin hội viên</span>
                        </div>
                    </div>
                    <div class="clearfix" style="margin-top: 10px;"></div>
                       
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="fontSize">Họ và tên</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <span id="name_edit"/>
                        </div>
                        <div class="dropfield col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="fontSize">CMTND</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <span id="identitycard_edit"/>
                        </div>
                    </div>
                    <div class="clearfix" style="margin-top: 10px;"></div>
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="fontSize">Số thẻ HDV du lịch</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <input type="text" id="touristcode_edit" value="" maxlength="9" style="width: 100%;">
                            <br/>
                            <span class="errorTouristCode text-left hidden error-msg"></span>
                        </div>
                        <div class="dropfield col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="fontSize">Ngày hết hạn</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <input type="text" class="date datetime-input" id="expireddate_edit" value="" style="width: 100%;">
                            <br/>
                            <span class="errorExpiredDate text-left hidden error-msg"></span>
                        </div>
                    </div>
                    <div class="clearfix" style="margin-top: 10px;"></div>
                    <div class="line row">
                        <div class="col-md-3 col-sm-3 col-xs-4 no-break">
                            <span class="fontSize">Hướng dẫn viên</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-8 dialog-content">
                            <select name="tourist_guide_type" id="tourist_guide_type" style="width: 100%;">
                            </select>
                            <br/>
                            <span class="errorTouristGuideType text-left hidden error-msg"></span>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-info edit" id="btnsubmit" tabindex="6">
                            <span class='glyphicon glyphicon-check'></span> Đồng ý 
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal" tabindex="7">
                            <span class='glyphicon glyphicon-remove'></span> Hủy bỏ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .fontSize {
        font-size: 14px;
        font-weight: bold;
    }

    @media (min-width: 768px) {
        #fix-modal {
            width: 700px;
            margin: 30px auto;
        }
    }
</style>
