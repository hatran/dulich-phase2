<?php

use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
?>
<style>
    body .field-wrap.dropfield:after {
        top: 10px;
    }
    body .field-wrap .datetime-icon {
        top: 10px;
        right: 35px;
    }
</style>
<form class="page-wrap" name="filter" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Họ tên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="name" type="text" name="fullName" value="{{ htmlentities(array_get($response_view, 'fullName', '')) }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Ngày sinh
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="date datetime-input" type="text" name="birthday" id="birthday"
                           value="{{ array_get($response_view, 'birthday', '') }}" placeholder="Ngày Sinh">
                    <span class="datetime-icon fa fa-calendar"></span>
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số điện thoại
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="firstMobile" type="text" name="firstMobile" value="{{ htmlentities(array_get($response_view, 'firstMobile', '')) }}" maxlength="20">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Email
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="firstEmail" type="text" name="firstEmail" value="{{ htmlentities(array_get($response_view, 'firstEmail', '')) }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;">
            <input type="button" class="button btn-primary" style="width: 100px" name="search" id="search" onclick="submitForm();" value="Tìm kiếm">
        </div>
    </div>
</form>

<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });

    function submitForm() { 
        $('#formSearch').submit();
    };
</script>
