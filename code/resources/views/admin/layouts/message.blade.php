@if(session('successes'))
    <div id="closeButton" class="alert alert-success">
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            @foreach (session('successes') as $success)
                <li>{{ $success }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ($errors->any())
    <div id="closeButton" class="alert alert-danger">
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session('error_ajax'))
    <div id="closeButton" class="alert alert-danger">
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            @foreach (session('error_ajax') as $success)
                <li>{{ $success }}</li>
            @endforeach
        </ul>
    </div>
@endif
<script>
    $(document).ready(function() {
        $('#closeButton').on('click', function(e) {
            $('.alert').remove();
        });
    });
</script>
