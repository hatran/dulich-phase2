<?php
use App\Providers\UserServiceProvider;
use App\Libs\Helpers\Utils;
use App\Constants\UserConstants;
?>

<div id="header">
    <div class="page-wrap">
        <span class="fa fa-navicon"></span>
        <div class="top-bar">
            <a href="{{ url('/') }}" class="logo">
                <img src="{{ asset('images/logo.png') }}" class="logo"/>
            </a>
            <div class="main-menu">
                <ul>
                    @if (auth()->user()->canSee(['admin_member_list_view', 'admin_list_member_detail_view', 'admin_member_rank_view']))
                        @if (auth()->user()->role != UserConstants::CONTENT_MANAGER_SUB_GROUP_USER)
                            <li class="{{ Utils::highlightMenu(['officesys/member', 'officesys/list_members', 'officesys/rank_members', 'officesys/list_members/notofficialmember']) ? 'active' : '' }}">
                                <a>Quản Trị Hồ Sơ HV</a>
                                <ul class="sub-menu">
                                    @if (auth()->user()->canSee(['admin_member_list_view', 'admin_list_member_detail_view']))

                                        <li class="{{ Utils::highlightMenu(['officesys/list_members']) ? 'sub-active' : '' }}">
                                            <a href="{{ url('/officesys/list_members') }}">Danh Sách Hồ Sơ</a>
                                        </li>
                                    @endif
                                    @if (auth()->user()->canSee(['admin_member_rank_view']) && empty(auth()->user()->province_type))
                                        <li class="{{ Utils::highlightMenu(['officesys/rank_members']) ? 'sub-active' : '' }}">
                                            <?php
                                                $sta = \App\Constants\MemberConstants::MEMBER_OFFICIAL_MEMBER;
                                                $rank_status = \App\Constants\MemberConstants::RANK_REGISTER;
                                            ?>
                                            <a href="{{ url('/officesys/rank_members?status='.$sta.'&rank_status='.$rank_status) }}">Xếp hạng HDV</a>
                                        </li>
                                    @endif
                                    @if (auth()->user()->canSee(['admin_member']) && (UserServiceProvider::isExpertRole() || UserServiceProvider::isAdmin()))
                                        <li class="{{ Utils::highlightMenu(['officesys/member']) && request()->get('status') == \App\Constants\MemberConstants::VERIFICATION_WAITING ? 'sub-active' : '' }}">
                                            <a href="{{ url('/officesys/member') }}?status={{\App\Constants\MemberConstants::VERIFICATION_WAITING}}">Thẩm Định Hồ Sơ</a>
                                        </li>
                                    @endif
                                    @if (auth()->user()->canSee(['admin_member']) && (UserServiceProvider::isLeaderRole() || UserServiceProvider::isAdmin()))
                                        <li class="{{ Utils::highlightMenu(['officesys/member']) && request()->get('status') == \App\Constants\MemberConstants::APPROVAL_WAITING ? 'sub-active' : '' }}">
                                            <a href="{{ url('/officesys/member') }}?status={{\App\Constants\MemberConstants::APPROVAL_WAITING}}">Phê Duyệt Hồ Sơ</a>
                                        </li>
                                    @endif
                                    @if (auth()->user()->canSee(['admin_member_list_not_official']) && empty(auth()->user()->province_type))
                                        <li class="{{ Utils::highlightMenu(['officesys/list_members/notofficialmember']) ? 'sub-active' : '' }}">
                                            <a href="{{ url('/officesys/list_members/notofficialmember') }}">Danh Sách Hồ Sơ Chưa Là Hội Viên</a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                    @endif

                    {{--@if (UserServiceProvider::isAccountant())--}}
                    @if (auth()->user()->canSee(['admin_member_list_payment_view', 'admin_member_list_payment_yearly_view']))
                        <li class="{{ Utils::highlightMenu(['officesys/payment_yearly_new/search_payment_first', 'officesys/payment_yearly_new/search_payment_yearly']) ? 'active' : '' }}">
                            <a>Quản Lý Phí</a>
                            <ul class="sub-menu">
                                <!-- <li class="{{ Utils::highlightMenu(['officesys/payment_yearly_new/search_payment_first']) ? 'sub-active' : '' }}">
                                    <a href="{{ route('admin_member_list_payment_view') }}?status=4">Nộp hội phí lần đầu</a>
                                </li> -->

                                <li class="{{ Utils::highlightMenu(['officesys/payment_yearly_new/search_payment_yearly']) ? 'sub-active' : '' }}">
                                    <a href="{{ route('admin_member_list_payment_yearly_view') }}">Tra cứu phí</a>
                                </li>

                                <li class="{{ Utils::highlightMenu(['officesys/fee-policy/index']) ? 'sub-active' : '' }}">
                                    <a href="{{ route('fee_policy.index') }}">Quản trị hội phí</a>
                                </li>
                            </ul>
                        </li>
                    @endif

                    {{--@if (UserServiceProvider::isMemberShipCardIssuer())--}}
                    @if (auth()->user()->canSee(['admin_member_create_card_view', 'admin_member_do_create_card_view', 'admin_member_list_decision_view', 'admin_member_create_card_view', 'admin_member_print_excel_member_card_view']))
                    <li class="{{ Utils::highlightMenu(['officesys/decision', 'officesys/member/create_card', 'officesys/member/export_card_list']) ? 'active' : '' }}">
                        <a>Quản Trị Cấp Thẻ</a>
                        <ul class="sub-menu">
                            @if (auth()->user()->canSee(['admin_member_create_card_view']))
                                <li class="{{ Utils::highlightMenu(['officesys/member/create_card']) ? 'sub-active' : '' }}">
                                    <a href="{{ route('admin_member_create_card_view') }}?status={{\App\Constants\MemberConstants::CODE_PROVIDING_WAITING}}">Cấp Mã Hội Viên</a>
                                </li>
                            @endif

                            @if (auth()->user()->canSee(['admin_member_print_excel_member_card_view']))
                                <li class="{{ Utils::highlightMenu(['officesys/member/export_card_list']) ? 'sub-active' : '' }}">
                                    <a href="{{ route('admin_member_print_excel_member_card_view') }}?is_printed_card=130">In Thẻ Hội Viên</a>
                                </li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    {{--@if (UserServiceProvider::isAdmin())--}}
                    @if (auth()->user()->canSee(['users.index', 'users.create', 'users.show', 'users.edit', 'groups.index', 'groups.create', 'groups.show', 'admin_history_view', 'admin_history_emailsms_view', 'admin.openquiz']) && empty(auth()->user()->province_type))
                        <li class="{{ Utils::highlightMenu(['officesys/users', 'officesys/groups', 'officesys/history', 'officesys/question/openquiz', 'officesys/sendnoti', 'officesys/history/email_sms', 'officesys/emails']) ? 'active' : '' }}">
                            <a>Quản Trị Hệ Thống</a>
                            <ul class="sub-menu">
                                @if (auth()->user()->canSee(['users.index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/users']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/users') }}">Quản Trị Người Dùng</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['group_role.index']))
                                <li class="{{ Utils::highlightMenu(['officesys/group-role']) ? 'sub-active' : '' }}">
                                    <a href="{{ url('/officesys/group-role') }}">Quản Trị Quyền Truy Cập</a>
                                </li>
                                @endif
                                @if (auth()->user()->canSee(['groups.index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/groups']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/groups') }}">Quản Trị Vai Trò</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['categories.index', 'categories.edit', 'categories.store', 'categories.update', 'categories.destroy', 'admin_categories_sedit_detail', 'admin_categories_deletdit_detail', 'admin_categories_savedeletdit_detail']))
                                    <li class="{{ Utils::highlightMenu(['officesys/categories']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/categories?option_code=CATEGORY_01') }}">Quản Trị Danh Mục</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['admin_history_view']))
                                    <li class="{{ Utils::highlightMenu(['officesys/history']) && 'officesys/history' == Utils::getPath() ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/history') }}">Tra Cứu Lịch Sử Truy Cập Hệ Thống</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['admin_history_emailsms_view']))
                                    <li class="{{ Utils::highlightMenu(['officesys/history/email_sms']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/history/email_sms') }}">Tra Cứu Lịch Sử Gửi Email, SMS</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['emails.index', 'emails.create', 'emails.show']) && empty(auth()->user()->province_type))
                                    <li class="{{ Utils::highlightMenu(['officesys/emails']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/emails') }}">Quản Trị Nội Dung Email</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['admin.openquiz']) && empty(auth()->user()->province_type))
                                    <li class="{{ Utils::highlightMenu(['officesys/question/openquiz']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/question/openquiz') }}">Đóng/Mở Thi Xếp Hạng HDV</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['admin.sendnoti']) && empty(auth()->user()->province_type))
                                    <li class="{{ Utils::highlightMenu(['officesys/sendnoti']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/sendnoti') }}">Gửi thông báo</a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    {{--@if (UserServiceProvider::isAdmin())--}}
                    @if (auth()->user()->canSee(['admin_employee_view', 'branches.index', 'club_of_head_index', 'admin_representative_office_view', 'club_of_branch_index', 'club_of_branch_member_index']))
                        @if (auth()->user()->role == UserConstants::CONTENT_MANAGER_SUB_GROUP_USER)
                            <li class="{{ Utils::highlightMenu(['officesys/editor/listmember']) ? 'active' : '' }}">
                                <a href="#">Quản Trị Hội viên</a>
                                <ul class="sub-menu">
                                @if (auth()->user()->canSee(['admin_editor_listmember']))
                                    <li class="{{ Utils::highlightMenu(['officesys/editor/listmember']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/editor/listmember') }}">Danh sách hội viên</a>
                                    </li>
                                @endif
                                </ul>
                            </li>
                        @endif
                        <li class="{{ Utils::highlightMenu(['officesys/branches', 'officesys/club-of-head', 'officesys/club-of-branch', 'officesys/representative_office', 'officesys/employee']) ? 'active' : '' }}">
                            <a href="#">Quản Trị Cơ Cấu Tổ Chức</a>
                            <ul class="sub-menu">
                                @if (auth()->user()->canSee(['admin_representative_office_view']))
                                    <li class="{{ Utils::highlightMenu(['officesys/representative_office']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/representative_office') }}">Quản Trị Văn Phòng Đại Diện</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['club_of_head_index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/branches/club-of-head']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/branches/club-of-head') }}">Quản Trị CLB Thuộc Hội</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['branches.index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/branches']) && 'officesys/branches' == Utils::getPath() ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/branches') }}">Quản Trị Chi Hội</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['club_of_branch_index', 'club_of_branch_member_index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/branches/club-of-branch']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/branches/club-of-branch') }}">Quản Trị CLB Thuộc Chi Hội</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['admin_employee_view']))
                                    <li class="{{ Utils::highlightMenu(['officesys/employee']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/employee') }}">Quản Trị Ban chấp hành</a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    @if (auth()->user()->canSee(['admin.news_ch_clb.index', 'infomation.index', 'admin.news_ch.index', 'admin.news_clb.index', 'infomation.index', 'subjectknowledge.index', 'evaluationcriteria.index', 'travelercompanyrank.index']))
                        <li class="{{ Utils::highlightMenu(['officesys/banners', 'officesys/introductions', 'officesys/news', 'officesys/news-branches', 'news-club', 'officesys/infomation', 'officesys/subjectknowledge', 'officesys/travelercompanyrank', 'officesys/evaluationcriteria', 'company/evaluate/list']) ? 'active' : '' }}">
                            <a href="#">Quản Trị Tin Tức và Thông Tin</a>
                            <ul class="sub-menu">
                                @if (auth()->user()->canSee(['banners.index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/banners']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/banners') }}">Quản Trị Hình ảnh và Quảng cáo</a>
                                    </li>
                                @endif

                                @if (auth()->user()->canSee(['introductions.index', 'introductions.create', 'introductions.show']))
                                    <li class="{{ Utils::highlightMenu(['officesys/introductions']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/introductions') }}">Quản Trị CMS</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['admin.news_ch_clb.index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/news']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/news') }}">Quản Trị Tin Tức và Sự Kiện Hội</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['admin.news_ch.index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/news-branches']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/news-branches') }}">Quản Trị Tin tức và sự kiện Chi Hội</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['admin.news_clb.index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/news-club']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/news-club') }}">Quản Trị Tin Tức và Sự Kiện CLB Thuộc Hội</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['infomation.index']))
                                    <li class="{{ Utils::highlightMenu(['officesys/infomation']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/infomation') }}">Quản Trị Thông Tin</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['subjectknowledge.index', 'admin_subjectknowledge_deletdit_detail', 'admin_subjectknowledge_sedit_detail', 'admin_subjectknowledge_savedeletdit_detail']))
                                    <li class="{{ Utils::highlightMenu(['officesys/subjectknowledge']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/subjectknowledge') }}">Quản Trị Danh Mục Câu Hỏi Kiến Thức</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['evaluationcriteria.index', 'admin_evaluationcriteria_deletdit_detail', 'admin_evaluationcriteria_sedit_detail', 'admin_evaluationcriteria_savedeletdit_detail']))
                                    <li class="{{ Utils::highlightMenu(['officesys/evaluationcriteria']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/evaluationcriteria') }}">Quản Trị Danh Mục Tiêu Chí Đánh Giá</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['travelercompanyrank.index', 'admin_travelercompanyrank_deletdit_detail', 'admin_travelercompanyrank_sedit_detail', 'admin_travelercompanyrank_savedeletdit_detail']))
                                    <li class="{{ Utils::highlightMenu(['officesys/travelercompanyrank']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/travelercompanyrank') }}">Quản Trị Công Ty Lữ Hành</a>
                                    </li>
                                @endif
                                @if (empty(auth()->user()->province_type))
                                    <li class="{{ Utils::highlightMenu(['/company/evaluate/list']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/company/evaluate/list') }}">Quản Trị Danh Sách Đánh Giá Của Công Ty Lữ Hành </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endif
                        @if (auth()->user()->canSee(['admin_member_new_list_export_view', 'admin_member_general_report_view']) && empty(auth()->user()->province_type))
                        <li class="{{ Utils::highlightMenu(['officesys/list_members/list_export', 'officesys/list_members/general_report']) ? 'active' : '' }}">
                            <a href="#">Thống Kê Báo Cáo</a>
                            <ul class="sub-menu">
                                @if (auth()->user()->canSee(['admin_member_new_list_export_view']))
                                    <li class="{{ Utils::highlightMenu(['officesys/list_members/list_export']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/list_members/list_export') }}">Tổng Số Hồ Sơ</a>
                                    </li>
                                @endif
                                @if (auth()->user()->canSee(['admin_member_general_report_view']))
                                    <li class="{{ Utils::highlightMenu(['officesys/list_members/general_report']) ? 'sub-active' : '' }}">
                                        <a href="{{ url('/officesys/list_members/general_report') }}">Báo Cáo Tổng Hợp</a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        @endif
                </ul>
            </div>
        </div>
        <div class="user-buttons">
            <img src="{{ asset('admin/images/ava.png') }}"/>
            <div class="user-options">
                <a href="{{ route('admin_user_profile') }}">Thông Tin Người Dùng</a>
                <a href="{{ route('admin_user_change_password') }}">Thay Đổi Mật Khẩu</a>
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng Xuất</a>
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </div>
</div>
