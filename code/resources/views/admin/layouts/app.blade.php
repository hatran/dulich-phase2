<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/arcana.css ') }}" />

    <script type="text/javascript" src="{{ asset('vendors/jQuery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tinymce/jquery.tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('images/favicon.ico') }}" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}" />

    <!-- Bootstrap CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

    <!-- toastr notifications -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{asset('admin/css/jquery.loader.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/offices.css')}}"/>


    <script>
        tinymce.init({
            selector:'textarea.tinymce',
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern codesample",
                "toc imagetools help"
            ],
            // max_word: 2000,
            // setup: function (ed) {
            //     ed.on('keyup', function (e) {
            //         var writtenWords = $('.mce-wordcount').html();
            //         writtenWords = writtenWords.replace("Words: ", "");
            //         // var maxWord = ed.settings.max_word;
            //         var limited = "";
            //         var content = ed.getContent();
            //         if (parseInt(writtenWords) >= parseInt(maxWord)) {
            //             $('.mce-wordcount').css("color", "red");
            //             limited = $.trim(content).split(" ", maxWord);
            //             limited = limited.join(" ");
            //             ed.setContent(limited);
            //         } else {
            //             $('.mce-wordcount').css("color", "green");
            //         }
            //     });
            // },
            relative_urls : false,
            remove_script_host : false,
            convert_urls : true,
            // without images_upload_url set, Upload tab won't show up
            images_upload_url: '{{ URL::route("tinymce_upload") }}',
            // override default upload handler to simulate successful upload
            images_upload_handler: function (blobInfo, success, failure) {
                if (blobInfo.blob().size/1024/1024 > 10) {
                    failure('Dung lượng ảnh upload phải nhỏ hơn 10Mb');
                    return;
                }
                var xhr, formData;
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', '{{ URL::route("tinymce_upload") }}');
                xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}');
                xhr.onload = function() {
                    var json;
                    if (xhr.status != 200) {
                        failure(xhr.status);
                        return;
                    }

                    json = JSON.parse(xhr.responseText);
                    if (json.error) {
                        failure(json.error.image[0]);
                        return;
                    }
                    if (!json || typeof json.location != 'string') {
                        failure(xhr.responseText);
                        return;
                    }

                    success(json.location);
                };
                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());
                xhr.send(formData);
            },
        })
    </script>
    <style>
        .main-content {
            box-shadow: none !important;
        }
        .steps {
            padding: 0 15px;
        }
    </style>
    @yield('header_embed')
</head>
<body>
@include('admin.layouts.header')

<div id="content">
    @yield('content')
</div>
@yield('footer_embed')
</body>
</html>
