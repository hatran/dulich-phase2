
<?php
    use App\Providers\UserServiceProvider;
    use App\Constants\MemberConstants;
?>
<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
    <div class="block-heading">Thông tin hồ sơ hội viên:</div>
    <div class="block-content">
        <div class="table-line"> 
            <div class="line row">
                <div class="line-content col-md-4 col-sm-4 col-xs-12">
                    <span>Mã hồ sơ: </span> {{ !empty($objMember->file_code) ? htmlentities($objMember->file_code) : '' }}
                </div>
				<div class="line-content col-md-4 col-sm-4 col-xs-12">
                    <span>Gia nhập VTGA từ: </span> {{ !empty($objMember->member_from) ? htmlentities($objMember->member_from) : '' }}
                </div>
            </div>
			<div class="line row">
                <div class="line-content col-md-4 col-sm-4 col-xs-12">
                    <span>Số thẻ hội viên: </span> {{ !empty($objMember->member_code) ? htmlentities($objMember->member_code) : '' }}
                </div>            
                <div class="line-content col-md-4 col-sm-4 col-xs-12">
                    <span>Ngày hết hạn: </span> {{ !empty($objMember->member_code_expiration) ? date('d/m/Y', strtotime(htmlentities($objMember->member_code_expiration))) : '' }}
                </div>
            </div>
			<div class="line row">
                <div class="line-content col-md-4 col-sm-4 col-xs-12">
                    <span>Trạng thái hội viên: </span> 
										@if ($objMember->wCardType == '1')
                                            {{ 'Chờ in thẻ gia hạn' }}
                                        @elseif ($objMember->wCardType == '2')
                                            {{ 'Chờ in thẻ cấp lại' }}
                                        @elseif ($objMember->wCardType == '0')
                                            {{ 'Chờ in thẻ lần đầu' }}
                                        @elseif ($objMember->pCardType == '1')
                                            {{ 'Đã in thẻ gia hạn' }}
                                        @elseif ($objMember->pCardType == '2')
                                            {{ 'Đã in thẻ cấp lại' }}
                                        @elseif ($objMember->pCardType == '0')
                                            {{ 'Hội viên chính thức' }}
                                        @else
                                            {{ array_get(MemberConstants::$file_const_display_status, $objMember->status, '') }}
                                        @endif
                </div>
            </div>
        </div>               
    </div>    
</div>