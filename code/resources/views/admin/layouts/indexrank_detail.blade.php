<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Constants\OtherConstants;
use App\Providers\UserServiceProvider;
?>
<div class="block">
    <div class="block-heading">1. Thông tin chung</div>
    <div class="block-content">
        <div class="table-line" style="width: 550px !important; margin-bottom: 0">
            <div class="line row">
                <div class="line-content col-md-4 col-sm-4 col-xs-12">
                    <span>Họ và tên:</span> {{ !empty($objMember->fullName) ? htmlentities($objMember->fullName) : '' }}
                </div>
            </div>
            <div class="line row">
                <div class="line-content col-md-12 col-sm-12 col-xs-12">
                    <span style="display: inline-block; width: 300px; line-height: 12px; height: 12px">Giới tính: {{ ($objMember->gender == 1) ? 'Nam' : 'Nữ' }} </span> <span style="line-height: 12px; width: 300px; height: 12px"> Ngày sinh: {{ empty($objMember->birthday) ? '' : date('d/m/Y', strtotime($objMember->birthday)) }}</span>
                </div>
            </div>
            <div class="line row">
                <div class="line-content">
                    <span>Hướng dẫn viên:</span> {{ !empty($typeOfTravelGuide) ? array_get(MemberConstants::$member_typeOfTravelGuide, $typeOfTravelGuide, '') : '' }}
                </div>
            </div>
            @if($typeOfTravelGuide == 1)
            <div class="line">
                <div class="line-content">
                    <span>Inbound / Outbound:</span>
                        @if ($objMember->inboundOutbound == 1 && $typeOfTravelGuide == 1)
                            <span style="font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;color: #3D3A3A;">HDV Du Lịch Quốc Tế Inbound</span>
                        @elseif ($objMember->inboundOutbound == 2 && $typeOfTravelGuide == 1)
                            <span style="font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;color: #3D3A3A;">HDV Du Lịch Quốc Tế Outbound</span>
                        @elseif ($objMember->inboundOutbound == 3 && $typeOfTravelGuide == 1)
                            <span style="font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;color: #3D3A3A;">HDV Du Lịch Quốc Tế Inbound - Outbound</span>
                        @endif
                </div>
            </div>
            @endif
            <div class="line row" style="padding-top: 5px; height: 40px">
                <div class="line-content col-md-12 col-sm-12 col-xs-12" style="border-bottom: 1px solid #fff;">
                    <span style="display: inline-block; width: 300px;">Số thẻ HDV du lịch: {{ $objMember->touristGuideCode }}</span>
                @if($typeOfTravelGuide == 1 || $typeOfTravelGuide == 2)
                <span style="display: inline-block; width: 300px;">
                    Ngày hết hạn: {{ empty($objMember->expirationDate) ? '' : date('d/m/Y', strtotime($objMember->expirationDate)) }}</span>
                @endif
                </div>                        
            </div>
        </div>
        @if (!empty($profileImg))
            @if($isProfile)
                <div class="photo" style="float: right; position: absolute;right: 0; top: 36px;">
            @else 
                <div class="photo" style="float: right;">
            @endif
                    <img src="{{ $profileImg }}" style="width: 150px;" height="180"/>
                </div>
        @else
            <div class="photo" style="float: right;">
                <img src="{{ asset('images/3_4.jpg') }}" style="width: 150px;" height="180" />
            </div>
        @endif
    </div>
    <div class="block-content">
        <div class="table-line">
            <div class="line" style="border-top: 1px solid #bbbaba; line-height: 18px" >
                <div class="line-content">
                    <span style="display: inline-block; width: 300px; line-height: 12px; height: 12px">Số CMT/CCCD: {{ !empty($objMember->cmtCccd) ? htmlentities($objMember->cmtCccd) : '' }}</span> <span style="display: inline-block; width: 300px; line-height: 12px; height: 12px"> Cấp ngày: {{ empty($objMember->dateIssued) ? '' : date('d/m/Y', strtotime($objMember->dateIssued)) }}</span><br/><br/>
                    <span style="display: inline-block; width: 300px; line-height: 12px; height: 12px;  word-wrap: break-word;">Nơi cấp: {{ empty($objMember->issuedBy) ? '' : htmlentities($objMember->issuedBy) }}</span> 
                </div>
            </div>
        </div>
    </div>
    <div class="block-content">
        <div class="table-line">
            <div class="line" style="border-top: 1px solid #bbbaba;" >
                <div class="line-content" style=" word-wrap: break-word;">
                    <span>Địa chỉ thường trú:</span> {{ empty($objMember->permanentAddress) ? '' : htmlentities($objMember->permanentAddress) }}
                </div>
            </div>
            <div class="line">
                <div class="line-content" style="padding-top: 10px; word-wrap: break-word;">
                    <span>Địa chỉ liên hệ:</span> {{ empty($objMember->address) ? '' : htmlentities($objMember->address) }}
                </div>
            </div>
            <div class="line">
                <div class="line-content">
                    <span>Địa bàn hoạt động:</span>
                    {{ $objMember->province ? array_get($provincial, $objMember->province, '') : ''}}
                </div>
            </div>
            <div class="line row">
                <div class="line-content col-md-12 col-sm-12 col-xs-12">
                    <span style="display: inline-block; width: 300px; line-height: 12px; height: 12px">Điện thoại di động 1: {{ empty($objMember->firstMobile) ? '' : $objMember->firstMobile }}</span>
                    <span style="line-height: 12px; height: 12px">Điện thoại di động 2: {{ empty($objMember->secondMobile) ? '' : htmlentities($objMember->secondMobile) }}</span>
                </div>
            </div>
            <div class="line row">
                <div class="line-content col-md-12 col-sm-12 col-xs-12">
                    <span style="display: inline-block; width: 300px; line-height: 12px; height: 12px">Email 1: {{ empty($objMember->firstEmail) ? '' : $objMember->firstEmail }}</span>
                     <span style="line-height: 12px; height: 12px">Email 2: {{ empty($objMember->secondEmail) ? '' : htmlentities($objMember->secondEmail) }}</span>
                </div>
            </div>
            
            <div class="line">
                <div class="line-content">
                    <span>Sinh hoạt tại:</span>
                    {{ $typeOfPlace == 1 ? 'Chi hội ' . array_get($branch_chihoi, $objMember->province_code, '') : '' }}
                    {{ $typeOfPlace == 2 ? 'CLB thuộc hội ' . array_get($branch_clbthuochoi, $objMember->province_code, '') : '' }}
                </div>
            </div>
            <div class="line">
                <div class="line-content">
                    <span>Ngôn ngữ hướng dẫn:</span>
                    @php
                        $language = $objMember->guideLanguage;
                    @endphp
                    <?php $languageName = ""; ?>
                    @foreach(explode(",", $language) as $key => $lang)
                        @foreach($languages as $data => $val)
                            @if($data == $lang)
                                <?php $languageName .= $val ."," ?>
                            @endif
                        @endforeach
                    @endforeach
                    {{ rtrim($languageName, ",") }} 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">2. THÔNG TIN HỌC VẤN</div>
    <div class="block-content">
        <div class="table-line">
            <div class="line">
                <span> Chuyên ngành: {{ empty($objBrand->branchName) ? '' : $objBrand->branchName }}  &nbsp; Trình độ: {{ empty($objDegree->degree) ? '' : $objDegree->degree }} </span>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">3. THÔNG TIN VỀ NGHIỆP VỤ HƯỚNG DẪN</div>           
    <div class="block-content">
        <div class="table-line">
            <div class="line">
                <span>
                    {{ empty($objMajor) ? '' : $objMajor->majorName }}
                </span>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">4. THÔNG TIN VỀ TRÌNH ĐỘ NGOẠI NGỮ</div>            
    <div class="block-content">
        <div class="table-line">
            <div class="line">
                <p>Ngoại ngữ: {{ empty($objLanguage) ? '' : $objLanguage->languageName }}<br/>
               Trình độ: {{ empty($objLanguageLevel) ? '' : $objLanguageLevel->levelName }}</p>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">5. THÔNG TIN VỀ QUÁ TRÌNH HÀNH NGHỀ HƯỚNG DẪN</div>    
    @if (!empty($workHistories))
        @foreach($workHistories as $key => $work)
            <div class="block-content">
                <div class="table-line">
                    <div class="line">
                        <p style="word-wrap: break-word; width: 550px">{{ $key + 1 }}. Tên công ty: {{ empty($work->elementName) ? '' : htmlentities($work->elementName) }} <br/>
                        <span style="display: inline-block; width: 300px; line-height: 12px; height: 12px">Hình thức lao động: {{ empty($work->typeOfContract) ? '' : array_get($workConstant, $work->typeOfContract, '') }} </span>
                        <span style="display: inline-block; line-height: 12px; height: 12px">Thời gian từ: {{ empty($work->fromDate) ? '' : date('Y', strtotime($work->fromDate)) }} &nbsp;&nbsp;&nbsp;&nbsp;
                        Thời gian đến: {{ empty($work->toDate) ? '' : date('Y', strtotime($work->toDate)) }}</span> </p>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
<div class="block">
    <div class="block-heading">6. SỞ TRƯỜNG HƯỚNG DẪN</div>
    <div class="block-content">
        <div class="table-line">
            <div class="line">                    
                    @php
                        $forteTour = $objMember->forteTour;
                    @endphp
                    <?php $forteTourName = ""; ?>
                    @foreach(explode(",", $forteTour) as $key => $lang)
                        @foreach($forteTours as $data => $val)
                            @if($data == $lang)
                                <?php $forteTourName .= $val ."," ?>
                            @endif
                        @endforeach
                    @endforeach
                    <p>Loại hình: {{ $typeGuideName or '' }} &nbsp;
                        Quy mô đoàn: {{ $groupSizeName or '' }} &nbsp;
                        Tuyến du lịch sở trường : {{ rtrim($forteTourName, ",") }} 
                        </p>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">7. KINH NGHIỆM HƯỚNG DẪN</div>
    <div class="block-content">
        <div class="table-line">
            <div class="line">
                <p>Năm bắt đầu hướng dẫn: {{ $objMember->experienceYear }} &nbsp;
                    Số năm kinh nghiệm: {{ !empty($objMember->experienceLevel) ? array_get($otherConstant, $objMember->experienceLevel, '') : ''  }} </p>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">8. CÁC KỸ NĂNG CHUYÊN MÔN KHÁC (MC, GAME SHOW,...)</div>
    <div class="block-content">
        <div class="table-line">
            <div class="line" style="word-wrap: break-word;">
                <div class="line-inline">
                    @if (empty($objMember->otherSkills))
                        
                    @else
                        {{ htmlentities($objMember->otherSkills) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">9. THÔNG TIN KHÁC</div>
    <div class="block-content">
        <div class="table-line">
            <div class="line" style="word-wrap: break-word;">
                <div class="line-inline">
                    @if (empty($objMember->otherInformation))
                        
                    @else
                        {{ htmlentities($objMember->otherInformation) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">10. HẠNG HƯỚNG DẪN VIÊN</div>
    <div class="block-content">
        <div class="table-line">
            <div class="line" style="word-wrap: break-word;">
                <div class="line-inline">
                    <!-- @if (empty($objMember->touristGuideLevel))
                        
                    @else
                        {{ htmlentities($objMember->touristGuideLevel) }}
                    @endif  -->
                    <div class="block-content">
                        @if ($objMember->status == $memberOfficial)
                            @if ($objMember->rank_status > 0)
                                @if ($objMember->rank_status != 4)
                                    Đăng ký {{ $ranks[$memberRank->rank_id] }}
                                @else 
                                    $ranks[$memberRank->id]
                                @endif
                            @else
                                Chưa xếp hạng HDV
                            @endif
                        @else
                            Chưa xếp hạng HDV
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block">
    <div class="block-heading">11. THÀNH TÍCH (KHEN THƯỞNG, GIẤY KHEN, BẰNG KHEN,...)</div>
    <div class="block-content">
        <div class="table-line">
            <div class="line" style="word-wrap: break-word;">
                <div class="line-inline">
                    @if (empty($objMember->achievements))
                        
                    @else
                        {{ htmlentities($objMember->achievements) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
