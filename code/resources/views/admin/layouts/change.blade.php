<?php
    use App\Providers\UserServiceProvider;
    use App\Constants\MemberConstants;
?>
<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
	<div class="block-heading">Lịch sử tác động:</div>
	<div class="block-content" style="margin-top:  10px;">
		<div class="table-line"> 
			@if (count($notes) > 0 && $note->function_type == MemberConstants::IS_NUMBER_TOUR_GUIDE_CHANGE)
			<div class="table-wrap">
				<table>
				<thead>
				<tr>
					<td width="50">Số TT</td>
					<td width="250">Người tác động</td>
					<td width="150">Ngày tác động</td>
					<td width="150">Nội dung tác động</td>
					<td width="800">Ghi chú</td>
				</tr>
				</thead>
				<tbody>
					@foreach ($notes as $key => $note)
						<tr>
						@if (!empty($note) && $note->function_type == MemberConstants::IS_NUMBER_TOUR_GUIDE_CHANGE)
							<td class="text-right">{{ ++$key }}</td>
							<td class="text-left">{{ $note->fullname }}</td>
							<td class="text-center">{{ !empty($note->created_at) ? $note->created_at->format('d/m/Y') : '' }}</td>
							<td class="text-left">{{ array_get(MemberConstants::$file_const_history, $note->action_type, '') }}</td>
							<td class="text-left">
								<div style="width: 500px; height: auto; white-space: pre-wrap;">{{  htmlentities($note->note) }}</div></td>
						@endif
						</tr>
					@endforeach
				</tbody>
				</table>
			</div>
			@endif
		</div>
	</div>
</div>
