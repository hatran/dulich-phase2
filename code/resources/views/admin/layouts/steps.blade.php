<?php 
	use App\Providers\UserServiceProvider;
    use App\Constants\UserConstants;
?>
@if (auth()->user()->role != UserConstants::CONTENT_MANAGER_SUB_GROUP_USER)
<div class="steps">
    @if (UserServiceProvider::isExpertRole())
    <div class="step @if(Route::currentRouteName() == 'admin_member') active @endif" data-step="1"><a href="{{ route('admin_member') }}?status={{\App\Constants\MemberConstants::VERIFICATION_WAITING}}"><span class="number">1</span><br><span class="desc">Thẩm Định</span></a></div>
    @else
    	<div class="step" data-step="1"><span class="number">1</span><br><span class="desc">Thẩm Định</span></div>
    @endif
    @if (UserServiceProvider::isLeaderRole())
    	<div class="step @if(Route::currentRouteName() == 'admin_member') active @endif" data-step="2"><a href="{{ route('admin_member') }}?status={{\App\Constants\MemberConstants::APPROVAL_WAITING}}"><span class="number">2</span><br><span class="desc">Phê Duyệt</span></a></div>
    @else
    	<div class="step" data-step="2"><span class="number">2</span><br><span class="desc">Phê Duyệt</span></div>
    @endif
    @if (UserServiceProvider::isAccountant())
    	<div class="step {{ Route::currentRouteName() == 'admin_member_list_payment_view' ? 'active' : ' '}}" data-step="3"><a href="{{ route('admin_member_list_payment_view') }}?status={{\App\Constants\MemberConstants::FEE_WAITING}}"><span class="number">3</span><br><span class="desc">Đóng Lệ Phí + Hội Phí</span></a></div>
    @else
    	<div class="step" data-step="3"><span class="number">3</span><br><span class="desc">Đóng Lệ Phí + Hội Phí</span></div>
    @endif
    @if (UserServiceProvider::isMemberShipCardIssuer())
        <div class="step {{ Route::currentRouteName() == 'admin_member_create_card_view' ? 'active' : ' '}}" data-step="4"><a href="{{ route('admin_member_create_card_view') }}?status={{\App\Constants\MemberConstants::CODE_PROVIDING_WAITING}}"><span class="number">4</span><br><span class="desc">Cấp Mã Hội Viên</span></a></div>
    @else
        <div class="step" data-step="4"><span class="number">4</span><br><span class="desc">Cấp Mã Hội Viên</span></div>
    @endif
    @if (UserServiceProvider::isMemberShipCardIssuer())
        <div class="step {{ Route::currentRouteName() == 'admin_member_print_excel_member_card_view' ? 'active' : ' '}}" data-step="5"><a href="{{ route('admin_member_print_excel_member_card_view') }}?status={{\App\Constants\MemberConstants::CARD_PRINTING_WAITING}}"><span class="number">5</span><br><span class="desc">In Thẻ</span></a></div>
    @else
        <div class="step" data-step="5"><span class="number">5</span><br><span class="desc">In Thẻ</span></div>
    @endif

</div>
@endif