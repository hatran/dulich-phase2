<style>
    .pagination-a li a {
        border: none;
        background: #ddd;
        margin-left: 4px;
        color: #000;
        font-size: 14px;
        border-radius: 2px;
    }
</style>

@if (!empty($paginator))
    @if ($paginator->lastPage() > 1)
        <div class="pagination-a">
            <ul class="pagination pagination-sm pull-right">
                <li>
                    <a href="{{ $paginator->url($paginator->url(1)) }}">
                        Trang đầu
                    </a>
                </li>
                {{--@if ($paginator->currentPage() != 1 && $paginator->lastPage() >= 5)
                    <li>
                        <a href="{{ $paginator->url($paginator->url(1)) }}">
                            <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>
                    </li>
                @endif--}}

                @if($paginator->currentPage() != 1)
                    <li>
                        <a href="{{ $paginator->url($paginator->currentPage()-1) }}" style="color: #969090; height: 31px">
                            <i class="fa fa-angle-left" aria-hidden="true" style="line-height: 21px;"></i>
                        </a>
                    </li>
                @endif

                @for($i = max($paginator->currentPage() - 2, 1); $i <= min(max($paginator->currentPage() - 2, 1) + 4, $paginator->lastPage()); $i++)
                    <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endfor

                @if (($paginator->currentPage() + 10) <= $paginator->lastPage())
                    <li><a href="#" style="border-top: none; border-bottom: none; letter-spacing: 4px; padding-top: 14px; background: none;">...</a></li>
                    <li><a href="{{ $paginator->url($paginator->currentPage() + 10) }}">{{ $paginator->currentPage() + 10 }}</a></li>
                @endif



                @if ($paginator->currentPage() != $paginator->lastPage())
                    <li>
                        <a href="{{ $paginator->url($paginator->currentPage()+1) }}" style="color: #969090; height: 31px">
                            <i class="fa fa-angle-right" aria-hidden="true" style=" line-height: 21px;"></i>
                        </a>
                    </li>
                @endif

                <li>
                    <a href="{{ $paginator->url($paginator->lastPage()) }}">
                        Trang cuối
                    </a>
                </li>
                {{--@if ($paginator->currentPage() != $paginator->lastPage() && $paginator->lastPage() >= 5)
                    <li>
                        <a href="{{ $paginator->url($paginator->lastPage()) }}">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </a>
                    </li>
                @endif--}}
            </ul>
        </div>



        {{--<div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right">
                @if ($paginator->currentPage() != 1 && $paginator->lastPage() >= 5)
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->url($paginator->url(1)) }}" aria-label="Begin">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Begin</span>
                        </a>
                    </li>
                @endif

                @if($paginator->currentPage() != 1)
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->url($paginator->currentPage()-1) }}" aria-label="Previous">
                            <span aria-hidden="true">&lsaquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                @endif

                @for($i = max($paginator->currentPage()-2, 1); $i <= min(max($paginator->currentPage()-2, 1)+4,$paginator->lastPage()); $i++)
                    <li class="page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                        <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endfor


                @if ($paginator->currentPage() != $paginator->lastPage())
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->url($paginator->currentPage()+1) }}" aria-label="Next">
                            <span aria-hidden="true">&rsaquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                @endif

                @if ($paginator->currentPage() != $paginator->lastPage() && $paginator->lastPage() >= 5)
                    <li class="page-item">
                        <a class="page-link" href="{{ $paginator->url($paginator->lastPage()) }}" aria-label="End">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">End</span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>--}}
    @endif
@endif

