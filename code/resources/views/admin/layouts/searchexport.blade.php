<?php

use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
?>
<style>
    body .field-wrap.dropfield:after {
        top: 10px;
    }
    body .field-wrap .datetime-icon {
        top: 10px;
        right: 35px;
    }
</style>
<form class="page-wrap" name="filter" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row" style="width: 98%">
        @if (!isset($member_status))
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
                Ngày tạo hồ sơ
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="from_date" id="from_date" tabindex="1" value="{{ array_get($response_view, 'from_date', '') }}" placeholder="Từ ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="to_date" id="to_date"
                       value="{{ array_get($response_view, 'to_date', '') }}" placeholder="Đến ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
        @endif
        @if (empty(auth()->user()->branch_id))
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
                Nơi đăng ký sinh hoạt
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select class="province_code" id="province_code" name="province_code" tabindex="2">
                    <option value="" selected></option>
                    @foreach(\App\Models\Offices::whereNotNull('parent_id')->get() as $key => $val)
                        <option value="{{ $val->id }}" data-parent="{{$val->parent_id}}"
                                @if (array_get($response_view, 'province_code', '') == $val->id) selected="selected" @endif>{{ $val->name }}</option>
                    @endforeach
                </select>
            </div>
        @endif

        @if (\Route::current()->getName() == 'admin_member_general_report_view')
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
                Trạng thái hồ sơ
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select name="member_status" tabindex="3">
                    <option value="" selected>Tất cả</option>
                    @foreach($member_status as $key => $val)
                        <option value="{{ $key }}" @if (array_get($response_view, 'member_status', '') == $key) selected="selected" @endif>{{ $val }}</option>
                    @endforeach
                </select>
            </div>
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12" style="padding-top: 7px">
                Hội viên VTGA từ
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input type="text" name="member_from" id="member_from" tabindex="4" value="{{ array_get($response_view, 'member_from', '') }}" placeholder="Hội viên VTGA từ ngày" maxlength="4" onkeypress="return isNumber(event)">
            </div>
        @endif
        @if (\Route::current()->getName() == 'admin_member_general_report_view')
            @php $tabIndex = 5; @endphp
        @else
            @php $tabIndex = 3; @endphp
        @endif
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;">
            <input type="button" class="button btn-primary" style="width: 100px" name="search" id="search" onclick="submitForm();" value="Tìm kiếm" tabindex="{{ $tabIndex }}">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm hồ sơ</h4>
            </div>
            <div class="modal-body">
                @if($step == 3)
                    <h4 class="text-center">Ngày chứng từ không hợp lệ</h4>
                @else
                    <h4 class="text-center">Ngày tạo hồ sơ không hợp lệ</h4>
                @endif
                <br/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
    var isValild = true;
    $('#from_date').on('dp.change', function(e){
        fromDate = parseDate($(this).val());
        toDate = parseDate($('#to_date').val());
        if(fromDate > toDate)
        {
           isValild = false;
        } else {
            isValild = true;
        }
    });

    $('#to_date').on('dp.change', function(e){
        toDate = parseDate($(this).val());
        fromDate = parseDate($('#from_date').val());
        if(fromDate > toDate)
        {
           isValild = false;
        } else {
            isValild = true;
        }
    });
    function parseDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    function submitForm() {
        if(isValild == false)
        {
           $("#showAlert").modal('show');
           return;
        }
        else {
            $('#formSearch').submit();
        }

    };

    $(".select_vpdd").change(function () {


        $id = $(this).find(":selected").val();


        // if ($id == 1) {
        //     var arrayFromPHP = <?php //echo $hnArea; ?>;


        // } else if ($id == 2) {

        //     var arrayFromPHP = <?php //echo $dnArea; ?>;


        // } else if ($id == 3) {
        //     var arrayFromPHP = <?php //echo $hcmArea ?>;


        // } else if ($id == '') {
        //     var arrayFromPHP = <?php //echo $allArea; ?>;


        // }

        // var data = arrayFromPHP;
        // var $select = $('#province_code');
        // $('#province_code').empty();
        // $select.append('<option value=""></option>');
        // $.each(data, function (i, value) {
        //     var prefix = '';
        //     if (i == 65) {
        //         prefix = "";
        //     } else {
        //         prefix = "Chi hội HDV du lịch ";
        //     }
        //     var option = $('<option value="' + i + '">' + prefix + value + '</option>');
        //     $select.append(option);
        //
        // });
    });

    var changeProvinceCode = function(){
        var val = jQuery('#province_type').val();
        $.each(jQuery('#province_code option'), function (i, value) {
            jQuery(this).show();
            if(jQuery(this).data('parent') == val){
                jQuery(this).show();
            } else jQuery(this).hide();
        });
    }
</script>
