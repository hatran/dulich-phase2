<?php
    use App\Constants\MemberConstants;
?>
<div class="block" style="margin-top:  10px; padding-bottom: 10px;">
	<div class="block-heading">Ghi chú của thành viên:</div>
	<div class="" style="margin-top: 20px;margin-bottom: 20px;">
		<button type="button" class="btn btn-link">
			Tổng số <span class="badge badge-light">{{ count($memberNotes) }}</span>
		</button>
		<button id="showAreaNoteForm" type="button" class="btn btn-info right" style="float: right;">
			Thêm mới
		</button>
	</div>
	<div class="clearfix"></div>
	<div class="block-content" style="margin-top:  10px;">
		<div class="table-line">
			@if (count($memberNotes) > 0)
			<div class="table-wrap">
				<table>
				<thead>
				<tr>
					<td width="50">Số TT</td>
					<td width="250">Ghi chú</td>
					<td width="150">Người tạo</td>
					<td width="150">Ngày tạo<br />(dd/mm/yyyy)</td>
				</tr>
				</thead>
				<tbody>
					@foreach ($memberNotes as $key => $note)
						<tr>
							<td class="text-center">{{ ++$key }}</td>
							<td class="text-left">
								<div style="width: 500px; height: auto; white-space: pre-wrap;">{!! $note->content !!} </div>
							</td>
							<td class="text-center">{{ \App\Models\MemberNote::getAuthorName($note->author_id) }}</td>
							<td class="text-center">{{ !empty($note->created_at) ? $note->created_at->format('d/m/Y') : '' }}</td>
						</tr>
					@endforeach
				</tbody>
				</table>
			</div>
				{{ $memberNotes->appends(['is_note' => 1])->links() }}
			@endif
		</div>
	</div>
	<div class="form-add-new" style="margin-top: 20px; display: none">
		<h5 class="page-header" style="background-color: #edf1f6; padding: 10px; font-weight: bold">Thêm mới ghi chú</h5>
		<div class="clearfix"></div>
		<form action="{{ route('store_member_note', [$objMember->id]) }}" method="POST">
			{{ csrf_field() }}
			<div class="row">
				<label for="content" class="col-lg-1">Nội dung</label>
				<div class="col-lg-11 form-group">
					<textarea maxlength="500" class="form-control tinymce" rows="3" id="content" name="content"></textarea>
				</div>
			</div>
			<div class="text-center">
				<div class="btn btn-group">
					<button class="btn btn-primary" type="submit">Lưu</button>
					<button class="btn btn-warning cancel-add" type="button">Huỷ</button>
				</div>
			</div>
		</form>
	</div>
</div>

@section('footer_embed')
	<script type="text/javascript">
		$("#showAreaNoteForm").click(function () {
			$(".form-add-new").show()
		})
		$(".cancel-add").click(function () {
			$(".form-add-new").hide()
		})
	</script>
@endsection
