<?php

use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Providers\UserServiceProvider;
?>
<style>
    body .field-wrap.dropfield:after {
        top: 10px;
    }
    body .field-wrap .datetime-icon {
        top: 10px;
        right: 35px;
    }
</style>
<form class="page-wrap" name="filter" method="get" id="formSearch">
    {{ csrf_field() }}
    <div class="row">
        @if($step != 1 && $step != 2)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Mã số hồ sơ
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input id="file_code" type="text" name="file_code" value="{{ htmlentities(array_get($response_view, 'file_code', '')) }}" maxlength="6">
            </div>
        @endif
        @if($step == 0 || $step == 5 || $step == 6)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Số thẻ hội viên
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input id="idMemberCode" type="text" name="member_code"
                       value="{{ htmlentities(array_get($response_view, 'member_code', '')) }}" maxlength="6">
            </div>
        @endif
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số thẻ HDV
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="idNumber" type="text" name="touristGuideCode"
                   value="{{ htmlentities(array_get($response_view, 'touristGuideCode', '')) }}" maxlength="9">
        </div>
         <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Họ tên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input id="name" type="text" name="fullName" value="{{ htmlentities(array_get($response_view, 'fullName', '')) }}" maxlength="50">
        </div>
        @if($step != 3)
            @if ($step == 6) 
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Ngày in thẻ
            </div>
             <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="from_date_expiration" id="from_date"
                       value="{{ array_get($response_view, 'from_date_expiration', '') }}" placeholder="Từ ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="to_date_expiration" id="to_date"
                       value="{{ array_get($response_view, 'to_date_expiration', '') }}" placeholder="Đến ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
            @else
                @if ($step != 0)
                    <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                        Ngày tạo hồ sơ
                    </div>
                     <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                        <input class="date datetime-input" type="text" name="from_date" id="from_date"
                               value="{{ array_get($response_view, 'from_date', '') }}" placeholder="Từ ngày">
                        <span class="datetime-icon fa fa-calendar"></span>
                    </div>
                    <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                        <input class="date datetime-input" type="text" name="to_date" id="to_date"
                               value="{{ array_get($response_view, 'to_date', '') }}" placeholder="Đến ngày">
                        <span class="datetime-icon fa fa-calendar"></span>
                    </div>
                @endif
            @endif
           
        @endif
        @if($step != 6)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Trạng thái hồ sơ
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select id="status" name="status">
                    @if (Route::currentRouteName() != 'admin_member_rank_view')
                        <option value=""></option>
                    @endif
                    
                    @foreach($arrFile as $key => $file)
                        <option value="{{ $key }}"
                                @if (array_get($response_view, 'status', '') == $key) selected="selected" @endif>{{ $file }}
                        </option>
                    @endforeach
                </select>
            </div>
        @else
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Trạng thái in thẻ
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select  class="is_printed_card" id="is_printed_card" name="is_printed_card">
                    <option value="" selected></option>
					
                    @foreach($arrFile as $key => $val)
                        <option value="{{ $key }}" @if (array_get($response_view, 'is_printed_card', '') == $key) selected="selected" @endif>{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        @endif
        @if($step == 3)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Số chứng từ
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input id="number_payment" type="text" name="number_payment" value="{{ htmlentities(array_get($response_view, 'number_payment', '')) }}" maxlength="20">
            </div>

            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Ngày chứng từ
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="from_date" value="{{ array_get($response_view, 'from_date', '') }}" id="from_date" placeholder="Từ ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12" style="margin-bottom: 10px;">
                <input class="date datetime-input" type="text" name="to_date" value="{{ array_get($response_view, 'to_date', '') }}" id="to_date" placeholder="Đến ngày">
                <span class="datetime-icon fa fa-calendar"></span>
            </div>
        @endif

        @if (auth()->user()->role != UserConstants::CONTENT_MANAGER_SUB_GROUP_USER && empty(auth()->user()->branch_id))
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Văn phòng đại diện
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select class="select_vpdd" id="province_type" name="province_type" onchange="changeProvinceCode()">
                    @if (count(\App\Models\Branches::getBranchesNameByUserProvinceType(auth()->user()->province_type)) != 0)
                        <option value="" selected></option>
                        <option value="{{ auth()->user()->province_type }}" @if (array_get($response_view, 'province_type', '') == auth()->user()->province_type) selected="selected" @endif>{{ \App\Models\Branches::getBranchesNameByUserProvinceType(auth()->user()->province_type)[0] }}</option>
                    @else
                        <option value="" selected></option>
                        @foreach($offices as $value)
                            <option value="{{ $value->id }}" @if (array_get($response_view, 'province_type', '') == $value->id) selected="selected" @endif>{{ $value->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        @endif
        
        @if($step == 4)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12"></div>
            <div class="dropfield col-md-4 col-sm-6 col-xs-12"></div>
        @endif
        @if (auth()->user()->role != UserConstants::CONTENT_MANAGER_SUB_GROUP_USER && empty(auth()->user()->branch_id))
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Nơi đăng ký sinh hoạt
            </div>
            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <select class="province_code" id="province_code" name="province_code">
                    <option value="" selected></option>
                </select>
                <input type="hidden" id="province-code-selected" value="<?php echo array_get($response_view, 'province_code', ''); ?>">
                <input type="hidden" id="filter-by-status" value="<?php echo isset($statusFilter) ? $statusFilter : -1 ?>">
            </div>
        @endif
        @if($step == 0)
            <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                Hạn thẻ hội viên
            </div>
            <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                <input type="text" name="tourist_expiration_year" id="tourist_expiration_year" value="{{ array_get($response_view, 'tourist_expiration_year', '') }}" maxlength="4" onkeypress="return isNumber(event)">
            </div>
            @if (! empty($print_status))
                <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                    Trạng thái in thẻ
                </div>
                <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                    <select class="print_status" id="print_status" name="print_status">
                        <option value=""></option>
                        @foreach($print_status as $key => $file)
                            <option value="{{ $key }}"
                                    @if (array_get($response_view, 'print_status', '') == $key) selected="selected" @endif>{{ $file }}
                            </option>
                        @endforeach
                    </select>
                </div>
            @endif
        @endif
        @if($step == 0)
            @if (! empty($guideLanguage))
                <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
                    Ngôn ngữ hướng dẫn
                </div>
                <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
                    <select class="guideLanguage" id="guideLanguage" name="guideLanguage">
                        <option value=""></option>
                        @foreach($guideLanguage as $key => $file)
                            <option value="{{ $key }}"
                                    @if (array_get($response_view, 'guideLanguage', '') == $key) selected="selected" @endif>{{ $file }}
                            </option>
                        @endforeach
                    </select>
                </div>
            @endif
        @endif
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;">
            <input type="button" class="button btn-primary search-btn-custom" style="width: 100px" name="search" id="search" onclick="submitForm();" value="Tìm kiếm">
        </div>
    </div>
</form>
<!-- Modal form to delete a form -->
<div id="showAlert" class="modal fade" role="dialog">
    <div class="modal-dialog" id="id_delete">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tìm kiếm hồ sơ</h4>
            </div>
            <div class="modal-body">
                @if($step == 3)
                    <h4 class="text-center">Ngày chứng từ không hợp lệ</h4>
                @else
                    <h4 class="text-center">Ngày tạo hồ sơ không hợp lệ</h4>
                @endif
                <br/>

                <div class="modal-footer">                   
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // Enter to submit form
    $('#formSearch').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
            $('#formSearch').submit();
        }
    });
    var isValild = true;
    $('#from_date').on('dp.change', function(e){ 
        fromDate = parseDate($(this).val());
        toDate = parseDate($('#to_date').val());
        if(fromDate > toDate)
        {
           isValild = false;
        } else {
            isValild = true;
        }
    });

    $('#to_date').on('dp.change', function(e){ 
        toDate = parseDate($(this).val());
        fromDate = parseDate($('#from_date').val());
        if(fromDate > toDate)
        {
           isValild = false;
        } else {
            isValild = true;
        }
    });
    function parseDate(dateStr) {
        var parts = dateStr.split("/");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    function submitForm() { 
        if(isValild == false)
        {
           $("#showAlert").modal('show');
           return;
        }
        else {
            $('#formSearch').submit();
        }

    };
    
    $(".select_vpdd").change(function () {


        // $id = $(this).find(":selected").val();


        // if ($id == 1) {
        //     var arrayFromPHP = <?php //echo $hnArea; ?>;


        // } else if ($id == 2) {

        //     var arrayFromPHP = <?php //echo $dnArea; ?>;


        // } else if ($id == 3) {
        //     var arrayFromPHP = <?php //echo $hcmArea ?>;


        // } else if ($id == '') {
        //     var arrayFromPHP = <?php //echo $allArea; ?>;


        // }

        // var data = arrayFromPHP;
        // var $select = $('#province_code');
        // $('#province_code').empty();
        // $select.append('<option value=""></option>');
        // $.each(data, function (i, value) {
        //     var prefix = '';
        //     if (i == 65) {
        //         prefix = "";
        //     } else {
        //         prefix = "Chi hội HDV du lịch ";
        //     }
        //     var option = $('<option value="' + i + '">' + prefix + value + '</option>');
        //     $select.append(option);
        //
        // });
    });

 //    var changeProvinceCode = function(){
 //        var val = jQuery('#province_type').val();
 //        jQuery("#province_code option:selected").prop("selected", false);
	// $.each(jQuery('#province_code option'), function (i, value) {
 // 	    jQuery(this).show();
 //            if(val != ''){
	// 	if(jQuery(this).data('parent') == val){
 //                	jQuery(this).show();
 //            	} else jQuery(this).hide();
	//     }
 //        });
 //    }

    var changeProvinceCode = function(){
        var val = jQuery('#province_type').val();
        var sta = jQuery('#filter-by-status').val();
        changeProvinceType(val, sta);      
    }

    function changeProvinceType(parent_id, province_code_selected = '', status) {
        var province_type_array = [];
        $('#province_type option').each(function() {
            if ($(this).val() != "") {
                province_type_array.push($(this).val()); 
            }
        });
    
        $.ajax({
            type: "POST",
            url: '/searchactivityregistrationplace',
            data: { parent_id: parent_id, status: status, province_type: province_type_array, _token: '{{csrf_token()}}' },
            success: function (data) {
              $("#province_code option").remove();
              var option = "";
              option += "<option value='' selected></option>";
              $.each(data.value, function (i, value) {
                if (value.id == province_code_selected) {
                    option += "<option value="+ value.id +" data-parent="+ value.parent_id +" Selected>"+ value.name +"</option>";
                }
                else {
                    option += "<option value="+ value.id +" data-parent="+ value.parent_id +">"+ value.name +"</option>";
                }
              })
              $("#province_code").append(option);
              // console.log(option);
            },
            error: function (data, textStatus, errorThrown) {
                console.log(data);

            },
        });
    };
    var parent_id = $("#province_type option:selected").val();
    var province_code_selected = $("#province-code-selected").val();
    var filter_by_status = $("#filter-by-status").val();
    // console.log(province_code_selected);
    changeProvinceType(parent_id, province_code_selected, filter_by_status);
</script>
