@extends('layouts.newdesign.public')

@section('title', 'Cấp lại mật khẩu')
@section('content')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cta-title-2">
                            <h1 class="darkblue">Cấp lại mật khẩu</h1>
                        </div>
                        @if(session('successes'))
                            <div id="closeButton" class="alert alert-success">
                                <button type="button" class="close" aria-label="Close" style="width: auto;">
                                    <span aria-hidden="true" style="position: relative; bottom: 5px;">&times;</span>
                                </button>
                                <ul>
                                    @foreach (session('successes') as $success)
                                        <li>{{ $success }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div id="closeButton" class="alert alert-danger">
                                <button type="button" class="close" aria-label="Close" style="width: auto;">
                                    <span aria-hidden="true" style="position: relative; bottom: 5px;">&times;</span>
                                </button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div>
                            <form class="form-horizontal reset-form" method="POST" action="{{ route('user_forgot_password')
                            }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-2 control-label">Địa chỉ email</label>

                                    <div class="col-md-6">
                                        <input placeholder="Địa chỉ email" id="email" type="email" class="form-control"
                                               name="email" value="{{
                                        old('email') }}">

                                        @if ($errors->has('email'))
                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn-primary" style="width: auto">Gửi lại mật
                                            khẩu</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection