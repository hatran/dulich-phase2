<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div id="form-login">
    <div>
        <img class="logo" src="{{ asset('images/logo.png') }}">
    </div>
    <div class="form">
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="lb-input">Tên đăng nhập</label>
                <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" placeholder="Tên đăng nhập" autofocus>
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="lb-input">Mật khẩu</label>
                <input id="password" type="password" class="form-control" name="password" placeholder="*****">

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="checkbox">
                <label id="lb-remember"><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id="cb-remember" >Ghi nhớ mật khẩu </label>
                <a href="{{ route('password.request') }}" id="forget">Quên mật khẩu?</a>
            </div>

            <div style="margin-bottom: 10px" class="g-recaptcha" data-sitekey="6LeXw0AUAAAAAILh4xy9-asyjzF8n8CRPdmw00f0"></div>
            @if ($errors->has('g-recaptcha-response'))
                <span class="help-block" style="color: #a94442;">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
            @endif

            <div>
                <button type="submit" class="btn btn-primary btn-dang-nhap">ĐĂNG NHẬP</button>
            </div>

        </form>
    </div>

</div>
</body>
</html>
