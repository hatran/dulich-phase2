<div id="footer">
    <div class="page-wrap">
        <div class="foot-nav">
            <ul>
                <li class="{{ (\Request::is('/')) ? 'active' : '' }}">
                    <a href="{{ url('/') }}">Trang chủ</a>
                </li>
                <li class="{{ (\Request::is('gioithieu')) ? 'active' : '' }}">
                    <a href="{{ url('/gioithieu') }}">Giới thiệu</a>
                </li>
                <li class="{{ (\Request::is('chihoi')) ? 'active' : '' }}">
                    <a href="{{ url('/chihoi') }}">Chi hội</a>
                </li>
                <li class="{{ (\Request::is('clb')) ? 'active' : '' }}">
                    <a href="{{ url('/clb') }}">CLB thuộc hội</a>
                </li>
                <li class="{{ (\Request::is('news')) ? 'active' : '' }}">
                    <a href="{{ url('/news') }}">Tin tức và sự kiện</a>
                </li>
                <li class="{{ (\Request::is('hoivien')) ? 'active' : '' }}">
                    <a href="{{ url('/hoivien') }}">Hội viên</a>
                </li>
            </ul>
        </div>
        <div class="socials">
            <a href="#" target="_blank" class="fa fa-facebook"></a>
            <a href="#" target="_blank" class="fa fa-google-plus"></a>
        </div>
    </div>
    <div class="foot-text">
        <div class="page-wrap">
            <div class="copyright">
                &copy Hội Hướng dẫn viên Du lịch Việt Nam</br>
                @if(!empty($headOffice))
                    Địa chỉ: {{ $headOffice['address'] }}</br>
                    Điên thoại: 024.37835120, 37835122</br>
                    Email: hanoi@hoihuongdanvien.vn
                @else
                    Địa chỉ: Tầng 5, số 211 Giảng Võ, Phường Cát Linh, Quận Đống Đa, Hà Nội</br>
                    Điên thoại: 024.37835120, 37835122</br>
                    Email: hanoi@hoihuongdanvien.vn
                @endif
            </div>
            {{--<div class="shortcuts">
                <a href="#">Terms of Service</a>
                <a href="#">Privacy Policy</a>
            </div>--}}
        </div>
    </div>
</div>
