@if(session('successes'))
    <div id="closeButton" class="alert alert-success">
        <button type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            @foreach (session('successes') as $success)
                <li>{{ $success }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <button id="closeButton" type="button" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<style>
    .alert-success {
        background-color: #dff0d8 !important;
        color: #3c763d !important;
        border-color: #d0e9c6;
    }
</style>
