<?php
use App\Constants\UserConstants;
use App\Providers\UserServiceProvider;
?>
<header id="header">
    <div id="header-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a class="" href="{{ url('/') }}" style="line-height: 1.6em">
                        <img src="{{ url('images/logo.png') }}" alt="" class="img-responsive logo" style="height: 100%;">
                        <span class="font-name" style="padding-left: 13px;">Hội Hướng dẫn viên Du lịch Việt Nam</span>
                    </a>
                    @if (!Auth::guest() || ! \App\Providers\UserServiceProvider::isNormalUser())
                    <div class="header-btn" style="z-index: 999">
                        <a class="hover-dark darkblue" href="{{ route('profile') }}">Xem thông tin cá nhân</a>
                        |
                        <a class="hover-dark darkblue" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng xuất</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                    @endif
                    <ul id="main-menu" class="main-menu" style="float: right;">
                        <li><a href="{{ url('/') }}">Trang chủ</a></li>
                        <li>
                            <a href="#" class="sf-with-ul">Giới thiệu</a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/vechungtoi') }}">Về Chúng Tôi</a></li>
                                <li><a href="{{ url('/tamnhinvasumenh') }}">Tầm Nhìn & Sứ Mệnh</a></li>
                                <li><a href="{{ url('/quychehoatdong') }}">Quy Chế Hoạt Động</a></li>
                                <li><a href="{{ url('/quydinh') }}">Quy Định</a></li>
                                <li><a href="{{ url('/huongdandangky') }}">Hướng Dẫn Đăng Ký</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ url('/hoivien') }}">Hội viên</a></li>
                        <li>
                            <a href="#" class="sf-with-ul">Cơ cấu tổ chức</a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/chihoi') }}">Chi Hội</a></li>
                                <li><a href="{{ url('/clb') }}">CLB Thuộc Hội</a></li>
                                <li><a href="{{ url('/banchaphanh') }}">Ban Chấp Hành</a></li>
                                <li><a href="{{ url('/banthuongtruc') }}">Ban Thường Trực</a></li>
                                <li><a href="{{ url('/banchuyenmon') }}">Ban Chuyên Môn</a></li>
                                <li><a href="{{ url('/bancovan') }}">Ban Cố Vấn</a></li>
                                <li><a href="{{ url('/vanphongdaidien') }}">Văn Phòng Đại Diện</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ url('/tintucvasukienhoi') }}" class="">Tin tức sự kiện</a>
                            <!-- <ul class="sub-menu">
                                <li><a href="{{ url('/tintucvasukienhoi') }}">Tin Tức & Sự Kiện Hội</a></li>
                                <li><a href="{{ url('/tintucvasukienchihoi') }}">Tin Tức & Sự Kiện Chi Hội</a></li>
                                <li><a href="{{ url('/tintucvasukienclbthuochoi') }}">Tin Tức & Sự Kiện CLB Thuộc Hội</a></li>
                            </ul> -->
                        </li>
                        <li>
                            <a href="#" class="sf-with-ul">Hỗ trợ hội viên</a>
                            <ul class="sub-menu">
                                <li><a href="{{URL::route('front_info_list')}}">Thông Tin</a></li>
                                <li><a href="#">Đào Tạo</a></li>
                                <li><a href="#">Sàn Giao Dịch Việc Làm</a></li>
                                <li><a href="#">Xếp hạng HDV</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="mobile-menu-btn hamburger hamburger--slider">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                    </div>
                    <div id="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
</header>
