<footer id="footer-mini" class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copyrights-message">2018 © <span>Hội Hướng dẫn viên Du lịch Việt Nam</span>
                    <br>
                    @if(!empty($headOffice))
                        Địa chỉ: {{ $headOffice['address'] }}</br>
                        Điên thoại: {{ $headOffice['phone'] }}</br>
                        Email: {{ $headOffice['email'] }}
                    @else
                        Địa chỉ: Tầng 5, số 211 Giảng Võ, Phường Cát Linh, Quận Đống Đa, Hà Nội</br>
                        Điên thoại: 024.37835120, 37835122</br>
                        Email: hanoi@hoihuongdanvien.vn
                    @endif
                </div>
            </div>
        </div>
    </div>
</footer>