<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{ asset('images/favicon.ico') }}" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/earlyaccess/droidarabickufi.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=vietnamese" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.4.7.0.min.css') }}" />

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.3.3.7.min.css') }}">

    <!-- owl-carousel -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.1.3.3.min.css') }}">

    <!-- Magnific Popup -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/magnific-popup.1.1.0.min.css') }}">

    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.3.5.2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/travel/css/owl.carousel.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/travel/css/load-awesome.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/travel/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/travel/css/skin-default.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/css/arcana.css') }}" rel="stylesheet">
    {{--@endif--}}
    @yield('header_embed')
</head>
<body>
    <div id="full-container">
        @include('layouts.newdesign.header')

        @yield('content')

        @include('layouts.newdesign.footer')
        {{--@if (! \Request::is('/'))--}}
            {{--@include('layouts.footer')--}}
        {{--@endif--}}

        @yield('footer_embed')
    </div>
    <a href="#" class="scroll-top"><i class="fa fa-angle-up"></i></a>
    <script src="{{ asset('theme/travel/js/jquery.js') }}"></script>
    <script src="{{ asset('js/bootstrap.3.3.7.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/jRespond.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/jquery.fitvids.js') }}"></script>
    <!--<script src="./js/wow.min.js"></script>-->
    <script src="{{ asset('theme/travel/js/jquery.stellar.js') }}"></script>
    <script src="{{ asset('theme/travel/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/hoverIntent.js') }}"></script>
    <script src="{{ asset('theme/travel/js/simple-scrollbar.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/superfish.js') }}"></script>
    <script src="{{ asset('theme/travel/js/scrollIt.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/jquery.waitforimages.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('theme/travel/js/jquery-contained-sticky-scroll.js') }}"></script>

    <script src="{{ asset('theme/travel/js/app.js') }}"></script>
    <script src="{{ asset('js/branches.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <div id="website-loading">
        <div class="loader">
            <div class="la-ball-scale la-2x">
                <div></div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('.scrollbanner').containedStickyScroll({
                duration: 300,
                unstick: true,
                closeChar: '.'
            });

            jQuery('.date').datetimepicker({
                locale: 'vi',
                viewMode: 'days',
                format: 'DD/MM/Y',
            });
        });

        jQuery(document).ready(function() {
            jQuery('#closeButton').on('click', function(e) {
                jQuery('.alert').remove();
            });
        });

    </script>
</body>
</html>
