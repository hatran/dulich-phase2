<?php if (!empty($bannerName['profile_image'])) { ?>
<div class="parallax-section cta-title-1" data-scroll-index="1" data-parallax-bg-img="{{ !empty($bannerName['profile_image']) ? $bannerName['profile_image'] : '' }}" data-stellar-background-ratio="0.2">
    <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
    <div class="section-inner">

    </div>
</div>
<?php } ?>
<style>
    @media (max-width: 992px) {
        #header {
            height: 90px;
        }
    }
</style>
