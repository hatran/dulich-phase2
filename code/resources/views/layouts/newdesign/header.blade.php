<?php
use App\Constants\UserConstants;
use App\Providers\UserServiceProvider;
use App\Libs\Helpers\Utils;
use App\Providers\MemberServiceProvider;
use App\Models\MemberRank;
?>
<style>
    .dropdown-traveler {
        position: relative;
        display: inline-block;
    }

    .dropdown-content-traveler {
        display: none;
        position: absolute;
        background-color: #fff;
        min-width: 220px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        padding: 12px 16px;
        z-index: 1;
        color: #044d9f !important;
        font-weight: 600 !important;
        text-transform: uppercase !important;
        text-align: center;
        right: -110px;
    }

    .dropdown-content-traveler p {
        float: left;
    }

    .dropdown-content-traveler p a {
        color: #0355b3;
    }

    .dropdown-content-traveler p a:hover {
        border-bottom: unset;
        color: #ffc527 !important;
    }

    .dropdown-traveler:hover .dropdown-content-traveler {
        display: block;
        color: #222;
    }

    #color-traveler {
        color: #0355b3;
    }
    
    .box {
        margin: 0;
        border: 1px solid #d2dfec;
        border-radius: 5px;
        padding: 5px;
        background: #fff;
    }

    .main-menu > li > a {
        padding: 0 8px;
    }
</style>
<header id="header">
    <div id="header-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a class="" href="{{ url('/') }}">
                        <img src="{{ url('images/logo.png') }}" alt="" class="img-responsive logo">
                        <span class="font-name">Hội Hướng dẫn viên Du lịch Việt Nam</span>
                    </a>
                    @if (!Auth::guest() || ! \App\Providers\UserServiceProvider::isNormalUser())
                    <div class="header-btn" style="z-index: 999">
                        @if (Auth::user()->is_traveler_company == 1)
                            <div class="dropdown-traveler">
                                <span id="color-traveler">{{ Auth::user()->fullname }}</span>
                                <div class="dropdown-content-traveler">             
                                    <p>
                                        <a href="{{ route('profile') }}">Thông tin người dùng</a>
                                    </p>
                                    <p>                                   
                                        <a href="{{ url('company/evaluate') }}">Đánh giá HDV</a>
                                    </p>
                                    <p>                                  
                                        <a href="{{ route('company_evaluate_list_frontend') }}">Danh sách đánh giá</a>
                                    </p>
                                </div>
                            </div>
                        @else 
                            <?php $memberId = Auth::user()->memberId; $objMember = MemberServiceProvider::getMemberById($memberId); ?>
                            @if ($memberId != Null && $objMember != Null)
                                <div class="dropdown-traveler">
                                    <span id="color-traveler">{{ Auth::user()->fullname }}</span>
                                    <div class="dropdown-content-traveler" style="right: -150px;">             
                                        <p>
                                            <a href="{{ route('profile') }}">Thông tin hồ sơ </a>
                                        </p>
                                        <p>
                                            <a href="{{ route('user_change_password') }}">Thay đổi mật khẩu</a>
                                        </p>
                                        @if (!empty($objMember) && $objMember->rank_status >= 1 && !empty(MemberRank::getInfoRankByMemberId($objMember->id)))
                                            <p>
                                                <a href="{{ route('front_end_rank_info') }}">Thông tin xếp hạng HDV </a>
                                            </p>
                                            <p>                                  
                                                <a href="{{ route('company_evaluate_list_frontend') }}">Danh sách đánh giá</a>
                                            </p>
                                            <p>                                   
                                                <a href="{{ route('front_end_joinquiz') }}">Thi kiến thức xếp hạng HDV</a>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            @else
                                <a class="hover-dark darkblue" href="{{ route('profile') }}">Xem thông tin cá nhân</a>
                            @endif
                        @endif
                        |
                        <a class="hover-dark darkblue" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng xuất</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                    @endif
                    <ul id="main-menu" class="main-menu pull-right">
                        <li class="{{ Utils::highlightMenu(['vechungtoi', 'tamnhinvasumenh', 'quychehoatdong', 'quydinh', 'huongdandangky']) ? 'frontend-active' : '' }}">
                            <a href="#" class="sf-with-ul">Giới thiệu</a>
                            <ul class="sub-menu">
                                <li class="{{ Utils::highlightMenu(['vechungtoi']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/vechungtoi') }}">Về Chúng Tôi</a></li>
                                <li class="{{ Utils::highlightMenu(['tamnhinvasumenh']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/tamnhinvasumenh') }}">Tầm Nhìn & Sứ Mệnh</a></li>
                                <li class="{{ Utils::highlightMenu(['quychehoatdong']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/quychehoatdong') }}">Quy Chế Hoạt Động</a></li>
                                <li class="{{ Utils::highlightMenu(['quydinh']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/quydinh') }}">Quy Định</a></li>
                                <li class="{{ Utils::highlightMenu(['huongdandangky']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/huongdandangky') }}">Hướng Dẫn Đăng Ký</a></li>
                            </ul>
                        </li>
                        <li class="{{ Utils::highlightMenu(['hoivien']) ? 'frontend-active' : '' }}"><a href="{{ url('/hoivien') }}">Hội viên</a></li>
                        <li class="{{ Utils::highlightMenu(['survey']) ? 'frontend-active' : '' }}"><a href="{{ URL::route('front_end_survey') }}">Xếp hạng HDV</a></li>
                        <li class="{{ Utils::highlightMenu(['chihoi', 'clb', 'banchaphanh', 'banthuongtruc', 'banchuyenmon', 'bancovan', 'vanphongdaidien']) ? 'frontend-active' : '' }}">
                            <a href="#" class="sf-with-ul">Cơ cấu tổ chức</a>
                            <ul class="sub-menu">
                                <li class="{{ Utils::highlightMenu(['chihoi']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/chihoi') }}">Chi Hội</a></li>
                                <li class="{{ Utils::highlightMenu(['clb']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/clb') }}">CLB Thuộc Hội</a></li>
                                <li class="{{ Utils::highlightMenu(['banchaphanh']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/banchaphanh') }}">Ban Chấp Hành</a></li>
                                <li class="{{ Utils::highlightMenu(['banthuongtruc']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/banthuongtruc') }}">Ban Thường Trực</a></li>
                                <li class="{{ Utils::highlightMenu(['banchuyenmon']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/banchuyenmon') }}">Ban Chuyên Môn</a></li>
                                <li class="{{ Utils::highlightMenu(['bancovan']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/bancovan') }}">Ban Cố Vấn</a></li>
                                <li class="{{ Utils::highlightMenu(['vanphongdaidien']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/vanphongdaidien') }}">Văn Phòng Đại Diện</a></li>
                            </ul>
                        </li>
                        <li class="{{ Utils::highlightMenu(['tintucvasukienhoi']) ? 'frontend-active' : '' }}">
                            <a href="#" class="sf-with-ul">Tin tức sự kiện</a>
                            <ul class="sub-menu">
                                <li class="{{ Utils::highlightMenu(['tintucvasukienhoi']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/tintucvasukienhoi') }}">Tin Tức & Sự Kiện Hội</a></li>
                                <li class="{{ Utils::highlightMenu(['tintucvasukienchihoi']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/tintucvasukienchihoi/1') }}">Tin Tức & Sự Kiện Chi Hội</a></li>
                                <!-- <li class="{{ Utils::highlightMenu(['tintucvasukienclbthuochoi']) ? 'frontend-sub-active' : '' }}"><a href="{{ url('/tintucvasukienclbthuochoi') }}">Tin Tức & Sự Kiện CLB Thuộc Hội</a></li> -->
                            </ul>
                        </li>
                        <li class="{{ Utils::highlightMenu(['thong-tin', 'training', 'worktrading', 'survey']) ? 'frontend-active' : '' }}">
                            <a href="#" class="sf-with-ul">Hỗ trợ hội viên</a>
                            <ul class="sub-menu">
                                <li class="{{ Utils::highlightMenu(['thong-tin']) ? 'frontend-sub-active' : '' }}"><a href="{{ url("thong-tin") }}">Thông Tin</a></li>
                                <li class="{{ Utils::highlightMenu(['training']) ? 'frontend-sub-active' : '' }}"><a href="{{URL::route('front_end_training')}}">Đào Tạo</a></li>
                                <li class="{{ Utils::highlightMenu(['worktrading']) ? 'frontend-sub-active' : '' }}"><a href="{{URL::route('front_end_work_trading')}}">Sàn Giao Dịch Việc Làm</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="mobile-menu-btn hamburger hamburger--slider">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                    </div>
                    <div id="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
</header>
