@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Xin Chào!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
@php
    $color = "";
    switch ($level) {
        case 'success':
            $color = "green";
            break;
        case 'error':
            $color = "red";
            break;
        default:
            $color = "blue";
    }
@endphp
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
Regards,<br>{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
Nếu nút "{{ $actionText }}" không hoạt động, vui lòng sao chép link dưới đây dán và truy cập tại trình duyệt của bạn:
[{{
$actionUrl
 }}]
({{
$actionUrl }})
@endcomponent
@endisset
@endcomponent
