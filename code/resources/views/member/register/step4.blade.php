@extends('member.register.master')
@section('register_content')
    <div id="step4" class="show block-wrap">
        <div class="center block">
            <div class="checkbox-list choices">
                <div class="choice" id="mustAccepted">
                    <input id="acceptPolicy" type="checkbox" name="acceptPolicy">
                    <label for="acceptPolicy" id="agreePolicy"></label>Tôi đã đọc kỹ và đồng ý với <a class="download-file" target="_blank" href="{{ asset('docs/Qui che Hoi HDV DLVN .pdf') }}">Quy chế hoạt động</a> và <a class="download-file" href="{{ asset('docs/Quy dinh Hoi huong dan vien  DLVN.pdf') }}" target="_blank">Quy định</a>  của Hội hướng dẫn viên du lịch Việt Nam
                    <input type="hidden" name="memberId" value="{{$memberId}}">
                </div>
            </div>
        </div>
        <div class="center form-nav">
            <span class="prev-step btn-step" data-step="3" data-current="4">Quay lại</span>
            <input id="submit_button_register" type="submit" class="next-step btn-step" value="Đăng ký hội viên" style="pointer-events:none;">
        </div>
    </div>
@endsection
