@extends('member.register.master')
@section('register_content')
    <div id="step4" class="show block-wrap">
        <div class="center block">
            <div class="checkbox-list choices">
                <div class="choice">
                    <p>Đăng ký gia nhập thành công !!</p>
                    <p>Cảm ơn ông/bà đã đăng ký gia nhập Hội Hướng dẫn viên Du lịch Việt Nam</p>
                    <p>Thời gian xử lý hồ sơ đăng ký gia nhập Hội: <span style="font-size: 18px; font-weight: bold;">05</span> ngày làm việc</p>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
