@extends('member.register.master')
@section('body_end')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.min.js"></script>
    <script id="workHistory-template" type="text/x-handlebars-template">
        <div class="row">
            <div class="col-md-5">
                <div class="form-order-item"></div>
                <div class="form-group">
                    <label for="inputWorkElementName@{{workHistoryId}}">Tên doanh nghiệp đã và đang công tác</label>
                    <input type="text" id="inputWorkElementName@{{workHistoryId}}" name="workElementNames[@{{workHistoryId}}]"
                           class="form-control"
                           placeholder="Công ty A">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="inputWorkTypeOfContract@{{workHistoryId}}">Hình thức lao động</label>
                    <select id="inputWorkTypeOfContract@{{workHistoryId}}" name="workTypeOfContracts[@{{workHistoryId}}]"
                            class="form-control">
                        <option value=""></option>
                        <option value="1">Nhân viên chính thức</option>
                        <option value="2">Cộng tác viên</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="inputWorkFromDate@{{workHistoryId}}">Thời gian bắt đầu</label>
                    <input type="text" id="inputWorkFromDate@{{workHistoryId}}" name="workFromDates[@{{workHistoryId}}]" class="form-control"
                           placeholder="Từ năm">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="inputWorkToDate@{{workHistoryId}}">Thời gian kết thúc</label>
                    <input type="text" id="inputWorkToDate@{{workHistoryId}}" name="workToDates[@{{workHistoryId}}]" class="form-control"
                           placeholder="Đến năm">
                </div>
            </div>
        </div>
    </script>
    <script id="education-template" type="text/x-handlebars-template">
        <tr id="education-@{{branchId}}-@{{degreeId}}">
            <td class="simple-item action-1">
                <a class="btn-remove-education text-danger" href="#"
                   data-target="#education-@{{branchId}}-@{{degreeId}}">
                    <i class="fa fa-close"></i>
                </a>
            </td>
            <td class="simple-item"><span class="simple-order"></span>. @{{branchName}} - @{{degreeName}}</td>
            <td class="simple-item">
                <input type="hidden" name="educationBranches[@{{ educationId }}]" value="">
                <input type="hidden" name="educationDegrees[@{{ educationId }}]" value="">
                <div class="simple-upload">
                    <input type="file" id="inputEducationFile1"
                           class="simple-upload-file hidden"
                           name="educationFiles[@{{ educationId }}]" value="">
                    <span class="simple-upload-view hidden" href="#">
                        <span class="simple-upload-text"></span>
                        <a class="simple-upload-remove text-danger" href="#">
                            <i class="fa fa-close"></i>
                        </a>
                    </span>
                    <a class="simple-upload-action" href="#">
                        <i class="fa fa-upload"></i>
                        Tải bản sao có chứng thực văn bằng
                    </a>
                </div>
            </td>
        </tr>
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.simple-upload-action', function (e) {
                e.preventDefault();
                $(this).closest('.simple-upload').find('.simple-upload-file').trigger('click');
            });
            $(document).on('change', '.simple-upload-file', function () {
                var $simpleUpload = $(this).closest('.simple-upload');
                $simpleUpload.find('.simple-upload-view').removeClass('hidden');
                $simpleUpload.find('.simple-upload-action').addClass('hidden');
                $simpleUpload.find('.simple-upload-text').text($simpleUpload.find('.simple-upload-file').get(0).files[0].name);
            });
            $(document).on('click', '.simple-upload-remove', function (e) {
                e.preventDefault();
                var $simpleUpload = $(this).closest('.simple-upload');
                $simpleUpload.find('.simple-upload-file').val('');
                $simpleUpload.find('.simple-upload-view').addClass('hidden');
                $simpleUpload.find('.simple-upload-action').removeClass('hidden');
            });
            $(document).on('click', '.group-upload-action', function (e) {
                e.preventDefault();
                $(this).closest('.group-upload').find('.group-upload-file').trigger('click');
            });
            $(document).on('change', '.group-upload-file', function () {
                $($(this).attr('data-target')).append();
            });
            $(document).on('click', '.group-upload-remove', function (e) {
                e.preventDefault();
                $(this).closest('.group-upload-view').remove();
            });

            var $inputEducationBranch = $('#inputEducationBranch'),
                $inputEducationDegree = $('#inputEducationDegree'),
                $educationHolder = $('#educationHolder'),
                $educationHolderBody = $educationHolder.find('tbody'),
                educationTemplate = Handlebars.compile($('#education-template').html()),
                educationId = 0,
                educationCount = 1;
            $(document).on('click', '.btn-add-education', function (e) {
                e.preventDefault();
                var branchId = $inputEducationBranch.val(),
                    degreeId = $inputEducationDegree.val();
                if (branchId !== '' && degreeId !== ''
                    && $('#education-' + branchId + '-' + degreeId).length <= 0) {
                    ++educationId;
                    ++educationCount;
                    $educationHolderBody.append(educationTemplate({
                        educationId: educationId,
                        branchId: branchId,
                        degreeId: degreeId,
                        branchName: $inputEducationBranch.find('[value="' + branchId + '"]').html(),
                        degreeName: $inputEducationDegree.find('[value="' + degreeId + '"]').html()
                    }));
                    var order = 0;
                    $educationHolderBody.find('.simple-order').each(function(){
                        $(this).text(++order);
                    });
                    if (educationCount > 0) {
                        $educationHolder.removeClass('hidden');
                    }
                }
            });
            $(document).on('click', '.btn-remove-education', function (e) {
                e.preventDefault();
                --educationCount;
                $($(this).attr('data-target')).remove();
                if (educationCount === 0) {
                    $educationHolder.addClass('hidden');
                }
            });

            var $inputLanguage = $('#inputLanguage'),
                $inputLanguageLevel = $('#inputLanguageLevel'),
                $languageSkillHolder = $('#languageSkillHolder'),
                languageSkillId = 0,
                languageSkillCount = 1;
            $(document).on('click', '.btn-add-languageSkill', function (e) {
                e.preventDefault();
                var language = $inputLanguage.val(),
                    languageLevel = $inputLanguageLevel.val();
                if (language !== '' && languageLevel !== ''
                    && $('#languageSkill-' + language + '-' + languageLevel).length <= 0) {
                    ++languageSkillId;
                    ++languageSkillCount;
                    console.log('add languageSkill');
                    if (languageSkillCount > 0) {
                        $languageSkillHolder.removeClass('hidden');
                    }
                }
            });
            $(document).on('click', '.btn-remove-languageSkill', function (e) {
                e.preventDefault();
                --languageSkillCount;
                $($(this).attr('data-target')).remove();
                if (languageSkillCount === 0) {
                    $languageSkillHolder.addClass('hidden');
                }
            });

            var $inputGroupSize = $('#inputGroupSize'),
                $inputTypeGuide = $('#inputTypeGuide'),
                $elementHolder = $('#elementHolder'),
                elementId = 0,
                elementCount = 1;
            $(document).on('click', '.btn-add-element', function (e) {
                e.preventDefault();
                var groupSize = $inputGroupSize.val(),
                    typeGuide = $inputTypeGuide.val();
                if (groupSize !== '' && typeGuide !== ''
                    && $('#element-' + groupSize + '-' + typeGuide).length <= 0) {
                    ++elementId;
                    ++elementCount;
                    console.log('add element');
                    if (elementCount > 0) {
                        $elementHolder.removeClass('hidden');
                    }
                }
            });
            $(document).on('click', '.btn-remove-element', function (e) {
                e.preventDefault();
                --elementCount;
                $($(this).attr('data-target')).remove();
                if (elementCount === 0) {
                    $elementHolder.addClass('hidden');
                }
            });

            var workHistoryTemplate = Handlebars.compile($('#workHistory-template').html()),
                $workHistoryHolder = $('#workHistoryHolder'),
                workHistoryId = 0;
            $(document).on('click', '.btn-add-work', function (e) {
                e.preventDefault();
                ++workHistoryId;
                $workHistoryHolder.append(workHistoryTemplate({
                    workHistoryId: workHistoryId
                }));
                var order = 0;
                $workHistoryHolder.find('.form-order-item').each(function () {
                    $(this).text(++order);
                });
            });
        })
    </script>
@endsection
@section('register_content')
    <form method="post">
        {{ csrf_field() }}
        <input type="hidden" name="step" value="1">
        <div class="section">
            <div class="section-title">
                <h4>1. Thông tin chung</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="inputFullName" class="required">Họ và tên</label>
                                    <input type="text" id="inputFullName" name="fullName" class="form-control"
                                           placeholder="Nguyễn Văn A" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputGender" class="required">Giới tính</label>
                                    <select id="inputGender" name="gender" class="form-control" required>
                                        <option value=""></option>
                                        <option value="1">Nam</option>
                                        <option value="2">Nữ</option>
                                        <option value="3">Không xác định</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-group-date-separator-md">
                                    <label for="inputBirthDate" class="required">Ngày sinh</label>
                                    <select id="inputBirthDate" name="birthDate" class="form-control" required>
                                        <option value=""></option>
                                        @for($i = 1; $i <= 31; ++$i)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-group-date-separator-md">
                                    <label for="inputBirthMonth" class="required">Tháng sinh</label>
                                    <select id="inputBirthMonth" name="birthMonth" class="form-control" required>
                                        <option value=""></option>
                                        @for($i = 1; $i <= 12; ++$i)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputBirthYear" class="required">Năm sinh</label>
                                    <select id="inputBirthYear" name="birthYear" class="form-control" required>
                                        <option value=""></option>
                                        @for($i = date('Y'), $lower = $i - 150; $i >= $lower; --$i)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="simple-upload">
                                <input type="file" id="inputAvatar" class="simple-upload-file hidden" name="avatar"
                                       value="">
                                <span class="simple-upload-view hidden" href="#">
                                    <span class="simple-upload-text"></span>
                                    <a class="simple-upload-remove text-danger" href="#">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </span>
                                <a class="simple-upload-action" href="#">
                                    <i class="fa fa-upload"></i>
                                    Tải hình ảnh đại diện của bạn lên
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputTouristGuideCode" class="required">Số thẻ HDV du lịch</label>
                            <input type="text" id="inputTouristGuideCode" name="touristGuideCode" class="form-control"
                                   placeholder="123456789" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputExpirationDate" class="required">Ngày hết hạn</label>
                            <input type="text" id="inputExpirationDate" name="expirationDate" class="form-control"
                                   placeholder="XX/MM/YYYY" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputCmtCccd" class="required">Số CMT/CCCD</label>
                            <input type="text" id="inputCmtCccd" name="cmtCccd" class="form-control"
                                   placeholder="123456789"
                                   required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputDateIssued" class="required">Cấp ngày</label>
                            <input type="text" id="inputDateIssued" name="dateIssued" class="form-control"
                                   placeholder="XX/MM/YYYY"
                                   required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputIssuedBy">Tại</label>
                            <input type="text" id="inputIssuedBy" name="issuedBy" class="form-control"
                                   placeholder="Nơi cấp">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputPermanentAddress" class="required">Địa chỉ thường trú</label>
                            <textarea id="inputPermanentAddress" name="permanentAddress" class="form-control" cols="10"
                                      rows="2"
                                      placeholder="Thông tin địa chỉ Phường, Quận, Thành phố" required></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputAddress" class="required">Địa chỉ liên hệ</label>
                            <textarea id="inputAddress" name="address" class="form-control" cols="10" rows="2"
                                      placeholder="Thông tin địa chỉ Phường, Quận, Thành phố" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputFirstMobile" class="required">Điện thoại di động 1</label>
                            <input type="text" id="inputFirstMobile" name="firstMobile" class="form-control"
                                   placeholder="09x xxxx xxx" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputSecondMobile">Điện thoại di động 2</label>
                            <input type="text" id="inputSecondMobile" name="secondMobile" class="form-control"
                                   placeholder="09x xxxx xxx">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputFirstEmail" class="required">Email 1</label>
                            <input type="text" id="inputFirstEmail" name="firstEmail" class="form-control"
                                   placeholder="...@example.com" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputSecondEmail">Email 2</label>
                            <input type="text" id="inputSecondEmail" name="secondEmail" class="form-control"
                                   placeholder="...@example.com">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="typeOfTourGuide" value="1" checked>
                                    HDV du lịch quốc tế
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="typeOfTourGuide" value="2">
                                    HDV du lịch nội địa
                                </label>
                            </div>
                            <div class="radio-inline">
                                <label>
                                    <input type="radio" name="typeOfTourGuide" value="3">
                                    HDV du lịch tại điểm
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>2. Thông tin học vấn</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputEducationBranch">Chuyên ngành</label>
                            <select id="inputEducationBranch" name="educationBranch" class="form-control">
                                <option value=""></option>
                                @foreach($educationBranches as $educationBranch)
                                    <option value="{{$educationBranch->id}}">{{$educationBranch->branchName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputEducationDegree">Trình độ</label>
                            <select id="inputEducationDegree" name="educationDegree" class="form-control">
                                <option value=""></option>
                                @foreach($educationDegrees as $educationDegree)
                                    <option value="{{$educationDegree->id}}">{{$educationDegree->degree}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="hidden-sm hidden-xs">&nbsp;</label>
                            <div>
                                <button class="btn btn-primary btn-add-education" type="button">Thêm</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="educationHolder" class="form-group">
                            <table class="simple-list">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>3. Thông tin về nghiệp vụ hướng dẫn</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            @foreach($majors as $major)
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="majors[]" value="{{$major->id}}">
                                        {{$major->majorName}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <table class="simple-list">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="simple-upload">
                                            <input type="file" id="inputMajorSkillFile1"
                                                   class="simple-upload-file hidden"
                                                   name="majorSkillFiles[1]" value="">
                                            <span class="simple-upload-view hidden" href="#">
                                                <span class="simple-upload-text"></span>
                                                <a class="simple-upload-remove text-danger" href="#">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </span>
                                            <a class="simple-upload-action" href="#">
                                                <i class="fa fa-upload"></i>
                                                Tải bản sao có chứng thực văn bằng hoặc giấy chứng nhận
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>4. Thông tin về trình độ ngoại ngữ</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <span>Đối với hướng dẫn viên quốc tế</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputLanguage">Ngoại ngữ</label>
                            <select id="inputLanguage" name="language" class="form-control">
                                <option value=""></option>
                                @foreach($languages as $language)
                                    <option value="{{$language->id}}">{{$language->languageName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputLanguageLevel">Trình độ</label>
                            <select id="inputLanguageLevel" name="languageLevel" class="form-control">
                                <option value=""></option>
                                @foreach($languageLevels as $languageLevel)
                                    <option value="{{$languageLevel->id}}">{{$languageLevel->levelName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="hidden-sm hidden-xs">&nbsp;</label>
                            <div>
                                <button class="btn btn-primary btn-add-languageSkill" type="button">Thêm</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="languageSkillHolder" class="form-group">
                            <table class="simple-list">
                                <tbody>
                                <tr id="languageSkill-1-1">
                                    <td class="simple-item action-1">
                                        <a class="btn-remove-languageSkill text-danger" href="#"
                                           data-target="#languageSkill-1-1">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                    <td class="simple-item">1. Du lịch - Trung cấp</td>
                                    <td class="simple-item">
                                        <input type="hidden" name="languages[1]" value="">
                                        <input type="hidden" name="languageLevels[1]" value="">
                                        <div class="simple-upload">
                                            <input type="file" id="inputLanguageSkillFiles1"
                                                   class="simple-upload-file hidden"
                                                   name="languageSkillFiles[1]" value="">
                                            <span class="simple-upload-view hidden" href="#">
                                                <span class="simple-upload-text"></span>
                                                <a class="simple-upload-remove text-danger" href="#">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </span>
                                            <a class="simple-upload-action" href="#">
                                                <i class="fa fa-upload"></i>
                                                Tải bản sao có chứng thực văn bằng
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>5. Thông tin về quá trình hành nghề hướng dẫn</h4>
            </div>
            <div class="section-body">
                <div class="form-order">
                    <div id="workHistoryHolder">

                    </div>
                    <div class="form-group">
                        <a href="#" class="btn-add-work">
                            Thêm thông tin hành nghề
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>6. Sở trường hướng dẫn</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputGroupSize">Quy mô đoàn</label>
                            <select id="inputGroupSize" name="groupSize" class="form-control">
                                <option value=""></option>
                                @foreach($groupSizes as $groupSize)
                                    <option value="{{$groupSize->id}}">{{$groupSize->groupSize}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputTypeGuide">Loại hình</label>
                            <select id="inputTypeGuide" name="typeGuide" class="form-control">
                                <option value=""></option>
                                @foreach($typeGuides as $typeGuide)
                                    <option value="{{$typeGuide->id}}">{{$typeGuide->typeName}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="hidden-sm hidden-xs">&nbsp;</label>
                            <div>
                                <button class="btn btn-primary btn-add-element" type="button">Thêm</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="elementHolder" class="form-group">
                            <table class="simple-list">
                                <tbody>
                                <tr id="element-1-1">
                                    <td class="simple-item action-1">
                                        <a class="btn-remove-element text-danger" href="#"
                                           data-target="#element-1-1">
                                            <i class="fa fa-close"></i>
                                        </a>
                                    </td>
                                    <td class="simple-item">1. Du lịch - Trung cấp</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>7. Kinh nghiệm hướng dẫn</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputExperienceStartYear">Năm bắt đầu hướng dẫn</label>
                            <select id="inputExperienceStartYear" name="experienceStartYear" class="form-control">
                                @for($i = date('Y'), $lower = $i - 150; $i >= $lower; --$i)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputExperienceDuration">Số năm hướng dẫn</label>
                            <select id="inputExperienceDuration" name="experienceDuration" class="form-control">
                                <option value="1">1 năm</option>
                                <option value="2">10 năm</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>8. Các kỹ năng chuyên môn khác (MC, Game show)</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputOtherSkills" class="sr-only">Các kỹ năng chuyên môn khác</label>
                            <textarea id="inputOtherSkills" name="otherSkills" class="form-control" cols="10"
                                      rows="4"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>9. Thông tin khác</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputOtherInformation" class="sr-only">Thông tin khác</label>
                            <textarea id="inputOtherInformation" name="otherInformation" class="form-control" cols="10"
                                      rows="4"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="section-title">
                <h4>10. Các thành tích (Khen thưởng, Giấy khen, Bằng khen, ...)</h4>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputAchievements" class="sr-only">Các thành tích</label>
                            <textarea id="inputAchievements" name="achievements" class="form-control" cols="10"
                                      rows="4"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <table class="simple-list">
                                <tbody>
                                <tr>
                                    <td>
                                        <div id="achievementHolder"></div>
                                        <div class="group-upload">
                                            <input type="file" class="group-upload-file hidden"
                                                   data-target="#achievementHolder">
                                            <a class="group-upload-action" href="#">
                                                <i class="fa fa-upload"></i>
                                                Tải các thành tích lên
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection