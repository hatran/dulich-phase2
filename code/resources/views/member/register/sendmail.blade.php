<p>Cảm ơn ông/bà đã đăng ký gia nhập Hội Hướng dẫn viên Du lịch Việt Nam</p>
<p>Mã xác nhận của ông/bà là: {{$mailToken}}</p>
</br>
<p>Vui lòng nhập mã xác nhận này vào mục xác nhận địa chỉ email để hoàn tất quá trình xác nhận. Mã xác nhận sẽ hết hiệu lực sau 24 giờ.</p>
</br>
<p>Xin cảm ơn,</p>
</br>
<p>Thư điện tử này được gửi ra từ hệ thống tự động. Vui lòng không trả lời thư điện tử này.</p>
