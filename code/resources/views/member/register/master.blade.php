@section('title', 'Đăng ký hội viên')
@extends('layouts.public')
@section('content')
    <form method="post" name="user-info" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="step" value="{{ $step or 3}}">
        <div class="steps">
            <div class="step{{!empty($step) && $step == 1 ? ' active' : ''}}" data-step="1">1. Đăng ký hội viên</div>
            <div class="step{{!empty($step) && $step == 2 ? ' active' : ''}}" data-step="2">2. Xác nhận số điện thoại</div>
            <div class="step{{empty($step) || $step == 3 ? ' active' : ''}}" data-step="3">3. Hoàn thành</div>
        </div>
        @yield('register_content')
    </form>
@endsection
