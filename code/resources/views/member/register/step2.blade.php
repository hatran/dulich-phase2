@extends('member.register.master')
@section('register_content')
    <div id="step2" class="show block-wrap">
        <div class="center block">
            <div class="showout block-content">
                <div class="field-wrap">
                    Xác nhận email
                    <input type="text" name="emailToken" placeholder="Nhập mã số" required>
                    <input type="hidden" name="memberId" value="{{$memberId}}">
                </div>
                <div class="notice">Mã xác nhận email đã được gửi đến email đăng ký của bạn</div>
            </div>
        </div>
        <div class="form-nav">
            <span class="prev-step btn-step" data-step="1" data-current="2">Quay lại</span>
            <button type="submit" class="next-step btn-step" data-step="3">Tiếp theo</button>
        </div>
    </div>
@endsection