@extends('member.register.master')
@section('register_content')
    <div id="step3" class="show block-wrap">
        <div class="center block">
            <div class="showout block-content">
                <div class="field-wrap">
                    Xác nhận số điện thoại
                    <input type="text" name="phoneToken" placeholder="Nhập mã số" value="" required id='phoneToken'>
                    <input type="hidden" name="memberId" value="{{ $memberId }}" id="memberId">
                    @if(!empty($errors->all()))
                    <span class="help-block">
                        {{ $errors->all()[0] }}
                    </span>
                    @endif
                </div>
                <div class="notice">
                    <p>Hệ Thống sẽ gửi mã xác nhận đến số điện thoại của bạn. Vui lòng xác nhận trong vòng 24h kể từ thời điểm đăng ký.</p>
                    <p>Nếu bạn không nhận được mã xác nhận. Vui lòng click <a href="javascript:void(0)" id="ajaxResendSMS">vào đây</a> để gửi lại mã xác nhận</p>
                </div>
                <div class="alert alert-success" style="display: none; width: 30%; margin: 0 auto;">
                    Gửi lại mã xác nhận thành công
                </div>
            </div>
        </div>
        <div class="form-nav">
            <a style="background-color: #d7d7d7;" href="{{ route('member_register_view') }}?step=1" class="btn-step">Quay lại</a>
            <button type="submit" class="next-step btn-step" data-step="4">Tiếp theo</button>
        </div>
        <form method="post" action="{{ route('member_register_resend_sms_action') }}" id="resendSMS">
            {{ csrf_field() }}
            <input type="hidden" name="memberId" value="{{$memberId}}">
        </form>
    </div>
@endsection
@section('footer_embed')
    <script>
        function resendSms () {
            $('#resendSMS').submit();
        }

        @if(!empty($errors->all()))
            $('#phoneToken').css('box-shadow', 'rgb(206, 63, 56) 0px 0px 10px');
        @endif

        $('#ajaxResendSMS').click(function(e) {
            var memberId = $('#memberId').val();
            $.ajax({
                url: "/member/register/resend_sms",
                type: 'POST',
                dataType: "text",
                data: {memberId: memberId, _token: '{{csrf_token()}}'},
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.status == 1) {          
                        $('.alert-success').fadeIn('slow', function(){
                           $('.alert-success').delay(60000).fadeOut(); 
                        });                     
                    }
                    else {
                        $('.alert-success').css('display', 'none');
                    }
                },
            });
        });

        $('.alert-success').css('display', 'none');
    </script>
@endsection
