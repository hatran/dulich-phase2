<?php
    use App\Libs\Helpers\MemberRegisterHelper;
?>
@extends('member.register.master')
@section('header_embed')
    <link rel="stylesheet" href="{{ asset('css/upload-file.css') }}">
@stop
@section('register_content')
    <div id="step1" class="show block-wrap">
        <div class="block">
            {{--@if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif--}}
            @if ($errors->has('create_member_fail'))
                <div class="alert alert-danger">
                    <div>{{ $errors->first('create_member_fail') }}</div>
                </div>
            @endif
            <div class="block-heading">1. Thông tin chung</div>
            <div class="block-content showout">
                <div class="row">
                    <div class="field-wrap col-md-4 col-sm-10 col-xs-12">
                        <label for="name">Họ và tên <span class="star">*</span></label>
                        <input id="name" type="text" name="fullName" tabindex="1" maxlength="50"
                               value="{{ htmlentities(MemberRegisterHelper::getValueInputView('fullName', $objMember)) }}"
                               class="{{ ($errors->has('fullName')) ? 'error-input' : '' }}" autofocus>
                        @if ($errors->has('fullName'))
                            <span id="name-error" class="help-block">{{ $errors->first('fullName') }}</span>
                        @endif
                    </div>
                    <div class="dropfield field-wrap col-md-2 col-sm-2 col-xs-12">
                        <label for="gender">Giới tính <span class="star">*</span></label>
                        <select id="gender" name="gender" class="{{ ($errors->has('gender')) ? 'error-input' : '' }}" tabindex="2">
                            <option value=""></option>
                            <option value="1"{{MemberRegisterHelper::getValueInputView('gender', $objMember) == 1 ? 'selected' : ''}}>Nam
                            </option>
                            <option value="2"{{MemberRegisterHelper::getValueInputView('gender', $objMember) == 2 ? 'selected' : ''}}>Nữ
                            </option>
                        </select>
                        @if ($errors->has('gender'))
                            <span id="gender-error" class="help-block">{{ $errors->first('gender') }}</span>
                        @endif
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="datetime field-wrap col-md-4 col-sm-4 col-xs-4">
                                <label for="date">Ngày sinh <span class="star">*</span></label>
                                <input id="date" tabindex="3"
                                       class="day datetime-input {{ ($errors->has('birthDate') || $errors->has('bod')) ? 'error-input' : '' }}"
                                       required type="text" maxlength="2" name="birthDate"
                                       value="{{ MemberRegisterHelper::getValueInputView('birthDate', $objMember) }}">
                                <span class="datetime-icon fa fa-calendar"></span>
                                @if ($errors->has('birthDate'))
                                    <span id="date-error" class="help-block">{{ $errors->first('birthDate') }}</span>
                                @endif
                            </div>
                            <div class="datetime field-wrap col-md-4 col-sm-4 col-xs-4">
                                <label for="month">Tháng sinh <span class="star">*</span></label>
                                <input id="month" tabindex="4"
                                       class="month datetime-input {{ ($errors->has('birthMonth') || $errors->has('bod')) ? 'error-input' : '' }}"
                                       type="text" maxlength="2"
                                       name="birthMonth" required
                                       value="{{ MemberRegisterHelper::getValueInputView('birthMonth', $objMember) }}">
                                <span class="datetime-icon fa fa-calendar"></span>
                                @if ($errors->has('birthMonth'))
                                    <span id="date-error" class="help-block">{{ $errors->first('birthMonth') }}</span>
                                @endif
                            </div>
                            <div class="datetime field-wrap col-md-4 col-sm-4 col-xs-4">
                                <label for="year">Năm sinh <span class="star">*</span></label>
                                <input id="year" tabindex="5"
                                       class="expireyear datetime-input {{ ($errors->has('birthYear') || $errors->has('bod')) ? 'error-input' : '' }}"
                                       required type="text" maxlength="4" name="birthYear"
                                       value="{{ MemberRegisterHelper::getValueInputView('birthYear', $objMember) }}">
                                <span class="datetime-icon fa fa-calendar"></span>
                                @if ($errors->has('birthYear'))
                                    <span id="date-error" class="help-block">{{ $errors->first('birthYear') }}</span>
                                @endif
                            </div>
                            @if ($errors->has('bod'))
                                <span id="date-error" class="help-block"
                                      style="margin-left: 15px">{{ $errors->first('bod') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="field-wrap col-md-12 col-sm-12 col-xs-12">
                        <span class="btn btn-success fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>Thêm ảnh đại diện...</span>
                            <!-- The file input field used as target for the file upload widget -->
                            <input id="fileupload" type="file" name="file">
                            <input type="text" id="avatar" name="avatar" value="" style="visibility:  hidden;">
                        </span>
                        <span class="star" style="color: #a94442">*</span>
                        <span id="fileuploaded"></span>
                        <span class="help-block">Tải ảnh chân dung mầu cỡ 1.5 cm x 1.5 cm</span>
                    </div>
                </div>
                <div class="row">
                    <div class="field-wrap col-md-12 radio-list choices">
                        <label>Hướng dẫn viên <span class="star">*</span></label>
                        <div class="choice {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 1 ? 'chosen' : '' }}">
                            <label for="job1" tabindex="7">HDV du lịch quốc tế </label>
                            <input id="job1" type="radio" name="typeOfTravelGuide"
                                   value="1" {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 1 ? 'checked' : '' }}>
                        </div>
                        <div class="choice {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 2 ? 'chosen' : '' }}">
                            <label for="job2" tabindex="8">HDV du lịch nội địa
                            </label>
                            <input id="job2" type="radio" name="typeOfTravelGuide"
                                   value="2" {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 2 ? 'checked' : '' }}>
                        </div>
                        <div class="choice {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 3 ? 'chosen' : '' }}">
                            <label for="job3" tabindex="9">HDV du lịch tại điểm
                            </label>
                            <input id="job3" type="radio" name="typeOfTravelGuide"
                                   value="3" {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 3 ? 'checked' : '' }}>
                        </div>
                        @if ($errors->has('typeOfTravelGuide'))
                            <span id="typeOfTravelGuide-error"
                                  class="help-block">{{ $errors->first('typeOfTravelGuide') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="dropfield field-wrap {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) != 1 ? 'hidden' : '' }} col-md-4 col-sm-6 col-xs-12">
                        <label for="inboundOutbound">Inbound/Outbound <span class="star">*</span></label>
                        <select id="inboundOutbound" name="inboundOutbound" tabindex="10"
                                class="{{ ($errors->has('inboundOutbound')) ? 'error-input' : '' }}">
                            <option value=""></option>
                            <option value="1" {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 1 && MemberRegisterHelper::getValueInputView('inboundOutbound', $objMember) == 1 ? 'selected' : '' }}>
                                HDV Du lịch Quốc tế Inbound
                            </option>
                            <option value="2" {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 1 && MemberRegisterHelper::getValueInputView('inboundOutbound', $objMember) == 2 ? 'selected' : '' }}>
                                HDV Du lịch Quốc tế Outbound
                            </option>
                            <option value="3" {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 1 && MemberRegisterHelper::getValueInputView('inboundOutbound', $objMember) == 3 ? 'selected' : '' }}>
                                HDV Du lịch Quốc tế Inbound - Outbound
                            </option>
                        </select>
                        @if ($errors->has('inboundOutbound'))
                            <span id="inboundOutbound-error"
                                  class="help-block">{{ $errors->first('inboundOutbound') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="idNumb">Số thẻ HDV du lịch <span class="star">*</span></label>
                        <input id="idNumb" type="text" name="touristGuideCode" tabindex="11" maxlength="9"
                               value="{{ MemberRegisterHelper::getValueInputView('touristGuideCode', $objMember) }}"
                               class="{{ ($errors->has('touristGuideCode')) ? 'error-input' : '' }}">
                        @if ($errors->has('touristGuideCode'))
                            <span id="touristGuideCode-error"
                                  class="help-block">{{ $errors->first('touristGuideCode') }}</span>
                        @endif
                    </div>

                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12 {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 3 ? 'hidden' : '' }}">
                        <label for="expireDate">Ngày hết hạn<span
                                    class="star">*</span></label>
                        <input id="expireDate" type="text" name="expirationDate" placeholder="DD/MM/YYYY" value="" tabindex="12" maxlength="10"
                               class="{{ ($errors->has('expirationDate')) ? 'error-input' : '' }}">
                        @if ($errors->has('expirationDate'))
                            <span id="touristGuideCode-error"
                                  class="help-block">{{ $errors->first('expirationDate') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                        <label for="cmt">Số CMT/CCCD <span class="star">*</span></label>
                        <input id="cmt" type="text" name="cmtCccd" tabindex="13" maxlength="20"
                               value="{{ htmlentities(MemberRegisterHelper::getValueInputView('cmtCccd', $objMember)) }}"
                               class="{{ ($errors->has('cmtCccd')) ? 'error-input' : '' }}">
                        @if ($errors->has('cmtCccd'))
                            <span id="cmt-error" class="help-block">{{ $errors->first('cmtCccd') }}</span>
                        @endif
                    </div>
                    <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                        <label for="cmtDate">Cấp ngày <span class="star">*</span></label>
                        <input id="cmtDate" type="text" name="dateIssued" placeholder="DD/MM/YYYY" required value="" tabindex="14" maxlength="10"
                               class="{{ ($errors->has('dateIssued')) ? 'error-input' : '' }}">
                        @if ($errors->has('dateIssued'))
                            <span id="cmtDate-error" class="help-block">{{ $errors->first('dateIssued') }}</span>
                        @endif
                    </div>
                    <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                        <label for="cmtPlace">Tại <span class="star">*</span></label>
                        <input id="cmtPlace" type="text" maxlength="50" name="issuedBy" tabindex="15"
                               value="{{ htmlentities(MemberRegisterHelper::getValueInputView('issuedBy', $objMember)) }}"
                               class="{{ ($errors->has('issuedBy')) ? 'error-input' : '' }}">
                        @if ($errors->has('issuedBy'))
                            <span id="cmtPlace-error" class="help-block">{{ $errors->first('issuedBy') }}</span>
                        @endif
                    </div>
                </div>
                <div class="field-wrap">
                    <label for="livingAddress">Địa chỉ thường trú <span class="star">*</span></label>
                    <textarea id="livingAddress" name="permanentAddress" rows="3" tabindex="16" maxlength="200"
                              class="{{ ($errors->has('permanentAddress')) ? 'error-input' : '' }}"
                              placeholder="Đề nghị ghi rõ địa chỉ từ số nhà, thôn( xóm), đường, phường (xã), quận(huyện), tỉnh(thành phố)">{{ htmlentities(MemberRegisterHelper::getValueInputView('permanentAddress', $objMember)) }}</textarea>
                    @if ($errors->has('permanentAddress'))
                        <span id="livingAddress-error"
                              class="help-block">{{ $errors->first('permanentAddress') }}</span>
                    @endif
                </div>

                <div class="field-wrap">
                    <label for="contactAddress">Địa chỉ liên hệ (Địa chỉ nhận thẻ hội viên) <span class="star">*</span></label>
                    <textarea id="contactAddress" name="address" rows="3" tabindex="17" maxlength="200"
                              class="{{ ($errors->has('address')) ? 'error-input' : '' }}"
                              placeholder="Đề nghị ghi rõ địa chỉ từ số nhà, thôn( xóm), đường, phường (xã), quận(huyện), tỉnh(thành phố)">{{ htmlentities(MemberRegisterHelper::getValueInputView('address', $objMember)) }}</textarea>
                    @if ($errors->has('address'))
                        <span id="contactAddress-error" class="help-block">{{ $errors->first('address') }}</span>
                    @endif
                </div>

                {{--Tỉnh thành--}}
                <div class="row">
                    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="province">Địa bàn hoạt động <span class="star">*</span></label>
                        <select id="province" name="province" tabindex="18"
                                class="{{ ($errors->has('province')) ? 'error-input' : '' }}">
                            <option value=""></option>
                            @if (!empty($provincial))
                                @foreach($provincial as $key => $value)
                                    <option value="{{ $value->id }}" {{ (MemberRegisterHelper::getValueInputView('province', $objMember) == $value->id) ? 'selected' : '' }}> {{ $value->value }} </option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors->has('province'))
                            <span id="province-error"
                                  class="help-block">{{ $errors->first('province') }}</span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="phone1">Điện thoại di động 1 <span class="star">*</span></label>
                        <input id="phone1" type="text" name="firstMobile" tabindex="19" maxlength="15"
                               value="{{ MemberRegisterHelper::getValueInputView('firstMobile', $objMember) }}"
                               class="{{ ($errors->has('firstMobile')) ? 'error-input' : '' }}">
                        @if ($errors->has('firstMobile'))
                            <span id="phone1-error" class="help-block">{{ $errors->first('firstMobile') }}</span>
                        @endif
                    </div>
                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="phone2">Điện thoại di động 2</label>
                        <input id="phone2" type="text" name="secondMobile" tabindex="20" maxlength="15"
                               value="{{ htmlentities(MemberRegisterHelper::getValueInputView('secondMobile', $objMember)) }}"
                               class="{{ ($errors->has('secondMobile')) ? 'error-input' : '' }}">
                        @if ($errors->has('secondMobile'))
                            <span id="email1-error" class="help-block">{{ $errors->first('secondMobile') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="email1">Email <span class="star">*</span></label>
                        <input id="email1" type="text" name="firstEmail" tabindex="21" maxlength="50"
                               value="{{ MemberRegisterHelper::getValueInputView('firstEmail', $objMember) }}"
                               class="{{ ($errors->has('firstEmail')) ? 'error-input' : '' }}">
                        @if ($errors->has('firstEmail'))
                            <span id="email1-error" class="help-block">{{ $errors->first('firstEmail') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="emailConfirm">Xác nhận Email <span class="star">*</span></label>
                        <input id="emailConfirm" type="text" name="emailConfirm" tabindex="22" maxlength="50"
                               value="{{ htmlentities(MemberRegisterHelper::getValueInputView('firstEmail', $objMember)) }}"
                               class="{{ ($errors->has('emailConfirm')) ? 'error-input' : '' }}">
                        @if ($errors->has('emailConfirm'))
                            <span id="email1-error" class="help-block">{{ $errors->first('emailConfirm') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row" id="guideLanguage-block" class="block">
                    @if(!empty($objMem))
                        <?php $objGuideLanguage = $objMem->guideLanguage; ?>
                        @foreach(explode(",", $objGuideLanguage) as $key => $guideLanguage)
                            <div class="ol-guideLanguage-list {{ ($key == 0 ? '' : 'guideLanguage-ol') }}" data-guideLanguage-ol="{{ $key + 1 }}">
                                <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                                    <label for="guideLanguage{{ ($key == 0) ? '' : $key }}" style="width: auto;">Ngôn ngữ hướng dẫn {{ ($key == 0) ? '1' : $key+1 }}</label>
                                    @if ($key != 0)
                                        <span class="delete-guideLanguage-field icon-close" style="font-size: 16px;color: #2481bf;cursor: pointer;float:right;"></span>
                                        <div style="clear:both;"></div>
                                    @endif
                                    <select id="guideLanguage{{ ($key == 0) ? '' : $key }}" name="guideLanguage[{{$key}}]" tabindex="<?= $position + 3; ?>"
                                            {{ ($key == 0) ? 'required' : '' }} class="{{ ($errors->has('guideLanguage[]')) ? 'error-input' : '' }}">
                                        <option value=""></option>
                                        @if (!empty($languages))
                                            @foreach($languages as $language)
                                                <option value="{{ $language->id }}" {{ ($language->id == $guideLanguage) ? 'selected' : '' }}> {{ $language->languageName }} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('guideLanguage[]'))
                                        <span id="guideLanguage-error"
                                              class="help-block">{{ $errors->first('guideLanguage[]') }}</span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @elseif (!empty(old('guideLanguage')) && count(old('guideLanguage')) > 1)
                        @foreach(old('guideLanguage') as $keyLoopOldWork => $guideLanguage)
                            <div class="{{ $keyLoopOldWork != 0 ? 'guideLanguage-ol' : '' }} ol-guideLanguage-list"
                                 data-guideLanguage-ol="{{ $keyLoopOldWork + 1 }}">
                                
                                <div class="dropfield field-wrap col-md-3 col-sm-2 col-xs-12">
                                    <label for="guideLanguage" style="width: auto;">Ngôn ngữ hướng dẫn {{ $keyLoopOldWork+1 }} <span style="color: red;">*</span></label>
                                    @if ($keyLoopOldWork != 0)
                                        <span class="delete-guideLanguage-field icon-close" style="font-size: 16px;color: #2481bf;cursor: pointer;float:right;"></span>
                                        <div style="clear:both;"></div>
                                    @endif
                                    <select id="guideLanguage{{ ($keyLoopOldWork == 0) ? '' : $keyLoopOldWork }}" required tabindex="<?= $position + 3; ?>"
                                            name="guideLanguage[{{$keyLoopOldWork}}]" {{ $keyLoopOldWork == 0 ? 'required' : '' }}>
                                        <option value=""></option>
                                        @if (!empty($languages))
                                        @foreach($languages as $language)
                                            <option value="{{ $language->id }}" {{ (array_get(old('guideLanguage'), $keyLoopOldWork, '') == $language->id) ? 'selected' : '' }}> {{ $language->languageName }} </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="ol-guideLanguage-list" data-guideLanguage-ol="1">
                            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                            <label for="guideLanguage" style="width: auto;">Ngôn ngữ hướng dẫn 1 <span style="color: red;">*</span></label>
                                <select id="guideLanguage" name="guideLanguage[0]" required tabindex="<?= $position + 3; ?>" class="{{ ($errors->has('guideLanguage[0]')) ? 'error-input' : '' }}" style="box-shadow:none">
                                    <option value=""></option>
                                    @if (!empty($languages))
                                        @foreach($languages as $language)
                                            <option value="{{ $language->id }}" {{ (array_get(old('guideLanguage'), 0, '') == $language->id) ? 'selected' : '' }}> {{ $language->languageName }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('guideLanguage[0]'))
                                    <span id="forte-error"
                                          class="help-block">{{ $errors->first('guideLanguage[0]') }}</span>
                                @endif
                            </div>
                        </div>
                    @endif
                    <div class="add-guideLanguage-field">
                        <div class="field-wrap col-md-12 col-sm-12 col-xs-12">
                            Thêm ngôn ngữ hướng dẫn
                        </div>
                    </div>
                   
                </div>
                <div class="flex radio-list choices field-wrap">
                    <label>Đăng ký sinh hoạt tại <span class="star">*</span></label> <br/>
                    <div class="field-col">
                        <div class="choices-wrap" style="padding: 0; margin: 0; border: none; margin-left: 20px;position: relative;
    top: -6px;">
                            <div class="choice {{ MemberRegisterHelper::getValueInputView('typeOfPlace', $objMember) == 1 ? 'chosen' : '' }}">
                                <label for="activity1" tabindex="24">Chi hội</label>
                                <input id="activity1" type="radio" name="typeOfPlace"
                                       value="1" {{MemberRegisterHelper::getValueInputView('typeOfPlace', $objMember) == 1 ? 'checked' : ''}}>
                            </div>
                            <div class="dropfield field-wrap" style="position: relative; top: -2px;">
                                <select name="province_code" id="province_code" tabindex="25"
                                        class="{{ ($errors->has('province_code')) ? 'error-input' : '' }}">
                                    <option value=""></option>
                                    @foreach($branch_chihoi as $key => $chiHoi)
                                        <option value="{{ $chiHoi->id }}" {{ (MemberRegisterHelper::getValueInputView('province_code', $objMember) == $chiHoi->id && MemberRegisterHelper::getValueInputView('typeOfPlace', $objMember) == 1) ? ' selected' : '' }}>
                                            {{ $chiHoi->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('province_code'))
                                    <span id="province_code-error"
                                          class="help-block">{{ $errors->first('province_code') }}</span>
                                @endif
                            </div>
                        </div>
                       <!--  <div class="choices-wrap">
                            <div class="choice {{ MemberRegisterHelper::getValueInputView('typeOfPlace', $objMember) == 2 ? 'chosen' : ''}}">
                                <label for="activity2" tabindex="26">CLB thuộc hội</label>
                                <input id="activity2" type="radio" name="typeOfPlace"
                                       value="2" {{ MemberRegisterHelper::getValueInputView('typeOfPlace', $objMember) == 2 ? 'checked' : ''}}>
                            </div>
                            <div class="dropfield field-wrap">
                                <select name="club" class="{{ ($errors->has('club')) ? 'error-input' : '' }}" id="club" tabindex="27">
                                    <option value=""></option>
                                    {{--<option value="65" {{ ((old('club') == 65 || MemberRegisterHelper::getValueInputView('province_code', $objMember) == 65) && MemberRegisterHelper::getValueInputView('typeOfPlace', $objMember) == 2) ? ' selected' : '' }}>
                                        CLB HDV tiếng Nhật Hà Nội
                                    </option>--}}
                                    @foreach($branch_clbthuochoi as $key => $clbThuocHoi)
                                        <option value="{{ $clbThuocHoi->id }}" {{ (MemberRegisterHelper::getValueInputView('province_code', $objMember) == $clbThuocHoi->id && MemberRegisterHelper::getValueInputView('typeOfPlace', $objMember) == 2) ? ' selected' : '' }}>
                                            {{ $clbThuocHoi->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('club'))
                                    <span id="club-error" class="help-block">{{ $errors->first('club') }}</span>
                                @endif
                            </div>
                        </div> -->
                        @if ($errors->has('typeOfPlace'))
                            <span id="club-error" class="help-block">{{ $errors->first('typeOfPlace') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div id="education-block" class="block">
            <div class="block-heading">2. Thông tin học vấn <span class="star">*</span></div>
            <div class="block-content">
                <div class="row">
                    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="specialized">Chuyên ngành</label>
                        <select id="specialized" name="educationBranches" tabindex="28"
                                class="{{ ($errors->has('educationBranches')) ? 'error-input' : '' }}">
                            <option value=""></option>
                            @foreach($educationBranches as $educationBranch)
                                <option value="{{ $educationBranch->id }}" {{ MemberRegisterHelper::getValueInputHasRelationView('educationBranches', 'educationBranchId', $objMember) == $educationBranch->id ? 'selected' : '' }}>{{$educationBranch->branchName}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('educationBranches'))
                            <span id="specialized-error"
                                  class="help-block">{{ $errors->first('educationBranches') }}</span>
                        @endif
                    </div>
                    <div class="selectdrop dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="level">Trình độ</label>
                        <select id="level" name="educationDegrees" tabindex="29"
                                class="{{ ($errors->has('educationDegrees')) ? 'error-input' : '' }}">
                            <option value=""></option>
                            @foreach($educationDegrees as $educationDegree)
                                <option value="{{ $educationDegree->id }}" {{ MemberRegisterHelper::getValueInputHasRelationView('educationDegrees', 'educationDegreeId', $objMember) == $educationDegree->id ? 'selected' : '' }}>{{ $educationDegree->degree }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('educationDegrees'))
                            <span id="level-error" class="help-block">{{ $errors->first('educationDegrees') }}</span>
                        @endif
                    </div>
                </div>
                {{--<div class="btn-upload" data-auto-remove="1">
                    <label class="fa fa-upload" for="majorFile1"> Tải lên bản sao văn bằng hoặc giấy chứng nhận</label>
                    <input id="majorFile1" type="file" name="educationFiles" accept="image/*">
                </div>--}}
            </div>
        </div>
        <div id="major-block" class="block" style="margin-bottom: 0px">
            <div class="block-heading">3. Thông tin về nghiệp vụ hướng dẫn <span class="star">*</span></div>
            <div class="block-content">
                <ul class="radio-list choices has-options list" style="padding-left: 0px">
                    <div class="field-wrap">
                    @foreach($majors as $key => $major)
                        <li class="choice {{ MemberRegisterHelper::getValueInputHasRelationView('majors', 'majorId', $objMember) == $major->id ? 'chosen' : '' }}">
                            <input id="major{{$major->id}}" type="radio" name="majors"
                                   value="{{ $major->id }}" {{ MemberRegisterHelper::getValueInputHasRelationView('majors', 'majorId', $objMember) == $major->id ? 'checked' : '' }}>
                            <label for="major{{$major->id}}" tabindex="<?= $key + 29; ?>" style="font-weight: 100">{{ $major->majorName }}</label>
                        </li>
                    @endforeach
                    </div>
                </ul>
                @if ($errors->has('majors'))
                    <span id="majors-error" class="help-block">{{ $errors->first('majors') }}</span>
                @endif
                {{--<div class="upload-list">
                    <div class="btn-upload" data-auto-remove="1">
                        <label class="fa fa-upload" for="majorFile1"> Tải lên bản sao văn bằng hoặc giấy chứng nhận</label>
                        <input id="majorFile1" type="file" name="majorFiles[]" accept="image/*">
                    </div>
                </div>--}}

            </div>
        </div>
        <div id="language-skill-block" class="block">
            <div class="block-heading">4. Thông tin về trình độ ngoại ngữ <span class="star {{ MemberRegisterHelper::getValueInputView('typeOfTravelGuide', $objMember) == 1 ? ' ' : 'hidden' }}">*</span></div>
            <div class="block-content">
                <div class="note">Đối với hướng dẫn viên quốc tế</div>
                <div class="row">
                    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="languages">Chuyên ngành</label>
                        <select id="language" name="language" tabindex="<?= $position; ?>"
                                class="{{ ($errors->has('language')) ? 'error-input' : '' }}">
                            <option value=""></option>
                            @foreach($languages as $language)
                                <option value="{{ $language->id }}" {{ MemberRegisterHelper::getValueInputHasRelationView('language', 'languageId', $objMember) == $language->id ? 'selected' : '' }}>{{$language->languageName}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('language'))
                            <span id="language-error" class="help-block">{{ $errors->first('language') }}</span>
                        @endif
                    </div>

                    <div class="selectdrop dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="level2">Trình độ</label>
                        <select id="level2" name="level2" class="{{ ($errors->has('level2')) ? 'error-input' : '' }}" tabindex="<?= $position + 1; ?>">
                            <option value=""></option>
                            @foreach($languageLevels as $languageLevel)
                                <option value="{{ $languageLevel->id }}" {{ MemberRegisterHelper::getValueInputHasRelationView('level2', 'levelId', $objMember) == $languageLevel->id ? 'selected' : '' }}>{{$languageLevel->levelName}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('level2'))
                            <span id="language-error" class="help-block">{{ $errors->first('level2') }}</span>
                        @endif


                        {{--<input id="level2" class="selectdrop-input {{ ($errors->has('level2')) ? 'error-input' : '' }}" type="text" name="level2" readonly>
                        <div class="selectdropdown radio-list choices">
                            @foreach($languageLevels as $languageLevel)
                                <div class="choice" data-id="{{$languageLevel->id}}">
                                    <label>{{$languageLevel->levelName}}</label>
                                </div>
                            @endforeach
                        </div>--}}
                        {{--@if ($errors->has('level2'))
                            <span id="level2-error" class="help-block">{{ $errors->first('level2') }}</span>
                        @endif--}}
                    </div>
                </div>
                <ul class="list"></ul>
            </div>
        </div>
        <div id="work-block" class="block">
            <div class="block-heading">5. Thông tin về quá trình hành nghề hướng dẫn <span class="star">*</span>
            </div>
            <div class="block-content">
                @if(!empty($objMember))
                    @foreach($objMember->works as $key => $work)
                        <div class="ol-list {{ ($key == 0 ? '' : 'new-ol') }}" data-ol="{{ $key + 1 }}">
                            <div class="field-wrap col-md-4 col-sm-12 col-xs-12">
                                <label for="company{{ ($key == 0) ? '' : $key }}">Tên doanh nghiệp đã và đang công
                                    tác</label>
                                <input id="company{{ ($key == 0) ? '' : $key }}" type="text" name="workElementNames[]" maxlength="100" tabindex="<?= $position + 2; ?>"
                                       {{ ($key == 0) ? 'required' : '' }} value="{{ htmlentities($work->elementName) }}"
                                       class="{{ ($errors->has('workElementNames[]')) ? 'error-input' : '' }}">
                                @if ($errors->has('workElementNames[]'))
                                    <span id="company-error"
                                          class="help-block">{{ $errors->first('workElementNames[]') }}</span>
                                @endif
                            </div>
                            <div class="dropfield field-wrap col-md-3 col-sm-2 col-xs-12">
                                <label for="jobStatus{{ ($key == 0) ? '' : $key }}">Hình thức lao động</label>
                                <select id="jobStatus{{ ($key == 0) ? '' : $key }}" name="workTypeOfContracts[]" tabindex="<?= $position + 3; ?>"
                                        {{ ($key == 0) ? 'required' : '' }} class="{{ ($errors->has('workTypeOfContracts[]')) ? 'error-input' : '' }}">
                                    <option value=""></option>
                                    @if (!empty($workConstant))
                                        @foreach($workConstant as $key => $value)
                                            <option value="{{ $value->id }}" {{ ($work->typeOfContract == $value->id) ? 'selected' : '' }}> {{ $value->workName }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('workTypeOfContracts[]'))
                                    <span id="jobStatus-error"
                                          class="help-block">{{ $errors->first('workTypeOfContracts[]') }}</span>
                                @endif
                            </div>
                            <div class="col-md-5 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="datetime field-wrap col-md-6 col-sm-6 col-xs-6">
                                        <label for="jobYearFrom{{ ($key == 0) ? '' : $key }}">Thời gian bắt đầu</label>
                                        <input id="jobYearFrom{{ ($key == 0) ? '' : $key }}" tabindex="<?= $position + 4; ?>"  maxlength="100"
                                               class="year datetime-input {{ ($errors->has('workFromDates[]')) ? 'error-input' : '' }}"
                                               {{ ($key == 0) ? 'required' : '' }} value="{{ old('workFromDates[]') }}"
                                               type="text" maxlength="4" name="workFromDates[]">
                                        <span class="datetime-icon fa fa-calendar"></span>
                                        @if ($errors->has('workFromDates[]'))
                                            <span id="jobYearFrom-error"
                                                  class="help-block">{{ $errors->first('workFromDates[]') }}</span>
                                        @endif
                                    </div>
                                    <div class="datetime field-wrap col-md-6 col-sm-6 col-xs-6">
                                        <label for="jobYearTo{{ ($key == 0) ? '' : $key }}">Thời gian kết thúc</label>
                                        <input id="jobYearTo{{ ($key == 0) ? '' : $key }}" tabindex="<?= $position + 5; ?>"
                                               class="year datetime-input {{ ($errors->has('workToDates[]')) ? 'error-input' : '' }}"
                                               {{ ($key == 0) ? 'required' : '' }} value="{{ old('workToDates[]') }}"
                                               type="text" maxlength="4" name="workToDates[]">
                                        <span class="datetime-icon fa fa-calendar"></span>
                                        @if ($errors->has('workToDates[]'))
                                            <span id="jobYearTo-error"
                                                  class="help-block">{{ $errors->first('workToDates[]') }}</span>
                                        @endif
                                    </div>
                                    <script>
                                        $(document).ready(function () {
                                            @if ($key == 0)
                                            $('#jobYearFrom').val('{{ empty($work->fromDate) ? '' : $work->fromDate->format('Y') }}');
                                            $('#jobYearTo').val('{{ empty($work->toDate) ? '' : $work->toDate->format('Y') }}');
                                            @else
                                            $('#jobYearFrom{{ $key }}').val('{{ empty($work->fromDate) ? '' : $work->fromDate->format('Y') }}');
                                            $('#jobYearTo{{ $key }}').val('{{ empty($work->toDate) ? '' : $work->toDate->format('Y') }}');
                                            @endif
                                        });
                                    </script>
                                </div>
                            </div>
                            @if ($key != 0)
                                <span class="delete-field icon-close"></span>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    @endforeach
                @elseif (!empty(old('workElementNames') && count(old('workElementNames')) > 1))
                    @foreach(old('workElementNames') as $keyLoopOldWork => $workElementNames)
                        <div class="{{ $keyLoopOldWork != 0 ? 'new-ol' : '' }} ol-list"
                             data-ol="{{ $keyLoopOldWork + 1 }}">
                            <div class="field-wrap col-md-4 col-sm-12 col-xs-12">
                                <label for="company">Tên doanh nghiệp đã và đang công tác</label>
                                <input id="company" type="text" name="workElementNames[]" tabindex="<?= $position + 2; ?>" maxlength="100"
                                       {{ $keyLoopOldWork == 0 ? 'required' : '' }} value="{{ htmlentities($workElementNames) }}">
                                {{--@if ($errors->has('workElementNames[0]'))
                                    <span id="company-error" class="help-block">{{ $errors->first('workElementNames[0]') }}</span>
                                @endif--}}
                            </div>
                            <div class="dropfield field-wrap col-md-3 col-sm-2 col-xs-12">
                                <label for="jobStatus">Hình thức lao động</label>
                                <select id="jobStatus" tabindex="<?= $position + 3; ?>"
                                        name="workTypeOfContracts[]" {{ $keyLoopOldWork == 0 ? 'required' : '' }}>
                                    <option value=""></option>
                                    @if (!empty($workConstant))
                                        @foreach($workConstant as $key => $value)
                                            <option value="{{ $value->id }}" {{ (array_get(old('workTypeOfContracts'), $keyLoopOldWork, '') == $value->id) ? 'selected' : '' }}> {{ $value->workName }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                {{--@if ($errors->has('workTypeOfContracts[0]'))
                                    <span id="jobStatus-error" class="help-block">{{ $errors->first('workTypeOfContracts[0]') }}</span>
                                @endif--}}
                            </div>
                            <div class="col-md-5 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="datetime field-wrap col-md-6 col-sm-6 col-xs-6">
                                        <label for="jobYearFrom{{ $keyLoopOldWork != 0 ? $keyLoopOldWork : ''}}">Thời
                                            gian bắt đầu</label>
                                        <input id="jobYearFrom{{ $keyLoopOldWork != 0 ? $keyLoopOldWork : ''}}" tabindex="<?= $position + 4; ?>"
                                               class="year datetime-input"
                                               {{ $keyLoopOldWork == 0 ? 'required' : '' }} type="text" maxlength="4"
                                               name="workFromDates[]">
                                        <span class="datetime-icon fa fa-calendar"></span>
                                        {{--@if ($errors->has('workFromDates[0]'))
                                            <span id="jobYearFrom-error" class="help-block">{{ $errors->first('workFromDates[0]') }}</span>
                                        @endif--}}
                                    </div>
                                    <div class="datetime field-wrap col-md-6 col-sm-6 col-xs-6">
                                        <label for="jobYearTo{{ $keyLoopOldWork != 0 ? $keyLoopOldWork : ''}}">Thời gian
                                            kết thúc</label>
                                        <input id="jobYearTo{{ $keyLoopOldWork != 0 ? $keyLoopOldWork : ''}}" tabindex="<?= $position + 5; ?>"
                                               class="year datetime-input"
                                               {{ $keyLoopOldWork == 0 ? 'required' : '' }} type="text" maxlength="4"
                                               name="workToDates[]">
                                        <span class="datetime-icon fa fa-calendar"></span>
                                        {{--@if ($errors->has('workToDates[0]'))
                                            <span id="jobYearTo-error" class="help-block">{{ $errors->first('workToDates[0]') }}</span>
                                        @endif--}}
                                    </div>
                                </div>
                            </div>
                            <script>
                                $(document).ready(function () {
                                    @if ($keyLoopOldWork == 0)
                                    $('#jobYearFrom').val('{{ array_get(old('workFromDates'), $keyLoopOldWork, '') }}');
                                    $('#jobYearTo').val('{{ array_get(old('workToDates'), $keyLoopOldWork, '') }}');
                                    @else
                                    $('#jobYearFrom{{ $keyLoopOldWork }}').val('{{ array_get(old('workFromDates'), $keyLoopOldWork, '') }}');
                                    $('#jobYearTo{{ $keyLoopOldWork }}').val('{{ array_get(old('workToDates'), $keyLoopOldWork, '') }}');
                                    @endif
                                });
                            </script>
                            @if ($keyLoopOldWork != 0)
                                <span class="delete-field icon-close"></span>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    @endforeach
                @else
                    <div class="ol-list" data-ol="1">
                        <div class="field-wrap col-md-4 col-sm-12 col-xs-12">
                            <label for="company">Tên doanh nghiệp đã và đang công tác</label>
                            <input id="company" type="text" name="workElementNames[0]" required tabindex="<?= $position + 2; ?>" maxlength="100"
                                   value="{{ htmlentities(array_get(old('workElementNames'), 0, '')) }}"
                                   class="{{ ($errors->has('workElementNames[0]')) ? 'error-input' : '' }}">
                            @if ($errors->has('workElementNames[0]'))
                                <span id="company-error"
                                      class="help-block">{{ $errors->first('workElementNames[0]') }}</span>
                            @endif
                        </div>
                        <div class="dropfield field-wrap col-md-3 col-sm-2 col-xs-12">
                            <label for="jobStatus">Hình thức lao động</label>
                            <select id="jobStatus" name="workTypeOfContracts[0]" required tabindex="<?= $position + 3; ?>" class="{{ ($errors->has('workTypeOfContracts[0]')) ? 'error-input' : '' }}">
                                <option value=""></option>
                                @if (!empty($workConstant))
                                    @foreach($workConstant as $key => $value)
                                        <option value="{{ $value->id }}" {{ (array_get(old('workTypeOfContracts'), 0, '') == $value->id) ? 'selected' : '' }}> {{ $value->workName }} </option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('workTypeOfContracts[0]'))
                                <span id="jobStatus-error"
                                      class="help-block">{{ $errors->first('workTypeOfContracts[0]') }}</span>
                            @endif
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-12">
                            <div class="row">
                                <div class="datetime field-wrap col-md-6 col-sm-6 col-xs-6">
                                    <label for="jobYearFrom">Thời gian bắt đầu</label>
                                    <input id="jobYearFrom" tabindex="<?= $position + 4; ?>"
                                           class="year datetime-input {{ ($errors->has('workFromDates[0]')) ? 'error-input' : '' }}"
                                           required value="{{ old('workFromDates[0]') }}" type="text" maxlength="4"
                                           name="workFromDates[0]" placeholder="Từ năm">
                                    <span class="datetime-icon fa fa-calendar"></span>
                                    @if ($errors->has('workFromDates[0]'))
                                        <span id="jobYearFrom-error"
                                              class="help-block">{{ $errors->first('workFromDates[0]') }}</span>
                                    @endif
                                </div>
                                <div class="datetime field-wrap col-md-6 col-sm-6 col-xs-6">
                                    <label for="jobYearTo">Thời gian kết thúc</label>
                                    <input id="jobYearTo" tabindex="<?= $position + 5; ?>"
                                           class="year datetime-input {{ ($errors->has('workToDates[0]')) ? 'error-input' : '' }}"
                                           required value="{{ old('workToDates[0]') }}" type="text" maxlength="4"
                                           name="workToDates[0]" placeholder="Đến năm">
                                    <span class="datetime-icon fa fa-calendar"></span>
                                    @if ($errors->has('workToDates[0]'))
                                        <span id="jobYearTo-error"
                                              class="help-block">{{ $errors->first('workToDates[0]') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('#jobYearFrom').val('{{ array_get(old('workFromDates'), 0, '') }}');
                                $('#jobYearTo').val('{{ array_get(old('workToDates'), 0, '') }}');
                            });
                        </script>
                        <div class="clearfix"></div>
                    </div>
                @endif
                <div class="add-field">Thêm thông tin hành nghề</div>
            </div>
        </div>
        <div id="forte-block" class="block">
            <div class="block-heading">6. Sở trường hướng dẫn <span class="star">*</span></div>
            <div class="block-content">
                <div class="row">
                    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="type">Loại hình</label>
                        <select id="type" name="typeGuides" tabindex="<?= $position + 6; ?>"
                                class="{{ ($errors->has('typeGuides')) ? 'error-input' : '' }}">
                            <option value=""></option>
                            @foreach($typeGuides as $typeGuide)
                                <option value="{{ $typeGuide->id }}" {{ MemberRegisterHelper::getValueInputHasRelationView('typeGuides', 'typeGuideId', $objMember) == $typeGuide->id ? 'selected' : '' }}>{{$typeGuide->typeName}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('typeGuides'))
                            <span id="type-error" class="help-block">{{ $errors->first('typeGuides') }}</span>
                        @endif
                    </div>
                    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="strength">Quy mô đoàn</label>
                        <select id="strength" name="groupSizes" tabindex="<?= $position + 7; ?>"
                                class="{{ ($errors->has('groupSizes')) ? 'error-input' : '' }}">
                            <option value=""></option>
                            @foreach($groupSizes as $groupSize)
                                <option value="{{ $groupSize->id }}" {{ MemberRegisterHelper::getValueInputHasRelationView('groupSizes', 'groupSizeId', $objMember) == $groupSize->id ? 'selected' : '' }}>{{ $groupSize->groupSize }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('groupSizes'))
                            <span id="strength-error" class="help-block">{{ $errors->first('groupSizes') }}</span>
                        @endif
                    </div>
                    @if(!empty($objMem))
                        <?php //$objForteTour = $objMem->forteTour; ?>
						<?php 
							$objForteTour = explode(",", $objMem->forteTour);
							if(!empty($objForteTour)) { ?>
                        @foreach( $objForteTour as $key => $forte)
                            <div class="ol-forte-list {{ ($key == 0 ? '' : 'forte-ol') }}" data-forte-ol="{{ $key + 1 }}">
                                <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                                    <label for="forte{{ ($key == 0) ? '' : $key }}" style="width: auto;">Tuyến du lịch sở trường {{ ($key == 0) ? '1' : $key+1 }}</label>
                                    @if ($key != 0)
                                        <span class="delete-forte-field icon-close" style="font-size: 16px;color: #2481bf;cursor: pointer;float:right;"></span>
                                        <div style="clear:both;"></div>
                                    @endif
                                    <select id="forte{{ ($key == 0) ? '' : $key }}" name="forteTour[{{$key}}]" tabindex="<?= $position + 3; ?>"
                                            {{ ($key == 0) ? 'required' : '' }} class="{{ ($errors->has('forteTour[]')) ? 'error-input' : '' }}">
                                        <option value=""></option>
                                        @if (!empty($forteTour))
                                            @foreach($forteTour as $key => $value)
                                                <option value="{{ $value->id }}" {{ ($forte == $value->id) ? 'selected' : '' }}> {{ $value->name }} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('forteTour[]'))
                                        <span id="forte-error"
                                              class="help-block">{{ $errors->first('forteTour[]') }}</span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
							<?php } ?>
                    @elseif (!empty(old('forteTour') && count(old('forteTour')) > 1))
                        @foreach(old('forteTour') as $keyLoopOldWork => $forteTour)
                            <div class="{{ $keyLoopOldWork != 0 ? 'forte-ol' : '' }} ol-forte-list"
                                 data-forte-ol="{{ $keyLoopOldWork + 1 }}">
                                
                                <div class="dropfield field-wrap col-md-3 col-sm-2 col-xs-12">
                                    <label for="forte" style="width: auto;">Tuyến du lịch sở trường {{ $keyLoopOldWork+1 }} <span style="color: red;">*</span></label>
                                    @if ($keyLoopOldWork != 0)
                                        <span class="delete-forte-field icon-close" style="font-size: 16px;color: #2481bf;cursor: pointer;float:right;"></span>
                                        <div style="clear:both;"></div>
                                    @endif
                                    <select id="forte" required tabindex="<?= $position + 3; ?>"
                                            name="forteTour[{{$keyLoopOldWork}}]" {{ $keyLoopOldWork == 0 ? 'required' : '' }}>
                                        <option value=""></option>
                                        @if (!empty($forteTour))
                                        @foreach($forteTour as $key => $value)
                                                <option value="{{ $value->id }}" {{ (array_get(old('forteTour'), $keyLoopOldWork, '') == $value->id) ? 'selected' : '' }}> {{ $value->name }} </option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="ol-forte-list" data-forte-ol="1">
                            <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                            <label for="forte" style="width: auto;">Tuyến du lịch sở trường 1 <span style="color: red;">*</span></label>
                                <select id="forte" name="forteTour[0]" required tabindex="<?= $position + 3; ?>" class="{{ ($errors->has('forteTour[0]')) ? 'error-input' : '' }}" style="box-shadow:none">
                                    <option value=""></option>
                                    @if (!empty($forteTour))
                                        @foreach($forteTour as $key => $value)
                                            <option value="{{ $value->id }}" {{ (array_get(old('forteTour'), 0, '') == $value->id) ? 'selected' : '' }}> {{ $value->name }} </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('forteTour[0]'))
                                    <span id="forte-error"
                                          class="help-block">{{ $errors->first('forteTour[0]') }}</span>
                                @endif
                            </div>
                        </div>
                    @endif
                    <div class="add-forte-field">
                        <div class="field-wrap col-md-12 col-sm-12 col-xs-12">
                            Thêm tuyến du lịch sở trường (dành cho các HDV muốn tham dự vào Sàn giao dịch việc làm)
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="block-heading">7. Kinh nghiệm hướng dẫn <span class="star">*</span></div>
            <div class="block-content">
                <div class="row">
                    <div class="datetime field-wrap col-md-4">
                        <label for="exp">Năm bắt đầu hướng dẫn</label>
                        <input id="exp" tabindex="<?= $position + 8; ?>"
                               class="year datetime-input {{ ($errors->has('experienceYear')) ? 'error-input' : '' }}"
                               required type="text" maxlength="4" name="experienceYear"
                               value="{{old('experienceYear')}}">
                        <span class="datetime-icon fa fa-calendar"></span>
                        @if ($errors->has('experienceYear'))
                            <span id="exp-error" class="help-block">{{ $errors->first('experienceYear') }}</span>
                        @endif
                    </div>
                    <div class="dropfield field-wrap col-md-4 col-sm-6 col-xs-12">
                        <label for="seniority">Số năm kinh nghiệm</label>
                        <select id="seniority" name="experienceLevel" tabindex="<?= $position + 9; ?>"
                                class="{{ ($errors->has('experienceLevel')) ? 'error-input' : '' }}">
                            <option value=""></option>
                            @if (!empty($otherConstant))
                                @foreach($otherConstant as $key => $value)
                                    <option value="{{ $value->id }}" {{ (MemberRegisterHelper::getValueInputView('experienceLevel', $objMember) == $value->id) ? 'selected' : '' }}> {{ $value->otherName }} </option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors->has('experienceLevel'))
                            <span id="seniority-error" class="help-block">{{ $errors->first('experienceLevel') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="block-heading">8. Các kỹ năng chuyên môn khác (MC, Game show,...)</div>
            <div class="block-content">
                <div class="field-wrap">
                    <textarea name="otherSkills" rows="5" tabindex="<?= $position + 10; ?>" maxlength="300">{{ htmlentities(MemberRegisterHelper::getValueInputView('otherSkills', $objMember)) }}</textarea>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="block-heading">9. Thông tin khác</div>
            <div class="block-content">
                <div class="field-wrap">
                    <textarea name="otherInformation" tabindex="<?= $position + 11; ?>" maxlength="300"
                              rows="5">{{ htmlentities(MemberRegisterHelper::getValueInputView('otherInformation', $objMember)) }}</textarea>
                </div>
            </div>
        </div>
        <!-- <div class="block">
            <div class="block-heading">10. Hạng hướng dẫn viên</div>
            <div class="block-content">
                <ul class="radio-list choices has-options list" style="padding-left: 0px">
                    <div class="field-wrap">
                    
                        <li class="choice">
                            <input type="radio" name="majors">
                            <label style="font-weight: 100">Hạng 5 sao</label>
                        </li>
                        <li class="choice">
                            <input type="radio" name="majors">
                            <label style="font-weight: 100">Hạng 4 sao</label>
                        </li>
                        <li class="choice">
                            <input type="radio" name="majors">
                            <label style="font-weight: 100">Hạng 3 sao</label>
                        </li>
                        <li class="choice">
                            <input type="radio" name="majors">
                            <label style="font-weight: 100">Không xếp hạng</label>
                        </li>
                 
                    </div>
                </ul>
              
            </div>
        </div> -->
        <div id="achievement-block" class="block">
            <div class="block-heading">10. Thành tích (Khen thưởng, giấy khen, bằng khen,...)</div>
            <div class="block-content">
                <div class="field-wrap">
                    <textarea name="achievements" tabindex="<?= $position + 13; ?>" maxlength="300"
                              rows="5">{{ htmlentities(MemberRegisterHelper::getValueInputView('achievements', $objMember)) }}</textarea>
                </div>
                {{--<div class="upload-list">
                    <div class="btn-upload" data-auto-remove="1">
                        <label class="fa fa-upload" for="achievement1"> Tải lên các thành tích</label>
                        <input id="achievement1" type="file" name="achievementFiles[]">
                    </div>
                </div>--}}
            </div>
        </div>
        <div class="form-nav">
            <button type="submit" class="next-step btn-step" data-step="2" tabindex="<?= $position + 14; ?>">Tiếp theo</button>
        </div>
    </div>
@endsection

<style>
    #fileName {
        height: 0;
        visibility: hidden;
    }

    #typeOfTravelGuide-error {
        margin-bottom: 29px;
    }

    .error-input {
        box-shadow: rgb(206, 63, 56) 0px 0px 10px;
    }

    .help-block {
        font-size: 12px;
    }
</style>

@section('footer_embed')
    <script type="text/javascript" src="{{ asset('js/upload-file.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#upload-label').keydown(function(event){
                var keyCode = (event.keyCode ? event.keyCode : event.which);
                if (keyCode == 13) {
                    $('#upload-label').trigger('click');
                }
            });

            // show option when tab focus on select tag
            $('select').focus(function(){
                $(this).css('box-shadow', '0 0 3pt 2pt #719ECE');
            });
            $('select').blur(function(){
                $(this).css('box-shadow', 'none');
            });

            var selectAvatar = function (filename, $input) {
                var $parent = $input.parent(),
                    $label = $parent.find("label");
                $input.closest(".btn-upload").find(".help-block").remove();
                $label.removeClass("fa fa-upload");
                var currentText = $label.text();
                $label.text(filename);
                $('#fileName').val(filename);
                if ($parent.find(".spli.icon-close").length === 0) {
                    $parent.prepend("<span class='spli icon-close'></span>")
                        .find(".spli.icon-close")
                        .click(function () {
                            $parent.find("input[type=file]").val('');
                            $('#fileName').val('');
                            $parent.find("label")
                                .addClass("fa fa-upload")
                                .text(currentText);
                            $(this).remove();
                        });
                }
            }

            $(document).on('change', 'input[type=file]', function () {
                var filename = $(this)[0].files[0].name;
                selectAvatar(filename, $(this));
            });

            var userPhoto = "{{ !empty($objMember->profileImg->originalName) ? $objMember->profileImg->originalName : '' }}";
            if (userPhoto && userPhoto != "") {
                selectAvatar(userPhoto, $('#userPhoto'));
            }

            @if (!empty(MemberRegisterHelper::getBirthdayValueInputView('birthDate', 'd', $objMember)))
            $('.day').val('{{ MemberRegisterHelper::getBirthdayValueInputView('birthDate', 'd', $objMember) }}');
            @endif

            @if (!empty(MemberRegisterHelper::getBirthdayValueInputView('birthMonth', 'm', $objMember)))
            $('.month').val('{{ MemberRegisterHelper::getBirthdayValueInputView('birthMonth', 'm', $objMember) }}');
            @endif

            @if (!empty(MemberRegisterHelper::getBirthdayValueInputView('birthYear', 'Y' ,$objMember)))
            $('#year').val('{{ MemberRegisterHelper::getBirthdayValueInputView('birthYear', 'Y' ,$objMember) }}');
            @endif

            @if (!empty(MemberRegisterHelper::getValueDateTimeView('expirationDate', $objMember) ))
                $('#expireDate').val('{{ MemberRegisterHelper::getExpirationDate($objMember) }}');
            @endif

            @if (!empty(MemberRegisterHelper::getValueDateTimeView('dateIssued', $objMember)))
                $("#cmtDate").val('{{ MemberRegisterHelper::getDateIssued($objMember) }}');
            @endif

            @if (!empty(MemberRegisterHelper::getValueInputView('experienceYear', $objMember)))
                $("#exp").val('{{ MemberRegisterHelper::getValueInputView('experienceYear', $objMember) }}');
            @endif

            UploadFile.init();

            @if(!empty(MemberRegisterHelper::getValueInputView('profileImg', $objMember)))
                var fileNameUploaded = '{{ MemberRegisterHelper::getValueInputView('profileImg', $objMember) }}';
                $('#fileuploaded').text(fileNameUploaded.split('/').pop());
            @endif
        });
    </script>
@endsection
