<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/earlyaccess/droidarabickufi.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=vietnamese" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/homepage/css/style.css') }}?v=201712113"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/travel/css/skin-default.css') }}?v=201712112"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/blueimp-file-upload/css/jquery.fileupload.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/toastr/toastr.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.4.7.0.min.css') }}" />

    

    {{--@if (\Request::is('/') || \Request::is('gioithieu'))--}}
        <script type="text/javascript" src="{{ asset('theme/homepage/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('theme/homepage/js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('theme/homepage/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('theme/js/validator.js') }}"></script>
        <script type="text/javascript" src="{{ asset('theme/homepage/js/slick.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/constants.js') }}"></script>
        <script type="text/javascript" src="{{ asset('theme/homepage/js/scripts.js') }}"></script>
        <script type="text/javascript" src="{{ asset('theme/travel/js/simple-scrollbar.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendors/blueimp-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendors/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendors/blueimp-load-image/js/load-image.all.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendors/blueimp-canvas-to-blob/js/canvas-to-blob.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendors/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('theme/travel/js/jquery.js') }}"></script>
        <script src="{{ asset('js/bootstrap.3.3.7.min.js') }}"></script>
        <script src="{{ asset('theme/travel/js/jRespond.min.js') }}"></script>
    
        <script src="{{ asset('theme/travel/js/jquery.fitvids.js') }}"></script>
        <!--<script src="./js/wow.min.js"></script>-->

        <script src="{{ asset('theme/travel/js/simple-scrollbar.min.js') }}"></script>
        <script src="{{ asset('theme/travel/js/superfish.js') }}"></script>
        <script src="{{ asset('theme/travel/js/scrollIt.min.js') }}"></script>
       
        <script type="text/javascript" src="{{ asset('js/app.bak.js') }}?v=201812113"></script>
    {{--@endif--}}
    <style>
        .choices {
            z-index: 100;
        }
        .remove-padding {
            padding: 0;
        }
    </style>


    @yield('header_embed')
</head>
<body>
    <div id="full-container">
        @include('layouts.header')


        <div id="content" class="page-wrap" style="border-top: 1px solid #BBBABA">
            @yield('content')
        </div>

        @if (! \Request::is('/'))
        @include('layouts.footer')
        @endif

        @yield('footer_embed')
     </div>
</body>
</html>
