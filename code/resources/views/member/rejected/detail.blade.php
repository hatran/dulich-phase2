<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Constants\OtherConstants;
?>

@extends('layouts.public')
@section('title', 'Chi tiết thành viên: ' . $objMember->fullName)
@section('header_embed')
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/member/style.css') }}" />
@endsection
@section('content')

   
        <div class="heading">
            <span class="text-heading">Thông tin hồ sơ bị từ chối</span>
        </div>
        @include('admin.layouts.detail')
    
@endsection
<style>
    body .btn-view {
        margin-top: 15px;
    }
    .submit_button {
        background-color: #2b95cb;
        color: #fff;
        border: 0;
        text-transform: uppercase;
        height: 30px;
        line-height: 30px;
        padding: 0 15px;
        border-radius: 2px;
    }
    .help-block {
        color: #a94442;
        margin-top: 5px;
        margin-bottom: 10px;
    }

    .text-heading {
        font-size: 20px;
    }
    
    .page-wrap {
        padding-top: 58px;
    }

    /*body #header .page-wrap {
        width: 1140px;
    }
    body #header .user-buttons {
        margin-bottom: 10px;
    } 
    body #content .page-wrap {
        margin-bottom: 60px !important;
        width: 1140px;
    }
    body #header .page-wrap .top-bar {
        height: 10px;
        margin-top: 30px;
    }*/
    /*body .page-wrap .heading .text-heading {
        line-height: 35px;
        font-size: 18px;
        color: #2b95cb;
        margin-right: 15px;
        white-space: nowrap;
        flex-basis: 100%;
    }
    body .page-wrap .heading {
        height: 35px;
        padding: 10px;
        margin-top: 10px;
        border-left: 3px solid #2b95cb;
        display: flex;
        justify-content: flex-start;
        margin-bottom: 15px;
    }*/
    /*body #header {
        height: 80px !important;
    }
    body #header .user-buttons .page-wrap a {
        font-family: 'HarmoniaSansProCyr-SemiBd',sans-serif;
        font-size: 14px;
        color: #6c6b6b;
        text-transform: uppercase;
        padding: 5px 10px;
        border-right: 1px solid #fff;
        border-top: 0;
        display: inline-block;
        transition: .3s;
        -moz-transition: .3s;
        -webkit-transition: .3s;
        -o-transition: .3s;
        -ms-transition: .3s;
    }*/

</style>
