<?php
use App\Constants\MemberConstants;
use App\Constants\UserConstants;
use App\Constants\BranchConstants;
use App\Constants\OtherConstants;
?>


@extends('layouts.public')
@section('title', 'Chi tiết thành viên: ' . $objMember->fullName)
@section('header_embed')
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/member/style.css') }}" />
@endsection
@section('content')
        <div style="margin-top: 50px; font-size: 20px;">
            @if($objMember->status == MemberConstants::UPDATE_INFO_2)
                <div class="alert alert-success">
                    Hồ sơ của bạn đang chờ duyệt
                </div>
            @else
                @if (session('successes'))
                    <div class="alert alert-success">
                        <p>{{ session('successes') }}</p>
                    </div>
                @endif
            @endif            
        </div>
        @include('admin.layouts.detail')
        
        @if($objMember->status == MemberConstants::MEMBER_OFFICIAL_MEMBER)
        <div class="form-nav margin-bottom-50">
            <a href="{{ route('profile_edit') }}" class="btn btn-primary pull-right" style="margin-top: 20px !important">Cập Nhật</a>
        </div>
        @endif

@endsection
<style>
    body .btn-view {
        margin-top: 15px;
    }
    .submit_button {
        background-color: #2b95cb;
        color: #fff;
        border: 0;
        text-transform: uppercase;
        height: 30px;
        line-height: 30px;
        padding: 0 15px;
        border-radius: 2px;
    }
    .help-block {
        color: #a94442;
        margin-top: 5px;
        margin-bottom: 10px;
    }
    .margin-bottom-50 {
        margin-bottom: 105px;;
        padding: 0 15px;
    }

    a.btn.btn-primary {
        color: #FFFFFF;
        min-width: 120px;
        height: 35px;
        padding: 0 10px;
        line-height: 34px;
        text-align: center;
        cursor: pointer;
        border-radius: 3px;
        margin-left: 15px;
        transition: .3s;
        text-transform: uppercase;
        background-color: #2b95cb;
    }
</style>
