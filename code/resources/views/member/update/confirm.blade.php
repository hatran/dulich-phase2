<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none; ">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Bạn có muốn cập nhật thông tin hồ sơ?</h4>
            </div>
            <div class="modal-body">
                <div class="modal-footer">
                    <button type="button" class="btn btn-info edit">
                            <span class='glyphicon glyphicon-check'></span> Lưu
                        </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Thoát
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
