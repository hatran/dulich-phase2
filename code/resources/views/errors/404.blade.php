@extends((Auth::guest() || \App\Providers\UserServiceProvider::isNormalUser()) ? 'layouts.newdesign.public' : 'admin.layouts.app')
@section('title', 'Không tìm thấy trang này')
@section('content')
    @if (Auth::guest() || \App\Providers\UserServiceProvider::isNormalUser())
    <div class="parallax-section cta-title-1" data-scroll-index="1" data-parallax-bg-img="006.jpg" data-stellar-background-ratio="0.2" style="background-image: url({{ asset('images/banners/006.jpg') }});">
        <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
        <div class="section-inner">

        </div>
    </div>

    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2"><h1 class="darkblue">Không tìm thấy trang này</h1></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @else
        <div class="main-content">
            <div class="page-wrap">
                <h4 style="text-align: center">Trang này không tồn tại hoặc bạn không có quyền truy cập trang này, vui lòng liên hệ quản trị viên để biết thêm chi tiết.</h4>
            </div>
        </div>
    @endif
@endsection
