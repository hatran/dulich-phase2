@extends('layouts.newdesign.public')
@section('title', 'Trang chủ')
@section('content')
    <section id="banner">
        <div id="home" class="banner-parallax" data-scroll-index="0">
            <div class="banner-slider">
                <ul class="owl-carousel slider-img-bg">
                    @if (!empty($bannerName['home'])&& count($bannerName['home']) > 0)
                        @foreach ($bannerName['home'] as $value)
                            <li>
                                <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
                                <div class="overlay-pattern" data-bg-color="#000" data-bg-color-opacity="0"></div>
                                <div class="slide">
                                    <img src="{{ asset('images/banners/'.$value['profile_image']) }}">
                                    <div class="slide-content">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-0">
                                                    <div class="banner-center-box">
                                                        <h1>
                                                            {{ $value['content'] != '' ? $value['content'] : '' }}<?php if($value['content2'] != ''){ ?> <span class="colored"> <?php echo $value['content2'];?></span> <?php }?>
                                                        </h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @else
                        <li>
                            <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
                            <div class="overlay-pattern" data-bg-color="#000" data-bg-color-opacity="0"></div>
                            <div class="slide">
                                <img src="{{ asset('images/banners/001.jpg') }}">
                                <div class="slide-content">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-0">
                                                <div class="banner-center-box">
                                                    <h1>
                                                        Góp phần phát triển Du lịch Việt Nam<span class="colored"> trở thành ngành kinh tế mũi nhọn</span>
                                                    </h1>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endif

                </ul>
            </div>
        </div>

        @if (Auth::guest() || ! \App\Providers\UserServiceProvider::isNormalUser())
        <div class="login-box" id="login-box-homepage">
            <h4 class="title">Đăng nhập</h4>
            <form class="form frmLogin" action="{{ url('/login') }}" id="login" name="login" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <input placeholder="Tên truy cập" type="text" name="username" id="bill-number" class="form-control" required
                           autocomplete="off" value="{{ old('username') }}">
                </div>
                <div class="form-group">
                    <input placeholder="Mật khẩu" type="password" name="password" id="bill-code" class="form-control" required
                           autocomplete="off">
                </div>

                <div>
                    @if ($errors->has('captcha'))
                        <font style="color:#ffc527">
                            Mã xác nhận không hợp lệ
                        </font>
                    @elseif ($errors->has('username') || $errors->has('password'))
                        <font style="color:#ffc527">
                            Tên đăng nhập hoặc mật khẩu không đúng
                        </font>
                    @endif
                </div>
                <div class="form-group">
                    <table style="cellpadding:0; cellspacing:0">
                        <tr>
                            <td style="padding: 5px 8px">
                                <input placeholder="Nhập mã kiểm tra" name="captcha" id="bill-code-kt"
                                       class="form-control" autocomplete="off">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <table style="cellpadding:0; cellspacing:0">
                        <tr>
                            <td style="padding: 5px 8px">
                                <img id="captcha" src="{{ Captcha::src() }}">
                                <a href="#" style="float: right" onclick="reloadCaptcha(jQuery('#captcha'))">
                                    <img src="images/refresh-icon.png">
                                    <span class="validate" id=""></span>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <div class="checkbox choice">
                        <label><input id="remember-password" type="checkbox" name="remember">Ghi nhớ mật khẩu</label>
                        <a href="{{ route('password.request') }}" style="float: right; color: #FFF;">Quên mật khẩu</a>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn small colorful hover-transparent-colorful" id="submit">Đăng nhập</button>
                </div>
                <div class="form-group text-center"><label class="small">Hoặc</label></div>

                <input type="hidden" name="login_homepage" value="1">
                <div class="form-group">
                    <a href="{{ url('member/register') }}" class="btn small white hover-transparent-white"
                       style="width: 100%; line-height: 12px">
                        Đăng ký hội viên
                    </a>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
        @endif
    </section>

    <section id="content">
        <div id="content-wrap">
            <div id="intro" class="flat-section intro bg-grey" data-scroll-index="1">
                <div class="section-title">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>GIỚI THIỆU</h2>
                                <ul class="intro-tabs">
                                    <li><a href="/vechungtoi"><i class="fa fa-bank" aria-hidden="true"></i>Về chúng tôi</a></li>
                                    <li><a href="/tamnhinvasumenh"><i class="fa fa-flag" aria-hidden="true"></i>Tầm nhìn & Sứ mệnh</a></li>
                                    <li><a href="/quychehoatdong"><i class="fa fa-book" aria-hidden="true"></i>Quy chế hoạt động</a></li>
                                    <li><a href="/quydinh"><i class="fa fa-gavel" aria-hidden="true"></i>Quy định</a></li>
                                    <li><a href="/huongdandangky"><i class="fa fa-info" aria-hidden="true"></i>Hướng dẫn đăng ký</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($lastestNews->count())
            <div id="news" class="flat-section popular-packages" data-scroll-index="1">
                <div class="section-title">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>TIN TỨC NỔI BẬT</h2>
                                <div class="popular-packages-slider">
                                    <ul class="highlight-news text-left owl-carousel">
                                        @foreach ($lastestNews as $lastestNew)
                                        <li>
                                            <div class="box-preview">
                                                <div class="box-img img-bg">
                                                    <a href="{{url('/news/'.$lastestNew->id)}}">
                                                        <img src="{{ '/images/news/' . $lastestNew->thumnail_image }}">
                                                    </a>
                                                </div>
                                                <div class="box-content" style="max-height: 210px">
                                                    <h5><a href="{{url('/news/'.$lastestNew->id)}}">{{$lastestNew->title}}</a></h5>
                                                    <p>{{ htmlentities(str_limit($lastestNew->short_description,150,'....')) }}</p>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div id="offices" class="flat-section offices bg-grey" data-scroll-index="2">
                <div class="section-title">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>VĂN PHÒNG ĐẠI DIỆN</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row offices-container">
                        @if(!empty($bannerName['vpdd']))
                        @foreach ($bannerName['vpdd'] as $key => $value)
                        <div class="{{$key == 0 ? 'col-md-8' : 'col-md-4'}}">
                            <div class="office-item" onclick="location.href='/vanphongdaidien'">
                                <div class="preview img-bg">
                                    <img src="<?php echo (!empty($value['images'])) ? url('/files/offices').$value['images'] : 'images/offices/img-1.jpg' ?>" alt="">
                                </div>
                                <a class="overlay" href="/vanphongdaidien">
                                    <i class="fa fa-info"></i>
                                    <div class="overlay-inner">
                                        <h5>{{$value['name']}}</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="/vanphongdaidien"></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        {{--<div class="col-md-4">
                            <div class="office-item" onclick="location.href='/vanphongdaidien'">
                                <div class="preview img-bg">
                                    @if (!empty($bannerName['VPDD_HCM'])&& count($bannerName['VPDD_HCM']) > 0)
                                        <img src="{{ asset('images/banners/'.$bannerName['VPDD_HCM'][0]['profile_image']) }}" alt="">
                                    @else
                                        <img src="{{ asset('images/offices/img-2.jpg') }}" alt="">
                                    @endif
                                </div>
                                <a class="overlay" href="/vanphongdaidien">
                                    <i class="fa fa-info"></i>
                                    <div class="overlay-inner">
                                        <h5>TP. Hồ Chí Minh</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="/vanphongdaidien"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-item" onclick="location.href='/vanphongdaidien'">
                                <div class="preview img-bg">
                                    @if (!empty($bannerName['VPDD_DN'])&& count($bannerName['VPDD_DN']) > 0)
                                        <img src="{{ asset('images/banners/'.$bannerName['VPDD_DN'][0]['profile_image']) }}" alt="">
                                    @else
                                        <img src="{{ asset('images/offices/img-3.jpg') }}" alt="">
                                    @endif
                                </div>
                                <a class="overlay" href="/vanphongdaidien">
                                    <i class="fa fa-info"></i>
                                    <div class="overlay-inner">
                                        <h5>TP. Đà Nẵng</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="/vanphongdaidien"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-item" onclick="location.href='/vanphongdaidien'">
                                <div class="preview img-bg">
                                    @if (!empty($bannerName['VPDD_HCM'])&& count($bannerName['VPDD_HCM']) > 1)
                                        <img src="{{ asset('images/banners/'.$bannerName['VPDD_HCM'][1]['profile_image']) }}" alt="">
                                    @else
                                        <img src="{{ asset('images/offices/img-4.jpg') }}" alt="">
                                    @endif
                                </div>
                                <a class="overlay" href="/vanphongdaidien">
                                    <i class="fa fa-info"></i>
                                    <div class="overlay-inner">
                                        <h5>TP. Hồ Chí Minh</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="/vanphongdaidien"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-item" onclick="location.href='/vanphongdaidien'">
                                <div class="preview img-bg">
                                    @if (!empty($bannerName['VPDD_DN'])&& count($bannerName['VPDD_DN']) > 1)
                                        <img src="{{ asset('images/banners/'.$bannerName['VPDD_DN'][1]['profile_image']) }}" alt="">
                                    @else
                                        <img src="{{ asset('images/offices/img-5.jpg') }}" alt="">
                                    @endif
                                </div>
                                <a class="overlay" href="/vanphongdaidien">
                                    <i class="fa fa-picture-o"></i>
                                    <div class="overlay-inner">
                                        <h5>TP. Đà Nẵng</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="/vanphongdaidien"></a>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>

            <div id="info" class="flat-section info" data-scroll-index="3">
                <div class="section-title">
                    <div class="container swiper-container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>THÔNG TIN</h2>
                            </div>
                        </div>
                        <div class="row mt30 col-md-12 swiper-wrapper">
                            
                                @foreach ($information as $info)
                                    <div class="swiper-slide">
                                        <a href="{{ URL::route('front_sub_info_list', ['id' => $info->id]) }}">
                                            <div class="wrap-thumb">
                                                @if ($info->code == 'DLVN')
                                                    @if (!empty($bannerName['THONGTIN_DLVN'])&& count($bannerName['THONGTIN_DLVN']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_DLVN']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-1.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'LSVN')
                                                    @if (!empty($bannerName['THONGTIN_LSVN'])&& count($bannerName['THONGTIN_LSVN']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_LSVN']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-2.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'VHVN')
                                                    @if (!empty($bannerName['THONGTIN_VHVN'])&& count($bannerName['THONGTIN_VHVN']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_VHVN']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-3.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'DSVN')
                                                    @if (!empty($bannerName['THONGTIN_DSVN'])&& count($bannerName['THONGTIN_DSVN']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_DSVN']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-4.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'CTVN')
                                                    @if (!empty($bannerName['THONGTIN_CTVN'])&& count($bannerName['THONGTIN_CTVN']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_CTVN']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-5.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'PLVN')
                                                    @if (!empty($bannerName['THONGTIN_PLVN'])&& count($bannerName['THONGTIN_PLVN']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_PLVN']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-1.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'CSHT')
                                                    @if (!empty($bannerName['THONGTIN_CSHT'])&& count($bannerName['THONGTIN_CSHT']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_CSHT']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-2.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'LSVM')
                                                    @if (!empty($bannerName['THONGTIN_LSVM'])&& count($bannerName['THONGTIN_LSVM']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_LSVM']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-3.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'TQDL')
                                                    @if (!empty($bannerName['THONGTIN_TQDL'])&& count($bannerName['THONGTIN_TQDL']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_TQDL']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-4.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'DDDL')
                                                    @if (!empty($bannerName['THONGTIN_DDDL'])&& count($bannerName['THONGTIN_DDDL']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_DDDL']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-5.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'SLTK')
                                                    @if (!empty($bannerName['THONGTIN_SLTK'])&& count($bannerName['THONGTIN_SLTK']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_SLTK']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-1.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'BHDM')
                                                    @if (!empty($bannerName['THONGTIN_BHDM'])&& count($bannerName['THONGTIN_BHDM']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_BHDM']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-2.jpg') }}" class="thumb">
                                                    @endif
                                                @elseif ($info->code == 'TDKN')
                                                    @if (!empty($bannerName['THONGTIN_TDKN'])&& count($bannerName['THONGTIN_TDKN']) > 0)
                                                        <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_TDKN']['profile_image']) }}" alt="" class="thumb">
                                                    @else
                                                        <img src="{{ asset('images/infos/img-3.jpg') }}" class="thumb">
                                                    @endif
                                                @endif
                                                <span class="info">{{ $info->name }}</span></a>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach 
                                <!-- <div class="col-xs-12 col-sm-4 p0">
                                    <div class="wrap-thumb"><a href="{{ URL::route('front_info_trangphuc') }}">
                                            @if (!empty($bannerName['THONGTIN_TP'])&& count($bannerName['THONGTIN_TP']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_TP']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-5.jpg') }}" class="thumb">
                                            @endif
                                                <span class="info">Trang phục</span></a></div>
                                </div><div class="col-xs-12 col-sm-4 p0">
                                    <div class="wrap-thumb"><a href="{{ URL::route('front_info_lehoi') }}">
                                            @if (!empty($bannerName['THONGTIN_LH'])&& count($bannerName['THONGTIN_LH']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_LH']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-7.jpg') }}" class="thumb">
                                            @endif
                                                <span class="info">Lễ hội</span></a></div>
                                </div><<div class="col-xs-12 col-sm-4 p0">
                                    <div class="wrap-thumb"><a href="{{ URL::route('front_info_diemden') }}">
                                            @if (!empty($bannerName['THONGTIN_DD'])&& count($bannerName['THONGTIN_DD']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_DD']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-4.jpg') }}" class="thumb">
                                            @endif
                                            <span class="info">Điểm đến</span></a></div>
                                </div><div class="col-xs-12 col-sm-4 p0">
                                    <div class="wrap-thumb"><a href="{{ URL::route('front_info_traffic') }}">
                                            @if (!empty($bannerName['THONGTIN_GT'])&& count($bannerName['THONGTIN_GT']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_GT']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-6.jpg') }}" class="thumb">
                                            @endif
                                                <span class="info">Giao thông</span></a></div>
                                </div> -->
                        </div>
                        <div class="swiper-pagination" style="position: relative;"></div>
                    </div>
                </div>
            </div>
        
    </section>
@endsection

@section('footer_embed')
<link rel="stylesheet" type="text/css" href="{{ asset('css/swiper.min.css') }}">
<script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        slidesPerColumn: 2,
        spaceBetween: 30,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        loopAdditionalSlides:3,
    });
</script>
<script>
    window.onload = function () {
        jQuery("#login").submit(function (e) {
            e.preventDefault();
            login();
        });
    };

    var login = function () {
        var url = "<?php echo e(url('/login')); ?>";
        jQuery.ajax({
            type: "POST",
            url: url,
            dataType: "text",
            data: jQuery("#login").serialize(),
            success: function (data) {
                if(data == 'true'){
                    window.location.replace("<?php echo e(url('/redirect')); ?>");
                } else jQuery('#login-box-homepage').html(data);
            },
            error: function (data) {
                var errors = '';
                var data2 = jQuery.parseJSON(data.responseText);
                if(data2.errors.captcha != null) errors = errors + data2.errors.captcha[0] + '<br/>';
                if(data2.errors.password != null) errors = errors + data2.errors.password[0] + '<br/>';
                if(data2.errors.username != null) errors = errors + data2.errors.username[0] + '<br/>';
                jQuery('#errors').html(errors);
            }
        });
    };
</script>
@endsection
