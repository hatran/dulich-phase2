<div class="col-heading">Bài viết gần đây</div>
<div class="recent-news">
    <a href="#">
        <img src="{{ asset('images/recent-news.jpg') }}"/>
    </a>
    <a href="#">
        <p class="post-title">Hội chợ Du lịch Quốc tế Việt Nam – thương hiệu đã được khẳng định</p>
        <p class="post-date">4/12/2017  /  15:00:00</p>
    </a>
</div>
