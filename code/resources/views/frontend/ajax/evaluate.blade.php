<?php
    use App\Models\Branches;
    use App\Models\MemberRank;
    use App\Constants\MemberConstants;
    use App\Models\Member;
?>
<div class="container not-partner">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="cta-title-2">
                <h1 class="darkblue">Đánh giá, cho điểm HDV của công ty du lịch lữ hành</h1>
            </div>
            <div>
                <div class="contentDetail">
                    @if(session('successes'))
                        <div id="closeButton" class="alert alert-success">
                            <ul>
                               
                                    <li>{{ session('successes') }}</li>
                              
                            </ul>
                        </div>
                    @endif
                    <form class="full-width" id="member-search" name="member-search" method="post"
                          onsubmit="memberSubmitFunction(event)">
                        <input type="hidden" name="check" value="1" id="hidden-check" />
                        <div class="row mb20">
                            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="name2">Họ và tên</label>
                                <input id="name2" name="name" type="text"
                                       value="{{$request['name']  or '' }}">
                            </div>
                            <div class="dropfield field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="type2">Chi hội</label>
                                <select id="type2" name="type" onchange="typeClick()">
                                    <option value="">Chọn chi hội</option>
                                    <?php foreach($objBranche as $key => $branche): ?>
                                    <?php if($branche['status'] == 1):?>
                                    <option value="<?php echo $branche['id'] ?>" <?php echo (!empty($request['type']) && $request['type'] == $branche['id']) ? 'selected' : '' ?>><?php echo $branche['name'] ?></option>
                                    <?php endif ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="dropfield field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="active">CLB thuộc hội</label>
                                <select id="active" name="active" onchange="activeClick()">
                                    <option value="">Chọn clb thuộc hội</option>
                                    <?php foreach ($objClub as $key => $club): ?>
                                    <option value="{{$club['id']}}" <?php echo (!empty($request['type']) && $request['active'] == $club['id']) ? 'selected' : '' ?>>{{$club['name']}}</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="row mb20">
                            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="id2">Số thẻ hội viên</label>
                                <input id="id2" name="id2" type="text"
                                       value="{{$request['id2']  or '' }}"/>
                            </div>
                            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="id3">Số thẻ HDV</label>
                                <input id="id3" name="id3" type="text"
                                       value="{{$request['id3']  or '' }}"/>
                            </div>
                            <div class="dropfield field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="language2">Ngôn ngữ hướng dẫn</label>
                                <select id="language2" name="language">
                                    <option value="">Chọn ngôn ngữ</option>
                                    @foreach($languages as $value)
                                        <option value="{{$value->id}}" <?php echo (!empty($request['language']) && $request['language'] == ($value->id)) ? 'selected' : '' ?>>{{$value->languageName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="id4">Mã xác nhận</label>
                                <input id="id4" name="captcha" type="text"
                                       value="{{$request['captcha']  or '' }}" required>
                                @if(empty($members) and $total == 0)
                                    <p style="color: #ff0000;">Mã không chính xác!</p>
                                @endif

                            </div>
                            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                <div class="fs20 mt16" style="margin-top: 33px !important">
                                    <label for="id4"></label>
                                    <input type="hidden" name="_token" value="{{csrf_token()}}" id="hidden-token" />
                                    <input type="hidden" name="tmp" value="1" />
                                    <img id="captcha" src="{{Captcha::src()}}">
                                    <a nohref onclick="reloadCaptcha(jQuery('#captcha'))" style="
    							left: 10px;
    							position:  relative;
						">
                                        <img src="images/refresh-icon.png">
                                        <span class="validate" id=""></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="field-wrap pt30 text-center">
                            <input class="btn" name="check" type="submit" value="Tìm hội viên"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if(!empty($members))
            <div class="col-md-12">
                <h4 class="pl15 mt30">
                    @if (!empty($total))
                        Danh sách hội viên - có {{ $total }} thành viên.
                    @else
                        Không có bản ghi nào phù hợp theo điều kiện tìm kiếm.
                    @endif
                </h4>
                <div class="block-list mt10 box-preview">
                    @foreach($members as $member)
                        <div class="col-sm-6 mt20">
                            <div class="flex-box box-list">
                                <div class="avatar">
                                    @if (!empty($member->profileImg))
                                        <img src="{{ $member->profileImg }}"/>
                                    @else
                                        <img src="./images/3_4.jpg"/>
                                    @endif
                                    <a href="{{ route('company_create_evaluation', ['id' => $member->id]) }}" class="btn btn-primary company-evaluate-button">Đánh giá HDV</a>
                                </div>
                                <div class="inform">
                                    <table width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td style="width: 58%; vertical-align: top;">1. Họ tên</td>
                                            <td>
                                                <div style="overflow: hidden; height:auto;">{{$member->fullName}}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2. Số thẻ HDV</td>
                                            <td>{{$member->touristGuideCode}}</td>
                                        </tr>
                                        <tr>
                                            <td>3. Số thẻ hội viên</td>
                                            <td>{{$member->member_code}}</td>
                                        </tr>
                                        <tr>
                                            <td>4. Ngày sinh</td>
                                            <td>{{date('d/m/y',strtotime($member->birthday))}}</td>
                                        </tr>
                                        <tr>
                                            <td>5. Ngôn ngữ hướng dẫn</td>
                                            <td>@php
                                                    $language = $member->guideLanguage;
                                                @endphp
                                                <?php $languageName = ""; ?>
                                                @foreach(explode(",", $language) as $key => $lang)
                                                    @foreach($languages as $data)
                                                        @if($data->id == $lang)
                                                            <?php $languageName .= $data->languageName ."," ?>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                                {{ rtrim($languageName, ",") }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>6. Chi hội (Nơi ĐK sinh hoạt)</td>
                                            <td>
                                                <div style="overflow: hidden; height: 63px;">
                                                <?php
                                                    if (Branches::getBranches01Province($member->province_code) != "") {
                                                        $branch = Branches::getBranches01Province($member->province_code);
                                                        foreach($branch as $key => $item) {
                                                            if ($key == "BRANCHES03") {
                                                                echo "Câu lạc bộ ".$item;
                                                            }
                                                            else {
                                                                echo "Chi hội ".$item;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>7. Hội viên VTGA</td>
                                            <td>{{date('d/m/y',strtotime($member->member_from))}}</td>
                                        </tr>
                                        <tr>
                                            <td>8. Hạng HDV</td>
                                            <td>
                                                <?php
                                                $rankDisplayed = MemberConstants::$rank_status[2];
                                                $latestRankMember = MemberRank::getLatestRankByMemberId($member->id);
                                                ?>
                                                @if ($latestRankMember)
                                                    <?php $rankIsValid = Member::checkRankConditionMember($latestRankMember->member_id, $latestRankMember->rank_id); ?>
                                                    @if ($rankIsValid)
                                                        <?php $rankDisplayed = $ranks[$latestRankMember->rank_id]; ?>
                                                    @endif
                                                @endif
                                                {{ $rankDisplayed }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <div class="mt30 text-center paginate">
        @if(!empty($members))
            {{$members->appends($request)->links()}}
        @endif
    </div>
</div>
<style>
    .company-evaluate-button {
        margin-top: 10px;
        font-size: 12px;
        padding: 17px 7px;
        border: unset;
        background: #0355B3;
    }
</style>