<?php
    use App\Models\Branches;
    use App\Models\MemberRank;
    use App\Constants\MemberConstants;
    use App\Models\Member;
?>
<div class="container not-partner">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="cta-title-2">
                <h1 class="darkblue">Hội viên</h1>
            </div>
            <div>
                <div class="contentDetail">
                    <form class="full-width" id="member-search" name="member-search" method="post"
                          onsubmit="memberSubmitFunction(event)">
                        <input type="hidden" name="check" value="1" id="hidden-check" />
                        <div class="row mb20">
                            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="name2">Họ và tên</label>
                                <input id="name2" name="name" type="text"
                                       value="{{$request['name']  or '' }}">
                            </div>
                            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="id2">Số thẻ hội viên</label>
                                <input id="id2" name="id2" type="text"
                                       value="{{$request['id2']  or '' }}"/>
                            </div>
                            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                <label for="id3">Số thẻ HDV</label>
                                <input id="id3" name="id3" type="text"
                                       value="{{$request['id3']  or '' }}"/>
                            </div>
                        </div>

                        <div class="field-wrap pt30 text-center">
                            <input class="btn" name="check" type="submit" value="Tìm hội viên"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @if(!empty($members))
            <div class="col-md-12">
                <h4 class="pl15 mt30">
                    @if (!empty($total))
                        Danh sách hội viên - có {{ $total }} thành viên.
                    @else
                        Không có bản ghi nào phù hợp theo điều kiện tìm kiếm.
                    @endif
                </h4>
                <div class="block-list mt10 box-preview">
                    @foreach($members as $member)
                        <div class="col-sm-6 mt20">
                            <div class="flex-box box-list">
                                <div class="avatar">
                                    @if (!empty($member->profileImg))
                                        <img src="{{ $member->profileImg }}"/>
                                    @else
                                        <img src="./images/3_4.jpg"/>
                                    @endif
                                </div>
                                <div class="inform">
                                    <table width="100%" border="0">
                                        <tbody>
                                        <tr>
                                            <td style="width: 58%; vertical-align: top;">Họ tên</td>
                                            <td>
                                                <div style="overflow: hidden; height:auto;">{{$member->fullName}}</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Số thẻ HDV</td>
                                            <td>{{$member->touristGuideCode}}</td>
                                        </tr>
                                        <tr>
                                            <td>Số thẻ hội viên</td>
                                            <td>{{$member->member_code}}</td>
                                        </tr>
                                        <tr>
                                            <td>Ngày sinh</td>
                                            <td>{{date('d/m/Y',strtotime($member->birthday))}}</td>
                                        </tr>
                                        <tr>
                                            <td>Ngôn ngữ hướng dẫn</td>
                                            <td>@php
                                                    $language = $member->guideLanguage;
                                                @endphp
                                                <?php $languageName = ""; ?>
                                                @foreach(explode(",", $language) as $key => $lang)
                                                    @foreach($languages as $data)
                                                        @if($data->id == $lang)
                                                            <?php $languageName .= $data->languageName ."," ?>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                                {{ rtrim($languageName, ",") }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chi hội (Nơi ĐK sinh hoạt)</td>
                                            <td>
                                                <div style="overflow: hidden; height: 63px;">
                                                <?php
                                                    if ($branch = Branches::getBranches01Province($member->province_code)) {
                                                        foreach($branch as $key => $item) {
                                                            if ($key == "BRANCHES03") {
                                                                echo "Câu lạc bộ ".$item;
                                                            }
                                                            else {
                                                                echo "Chi hội ".$item;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Hạn thẻ hội viên</td>
                                            <td>{{date('d/m/Y',strtotime($member->member_code_expiration))}}</td>
                                        </tr>
                                        <tr>
                                            <td>Hạng HDV</td>
                                            <td>
                                                <?php
                                                $rankDisplayed = MemberConstants::$rank_status[2];
                                                $latestRankMember = MemberRank::getLatestRankByMemberId($member->id);
                                                ?>
                                                @if ($latestRankMember)
                                                    <?php $rankIsValid = Member::checkRankConditionMember($latestRankMember->member_id, $latestRankMember->rank_id); ?>
                                                    @if ($rankIsValid)
                                                        <?php $rankDisplayed = $ranks[$latestRankMember->rank_id]; ?>
                                                    @endif
                                                @endif
                                                {{ $rankDisplayed }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <div class="mt30 text-center paginate">
        @if(!empty($members))
            {{$members->appends($request)->links()}}
        @endif
    </div>
</div>
