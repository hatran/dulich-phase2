@extends('layouts.newdesign.public')
@section('title', 'Cập nhật Mật Khẩu Người Dùng')
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .has-error .control-label {
        color: #3D3A3A !important
    }

    #fix-a {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
        width: 20%;
    }

    #fix-button {
        height: 34px;
        line-height: 34px;
        font-size: 14px;
        font-weight: normal;
    }

    #fix-button:hover {
        background-color: #265a88;
        background-position: 0 -15px;
    }
</style>
@section('content')
    <div class="page-wrap" style="margin-top: 45px; height: 350px;">
        <div class="panel panel-default" style="margin-top: 20px; border-radius: 0 !important;">
            <div class="panel panel-heading" style="margin-bottom: 0px;">Thay đổi mật khẩu</div>
            <div class="panel-body">
                @if(session('successes'))
                    <div id="closeButton" class="alert alert-success">
                        <button type="button" class="close" aria-label="Close" style="width: auto;height: auto;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach (session('successes') as $success)
                                <li>{{ $success }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="form-horizontal reset-form" method="POST" action="{{ route('user_change_password') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Mật khẩu cũ <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="old_password" type="password" class="form-control" name="old_password">

                            @if ($errors->has('old_password'))
                                <span class="help-block"> {{ $errors->first('old_password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Mật khẩu mới <span class="color-required">*</span></label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block"> {{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-4 control-label">Xác nhận mật
                            khẩu mới <span class="color-required">*</span></label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <input type="submit" value="Đổi mật khẩu" class="btn btn-primary" id="fix-button"> 
                            <a href="{{URL::route('front_end_index')}}" class="btn btn-warning" id="fix-a" style="color: #FFFFFF;">Thoát</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
