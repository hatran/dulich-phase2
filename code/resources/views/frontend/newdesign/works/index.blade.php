@extends('layouts.newdesign.public')
@section('title', 'Sàn Giao Dịch Việc Làm')
@section('content')

    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">

        <div class="section-content">
            <div class="container not-partner">
                @include('layouts.newdesign.slide')
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2"><h1 class="darkblue">{!! $data['title'] !!}</h1></div>
                        <div class="main-content" style="word-wrap: break-word;">
                            {!! $data['content'] !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
