@extends('layouts.newdesign.public')
@section('title', 'CLB Thuộc Hội')
@section('content')
<style>
    #clb_name,#search_guideLanguage{line-height: 100%}
    .error-list{list-style: none}
</style>
@include('layouts.newdesign.banner')
<div id="coached" class="flat-section coached" data-scroll-index="6">
    <div class="section-content">
        <div class="container not-partner">
            <div class="row">
                <div class="col-md-12">
                    <div class="cta-title-2">
                        <h1 class="darkblue">Câu lạc bộ thuộc Hội</h1>
                    </div>
                    <div class="clb-content">
                        <div class="">
                            @include('frontend.newdesign.clubofhead.search')
                            @if (!empty($currentClb))
                            <div class="row"  style="margin-top:50px;">
                                <div class="tab-head"  uib-dropdown>
                                    <ul id="intro_tabs" class="intro_tabs nav nav-tabs nav-justified nav-pills">
                                        <li class="active">
                                            <a data-toggle="tab" href="#content-tab1">Thông Tin Câu lạc bộ thuộc Hội</a>
                                        </li>
                                        <li><a data-link="{{URL::route('front_end_ajax_get_news', ['id' => $currentClb, 'type' => $type])}}" onclick="tabContentAjax(event)" data-toggle="tab" href="#content-tab2">Tin tức sự kiện</a></li>
                                        <li><a data-toggle="tab" href="#content-tab3">&nbsp</a></li>
                                    </ul>
                                </div>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane fade active in" id="content-tab1">
                                        <div class="p30">
                                            <div class="">
                                                <div class="cta-title-2">
                                                    <h4 class="text-uppercase darkblue">Ban chấp hành</h4>
                                                </div>
                                                <div>
                                                    @include('frontend.newdesign.clubofhead.list_manager')
                                                </div>
                                            </div>
                                            <hr class="pt20 pb20">
                                            <div class="mt30">
                                                <div class="cta-title-2">
                                                    <h4 class="text-uppercase darkblue">Hội viên</h4>
                                                </div>
                                                <div>
                                                    @include('frontend.newdesign.clubofhead.search_hoivien')
                                                    @include('frontend.newdesign.clubofhead.list_member')
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="content-tab2"></div>
                                    <div class="tab-pane fade " id="content-tab3"></div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
