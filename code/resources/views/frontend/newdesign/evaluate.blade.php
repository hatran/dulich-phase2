<?php
    use App\Models\Branches;
    use App\Models\MemberRank;
    use App\Constants\MemberConstants;
    use App\Models\Member;
?>
@extends('layouts.newdesign.public')
@section('title', 'Đánh giá, cho điểm HDV của công ty du lịch lữ hành')
@section('content')
    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2">
                            <h1 class="darkblue">Đánh giá, cho điểm HDV của công ty du lịch lữ hành</h1>
                        </div>
                        <div>
                            <div class="contentDetail">
                                @if(session('successes'))
                                    <div id="closeButton" class="alert alert-success">
                                        <ul>
                                            @foreach (session('successes') as $suc)
                                                <li>{{ $suc }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form class="full-width" id="member-search" name="member-search" method="post"
                                      onsubmit="memberSubmitFunction(event)">
                                    <input type="hidden" name="check" value="1" id="hidden-check"/>
                                    <div class="row mb20">
                                        <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                            <label for="name2">Họ và tên</label>
                                            <input id="name2" name="name" type="text"
                                                   value="{{$request['name']  or '' }}">
                                        </div>
                                        <div class="dropfield field-wrap col-md-4 col-sm-4 col-xs-12">
                                            <label for="type2">Chi hội</label>
                                            <select id="type2" name="type" onchange="typeClick()">
                                                <option value="">Chọn chi hội</option>
                                                <?php foreach($objBranche as $key => $branche): ?>
                                                <?php if($branche['status'] == 1):?>
                                                <option value="<?php echo $branche['id'] ?>" <?php echo (!empty($request['type']) && $request['type'] == $branche['id']) ? 'selected' : '' ?>><?php echo $branche['name'] ?></option>
                                                <?php endif ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="dropfield field-wrap col-md-4 col-sm-4 col-xs-12">
                                            <label for="active">CLB thuộc hội</label>
                                            <select id="active" name="active" onchange="activeClick()">
                                                <option value="">Chọn clb thuộc hội</option>
                                                <?php foreach ($objClub as $key => $club): ?>
                                                <option value="{{$club['id']}}" <?php echo (!empty($request['active']) && $request['active'] == $club['id']) ? 'selected' : '' ?>>{{$club['name']}}</option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb20">
                                        <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                            <label for="id2">Số thẻ hội viên</label>
                                            <input id="id2" name="id2" type="text"
                                                   value="{{$request['id2']  or '' }}"/>
                                        </div>
                                        <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                            <label for="id3">Số thẻ HDV</label>
                                            <input id="id3" name="id3" type="text"
                                                   value="{{$request['id3']  or '' }}"/>
                                        </div>
                                        <div class="dropfield field-wrap col-md-4 col-sm-4 col-xs-12">
                                            <label for="language2">Ngôn ngữ hướng dẫn</label>
                                            <select id="language2" name="language">
                                                <option value="">Chọn ngôn ngữ</option>
                                                @foreach($languages as $value)
                                                    <option value="{{$value->id}}" <?php echo (!empty($request['language']) && $request['language'] == ($value->id)) ? 'selected' : '' ?>>{{$value->languageName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                            <label for="id4">Mã xác nhận</label>
                                            <input id="id4" name="captcha" type="text"
                                                   value="{{$request['captcha']  or '' }}" required>
                                            @if(empty($members) and $total == 0)
                                                <p style="color: #ff0000;">Mã không chính xác!</p>
                                            @endif

                                        </div>
                                        <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                                            <div class="fs20 mt16" style="margin-top: 33px !important;">
                                                <label for="id4"></label>
                                                <input type="hidden" name="_token" value="{{csrf_token()}}" id="hidden-token">
                                                <img id="captcha" src="{{Captcha::src()}}">
                                                <a nohref onclick="reloadCaptcha(jQuery('#captcha'))" style="
    							left: 10px;
    							position:  relative;
						">
                                                    <img src="images/refresh-icon.png">
                                                    <span class="validate" id=""></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-wrap pt30 text-center">
                                        <input class="btn" name="check" type="submit" value="Tìm Hội viên"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @if(!empty($members))
                        <div class="col-md-12">
                            <h4 class="pl15 mt30"> <?php echo (!empty($total)) ? 'Danh sách hội viên - có ' . $total . ' thành viên.' : 'Không có hội viên phù hợp' ?> </h4>
                            <div class="block-list mt10 box-preview">
                                @foreach($members as $member)
                                    <div class="col-sm-6 mt20">
                                        <div class="flex-box box-list">
                                            <div class="avatar">
                                                @if (!empty($member->profileImg))
                                                    <img src="{{ $member->profileImg }}"/>
                                                @else
                                                    <img src="./images/3_4.jpg"/>
                                                @endif
                                                <a href="{{ route('company_create_evaluation', ['id' => $member->id]) }}" class="btn btn-primary">Đánh giá HDV</a>
                                            </div>
                                            <div class="inform">
                                                <table width="100%" border="0">
                                                    <tbody>
                                                    <tr>
                                                        <td style="width: 58%; vertical-align: top;">1. Họ tên</td>
                                                        <td>
                                                            <div style="overflow: hidden; height:auto;">{{ htmlentities($member->fullName) }}</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2. Số thẻ HDV</td>
                                                        <td>{{ $member->touristGuideCode }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3. Số thẻ hội viên</td>
                                                        <td>{{ $member->member_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4. Ngày sinh</td>
                                                        <td>{{date('d/m/Y',strtotime($member->birthday))}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5. Ngôn ngữ hướng dẫn</td>
                                                        <td>@php
                                                                $language = $member->guideLanguage;
                                                            @endphp
                                                            <?php $languageName = ""; ?>
                                                            @foreach(explode(",", $language) as $key => $lang)
                                                                @foreach($languages as $data)
                                                                    @if($data->id == $lang)
                                                                        <?php $languageName .= $data->languageName ."," ?>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                            {{ htmlentities(rtrim($languageName, ",")) }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>6. Chi hội (Nơi ĐK sinh hoạt)</td>
                                                        <td>
                                                            <div style="overflow: hidden; height: 63px;">
                                                            <?php
                                                                if (Branches::getBranches01Province($member->province_code) != "") {
                                                                    $branch = Branches::getBranches01Province($member->province_code);
                                                                    foreach($branch as $key => $item) {
                                                                        if ($key == "BRANCHES03") {
                                                                            echo "Câu lạc bộ ".$item;
                                                                        }
                                                                        else {
                                                                            echo "Chi hội ".$item;
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>7. Hội viên VTGA</td>
                                                        <td>{{ date('d/m/y',strtotime($member->member_from)) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>8. Hạng HDV</td>
                                                        <td>
                                                            <?php
                                                            $rankDisplayed = MemberConstants::$rank_status[2];
                                                            $latestRankMember = MemberRank::getLatestRankByMemberId($member->id);
                                                            ?>
                                                            @if ($latestRankMember)
                                                                <?php $rankIsValid = Member::checkRankConditionMember($latestRankMember->member_id, $latestRankMember->rank_id); ?>
                                                                @if ($rankIsValid)
                                                                    <?php $rankDisplayed = $ranks[$latestRankMember->rank_id]; ?>
                                                                @endif
                                                            @endif
                                                            {{ $rankDisplayed }}
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
                <div class="mt30 text-center paginate">
                    @if(!empty($members))
                        {{htmlentities($members->appends($request)->links())}}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_embed')
    <script>
        var typeClick = function () {
            jQuery('#active option').prop('selected', function () {
                return this.defaultSelected;
            });
        };

        var activeClick = function () {
            jQuery('#type option').prop('selected', function () {
                return this.defaultSelected;
            });
        };

        function memberSubmitFunction(evt) {
            jQuery("#member-search input[type=submit]").attr("disabled", "disabled");
            evt.preventDefault();
            var url = "<?php echo e(url('/company/evaluate')); ?>";
            var urlParams = jQuery('#member-search').serialize();
            var urlWithParams = url + "?" + urlParams;
            console.log(urlParams);
            jQuery.ajax({
                type: "GET",
                url: url,
                dataType: "text",
                data: jQuery("#member-search").serialize(),
                success: function (data) {
                    jQuery('.section-content').html(data);
                    ChangeUrl('Hội Viên', urlWithParams);
                    function tFunction() {
                        jQuery('.pagination > li > a').click(function() {
                            var dom = jQuery(this).attr("href");
                           
                            var objPage = getAllUrlParams(dom);
                            
                            jQuery("#hidden-check").val("");
                            jQuery("#hidden-token").val("");
                            var page_click = jQuery(this).text();
                            if (jQuery(this).attr("rel")) {
                                if (jQuery(this).attr("rel") == 'next') {
                                    page_click = parseInt(jQuery('.pagination > .active').text()) + 1;
                                }
                                else {
                                    page_click = jQuery('.pagination > .active').text() - 1;
                                }
                            }
                            
                            var str = "?";
                            if (typeof objPage.name != 'undefined') {
                                var name2 = decodeURI(objPage.name).replace(/\+/g, ' ');
                                str += "name="+name2+"&";
                                jQuery("#name2").val(name2);
                            }
                            else {
                                name2 = "";
                                jQuery("#name2").val("");
                            }
                            var id2 = objPage.id2;
                            var id3 = objPage.id3;
                            var type2 = objPage.type;
                            var active = objPage.active;
                            var language2 = objPage.language;


                            if (typeof id2 != 'undefined') {
                                str += "id2="+id2+"&";
                                jQuery("#id2").val(id2);
                            }
                            else {
                                id2 = "";
                                jQuery("#id2").val("");
                            }

                            if (typeof id3 != 'undefined') {
                                str += "id3="+id3+"&";
                                jQuery("#id3").val(id3);
                            }
                            else {
                                id3 = "";
                                jQuery("#id3").val("");
                            }

                            if (typeof type2 != 'undefined') {
                                str += "type="+type2+"&";
                                jQuery("#type2 option[value="+type2+"]").attr('selected','selected');
                            }
                            else {
                                type2 = "";
                                jQuery('#type2 option[value=""]').attr('selected','selected');
                            }

                            if (typeof active != 'undefined') {
                                str += "active="+active+"&";
                                jQuery('#active option[value='+active+']').attr('selected','selected');
                            }
                            else {
                                active = "";
                                jQuery('#active option[value=""]').attr('selected', 'selected');
                            }

                            if (typeof language2 != 'undefined') {
                                str += "language="+language2+"&";
                                jQuery('#language2 option[value='+language2+']').attr('selected','selected');
                            }
                            else {
                                language2 = "";
                                jQuery('#language2 option[value=""]').attr('selected', 'selected');
                            }
                            
                            str += "page="+page_click+"&";
                            str = str.substring(0, str.length - 1);
                            var check = "";
                            var captcha = "";
                            var token = "";
                            var url1 = "<?php echo e(url('/company/evaluate')) ?>"+str;
                            jQuery.ajax({
                                type: "GET",
                                url: url1,
                                dataType: "text",
                                data: {check: check, name: name2, type: type2, active: active, id2: id2, id3: id3, language: language2, captcha: captcha, tmp: 1},
                                success: function (data1) {
                                    jQuery('.section-content').empty();
                                    jQuery('.section-content').html(data1);
                                    tFunction();
                                },
                                error: function (data1) {
                                }
                            });
                            return false;
                        });
                    };
                    tFunction();
                },
                error: function (data) {
                }
            });
            return false;
        }

        function ChangeUrl(page, url) {
            if (typeof (history.pushState) != "undefined") {
                var obj = {Page: page, Url: url};
                history.pushState(obj, obj.Page, obj.Url);
            } else {
                window.location.href = "homePage";
                // alert("Browser does not support HTML5.");
            }
        }

        function getAllUrlParams(url) {

            // get query string from url (optional) or window
            var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

            // we'll store the parameters here
            var obj = {};

            // if query string exists
            if (queryString) {

            // stuff after # is not part of query string, so get rid of it
            queryString = queryString.split('#')[0];

            // split our query string into its component parts
            var arr = queryString.split('&');

            for (var i=0; i<arr.length; i++) {
                // separate the keys and the values
                var a = arr[i].split('=');

              // in case params look like: list[]=thing1&list[]=thing2
              var paramNum = undefined;
              var paramName = a[0].replace(/\[\d*\]/, function(v) {
                paramNum = v.slice(1,-1);
                return '';
              });

              // set parameter value (use 'true' if empty)
              var paramValue = typeof(a[1])==='undefined' ? true : a[1];

              // (optional) keep case consistent
              paramName = paramName.toLowerCase();
              paramValue = paramValue.toLowerCase();

              // if parameter name already exists
              if (obj[paramName]) {
                // convert value to array (if still string)
                if (typeof obj[paramName] === 'string') {
                  obj[paramName] = [obj[paramName]];
                }
                // if no array index number specified...
                if (typeof paramNum === 'undefined') {
                  // put the value on the end of the array
                  obj[paramName].push(paramValue);
                }
                // if array index number specified...
                else {
                  // put the value at that index number
                  obj[paramName][paramNum] = paramValue;
                }
              }
              // if param name doesn't exist yet, set it
              else {
                obj[paramName] = paramValue;
              }
            }
          }

          return obj;
        }

    </script>
@endsection
