@extends('layouts.newdesign.public')
@section('title', 'Văn Phòng Đại Diện')
@section('content')
    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2"><h1 class="darkblue">Văn phòng đại diện</h1></div>
                        @for ($i = 0; $i <= $officeCount; $i = $i+2)
                        <?php if(!empty($office[$i])): ?>
                        <div class="row mt40">
                            <div class="col-md-6">
                                <div class="img-preview">
                                    <img src="<?php echo (!empty($office[$i]['images'])) ? url('/files/offices') . $office[$i]['images'] : 'images/offices/img-1.jpg' ?>" alt="" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <h4>{{ array_get($office[$i], 'name', '') }}</h4>
                                    <ul class="list-add">
                                        <li><span>Địa chỉ:</span>
                                            {{ array_get($office[$i], 'address', '') }}
                                        </li>
                                        <li><span>Điện thoại:</span> {{ array_get($office[$i], 'phone', '') }}</li>
                                        <li><span>Email</span>{{ array_get($office[$i], 'email', '') }}</li>
                                        <hr>
                                        <?php if(array_get($office[($i)], 'manage.employees.fullname', '')): ?>
                                        <li>
                                            <span>Trưởng VPĐD: </span> {{ array_get($office[$i], 'manage.employees.fullname', '') }}
                                        </li>
                                        <li>
                                            <span> Điện thoại: </span> {{ array_get($office[$i], 'manage.employees.phone', '') }}
                                        </li>
                                        <li>
                                            <span> Email: </span> {{ array_get($office[$i], 'manage.employees.email', '') }}
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                        <?php if(!empty($office[$i + 1])): ?>
                        <div class="row mt40">
                            <div class="col-md-6">
                                <div class="">
                                    <h4>
                                        {{ array_get($office[($i + 1)], 'name', '') }}
                                    </h4>
                                    <ul class="list-add">
                                        <li><span>Địa chỉ:</span>
                                            {{ array_get($office[($i + 1)], 'address', '') }}
                                        </li>
                                        <li><span>Điện thoại:</span> {{ array_get($office[($i + 1)], 'phone', '') }}
                                        </li>
                                        <li><span>Email</span> {{ array_get($office[($i + 1)], 'email', '') }}</li>
                                        <hr>
                                        <?php if(array_get($office[($i + 1)], 'manage.employees.fullname', '')): ?>
                                        <li>
                                            <span>Trưởng VPĐD: </span> {{ array_get($office[($i + 1)], 'manage.employees.fullname', '') }}
                                        </li>
                                        <li>
                                            <span> Điện thoại: </span> {{ array_get($office[($i + 1)], 'manage.employees.phone', '') }}
                                        </li>
                                        <li>
                                            <span> Email: </span> {{ array_get($office[($i + 1)], 'manage.employees.email', '') }}
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="img-preview">
                                    <img src="<?php echo (!empty($office[$i + 1]['images'])) ? url('/files/offices') . $office[$i + 1]['images'] : 'images/offices/img-2.jpg' ?>" alt="" />
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
