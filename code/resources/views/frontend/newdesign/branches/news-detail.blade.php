<div class="p30">
    <div class="contentDetail">
        <div class="row">
            <a class="btn btn-primary btn-back-custom" href="{{$breadcrumb['link']}}"><i class="fa fa-angle-double-left"></i> {{$breadcrumb['text']}}</a>
        </div>
        <div class="row">
            <h2 class="">{{$detail->title}}</h2>
            <div class="entry-content">
                <p class="text-center"><img src="{{url('/images/news/'.$detail->thumnail_image)}}" class=""></p>
                {!! $detail->content !!}</div>
        </div>
    </div>
</div>