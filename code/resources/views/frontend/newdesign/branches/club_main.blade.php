<div class="clearfix pt30">
    <div class="cta-title-2">
        <h4 class="text-uppercase darkblue">Ban chấp hành CLB thuộc Chi Hội</h4>
    </div>
    <div>
        @include('frontend.newdesign.branches.club_list_manager')
    </div>
</div>
<hr class="pt20 pb20">
<div class="mt30">
    <div class="cta-title-2">
        <h4 class="text-uppercase darkblue">Danh sách hội viên CLB thuộc chi hội</h4>
    </div>
    <div>
        @include('frontend.newdesign.branches.club_search_hoivien')
        @include('frontend.newdesign.branches.club_list_member')
    </div>

</div>