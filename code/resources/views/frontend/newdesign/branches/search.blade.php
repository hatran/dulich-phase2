<div class="">
	<form class="full-width" name="filter" id="filter" method="get">
        <div class="row mb20">
            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                <label for="clb_name">Tên chi hội</label>
                <select id="clb_name" name="branch_name" tabindex="1">
                    <option value="">Chọn Chi hội</option>
                    @if (!empty($listBranches))
                    @foreach($listBranches as $key => $val)
                    <option @php if ($currentBranch == $key || old('currentBranch') == $key) {echo 'selected="selected"';}
                            @endphp
                            value="{{$key}}">{{$val}}</option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="field-wrap col-md-3 col-sm-3 col-xs-12">
                <label for="captcha">Mã xác nhận</label>
                <input id="captcha2" name="captcha" type="text" tabindex="2">
				@if ($errors->any())
					@foreach ($errors->all() as $error)
					<div class="text-danger">{{ $error }}</div>
					@endforeach
				@endif
            </div>
            <div class="field-wrap col-md-2 col-sm-2 col-xs-12">
                <label></label>
                <div class="mt16 captcha_hoivien">
                    <img id="captcha" src="{{Captcha::src()}}">
                    <a href="javascript:;" onclick="reloadCaptcha(jQuery('#captcha'))">
                        <img src="images/refresh-icon.png">
                    </a>
                </div>
            </div>
            <div class="field-wrap pt30 text-center">
                <input class="btn" name="btnFilter" value="Tìm kiếm" type="submit" tabindex="3">
            </div>
        </div>
    </form>
</div>