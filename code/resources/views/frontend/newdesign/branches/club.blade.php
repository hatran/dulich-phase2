<style>
    .clb_name,#search_guideLanguage{line-height: 100%}
    .error-list{list-style: none}
</style>
<div class="p30">
    <div class="field-wrap pt30 ">
        <label class="col-sm-3" for="name3 ">Câu lạc bộ thuộc Chi Hội</label>
        <div class="col-sm-4">
            <select class="clb_name" onchange="getBrancheInfoById(jQuery(this))" name="branch_name" tabindex="1" required="">
                <option value="">Chọn CLB thuộc Chi Hội</option>
                @if (!empty($listClb))
                @foreach($listClb as $key => $val)
                <option value="{{$key}}">{{$val}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-sm-5"></div>
    </div>
    <div class="clb-group-content pt30"></div>
</div>
<script>var baseBranchesAjax = '{{$baseAjaxBranch}}';</script>