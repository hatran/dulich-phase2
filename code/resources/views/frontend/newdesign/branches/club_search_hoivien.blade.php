<div class="">
    <div class="alert alert-danger hidden error-search-member"></div>
    <form action="{{ URL::route('front_end_ajax_get_member', ['id' => $id, 'type' => $type]) }}" class="full-width" name="filter-member" method="post">
        <div class="row mb20">
            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                <label for="search_member_name">Họ và tên</label>
                <input value="{{Input::get('search_member_name', '')}}" id="search_member_name" name="search_member_name" type="text">
            </div>
            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                <label for="search_touristGuideCode">Số thẻ HDV</label>
                <input id="search_touristGuideCode" name="search_touristGuideCode" type="text">
            </div>
            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                <label for="search_member_code">Số thẻ hội viên</label>
                <input id="search_member_code" name="search_member_code" type="text">
            </div>
        </div>

        <div class="row mb20">
            <div class="dropfield field-wrap col-md-4 col-sm-4 col-xs-12">
                <label for="search_guideLanguage">Ngôn ngữ hướng dẫn</label>
                <select id="search_guideLanguage" name="search_guideLanguage">
                    <option value="">Chọn ngôn ngữ</option>
                    @if (!empty($listLanguage))
                    @foreach($listLanguage as $key => $val)
                    <option value="{{$key}}">{{$val}}</option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                <label for="ct2">Mã xác nhận</label>
                <input id="ct2" name="captcha2" type="text">
            </div>
            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                <label for="id4"></label>
                <div class="mt16 captcha_hoivien">
                    <img id="captcha" src="{{Captcha::src()}}">
                    <a href="javascript:;" onclick="reloadCaptcha2(jQuery(this))">
                        <img src="images/refresh-icon.png">
                    </a>
                </div>
            </div>
        </div>

        <div class="field-wrap pt30 text-center">
            <input class="btn" name="btnFilterMember" value="Tìm Hội viên" type="submit">
        </div>
    </form>

</div>