@extends('layouts.newdesign.public')
@section('title', 'Xếp Hạng Hướng Dẫn Viên')
@section('content')

@include('layouts.newdesign.banner')
<div id="coached" class="flat-section coached" data-scroll-index="6">
    <div class="section-content">
        <div class="container not-partner">
            <div class="row">

                <div class="col-md-3">
                    @include('frontend.newdesign.survey.common')
                </div>
                <div class="col-md-9">
                   <div>
                    <div class="contentDetail">

                      <ul class="home-list-4 scale-images">
                        <li class="clearfix">
                            <div class="main-content" style="word-wrap: break-word; font-weight: bold;"><?php echo isset($data) ? $data : '' ?></div>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
