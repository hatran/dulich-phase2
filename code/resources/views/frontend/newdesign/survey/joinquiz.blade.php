@extends('layouts.newdesign.public')
@section('title', 'Thi kiến thức xếp hạng HDV')
@section('content')

@include('layouts.newdesign.banner')
<div id="coached" class="flat-section coached" data-scroll-index="6">
    <div class="section-content">
        <div class="container not-partner">
            <div class="row">
                @if(session('successes'))
                    <div id="closeButton" class="alert alert-success">
                        <button type="button" class="close" aria-label="Close" style="width: 0%; height: 0px;">
                            <span aria-hidden="true" style="float: right;">&times;</span>
                        </button>
                        <ul style="display: flex;">
                            @foreach (session('successes') as $success)
                                <li>{{ $success }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <button id="closeButton" type="button" class="close" aria-label="Close" style="width: 0%; height: 0px;">
                            <span aria-hidden="true" style="float: right;">&times;</span>
                        </button>
                        <ul style="display: flex;">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-md-offset-1 col-md-11">
                    <div class="cta-title-2">
                        <h1 class="darkblue">Thi kiến thức xếp hạng HDV</h1>
                    </div>

                    <div>
                        <h3>Loại xếp hạng: {{ $registered_rank_name->name }}</h3>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <div class="table-wrap">
                        <table>
                            <thead>
                                <tr>
                                    <td class="text-center">Ngày thi</td>
                                    <td class="text-center">Tổng số câu kiến thức cơ sở trả lời đúng</td>
                                    <td class="text-center">Tổng số câu kiến thức chuyên ngành và nghiệp vụ trả lời đúng</td>
                                    <td class="text-center">Tổng điểm</td>     
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($member_score) == 0)
                                <tr>
                                    <td colspan="4" class="text-center"><p style="font-weight: 700;">Bạn chưa tham gia thi</p></td>
                                </tr>
                                @else
                                    @foreach ($member_score as $result)
                                    <tr>
                                        <td class="text-center">{{ date('d/m/Y H:i:s', strtotime($result->join_date)) }}</td>
                                        <td class="text-center">{{ !empty($result->base_question) ? $result->base_question : 0 }}</td>
                                        <td class="text-center">{{ !empty($result->subject_question) ? $result->subject_question : 0 }}</td>
                                        <td class="text-center">{{ round($result->score, 2) }}</td>
                                    </tr>
                                    @endforeach     
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                @if (!empty($quiz_status))
                    <div class="field-wrap pt30 text-center">
                        <a href="{{ route('front_end_quiz') }}" class="btn" style="background: #0355b3;color: #FFFFFF;border-color: #FFFFFF">{{ count($member_score) == 0 ? 'Bắt đầu' : 'Thi lại' }}</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_embed')
<style>
    .cta-title-2 h1::after {
        content: "";
        display: block;
        width: 90%;
        height: 1px;
        background: #02438d;
        margin: 20px 5px 15px;
    }

    .button {
        border-color: #337ab7;
    }

    .darkblue {
        color: #0355b3;
    }

    .table-wrap table thead tr {
        background-color: #EDF1F6;
    }
    .table-wrap table tr {
        border-bottom: 1px solid #BBBABA;
        transition: 0.3s;
        -moz-transition: 0.3s;
        -webkit-transition: 0.3s;
        -o-transition: 0.3s;
        -ms-transition: 0.3s;
    }

    .table-wrap table thead tr td {
        color: #144a8b;
        /* text-transform: uppercase; */
        text-align: center;
    }

    table tbody tr td:last-child {
        border-right: 1px solid #BBBABA;
    }

    .table-wrap table tr td {
        padding: 10px 5px;
        font-family: "Times New Roman", Times, serif;
        white-space: nowrap;
        text-align: left;
        border: 1px solid #BBBABA;
        font-size: 14px;
    }

    body a:focus, body a:active, body a:hover, body a:visited {
        text-decoration: none;
        outline: none;
        color: #337ab7;
    }

    .text-center {
        text-align: center !important;
    }
</style>
@endsection
