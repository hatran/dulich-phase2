@extends('layouts.newdesign.public')
@section('title', 'Xếp Hạng Hướng Dẫn Viên')
@section('content')

@include('layouts.newdesign.banner')
<div id="coached" class="flat-section coached" data-scroll-index="6">
    <div class="section-content">
        <div class="container not-partner">
            <div class="row">
                <div class="col-md-3">
                    @include('frontend.newdesign.survey.common')
                </div>
                <div class="col-md-9">
                    <div class="cta-title-2">
                       <h1 class="darkblue">Câu hỏi xếp hạng HDV</h1>
                   </div>
                   <div>
                    <div class="contentDetail">
                        <div class="main-content" style="word-wrap: break-word;">
                            @if (count($knowledge) != 0)
                                <p style="font-weight: bold; font-style: italic;">*Các câu hỏi này thuộc về bản quyền của Hội Hướng dẫn viên Du lịch Việt Nam. Mọi hình thức sao chép và sử dụng không được sự đồng ý của Hội Hướng dẫn viên Du lịch Việt Nam là vi phạm pháp luật</p>
                            <ol>
                                @foreach ($knowledge as $list => $name)
                                    <li>
                                        <span style="font-weight: bold; font-size: 16px;">{{ $name }}</span>
                                        <ul style="padding-top: 10px;">
                                            @foreach ($arrKnowledge[$list] as $objList)
                                                <?php
                                                    $filename = !empty($objList->file_name) ? '/pdf/subject_knowledge/'.$objList->file_name : "";
                                                ?>

                                                <li class="fix-li">
                                                    <span>{{ $objList->name ?? '' }}</span>
                                                    &nbsp&nbsp
                                                    @if (! empty($filename))
                                                        (<a target="_blank" href="<?php echo $filename; ?>" class="link">Xem</a>&nbsp
                                                        <a href="<?php echo $filename; ?>" class="link" download>Download</a>)
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ol>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
<style>
    .fix-li::before {
        content: "- ";
    }
    .fix-li {
        list-style-type: none;
    }

    .link {
        color: #337ab7 !important;
    }
</style>