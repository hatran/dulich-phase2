@extends('layouts.newdesign.public')
@section('title', 'Xếp Hạng Hướng Dẫn Viên')
@section('content')
    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-3">
                        @include('frontend.newdesign.survey.common')
                    </div>
                    <div class="col-md-9">
                        @include('frontend.newdesign.survey.message')
                        <div class="cta-title-2">
                            <h1 class="darkblue">Đăng ký xếp hạng HDV</h1>
                        </div>
                        <div class="block-content showout">
                            <form action="{{ route('front_sub_update_inforegister_not_member') }}" method="post" style="width: 100%">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="field-wrap col-md-4 col-sm-10 col-xs-12">
                                        <label for="name">Họ và tên <span class="star">*</span></label>
                                        <input id="name" type="text" name="fullName" tabindex="1" maxlength="50"
                                               value="{{ old('fullName') }}"
                                               class="{{ ($errors->has('fullName')) ? 'error-input' : '' }}" autofocus>
                                        @if ($errors->has('fullName'))
                                            <span id="name-error" class="help-block">{{ $errors->first('fullName') }}</span>
                                        @endif
                                    </div>
                                    <div class="dropfield col-md-2 col-sm-2 col-xs-12">
                                        <label for="gender">Giới tính <span class="star">*</span></label>
                                        <select id="gender" name="gender" class="{{ ($errors->has('gender')) ? 'error-input' : '' }}" tabindex="2">
                                            <option value=""></option>
                                            <option value="1"{{ old('gender') == 1 ? 'selected' : ''}}>Nam
                                            </option>
                                            <option value="2"{{ old('gender') == 2 ? 'selected' : ''}}>Nữ
                                            </option>
                                        </select>
                                        @if ($errors->has('gender'))
                                            <span id="gender-error" class="help-block">{{ $errors->first('gender') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="datetime field-wrap col-md-4 col-sm-4 col-xs-4">
                                                <label for="date">Ngày sinh <span class="star">*</span></label>
                                                <input id="date" tabindex="3"
                                                       class="day datetime-input {{ ($errors->has('birthDate') || $errors->has('bod')) ? 'error-input' : '' }}"
                                                       type="text" maxlength="2" name="birthDate"
                                                       value="{{ old('birthDate') }}" placeholder="dd">
                                                <span class="datetime-icon fa fa-calendar"></span>
                                                @if ($errors->has('birthDate'))
                                                    <span id="date-error" class="help-block">{{ $errors->first('birthDate') }}</span>
                                                @endif
                                            </div>
                                            <div class="datetime field-wrap col-md-4 col-sm-4 col-xs-4">
                                                <label for="month">Tháng sinh <span class="star">*</span></label>
                                                <input id="month" tabindex="4"
                                                       class="month datetime-input {{ ($errors->has('birthMonth') || $errors->has('bod')) ? 'error-input' : '' }}"
                                                       type="text" maxlength="2"
                                                       name="birthMonth"
                                                       value="{{ old('birthMonth') }}" placeholder="mm">
                                                <span class="datetime-icon fa fa-calendar"></span>
                                                @if ($errors->has('birthMonth'))
                                                    <span id="date-error" class="help-block">{{ $errors->first('birthMonth') }}</span>
                                                @endif
                                            </div>
                                            <div class="datetime field-wrap col-md-4 col-sm-4 col-xs-4">
                                                <label for="year">Năm sinh <span class="star">*</span></label>
                                                <input id="year" tabindex="5"
                                                       class="expireyear datetime-input {{ ($errors->has('birthYear') || $errors->has('bod')) ? 'error-input' : '' }}"
                                                       type="text" maxlength="4" name="birthYear"
                                                       value="{{ old('birthYear') }}" placeholder="yyyy">
                                                <span class="datetime-icon fa fa-calendar"></span>
                                                @if ($errors->has('birthYear'))
                                                    <span id="date-error" class="help-block">{{ $errors->first('birthYear') }}</span>
                                                @endif
                                            </div>
                                            @if ($errors->has('bod'))
                                                <span id="date-error" class="help-block"
                                                      style="margin-left: 15px">{{ $errors->first('bod') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="field-wrap col-md-4 col-sm-6 col-xs-12 blockmt-15">
                                        <label for="idNumb">Số thẻ HDV du lịch <span class="star">*</span></label>
                                        <input id="idNumb" type="text" name="touristGuideCode" tabindex="11" maxlength="9"
                                               value="{{ old('touristGuideCode') }}"
                                               class="{{ ($errors->has('touristGuideCode')) ? 'error-input' : '' }}">
                                        @if ($errors->has('touristGuideCode'))
                                            <span id="touristGuideCode-error"
                                                  class="help-block">{{ $errors->first('touristGuideCode') }}</span>
                                        @endif
                                    </div>

                                    <div class="blockmt-15 field-wrap col-md-4 col-sm-6 col-xs-12 {{ old('typeOfTravelGuide') == 3 ? 'hidden' : '' }}">
                                        <label for="expireDate">Ngày hết hạn<span
                                                    class="star">*</span></label>
                                        <input id="expireDate" type="text" name="expirationDate" placeholder="DD/MM/YYYY" value="{{ old('expirationDate') }}" tabindex="12" maxlength="10"
                                               class="{{ ($errors->has('expirationDate')) ? 'error-input' : '' }}">
                                        @if ($errors->has('expirationDate'))
                                            <span id="expirationDate-error"
                                                  class="help-block">{{ $errors->first('expirationDate') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="blockmt-15 dropfield col-md-4 col-sm-6 col-xs-12">
                                        <label for="province">Địa bàn hoạt động <span class="star">*</span></label>
                                        <select id="province" name="province" tabindex="18"
                                                class="{{ ($errors->has('province')) ? 'error-input' : '' }}">
                                            <option value=""></option>
                                            @if (!empty($provincial))
                                                @foreach($provincial as $key => $value)
                                                    <option value="{{ $value->id }}" {{ old('province') == $value->id ? 'selected' : '' }}> {{ $value->value }} </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('province'))
                                            <span id="province-error"
                                                  class="help-block">{{ $errors->first('province') }}</span>
                                        @endif
                                    </div>
                                    <div class="blockmt-15 field-wrap col-md-4 col-sm-6 col-xs-12">
                                        <label for="phone1">Số điện thoại<span class="star">*</span></label>
                                        <input id="phone1" type="text" name="firstMobile" tabindex="19" maxlength="15"
                                               value="{{ old('firstMobile') }}"
                                               class="{{ ($errors->has('firstMobile')) ? 'error-input' : '' }}">
                                        @if ($errors->has('firstMobile'))
                                            <span id="phone1-error" class="help-block">{{ $errors->first('firstMobile') }}</span>
                                        @endif
                                    </div>
                                    <div class="blockmt-15 field-wrap col-md-4 col-sm-6 col-xs-12">
                                        <label for="email1">Email <span class="star">*</span></label>
                                        <input id="email1" type="text" name="firstEmail" tabindex="21" maxlength="50"
                                               value="{{ old('firstEmail') }}"
                                               class="{{ ($errors->has('firstEmail')) ? 'error-input' : '' }}">
                                        @if ($errors->has('firstEmail'))
                                            <span id="email1-error" class="help-block">{{ $errors->first('firstEmail') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="hdvBlock">
                                        <div class="some-class">
                                            <label class="hdv">Hướng dẫn viên <span class="star">*</span></label>
                                            <input onchange="chooseRadios('qt', 'changed')" type="radio" class="radio" name="typeOfTravelGuide" value="1" {{ old('typeOfTravelGuide') == 1 ? 'checked' : '' }} data-id="qt"/>
                                            <label class="hdv1" for="y">HDV du lịch quốc tế </label>
                                            <input onchange="chooseRadios('nd', 'changed')" type="radio" class="radio" name="typeOfTravelGuide" value="2" {{ old('typeOfTravelGuide') == 2 ? 'checked' : '' }} data-id="nd"/>
                                            <label class="hdv2" for="z">HDV du lịch nội địa</label>
                                        </div>
                                        @if ($errors->has('typeOfTravelGuide'))
                                            <span id="typeOfTravelGuide-error"
                                                  class="help-block">{{ $errors->first('typeOfTravelGuide') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="dkh" class="field-wrap col-md-4 col-sm-6 col-xs-12 block-content hidden">
                                        <label>Đăng ký <span class="star">*</span></label>
                                        <ul id="dkhList" class="radio-list ranks has-options list hidden">
                                            <?php $qt = [1, 2, 3]; ?>
                                            @foreach ($ranks as $key => $value)
                                                <?php $classCustom = in_array($key, $qt, true) ? "qt" : "nd"; ?>
                                                <li class="rank hidden {{ $classCustom }}">
                                                    <input type="radio" name="majors" value="{{ $key }}" {{ old('majors') == $key ? 'checked' : '' }}>
                                                    <label style="font-weight: bold; width: auto;"> {{ $value }}</label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div style="text-align: center;">
                                    <button class="btn btn-primary fix-button" style="width: 15%;background: #0355b3">Đăng ký</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function chooseRadios(classOpened, status) {
        if (classOpened === '') {
            return;
        }
        document.getElementById('dkh').classList.remove('hidden');
        document.getElementById('dkhList').classList.remove('hidden');
        if (status === 'changed') {
            this.setUncheckedRadio();
        }
        var classRemoved = classOpened === 'qt' ? 'nd' : 'qt';
        this.toggleOpenHidden(classRemoved, 'removed');
        this.toggleOpenHidden(classOpened, 'opened');
    }

    function toggleOpenHidden(className, status) {
        var matches = document.getElementsByClassName(className);
        for (var i=0; i<matches.length; i++) {
            matches[i].classList.remove(status === 'opened' ? 'hidden' : 'open');
            matches.item(i).classList.add(status === 'opened' ? 'open' : 'hidden');
        }
    }

    function setUncheckedRadio() {
        var matches = document.getElementsByName('majors');
        for (var i=0; i<matches.length; i++) {
            if (matches[i].checked) {
                matches[i].checked = false;
            }
        }
    }

    function openRadioChecked() {
        var matches = document.getElementsByName('typeOfTravelGuide');
        var classOpened = '';
        for (var i=0; i<matches.length; i++) {
            if (matches[i].checked) {
                classOpened = matches[i].getAttribute('data-id');
            }
        }

        this.chooseRadios(classOpened, 'static');
    }

    window.onload = function () {
        this.openRadioChecked();
    }
</script>
<style>
    .block .block-content {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }

    .block .block-content .list {
        margin: 0;
        padding: 0;
    }

    .ranks {
        z-index: auto!important;
    }
    .block .block-content .list li {
        list-style: none;
        padding: 7px 0;
    }
    .ranks.radio-list .rank label:after {
        content: "";
        left: -20px;
        width: 7px;
        height: 7px;
        margin-top: -2px;
        border-radius: 50%;
        background-color: #bbbaba;
    }

    .ranks.radio-list .rank label {
        vertical-align: unset;
    }

    input[name="majors"] {
        width: auto;
        vertical-align: middle;
    }

    .cta-title-2 h1::after {
        margin: 10px 5px 0px;
    }

    .fix-button {
        padding: 0px !important;
        border: 2px solid #ffffff;
    }

    .some-class {
        float: left;
        clear: none;
        margin-top: 20px;
    }

    .hdv, .hdv1, .hdv2 {
        float: left;
        clear: none;
        display: block;
        padding: 0px 1em 0px 8px;
    }

    input.radio {
        float: left;
        clear: none;
        margin: 2px 0 0 2px;
        width: unset;
        height: unset;
    }

    .hdvBlock {
        overflow: hidden;
    }

    .hidden {
        display: none;
    }

    .open {
        display: block;
    }

    .star, .help-block {
        color: red !important;
    }

    #gender, #province {
        padding-top: 0px;
        padding-left: 10px;
    }

    #typeOfTravelGuide-error {
        margin-top: 20px !important;
    }

    .bootstrap-datetimepicker-widget {
        list-style: none;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu {
        display: block;
        margin: 2px 0;
        padding: 4px;
        width: 19em;
    }
    @media (min-width: 768px) {
        .bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs {
            width: 38em;
        }
    }
    @media (min-width: 992px) {
        .bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs {
            width: 38em;
        }
    }
    @media (min-width: 1200px) {
        .bootstrap-datetimepicker-widget.dropdown-menu.timepicker-sbs {
            width: 38em;
        }
    }
    .bootstrap-datetimepicker-widget.dropdown-menu:before,
    .bootstrap-datetimepicker-widget.dropdown-menu:after {
        content: '';
        display: inline-block;
        position: absolute;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu.bottom:before {
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        border-bottom: 7px solid #ccc;
        border-bottom-color: rgba(0, 0, 0, 0.2);
        top: -7px;
        left: 7px;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu.bottom:after {
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-bottom: 6px solid white;
        top: -6px;
        left: 8px;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu.top:before {
        border-left: 7px solid transparent;
        border-right: 7px solid transparent;
        border-top: 7px solid #ccc;
        border-top-color: rgba(0, 0, 0, 0.2);
        bottom: -7px;
        left: 6px;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu.top:after {
        border-left: 6px solid transparent;
        border-right: 6px solid transparent;
        border-top: 6px solid white;
        bottom: -6px;
        left: 7px;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu.pull-right:before {
        left: auto;
        right: 6px;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu.pull-right:after {
        left: auto;
        right: 7px;
    }
    .bootstrap-datetimepicker-widget .list-unstyled {
        margin: 0;
    }
    .bootstrap-datetimepicker-widget a[data-action] {
        padding: 6px 0;
    }
    .bootstrap-datetimepicker-widget a[data-action]:active {
        box-shadow: none;
    }
    .bootstrap-datetimepicker-widget .timepicker-hour,
    .bootstrap-datetimepicker-widget .timepicker-minute,
    .bootstrap-datetimepicker-widget .timepicker-second {
        width: 54px;
        font-weight: bold;
        font-size: 1.2em;
        margin: 0;
    }
    .bootstrap-datetimepicker-widget button[data-action] {
        padding: 6px;
    }
    .bootstrap-datetimepicker-widget .btn[data-action="incrementHours"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Increment Hours";
    }
    .bootstrap-datetimepicker-widget .btn[data-action="incrementMinutes"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Increment Minutes";
    }
    .bootstrap-datetimepicker-widget .btn[data-action="decrementHours"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Decrement Hours";
    }
    .bootstrap-datetimepicker-widget .btn[data-action="decrementMinutes"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Decrement Minutes";
    }
    .bootstrap-datetimepicker-widget .btn[data-action="showHours"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Show Hours";
    }
    .bootstrap-datetimepicker-widget .btn[data-action="showMinutes"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Show Minutes";
    }
    .bootstrap-datetimepicker-widget .btn[data-action="togglePeriod"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Toggle AM/PM";
    }
    .bootstrap-datetimepicker-widget .btn[data-action="clear"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Clear the picker";
    }
    .bootstrap-datetimepicker-widget .btn[data-action="today"]::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Set the date to today";
    }
    .bootstrap-datetimepicker-widget .picker-switch {
        text-align: center;
    }
    .bootstrap-datetimepicker-widget .picker-switch::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Toggle Date and Time Screens";
    }
    .bootstrap-datetimepicker-widget .picker-switch td {
        padding: 0;
        margin: 0;
        height: auto;
        width: auto;
        line-height: inherit;
    }
    .bootstrap-datetimepicker-widget .picker-switch td span {
        line-height: 2.5;
        height: 2.5em;
        width: 100%;
    }
    .bootstrap-datetimepicker-widget table {
        width: 100%;
        margin: 0;
    }
    .bootstrap-datetimepicker-widget table td,
    .bootstrap-datetimepicker-widget table th {
        text-align: center;
        border-radius: 4px;
    }
    .bootstrap-datetimepicker-widget table th {
        height: 20px;
        line-height: 20px;
        width: 20px;
    }
    .bootstrap-datetimepicker-widget table th.picker-switch {
        width: 145px;
    }
    .bootstrap-datetimepicker-widget table th.disabled,
    .bootstrap-datetimepicker-widget table th.disabled:hover {
        background: none;
        color: #777777;
        cursor: not-allowed;
    }
    .bootstrap-datetimepicker-widget table th.prev::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Previous Month";
    }
    .bootstrap-datetimepicker-widget table th.next::after {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
        content: "Next Month";
    }
    .bootstrap-datetimepicker-widget table thead tr:first-child th {
        cursor: pointer;
    }
    .bootstrap-datetimepicker-widget table thead tr:first-child th:hover {
        background: #eeeeee;
    }
    .bootstrap-datetimepicker-widget table td {
        height: 54px;
        line-height: 54px;
        width: 54px;
    }
    .bootstrap-datetimepicker-widget table td.cw {
        font-size: .8em;
        height: 20px;
        line-height: 20px;
        color: #777777;
    }
    .bootstrap-datetimepicker-widget table td.day {
        height: 20px;
        line-height: 20px;
        width: 20px;
    }
    .bootstrap-datetimepicker-widget table td.day:hover,
    .bootstrap-datetimepicker-widget table td.hour:hover,
    .bootstrap-datetimepicker-widget table td.minute:hover,
    .bootstrap-datetimepicker-widget table td.second:hover {
        background: #eeeeee;
        cursor: pointer;
    }
    .bootstrap-datetimepicker-widget table td.old,
    .bootstrap-datetimepicker-widget table td.new {
        color: #777777;
    }
    .bootstrap-datetimepicker-widget table td.today {
        position: relative;
    }
    .bootstrap-datetimepicker-widget table td.today:before {
        content: '';
        display: inline-block;
        border: solid transparent;
        border-width: 0 0 7px 7px;
        border-bottom-color: #337ab7;
        border-top-color: rgba(0, 0, 0, 0.2);
        position: absolute;
        bottom: 4px;
        right: 4px;
    }
    .bootstrap-datetimepicker-widget table td.active,
    .bootstrap-datetimepicker-widget table td.active:hover {
        background-color: #337ab7;
        color: #fff;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    }
    .bootstrap-datetimepicker-widget table td.active.today:before {
        border-bottom-color: #fff;
    }
    .bootstrap-datetimepicker-widget table td.disabled,
    .bootstrap-datetimepicker-widget table td.disabled:hover {
        background: none;
        color: #777777;
        cursor: not-allowed;
    }
    .bootstrap-datetimepicker-widget table td span {
        display: inline-block;
        width: 54px;
        height: 54px;
        line-height: 54px;
        margin: 2px 1.5px;
        cursor: pointer;
        border-radius: 4px;
    }
    .bootstrap-datetimepicker-widget table td span:hover {
        background: #eeeeee;
    }
    .bootstrap-datetimepicker-widget table td span.active {
        background-color: #337ab7;
        color: #fff;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    }
    .bootstrap-datetimepicker-widget table td span.old {
        color: #777777;
    }
    .bootstrap-datetimepicker-widget table td span.disabled,
    .bootstrap-datetimepicker-widget table td span.disabled:hover {
        background: none;
        color: #777777;
        cursor: not-allowed;
    }
    .bootstrap-datetimepicker-widget.usetwentyfour td.hour {
        height: 27px;
        line-height: 27px;
    }
    .bootstrap-datetimepicker-widget.wider {
        width: 21em;
    }
    .bootstrap-datetimepicker-widget .datepicker-decades .decade {
        line-height: 1.8em !important;
    }
    .input-group.date .input-group-addon {
        cursor: pointer;
    }
    .sr-only {
        position: absolute;
        width: 1px;
        height: 1px;
        margin: -1px;
        padding: 0;
        overflow: hidden;
        clip: rect(0, 0, 0, 0);
        border: 0;
    }
    body .field-wrap .datetime-icon {
        position: absolute;
        top: 43px;
        right: 25px;
        bottom: 0;
        font-size: 14px;
        color: #bbbaba;
    }
    .blockmt-15 {
        margin-top: 15px;
    }
</style>