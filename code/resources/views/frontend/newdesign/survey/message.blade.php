@if(session('successes'))
    <div id="closeButton" class="alert alert-success">
        <ul>
            <li>{{ session('successes') }}</li>
        </ul>
    </div>
@endif
@if ($errors->any())
    <div id="closeButton" class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session('error_ajax'))
    <div id="closeButton" class="alert alert-danger">
        <ul>
            <li>{{ session('error_ajax')}}</li>
        </ul>
    </div>
@endif

