<?php
    use App\Libs\Helpers\Utils;
?>
<style>
    .active a {
        color: #ffc527 !important;
    }
</style>
<div class="sidebar_title">
    <i class="fa fa-bars"></i> Xếp hạng HDV
</div>
<div class="nav_sidebar">
    <ul class="menu_sidebar">
        <li class="{{ Utils::highlightMenu(['survey']) ? 'active' : '' }}">
            <a href="{{ route('front_end_survey') }}">Thông tin chung</a>
        </li>
        <li class="{{ Utils::highlightMenu(['rankcriteria']) ? 'active' : '' }}">
            <a href="{{ route('front_end_criteria') }}">Tiêu chí xếp hạng</a>
        </li>
        <li class="{{ Utils::highlightMenu(['rankregister']) ? 'active' : '' }}">
            <a href="{{ route('front_end_register') }}">Hướng dẫn đăng ký xếp hạng HDV</a>
        </li>
        <li class="{{ Utils::highlightMenu(['rankscore']) ? 'active' : '' }}">
            <a href="{{ route('front_end_score') }}">Hướng dẫn chấm điểm HDV của Cty lữ hành</a>
        </li>
        <li class="{{ Utils::highlightMenu(['infoquestion']) ? 'active' : '' }}">
            <a href="{{ route('front_end_infoquestion') }}">Câu hỏi xếp hạng</a>
        </li>
        @if (empty(auth()->user()->is_traveler_company))
        <li class="{{ Utils::highlightMenu(['inforegister']) ? 'active' : '' }}">
            <a href="{{ route('front_end_inforegister') }}">Đăng ký xếp hạng</a>
        </li>
        @endif
    </ul>
</div>
