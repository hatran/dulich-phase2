@extends('layouts.newdesign.public')
@section('title', 'Chi tiết thông tin xếp hạng HDV - ' . $member->fullName)
@section('content')
   @include('layouts.newdesign.banner')
<style>
     .block {
        background-color: #FFFFFF;
        padding: 0 15px;
    }
     .block .block-heading {
      font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;
      font-size: 16px;
      text-transform: uppercase;
      border-bottom: 1px solid #BBBABA;
      padding: 15px 0 10px 0;
    }
     .block .block-content {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
          -ms-flex-pack: justify;
              justify-content: space-between;
    }
     .block .block-content .table-line {
      -ms-flex-preferred-size: 100%;
          flex-basis: 100%;
    }
     .block .block-content .table-line .line {
      padding: 0 15px;
      border-bottom: 1px solid #BBBABA;
      margin: 0;
    }
     .block .block-content .table-line .line .line-content {
      padding: 10px 0;
      font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;
    }
     .block .block-content .table-line .line .line-content span {
      color: #6C6B6B;
    }
     .block .block-content .table-line .line .line-content.line-col {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: start;
          -ms-flex-align: start;
              align-items: flex-start;
    }
     .block .block-content .table-line .line .line-content.line-col .col-content {
      margin-left: 5px;
      color: #3D3A3A;
    }
     .block .block-content .table-line .line .line-content.line-col .col-content .img-wrap {
      display: block;
    }
     .block .block-content .table-line .line:last-of-type {
      border-bottom: none;
    }
     .block .block-content .photo {
      padding-left: 15px;
      margin-top: 10px;
    }
     .block .block-content .line-row {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: start;
      -ms-flex-align: start;
      align-items: flex-start;
      width: 100%;
    }
     .block .block-content .line-row .line-inline {
      margin-right: 50px;
      font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;
      padding: 10px 0;
    }
     .block .block-content .line-row .line-inline span {
      color: #6C6B6B;
    }
     .block .block-content .list {
      margin: 0;
      padding: 0;
    }
     .block .block-content .list li {
      list-style: none;
      padding: 7px 0;
    }
     .block .block-content .list li .btn-upload {
      margin-left: 10px;
    }
     .block .block-content .list li:last-of-type {
      border-bottom: none;
    }
     .block form[name=approve] {
      background-color: #f3f6fa;
      padding: 15px;
    }
     .block form[name=approve] table {
      border-bottom: 1px solid #BBBABA;
      width: 100%;
    }
    .block form[name=approve] table tr td {
      padding: 10px 10px 10px 0;
      vertical-align: top;
      max-width: 80%;
    }
    #listEvaluation > thead > tr > td, #listEvaluation > tbody > tr > td {
        border: 1px solid black;
        text-align: center;
    }
     #listEvaluation > thead > tr > td {
         font-weight: bold;
     }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="page-wrap">
        <div class="block">
            <div class="block-content">
        <div id="coached" class="flat-section coached" data-scroll-index="6">
            <div class="section-content">
                <div class="container not-partner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cta-title-2">
                                <h1 class="darkblue">Thông tin xếp hạng HDV</h1>
                            </div>
                            <div>
                                <div class="contentDetail">
                                    <div class="table-responsive add-front" style="margin-top: -162px;">
                                        @if (!empty($profileImg))
                                            @if($isProfile)
                                                <div class="photo" style="position: absolute;right: 0; top: 36px;">
                                            @else
                                                <div class="photo">
                                            @endif
                                                    <img src="{{ $profileImg }}" style="width: 100px; float: right;position: relative;top: 162px;" height="150"/>
                                                </div>
                                        @else
                                            <div class="photo">
                                                <img src="{{ asset('images/3_4.jpg') }}" style="width: 100px; float: right;position: relative;top: 162px;" height="150" />
                                            </div>
                                        @endif
                                        <table id="table1" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                            <tbody>
                                                <tr>
                                                 <th colspan="2" style="padding-left: 0px; vertical-align: middle;"><h1 style="font-size: 14px; font-weight: 700;">Thông tin HDV</h1></th>
                                                </tr>
                                                <tr>
                                                    <td>Họ và tên</td>
                                                    <td>{{ !empty($member->fullName) ? htmlentities($member->fullName) : '' }}</td>

                                                </tr>
                                                <tr>
                                                    <td>Hướng dẫn viên</td>
                                                    <td>{{ !empty($typeOfTravelGuide) ? array_get($member_typeOfTravelGuide, $typeOfTravelGuide, '') : '' }}</td>

                                                </tr>
                                                <tr>
                                                    <td>Số thẻ HDV du lịch</td>
                                                    <td>{{ $member->touristGuideCode }}</td>

                                                </tr>
                                                <tr>
                                                    <td>Ngày hết hạn</td>
                                                    <td>
                                                    @if($typeOfTravelGuide == 1 || $typeOfTravelGuide == 2)
                                                      {{ empty($member->expirationDate) ? '' : date('d/m/Y', strtotime($member->expirationDate)) }}
                                                    @endif
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td>Số thẻ hội viên:</td>
                                                    <td>{{ $member->member_code }}</td>

                                                </tr>
                                                <tr>
                                                    <td>Địa bàn hoạt động</td>
                                                    <td> {{ $member->province ? array_get($provincial, $member->province, '') : ''}}</td>

                                                </tr>
                                                <tr>
                                                    <td>Điện thoại di động 1 </td>
                                                    <td>{{ empty($member->firstMobile) ? '' : $member->firstMobile }}</td>

                                                </tr>

                                                <tr>
                                                    <td>Điện thoại di động 2</td>
                                                    <td>{{ empty($member->secondMobile) ? '' : htmlentities($member->secondMobile) }}</td>

                                                </tr>
                                                <tr>
                                                    <td>Email 1</td>
                                                    <td>{{ empty($member->firstEmail) ? '' : $member->firstEmail }}</td>

                                                </tr>

                                                <tr>
                                                    <td style="width: 50%;">Giới tính</td>
                                                    <td>{{ ($member->gender == 1) ? 'Nam' : 'Nữ' }}</td>

                                                </tr>
                                                <tr>
                                                    <td> Inbound / Outbound</td>
                                                    <td>{{ ($member->inboundOutbound == 1 && $typeOfTravelGuide == 1) ? 'HDV Du Lịch Quốc Tế Inbound' : (($member->inboundOutbound == 2 && $typeOfTravelGuide == 1) ? 'HDV Du Lịch Quốc Tế Outbound' : '') }}</td>

                                                </tr>

                                                <tr>
                                                    <td>Sinh hoạt tại</td>
                                                    <td>{{ $typeOfPlace == 1 ? 'Chi hội ' . array_get($branch_chihoi, $member->province_code, '') : '' }}
                                                  {{ $typeOfPlace == 2 ? 'CLB thuộc hội ' . array_get($branch_clbthuochoi, $member->province_code, '') : '' }}</td>

                                                </tr>
                                                <tr>
                                                    <td>Đăng ký, xếp hạng HDV</td>
                                                    <td style="font-weight: bold;">{{ $member_rank->name ?? '' }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table id="table2" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                            <tbody>
                                                <tr>
                                                    <th colspan="2" style="padding-left: 0px; font-weight: 700; text-transform: uppercase;">Điểm thi kiến thức</th>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;">Tổng số câu kiến thức cơ sở trả lời đúng: </td>
                                                    <?php $_base = $member_rank->base_question / $base * 40; $_base = round($_base); ?>
                                                    <td>{{ !empty($member_rank->base_question) ? $member_rank->base_question : '0' }}/{{ $base }} = {{ $_base }} điểm (trọng số 40%)</td>
                                                </tr>
                                                <tr>
                                                    <td>Tổng số câu kiến thức chuyên ngành và nghiệp vụ trả lời đúng: </td>
                                                    <?php $_sub = $member_rank->subject_question / $sub * 60; $_sub = round($_sub); ?>
                                                    <td>{{ !empty($member_rank->subject_question) ? $member_rank->subject_question : '0' }}/{{ $sub }} = {{ $_sub }} điểm (trọng số 60%)</td>
                                                </tr>
                                                <tr>
                                                    <td>Tổng điểm</td>
                                                    <td>{{ !empty($member_rank->score) ? $member_rank->score : '0' }} điểm</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table id="table6" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                            <tbody>
                                                <tr>
                                                    <th colspan="2" style="padding-left: 0px; font-weight: 700; text-transform: uppercase;">Đánh giá, chấm điểm của công ty lữ hành</th>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;">Tổng số lượt đánh giá: </td>
                                                    <td>{{ count($total) != 0 ? $total[0]->total : 0 }}</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 50%;">Tổng số công ty lữ hành đánh giá: </td>
                                                    <td>{{ count($total) != 0 ? $total[0]->cnt : 0 }} công ty lữ hành
                                                    @if (!empty($isAdmin))
                                                        <a href="{{ route('company_evaluate_list_frontend_by_member', ['id' => $isAdmin]) }}" style="color: #337ab7 !important;">(Danh sách đánh giá)</a>
                                                    @else
                                                        <a href="{{ route('company_evaluate_list_frontend') }}" style="color: #337ab7 !important;">(Danh sách đánh giá)</a>
                                                    @endif

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: 700;">Chi tiết điểm đánh giá theo từng mục: </td>
                                                    <td>
                                                        @if (count($total) == 0)
                                                            Chưa có điểm đánh giá
                                                        @endif
                                                    </td>
                                                </tr>
                                                <?php $incre = 1; $score = 0;?>
                                                @if (count($total) != 0)
                                                    @foreach ($criteria as $key => $value)
                                                        <tr>
                                                            <td style="width: 50%;vertical-align: middle;">{{ $incre }}. {{ $value }} :</td>
                                                            <td>
                                                                @if ($key == $total[0]->criteria1_code)
                                                                    {{ $total[0]->ct1 }}
                                                                @elseif ($key == $total[0]->criteria2_code)
                                                                    {{ $total[0]->ct2 }}
                                                                @elseif ($key == $total[0]->criteria3_code)
                                                                    {{ $total[0]->ct3 }}
                                                                @elseif ($key == $total[0]->criteria4_code)
                                                                    {{ $total[0]->ct4 }}
                                                                @elseif ($key == $total[0]->criteria5_code)
                                                                    {{ $total[0]->ct5 }}
                                                                @endif
                                                                điểm (Trọng số {{ $percentage[$key] }}%)
                                                            </td>
                                                        </tr>
                                                    <?php $incre++; ?>
                                                    @endforeach
                                                        <tr>
                                                            <th style="vertical-align: middle; padding-left: 0px;font-weight: 700; text-transform: uppercase;">Tổng điểm đánh giá</th>
                                                            <td style="display: inline-block; width: 100%;">
                                                                @foreach ($percentage as $key => $value)
                                                                    @if ($key == $total[0]->criteria1_code)
                                                                        <?php $score += $total[0]->ct1 * $value / 100; ?>
                                                                    @elseif ($key == $total[0]->criteria2_code)
                                                                        <?php $score += $total[0]->ct2 * $value / 100; ?>
                                                                    @elseif ($key == $total[0]->criteria3_code)
                                                                        <?php $score += $total[0]->ct3 * $value / 100; ?>
                                                                    @elseif ($key == $total[0]->criteria4_code)
                                                                        <?php $score += $total[0]->ct4 * $value / 100; ?>
                                                                    @elseif ($key == $total[0]->criteria5_code)
                                                                        <?php $score += $total[0]->ct5 * $value / 100; ?>
                                                                    @else
                                                                    @endif
                                                                @endforeach
                                                                {{ round($score, 2) }} điểm
                                                            </td>
                                                        </tr>
                                                    @endif
                                                        <tr>
                                                            <th style="vertical-align: middle; padding-left: 0px;font-weight: 700; text-transform: uppercase;">Điểm hồ sơ</th>
                                                            <td style="display: inline-block; width: 100%;">
                                                               {{ !empty($member_rank->score_file) ? $member_rank->score_file : 'Chưa có ' }} điểm
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th style="vertical-align: middle; padding-left: 0px;font-weight: 700; text-transform: uppercase;">Điểm giải thưởng</th>
                                                            <td style="display: inline-block; width: 100%;">
                                                                {{ !empty($member_rank->score_award) ? $member_rank->score_award : 'Chưa có ' }} điểm
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                        <div class="block" style="margin-top:  10px; padding: 0px 0px; background-color: unset;">
                                                            <div style="vertical-align: middle; padding-left: 0px; font-weight: 700; text-transform: uppercase;">Lịch sử đánh giá {{ $member_rank_name ?? '' }}:</div>
                                                            <div class="block-content" style="margin-top: 10px;">
                                                                <div class="table-line">
                                                                        <div class="table-wrap">
                                                                            <table id="listEvaluation">
                                                                                <thead>
                                                                                <tr>
                                                                                    <td>Số TT</td>
                                                                                    <td>Tên công ty lữ hành</td>
                                                                                    <td>Mã số thuế</td>
                                                                                    <?php
                                                                                        $incre = 1;
                                                                                        foreach ($criteria as $value) {
                                                                                            echo '<td>Tiêu chí '.$incre.'</td>';
                                                                                            $incre++;
                                                                                        }
                                                                                    ?>
                                                                                    <td>Thời gian đánh giá</td>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <?php $i = 1; ?>
                                                                                    @foreach ($companyEvaluationList as $companyEvaluation)
                                                                                        <tr>
                                                                                            <td>{{ $i }}</td>
                                                                                            <td>{{ $companyEvaluation->company_name }}</td>
                                                                                            <td>{{ $companyEvaluation->taxcode_company }}</td>
                                                                                            <td>{{ $companyEvaluation->criteria1 }} điểm</td>
                                                                                            <td>{{ $companyEvaluation->criteria2 }} điểm</td>
                                                                                            <td>{{ $companyEvaluation->criteria3 }} điểm</td>
                                                                                            <td>{{ $companyEvaluation->criteria4 }} điểm</td>
                                                                                            <td>{{ $companyEvaluation->criteria5 }} điểm</td>
                                                                                            <td>{{ $companyEvaluation->created_at }}</td>
                                                                                        </tr>
                                                                                        <?php $i++; ?>
                                                                                    @endforeach
                                                                                </tbody>
                                                                            </table>
                                                                        </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                        </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection
