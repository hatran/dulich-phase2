@extends('layouts.newdesign.public')
@section('title', 'Xếp Hạng Hướng Dẫn Viên')
@section('content')

@include('layouts.newdesign.banner')
<div id="coached" class="flat-section coached" data-scroll-index="6">
    <div class="section-content">
        <div class="container not-partner">
            <div class="row">

                <div class="col-md-3">
                    @include('frontend.newdesign.survey.common')
                </div>
                <div class="col-md-9">
                    <div class="cta-title-2">
                       <h1 class="darkblue">Thông tin chung</h1>
                   </div>
                   <div>
                    <div class="contentDetail">

                      <ul class="home-list-4 scale-images home-list-4-fix-size">
                        <li class="clearfix">
                            <div class="main-content" style="word-wrap: break-word;"><?php echo isset($data['content']) ? $data['content'] : '' ?></div>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
