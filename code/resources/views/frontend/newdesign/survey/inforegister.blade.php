@extends('layouts.newdesign.public')
@section('title', 'Xếp Hạng Hướng Dẫn Viên')
@section('content')
    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-3">
                        @include('frontend.newdesign.survey.common')
                    </div>
                    <div class="col-md-9">
                        @include('frontend.newdesign.survey.message')
                        <div class="cta-title-2">
                            <h1 class="darkblue">Đăng ký xếp hạng HDV</h1>
                        </div>
                        <div>
                            <div class="contentDetail">
                                <ul class="home-list-4 scale-images">
                                    <li class="clearfix">
                                        <div class="main-content" style="word-wrap: break-word;">
                                        <div class="block">
                                            <div class="block-content">
                                                <div class="table-line" style="width: 100%">
                                                    <div class="line" style="word-wrap: break-word;">
                                                        <div class="line-inline">
                                                            <div class="block-content">
                                                                @if ($member->status == $memberOfficial)
                                                                    <form action="{{ route('front_sub_update_inforegister') }}" method="post" style="width: 100%">
                                                                        {{ csrf_field() }}
                                                                        <ul class="radio-list ranks has-options list">
                                                                            @foreach ($ranks as $key => $value)
                                                                                <li class="rank">
                                                                                    <input type="radio" name="majors" value="{{ $key }}">
                                                                                    <label style="font-weight: bold"> {{ $value }}</label>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                        <div style="text-align: center;">
                                                                            <button class="btn btn-primary fix-button" style="width: 15%;background: #0355b3">Đăng ký</button>
                                                                        </div>
                                                                    </form>
                                                                @else
                                                                    <h3>Bạn chưa là hội viên chính thức để đăng ký xếp hạng HDV</h3>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                     </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <style>
        .block .block-content {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }

        .block .block-content .list {
            margin: 0;
            padding: 0;
        }

        .ranks {
            z-index: auto!important;
        }
        .block .block-content .list li {
            list-style: none;
            padding: 7px 0;
        }
        .ranks.radio-list .rank label:after {
            content: "";
            left: -20px;
            width: 7px;
            height: 7px;
            margin-top: -2px;
            border-radius: 50%;
            background-color: #bbbaba;
        }

        .ranks.radio-list .rank label {
            vertical-align: unset;
        }

        input[name="majors"] {
            width: auto;
            vertical-align: middle;
        }

        .cta-title-2 h1::after {
            margin: 10px 5px 0px;
        }

        .fix-button {
            padding: 0px !important;
            border: 2px solid #ffffff;
        }
    </style>
@endsection
