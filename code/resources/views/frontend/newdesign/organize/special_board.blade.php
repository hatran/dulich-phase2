<?php
/**
 * Created by PhpStorm.
 * User: quantt
 * Date: 4/3/2018
 * Time: 1:22 AM
 */
?>

@extends('layouts.newdesign.public')
@section('title', 'Ban Chuyên Môn')
@section('content')
    @include('layouts.newdesign.banner')

    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cta-title-2">
                            <h1 class="darkblue">Ban chuyên môn</h1>
                        </div>
                        <div>
                            <div class="contentDetail">
                                <div class="bhoechie-tab-content add-front">
                                    <div class="pb20">
                                        <h4>Ban hội viên</h4>
                                        @if(isset($employees['banhoivien']))
                                            @foreach($employees['banhoivien'] as $employee)
                                                <div class="info-wrap row bg-white">
                                                    <div class="info col-md-4 col-sm-4 col-xs-12">
                                                        <span>{{$employee['option_value']}}
                                                            :</span> {{$employee['fullname']}}
                                                    </div>
                                                    <div class="info col-md-3 col-sm-4 col-xs-12">
                                                        <span>Điện thoại:</span> {{$employee['phone']}}
                                                    </div>
                                                    <div class="info col-md-5 col-sm-4 col-xs-12">
                                                        <span>Email:</span> {{$employee['email']}}
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="pb20">
                                        <h4>Ban đào tạo</h4>
                                        @if(isset($employees['bandaotao']))
                                            @foreach($employees['bandaotao'] as $employee)
                                                <div class="info-wrap row bg-white">
                                                    <div class="info col-md-4 col-sm-4 col-xs-12">
                                                        <span>{{$employee['option_value']}}
                                                            :</span> {{$employee['fullname']}}
                                                    </div>
                                                    <div class="info col-md-3 col-sm-4 col-xs-12">
                                                        <span>Điện thoại:</span> {{$employee['phone']}}
                                                    </div>
                                                    <div class="info col-md-5 col-sm-4 col-xs-12">
                                                        <span>Email:</span> {{$employee['email']}}
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="pb20">
                                        <h4>Ban truyền thông và Tổ chức sự kiện</h4>
                                        @if(isset($employees['banttsk']))
                                            @foreach($employees['banttsk'] as $employee)
                                                <div class="info-wrap row bg-white">
                                                    <div class="info col-md-4 col-sm-4 col-xs-12">
                                                        <span>{{$employee['option_value']}}
                                                            :</span> {{$employee['fullname']}}
                                                    </div>
                                                    <div class="info col-md-3 col-sm-4 col-xs-12">
                                                        <span>Điện thoại:</span> {{$employee['phone']}}
                                                    </div>
                                                    <div class="info col-md-5 col-sm-4 col-xs-12">
                                                        <span>Email:</span> {{$employee['email']}}
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection