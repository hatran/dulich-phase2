<?php
/**
 * Created by PhpStorm.
 * User: quantt
 * Date: 4/3/2018
 * Time: 1:22 AM
 */
?>

@extends('layouts.newdesign.public')
@section('title', 'Ban Cố Vấn')
@section('content')
    @include('layouts.newdesign.banner')
    <style>
        .add-front{font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;}
    </style>
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cta-title-2">
                            <h1 class="darkblue">Ban cố vấn</h1>
                        </div>
                        <div>
                            <div class="contentDetail">
                                <div class="table-responsive add-front">
                                    <table id="table2" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="15%">Họ và tên</th>
                                            <th class="text-center" width="10%">Chức danh</th>
                                            <th width="28%" class="text-center">Đơn vị công tác</th>
                                            <th style="margin-left: 30px" class="text-center">Địa chỉ</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php($i=0)
                                            @foreach($employees as $employee)
                                                <tr>
                                                    <td>{{$employee['fullname']}}</td>
                                                    <td>{{$employee['option_value']}}</td>
                                                    <td style="word-break: break-word;">{{$employee['company']}}</td>
                                                    <td class="blue" style="word-break: break-word;">{{$employee['address']}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div>
                                    {{ $employees->links() }}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection