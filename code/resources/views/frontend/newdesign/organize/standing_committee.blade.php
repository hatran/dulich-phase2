<?php
/**
 * Created by PhpStorm.
 * User: quantt
 * Date: 4/3/2018
 * Time: 1:22 AM
 */
?>

@extends('layouts.newdesign.public')
@section('title', 'Ban Thường Trực')
@section('content')
    @include('layouts.newdesign.banner')
<style>
    .dropdown-traveler {
        position: relative;
        display: inline-block;
    }

    .dropdown-content-traveler {
        display: none;
        position: absolute;
        background-color: #fff;
        min-width: 180px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        padding: 12px 16px;
        z-index: 1;
        color: #044d9f !important;
        font-weight: 600 !important;
        text-transform: uppercase !important;
        text-align: center;
        right: -110px;
    }

    .dropdown-content-traveler p {
        float: left;
    }

    .dropdown-content-traveler p a {
        color: #0355b3;
    }

    .dropdown-content-traveler p a:hover {
        border-bottom: unset;
        color: #ffc527 !important;
    }

    .dropdown-traveler:hover .dropdown-content-traveler {
        display: block;
        color: #222;
    }

    #color-traveler {
        color: #0355b3;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        font-size: 12px;
    }
    .box {
        margin: 0;
        border: 1px solid #d2dfec;
        border-radius: 5px;
        padding: 5px;
        background: #fff;
    }
    .table {
        margin-bottom: 0;
        height: 265px;
    }
    .table td,.table th {
        background: #fff;
    }
    .col-md-6 {
        margin-bottom: 20px;
    }
    .table-responsive {
        min-height: .01%;
        overflow-x: unset; 
    }
    .box .col-md-5 {
        height: 265px;
        overflow: hidden;
    }
</style>

    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="cta-title-2">
                            <h1 class="darkblue">Ban thường trực</h1>
                        </div>
                        <div>
                            <div class="contentDetail">
                                <div class="table-responsive add-front">                                    
                                        @foreach($employees as $key => $employee)
                                        <?php 
                                        $key++;
                                        if($key%2 != 0): ?>
                                                <div class="row">
                                        <?php endif; ?>
                                        <div class="col-md-6" style="margin-bottom: 20px;">
                                            <div class="row box">
                                                <div class="col-md-5">
                                                    <?php if(!empty($employee['profile_image'])): ?>
                                                        <img src="{{URL::asset('files/'.$employee['profile_image'])}}" alt="{{$employee['profile_image']}}" width="100%">
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-md-7">
                                                    <table class="table table-striped" width="100%">
                                                      <tbody>
                                                        <tr>
                                                          <th scope="col" style="width: 50%; border: none;">Họ và tên</th>
                                                          <th scope="col" style="width: 50%; border: none;">{{ $employee['fullname'] }}</th>
                                                        </tr>
                                                        <tr>
                                                          <th scope="row">Chức danh</th>
                                                          <td>{{$employee['option_value']}}</td>
                                                        </tr>
                                                        <tr>
                                                          <th scope="row">Đơn vị công tác</th>
                                                          <td style="word-break: break-word;">{{$employee['company']}}</td>
                                                        </tr>
                                                        <tr>
                                                          <th scope="row">Địa chỉ</th>
                                                          <td>{{$employee['address']}}</td>
                                                        </tr>                                                        
                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($key%2 == 0): ?>
                                                </div>
                                        <?php endif; ?>
                                        @endforeach
                                    </div>
                                    <!-- <table id="table2" class="table table0 mb30" border="0" cellpadding="10"
                                           cellspacing="10">
                                        <thead>
                                        <tr>
                                            <th style="width: 15%" class="text-center">Hình ảnh</th>
                                            <th width="15%" class="text-center">Họ và tên</th>
                                            <th width="10%" class="text-center">Chức danh</th>
                                            <th width="28%" class="text-center">Đơn vị công tác</th>
                                            <th style="margin-left: 30px" class="text-center">Địa chỉ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($employees as $employee)
                                            <tr>
                                                <td class="text-center">
                                                    <?php //if(!empty($employee['profile_image'])): ?>
                                                    <img src="{{URL::asset('files/'.$employee['profile_image'])}}"
                                                         alt="{{$employee['profile_image']}}"/>
                                                    <?php //endif; ?>
                                                </td>
                                                <td>{{$employee['fullname']}}</td>
                                                <td>{{$employee['option_value']}}</td>
                                                <td style="word-break: break-word;">{{$employee['company']}}</td>
                                                <td class="blue"
                                                    style="word-break: break-word;">{{$employee['address']}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table> -->
                                </div>
                                <div>
                                    {{ $employees->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection