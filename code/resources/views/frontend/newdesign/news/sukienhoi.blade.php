@extends('layouts.newdesign.public')
@section('title', 'Tin tức và sự kiện')
@section('content')
    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2">
                            <h1 class="darkblue">Tin tức sự kiện hội</h1>
                        </div>
                        <div>
                            <div class="contentDetail">
                                <ul class="home-list-4 scale-images">
                                    @foreach($news as $data)
                                        <li class="clearfix">
                                            <a href="{{url('/news/'.$data->id)}}">
                                                <img src="{{url('/images/news/'.$data->thumnail_image)}}" class="">
                                            </a>
                                            <h2 class="title-new">
                                                <a href="{{url('/news/'.$data->id)}}">{{$data->title}}</a>
                                            </h2>
                                            <p class="time-post">{{\App\Libs\Helpers\Utils::formatDatetimeVietnam($data->created_at)}}</p>
                                            <p>{{ str_limit($data->short_description,300,'....')}}</p>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="mt30 text-center paginate">
                                    {{ $news->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
