@extends('layouts.newdesign.public')
@section('title', 'Tin tức và sự kiện')
@section('content')
    @include('layouts.newdesign.banner')
        <div id="coached" class="flat-section coached" data-scroll-index="6">
            <div class="section-content">
                <div class="container not-partner">
                    <div class="row">
                        
                           
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="sidebar_title">
                                        <i class="fa fa-bars"></i> DANH MỤC CHI HỘI
                                    </div>
                                    <div class="nav_sidebar">
                                        <ul class="menu_sidebar">
                                            @foreach($listBranches as $key => $val)
                                                <li>
                                                    <a href="{{ url('/tintucvasukienchihoi', ['id' => $key]) }}">{{ $val }}</a>
                                                </li>
                                            @endforeach
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="cta-title-2">
                                         <h1 class="darkblue">{{ $detailBranch->name }}</h1>
                                       </div>
                                    <div>
                                        <div class="contentDetail">
                                          <ul class="home-list-4 scale-images">
                                            @foreach($news as $data)
                                            <li class="clearfix"> <a href="{{url('/news2/'.$data->id)}}"><img class="img-responsive" src="{{url('/images/news/'.$data->thumnail_image)}}"></a>
                                              <h2 class="title-new"><a href="{{url('/news2/'.$data->id)}}">{{$data->title}}</a></h2>
                                              <p class="time-post">{{\App\Libs\Helpers\Utils::formatDatetimeVietnam($data->created_at)}}</p>
                                              <p>{{str_limit($data->short_description,300,'....')}}</p>
                                            </li>
                                            @endforeach
                                            

                                          </ul>
                                        <div class="mt30 text-center paginate">
                                                {{ $news->links() }}
                                            </div>
                                   
                                        
                                    </div>
                                </div>
                            </div>
                            
                        
                    </div>
                </div>
            </div>
        </div>
@endsection
