@extends('layouts.newdesign.public')
@section('title', 'Tin tức và sự kiện')
@section('content')
<div id="full-container">
    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-3">
                        <div class="sidebar_title">
                            <i class="fa fa-bars"></i> DANH MỤC THÔNG TIN
                        </div>
                        <div class="nav_sidebar">
                            <ul class="menu_sidebar">
                                @foreach($listBranches as $key => $val)
                                <li>
                                    <a href="{{ url('/tintucvasukienchihoi', ['id' => $key]) }}">{{ $val }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="cta-title-2">
                         <h1 class="darkblue">{{ $listBranches[$news->branch_id] }}</h1>
                     </div>
                     <div>
                        <div class="contentDetail">
                            <div class="row">
                                <h2 class="">{{$news->title}}</h2>
                                <div class="entry-content">
                                    <?php echo $news->content?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 tinlienquan">
                    <hr>
                    <h5 class="text-bold">CÁC BÀI VIẾT KHÁC:</h5>
                    <div class="tour-guide-slider">

                        @if (!empty($newsRelated))
                        @if (count($newsRelated) < 3)
                        <ul class="">
                            @foreach ($newsRelated as $newsRel)
                            <li style="width: 33%; list-style-type: none; display: inline-block; margin-right: 10px;">
                             <div class="slide">
                                <div class="box-preview">
                                 <div class="box-img img-bg"> <a href="{{url('/news2/'.$newsRel->id)}}" class="news-detail-popup"><img src="{{url('/images/news/'.$newsRel->thumnail_image)}}" alt=""></a> </div>
                                 <div class="pt15">
                                    <h5><a href="{{url('/news2/'.$newsRel->id)}}" class="news-detail-popup">{{$newsRel->title}}</a></h5>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @else
                <ul class="owl-carousel">
                    @foreach ($newsRelated as $newsRel)
                    <li>
                     <div class="slide">
                        <div class="box-preview">
                         <div class="box-img img-bg"> <a href="{{url('/news2/'.$newsRel->id)}}" class="news-detail-popup"><img src="{{url('/images/news/'.$newsRel->thumnail_image)}}" alt=""></a> </div>
                         <div class="pt15">
                            <h5><a href="{{url('/news2/'.$newsRel->id)}}" class="news-detail-popup">{{$newsRel->title}}</a></h5>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
        @endif
        @endif
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection

@section ('footer_embed')
<script type="text/javascript" src="{{ asset('theme/homepage/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/homepage/js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/homepage/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/scripts.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/validator.js') }}"></script>
@endsection
