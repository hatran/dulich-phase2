<!-- Modal -->
<div class="modal fade" id="confirmSubmitQuiz" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Nộp bài thi xếp hạng HDV</h4>
            </div>
            <div class="modal-body">
                <h5>Bạn có chắc chắn muốn nộp bài thi?</h5>
            </div>
            <div class="modal-footer" style="text-align: center;">
                <button type="button" class="btn btn-warning" onclick="return continueQuiz();"
                        data-dismiss="modal" style="width: unset; border-color: unset; height: unset; padding: 10px 20px;">Quay lại</button>
                <button type="button" class="btn btn-primary" onclick="return submitQuiz();" style="width: unset; border-color: unset; ; height: unset; padding: 10px 20px;">Nộp</button>
            </div>
        </div>
    </div>
</div>