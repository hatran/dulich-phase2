@if (Auth::guest() || ! \App\Providers\UserServiceProvider::isNormalUser())
    <h4 class="title">Đăng nhập</h4>
    <form class="form frmLogin" action="{{ url('/login') }}" id="login" name="login" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <input placeholder="Tên truy cập" type="text" name="username" id="bill-number" class="form-control"
                   autocomplete="off" value="{{$request['username']  or '' }}">
        </div>
        <div class="form-group">
            <input placeholder="Mật khẩu" type="password" name="password" id="bill-code" class="form-control"
                   autocomplete="off">
        </div>

        <div>
            @if ($errors->has('captcha'))
                <font style="color:#ffc527">
                    Mã xác nhận không hợp lệ
                </font>
            @elseif ($errors->has('username') || $errors->has('password'))
                <font style="color:#ffc527">
                    Tên đăng nhập hoặc mật khẩu không đúng
                </font>
            @endif
        </div>
        <div class="form-group">
            <table style="cellpadding:0; cellspacing:0">
                <tr>
                    <td style="padding: 5px 8px">
                        <input placeholder="Nhập mã kiểm tra" style="width:150px" name="captcha" id="bill-code-kt"
                               class="form-control" autocomplete="off" value="{{ old('captcha') }}">
                    </td>
                </tr>
            </table>
        </div>
        <div class="form-group">
            <table style="cellpadding:0; cellspacing:0">
                <tr>
                    <td style="padding: 5px 8px">
                        <img id="captcha" src="{{ Captcha::src() }}">
                        <a href="#" style="float: right" onclick="reloadCaptcha(jQuery('#captcha'))">
                            <img src="images/refresh-icon.png">
                            <span class="validate" id=""></span>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="form-group">
            <div class="checkbox choice">
                <label><input id="remember-password" type="checkbox" name="remember-password">Ghi nhớ mật khẩu</label>
            </div>
        </div>
        <div class="form-group">
            <button class="btn small colorful hover-transparent-colorful" id="submit">Đăng nhập</button>
        </div>
        <div class="form-group text-center"><label class="small">Hoặc</label></div>

        <input type="hidden" name="login_homepage" value="1">
        <div class="form-group">
            <a href="{{ url('member/register') }}" class="btn small white hover-transparent-white"
               style="width: 100%; line-height: 12px">
                Đăng ký hội viên
            </a>
        </div>
    </form>
    <div class="clearfix"></div>
    <script>
        jQuery("#login").submit(function (e) {
            e.preventDefault();
            login();
        });
    </script>
@endif