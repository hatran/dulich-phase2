<form class="page-wrap" name="" method="get" id="formSearch">
    <div class="row">
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số thẻ HDV
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="tourist_guide_code" type="text" name="tourist_guide_code" value="{{ Input::get('tourist_guide_code', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Số thẻ hội viên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="member_code" type="text" name="member_code" value="{{ Input::get('member_code', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Họ tên hướng dẫn viên
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="member_name" type="text" name="member_name" value="{{ Input::get('member_name', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Mã đoàn
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="tourist_code" type="text" name="tourist_code" value="{{ Input::get('tourist_code', '') }}" maxlength="50">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Thời gian tour tuyến
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="display: flex; margin-bottom: 10px;">
            
            <input type="text" class="date datetime-input" name="from_date" id="start_time" value="{{ Input::get('from_date', '') }}" style="width: 50%">

            <input type="text" id="end_time" class="date datetime-input" name="to_date" value="{{ Input::get('to_date', '') }}" style="width: 50%; margin-left: 10px;">
        </div>
        <div class="field-wrap col-md-2 col-sm-6 col-xs-12">
            Tour tuyến
        </div>
        <div class="field-wrap col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 10px;">
            <input class="form-control" tabindex="1" id="forte_tour" type="text" name="forte_tour" value="{{ Input::get('forte_tour', '') }}" maxlength="50" placeholder="Tên tour tuyến">
        </div>
        
        {{ csrf_field() }}
        <div class="field-wrap col-md-2 col-sm-3 col-xs-2" style="margin-left: 45%;margin-bottom: 30px;">
            <input tabindex="7" type="submit" class="button btn-primary" name="search" value="Tìm kiếm" style="width: 100px;">
        </div>
    </div>
</form>