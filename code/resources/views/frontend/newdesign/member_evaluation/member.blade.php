@extends('layouts.newdesign.public')
@section('title', 'Danh sách đánh giá, cho điểm của công ty lữ hành')
@section('content')

@include('layouts.newdesign.banner')

<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    .error_border {-webkit-box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);box-shadow: inset 0 1px 1px rgba(219,17,17,.075), 0 0 8px rgba(219,17,17,.6);}
    .total {line-height: 30px;padding: 6px 10px;color: #0000F0 !important;border-radius: 3px;font-size: 16px;border: 0;display: inline-block;}
    .form-horizontal .control-label {padding-top: 7px;margin-bottom: 0;text-align: left;}
    .fa{cursor: pointer}
    .fa-2x {font-size: 1.2em;}
    .table-wrap table tr td {position: relative;word-wrap: break-word;white-space: normal !important;}

    .table-wrap table span {font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;}
    .tooltip-inner {
        max-width: 350px;
        width: 350px;
        position: relative;
        word-wrap: break-word;
        white-space: normal !important;
        z-index: 999999;
    }
    .rq-star {color: #ff0000;}
    .btn-primary.btn-custom{
        color: #fff;
        line-height: 21px;
        height: 35px;
        line-height: 35px;
        border: 1px solid #BBBABA;
        padding: 0 10px;
        max-width: 100%;
        display: block;
        border-radius: 2px;
    }
    .modal-title {
        margin: 0;
        font-weight: bold;
        float: left;
        font-size: 20px;
    }

    table tbody tr td:last-child {
        border-right: 1px solid #BBBABA;
    }

    input[type="submit"] {
        width: 60% !important;
    }
</style>
<div class="main-content" style="padding: 50px;">
    <div class="cta-title-2">
       <h1 class="darkblue">Danh sách đánh giá, cho điểm của công ty lữ hành</h1>
   </div>
    @include('frontend.newdesign.member_evaluation.search_member')
    <div class="clearfix"></div>
    <div class="page-wrap">
        <div class="total">Tổng số: {{$count_evaluations}}</div>
        <div class="table-wrap">
            <table>
                <thead>
                    <tr>
                        <td class="text-center" width="3%">STT</td>
                        <td class="text-center" width="15%">Hạng HDV</td>
                        <td class="text-center" width="10%">Tên công ty lữ hành</td>
                        <td class="text-center" width="8%">Mã số thuế</td>
                        <td class="text-center" width="7%">Tổng điểm đánh giá</td>     
                        <td class="text-center" width="10%">Mã đoàn</td>
                        <td class="text-center" width="15%">Tour tuyến</td>
                        <td class="text-center" width="13%">Tên người đánh giá</td>
                        <td class="text-center" width="10%">Số ĐT<br/> người đánh giá</td>
                        <td class="text-center" width="9%">Ngày<br/> đánh giá</td>
                    </tr>
                </thead>
                <tbody>
                    @if (count($objEvaluations) == 0)
                    <tr>
                        <td colspan="11" class="text-center"><p>Không có bản ghi nào phù hợp theo điều kiện tìm kiếm</p></td>
                    </tr>
                    @else
                    @foreach($objEvaluations as $key => $item)
                    <tr>
                        <td class="text-center"> {{$current_paginator + $key + 1 }} </td>
                        <td class="text-center">{{ $ranks[$item->rid] ?? '' }}</td>
                        <td><a href="{{ route('company_evaluate_show', ['id' => $item->id, 'tourist_code' => $item->tourist_code]) }}" style="color: #337ab7;">{{ $item->name }}</a></td>
                        <td class="text-center">{{ $item->tax_code }}</td>
                        <td class="text-center">
                            <?php $score = 0; ?>
                            @foreach ($criteria as $key => $value)
                                @if ($key == $item->criteria1_code)
                                    <?php $score += $item->criteria1 * $value / 100; ?>
                                @elseif ($key == $item->criteria2_code)
                                    <?php $score += $item->criteria2 * $value / 100; ?>
                                @elseif ($key == $item->criteria3_code)
                                    <?php $score += $item->criteria3 * $value / 100; ?>
                                @elseif ($key == $item->criteria4_code)
                                    <?php $score += $item->criteria4 * $value / 100; ?>
                                @elseif ($key == $item->criteria5_code)
                                    <?php $score += $item->criteria5 * $value / 100; ?>
                                @endif
                            @endforeach
                            {{ round($score, 2) }}
                        </td>
                        <td class="text-center">{{ $item->tourist_code }}</td>
                        <td>{{ $item->forte_tour }}</td>
                        <td class="text-center">{{ $item->name }}</td>
                        <td class="text-center">{{ $item->phone}}</td>
                        <td class="text-center">{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @include('admin.layouts.pagination')
    </div>
</div>

@endsection
<style>
    .table-wrap table thead tr {
        background-color: #EDF1F6;
    }
    .table-wrap table tr {
        border-bottom: 1px solid #BBBABA;
        transition: 0.3s;
        -moz-transition: 0.3s;
        -webkit-transition: 0.3s;
        -o-transition: 0.3s;
        -ms-transition: 0.3s;
    }

    .table-wrap table thead tr td {
        color: #144a8b;
        /* text-transform: uppercase; */
        text-align: center;
    }

    .table-wrap table tr td {
        padding: 10px 5px;
        font-family: "Times New Roman", Times, serif;
        white-space: nowrap;
        text-align: left;
        border-left: 1px solid #BBBABA;
        font-size: 14px;
    }

    body a:focus, body a:active, body a:hover, body a:visited {
        text-decoration: none;
        outline: none;
        color: #337ab7;
    }

    .text-center {
        text-align: center !important;
    }
</style>
