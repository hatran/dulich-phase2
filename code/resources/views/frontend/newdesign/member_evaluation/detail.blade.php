@extends('layouts.newdesign.public')
@section('title', 'Chi tiết đánh giá HDV - ' . $objMember->fullName)
<style>
     .block {
        background-color: #FFFFFF;
        padding: 0 15px;
    }
     .block .block-heading {
      font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;
      font-size: 16px;
      text-transform: uppercase;
      border-bottom: 1px solid #BBBABA;
      padding: 15px 0 10px 0;
    }
     .block .block-content {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
          -ms-flex-pack: justify;
              justify-content: space-between;
    }
     .block .block-content .table-line {
      -ms-flex-preferred-size: 100%;
          flex-basis: 100%;
    }
     .block .block-content .table-line .line {
      padding: 0 15px;
      border-bottom: 1px solid #BBBABA;
      margin: 0;
    }
     .block .block-content .table-line .line .line-content {
      padding: 10px 0;
      font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;
    }
     .block .block-content .table-line .line .line-content span {
      color: #6C6B6B;
    }
     .block .block-content .table-line .line .line-content.line-col {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: start;
          -ms-flex-align: start;
              align-items: flex-start;
    }
     .block .block-content .table-line .line .line-content.line-col .col-content {
      margin-left: 5px;
      color: #3D3A3A;
    }
     .block .block-content .table-line .line .line-content.line-col .col-content .img-wrap {
      display: block;
    }
     .block .block-content .table-line .line:last-of-type {
      border-bottom: none;
    }
     .block .block-content .photo {
      padding-left: 15px;
      margin-top: 10px;
    }
     .block .block-content .line-row {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: start;
      -ms-flex-align: start;
      align-items: flex-start;
      width: 100%;
    }
     .block .block-content .line-row .line-inline {
      margin-right: 50px;
      font-family: 'HarmoniaSansProCyr-SemiBd', sans-serif;
      padding: 10px 0;
    }
     .block .block-content .line-row .line-inline span {
      color: #6C6B6B;
    }
     .block .block-content .list {
      margin: 0;
      padding: 0;
    }
     .block .block-content .list li {
      list-style: none;
      padding: 7px 0;
    }
     .block .block-content .list li .btn-upload {
      margin-left: 10px;
    }
     .block .block-content .list li:last-of-type {
      border-bottom: none;
    }
     .block form[name=approve] {
      background-color: #f3f6fa;
      padding: 15px;
    }
     .block form[name=approve] table {
      border-bottom: 1px solid #BBBABA;
      width: 100%;
    }
    .block form[name=approve] table tr td {
      padding: 10px 10px 10px 0;
      vertical-align: top;
      max-width: 80%;
    }
</style>
@section('content')
   @include('layouts.newdesign.banner')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="page-wrap" style="margin-top: 60px;">
        <div class="block">
            <div class="block-content">
        <div id="coached" class="flat-section coached" data-scroll-index="6">
            <div class="section-content">
                <div class="container not-partner">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <div class="contentDetail">
                                 <!--  <h2 class="darkblue">Chi tiết đánh giá HDV {{$objMember->fullName}}</h2> -->
                                    <div class="table-responsive add-front">
                                        <form>
                                            <table id="table1" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                         <th style="padding-left: 0px; vertical-align: middle;"><h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Thông tin HDV</th>
                                                         <th>
                                                            @if (!empty($profileImg))
                                                                @if($isProfile)
                                                                    <div class="photo" style="position: absolute;right: 0; top: 36px;">
                                                                @else 
                                                                    <div class="photo">
                                                                @endif
                                                                        <img src="{{ $profileImg }}" style="width: 100px;" height="150"/>
                                                                    </div>
                                                            @else
                                                                <div class="photo">
                                                                    <img src="{{ asset('images/3_4.jpg') }}" style="width: 100px;" height="150" />
                                                                </div>
                                                            @endif
                                                         </th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;">Họ và tên</td>
                                                        <td>{{ !empty($objMember->fullName) ? htmlentities($objMember->fullName) : '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hướng dẫn viên</td>
                                                        <td>{{ !empty($typeOfTravelGuide) ? array_get($member_typeOfTravelGuide, $typeOfTravelGuide, '') : '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Số thẻ HDV du lịch</td>
                                                        <td>{{ $objMember->touristGuideCode }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ngày hết hạn</td>
                                                        <td>
                                                        @if($typeOfTravelGuide == 1 || $typeOfTravelGuide == 2)
                                                          {{ empty($objMember->expirationDate) ? '' : date('d/m/Y', strtotime($objMember->expirationDate)) }}
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Địa bàn hoạt động</td>
                                                        <td> {{ $objMember->province ? array_get($provincial, $objMember->province, '') : ''}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;">Giới tính</td>
                                                        <td>{{ ($objMember->gender == 1) ? 'Nam' : 'Nữ' }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table id="table2" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                         <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Thông tin công ty lữ hành</th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;">Tên công ty</td>
                                                        <td>{{ $travelerCompany->company_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Địa chỉ</td>
                                                        <td>{{ $travelerCompany->address }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Số điện thoại</td>
                                                        <td>{{ $travelerCompany->phone }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <td>{{ $travelerCompany->email }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mã số thuế </td>
                                                        <td>{{ $travelerCompany->tax_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Số giấy phép kinh doanh lữ hành quốc tế (đối với công ty kinh doanh lữ hành quốc tế)</td>
                                                        <td>{{ $travelerCompany->business_certificate }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table id="table3" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Thông tin đại diện công ty lữ hành đánh giá</h1></th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;vertical-align: middle;">Tên người cho điểm<span class="color-required">*</span></td>
                                                        <td>
                                                            {{ $memberEvaluation->name }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">Chức danh người cho điểm<span class="color-required">*</span></td>
                                                        <td>
                                                            {{ $memberEvaluation->position }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">ĐT di động người cho điểm<span class="color-required">*</span></td>
                                                        <td>
                                                            {{ $memberEvaluation->phone }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table id="table4" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Thông tin tour HDV đã hoàn thành hướng dẫn</h1></th>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;vertical-align: middle;">Mã đoàn</td>
                                                        <td>
                                                            {{ $memberEvaluation->tourist_code }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">Tour tuyến</td>
                                                        <td>
                                                            {{ $memberEvaluation->forte_tour }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">Số lượng khách</td>
                                                        <td>
                                                            {{ $memberEvaluation->number_of_tourist }}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: middle;">Thời gian (từ ngày/tháng/năm - đến ngày/tháng/năm)<span class="color-required">*</span></td>
                                                        <td style="display: inline-block; width: 100%;">
                                                            {{ date('d/m/Y', strtotime($memberEvaluation->from_date)) }}
                                                            -
                                                            {{ date('d/m/Y', strtotime($memberEvaluation->to_date)) }}
                                                           
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table id="table6" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
                                                <tbody>
                                                    <tr>
                                                        <th colspan="2" style="padding-left: 0px;"><h1 class="darkblue" style="font-size: 14px; font-weight: 700;">Nội dung chấm điểm</h1></th>
                                                    </tr>
                                                    <?php $incre = 1; $score = 0;?>
                                                    @foreach ($criteria as $key => $value)
                                                        <tr>
                                                            <td style="width: 50%;vertical-align: middle;">{{ $value }}</td>
                                                            <td>
                                                                @if ($key == $memberEvaluation->criteria1_code)
                                                                    {{ $memberEvaluation->criteria1 }}
                                                                @elseif ($key == $memberEvaluation->criteria2_code)
                                                                    {{ $memberEvaluation->criteria2 }}
                                                                @elseif ($key == $memberEvaluation->criteria3_code)
                                                                    {{ $memberEvaluation->criteria3 }}
                                                                @elseif ($key == $memberEvaluation->criteria4_code)
                                                                    {{ $memberEvaluation->criteria4 }}
                                                                @elseif ($key == $memberEvaluation->criteria5_code)
                                                                    {{ $memberEvaluation->criteria5 }}
                                                                @endif
                                                                điểm (Trọng số {{ $percentage[$key] }}%)
                                                            </td>
                                                        </tr>
                                                    <?php $incre++; ?>
                                                    @endforeach
                                                        <tr>
                                                            <td style="vertical-align: middle;    padding-left: 0px;font-size: 20px;font-weight: 700;">Tổng điểm</td>
                                                            <td style="display: inline-block; width: 100%;">
                                                                @foreach ($percentage as $key => $value)
                                                                    @if ($key == $memberEvaluation->criteria1_code)
                                                                        <?php $score += $memberEvaluation->criteria1 * $value / 100; ?>
                                                                    @elseif ($key == $memberEvaluation->criteria2_code)
                                                                        <?php $score += $memberEvaluation->criteria2 * $value / 100; ?>
                                                                    @elseif ($key == $memberEvaluation->criteria3_code)
                                                                        <?php $score += $memberEvaluation->criteria3 * $value / 100; ?>
                                                                    @elseif ($key == $memberEvaluation->criteria4_code)
                                                                        <?php $score += $memberEvaluation->criteria4 * $value / 100; ?>
                                                                    @elseif ($key == $memberEvaluation->criteria5_code)
                                                                        <?php $score += $memberEvaluation->criteria5 * $value / 100; ?>
                                                                    @else
                                                                    @endif
                                                                @endforeach
                                                                {{ round($score, 2) }} điểm
                                                            </td>
                                                        </tr>
                                                </tbody>
                                            </table>
                                            
                                        </form>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection
