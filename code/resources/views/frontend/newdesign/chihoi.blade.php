@extends('layouts.newdesign.public')
@section('title', 'Thông tin chi hội')
@section('content')

<style>
    #clb_name,#search_guideLanguage{line-height: 100%}
    .error-list{list-style: none}
</style>
@include('layouts.newdesign.banner')
<div id="coached" class="flat-section coached" data-scroll-index="6">
    <div class="section-content">
        <div class="container not-partner">
            <div class="row">
                <div class="col-md-12">
                    <div class="cta-title-2">
                        <h1 class="darkblue">Chi hội</h1>
                    </div>
                    <div>
                        <div class="">
                            @include('frontend.newdesign.branches.search')
                            @if (!empty($currentBranch))
                            <div class="row"  style="margin-top:50px;">
                                <div class="tab-head"  uib-dropdown>
                                    <ul id="intro_tabs" class="intro_tabs nav nav-tabs nav-justified nav-pills">
                                        <li class="active">
                                            <a data-toggle="tab" href="#content-tab1" id="branchInfo">Thông Tin Chi Hội</a>
                                        </li>
                                       <!--  <li>
                                            <a data-link="{{URL::route('front_end_ajax_get_news', ['id' => $currentBranch, 'type' => $type])}}" onclick="tabContentAjax(event)" data-toggle="tab" href="#content-tab2">Tin tức sự kiện</a>
                                        </li> -->
                                        <li>
                                            <a data-link="{{URL::route('front_end_ajax_club_of_branches', ['id' => $currentBranch])}}" onclick="tabContentAjax(event)" data-toggle="tab" href="#content-tab3">
                                                CLB thuộc chi hội
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane fade active in" id="content-tab1">
                                        <div class="p30">
                                            <div class="">
                                                <div class="cta-title-2">
                                                    <h4 class="text-uppercase darkblue">Ban chấp hành</h4>
                                                </div>
                                                <div>
                                                    @include('frontend.newdesign.branches.list_manager')
                                                </div>
                                            </div>
                                            <hr class="pt20 pb20">
                                            <div class="mt30">
                                                <div class="cta-title-2">
                                                    <h4 class="text-uppercase darkblue">Hội viên</h4>
                                                </div>
                                                <div>
                                                    @include('frontend.newdesign.branches.search_hoivien')
                                                    @include('frontend.newdesign.branches.list_member')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="content-tab2"></div>
                                    <div class="tab-pane fade " id="content-tab3"></div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
