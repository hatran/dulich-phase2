@extends('layouts.newdesign.public')
@section('title', 'Về chúng tôi')
@section('content')

    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">

        <div class="section-content">
            <div class="container not-partner">
                @include('layouts.newdesign.slide')
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2"><h1 class="darkblue">{{ isset($data['title']) ? $data['title'] : 'Về chúng tôi' }}</h1></div>
                        <div class="main-content" style="word-wrap: break-word;"><?php echo isset($data['content']) ? $data['content'] : '' ?></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
