@extends('layouts.newdesign.public')
@section('title', 'Quy chế hoạt động')
@section('content')
    @include('layouts.newdesign.banner')

    <div id="coached" class="flat-section coached" data-scroll-index="6">

        <div class="section-content">
            <div class="container not-partner">
                @include('layouts.newdesign.slide')
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2"><h1 class="darkblue">{{ isset($data['title']) ? $data['title'] : 'Quy chế hoạt động' }}</h1></div>
                        <div class="main-content" style="word-wrap: break-word; overflow-x: auto;height: 700px;line-height: 24px; "><?php echo isset($data['content']) ? $data['content'] : '' ?></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
