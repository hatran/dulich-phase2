<div class="row row-member-list">
    <div class="">
        <h5 class="pl15 mt30">Kết quả có {{ !empty($listMember) ? $listMember->total() : 0 }} hội viên</h5>
        <div class="block-list mt10 box-preview">
            @if (!empty($listMember) && $listMember->count())
            @foreach($listMember as $member)
            <div class="col-sm-6 mt20">
                <div class="flex-box box-list">
                    <div class="avatar"><img src="{{$member->profileImg}}"></div>
                    <div class="inform">
                        <table width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td style="width: 58%;" >1. Họ tên</td>
                                    <td><span class="member-item-name">{{ htmlentities($member->fullName)}}</span></td>
                                </tr>
                                <tr>
                                    <td>2. Số thẻ HDV</td>
                                    <td>{{$member->touristGuideCode}}</td>
                                </tr>
                                <tr>
                                    <td>3. Số thẻ hội viên</td>
                                    <td>{{$member->member_code}}</td>
                                </tr>
                                <tr>
                                    <td>4. Ngày sinh</td>
                                    <td>{{ \Carbon\Carbon::parse($member->birthday)->format('d/m/Y') }}</td>
                                </tr>
                                <tr>
                                    <td>5. Ngôn ngữ hướng dẫn</td>
                                    @php
                                        $language = $member->guideLanguage;
                                    @endphp
                                    <?php $languageName = ""; ?>
                                    @foreach(explode(",", $language) as $key => $lang)
                                        @if (isset($listLanguage[$lang]))
                                            <?php $languageName .= $member->languageName ."," ?>
                                        @endif
                                    @endforeach
                                    <td>{{ rtrim($languageName, ",") }}</td>
                                </tr>
                                <tr>
                                    <td>6. Chi hội (nơi đăng ký sinh hoạt)</td>
                                    <td>
                                        {{ $member->typeOfPlace == 1 ? 'Chi hội ' . array_get(\App\Models\Branches::getBranches01(), $member->province_code, '') : '' }}
                                        {{ $member->typeOfPlace == 2 ? 'CLB thuộc hội ' . array_get(\App\Models\Branches::getBranches03(), $member->province_code, '') : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>7. Hội viên VTGA</td>
                                    <td>{{ $member->member_from }}</td>
                                </tr>
                                <tr>
                                    <td>8. Hạng HDV</td>
                                    <td>Chưa tham gia xếp hạng</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-sm-12 mt20">Chưa có hội viên</div>
            @endif
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="mt10 text-center paginate">
        {!! !empty($listMember) ? $listMember->render() : "" !!}
    </div>
</div>