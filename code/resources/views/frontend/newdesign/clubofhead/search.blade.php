<div class="">
    <form class="full-width" name="filter" id="filter" method="get">
        <div class="row mb20">
            <div class="field-wrap col-md-4 col-sm-4 col-xs-12">
                <label for="clb_name">Tên câu lạc bộ thuộc Hội</label>
                <select id="clb_name" name="clb_name" tabindex="1">
                    <option value="">Chọn CLB</option>
                    @if (!empty($listClb))
                    @foreach($listClb as $key => $val)
                    <option @php if ($currentClb == $key || old('currentClb') == $key) {echo 'selected="selected"';} @endphp
                            value="{{$key}}">{{$val}}</option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="field-wrap col-md-3 col-sm-3 col-xs-12">
                <label for="captcha">Mã xác nhận</label>
                <input id="captcha" name="captcha" type="text" tabindex="2">
				@if ($errors->any())
					@foreach ($errors->all() as $error)
					<div class="text-danger">{{ $error }}</div>
					@endforeach
				@endif
            </div>
            <div class="field-wrap col-md-2 col-sm-2 col-xs-12">
                <label></label>
                <div class="mt16 captcha_hoivien">
                    {!! captcha_img('flat') !!}
                    <a href="javascript:;" onclick="reloadCaptcha(jQuery('.captcha_hoivien img'))"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="field-wrap pt30 text-center">
                <input class="btn" name="btnFilter" value="Tìm kiếm" type="submit" tabindex="3">
            </div>
        </div>
    </form>
</div>