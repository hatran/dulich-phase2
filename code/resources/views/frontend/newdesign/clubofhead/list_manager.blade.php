<div class="ajax-bld">
    <div class="table-responsive">
        <table id="table2" class="table table0 mb30" border="0" cellpadding="10" cellspacing="10">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Họ tên</th>
                    <th>Chức danh</th>
                    <th>SĐT</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                @if (!$listManager->count())
                    <tr class="no-item">
                        <td colspan="5">Ban chấp hành đang cập nhật</td>
                    </tr>
                    @else
                    @foreach ($listManager as $key => $item)
                    @php if ($item->employees == "") continue;@endphp
                    <tr id="{{$item->id}}">
                        <td class="text-center"> {{$offsets + $key + 1 }} </td>
                        <td class="l_fullname">{{ htmlentities($item->employees->fullname) }}</td>
                        <td class="l_position">{{ isset($listOption[$item->option_code]) ? $listOption[$item->option_code] : '' }}</td>
                        <td class="l_phone">{{ $item->employees->phone }}</td>
                        <td class="l_email">{{ $item->employees->email }}</td>
                    </tr>
                    @endforeach
                    @endif
            </tbody>
        </table>
    </div>
    <div class="mt10 text-center paginate paginate-clb">
        <ul class="pagination">
            {!! $listManager->render() !!}
        </ul>
    </div>
</div>
