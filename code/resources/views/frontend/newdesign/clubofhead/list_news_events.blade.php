<div class="p30 news-events">
    @if ($listNews->count())
    <ul class="home-list-4 scale-images">
        @foreach($listNews as $new)
        <li class="clearfix">
            <a onclick="ajaxNewsDetail(jQuery(this))" data-href="{{URL::route('front_end_ajax_get_detail_news', ['id' => $new->id])}}" href="javascript:;">
                <img src="{{asset('images/news/' . $new->thumnail_image)}}" class="">
            </a>
            <h2 class="title-new">
                <a onclick="ajaxNewsDetail(jQuery(this))" data-href="{{URL::route('front_end_ajax_get_detail_news', ['id' => $new->id])}}" href="javascript:;">{{$new->title}}</a>
            </h2>
            <p class="time-post">{{\App\Libs\Helpers\Utils::formatDatetimeVietnam($new->created_at)}}</p>
            <p>{{str_limit($new->short_description,300,'....')}}</p>
        </li>
        @endforeach
    </ul>
    @else
    <ul class="home-list-4 scale-images">
        <li class="clearfix">
            <p>Tin tức và sự kiện của CLB đang được cập nhật.</p>
        </li>
    </ul>
    @endif
    <div class="mt30 text-center paginate">
        {!! $listNews->render() !!}
    </div>
</div>