@extends('layouts.newdesign.public')
@section('title', 'Thông tin Điểm đến')
@section('content')

@include('layouts.newdesign.banner')
<div id="coached" class="flat-section coached" data-scroll-index="6">
    <div class="section-content">
        <div class="container not-partner">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="cta-title-2">
                        <h1 class="darkblue">Thông tin Điểm đến</h1>
                    </div>
                    <div>
                        <div class="contentDetail">
                            <ul class="home-list-4 scale-images">
                                @foreach($listThongtin as $new)
                                <li class="clearfix">
                                    <a href="{{URL::route('front_info_diemden_detail', ['id' => $new->id])}}">
                                        <img src="{{asset('images/news/' . $new->thumnail_image)}}" class="">
                                    </a>
                                    <h2 class="title-new">
                                        <a href="{{URL::route('front_info_diemden_detail', ['id' => $new->id])}}">{{$new->title}}</a>
                                    </h2>
                                    <p class="time-post">{{\App\Libs\Helpers\Utils::formatDatetimeVietnam($new->created_at)}}</p>
                                    <p>{{str_limit($new->short_description,300,'....')}}</p>
                                </li>
                                @endforeach
                            </ul>
                            <div class="mt30 text-center paginate">
                                {{ $listThongtin->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
