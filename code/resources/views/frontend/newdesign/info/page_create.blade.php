@extends('layouts.newdesign.public')
@section('title', 'Thông tin')
@section('content')

    @include('layouts.newdesign.banner')
     <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
            <div class="row">
                <div class="col-md-3">
                    <div class="sidebar_title">
                        <i class="fa fa-bars"></i> DANH MỤC THÔNG TIN
                    </div>
                    <div class="nav_sidebar">
                        <ul class="menu_sidebar">
                            @foreach ($arr as $key => $value)
                                @if ($key == $id)
                                    <li>
                                        <a href="#" style="color: #ffc527;">{{ isset($getParentInfo) ? $getParentInfo[$key] : '' }}</a>
                                        @if ($value != 0)
                                            <ul class="sub_menu_sidebar open_sidebar">
                                            @foreach ($value as $val)
                                                <li>
                                                    @if ($val->id === $info->id)
                                                        <a href="{{ url("thong-tin/{$key}/{$val->id}") }}" style="color: #ffc527;">{{ $val->name }}</a>
                                                    @else
                                                        <a href="{{ url("thong-tin/{$key}/{$val->id}") }}">{{ $val->name }}</a>
                                                    @endif
                                                    
                                                </li>
                                            @endforeach 
                                            </ul>
                                        @endif
                                        
                                    </li>
                                @else 
                                    <li>
                                        <a href="#">{{ isset($getParentInfo) ? $getParentInfo[$key] : '' }}</a>
                                        @if ($value != 0)
                                            <ul class="sub_menu_sidebar">
                                            @foreach ($value as $val)
                                                <li>
                                                    <a href="{{ url("thong-tin/{$key}/{$val->id}") }}">{{ $val->name }}</a>
                                                </li>
                                            @endforeach 
                                            </ul>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
                 <div class="col-md-9">
                    <div class="cta-title-2">
                         <h1 class="darkblue">{{ isset($getParentInfo) ? $getParentInfo[$id] : '' }} - {{ isset($info) ? $info->name  : ''}}</h1>
                       </div>
                    <div>
                        @if(session('successes'))
                            <div id="closeButton" class="alert alert-success">
                                <button type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true" style="float: right;">&times;</span>
                                </button>
                                <ul style="display: flex;">
                                    @foreach (session('successes') as $success)
                                        <li>{{ $success }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <button id="closeButton" type="button" class="close" aria-label="Close">
                                    <span aria-hidden="true" style="float: right;">&times;</span>
                                </button>
                                <ul style="display: flex;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                       <form id="infomation-form" enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{ URL::route('front_sub_info_store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="option_code" value="{{ $subid }}">
                    <input type="hidden" name="start_time" value="{{ date('Y-m-d H:i:s') }}">
                    <input type="hidden" name="end_time" value="">
                    <input type="hidden" name="status" value="0">

                    <div class="form-group{{ $errors->has('created_author') ? ' has-error' : '' }}" style="margin-bottom: 0px !important;">
                        <label for="created_author" class="col-md-3 control-label">Người gửi<span class="color-required">*</span></label>

                        <div class="col-md-9">
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" id="errorDate" style="margin-bottom: 10px;padding-left: 0px !important;">
                                <input type="text" style="" name="created_author" value="{{ old('created_author') }}" id="created_athor" placeholder="" tabindex="3">
                                @if ($errors->has('created_author'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('created_author') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="dropfield field-wrap col-md-2 col-sm-3 col-xs-12 email_author" style="">
                                Email
                            </div>
                            <div class="dropfield field-wrap col-md-5 col-sm-3 col-xs-12" style="margin-bottom: 10px; padding-right: 0px !important; " >
                                <input type="email" style="" name="email_author" value="{{ old('email_author') }}" id="email_author" placeholder="" tabindex="4">
                                @if ($errors->has('email_author'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email_author') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-3 text-right">Tiêu đề <span class="color-required">*</span></label>

                        <div class="col-md-9">
                            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}"  tabindex="2" maxlength="500" >

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                   
                    <div class="form-group">
                        <label for="thumnail_image" class="col-md-3 text-right">Hình ảnh tóm tắt<span class="color-required">*</span></label>
                        <div class="col-md-9" style="padding-top: 10px">
                            <input tabindex="6" onchange="return fileValidation(event);" type="file" class="form-control-file" name="thumnail_image" id="thumnail_image">
                            @if ($errors->has('thumnail_image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('thumnail_image') }}</strong>
                                </span>
                            @endif
                            <div id="image-preview" style="margin-top: 10px;float: left;"></div>
                        </div>
                      </div>
                    <div class="form-group{{ $errors->has('short_description') ? ' has-error' : '' }}">
                        <label for="short_description" class="col-md-3 text-right">Nội dung tóm tắt <span class="color-required">*</span></label>
                        <div class="col-md-9" style="padding-top: 10px">
                            <textarea maxlength="500" tabindex="7" rows="5" id="short_description" name="short_description" class="form-control" style="resize: none;">{{ old('short_description') }}</textarea>
                            @if ($errors->has('short_description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('short_description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                        <label for="content" class="col-md-3 text-right">Nội dung đầy đủ <span class="color-required">*</span></label>
                        <div class="col-md-9" style="padding-top: 10px">
                            <textarea tabindex="8" rows="15" id="content_rg" name="content" class="form-control position-content tinymce">{{ old('content') }}</textarea>
                            @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-5">
                            <a href="javascript:;" class="btn btn-primary" id="submitBtn" onclick="return submitForm();" tabindex="9">Lưu</a>
                            <a href="javascript:;" class="btn btn-warning" onclick="goBack()" tabindex="10">Thoát</a>
                        </div>
                    </div>
                </form>
                    </div>
                  </div>
             </div>
          </div>
           </div>
      </div>

@endsection
<style>
    .color-required{
        color: red
    }
    .error_border {
        border-color: #a94442 !important;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
    .help-block strong {
        color: red
    }
    .expire_date{
        margin-bottom: 10px;
        padding-top: 7px;
        font-weight: bold;
        padding-left: 0px;
        padding-right: 0px;
        text-align: center;
    }
    .col-md-1{
        width: 10%;
    }
    .has-error .control-label {
        color: #3D3A3A !important
    }
    .datetime-icon {
        position: absolute;
        top: 10px;
        right: 25px;
        bottom: 0;
        font-size: 14px;
        color: #BBBABA;
    }
    .btn {
        padding: 5px !important;
        border: 1px solid transparent !important;
        border-radius: 5px;
    }

    .alert-success {
        background-color: #dff0d8 !important;
        color: #3c763d !important;
        border-color: #d0e9c6;
    }
</style>
<script type="text/javascript" src="{{ asset('js/tinymce/jquery.tinymce.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
<script>
    tinymce.init({
        selector:'textarea.tinymce',
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern codesample",
            "toc imagetools help"
        ],
        // max_word: 2000,
        // setup: function (ed) {
        //     ed.on('keyup', function (e) {
        //         var writtenWords = $('.mce-wordcount').html();
        //         writtenWords = writtenWords.replace("Words: ", "");
        //         var maxWord = ed.settings.max_word;
        //         var limited = "";
        //         var content = ed.getContent();
        //         console.log(writtenWords);
        //         if (parseInt(writtenWords) >= parseInt(maxWord)) {
        //             $('.mce-wordcount').css("color", "red");
        //             limited = $.trim(content).split(" ", maxWord);
        //             limited = limited.join(" ");
        //             ed.setContent(limited);
        //         } else {
        //             $('.mce-wordcount').css("color", "green");
        //         }
        //     });
        // },
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        // without images_upload_url set, Upload tab won't show up
        images_upload_url: '{{ URL::route("tinymce_upload") }}',
        // override default upload handler to simulate successful upload
        images_upload_handler: function (blobInfo, success, failure) {
            if (blobInfo.blob().size/1024/1024 > 10) {
                failure('Dung lượng ảnh upload phải nhỏ hơn 10Mb');
                return;
            }
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '{{ URL::route("tinymce_upload") }}');
            xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}');
            xhr.onload = function() {
                var json;
                if (xhr.status != 200) {
                    failure(xhr.status);
                    return;
                }

                json = JSON.parse(xhr.responseText);
                if (json.error) {
                    failure(json.error.image[0]);
                    return;
                }
                if (!json || typeof json.location != 'string') {
                    failure(xhr.responseText);
                    return;
                }

                success(json.location);
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        },
    });
    window.onload = function () {
        jQuery('.sub_menu_sidebar').hide(); //Hide children by default
        jQuery('.open_sidebar').show();
        jQuery('.menu_sidebar').children('li').children('a').click(function(){
            event.preventDefault();
            jQuery(this).parent().children('.sub_menu_sidebar').slideToggle('slow');     
        });
        jQuery('#closeButton').on('click', function(e) {
            jQuery('.alert').remove();
        });
    }

    function goBack() {
      window.history.back();
    }
    // Enter to submit form registerForm
    document.body.addEventListener('keydown', function(e) {
        var key = e.which;
        if (key == 13) {
            $("#registerInfo").click();
        }
    });
    var flagValidate = true;
    function fileValidation(event) {
        event.preventDefault();
        var fileInput = event.target;
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        $(fileInput).parent().find('.help-block').remove();
        $('#image-preview').html('');
        if(!allowedExtensions.exec(filePath)){
            $(fileInput).parent().append('<span class="help-block"><strong>Ảnh đại diện phải là một tập tin có định dạng: jpeg, jpg, png, gif.</strong></span>');
            fileInput.value = '';
            return false;
        } else if (fileInput.files[0].size/1024/1024 > 10) {
            mess = 'Dung lượng ảnh đại diện quá lớn: ' + fileInput.files[0].size/1024/1024 + "MB";
            $(fileInput).parent().remove('.help-block').append('<span class="help-block"><strong>'+mess+'</strong></span>');
            fileInput.value = '';
            return false;
        } else {
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#image-preview').html('<img src="'+e.target.result+'" width="100" height="80"/>');
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
            return false;
        }
        
        return false;
    }
    
    $(document).ready(function () {
        document.getElementById("option_code").focus();
    });
    function submitForm() {
        $('#submitBtn').prop('disabled', true);
        $('#infomation-form').submit();
        return false;
    }
</script>
