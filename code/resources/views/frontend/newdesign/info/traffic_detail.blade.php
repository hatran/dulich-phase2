@extends('layouts.newdesign.public')
@section('title', $detail->title)
@section('content')
<div id="full-container">
    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2">
                            <h1 class="darkblue">Thông tin giao thông</h1>
                        </div>
                        <div>
                            <div class="contentDetail">
                                <div class="row">
                                    <h2 class=""> {{$detail->title}}</h2>
                                    <p>{{ $detail->short_description }}</p>
                                    @if (!empty($detail->main_image))
                                    <img src="{{asset('images/news/' . $detail->main_image)}}" />
                                    @endif
                                    <div class="entry-content" style="word-break: break-word;">
                                        <p>{!! $detail->content !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section ('footer_embed')
<script type="text/javascript" src="{{ asset('theme/homepage/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/homepage/js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/homepage/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/scripts.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/validator.js') }}"></script>
@endsection
