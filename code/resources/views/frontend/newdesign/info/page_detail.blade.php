@extends('layouts.newdesign.public')
@section('title', $news->title)
@section('content')
<div id="full-container">
    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
                <div class="row">
                    <div class="col-md-3">
                    <div class="sidebar_title">
                        <i class="fa fa-bars"></i> DANH MỤC THÔNG TIN
                    </div>
                    <div class="nav_sidebar">
                        <ul class="menu_sidebar">
                            @foreach ($arr as $key => $value)
                                @if ($key == $id)
                                    <li>
                                        <a href="#" style="color: #ffc527;">{{ isset($getParentInfo) ? $getParentInfo[$key] : '' }}</a>
                                        @if ($value != 0)
                                            <ul class="sub_menu_sidebar open_sidebar">
                                            @foreach ($value as $val)
                                                <li>
                                                    @if ($val->id === $info->id)
                                                        <a href="{{ url("thong-tin/{$key}/{$val->id}") }}" style="color: #ffc527;">{{ $val->name }}</a>
                                                    @else
                                                        <a href="{{ url("thong-tin/{$key}/{$val->id}") }}">{{ $val->name }}</a>
                                                    @endif
                                                    
                                                </li>
                                            @endforeach 
                                            </ul>
                                        @endif
                                        
                                    </li>
                                @else 
                                    <li>
                                        <a href="#">{{ isset($getParentInfo) ? $getParentInfo[$key] : '' }}</a>
                                        @if ($value != 0)
                                            <ul class="sub_menu_sidebar">
                                            @foreach ($value as $val)
                                                <li>
                                                    <a href="{{ url("thong-tin/{$key}/{$val->id}") }}">{{ $val->name }}</a>
                                                </li>
                                            @endforeach 
                                            </ul>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
                    <div class="col-md-9">
                        <div class="cta-title-2">
                            <h1 class="darkblue"> @if ($info->id == $id)
                                {{ isset($getParentInfo) ? $getParentInfo[$id] : '' }}
                            @else
                                {{ isset($getParentInfo) ? $getParentInfo[$id] : '' }} - {{ isset($info) ? $info->name  : ''}}
                            @endif
                            </h1>
                     </div>
                     <div>
                        <div class="contentDetail">
                            <div class="row">
                                <h2 class="">{{$news->title}}</h2>
                                <div class="entry-content">
                                    <?php echo $news->content?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
</div>
@section ('footer_embed')
<script type="text/javascript" src="{{ asset('theme/homepage/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/homepage/js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/homepage/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/scripts.js') }}"></script>
<script type="text/javascript" src="{{ asset('theme/js/validator.js') }}"></script>
@endsection
<script>
    window.onload = function () {
        jQuery('.sub_menu_sidebar').hide(); //Hide children by default
        jQuery('.open_sidebar').show();
        jQuery('.menu_sidebar').children('li').children('a').click(function(){
            event.preventDefault();
            jQuery(this).parent().children('.sub_menu_sidebar').slideToggle('slow');     
        });
    }
    
</script>
