@extends('layouts.newdesign.public')
@section('title', 'Thông tin')
@section('content')

    @include('layouts.newdesign.banner')
     <div id="coached" class="flat-section coached" data-scroll-index="6">
        <div class="section-content">
            <div class="container not-partner">
            <div class="row">
                <div class="col-md-3">
                    <div class="sidebar_title">
                        <i class="fa fa-bars"></i> DANH MỤC THÔNG TIN
                    </div>
                    <div class="nav_sidebar">
                        <ul class="menu_sidebar">
                            @foreach ($arr as $key => $value)
                                @if ($key == $id)
                                    <li>
                                    <a href="{{ url("thong-tin/{$key}") }}" style="color: #ffc527;">{{ isset($getParentInfo) ? $getParentInfo[$key] : '' }}</a><i class="fa fa-list fa-fw" style="float: right; margin-top: 5px;"></i>
                                        @if (count($value) != 0)
                                            <ul class="sub_menu_sidebar open_sidebar">
                                            @foreach ($value as $val)
                                                <li>
                                                    @if ($val->id === $info->id)
                                                        <a href="{{ url("thong-tin/{$key}/{$val->id}") }}" style="color: #ffc527;">{{ $val->name }}</a>
                                                    @else
                                                        <a href="{{ url("thong-tin/{$key}/{$val->id}") }}">{{ $val->name }}</a>
                                                    @endif
                                                    
                                                </li>
                                            @endforeach 
                                            </ul>
                                        @endif
                                        
                                    </li>
                                @else 
                                    <li>
                                    <a href="{{ url("thong-tin/{$key}") }}">{{ isset($getParentInfo) ? $getParentInfo[$key] : '' }}</a><i class="fa fa-list fa-fw" style="float: right; margin-top: 5px;"></i>
                                        @if (count($value) != 0)
                                            <ul class="sub_menu_sidebar">
                                            @foreach ($value as $val)
                                                <li>
                                                    <a href="{{ url("thong-tin/{$key}/{$val->id}") }}">{{ $val->name }}</a>
                                                </li>
                                            @endforeach 
                                            </ul>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
                 <div class="col-md-9">
                    <div class="cta-title-2">
                        <h1 class="darkblue">
                            @if ($info->id == $id)
                                {{ isset($getParentInfo) ? $getParentInfo[$id] : '' }}
                            @else
                                {{ isset($getParentInfo) ? $getParentInfo[$id] : '' }} - {{ isset($info) ? $info->name  : ''}}
                            @endif
                        </h1>
                       </div>
                    <div>
                        <div class="contentDetail">
                          <ul class="home-list-4 scale-images">
                            <?php $info->id = $id == $info->id ? 0 : $info->id; ?>
                            @foreach ($listThongtin as $listtt)
                                <li class="clearfix"> <a href="{{ url("thong-tin-chi-tiet/{$id}/{$info->id}/{$listtt->id}") }}"><img class="img-responsive" src="{{ asset("images/news/{$listtt->thumnail_image}") }}"></a>
                                  <h2 class="title-new"><a href="{{ url("thong-tin-chi-tiet/{$id}/{$info->id}/{$listtt->id}") }}">{{ $listtt->title }}</a></h2>
                                  <p class="time-post">{{ $listtt->created_at }}</p>
                                  <p> {{ $listtt->short_description }} </p>
                                </li>
                            @endforeach
                          </ul>
                                  <div class="mt30 text-center paginate">
                                    {{ count($listThongtin) != 0 ? $listThongtin->links() : '' }}
                                  </div>
                            </div>
                    </div>
                  </div>
             </div>
          </div>
           </div>
      </div>

@endsection
<script>
    window.onload = function () {
        jQuery('.sub_menu_sidebar').hide(); //Hide children by default
        jQuery('.open_sidebar').show();
        jQuery('.menu_sidebar').children('li').children('.fa-fw').click(function(){
            event.preventDefault();
            jQuery(this).parent().children('.sub_menu_sidebar').slideToggle('slow');     
        });
    }
    
</script>
