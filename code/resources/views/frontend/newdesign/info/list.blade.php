@extends('layouts.newdesign.public')
@section('title', 'Thông tin')
@section('content')

    @include('layouts.newdesign.banner')
    <div id="info" class="flat-section info" data-scroll-index="3">
        <div class="section-title">
            <div class="container swiper-container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>THÔNG TIN</h2>
                    </div>
                </div>
                <div class="row mt30 col-md-12 swiper-wrapper">
                    
                        @foreach ($information as $info)
                            <div class="swiper-slide">
                                <a href="{{ URL::route('front_sub_info_list', ['id' => $info->id]) }}">
                                    <div class="wrap-thumb">
                                        @if ($info->code == 'DLVN')
                                            @if (!empty($bannerName['THONGTIN_DLVN'])&& count($bannerName['THONGTIN_DLVN']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_DLVN']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-1.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'LSVN')
                                            @if (!empty($bannerName['THONGTIN_LSVN'])&& count($bannerName['THONGTIN_LSVN']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_LSVN']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-2.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'VHVN')
                                            @if (!empty($bannerName['THONGTIN_VHVN'])&& count($bannerName['THONGTIN_VHVN']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_VHVN']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-3.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'DSVN')
                                            @if (!empty($bannerName['THONGTIN_DSVN'])&& count($bannerName['THONGTIN_DSVN']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_DSVN']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-4.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'CTVN')
                                            @if (!empty($bannerName['THONGTIN_CTVN'])&& count($bannerName['THONGTIN_CTVN']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_CTVN']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-5.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'PLVN')
                                            @if (!empty($bannerName['THONGTIN_PLVN'])&& count($bannerName['THONGTIN_PLVN']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_PLVN']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-1.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'CSHT')
                                            @if (!empty($bannerName['THONGTIN_CSHT'])&& count($bannerName['THONGTIN_CSHT']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_CSHT']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-2.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'LSVM')
                                            @if (!empty($bannerName['THONGTIN_LSVM'])&& count($bannerName['THONGTIN_LSVM']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_LSVM']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-3.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'TQDL')
                                            @if (!empty($bannerName['THONGTIN_TQDL'])&& count($bannerName['THONGTIN_TQDL']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_TQDL']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-4.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'DDDL')
                                            @if (!empty($bannerName['THONGTIN_DDDL'])&& count($bannerName['THONGTIN_DDDL']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_DDDL']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-5.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'SLTK')
                                            @if (!empty($bannerName['THONGTIN_SLTK'])&& count($bannerName['THONGTIN_SLTK']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_SLTK']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-1.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'BHDM')
                                            @if (!empty($bannerName['THONGTIN_BHDM'])&& count($bannerName['THONGTIN_BHDM']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_BHDM']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-2.jpg') }}" class="thumb">
                                            @endif
                                        @elseif ($info->code == 'TDKN')
                                            @if (!empty($bannerName['THONGTIN_TDKN'])&& count($bannerName['THONGTIN_TDKN']) > 0)
                                                <img src="{{ asset('images/banners/'.$bannerName['THONGTIN_TDKN']['profile_image']) }}" alt="" class="thumb">
                                            @else
                                                <img src="{{ asset('images/infos/img-3.jpg') }}" class="thumb">
                                            @endif
                                        @endif
                                        <span class="info">{{ $info->name }}</span></a>
                                    </div>
                                </a>
                            </div>
                        @endforeach 
                </div>
                <div class="swiper-pagination" style="position: relative;"></div>
            </div>
        </div>
    </div>

@endsection
@section('footer_embed')
<link rel="stylesheet" type="text/css" href="{{ asset('css/swiper.min.css') }}">
<script type="text/javascript" src="{{ asset('js/swiper.min.js') }}"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        slidesPerColumn: 2,
        spaceBetween: 30,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        loopAdditionalSlides:3,
    });
</script>

@endsection