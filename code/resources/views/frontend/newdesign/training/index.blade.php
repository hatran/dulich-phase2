@extends('layouts.newdesign.public')
@section('title', 'Đào Tạo')
@section('content')

    @include('layouts.newdesign.banner')
    <div id="coached" class="flat-section coached" data-scroll-index="6">

        <div class="section-content">
            <div class="container not-partner">
                @include('layouts.newdesign.slide')
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="cta-title-2"><h1 class="darkblue">Đào Tạo</h1></div>
                        <div class="main-content" style="word-wrap: break-word;">
                            Nhằm nâng cao chất lượng chuyên môn và kỹ năng hướng dẫn cho các Hội viên, hỗ trợ Hội viên trong quá trình hành nghề, Hội HDV Du lịch Việt Nam sẽ tổ chức các khóa học bổ trợ offline và online như kỹ năng thuyết trình trong du lịch, kỹ năng hoạt náo trong du lịch, kỹ năng xử lý tình huống, du lịch tâm linh…
                            <p>Các khóa học online trên trang website của Hội đang trong quá trình xây dựng, mời quý Hội viên quay lại trong thời gian tới.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
