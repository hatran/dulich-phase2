@extends('layouts.public')
@section('title', 'Giới thiệu')
@section('content')
    <ul class="breadcrumb">
        <li>
            <a href="{{ url('/') }}" style="cursor: pointer">Trang chủ</a>
        </li>
        <li>
            <a href="{{ url('/gioithieu') }}" style="cursor: pointer">Giới thiệu</a>
        </li>
    </ul>

    <div class="tabs">
        <div class="active tab" data-tab="#tamnhinsumenh">Tầm nhìn & Sứ mệnh</div>
        <div class="tab" data-tab="#banchaphanh">Ban chấp hành</div>
        <div class="tab" data-tab="#banthuongtruc">Ban Thường trực</div>
        <div class="tab" data-tab="#bancovan">Ban cố vấn</div>
        <div class="tab" data-tab="#banchuyenmon">Ban chuyên môn</div>
        <div class="tab" data-tab="#vpdaidien">Văn phòng đại diện</div>
        <div class="tab" data-tab="#quychehoatdong">Quy Chế Hoạt Động</div>
        <div class="tab" data-tab="#quydinh">Quy định</div>
        <div class="tab" data-tab="#huongdandangkyhoivien">Hướng dẫn đăng ký hội viên</div>
        <div class="tab" data-tab="#vechungtoi">Về chúng tôi</div>
    </div>

    <div id="tamnhinsumenh" class="table-content active">
        <div class="heading">
            <span class="text-heading">Sứ mệnh</span>
        </div>
        <div class="main-content">
            <p>- Tập hợp các hướng dẫn viên giỏi trên toàn quốc</p>
            <p>- Xây dựng hệ thống hành chính, tạo điều kiện thuận lợi cho việc hoạt động và sự phát triển của hướng dẫn viên</p>
            <p>- Xậy dựng hệ thống thông tin, hệ thống đào tạo toàn diện, hiện đại</p>
            <p>- Tạo sân chơi lành mạnh, thân thiện cho cộng đồng hướng dẫn viên du lịch Việt Nam</p>
            <p>- Xây dựng hệ thống Review, phản hồi của khách hàng của các Công ty lữ hành</p>
        </div>
        <div class="heading">
            <span class="text-heading">Tầm nhìn</span>
        </div>
        <div class="main-content">
            <p>- Thu hút và đào tạo nhân tài</p>
            <p>- Phát triển khung năng lực thành viên</p>
            <p>- Đưa hướng dẫn viên Du lịch Việt Nam vươn tầm quốc tế</p>
        </div>
    </div>

    <div id="banchaphanh" class="table-content">
        <div class="table-wrap">
            <table>
                <thead>
                <tr>
                    <td width="5%">STT</td>
                    <td width="15%">Họ và tên</td>
                    <td width="15%">Chức danh</td>
                    <td width="30%">Đơn vị công tác</td>
                    <td width="35%">Địa chỉ</td>
                </tr>
                </thead>
                @if (count($memberBCHList) > 0)
                    <tbody>
                    @foreach($memberBCHList as $key => $member)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $member->name }}</td>
                            <td>{{ $member->position }}</td>
                            <td>{{ $member->company }}</td>
                            <td>{{ $member->address }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>
        </div>
    </div>

    <div id="banthuongtruc" class="table-content">
        <div class="table-wrap">
            <table>
                <thead>
                <tr>
                    <td width="4%">STT</td>
                    <td width="48%">Họ và tên</td>
                    <td width="48%">Chức danh</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Trần Trọng Kiên</td>
                    <td>Chủ tịch</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Phạm Lê Thảo</td>
                    <td>Phó Chủ tịch</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div id="bancovan" class="table-content">
        <div class="table-wrap">
            <table>
                <thead>
                <tr>
                    <td width="4%">STT</td>
                    <td width="48%">Họ và tên</td>
                    <td width="48%">Chức danh</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="stt">1</td>
                    <td>Nguyễn Cường Hiền</td>
                    <td>Ủy viên</td>
                </tr>
                <tr>
                    <td class="stt">2</td>
                    <td>Nguyễn Hữu Trà</td>
                    <td>Ủy viên</td>
                </tr>
                <tr>
                    <td class="stt">3</td>
                    <td>Đào Thị Huy</td>
                    <td>Ủy viên</td>
                </tr>
                <tr>
                    <td class="stt">4</td>
                    <td>Chu Đức Ảnh</td>
                    <td>Ủy viên</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div id="banchuyenmon" class="table-content">
        <div class="heading">
            <span class="text-heading">Ban Hội viên</span>
        </div>

        <div class="main-content">
            <div class="info-wrap row">
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Trưởng ban:</span> Phạm Mạnh Cương
                </div>
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Điện thoại:</span> 0986989661
                </div>
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Email:</span> cuongpm@hoihuongdanvien.vn
                </div>
            </div>
        </div>

        <div class="heading">
            <span class="text-heading">Ban Đào tạo</span>
        </div>

        <div class="main-content">
            <div class="info-wrap row">
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Trưởng ban:</span> Nguyễn Quang Trung
                </div>
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Điện thoại:</span> 0973581219
                </div>
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Email:</span> khoitue@hoihuongdanvien.vn
                </div>
            </div>
        </div>

        <div class="heading">
            <span class="text-heading">Ban Truyền thông và Tổ chức sự kiện</span>
        </div>

        <div class="main-content">
            <div class="info-wrap row">
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Trưởng ban:</span> Trần Bích Thủy
                </div>
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Điện thoại:</span> 0912669996
                </div>
                <div class="info col-md-3 col-sm-4 col-xs-12">
                    <span>Email:</span> bichthuy.tran@hoihuongdanvien.vn
                </div>
            </div>
        </div>
    </div>

    <div id="vpdaidien" class="table-content">
        <div class="heading">
            <span class="text-heading">VPĐD tại Đà Nẵng</span>
        </div>

        <div class="main-content">
            <table class="table-line">
                <tbody>
                <tr>
                    <td width="110px">
                        <span>Địa chỉ:</span>
                    </td>
                    <td>
                        Lầu 3, số 83 Nguyễn Thị Minh Khai, Quận Hải Châu, Tp. Đà Nẵng
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Điện thoại:</span>
                    </td>
                    <td>
                        <a href="tel:02363886977">0236.3886977</a> | <a href="tel:02363886933">0236.3886933</a>
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Email:</span>
                    </td>
                    <td>
                        <a href="mailto:danang@hoihuongdanvien.vn">danang@hoihuongdanvien.vn</a>
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Trưởng VPĐD:</span>
                    </td>
                    <td>
                        Nguyễn Đình Thành
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Điện thoại:</span>
                    </td>
                    <td>
                        <a href="tel:0949455599">0949 455 599</a>
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Email:</span>
                    </td>
                    <td>
                        <a href="mailto:thanh1957dn@gmail.com">thanh1957dn@gmail.com</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="heading">
            <span class="text-heading">VPĐD tại Tp. Hồ Chí Minh</span>
        </div>

        <div class="main-content">
            <table class="table-line">
                <tbody>
                <tr>
                    <td width="110px">
                        <span>Địa chỉ:</span>
                    </td>
                    <td>
                        Số 9A Nam Quốc Cang, Q.1, Tp. Hồ Chí Minh
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Điện thoại:</span>
                    </td>
                    <td>
                        <a href="tel:028.22535139">028.22535139</a>
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Email:</span>
                    </td>
                    <td>
                        <a href="mailto:hcm@hoihuongdanvien.vn">hcm@hoihuongdanvien.vn</a>
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Trưởng VPĐD:</span>
                    </td>
                    <td>
                        Nguyễn Đức Cường
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Điện thoại:</span>
                    </td>
                    <td>
                        <a href="tel:0903706401">0903 706 401</a>
                    </td>
                </tr>
                <tr>
                    <td width="110px">
                        <span>Email:</span>
                    </td>
                    <td>
                        <a href="mailto:cuongytc@gmail.com">cuongytc@gmail.com</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div id="quychehoatdong" class="table-content">
        <div class="main-content" style="overflow-x: auto;height: 800px;line-height: 24px; ">
            <div class="chuong">
				<h2 style="font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;">Chương I</h2>
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">NHỮNG QUY ĐỊNH CHUNG</h2>			
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 1. Tên của Hội</h2>
					<p>Hội lấy tên: <b>Hội Hướng dẫn viên Du lịch Việt Nam</b></p>

					<p>Tên giao dịch tiếng Anh: <b>Vietnam Tour Guide Association</b></p>

					<p>Tên viết tắt: <b>VTGA</b></p>
				</div>
            
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 2. Tôn chỉ, mục đích của Hội</h2>
					<p>Hội Hướng dẫn viên Du lịch Việt Nam (sau đây gọi tắt là Hội) là tổ chức xã hội nghề nghiệp về hướng dẫn du lịch, bao gồm các tổ chức, công dân Việt Nam hoạt động hợp pháp trong lĩnh vực hướng dẫn du lịch.</p>

					<p>Mục đích của Hội là bồi dưỡng cập nhật kiến thức, chuyên môn, nghiệp vụ, kỹ năng hành nghề hướng dẫn cho các hội viên để nâng cao trình độ nghiệp vụ và chất lượng dịch vụ hướng dẫn du lịch; liên kết, hợp tác phát triển đội ngũ hướng dẫn viên du lịch; bảo vệ quyền, lợi ích hợp pháp của hội viên.</p>

					<ol style="list-style-type:decimal;">
					</ol>
				</div>
            
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 3. Phạm vi hoạt động của Hội</h2>
					<p>Hội Hướng dẫn viên Du lịch Việt Nam hoạt động trong phạm vi cả nước và ở nước ngoài theo quy định của pháp luật nước Cộng hòa XHCN Việt Nam và pháp luật nơi đến du lịch, Điều lệ Hiệp hội Du lịch Việt Nam và Quy chế này.</p>
				</div>
            
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 4. Địa vị pháp lý</h2>            
					<ol style="list-style-type:decimal;">
						<li>Hội Hướng dẫn viên Du lịch Việt Nam là một chi hội của Hiệp hội Du lịch Việt Nam do Hiệp hội Du lịch Việt Nam thành lập, có biểu tượng riêng, hoạt động dưới sự chỉ đạo trực tiếp của Hiệp hội Du lịch Việt Nam.</li>
						<li>Hiệp hội Du lịch Việt Nam giao cho Hiệp hội Lữ hành Việt Nam hỗ trợ các hoạt động nghề nghiệp, tham gia đào tạo, bồi dưỡng, cập nhật kiến thức chuyên môn, nghiệp vụ, kỹ năng hành nghề hướng dẫn cho các hội viên Hội Hướng dẫn viên Du lịch Việt Nam.</li>
						<li>Trụ sở chính của Hội Hướng dẫn viên Du lịch Việt Nam đặt tại Hà Nội. Hội có thể mở văn phòng đại diện, chi nhánh trong nước khi có nhu cầu.</li>
						<li>Hội Hướng dẫn viên Du lịch Việt Nam được tham gia các tổ chức quốc tế cùng nghề nghiệp, được lập văn phòng đại diện ở nước ngoài theo quy định của pháp luật.</li>
					</ol>
				</div>
			</div>
			
			<div class="chuong">
				<h2 style="font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;">Chương II</h2>
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">NHIỆM VỤ, QUYỀN HẠN CỦA HỘI</h2>			
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 5. Nhiệm vụ</h2>
					<ol style="list-style-type:decimal;">
						<li>Tuyên truyền giáo dục để hội viên nhận thức và thực hiện đúng đường lối, chủ trương, chính sách của Đảng và pháp luật của Nhà nước trong kinh doanh và cung cấp dịch vụ hướng dẫn du lịch; động viên các hội viên tích cực tham gia các hoạt động liên quan đến hướng dẫn du lịch, bồi dưỡng và đào tạo hướng dẫn viên du lịch.</li>
						<li>Tham gia các hoạt động tuyên truyền, quảng bá hình ảnh đất nước, danh lam thắng cảnh, con người, truyền thống văn hóa và lịch sử của dân tộc Việt Nam với bạn bè quốc tế; tham gia xây dựng đường lối, chính sách phát triển Du lịch Việt Nam khi được yêu cầu.</li>
						<li>Đại diện cho hội viên kiến nghị với Nhà nước về những chủ trương, chính sách, biện pháp khuyến khích, tạo điều kiện phát triển ngành Du lịch, bảo vệ quyền và lợi ích chính đáng của hội viên, giải quyết các trường hợp, vụ việc gây thiệt hại đến quyền lợi của Ngành và của hội viên theo thẩm quyền của Hội, thực hiện nghĩa vụ đối với Nhà nước theo quy định của pháp luật.</li>
						<li>Động viên sự nhiệt tình và khả năng sáng tạo của hội viên, đoàn kết, hợp tác, hỗ trợ, giúp đỡ nhau trong hoạt động cung cấp dịch vụ hướng dẫn du lịch trên cơ sở nâng cao kiến thức, trao đổi, phổ biến kinh nghiệm và ứng dụng các công nghệ tiên tiến vào nghề hướng dẫn du lịch.</li>
						<li>Tư vấn, hỗ trợ cho các hội viên trong phát triển chuyên môn nghiệp vụ, tìm kiếm việc làm. Có giải pháp cung cấp thông tin cho các hội viên. Vận động hội viên thực hiện nghiêm túc đạo đức nghề nghiệp, quy tắc ứng xử văn minh trong du lịch.</li>
						<li>Quản lý hội viên để hội viên thực hiện đúng các quy định của Luật du lịch và quy định khác của pháp luật có liên quan.</li>
						<li>Phối hợp với các tổ chức liên quan trong Hiệp hội Du lịch Việt Nam nhằm đảm bảo thực hiện tôn chỉ, mục đích của Hội, góp phần cho Hiệp hội Du lịch Việt Nam phát triển, trở thành ngôi nhà chung của các doanh nghiệp du lịch và lao động trong lĩnh vực du lịch.</li>
						
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 6. Quyền của Hội</h2>
					<ol style="list-style-type:decimal;">
						<li>Tổ chức các hội nghị, hội thảo để trao đổi kinh nghiệm nghề nghiệp, khuyến khích hợp tác liên kết giữa các Hội viên để nâng cao chất lượng hoạt động cung cấp dịch vụ hướng dẫn du lịch.</li>
						<li>Tổ chức các hoạt động đào tạo, dịch vụ, tư vấn và các hoạt động khác theo quy định của pháp luật, Điều lệ Hiệp Du lịch Việt Nam và Quy chế này.</li>
						<li>Tổ chức các cuộc thi nghiệp vụ hướng dẫn, phong tặng danh hiệu hướng dẫn viên.</li>
						<li>Nghiên cứu, tham gia góp ý, đề xuất kiến nghị với Chính phủ và các cơ quan Nhà nước những chính sách, luật pháp trong lĩnh vực du lịch nói chung và hướng dẫn du lịch nói riêng.</li>
						<li>Phát triển hội viên, xây dựng cơ sở vật chất và phát triển các mối quan hệ của Hội với các tổ chức, cá nhân trong nước và ở nước ngoài theo quy định của pháp luật, nhằm nâng cao hiệu quả và vị thế của Hội.</li>
						<li>Xuất bản tập san, các ấn phẩm và các tài liệu tuyên truyền, quảng bá về du lịch Việt Nam, trọng tâm là công tác hướng dẫn du lịch; Xây dựng hệ thống quản lý, hỗ trợ hoạt động, cung cấp thông tin, tìm kiếm việc làm cho hội viên.</li>
						<li>Được gây quỹ Hội trên cơ sở hội phí của hội viên và các nguồn thu từ hoạt động, dịch vụ, được tham gia góp vốn trong các tổ chức kinh tế hoạt động theo quy định của pháp luật để tự trang trải về kinh phí hoạt động.</li>
						<li>Thành lập các chi hội, câu lạc bộ chuyên ngành trong lĩnh vực hướng dẫn du lịch.</li>
						<li>Thực hiện các quyền khác của Hội theo quy định của pháp luật.</li>
					</ol>
				</div>
			</div>
     
			<div class="chuong">
				<h2 style="font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;">Chương III</h2>
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">HỘI VIÊN</h2>			
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 7. Hình thức Hội viên</h2>
					<ol style="list-style-type:decimal;">
						<li>Hội viên chính thức
							<p>Những người có thẻ hướng dẫn viên du lịch Việt Nam, tán thành Quy chế Hội Hướng dẫn viên Du lịch Việt Nam, tự nguyện đăng ký tham gia Hội, đóng lệ phí gia nhập và hội phí được công nhận là hội viên chính thức của Hội. Hội viên của Hội Hướng dẫn viên Du lịch Việt Nam cũng đồng thời là hội viên của Hiệp hội Du lịch Việt Nam.</p>
						</li>
						<li>Hội viên liên kết
							<p>Các tổ chức, cá nhân hoạt động trong lĩnh vực du lịch hoặc liên quan đến du lịch, có đóng góp và hỗ trợ cho hoạt động của Hội, tự nguyện đăng ký tham gia Hội, đóng lệ phí gia nhập và hội phí được công nhận là hội viên liên kết của Hội. Đối với các hội viên liên kết là tổ chức, người được cử thay mặt Hội viên liên kết tham gia Hội phải là người đại diện có thẩm quyền trong các tổ chức đó, trường hợp người được cử tham gia Hội nghỉ hưu hoặc chuyển công tác khác, thì hội viên liên kết là tổ chức phải cử người đại diện có thẩm quyền thay thế.</p>
						</li>
						<li>Hội viên danh dự
							<p>Công dân, các nhà quản lý, nhà khoa học có công lao đối với sự nghiệp phát triển Ngành du lịch nói chung và nghề Hướng dẫn du lịch nói riêng được BCH Hội mời làm Hội viên danh dự.</p>
						</li>
					</ol>
				</div>           
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 8. Điều kiện trở thành hội viên</h2>
					<ol style="list-style-type:decimal;">
						<li>Tổ chức và cá nhân nói tại khoản 1, khoản 2 Điều 7 của Quy chế này có đơn đăng ký tham gia Hội, được Ban Thường trực Hội công nhận.</li>
						<li> Hồ sơ hội viên chính thức gồm có:
							<p>- Đơn đăng ký gia nhập Hội ( theo mẫu)</p>

							<p>- Phiếu thông tin hội viên có dán ảnh chân dung mầu cỡ 3c x 4cm( theo mẫu)</p>

							<p>- Bản sao Thẻ hướng dẫn viên Du lịch</p>

							<p>- Bản sao  Chứng minh thư/Căn cước công dân</p>
						</li>
						<li>Hồ sơ hội viên liên kết gồm có:
							<ol style="list-style-type:lower-alpha;">
								<li>
									Hồ sơ hội liên kết là cá nhân:
									<p>-  Đơn đăng ký gia nhập Hội (theo mẫu)</p>

									<p>- Phiếu thông tin hội viên có dán ảnh chân dung mầu cỡ 3c x 4cm ( theo mẫu)</p>

									<p>- Bản sao có chứng thực Chứng minh thư/Căn cước công dân</p>
								</li>
								<li>
									Hồ sơ hội liên kết là cá nhân:
									<p>-  Đơn đăng ký gia nhập Hội (theo mẫu)</p>

									<p>- Phiếu thông tin hội viên có dán ảnh chân dung mầu cỡ 3c x 4cm ( theo mẫu)</p>

									<p>- Bản sao có chứng thực Chứng minh thư/Căn cước công dân</p>
								</li>
							</ol>
						</li>
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 9. Quyền của hội viên</h2>
					<ol style="list-style-type:decimal;">
						<li>Được Hội xác nhận tư cách hội viên để có đủ điều kiện hành nghề hướng dẫn du lịch.</li>
						<li>Được tham gia các tổ chức thành viên của Hội, được tham gia các hoạt động chuyên môn của Hội. Được Hội thu xếp thủ tục liên quan đến đóng bảo hiểm xã hội nếu có nhu cầu.</li>
						<li>Được kiến nghị, đề đạt ý kiến của mình với các cơ quan Nhà nước thông qua Hội.</li>
						<li>Được ứng cử, đề cử và bầu cử Ban chấp hành Hội và các tổ chức khác của Hội theo quy trình tổ chức của Hội quy định tại Quy chế này.</li>
						<li>Được tham dự các khóa phổ biến kinh nghiệm, bồi dưỡng nghề nghiệp, nâng cao trình độ, được cung cấp thông tin, tài liệu, dự hội thảo, các lớp đào tạo, huấn luyện, trình diễn kỹ thuật, tham quan khảo sát ở trong và ngoài nước theo quy trình, kế hoạch và quy định của Hội.</li>
						<li>Được Hội hỗ trợ, giới thiệu việc làm</li>
						<li>Được bảo vệ quyền và lợi ích chính đáng, hợp pháp trong hoạt động hướng dẫn du lịch.</li>
						<li> Được hưởng sự trợ giúp của các tổ chức, cá nhân là Hội viên danh dự, Hội viên liên kết của Hội.</li>
						<li> Được quyền ra khỏi Hội.</li>
						<li>Hội viên liên kết và Hội viên danh dự được hưởng các quyền như hội viên chính thức, trừ các quyền ứng cử, bầu cử và biểu quyết.</li>
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 10. Nghĩa vụ của Hội viên</h2>
					<ol style="list-style-type:decimal;">
						<li>Chấp hành đúng đường lối, chính sách của Đảng. Nghiêm túc thực hiện các quy định của Luật Du lịch và các quy định khác của pháp luật có liên quan, các quy định của Điều lệ Hiệp hội Du lịch Việt Nam, của Quy chế và các quy định của Hội Hướng dẫn viên Du lịch Việt Nam; thực hiện nghị quyết của Hội, tuyên truyền phát triển Hội viên mới. Bảo vệ uy tín của Hội, không được nhân danh Hội trong các quan hệ đối ngoại khi chưa được tổ chức có thẩm quyền của Hội ủy nhiệm.</li>
						<li> Tham gia các hoạt động và sinh hoạt của Hội, đoàn kết, hợp tác với các hội viên khác để xây dựng Hội ngày càng vững mạnh.</li>
						<li>Đeo thẻ Hội viên trong khi hành nghề hướng dẫn du lịch.</li>
						<li>Tuân thủ quy tắc đạo đức và ứng xử nghề nghiệp hướng dẫn du lịch</li>
						<li>Cung cấp thông tin, số liệu cần thiết phục vụ cho công tác quản lý, giám sát, hỗ trợ hướng dẫn viên của Hội.</li>
						<li>Đóng lệ phí gia nhập và hội phí đầy đủ.</li>
						<li>Hội viên danh dự không phải đóng lệ phí gia nhập Hội và Hội phí.</li>
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 11. Chấm dứt tư cách hội viên</h2>
					<ol style="list-style-type:decimal;">
						<li>Chấm dứt tư cách hội viên trong các trường hợp sau:
							<ol style="list-style-type:lower-alpha;">
								<li>Hội viên tự nguyện xin rút ra khỏi Hội</li>
								<li>Hội viên vi phạm các quy định của pháp luật về hướng dẫn du lịch, các điều cấm trong Luật Du lịch; vi phạm nghiêm trọng Quy chế và các quy định của Hội, làm ảnh hưởng đến uy tín và tài chính của Hội;</li>
								<li>Hội viên không đóng hội phí một năm.</li>
								<li>Hội viên bị thu hồi thẻ hướng dẫn viên du lịch.   </li>
								<li>Hội viên có thẻ hướng dẫn viên hết hạn trên 3 tháng nhưng chưa được gia hạn hay cấp mới.</li>
								<li>Những hướng dẫn viên bị chấm dứt tư cách hội viên có nguyện vọng gia nhập lại Hội làm thủ tục theo quy định của Điều 8. Những hướng dẫn viên bị chấm dứt tư cách hội viên theo Mục a Điều 11 chỉ được làm thủ tục gia nhập Hội sau 6 tháng kể từ ngày bị chấm dứt tư cách hội viên. Những hướng dẫn viên bị chấm dứt tư cách hội viên theo Mục b, c, d Điều 11 chỉ được làm thủ tục gia nhập Hội sau 12 tháng kể từ ngày bị chấm dứt tư cách hội viên.</li>
								
							</ol>
						</li>
						<li>Tạm dừng tư cách hội viên: Trường hợp thẻ hết hạn hội viên bị tạm dừng tư cách hội viên cho đến khi được cấp lại thẻ hướng dẫn.</li>
						<li>Hội viên liên kết là tổ chức có những vi phạm nghiêm trọng, bị cơ quan nhà nước có thẩm quyền rút giấy phép kinh doanh, đình chỉ hoạt động, bị giải thể hoặc tuyên bố phá sản. Trường hợp tổ chức bị đình chỉ tạm thời hoạt động trong một thời hạn thì tư cách hội viên liên kết được tiếp tục khi tổ chức được phép hoạt động trở lại.
							<p>Ban Thường trực Hội thông báo công khai danh sách hội viên bị tạm dừng hoặc chấm dứt tư cách hội viên trên trang thông tin điện tử của Hội.</p>

							<p>Quyền và nghĩa vụ của hội viên bị chấm dứt ngay sau khi Ban Thường trực Hội ra thông báo.,</p>
						</li>                    
					</ol>
				</div>
			</div>
			<div class="chuong">
				<h2 style="font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;">Chương IV</h2>
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">TỔ CHỨC HỘI</h2>			
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 12. Nguyên tắc tổ chức và hoạt động của Hội</h2>
					<ol style="list-style-type:decimal;">
						<li>Hội Hướng dẫn viên Du lịch Việt Nam được tổ chức và hoạt động theo nguyên tắc tự nguyện, tự quản, bình đẳng, tự trang trải về tài chính và tự chịu trách nhiệm trước pháp luật.</li>
						<li>Các cơ quan của Hội hoạt động trên cơ sở bàn bạc dân chủ, lãnh đạo tập thể, thiểu số phục tùng đa số.</li>
						<li>Chương trình hoạt động hàng năm của Hội Hướng dẫn viên Du lịch Việt Nam được Hiệp hội Du lịch Việt Nam phê duyệt.</li>
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 13. Cơ cấu tổ chức của Hội.</h2>
					<ol style="list-style-type:decimal;">
						<li>Đại hội Đại biểu toàn quốc.</li>
						<li>Ban Chấp hành.</li>
						<li>Ban Kiểm tra của Hội</li>
						<li>Các ban chuyên môn</li>
						<li>Văn phòng Hội</li>
						<li>Các chi hội Hướng dẫn viên du lịch tỉnh, thành phố</li>
						<li>Các câu lạc bộ hướng dẫn viên (theo ngôn ngữ, thị trường, loại hình)</li>
						<li>Các đơn vị trực thuộc Hội</li>
						<li>Các văn phòng đại diện, chi nhánh</li>
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 14. Đại hội đại biểu toàn quốc và Đại hội bất thường.</h2>
					<ol style="list-style-type:decimal;">
						<li>Đại hội đại biểu toàn quốc là cơ quan Lãnh đạo, có thẩm quyền cao nhất của Hội. Đại hội tổ chức theo nhiệm kỳ 5 năm.

	Nhiệm vụ chính của Đại hội:
						<ol style="list-style-type:lower-alpha;">
							<li>Thảo luận và thông qua báo cáo tổng kết nhiệm kỳ và phương hướng, chương trình, kế hoạch hoạt động của Hội nhiệm kỳ tới.</li>
							<li>Kiến nghị sửa đổi và bổ sung Quy chế Hội (nếu có).</li>
							<li>Thảo luận và quyết định một số vấn đề quan trọng của Hội vượt quá thẩm quyền giải quyết của Ban chấp hành Hội.</li>
							<li>Thông qua báo cáo tài chính của Hội nhiệm kỳ cũ và thông qua dự toán kế hoạch tài chính của Hội nhiệm kỳ tới.</li>
							<li>Bầu Ban Chấp hành Hội</li>
							<li>Bầu Ban Kiểm tra Hội</li>
						</ol>
					</li>
						<li>Đại hội bất thường: để giải quyết những vấn đề cấp bách của Hội, Ban chấp hành Hội có thể hỏi ý kiến của từng hội viên hoặc triệu tập Đại hội bất thường. Đại hội bất thường được triệu tập khi có 2/3 ủy viên Ban chấp hành Hội hoặc trên ½ số hội viên yêu cầu.</li>
						<li>Các Nghị quyết của Đại hội, Hội nghị, được thông qua theo nguyên tắc tập trung dân chủ, thiểu số phục tùng đa số.</li>
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 15. Ban Chấp hành Hội.</h2>
					<ol style="list-style-type:decimal;">
						<li>Ban Chấp hành (BCH) Hội là cơ quan lãnh đạo điều hành mọi hoạt động của Hội giữa 2 kỳ Đại hội. Nhiệm kỳ của Ban chấp hành theo nhiệm kỳ Đại hội. Cơ cấu của Ban Chấp hành có đại diện Hiệp hội Du lịch Việt Nam, lãnh đạo hiệp hội du lịch một số địa phương; lãnh đạo hoặc người phụ trách bộ phận hướng dẫn viên của các doanh nghiệp kinh doanh dịch vụ lữ hành tiêu biểu; các hướng dẫn viên có uy tín, có kinh nghiệm. Số lượng ủy viên Ban chấp hành Hội do Đại hội quyết định. Ban Chấp hành được bầu trực tiếp bằng phiếu kín hoặc biểu quyết bằng giơ tay và phải đạt trên 50% số đại biểu dự đại hội nhất trí. Ban Chấp hành Hội chính thức hoạt động sau khi có quyết định phê duyệt của Hiệp hội Du lịch Việt Nam.</li>
						<li>Ủy viên Ban Chấp hành Hội, phải là người có tâm huyết với Hội, có đủ trình độ và khả năng điều hành, có phẩm chất đạo đức, chính trị tốt, có sức khỏe và thời gian để gánh vác các nhiệm vụ được giao.</li>
						<li>Đối với Ủy viên Ban Chấp hành Hội là đại diện tổ chức pháp nhân, khi về hưu hoặc chuyển công tác khác sẽ được thay thế bằng một người khác của tổ chức pháp nhân đó.</li>
						<li>Nhiệm vụ, quyền hạn của Ban Chấp hành Hội:
							<ol style="list-style-type:lower-alpha;">
								<li>Quyết định biện pháp thực hiện Nghị quyết, chương trình hoạt động trong nhiệm kỳ của Đại hội.</li>
								<li>Quyết định chương trình, kế hoạch công tác hàng năm và thông báo kết quả hoạt động của Ban Chấp hành Hội cho các hội viên, Chi hội, đơn vị trực thuộc Hội biết.</li>
								<li>Phê duyệt kế hoạch và quyết toán tài chính hàng năm.</li>
								<li>Thông qua các quy định tổ chức và hoạt động các Ban Chuyên môn, Văn phòng Hội, Văn phòng đại diện của Hội tại các khu vực. Thông qua các quy định về hoạt động của hội viên.</li>
								<li>Bầu cử và bãi miễn chức danh Lãnh đạo của Hội: Chủ tịch, Phó Chủ tịch và Tổng thư ký, Trưởng Ban Kiểm tra trình Hiệp hội Du lịch Việt Nam phê duyệt.</li>
								<li>Thành lập các đơn vị trực thuộc, các Ban Chuyên môn và đại diện của Hội ở các khu vực.</li>
								<li>Thông qua nội dung, chương trình, các tài liệu trình Đại hội.</li>
								<li>Quyết định triệu tập Đại hội nhiệm kỳ.</li>
								<li>Quy định mức Lệ phí gia nhập Hội và Hội phí của Hội viên.</li>
								<li>Bầu bổ sung ủy viên BCH, thường trực BKT (nếu thiếu).</li>
								<li>Thông qua danh sách thành viên Hội đồng chuyên môn.</li>							
							</ol>
						</li>
						<li>Các Quyết định của BCH Hội được lấy biểu quyết theo đa số thành viên dự họp. Trong trường hợp số phiếu ngang nhau thì quyết định thuộc về Chủ tịch Hội.</li>
						
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 16: Ban Thường trực Hội.</h2>
					<ol style="list-style-type:decimal;">
						<li>Ban Thường trực Hội có Chủ tịch, các Phó Chủ tịch, Tổng thư ký và các ủy viên thường trực khác do Ban Chấp hành bầu.</li>
						<li>Nhiệm vụ, quyền hạn của Ban Thường trực:
							<ol style="list-style-type:lower-alpha;">
								<li>Thay mặt Ban Chấp hành chỉ đạo, điều hành các hoạt động của Hội giữa 2 kỳ họp của Ban Chấp hành.</li>
								<li>Quyết định những công việc khẩn cấp, sau đó báo cáo với Ban Chấp hành trong kỳ họp gần nhất.</li>
								<li>Phê duyệt bổ nhiệm, miễn nhiệm Chủ tịch, Phó Chủ tịch các chi hội, các câu lạc bộ; Trưởng, Phó trưởng  các ban chuyên môn, văn phòng đại diện, chi nhánh.</li>
								<li>Xét kết nạp hội viên, tạm dừng, chấm dứt tư cách hội viên</li>
								<li>Xét khen thưởng, kỷ luật hội viên</li>
							</ol>
						</li>
						<li>Các Quyết định của Ban Thường trực Hội được lấy biểu quyết theo đa số thành viên dự họp. Trong trường hợp số phiếu ngang nhau thì quyết định thuộc về Chủ tịch Hội.</li>                    
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 17. Ban Kiểm tra Hội</h2>
					<ol style="list-style-type:decimal;">
						<li> Cơ cấu Ban Kiểm tra gồm:
							<ol style="list-style-type:lower-alpha;">
								<li>Thường trực Ban Kiểm tra do Đại hội đại biểu Hội trực tiếp bầu ra. Số lượng ủy viên thường trực Ban Kiểm tra do Đại hội quyết định.</li>
								<li>Các thành viên Ban Kiểm tra tại các chi hội do lãnh đạo các chi hội đề nghị, tại các đơnvị khác do Tổng thư ký Hội đề nghị, Chủ tịch Hội ký quyết định công nhận</li>
							</ol>
						</li>
						
						<li>Nhiệm kỳ của Thường trực Ban Kiểm tra theo nhiệm kỳ của Ban Chấp hành</li>
						<li>Thường trực Ban Kiểm tra có nhiệm vụ kiểm tra, giám sát hoạt động của Hội và của Hội viên. Các thành viên Ban Kiểm tra có nhiệm vụ kiểm tra, giám sát hoạt động của Hội viên trong việc tuân thủ Pháp luật và quy định của Nhà nước trong lĩnh vực hướng dẫn du lịch, trong việc thực hiện Quy chế và các quy định của Hội.</li>
						<li> Ban Kiểm tra hoạt động theo Quy chế do Hiệp hội Du lịch Việt Nam phê duyệt. Trưởng Ban Kiểm tra xây dựng Quy chế hoạt động trình lãnh đạo Hiệp hội Du lịch Việt Nam phê duyệt.</li>                    
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 18. Chủ tịch và Phó Chủ tịch</h2>
					<ol style="list-style-type:decimal;">
						<li>Chủ tịch Hội
							<ol style="list-style-type:lower-alpha;">
								<li>Chủ tịch Hội là người quản lý, điều hành toàn bộ hoạt động của Hội, tổ chức triển khai thực hiện các Nghị quyết của Đại hội, các Nghị quyết, quyết định của Ban Chấp hành Hội, quyết định Kế hoạch công tác của Hiệp hội Du lịch Việt Nam.</li>
								<li>Là đại diện trước pháp luật của Hội</li>
								<li>Triệu tập và chủ trì các cuộc họp của Ban Chấp hành Hội</li>
								<li>Trực tiếp chỉ đạo Tổng Thư ký Hội</li>
								<li>Phê duyệt nhân sự của Văn phòng Hội và các tổ chức, đơn vị khác do Hội thành lập, phê duyệt nhân sự là thành viên Ban Kiểm tra.</li>
								<li>Chịu trách nhiệm trước Hiệp hội Du lịch Việt Nam, trước Ban Chấp hành Hội và toàn thể Hội viên về các hoạt động của Hội.</li>
							</ol>
						</li>
						<li>Phó Chủ tịch Hội</li>                    
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 19.Tổng thư ký Hội.</h2>
					<ol style="list-style-type:decimal;">
						<li>Là người tổ chức điều hành trực tiếp mọi hoạt động của Văn phòng Hội, chịu trách nhiệm trước Chủ tịch Hội, trước Ban Chấp hành Hội và trước pháp luật về mọi hoạt động của Văn phòng Hội.</li>
						<li>Xây dựng Quy chế hoạt động của Văn phòng Hội và các Ban chuyên môn của Hội, Quy chế quản lý tài chính, tài sản của Hội trình Ban Chấp hành Hội phê duyệt.</li>
						<li>Định kỳ báo cáo BCH Hội về các hoạt động của Hội và của cơ quan Hội.</li>
						<li>Lập báo cáo hàng năm, báo cáo nhiệm kỳ của Ban Chấp hành Hội</li>
						<li>Quản lý danh sách, hồ sơ và tài liệu về các hội viên và các tổ chức trực thuộc</li>
						<li>Chuẩn bị các cuộc họp của Ban Chấp hành Hội, các Hội nghị, Hội thảo do Hội tổ chức và tổ chức triển khai các hoạt động khác theo Nghị quyết, quyết định của Ban Chấp hành Hội.</li>
						<li>Thực hiện các công việc do Chủ tịch Hội ủy quyền</li>
						<li>Giúp việc Tổng thư ký có một hoặc một số Phó Tổng thư ký, do Tổng thư ký đề xuất, Chủ tịch Hội ký bổ nhiệm.</li>
						
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 20. Cơ quan Hội.</h2>
					<ol style="list-style-type:decimal;">
						<li>Cơ quan Hội bao gồm Văn phòng Hội, các ban chuyên môn, văn phòng đại diện, chi nhánh của Hội. Các đơn vị trong cơ quan Hội được tổ chức và hoạt động theo Quy chế do Tổng thư ký trình Ban Chấp hành Hội phê duyệt.</li>
						<li>Các nhân viên được tuyển dụng và làm việc theo quy định của Luật Lao động và Quy định của Hiệp hội Du lịch Việt Nam.</li>
						<li>Kế hoạch kinh phí hoạt động của Cơ quan Hội, Chi hội do Chủ tịch Hội xây dựng, trình Lãnh đạo Hiệp hội Du lịch Việt Nam phê duyệt.</li>
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 21. Câu lạc bộ chuyên ngành Hướng dẫn Du lịch.</h2>
					
					<p>Tùy theo nhu cầu hoạt động, Hội Hướng dẫn viên Du lịch Việt Nam thành lập các câu lạc bộ chuyên ngành hướng dẫn theo ngôn ngữ, theo thị trường, theo loại hình du lịch hoặc theo địa bàn.</p>
					<p>Câu lạc bộ chuyên ngành Hướng dẫn Du lịch được thành lập và hoạt động trong cơ cấu thống nhất của Hội Hướng dẫn viên Du lịch Việt Nam. Câu lạc bộ hoạt động theo quy định của pháp luật, Quy chế Hội Hướng dẫn viên Du lịch Việt Nam  và Điều lệ Hiệp hội Du lịch Việt Nam.</p>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 22. Chi hội Hướng dẫn viên Du lịch tại các tỉnh, thành phố trực thuộc Trung ương .</h2>
					<ol style="list-style-type:decimal;">
						<li>Chi hội Hướng dẫn viên Du lịch tại các tỉnh, thành phố trực thuộc Trung ương là tổ chức trực thuộc Hội Hướng dẫn viên Du lịch Việt Nam, được Hiệp hội Du lịch Việt Nam thành lập trên cơ sở đề xuất của Hiệp hội du lịch các tỉnh, thành phố.</li>
						<li>Hiệp hội du lịch các tỉnh, thành phố trực tiếp quản lý chi hội Hướng dẫn viên Du lịch về tổ chức, nhân lực; hướng dẫn hoạt động của chi hội trên địa bàn; đề xuất nhân sự với Hiệp hội Du lịch Việt Nam để Hiệp hội Du lịch Việt Nam bổ nhiệm lãnh đạo chi hội.</li>
						<li>Ban lãnh đạo chi hội có Chủ tịch, các Phó Chủ tịch, Trưởng bộ phận phụ trách công tác bồi dưỡng nghiệp vụ hướng dẫn, công tác kiểm tra, hướng dẫn viên quốc tế, hướng dẫn viên du lịch nội địa</li>
						<li>Chi hội đề xuất nhân sự tham gia Ban Kiểm tra để phối hợp với các cơ quan quản lý nhà nước về du lịch địa phương kiểm tra, giám sát các hoạt động hướng dẫn của Hội viên trên địa bàn.</li>
						<li>Chi hội có nhiệm vụ đôn đốc, hướng dẫn hội viên thực hiện Quy chế, quy định, nghị quyết, kế hoạch hoạt động của Hội hướng dẫn viên Du lịch Việt Nam; giải quyết những công việc đột xuất theo chỉ đạo của Hội hướng dẫn viên Du lịch Việt Nam.</li>
					</ol>
				</div>
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;"> Điều 23: Hội đồng chuyên môn.</h2>
					<ol style="list-style-type:decimal;">
						<li>Hội đồng chuyên môn gồm các chuyên gia trong lĩnh vực du lịch và hướng dẫn</li>
						<li>Hội đồng chuyên môn có nhiệm vụ tư vấn cho Hội trong các hoạt động theo nhiệm vụ, quyền hạn của Hội</li>                   
					</ol>
				</div>
			</div>
			<div class="chuong">
				<h2 style="font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;">Chương V</h2>
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">TÀI CHÍNH, TÀI SẢN CỦA HỘI</h2>			
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 24. Nguồn thu của Hội</h2>
					<ol style="list-style-type:decimal;">
						<li>Lệ phí gia nhập Hội.</li>
						<li>Hội phí của hội viên đóng góp theo quy định.</li>
						<li>Tài trợ của các tổ chức và cá nhân trong và ngoài nước theo quy định của pháp luật.</li>
						<li>Các khoản thu hợp pháp khác</li>
					</ol>
				</div>
				
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 25. Các khoản chi của Hội</h2>
					<ol style="list-style-type:decimal;">
						<li>Chi một phần hội phí cho hoạt động của chi hội hướng dẫn viên tại các tỉnh, thành phố. Tỷ lệ chi cho chi hội từng tỉnh, thành phố do Ban Chấp hành quy định.</li>
						<li>Chi trả lương cho nhân viên, bồi dưỡng cộng tác viên.</li>
						<li>Chi cho cơ sở vật chất kỹ thuật, giao tiếp từ thiện.</li>
						<li>Các khoản chi hợp lý khác do Ban Chấp hành Hội quyết định.</li>
					</ol>
				</div>
				
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 26. Quản lý, sử dụng tài chính và tài sản của Hội</h2>
					<ol style="list-style-type:decimal;">
						<li>Hiệp hội Du lịch Việt Nam thống nhất quản lý tài chính, ban hành quy định sử dụng ngân sách của Hội</li>
						<li>Ban Chấp hành Hội quy định việc sử dụng tài chính và tài sản của Hội phù hợp với quy định của nhà nước, của Hiệp hội Du lịch Việt Nam.</li>
						<li>Ban Kiểm tra có trách nhiệm kiểm tra và báo cáo tài chính, tài sản công khai hàng năm.</li>
						<li>Khi Hội giải thể, tài sản, tài chính của Hội phải được kiểm tra và xử lý theo quy định của pháp luật.</li>
					</ol>
				</div>
			</div>
			<div class="chuong">
				<h2 style="font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;">Chương VI</h2>
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">KHEN THƯỞNG, KỶ LUẬT</h2>			
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 27. Khen thưởng</h2>
					<p>Hội viên, Ủy viên Ban Chấp hành Hội, Ban Kiểm tra, cán bộ, nhân viên của Hội có nhiều thành tích, đóng góp tích cực vào sự phát triển của Hội và sự nghiệp phát triển ngành Du lịch, được Hội khen thưởng hoặc đề nghị Hiệp hội Du lịch Việt Nam và các cơ quan có thẩm quyền khen thưởng.</p>
				</div>
				
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 28. Kỷ luật.</h2>
					<p>Hội viên, Ủy ban Ban Chấp hành Hội, Ban Kiểm tra, cán bộ, nhân viên của Hội hoạt động trái với Quy chế, Nghị quyết của Hội, làm tổn hại đến uy tín, danh dự của Hội, bỏ sinh hoạt thường kỳ nhiều lần không có lý do chính đáng, không đóng hội phí 1 năm, tùy theo mức độ sẽ bị phê bình, khiển trách, cảnh cáo, xóa tên trong danh sách hội viên hoặc đề nghị các cơ quan có thẩm quyền xử lý vi phạm theo quy định của pháp luật.</p>
				</div>
			</div>
			<div class="chuong">
				<h2 style="font-size: 12.0pt; font-weight: bold; text-align: center; line-height: 115%;">Chương VII</h2>
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">ĐIỀU KHOẢN THI HÀNH</h2>			
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 29: Điều khoản chuyển đổii</h2>
					<p>Các Chi hội, các Câu lạc bộ hướng dẫn viên do Hiệp hội Du lịch các tỉnh, thành phố trực thuộc Trung ương thành lập trước ngày 10/10/2017 vẫn tiếp tục hoạt động. Trong thời gian 6 tháng chuyển sang cơ chế hoạt động mới quy định trong Quy chế này.</p>
				</div>
				
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 30. Sửa đổi, bổ sung Quy chế</h2>
					<p>CViệc sửa đổi, bổ sung Quy chế phải được Đại hội thông qua hoặc có ít nhất trên ½ số hội viên chính thức nhất trí kiến nghị và được Hiệp hội Du lịch Việt Nam phê duyệt mới có giá trị thực hiện.</p>
				</div>
				
				<div style="text-align:justify">
					<h2 style="font-size: 12.0pt; font-weight: bold; text-align: left; line-height: 115%;">Điều 31. Hiệu lực thi hành.</h2>
					<p>Quy chế này gồm 7 chương, 31 điều, có hiệu lực thi hành kể từ ngày được  Chủ tịch Hiệp hội Du lịch Việt Nam ký quyết định ban hành./.</p>
				</div>
			</div>
		</div>
	</div>

    <div id="quydinh" class="table-content">
        <div class="main-content">
            <h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</h2>
			<h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">MỘT SỐ QUY ĐỊNH ĐỐI VỚI HỘI VIÊN</h2>
			<h2 style="font-size: 10.0pt; font-weight: bold; text-align: center; padding-bottom: 15px;line-height: 115%;">(v/v đăng ký gia nhập Hội, tham gia Hệ thống quản lý và hỗ trợ hoạt động của
Hướng dẫn viên)</h2>
			<div style="text-align:justify">
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: left; line-height: 115%;">I-Quy định chung</h2>
				<ol style="list-style-type:decimal;">
					<li>Hội viên thường xuyên nghiên cứu thông tin, xem các thông báo trên trang web: hoihuongdanvien.vn . Hội viên tự cập nhật thông tin trên profile cá nhân trên trang website của Hội khi có thay đổi về các thông tin sau: học vấn, nghiệp vụ hướng dẫn, trình độ ngoại ngữ, quá trình hành nghề hướng dẫn, sở trường hướng dẫn.</li>
					<li>Thông báo bằng văn bản hoặc email cho Hội khi thay đổi, email, địa chỉ liên hệ trong 10 ngày kể từ khi có sự thay đổi.</li>
					<li>Thực hiện đầy đủ, có trách nhiệm các quy định trong hợp đồng hướng dẫn, trong bản phân công công việc của các doanh nghiệp, các quy định về thông tin và báo cáo kết quả công việc.</li>
					<li>Tham gia vào các chương trình đào tạo, tập huấn, hội họp, sinh hoạt chuyên môn và các buổi chia sẻ kinh nghiệm, trải nghiệm với các chuyên gia, các nhà nghiên cứu, các hướng dẫn viên giỏi… do Hội tổ chức.</li>
					<li>Có tinh thần hợp tác, hỗ trợ và chia sẻ với các hội viên khác về nghiệp vụ, kinh nghiệm và kiến thức nghề nghiệp.</li>
					<li>Không vi phạm các quy định về hướng dẫn du lịch trong Luật Du lịch, các quy định của doanh nghiệp kinh doanh dịch vụ lữ hành về tài chính, về mua bán hàng hóa, vận chuyển hàng hóa cho khách du lịch trong thời gian đi hướng dẫn du lịch.</li>
					<li>Tuân thủ quy tắc đạo đức nghề nghiệp hướng dẫn do Hội ban hành và thực hiện ứng xử văn minh khi đi hướng dẫn du lịch.</li>
					<li>Chịu sự kiểm tra của Ban Kiểm tra Hội trong khi hành nghề.</li>
					<li>Khuyến khích Hội viên đăng ký 01 số điện thoại Viettel.Viettel có gói cước ưu đãi cao cho các Hội viên.</li>
					<li>Sử dụng ứng dụng App của Hội trên điện thoại để nhận tin tức, thông tin của Hội và thực hiện giao dịch giữa Hội và hội viên, giữa hội viên với các doanh nghiệp.</li>
				</ol>
			</div>
			<div style="text-align:justify">
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: left; line-height: 115%;">II-Lệ phí gia nhập Hội và hội phí</h2>
				<ol style="list-style-type:decimal;">
					<li>Lệ phí gia nhập Hội: 500.000 đ( đóng một lần khi đăng ký Hội viên)</li>
					<li>Hội phí: 500.000 đ/năm</li>
				</ol>
			</div>
			<div style="text-align:justify">
				<h2 style="font-size: 14.0pt; font-weight: bold; text-align: left; line-height: 115%;">III- Hỗ trợ hội viên</h2>
				<ol style="list-style-type:decimal;">
					<li>Hỗ trợ của Hội HDV Du lịch Việt Nam. <br/>Hội sẽ hỗ trợ mỗi hội viên 120.000 đ tiền phí hòa mạng Viettel và phí chuyển đổi gói cước Viettel</li>
					<li>Hỗ trợ của Viettel: <br/>Ưu đãi gói cước của Viettel cho Hội viên Hội HDV Du lịch Việt Nam như sau:
						<br/>
						<table style="width:100%" class="rule">
						  <tr>
							<th rowspan="2" class="rule" style="width: 8%;">Chương trình</th>
							<th class="rule">Phí tham gia</th> 
							<th colspan="2" class="rule" style="padding: 10px 0;">Ưu đãi khuyến mại trong 12 tháng</th>
						  </tr>
						   <tr>
							<th class="rule">(Đã có VAT và cước thuê bao tháng)</th>
							<th class="rule">Ưu đãi thoại</th> 
							<th class="rule">Ưu đãi data</th>
						  </tr>
						  <tr>
							<td class="rule table-center">CT1</td>
							<td class="rule table-center">180.000đ</td>
							<td class="rule" style="padding:10px;">1000 phút thoại nội mạng, miễn phí thoại nội nhóm Hội HDV Du lịch Việt Nam</td>
							<td class="rule" style="padding:10px;">MIMAX4G (3GB hết 3GB bóp băng thông về tốc độ 256/256 Kbps), MCA</td>
						  </tr>
						  <tr>
							<td class="rule table-center">CT2</td>
							<td class="rule table-center">300.000đ</td>
							<td class="rule" style="padding:10px;">1500 phút thoại nội mạng, 100 phút ngoại mạng, miễn phí thoại nội nhóm Hội HDV Du lịch Việt Nam</td>
							<td class="rule" style="padding:10px;">4G200 (10GB, hết 10GB ngừng truy cập), MCA, Tặng thẻ Privilegde hạng thân thiết</td>
						  </tr>
						  <tr>
							<td class="rule table-center">CT3</td>
							<td class="rule table-center">500.000đ</td>
							<td class="rule" style="padding:10px;">5000 phút thoại nội mạng, 300 phút ngoại mạng, miễn phí thoại nội nhóm Hội HDV Du lịch Việt Nam</td>
							<td class="rule" style="padding:10px;">4G400 (20GB, hết 20GB ngừng truy cập), MCA, Tặng thẻ Privilegde hạng bạc</td>
						  </tr>
						  <tr>
							<td class="rule table-center">CT4</td>
							<td class="rule table-center">1.000.000đ</td>
							<td class="rule" style="padding:10px;">Miễn phí thoại nội mạng, 500 phút ngoại mạng, 1500 tin nhắn nội mạng,miễn phí thoại nội nhóm Hội HDV Du lịch Việt Nam</td>
							<td class="rule" style="padding:10px;">4G500 (30GB, hết 30GB ngừng truy cập), MCA, Tặng thẻ Privilegde hạng vàng</td>
						  </tr>
						</table>
					</li>
				</ol>
			</div>
        </div>
    </div>

    <div id="huongdandangkyhoivien" class="table-content">
        <div class="main-content">
            <h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; line-height: 115%;">HƯỚNG DẪN ĐĂNG KÝ HỘI VIÊN</h2>
            <h2 style="font-size: 14.0pt; font-weight: bold; text-align: center; padding-bottom: 15px;line-height: 115%;">HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</h2>

            <p>Bước 1: Chuẩn bị các giấy tờ cần thiết khi đăng ký gia nhập Hội viên</p>
            <p>Bước 2: Vào Website: <a href="http://www.hoihuongdanvien.vn">http://www.hoihuongdanvien.vn</a> rồi vào mục Đăng ký hội viên hoặc click <a href="http://www.hoihuongdanvien.vn/member/register">đây</a></p>
            <p>Bước 3: Người đăng ký gia nhập khai FORM trực tuyến( Đơn đăng ký gia nhập Hội Hướng dẫn viên Du lịch Việt Nam, Phiếu thông tin hội viên). Người đăng ký gia nhập upload bản chụp Thẻ Hướng dẫn viên, CMT/CCCD, các văn bằng, chứng chỉ, nhấn nút “ HOÀN THÀNH”</p>
            <p>Bước 4: Hội kiểm tra thông tin và thông báo Hồ sơ đã được phê duyệt qua email cho người đăng ký gia nhập</p>
            <p>Bước 5: Người đăng ký gia nhập download FORM từ email, in ra, dán ảnh chân dung mầu cỡ 3cm x 4 cm, ký tên và gửi về Hội/Văn phòng đại diện</p>
            <p>Bước 6: Người đăng ký gia nhập đóng lệ phí gia nhập và hội phí</p>
            <p>Hội tiếp nhận hồ sơ đăng ký gia nhập gốc, cấp thẻ hội viên</p>
			<div style="margin-top: 30px;">
				<p><span style="font-size: 10.0pt; font-weight: bold;">&#42; Các giấy tờ cần chuẩn bị khi đăng ký gia nhập Hội viên:</span></p>
				<p>1.	Thẻ hướng dẫn viên Du lịch (bản sao)</p>
				<p>2.	Chứng minh thư/căn cước công dân (bản sao )</p>
			</div>
        </div>
    </div>

    <div id="vechungtoi" class="table-content">
        <div class="main-content" style='font-family: "Times New Roman","serif";'>
		<h1 style="font-size: 14.0pt; font-weight: bold; text-align: center; padding-bottom: 15px;line-height: 115%;">GIỚI THIỆU HỘI HƯỚNG DẪN VIÊN DU LỊCH VIỆT NAM</h1>
		<p>Hướng dẫn viên là đội quân tiên phong trong ngành du lịch, là những đại sứ có vai trò rất lớn góp phần xây dựng hình ảnh của du lịch Việt Nam trong mắt bạn bè trong nước và quốc tế. Tuy nhiên hiện nay, phần lớn các hướng dẫn viên đều hành nghề tự do mà không thuộc quân số đơn vị nào.</p>
        <p>Theo quy định của Luật Du lịch sửa đổi năm 2017, có hiệu lực từ ngày 01/01/2018, hướng dẫn viên chỉ được hành nghề khi đáp ứng các điều kiện: 1. Có thẻ hướng dẫn viên du lịch; 2. Có hợp đồng lao động với doanh nghiệp kinh doanh dịch vụ lữ hành, doanh nghiệp cung cấp dịch vụ hướng dẫn du lịch hoặc là hội viên của tổ chức xã hội - nghề nghiệp về hướng dẫn du lịch đối với hướng dẫn viên du lịch quốc tế và hướng dẫn viên du lịch nội địa; 3. Có hợp đồng hướng dẫn với doanh nghiệp kinh doanh dịch vụ lữ hành hoặc văn bản phân công hướng dẫn theo chương trình du lịch; đối với hướng dẫn viên du lịch tại điểm, phải có phân công của tổ chức, cá nhân quản lý khu du lịch, điểm du lịch.</p>
        <p>Với sứ mệnh thu hút và đào tạo nhân tài trong ngành hướng dẫn đồng thời nâng tầm hướng dẫn viên Việt Nam sánh ngang với khu vực và quốc tế, <b>Hội hướng dẫn viên du lịch Việt Nam</b> được thành lập ngày 10-10- 2017, là tổ chức xã hội nghề nghiệp của những công dân Việt Nam hoạt động trong lĩnh vực hướng dẫn du lịch; đại diện cho quyền và lợi ích hợp pháp của Hội viên là HDV du lịch Việt Nam. Đồng thời, góp phần hỗ trợ hoạt động, bồi dưỡng, cập nhật kiến thức, chuyên môn, nghiệp vụ, kỹ năng hành nghề hướng dẫn cho các hội viên để nâng cao trình độ nghiệp vụ và chất lượng dịch vụ hướng dẫn du lịch; và phát triển các hoạt động liên kết, hợp tác quốc tế để đào tạo phát triển năng lực đội ngũ HDV du lịch.</p>
        <p>Tôn chỉ và giá trị cốt lõi trong các hoạt động của Hội hướng dẫn viên du lịch Việt Nam là <b>sự minh bạch, hiệu quả và bền vững</b>. Sự minh bạch trong việc xây dựng cơ chế và quản lí tài chính; <b><i>các hoạt động tài chính sẽ được kiểm toán và giám sát bởi công ty kiểm toán quốc tế Deloitte và được công bố công khai hàng năm</i></b></p>
        <p>Bên cạnh đó, tính hiệu quả, bền vững cũng được đề cao để đảm bảo tất cả cáchoạt động tập trung, tạo giá trị cho đội ngũ hướng dẫn viên; đồng thời tìm kiếm hợp tácvới các đối tác uy tín trong nước và quốc tế nhằm hỗ trợ và phát triển năng lực hội viên. Một số đối tác đã đồng hành cùng Hội bao gồm: Cộng đồng châu Âu EU, Viettel...</p>
        <p>Hệ thống thông tin hướng dẫn du lịch của Hội Hướng dẫn viên Du lịch Việt Nam ra đời sẽ hỗ trợ hiệu quả cho các hướng dẫn viên trong quá trình tác nghiệp, đồng thời tạo kết nối chặt chẽ giữa hướng dẫn viên với các doanh nghiệp du lịch. Hệ thống này cũng sẽ tạo điều kiện thuận lợi cho việc nâng cao trình độ nghiệp vụ, tạo sàn giao dịch tìm kiếm công việc, tạo sự liên kết giữa doanh nghiệp với hướng dẫn viên, góp phần nâng cao vị thế của đội ngũ hướng dẫn viên trong ngành du lịch, xây dựng hình ảnh du lịch Việt Nam trong mắt bạn bè quốc tế.</p>
		</div>
    </div>

    <style>
        #huongdandangkyhoivien h2 {
            font-weight: 700;
            text-align: center;
        }
		table.rule, th.rule, td.rule {
			border: 1px solid black;
			border-collapse: collapse;
		}
		th.rule {
			text-align: center;
		}
		.table-center {
			text-align: center;
		}
		
    </style>
@endsection
