@extends('layouts.newdesign.public')
@section('title', 'Phần thi hạng HDV ')
@section('content')

@include('layouts.newdesign.banner')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div id="coached" class="flat-section coached" data-scroll-index="6">
    <div class="section-content">
        <div class="container not-partner">
            <div class="row">
                <div class="col-md-10">
                    <div class="cta-title-2">
                       <h1 class="darkblue">Phần thi hạng HDV</h1>
                    </div>
                    <div>
                    <div class="contentDetail">
                        <div class="main-content" style="word-wrap: break-word;">
                            <div id="page-wrap">
                                <form action="{{ route('front_end_check_answer') }}" method="post" id="quiz">
                                    {{ csrf_field() }}             
                                    <ol>
                                        @foreach ($users as $key => $user)
                                        <li>
                                            @if ($loop->first || ($users[$key]['subject_knowledge_id'] != $users[$key-1]['subject_knowledge_id']))
                                                <h2>KIẾN THỨC VỀ {{ $subjectKnowledges[$user['subject_knowledge_id']]  }}</h2>
                                            @endif
                                            <h3 id="moveQ{{$key + 1}}">Câu <?php echo ($key + 1). ": "; ?>{!! $user['content'] !!}</h3>
                                            <div>
                                                <input onchange="convertColor('quizNav{{$key + 1}}')" type="radio" name="{{$user['question_id']}}" id="question-{{ $user['id'] }}-answers-1" value="1" />
                                                <label class="answer" for="question-{{ $user['id'] }}-answers-A">A) {{$user['answer1']}}</label>
                                            </div>
                                            
                                            <div>
                                                <input onchange="convertColor('quizNav{{$key + 1}}')" type="radio" name="{{$user['question_id']}}" id="question-{{ $user['id'] }}-answers-2" value="2" />
                                                <label class="answer" for="question-{{ $user['id'] }}-answers-B">B) {{$user['answer2']}}</label>
                                            </div>
                                            <div>
                                                <input onchange="convertColor('quizNav{{$key + 1}}')" type="radio" name="{{$user['question_id']}}" id="question-{{ $user['id'] }}-answers-3" value="3" />
                                                <label class="answer" for="question-{{ $user['id'] }}-answers-C">C) {{$user['answer3']}}</label>
                                            </div>
                                            <div>
                                                <input onchange="convertColor('quizNav{{$key + 1}}')" type="radio" name="{{$user['question_id']}}" id="question-{{ $user['id'] }}-answers-4" value="4" />
                                                <label class="answer" for="question-{{ $user['id'] }}-answers-D">D) {{$user['answer4']}}</label>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ol>
                                    <div style="text-align: center;">
                                        <a onclick="openModal()" href="javascript:void(0);" id="nextstep" class="btn btn-primary submit-button">Nộp</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
                    <div class="col-md-2">
                        <div id="demo"></div>
                        <div class="rightBlock">
                            <div id="quizNavigation">
                                <table>
                                    @foreach ($quizNavigations as $lineNumber => $eachLine)
                                        <tr>
                                            @foreach ($eachLine as $number)
                                                <td onclick="moveToId('moveQ{{$number}}')" class="numberBox" id="quizNav{{$number}}"><span class="numberQuestion">{{ $number }}</span></td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div id="note">
                                <p><span style="background-color: green">&nbsp&nbsp&nbsp&nbsp</span> : các câu đã hoàn thành</p>
                                <p><span style="background-color: palevioletred">&nbsp&nbsp&nbsp&nbsp</span> : các câu chưa hoàn thành</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.newdesign.test_confirm')
@endsection
@section('footer_embed')
    <script>
        function moveToId(id) {
            document.getElementById(id).scrollIntoView({
                behavior: 'smooth'
            });
        }

        function convertColor(id) {
            document.getElementById(id).style.backgroundColor = "green";
        }

        var currentDateTime = "<?php echo $now->join_time; ?>"
        // Set the date we're counting down to
        var countDownDate = new Date(currentDateTime).getTime();
        let isOk = false;
        // Update the count down every 1 second
        var x = setInterval(function() {
            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            if (seconds < 10) {
                seconds = '0'+seconds;
            }

            if (minutes < 10) {
                minutes = '0'+minutes;
            }

            if (hours < 10) {
                hours = '0'+hours;
            }
            // Display the result in the element with id="demo"
            document.getElementById("demo").innerHTML =  hours + ":"
            + minutes + ':' + seconds ;

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "Hết giờ!";
                isOk = true;
                document.getElementById("quiz").submit();
            }
        }, 1000);

        window.onbeforeunload = confirmExit;
        function confirmExit()
        {
            if (! isOk) {
                return "Are you sure you want to exit this page?";
            }
            return;
        }

        function openModal() {
            jQuery('#confirmSubmitQuiz').modal();
        }

        function submitQuiz() {
            isOk = true;
            jQuery('#quiz').submit();
        }

        function continueQuiz() {
            isOk = false;
        }
    </script>
@endsection

<style>
    h1 { 
        margin: 25px 0; 
        font-size: 14px;
        text-transform: uppercase; 
        letter-spacing: 3px; 
    }

    #quiz input {
        vertical-align: middle;
        margin: 0px !important;
        width: auto !important;
        height: auto !important;
    }

    #quiz ol {
        margin: 0 0 10px 20px;
        padding-left: 0px !important;
    }

    #quiz ol li {
        /*   margin: 0 0 20px 0;*/
        list-style-type: none;
    }

    #quiz ol li h3 {
        color: #0355b3
    }

    #quiz ol li div {
        padding: 4px 0;
    }

    #quiz h3 {
        font-size: 17px; margin: 0 0 1px 0; color: #666;
    }

    .cta-title-2 h1::after {
        width: 100% !important;
        margin: 0px !important;
    }
    
    #demo, #quizNavigation {
        float: right;
        position: fixed;
        right: 40px;
        font-size: 30px;
        color: #0355b3
    }

    #note {
        float: right;
        position: fixed;
        right: 30px;
        padding-top: 220px;
    }
    
    .answer {
        display: initial !important;
    }

    .submit-button {
        padding: 6px 12px !important;
        border: 2px solid #ffffff !important;
    }

    #quizNavigation {
        height: 210px;
        overflow: auto;
        z-index: 999;
    }

    .rightBlock {
        margin-top: 60px;
    }

    .numberBox {
        background-color: pink;
        border: 1px solid white;
        cursor: pointer;
    }

    .numberQuestion {
        color: white;
    }
</style>