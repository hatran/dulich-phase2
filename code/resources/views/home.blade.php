@extends('layouts.public')

@section('content')
    <section id="banner">
        <div id="home" class="banner-parallax" data-scroll-index="0">
            <div class="banner-slider">
                <ul class="owl-carousel slider-img-bg">
                    <li>
                        <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
                        <div class="overlay-pattern" data-bg-color="#000" data-bg-color-opacity="0"></div>
                        <div class="slide">
                            <img src="images/banners/001.jpg">
                            <div class="slide-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-0">
                                            <div class="banner-center-box">
                                                <h1>
                                                    Góp phần phát triển Du lịch Việt Nam<span class="colored"> trở thành ngành kinh tế mũi nhọn</span>
                                                </h1>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
                        <div class="overlay-pattern" data-bg-color="#000" data-bg-color-opacity="0"></div>
                        <div class="slide">
                            <img src="images/banners/002.jpg">
                            <div class="slide-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-0">
                                            <div class="banner-center-box">
                                                <h1>
                                                    Tập hợp các<span class="colored"> hướng dẫn viên giỏi </span> trên toàn quốc
                                                </h1>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
                        <div class="overlay-pattern" data-bg-color="#000" data-bg-color-opacity="0"></div>
                        <div class="slide">
                            <img src="images/banners/003.jpg">
                            <div class="slide-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-0">
                                            <div class="banner-center-box">
                                                <h1>
                                                    Xây dựng<br><span class="colored">hệ thống ABC XYZ</span>
                                                </h1>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
                        <div class="overlay-pattern" data-bg-color="#000" data-bg-color-opacity="0"></div>
                        <div class="slide">
                            <img src="images/banners/004.jpg">
                            <div class="slide-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-0">
                                            <div class="banner-center-box">
                                                <h1>
                                                    Xây dựng<br><span class="colored">hệ thống ABC XYZ</span>
                                                </h1>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
                        <div class="overlay-pattern" data-bg-color="#000" data-bg-color-opacity="0"></div>
                        <div class="slide">
                            <img src="images/banners/005.jpg">
                            <div class="slide-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-0">
                                            <div class="banner-center-box">
                                                <h1>
                                                    Xây dựng<br><span class="colored">hệ thống ABC XYZ</span>
                                                </h1>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="overlay-colored" data-bg-color="#000" data-bg-color-opacity="0.30"></div>
                        <div class="overlay-pattern" data-bg-color="#000" data-bg-color-opacity="0"></div>
                        <div class="slide">
                            <img src="images/banners/006.jpg">
                            <div class="slide-content">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-0">
                                            <div class="banner-center-box">
                                                <h1>
                                                    Xây dựng<br><span class="colored">hệ thống ABC XYZ</span>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="login-box">
            <h4 class="title">Đăng nhập</h4>
            <form class="form frmLogin" id="frmChangePass">
                <div class="form-group">
                    <input placeholder="Tên truy cập" type="text" name="tentruycap" id="bill-number" class="form-control" autocomplete="off">
                    <span class="validate" id=""></span>
                </div>
                <div class="form-group">
                    <input placeholder="Mật khẩu" type="password" name="matkhau" id="bill-code" class="form-control" autocomplete="off">
                    <span class="validate" id=""></span>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label><input type="checkbox" value="">Ghi nhớ mật khẩu</label>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn small colorful hover-transparent-colorful">Đăng nhập</button>
                </div>
                <div class="form-group text-center"><label class="small">Hoặc</label></div>
                <div class="form-group">
                    <button class="btn small white hover-transparent-white">Đăng ký hội viên</button>
                </div>

            </form>
            <div class="clearfix"></div>
        </div>
    </section>

    <section id="content">
        <div id="content-wrap">
            <div id="intro" class="flat-section intro bg-grey" data-scroll-index="1">
                <div class="section-title">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>GIỚI THIỆU</h2>
                                <ul class="intro-tabs">
                                    <li><a href="#"><i class="fa fa-bank" aria-hidden="true"></i>Về chúng tôi</a></li>
                                    <li><a href="#"><i class="fa fa-flag" aria-hidden="true"></i>Tầm nhìn & Sứ mệnh</a></li>
                                    <li><a href="#"><i class="fa fa-book" aria-hidden="true"></i>Quy chế hoạt động</a></li>
                                    <li><a href="#"><i class="fa fa-gavel" aria-hidden="true"></i>Quy định</a></li>
                                    <li><a href="#"><i class="fa fa-info" aria-hidden="true"></i>Hướng dẫn đăng ký</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="news" class="flat-section popular-packages" data-scroll-index="1">
                <div class="section-title">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>TIN TỨC NỔI BẬT</h2>
                                <div class="popular-packages-slider">
                                    <ul class="highlight-news text-left owl-carousel">
                                        <li class="">
                                            <div class="box-preview">
                                                <div class="box-img img-bg">
                                                    <a href="#"><img src="images/news/img-1.jpg" alt=""></a>
                                                </div>
                                                <div class="box-content">
                                                    <h5><a href="#">Honeymoon</a></h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been indust.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="">
                                            <div class="box-preview">
                                                <div class="box-img img-bg">
                                                    <a href="#"><img src="images/news/img-2.jpg" alt=""></a>
                                                </div>
                                                <div class="box-content">
                                                    <h5><a href="#">Mountain tour</a></h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been indust.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="">
                                            <div class="box-preview">
                                                <div class="box-img img-bg">
                                                    <a href="#"><img src="images/news/img-3.jpg" alt=""></a>
                                                </div>
                                                <div class="box-content">
                                                    <h5><a href="#">Medical Tour</a></h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been indust.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="">
                                            <div class="box-preview">
                                                <div class="box-img img-bg">
                                                    <a href="#"><img src="images/news/img-3.jpg" alt=""></a>
                                                </div>
                                                <div class="box-content">
                                                    <h5><a href="#">Medical Tour</a></h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been indust.</p>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="">
                                            <div class="box-preview">
                                                <div class="box-img img-bg">
                                                    <a href="#"><img src="images/news/img-3.jpg" alt=""></a>
                                                </div>
                                                <div class="box-content">
                                                    <h5><a href="#">Medical Tour</a></h5>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been indust.</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="offices" class="flat-section offices bg-grey" data-scroll-index="2">
                <div class="section-title">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>VĂN PHÒNG ĐẠI DIỆN</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row offices-container">
                        <div class="col-md-8">
                            <div class="office-item">
                                <div class="preview img-bg">
                                    <img src="images/offices/img-1.jpg" alt="">
                                </div>
                                <a class="overlay" href="#">
                                    <i class="fa fa-info"></i>
                                    <div class="overlay-inner">
                                        <h5>Hội sở</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-item">
                                <div class="preview img-bg">
                                    <img src="images/offices/img-2.jpg" alt="">
                                </div>
                                <a class="overlay" href="#">
                                    <i class="fa fa-info"></i>
                                    <div class="overlay-inner">
                                        <h5>TP. Hồ Chí Minh</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-item">
                                <div class="preview img-bg">
                                    <img src="images/offices/img-3.jpg" alt="">
                                </div>
                                <a class="overlay" href="#">
                                    <i class="fa fa-info"></i>
                                    <div class="overlay-inner">
                                        <h5>TP. Đà Nẵng</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-item">
                                <div class="preview img-bg">
                                    <img src="images/offices/img-4.jpg" alt="">
                                </div>
                                <a class="overlay" href="#">
                                    <i class="fa fa-info"></i>
                                    <div class="overlay-inner">
                                        <h5>TP. Hồ Chí Minh</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-item">
                                <div class="preview img-bg">
                                    <img src="images/offices/img-5.jpg" alt="">
                                </div>
                                <a class="overlay" href="#">
                                    <i class="fa fa-picture-o"></i>
                                    <div class="overlay-inner">
                                        <h5>TP. Đà Nẵng</h5>
                                        <h5 class="sub-title">Văn phòng đại diện</h5>
                                    </div>
                                </a>
                                <div class="detail-link">
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="info" class="flat-section info" data-scroll-index="3">
                <div class="section-title">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>THÔNG TIN</h2>
                            </div>
                        </div>
                        <div class="row mt30">
                            <div class="col-xs-12 col-sm-4 p0">
                                <div class="wrap-thumb"><a href="/places-to-go/central-vietnam/da-nang"><img src="images/infos/img-1.jpg" class="thumb"><span class="info">Phong tục tập quán</span></a></div>
                            </div>
                            <div class="col-xs-12 col-sm-4 p0">
                                <div class="wrap-thumb"><a href="/places-to-go/central-vietnam/hoi-an"><img src="images/infos/img-5.jpg" class="thumb"><span class="info">Trang phục</span></a></div>
                            </div>
                            <div class="col-xs-12 col-sm-4 p0">
                                <div class="wrap-thumb"><a href="/places-to-go/central-vietnam/nha-trang"><img src="images/infos/img-7.jpg" class="thumb"><span class="info">Lễ hội</span></a></div>
                            </div>
                            <div class="col-xs-12 col-sm-4 p0">
                                <div class="wrap-thumb"><a href="/node/101"><img src="images/infos/img-4.jpg" class="thumb"><span class="info">Điểm đến</span></a></div>
                            </div>
                            <div class="col-xs-12 col-sm-4 p0">
                                <div class="wrap-thumb"><a href="/places-to-go/southern-vietnam/mui-ne"><img src="images/infos/img-6.jpg" class="thumb"><span class="info">Giao thông</span></a></div>
                            </div>
                            <div class="col-xs-12 col-sm-4 p0">
                                <div class="wrap-thumb"><a href="/places-to-go/central-vietnam/dalat"><img src="images/infos/img-2.jpg" class="thumb"><span class="info">Tour Dalat</span></a></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
