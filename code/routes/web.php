<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Frontend area
Route::namespace('Frontend')->group(function () {
    Route::get('/', 'FrontEndController@index')->name('front_end_index');
    Route::get('/gioithieu', 'FrontEndController@gioithieu')->name('front_end_gioithieu');
    Route::get('/chihoi', 'FrontEndController@chihoi')->name('front_end_chihoi');
    Route::get('/clb', 'FrontEndController@clb')->name('front_end_clb');
    // Route::get('/news', 'FrontEndController@news')->name('front_end_news');
    // Route::get('/news/{alias}', 'FrontEndController@newsDetail')->name('front_end_news_detail');
    Route::get('/hoivien', 'FrontEndController@hoivien')->name('front_end_hoivien');
    Route::get('/redirect', 'FrontEndController@redirect')->name('front_end_redirect');

    Route::prefix('ajax')->group(function () {
        Route::get('/get-news/{type}/{id}', 'FrontEndController@ajaxGetNews')->where(['type' => '[0-9A-Za-z\-]+', 'id', '\d+'])->name('front_end_ajax_get_news');
        // ajaxmember dung chung cho CH, CLB
        Route::any('/ajax-get-member/{id}/{type}', 'FrontEndController@ajaxGetHoiVien')->where('id', '\d+')->name('front_end_ajax_get_member');
        Route::any('/ajax-get-member-ext/{id}/{type}', 'FrontEndController@ajaxGetHoiVienExtend')->where('id', '\d+')->name('front_end_ajax_get_member_ext');
        Route::any('/club-of-branch/{id}', 'FrontEndController@ajaxClubOfBranches')->where('id', '\d+')->name('front_end_ajax_club_of_branches');
        Route::any('/get-content-club-of-branch/{id}', 'FrontEndController@ajaxClbGetContent')->where('id', '\d+')->name('ajax_get_content_club_of_branch');
        Route::get('get-detail-news/{id}', 'FrontEndController@ajaxGetDetailNews')->where(['type' => '[0-9A-Za-z\-]+', 'id', '\d+'])->name('front_end_ajax_get_detail_news');
    });

    // Co cau to chuc
    Route::get('/banchuyenmon', 'OrganizeStructureController@specialBoard')->name('front_end_special_board');
    Route::get('/banthuongtruc', 'OrganizeStructureController@standingCommittee')->name('front_end_standing_committee');
    Route::get('/bancovan', 'OrganizeStructureController@advisoryBoard')->name('front_end_standing_advisory_board');
    Route::get('/banchaphanh', 'OrganizeStructureController@executiveCommittee')->name('front_end_executive_committee');

    // Tin tuc
    Route::get('/news', 'NewsController@news')->name('front_end_news');
    Route::get('/news/{alias}', 'NewsController@detail')->name('front_end_news_detail');
    Route::get('/news2/{alias}', 'NewsController@detail2')->name('front_end_news_detail2');
    Route::get('/tintucvasukienhoi', 'NewsController@news')->name('front_end_news_1');
    Route::get('/tintucvasukienchihoi/{id}', 'NewsController@news2')->name('front_end_news_2');
    // Route::get('/tintucvasukienclbthuochoi', 'NewsController@news3')->name('front_end_news_3');

    // Hoi vien
    Route::get('/hoivien', 'MemberController@hoivien')->name('front_end_hoivien');
    Route::get('/hoivienAjax', 'MemberController@ajax')->name('front_end_hoivien');

    // Van phong dai dien
    Route::get('/vanphongdaidien', 'BranchesController@hoivien')->name('front_end_vanphongdaidien');
    // Tin tuc
    Route::get('/vechungtoi', 'IntroductionsController@vechungtoi')->name('front_vechungtoi');
    Route::get('/tamnhinvasumenh', 'IntroductionsController@tamnhinvasumenh')->name('front_tamnhinvasumenh');
    Route::get('/quychehoatdong', 'IntroductionsController@quychehoatdong')->name('front_quychehoatdong');
    Route::get('/quydinh', 'IntroductionsController@quydinh')->name('front_quydinh');
    Route::get('/huongdandangky', 'IntroductionsController@huongdandangky')->name('front_huongdandangky');


    // Thông tin Giao thông
    Route::get('giaothong', 'NewsController@trafficList')->name('front_info_traffic');
    Route::get('giaothong/{id}', 'NewsController@trafficDetail')->where('id', '\d+')->name('front_info_traffic_detail');
    // Thông tin Lễ hội
    Route::get('lehoi', 'NewsController@festivalList')->name('front_info_lehoi');
    Route::get('lehoi/{id}', 'NewsController@festivalDetail')->where('id', '\d+')->name('front_info_lehoi_detail');
    // Thông tin Điểm đến
    Route::get('diemden', 'NewsController@destinationList')->name('front_info_diemden');
    Route::get('diemden/{id}', 'NewsController@destinationDetail')->where('id', '\d+')->name('front_info_diemden_detail');
    // Thông tin Trang phục
    Route::get('trangphuc', 'NewsController@costumeList')->name('front_info_trangphuc');
    Route::get('trangphuc/{id}', 'NewsController@costumeDetail')->where('id', '\d+')->name('front_info_trangphuc_detail');
    // Phong tục tập quán
    Route::get('phongtuctapquan', 'NewsController@customandpracticesList')->name('front_info_customandpractices');
    Route::get('phongtuctapquan/{id}', 'NewsController@customandpracticesDetail')->where('id', '\d+')->name('front_info_customandpractices_detail');
    // Danh sach thong tin chung
    Route::get('thong-tin', 'NewsController@infoList')->name('front_info_list');
    // đào tạo
    Route::get('/training', 'TrainingController@index')->name('front_end_training');
    // Sàn giao dịch việc làm
    Route::get('/worktrading', 'WorkTradingController@index')->name('front_end_work_trading');
    // Xếp hạng HDV
    Route::get('/survey', 'SurveyControllerController@index')->name('front_end_survey');
    Route::get('/rankcriteria', 'SurveyControllerController@criteria')->name('front_end_criteria');
    Route::get('/rankregister', 'SurveyControllerController@register')->name('front_end_register');
    Route::get('/rankscore', 'SurveyControllerController@score')->name('front_end_score');
    Route::get('/infoquestion', 'SurveyControllerController@infoQuestion')->name('front_end_infoquestion')->middleware(['quizAuthenticate']);
    Route::get('/inforegister', 'SurveyControllerController@infoRegister')->name('front_end_inforegister');
    Route::get('thong-tin/{id}/{subid?}', 'NewsController@subInfoList')->name('front_sub_info_list');
    Route::get('thong-tin-chi-tiet/{id}/{subid}/{newsid}', 'NewsController@subInfoDetail')->name('front_sub_info_detail');
    Route::post('/create', 'NewsController@storeNews')->name('front_sub_info_store');
    Route::post('/inforegister', 'SurveyControllerController@updateInfoRegister')->name('front_sub_update_inforegister')->middleware(['quizAuthenticate']);
    Route::post('/inforegisternotmember', 'SurveyControllerController@updateInfoRegisterNotMember')->name('front_sub_update_inforegister_not_member');
    Route::get('/quiz', 'SurveyControllerController@quiz')->name('front_end_quiz')->middleware(['quizAuthenticate']);
    Route::post('/checkanswer', 'SurveyControllerController@checkAnswer')->name('front_end_check_answer');
    Route::get('/joinquiz', 'SurveyControllerController@joinQuiz')->name('front_end_joinquiz')->middleware(['quizAuthenticate']);
    Route::any('/change-password', 'MemberController@showChangePasswordForm')->name('user_change_password');
    Route::post('forgot-password', 'MemberController@forgotPassword')->name('user_forgot_password');
});

Route::get('/profile/edit', 'MemberController@showEdit')->name('profile_edit')->middleware(\App\Http\Middleware\ProfileAccess::class);
Route::get('/profile', 'MemberController@showDetails')->name('profile')->middleware(\App\Http\Middleware\ProfileAccess::class);
Route::get('/rankinfo', 'Admin\MemberEvaluationController@rankInfo')->name('front_end_rank_info');

Route::prefix('member')->group(function () {
    Route::get('/register', 'MemberController@getRegister')->name('member_register_view');
    Route::post('/register', 'MemberController@postRegister')->name('member_register_action');
    Route::post('/register/resend_sms', 'MemberController@reSendSmsPhoneToken')->name('member_register_resend_sms_action');
    Route::get('/download', 'MemberController@downloadContractFile')->name('download');
    Route::middleware('HiAcl')->get('/download-zip', 'MemberController@downloadZipFolder')->name('downloadZip');
    Route::get('/rejected/{id}', 'MemberController@showRejected')->middleware(['signedurl'])->name('member_rejected');
    Route::get('/update/{id}/edit', 'MemberController@showEdit')->middleware(['signedurl'])->name('member_update');
    Route::post('/update/{id}/submit', 'MemberController@updateProfile')->name('member_update');
    Route::post('/update/{id}/submit_ad', 'MemberController@updateProfileAd')->name('member_update_test');
    Route::get('/update/{id}/detail', 'MemberController@showDetails')->middleware(['signedurl'])->name('member_detail');
    Route::middleware('HiAcl')->get('/regenerate-and-download-zip', 'MemberController@regenerateAndDownloadZipFolder')->name('regenerateAndDownloadZip');
});

Route::prefix('company')->group(function () {
    Route::get('/evaluate', 'Admin\TravelerCompanyRankController@evaluate')->name('company_evaluate');
    Route::get('/evaluate/create/{id}', 'Admin\MemberEvaluationController@createEvaluation')->name('company_create_evaluation');
    Route::post('/evaluate/create', 'Admin\MemberEvaluationController@store')->name('company_create_evaluation.store');
    Route::get('/evaluate/list', 'Admin\MemberEvaluationController@list')->name('company_evaluate_list');
    Route::get('/evaluate/show/{id}/{tourist_code}', 'Admin\MemberEvaluationController@show')->name('company_evaluate_show');
    Route::get('/evaluate/listfrontend', 'Admin\MemberEvaluationController@listFrontend')->name('company_evaluate_list_frontend');
    Route::get('/evaluate/listfrontend/{id}', 'Admin\MemberEvaluationController@listFrontendMember')->name('company_evaluate_list_frontend_by_member');
});
// Admin area without ACL auth and HiAcl
Route::namespace('Admin')->prefix('officesys')->middleware(['auth'])->group(function () {
    Route::prefix('users')->group(function () {
        Route::any('/change-password', 'UserController@showChangePasswordForm')->name('admin_user_change_password');
        Route::any('/profile', 'UserController@showProfile')->name('admin_user_profile');
    });
});

// Admin area with ACL auth and HiAcl
Route::namespace('Admin')->prefix('officesys')->middleware(['auth', 'HiAcl'])->group(function () {
    // member area
    Route::prefix('member')->group(function () {
        Route::any('/', 'MemberController@index')->name('admin_member');
        Route::get('/detail/{id}', 'MemberController@show')->name('admin_member_detail_view');
        Route::get('/check_member', 'MemberController@checkMember')->name('admin_member_detail_view_ajax');
        Route::post('/detail/{id}', 'MemberController@update')->name('admin_member_detail_update_view');
        Route::get('/detail_tmp/{id}', 'MemberController@showtmp')->name('admin_member_detail_tmp_view');
        Route::post('/detail_tmp/{id}', 'MemberController@update')->name('admin_member_tmp_detail_update_view');
        Route::get('/detailUser', 'MemberController@apiDetailUser')->name('admin_member_apiDetailUser_view');
        Route::get('/changeStatus', 'MemberController@apiChangeStatus')->name('admin_member_apiChangeStatus_view');
        Route::get('/create_card', 'MemberController@createMemberCard')->name('admin_member_create_card_view');
        Route::get('/create_card/{id}', 'MemberController@doCreateMemberCard')->name('admin_member_do_create_card_view');
        Route::get('/export_card_list', 'MemberController@exportExcelCardList')->name('admin_member_print_excel_member_card_view');
        Route::get('/gen_pdf/{id}', 'MemberController@genPdf')->name('admin_gen_pdf');
        Route::post('/fakeSendMailApprove/{id}', 'MemberController@fakeSendMailApprove')->name('fakeSendMailApprove');
        Route::get('/providingcode', 'MemberController@providingCodeMany')->name('admin_member_providing_code_many');
        Route::get('/print_card_selected', 'MemberController@printCardSelected')->name('admin_member_print_card_selected_view');
        Route::post('add_print_card_selected', 'MemberController@addPrintCardSelected')->name('admin_member_add_print_card_selected');
        Route::post('delete_print_card_selected', 'MemberController@deletePrintCardSelected')->name('admin_member_delete_print_card_selected');
    });

    Route::prefix('payment_yearly')->group(function () {
        Route::post('/notifications/send', 'MemberYearlyPaymentController@sendNotification')->name('payment_yearly_send_notifications');
        Route::any('/excel/import', 'MemberYearlyPaymentController@importExcel')->name('admin_payment_fee_yearly_import_view');
        Route::get('/resend-notification', 'MemberYearlyPaymentController@resendNotification')->name('payment_yearly.resend_notification');
        Route::get('/resend-notification-email', 'MemberYearlyPaymentController@resendNotificationEmail')->name('payment_yearly.resend_notification_email');
        Route::get('/{id}', 'MemberYearlyPaymentController@show')->name('admin_member_detail_payment_yearly_view');
        Route::post('/{id}', 'MemberYearlyPaymentController@store')->name('payment_yearly_store');
        Route::post('/update/{id}', 'MemberYearlyPaymentController@update')->name('payment_yearly_update');
        Route::post('/destroy/{id}', 'PaymentYearlyNewController@destroy')->name('payment_yearly_destroy');
        Route::any('/excel/export_excel', 'MemberYearlyPaymentController@exportExcel')->name('admin_payment_fee_yearly_export_view');
    });

    Route::prefix('payment_yearly_new')->group(function () {      
        Route::get('/search_payment_first', 'PaymentYearlyNewController@searchPaymentFirst')->name('admin_member_list_payment_view');
        Route::get('/search_payment_yearly', 'PaymentYearlyNewController@searchPaymentYearly')->name('admin_member_list_payment_yearly_view');
    });

    Route::prefix('payment')->group(function () {
        // Route::get('/', 'MemberPaymentController@index')->name('admin_member_list_payment_view');
        Route::get('/{id}', 'MemberPaymentController@show')->name('admin_member_detail_payment_view');
        Route::post('/{id}', 'MemberPaymentController@store')->name('payment_store');
        Route::post('/update/{id}', 'MemberPaymentController@update')->name('payment_update');
        Route::post('/destroy/{numberpayment}', 'MemberPaymentController@destroy')->name('payment_destroy');
        Route::post('/notifications/send', 'MemberPaymentController@sendNotification')->name('payment_send_notifications');
        Route::any('/excel/import', 'MemberPaymentController@importExcel')->name('admin_payment_fee_import_view');
        Route::get('/email/send-notification-remind-completing-fee', 'MemberPaymentController@sendNotificationEmailRemindCompletingFee')->name('admin_list_member_send_notification_email_remind_completing_fee');
        Route::get('/sms/send-notification-remind-completing-fee', 'MemberPaymentController@sendNotificationSmsRemindCompletingFee')->name('admin_list_member_send_notification_sms_remind_completing_fee');
    });

    // decision/{id}/delete
    Route::prefix('decision')->group(function () {
        Route::get('/', 'MemberDecisionController@index')->name('admin_member_list_decision_view');
        Route::get('/create_report/{id}', 'MemberDecisionController@doCreateMemberCard')->name('member_decision_do_create_card_view');
        Route::post('/{id}', 'MemberDecisionController@update')->name('admin_member_update_decision_view');
        Route::delete('/{id}', 'MemberDecisionController@destroy')->name('decision');
        Route::get('/get_detail/{id}', 'MemberDecisionController@getDetail')->name('admin_member_get_detail_decision_view');
        Route::get('/checkprovidingdecision/{id}', 'MemberDecisionController@checkprovidingdecision')->name('admin_member_check_providing_decision');
        Route::get('/providingdecision', 'MemberDecisionController@providingDecisionMany')->name('admin_member_providing_decision_many');
    });

    // decision/{id}/delete
    Route::prefix('list_members')->group(function () {
        Route::get('/', 'ListMemberController@index')->name('admin_member_list_view');
        Route::get('/print_file/{id}', 'ListMemberController@printFile')->name('list_member_print_file');
        Route::get('/detail/{id}', 'ListMemberController@show')->name('admin_list_member_detail_view');
        Route::delete('/{id}', 'ListMemberController@destroy')->name('delete_member');
        Route::post('/{id}', 'ListMemberController@update')->name('update_member');
        Route::get('/export-excel', 'ListMemberController@exportExcel')->name('admin_list_member_export_view');
        Route::get('/export-excel-code', 'ListMemberController@exportExcelCode')->name('admin_list_member_export_code_view');
        Route::post('/note/{id}', 'ListMemberController@saveNoteMember')->name('update_member');
        Route::get('/note/{id}', 'ListMemberController@getNoteMember')->name('update_member');
        Route::post('/member-note/{id}', 'ListMemberController@storeMemberNote')->name('store_member_note');
        //export
        Route::get('/list_export', 'ListMemberController@listExport')->name('admin_member_new_list_export_view');
        Route::get('/get_member_for_list/{id}', 'ListMemberController@getMemberForList')->name('admin_list_member_get_member_for_list');
        Route::post('/updatetouristcode/{id}', 'ListMemberController@updateTouristCode')->name('admin_list_member_update_tourist_code');
        Route::post('/update/{id}/member_file', 'ListMemberController@updateMemberFileInfo')->name('admin_list_member_update_file');
        Route::get('/notofficialmember', 'ListMemberController@listNotOfficialMember')->name('admin_member_list_not_official');
        Route::get('/send-notification-email-to-not-member', 'ListMemberController@sendNotificationEmailToNotMember')->name('admin_list_member_send_notification_email_to_not_member');
        Route::get('/send-notification-sms-to-not-member', 'ListMemberController@sendNotificationSmsToNotMember')->name('admin_list_member_send_notification_sms_to_not_member');
        Route::get('member_file/{id}/profile_edit', 'MemberController@showEdit')->name('admin_member_profile_edit');
        Route::get('/general_report', 'ListMemberController@generalReport')->name('admin_member_general_report_view');
        Route::get('/export_general_report', 'ListMemberController@exportGeneralReport')->name('admin_list_member_export_general_report');
        Route::get('/send-member-card-expiration', 'MemberCardExpirationController@sendExpirationEmail')->name('member_card_expiration_email');
    });

    Route::prefix('editor')->group(function () {
        Route::get('/listmember', 'ListMemberController@listMemberByEditor')->name('admin_editor_listmember');
    });

    Route::prefix('rank_members')->group(function () {
        Route::get('/', 'ListMemberController@indexRank')->name('admin_member_rank_view');
        Route::get('/detail/{id}', 'MemberController@showIndexRank')->name('admin_member_detail_rank_view');
        Route::get('/fileaward/{member_id}/{rank_id}', 'ListMemberController@getScoreFileAward')->name('admin_member_rank_score_file_award');
        Route::post('/updatescore', 'MemberRankController@updateScoreFileAndAward')->name('admin_member_rank_update_score');
        Route::get('/delete/{id}', 'MemberRankController@deleteMemberRank')->name('admin_member_rank_delete');
        Route::get('/download-file', 'MemberRankController@downloadfile')->name('admin_member_rank_download_file');
    });

    Route::prefix('representative_office')->group(function () {
        Route::get('/', 'RepresentativeOfficeController@index')->name('admin_representative_office_view');
        Route::get('/view/{id}', 'RepresentativeOfficeController@show')->name('admin_representative_office_edit');
        Route::get('/delete', 'RepresentativeOfficeController@destroy')->name('admin_representative_office_delete');
        Route::post('/', 'RepresentativeOfficeController@update')->name('admin_representative_office_update');
        // START Quản lý lãnh đạo
        Route::get('/manager/{id}', 'RepresentativeOfficeController@manager')->where('id', '\d+')->name('admin_representative_office_manager');
        Route::post('/manager/ajax-save-manager', 'RepresentativeOfficeController@ajaxSaveMember')->name('admin_representative_office_save_manager');
        Route::post('/manager/ajax-check-valid-add-new-office', 'RepresentativeOfficeController@ajaxCheckValidAddNewOffice')->name('admin_representative_office_check_valid_add_new_office');
    });

    Route::prefix('employee')->group(function () {
        Route::get('/', 'EmployeeController@index')->name('admin_employee_view');
        Route::get('/view/{id}', 'EmployeeController@show')->name('admin_employee_edit');
        Route::get('/delete', 'EmployeeController@destroy')->name('admin_employee_delete');
        Route::post('/', 'EmployeeController@update')->name('admin_employee_update');
        Route::get('/create', 'EmployeeController@create')->name('admin_employee_create');
        Route::post('/create', 'EmployeeController@doCreate')->name('admin_employee_doCreate');
    });

    Route::prefix('users')->group(function () {
        /*Route::any('/change-password', 'UserController@showChangePasswordForm')->name('admin_user_change_password');
        Route::any('/profile', 'UserController@showProfile')->name('admin_user_profile');*/
        Route::get('/', 'UserController@index')->name('users.index');
        Route::get('/create', 'UserController@create')->name('users.create');
        Route::post('/create', 'UserController@store')->name('users.store');
        Route::post('{id}/delete', 'UserController@destroy')->name('users.destroy');
        Route::get('{id}', 'UserController@show')->name('users.show');
        Route::post('{id}', 'UserController@update')->name('users.update');
        Route::post('/reset-password/{id}', 'UserController@resetPassword')->name('admin_user_reset_password');
        Route::get('/create/not-member', 'UserController@createNotMember')->name('users_create_notmember');
        Route::post('/create/not-member', 'UserController@storeNotMember')->name('users_store_notmember');
    });
    // Route::resource('/users', 'UserController');
    Route::resource('/introductions', 'IntroductionController');
    Route::resource('/categories', 'CategoryController');
    Route::resource('/banners', 'BannerController');
    Route::resource('/emails', 'EmailController');

    Route::prefix('history')->group(function () {
        Route::get('/', 'HistoryController@index')->name('admin_history_view');
        Route::get('/ajax','HistoryController@ajax')->name('admin_history_ajax');
        Route::get('/email_sms', 'HistoryEmailSMSController@index')->name('admin_history_emailsms_view');
        Route::get('/email/ajax', 'HistoryEmailSMSController@ajax')->name('admin_history_emailsms_ajax');
        Route::get('/email/send/{id}', 'HistoryEmailSMSController@reSendEmail')->name('admin_history_email_resend');
        Route::get('/sms/send/{id}', 'HistoryEmailSMSController@reSendSms')->name('admin_history_sms_resend');
    });

    Route::prefix('introductions')->group(function () {
        Route::get('/detail/{id}', 'IntroductionController@detail')->name('admin_introductions_detail');
        Route::get('/check_status/{id}/{status}/{option_code}', 'IntroductionController@checkStatus')->name('admin_introductions_checkstatus');
    });

    Route::prefix('emails')->group(function () {
        Route::get('/detail/{id}', 'EmailController@detail')->name('admin_emails_detail');
        Route::get('/check_status/{id}/{status}/{option_code}', 'EmailController@checkStatus')->name('admin_emails_checkstatus');
        Route::get('/fee/{id}', 'EmailController@getListFee')->name('admin_email_get_list_fee');
    });

    Route::prefix('categories')->group(function () {
        Route::get('/{id}/{option}', 'CategoryController@showEdit')->name('admin_categories_sedit_detail');
        Route::get('/deleteall/{id}/{option}', 'CategoryController@deleteOption')->name('admin_categories_deletdit_detail');
        Route::post('/check_category', 'CategoryController@saveCategory')->name('admin_categories_savedeletdit_detail');
        Route::get('/{id}/{option}/{code}', 'CategoryController@checkCode')->name('admin_categories_checkcode_detail');
    });

    Route::prefix('fortetour')->group(function () {
        Route::get('/', 'ForteTourController@index')->name('fortetour.index');
        Route::get('/{id}/{option}', 'ForteTourController@showEdit')->name('admin_fortetour_sedit_detail');
        Route::get('/deleteall/{id}/{option}', 'ForteTourController@deleteOption')->name('admin_fortetour_deletdit_detail');
        Route::post('/check_forte', 'ForteTourController@saveForte')->name('admin_fortetour_savedeletdit_detail');
    });

    Route::prefix('subjectknowledge')->group(function () {
        Route::get('/', 'SubjectKnowledgeController@index')->name('subjectknowledge.index');
        Route::get('/{id}', 'SubjectKnowledgeController@showEdit')->name('admin_subjectknowledge_sedit_detail');
        Route::get('/deleteall/{id}', 'SubjectKnowledgeController@deleteOption')->name('admin_subjectknowledge_deletdit_detail');
        Route::post('/check_subject', 'SubjectKnowledgeController@saveSubject')->name('admin_subjectknowledge_savedeletdit_detail');
    });

    Route::prefix('travelercompanyrank')->group(function () {
        Route::get('/', 'TravelerCompanyRankController@index')->name('travelercompanyrank.index');
        Route::get('/{id}', 'TravelerCompanyRankController@showEdit')->name('admin_travelercompanyrank_sedit_detail');
        Route::get('/deleteall/{id}', 'TravelerCompanyRankController@deleteOption')->name('admin_travelercompanyrank_deletdit_detail');
        Route::post('/check_traveler', 'TravelerCompanyRankController@saveTravelers')->name('admin_travelercompanyrank_savedeletdit_detail');
    });

    Route::prefix('evaluationcriteria')->group(function () {
        Route::get('/', 'EvaluationCriteriaController@index')->name('evaluationcriteria.index');
        Route::get('/{id}', 'EvaluationCriteriaController@showEdit')->name('admin_evaluationcriteria_sedit_detail');
        Route::get('/deleteall/{id}', 'EvaluationCriteriaController@deleteOption')->name('admin_evaluationcriteria_deletdit_detail');
        Route::post('/check_evaluation', 'EvaluationCriteriaController@saveEvaluations')->name('admin_evaluationcriteria_savedeletdit_detail');
    });

    Route::prefix('question')->group(function () {
        Route::get('/', 'QuestionController@index')->name('question.index');
        Route::get('/deleteall/{id}', 'QuestionController@deleteOption')->name('admin_question_deletdit_detail');
        Route::post('/check_question', 'QuestionController@saveQuestion')->name('admin_question_savedeletdit_detail');
        Route::get('/create', 'QuestionController@create')->name('question.create');
        Route::get('/{id}/show', 'QuestionController@show')->name('question.show')->where(['id' => '\d+']);
        Route::get('/{id}/edit', 'QuestionController@edit')->name('question.edit')->where(['id' => '\d+']);
        Route::post('/addsubjectknowledge', 'QuestionController@ajaxAddSubjectKnowledge')->name('ajax_question_add_subject_knowledge');
        Route::post('/checkcode', 'QuestionController@ajaxCheckCode')->name('ajax_question_check_code');
        Route::get('/openquiz', 'QuestionController@openQuizStatus')->name('admin.openquiz');
        Route::get('/updatequiz', 'QuestionController@updateQuizStatus')->name('admin.updatequiz');
        Route::post('/importexcel', 'QuestionController@importExcel')->name('admin.question_importexcel');
        Route::get('/exportexcel', 'QuestionController@exportExcel')->name('admin.question_exportexcel');
    });

    Route::prefix('informationpage')->group(function () {
        Route::get('/', 'InformationPageController@index')->name('informationpage.index');
        Route::get('/{id}/{option}', 'InformationPageController@showEdit')->name('admin_informationpage_sedit_detail');
        Route::get('/deleteall/{id}/{option}', 'InformationPageController@deleteOption')->name('admin_informationpage_deletdit_detail');
        Route::post('/check_informationpage', 'InformationPageController@saveInformationPage')->name('admin_informationpage_savedeletdit_detail');
        Route::get('/{id}/{option}/{code}', 'InformationPageController@checkCode')->name('admin_informationpage_checkcode_detail');
    });

    Route::prefix('banners')->group(function () {
        Route::post('/create_banner', 'BannerController@createBanner')->name('admin_create_banner_update');
        Route::post('/update_banner', 'BannerController@updateBanner')->name('admin_update_banner_update');
        Route::get('/{id}', 'BannerController@show')->name('admin_banners_sedit_detail');
        Route::get('/deleteall/{id}', 'BannerController@deleteBanner')->name('admin_banner_deletdit_detail');
        Route::get('/check_banner/{id}/{nameAll}', 'BannerController@saveBanner')->name('admin_banners_savedeletdit_detail');
    });

    Route::prefix('branches')->group(function() {
        // Câu lạc bộ thuộc hội
        Route::prefix('club-of-head')->group(function() {
            Route::get('/', 'ClubOfHeadController@index')->name('club_of_head_index');
            Route::get('/create', 'ClubOfHeadController@create')->name('club_of_head_create');
            Route::post('/', 'ClubOfHeadController@store')->where('id', '\d+')->name('club_of_head_store');
            Route::get('/{id}/edit', 'ClubOfHeadController@edit')->where('id', '\d+')->name('club_of_head_edit');
            Route::post('/{id}', 'ClubOfHeadController@update')->where('id', '\d+')->name('club_of_head_update');
            Route::get('/{id}', 'ClubOfHeadController@show')->where('id', '\d+')->name('club_of_head_show');
            Route::post('/{id}/delete', 'ClubOfHeadController@destroy')->where('id', '\d+')->name('club_of_head_delete');
            // START Quản lý lãnh đạo
            Route::get('/manager/{id}', 'ClubOfHeadController@branchManager')->where('id', '\d+')->name('club_of_head_manager');
            Route::post('/manager/ajax-save-manager', 'ClubOfHeadController@ajaxSaveMember')->name('club_of_head_ajax_save_member');
            // END Quản lý lãnh đạo
            Route::get('/ajax-get-clb', 'ClubOfHeadController@ajaxGetCLB')->name('club_of_head_ajax_get_clb');
            Route::get('/export-excel/{id}', 'ClubOfHeadController@exportExcel')->name('club_of_head_export_excel');
        });

        // Cau lac bo thuoc chi hoi
        Route::prefix('club-of-branch')->group(function() {
            Route::get('/', 'ClubOfBranchController@index')->name('club_of_branch_index');
            Route::get('/create', 'ClubOfBranchController@create')->name('club_of_branch_create');
            Route::post('/', 'ClubOfBranchController@store')->name('club_of_branch_create_store');
            Route::get('/{id}/edit', 'ClubOfBranchController@edit')->where('id', '\d+')->name('club_of_branch_edit');
            Route::post('/{id}', 'ClubOfBranchController@update')->where('id', '\d+')->name('club_of_branch_update');
            Route::get('/{id}', 'ClubOfBranchController@show')->where('id', '\d+')->name('club_of_branch_show');
            Route::post('/{id}/delete', 'ClubOfBranchController@destroy')->where('id', '\d+')->name('club_of_branch_delete');

            // START Quản lý lãnh đạo
            Route::get('/manager/{id}', 'ClubOfBranchController@branchManager')->where('id', '\d+')->name('club_of_branches_manager');
            Route::post('/manager/ajax-save-manager', 'ClubOfBranchController@ajaxSaveMember')->name('club_of_branches_ajax_save_member');
            // END Quản lý lãnh đạo
            // START Member
            Route::get('/member/{id}', 'ClubOfBranchController@indexMember')->where('id', '\d+')->name('club_of_branch_member_index');
            Route::get('/ajax-search-member', 'ClubOfBranchController@ajaxSearchMember')->name('club_of_branch_member_ajax_search');
            Route::get('/member/{id}/create', 'ClubOfBranchController@createMember')->where('id', '\d+')->name('club_of_branch_member_create');
            Route::post('/member/{id}/create', 'ClubOfBranchController@storeMember')->where('id', '\d+')->name('club_of_branch_member_create');
            Route::get('/export-excel/{id}', 'ClubOfBranchController@exportExcel')->where('id', '\d+')->name('club_of_branch_export_excel');
            Route::post('/member/{id}/delete', 'ClubOfBranchController@destroyMember')->where('id', '\d+')->name('club_of_branch_member_delete');
            Route::get('/ajax-get-clb', 'ClubOfBranchController@ajaxGetCLB')->name('club_of_branch_ajax_get_clb');
        });
        // END Member

        // branches
        Route::get('/manager/{id}', 'BranchesController@branchManager')->where('id', '\d+')->name('branches_manager');
        Route::get('/manager/ajax-employees', 'BranchesController@ajaxFindMember')->name('branches_ajax_employees');
        Route::get('/export-excel/{id}', 'BranchesController@exportExcel')->where('id', '\d+')->name('branches_export_excel');
        Route::post('/manager/ajax-save-manager', 'BranchesController@ajaxSaveMember')->name('branches_ajax_save_member');
        Route::post('/{id}/delete', 'BranchesController@destroy')->where('id', '\d+')->name('branch_destroy');
        Route::post('/{id}/edit', 'BranchesController@update')->name('branches_update')->where(['id' => '\d+']);
    });

    Route::resource('branches', 'BranchesController');

    // infomation
    Route::prefix('infomation')->group(function() {
        Route::get('/', 'InfomationController@index')->name('infomation.index');
        Route::get('/create', 'InfomationController@create')->name('infomation.create');
        Route::post('/create', 'InfomationController@store')->name('infomation.store');
        Route::get('/{id}/show', 'InfomationController@show')->name('infomation.show')->where(['id' => '\d+']);
        Route::get('/{id}/edit', 'InfomationController@edit')->name('infomation.edit')->where(['id' => '\d+']);
        Route::post('/{id}/edit', 'InfomationController@update')->name('infomation.update')->where(['id' => '\d+']);
        Route::post('/{id}/delete', 'InfomationController@destroy')->name('infomation.destroy')->where(['id' => '\d+']);
        Route::post('/addsubinfo', 'InfomationController@ajaxAddSubInfo')->name('ajax_add_sub_info');
    });

    // news + sk hội
    Route::prefix('news')->group(function() {
        Route::get('/', 'NewsController@index')->name('admin.news_ch_clb.index');
        Route::get('/create', 'NewsController@create')->name('admin.news_ch_clb.create');
        Route::post('/create', 'NewsController@store')->name('admin.news_ch_clb.store');
        Route::get('/{id}/show', 'NewsController@show')->name('admin.news_ch_clb.show')->where(['id' => '\d+']);
        Route::get('/{id}/edit', 'NewsController@edit')->name('admin.news_ch_clb.edit')->where(['id' => '\d+']);
        Route::post('/{id}/edit', 'NewsController@update')->name('admin.news_ch_clb.update')->where(['id' => '\d+']);
        Route::post('/{id}/delete', 'NewsController@destroy')->name('admin.news_ch_clb.destroy')->where(['id' => '\d+']);
    });
    // news + sk Chi hội
    Route::prefix('news-branches')->group(function() {
        Route::get('/', 'NewsController@indexBranches')->name('admin.news_ch.index');
        Route::get('/create', 'NewsController@createBranches')->name('admin.news_ch.create');
        Route::post('/create', 'NewsController@storeBranches')->name('admin.news_ch.store');
        Route::get('/{id}/show', 'NewsController@showBranches')->name('admin.news_ch.show')->where(['id' => '\d+']);
        Route::get('/{id}/edit', 'NewsController@editBranches')->name('admin.news_ch.edit')->where(['id' => '\d+']);
        Route::post('/{id}/edit', 'NewsController@updateBranches')->name('admin.news_ch.update')->where(['id' => '\d+']);
        Route::post('/{id}/delete', 'NewsController@destroyBranches')->name('admin.news_ch.destroy')->where(['id' => '\d+']);
    });
    // news + sk CLB
    Route::prefix('news-club')->group(function() {
        Route::get('/', 'NewsController@indexClub')->name('admin.news_clb.index');
        Route::get('/create', 'NewsController@createClub')->name('admin.news_clb.create');
        Route::post('/create', 'NewsController@storeClub')->name('admin.news_clb.store');
        Route::get('/{id}/show', 'NewsController@showClub')->name('admin.news_clb.show')->where(['id' => '\d+']);
        Route::get('/{id}/edit', 'NewsController@editClub')->name('admin.news_clb.edit')->where(['id' => '\d+']);
        Route::post('/{id}/edit', 'NewsController@updateClub')->name('admin.news_clb.update')->where(['id' => '\d+']);
        Route::post('/{id}/delete', 'NewsController@destroyClub')->name('admin.news_clb.destroy')->where(['id' => '\d+']);
    });

    // permisstion manager
    Route::prefix('groups')->group(function() {
        Route::get('/', 'GroupsController@index')->name('groups.index');
        // Route::get('/gen_router_history', 'GroupsController@genRouterHistory')->name('groups.gen_router_history');
        Route::get('/create', 'GroupsController@create')->name('groups.create');
        Route::get('/{id}/show', 'GroupsController@show')->name('groups.show');
        Route::post('/create', 'GroupsController@store')->name('groups.store');
        Route::get('/{id}/edit', 'GroupsController@edit')->name('groups.edit');
        Route::post('/{id}/edit', 'GroupsController@updated')->name('groups.update');
        Route::post('/{id}/delete', 'GroupsController@delete')->name('groups.destroy');
    });

    // Quan tri quyen truy cap
    Route::prefix('group-role')->group(function() {
        Route::get('/', 'GroupRoleController@index')->name('group_role.index');
        // Route::get('/gen_router_history', 'GroupRoleController@genRouterHistory')->name('group_role.gen_router_history');
        Route::get('/create', 'GroupRoleController@create')->name('group_role.create');
        Route::post('/create', 'GroupRoleController@store')->name('group_role.store');
        Route::get('/{id}/edit', 'GroupRoleController@edit')->name('group_role.edit');
        Route::post('/{id}/edit', 'GroupRoleController@updated')->name('group_role.update');
        Route::post('/{id}/delete', 'GroupRoleController@delete')->name('group_role.destroy');
    });

    // Chinh sach duoc uu dai phi
            Route::prefix('fee-policy')->group(function () {
                Route::get('/', 'FeePolicyController@index')->name('fee_policy.index');
                Route::get('/create', 'FeePolicyController@create')->name('fee_policy.create');
                Route::post('/create', 'FeePolicyController@store')->name('fee_policy.store');
                Route::post('{id}/delete', 'FeePolicyController@destroy')->name('fee_policy.destroy');
                Route::get('{id}', 'FeePolicyController@show')->name('fee_policy.show');
                Route::post('{id}', 'FeePolicyController@update')->name('fee_policy.update');
                Route::post('{/update', 'FeePolicyController@saveList')->name('fee_policy.saveList');
                Route::post('{/storeupdate', 'FeePolicyController@storeUpdate')->name('fee_policy.storeupdate');
                Route::get('/checkCode/{code}', 'FeePolicyController@checkCode')->name('fee_policy.create');
            });

    Route::get('/sendnoti', 'MemberController@sendNotificationToMember')->name('admin.sendnoti');
    Route::post('/sendnotipost', 'MemberController@sendNotificationToMemberPost')->name('admin.sendnoti_post');
});
Route::post('/tinymce-upload', 'TinyMceController@tinyMceUpload')->name('tinymce_upload');
//Noi dang ky sinh hoat
Route::post('/searchactivityregistrationplace', 'Controller@searchByActivityRegistrationPlace')->name('list_member_search');
Route::namespace('Api\v1')->group(function () {
    Route::resource('upload', 'UploadController')->only(['store']);
});
Route::namespace('Api\v1')->prefix('api/v1')->group(function () {
    Route::get('vietios/verifyofficalmember/{touristCode}', 'VerifyOfficalMemberController@index')->name('verifyofficalmember');
});