<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'FrontEndController@index')->name('front_end_index');
Route::get('/gioithieu', 'FrontEndController@gioithieu')->name('front_end_gioithieu');
Route::get('/chihoi', 'FrontEndController@chihoi')->name('front_end_chihoi');
Route::get('/clb', 'FrontEndController@clb')->name('front_end_clb');
Route::get('/news', 'FrontEndController@news')->name('front_end_news');
Route::get('/news/{alias}', 'FrontEndController@newsDetail')->name('front_end_news_detail');
Route::get('/hoivien', 'FrontEndController@hoivien')->name('front_end_hoivien');
Route::get('/redirect', 'FrontEndController@redirect')->name('front_end_redirect');

Route::prefix('ajax')->group(function () {
    Route::get('/get-news/{type}/{id}', 'FrontEndController@ajaxGetNews')->where(['type' => '[0-9A-Za-z\-]+', 'id', '\d+'])->name('front_end_ajax_get_news');
    // ajaxmember dung chung cho CH, CLB
    Route::any('/ajax-get-member/{id}/{type}', 'FrontEndController@ajaxGetHoiVien')->where('id', '\d+')->name('front_end_ajax_get_member');
    Route::any('/club-of-branch/{id}', 'FrontEndController@ajaxClubOfBranches')->where('id', '\d+')->name('front_end_ajax_club_of_branches');
    Route::any('/get-content-club-of-branch/{id}', 'FrontEndController@ajaxClbGetContent')->where('id', '\d+')->name('ajax_get_content_club_of_branch');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile/edit', 'MemberController@showEdit')->name('profile_edit')->middleware(\App\Http\Middleware\ProfileAccess::class);
Route::get('/profile', 'MemberController@showDetails')->name('profile')->middleware(\App\Http\Middleware\ProfileAccess::class);

Route::prefix('member')->group(function () {
    Route::get('/register', 'MemberController@getRegister')->name('member_register_view');
    Route::post('/register', 'MemberController@postRegister')->name('member_register_action');
    Route::post('/register/resend_sms', 'MemberController@reSendSmsPhoneToken')->name('member_register_resend_sms_action');
    Route::get('/download', 'MemberController@downloadContractFile')->name('download');
    Route::get('/download-zip', 'MemberController@downloadZipFolder')->name('downloadZip');
    Route::get('/rejected/{id}', 'MemberController@showRejected')->middleware(['signedurl'])->name('member_rejected');
    Route::get('/update/{id}/edit', 'MemberController@showEdit')->middleware(['signedurl'])->name('member_update');
    Route::post('/update/{id}/submit', 'MemberController@updateProfile')->name('member_update');
    Route::get('/update/{id}/detail', 'MemberController@showDetails')->middleware(['signedurl'])->name('member_detail');
});

// Admin area
Route::namespace('Admin')->prefix('officesys')->middleware(['auth', 'aclDenied:normal'])->group(function () {
    // member area
    Route::prefix('member')->group(function () {
        Route::any('/', 'MemberController@index')->name('admin_member');
        Route::get('/detail/{id}', 'MemberController@show')->name('admin_member_detail_view');
        Route::get('/check_member', 'MemberController@checkMember')->name('admin_member_detail_view_ajax');
        Route::post('/detail/{id}', 'MemberController@update')->name('admin_member_detail_update_view');
        Route::get('/detail_tmp/{id}', 'MemberController@showtmp')->name('admin_member_detail_tmp_view');
        Route::post('/detail_tmp/{id}', 'MemberController@update')->name('admin_member_detail_update_view');
        Route::middleware(['acl:cardIssuer'])->get('/detailUser', 'MemberController@apiDetailUser')->name('admin_member_apiDetailUser_view');
        Route::middleware(['acl:cardIssuer'])->get('/changeStatus', 'MemberController@apiChangeStatus')->name('admin_member_apiChangeStatus_view');
        Route::middleware(['acl:cardIssuer'])->get('/create_card', 'MemberController@createMemberCard')->name('admin_member_create_card_view');
        Route::middleware(['acl:cardIssuer,accountant'])->get('/create_card/{id}', 'MemberController@doCreateMemberCard')->name('admin_member_do_create_card_view');
        Route::middleware(['acl:cardIssuer'])->get('/export_card_list', 'MemberController@exportExcelCardList')->name('admin_member_print_excel_member_card_view');
        Route::get('/gen_pdf/{id}', 'MemberController@genPdf')->name('admin_gen_pdf');
        Route::get('/fakeSendMailApprove/{id}', 'MemberController@fakeSendMailApprove')->name('fakeSendMailApprove');
    });

    Route::prefix('payment')->group(function () {
        Route::middleware(['acl:accountant'])->group(function () {
            Route::get('/', 'MemberPaymentController@index')->name('admin_member_list_payment_view');
            Route::get('/{id}', 'MemberPaymentController@show')->name('admin_member_detail_payment_view');
            Route::post('/{id}', 'MemberPaymentController@store')->name('payment_store');
            Route::put('/{id}', 'MemberPaymentController@update')->name('payment_update');
            Route::delete('/{id}', 'MemberPaymentController@destroy')->name('payment_destroy');
            Route::post('/notifications/send', 'MemberPaymentController@sendNotification')->name('payment_send_notifications');
            Route::any('/excel/import', 'MemberPaymentController@importExcel')->name('admin_payment_fee_import_view');
        });
    });

    // decision/{id}/delete
    Route::middleware(['acl:cardIssuer'])->prefix('decision')->group(function () {
        Route::get('/', 'MemberDecisionController@index')->name('admin_member_list_decision_view');
        Route::get('/create_report/{id}', 'MemberDecisionController@doCreateMemberCard')->name('member_decision_do_create_card_view');
        Route::post('/{id}', 'MemberDecisionController@update')->name('admin_member_update_decision_view');
        Route::delete('/{id}', 'MemberDecisionController@destroy')->name('decision');
    });

    // decision/{id}/delete
    Route::prefix('list_members')->group(function () {

        Route::get('/', 'ListMemberController@index')->name('admin_member_list_view');
        Route::get('/print_file/{id}', 'ListMemberController@printFile')->name('list_member_print_file');
        Route::get('/detail/{id}', 'ListMemberController@show')->name('admin_list_member_detail_view');
        Route::delete('/{id}', 'ListMemberController@destroy')->name('delete_member');
        Route::post('/{id}', 'ListMemberController@update')->name('update_member');


    });

    Route::prefix('representative_office')->group(function () {
        Route::get('/', 'RepresentativeOfficeController@index')->name('admin_representative_office_view');
        Route::get('/view/{id}', 'RepresentativeOfficeController@show')->name('admin_representative_office_edit');
        Route::get('/delete', 'RepresentativeOfficeController@destroy')->name('admin_representative_office_delete');
        Route::post('/', 'RepresentativeOfficeController@update')->name('admin_representative_office_update');
        // START Quản lý lãnh đạo
        Route::get('/manager/{id}', 'RepresentativeOfficeController@manager')->where('id', '\d+')->name('admin_representative_office_manager');
        Route::post('/manager/ajax-save-manager', 'RepresentativeOfficeController@ajaxSaveMember')->name('admin_representative_office_save_manager');
    });

    Route::prefix('employee')->group(function () {
        Route::get('/', 'EmployeeController@index')->name('admin_employee_view');
        Route::get('/view/{id}', 'EmployeeController@show')->name('admin_employee_edit');
        Route::get('/delete', 'EmployeeController@destroy')->name('admin_employee_delete');
        Route::post('/', 'EmployeeController@update')->name('admin_employee_update');
        Route::get('/create', 'EmployeeController@create')->name('admin_employee_create');
        Route::post('/create', 'EmployeeController@doCreate')->name('admin_employee_doCreate');
    });

    Route::middleware(['acl:admin'])->resource('/users', 'UserController');
    Route::middleware(['acl:admin'])->resource('/introductions', 'IntroductionController');
    Route::middleware(['acl:admin'])->resource('/categories', 'CategoryController');
    Route::prefix('introductions')->group(function () {
        Route::get('/detail/{id}', 'IntroductionController@detail')->name('admin_introductions_detail');
    });

    Route::prefix('categories')->group(function () {
        Route::get('/{id}/{option}', 'CategoryController@showEdit')->name('admin_categories_sedit_detail');
        Route::get('/deleteall/{id}/{option}', 'CategoryController@deleteOption')->name('admin_categories_deletdit_detail');
        Route::get('/check_category/{id}/{procode_all}/{nameAll}/{optionAll}', 'CategoryController@saveCategory')->name('admin_categories_savedeletdit_detail');
    });

    Route::middleware(['acl:admin'])->prefix('branches')->group(function() {
        // Câu lạc bộ thuộc hội
        Route::prefix('club-of-head')->group(function() {
            Route::get('/', 'ClubOfHeadController@index')->name('club_of_head_index');
            Route::get('/create', 'ClubOfHeadController@create')->name('club_of_head_create');
            Route::post('/', 'ClubOfHeadController@store')->where('id', '\d+')->name('club_of_head_store');
            Route::get('/{id}/edit', 'ClubOfHeadController@edit')->where('id', '\d+')->name('club_of_head_edit');
            Route::post('/{id}', 'ClubOfHeadController@update')->where('id', '\d+')->name('club_of_head_update');
            Route::get('/{id}', 'ClubOfHeadController@show')->where('id', '\d+')->name('club_of_head_show');
            Route::post('/{id}/delete', 'ClubOfHeadController@destroy')->where('id', '\d+')->name('club_of_head_delete');
            // START Quản lý lãnh đạo
            Route::get('/manager/{id}', 'ClubOfHeadController@branchManager')->where('id', '\d+')->name('club_of_head_manager');
            Route::post('/manager/ajax-save-manager', 'ClubOfHeadController@ajaxSaveMember')->name('club_of_head_ajax_save_member');
            // END Quản lý lãnh đạo
            Route::get('/ajax-get-clb', 'ClubOfHeadController@ajaxGetCLB')->name('club_of_head_ajax_get_clb');
            Route::get('/export-excel/{id}', 'ClubOfHeadController@exportExcel')->name('club_of_head_export_excel');
        });

        // Cau lac bo thuoc chi hoi
        Route::prefix('club-of-branch')->group(function() {
            Route::get('/', 'ClubOfBranchController@index')->name('club_of_branch_index');
            Route::get('/create', 'ClubOfBranchController@create')->name('club_of_branch_create');
            Route::post('/', 'ClubOfBranchController@store')->name('club_of_branch_create_store');
            Route::get('/{id}/edit', 'ClubOfBranchController@edit')->where('id', '\d+')->name('club_of_branch_edit');
            Route::post('/{id}', 'ClubOfBranchController@update')->where('id', '\d+')->name('club_of_branch_update');
            Route::get('/{id}', 'ClubOfBranchController@show')->where('id', '\d+')->name('club_of_branch_show');
            Route::post('/{id}/delete', 'ClubOfBranchController@destroy')->where('id', '\d+')->name('club_of_branch_delete');

            // START Quản lý lãnh đạo
            Route::get('/manager/{id}', 'ClubOfBranchController@branchManager')->where('id', '\d+')->name('club_of_branches_manager');
            Route::post('/manager/ajax-save-manager', 'ClubOfBranchController@ajaxSaveMember')->name('club_of_branches_ajax_save_member');
            // END Quản lý lãnh đạo
            // START Member
            Route::get('/member/{id}', 'ClubOfBranchController@indexMember')->where('id', '\d+')->name('club_of_branch_member_index');
            Route::get('/ajax-search-member', 'ClubOfBranchController@ajaxSearchMember')->name('club_of_branch_member_ajax_search');
            Route::get('/member/{id}/create', 'ClubOfBranchController@createMember')->where('id', '\d+')->name('club_of_branch_member_create');
            Route::post('/member/{id}/create', 'ClubOfBranchController@storeMember')->where('id', '\d+')->name('club_of_branch_member_create');
            Route::get('/export-excel/{id}', 'ClubOfBranchController@exportExcel')->where('id', '\d+')->name('club_of_branch_export_excel');
            Route::post('/member/{id}/delete', 'ClubOfBranchController@destroyMember')->where('id', '\d+')->name('club_of_branch_member_delete');
            Route::get('/ajax-get-clb', 'ClubOfBranchController@ajaxGetCLB')->name('club_of_branch_ajax_get_clb');
        });
        // END Member
        
        // branches
        Route::get('/manager/{id}', 'BranchesController@branchManager')->where('id', '\d+')->name('branches_manager');
        Route::get('/manager/ajax-employees', 'BranchesController@ajaxFindMember')->name('branches_ajax_employees');
        Route::get('/export-excel/{id}', 'BranchesController@exportExcel')->where('id', '\d+')->name('branches_export_excel');
        Route::post('/manager/ajax-save-manager', 'BranchesController@ajaxSaveMember')->name('branches_ajax_save_member');
        Route::post('/{id}/delete', 'BranchesController@destroy')->where('id', '\d+')->name('branch_destroy');
        
    });
    // infomation
    Route::prefix('infomation')->middleware('HiAcl')->group(function() {
        Route::get('/', 'InfomationController@index')->name('infomation.index');
        Route::get('/create', 'InfomationController@create')->name('infomation.create');
        Route::post('/create', 'InfomationController@store')->name('infomation.store');
        Route::get('/{id}/show', 'InfomationController@show')->name('infomation.show')->where(['id' => '\d+']);
        Route::get('/{id}/edit', 'InfomationController@edit')->name('infomation.edit')->where(['id' => '\d+']);
        Route::post('/{id}/edit', 'InfomationController@update')->name('infomation.update')->where(['id' => '\d+']);
        Route::post('/{id}/delete', 'InfomationController@destroy')->name('infomation.destroy')->where(['id' => '\d+']);
    });

    // permisstion manager
    Route::prefix('groups')->group(function() {
        Route::get('/', 'GroupsController@index')->name('groups.index');
        Route::get('/create', 'GroupsController@create')->name('groups.create');
        Route::post('/create', 'GroupsController@store')->name('groups.store');
        Route::get('/{id}/edit', 'GroupsController@edit')->name('groups.edit');
        Route::post('/{id}/edit', 'GroupsController@updated')->name('groups.update');
        Route::post('/{id}/delete', 'GroupsController@delete')->name('groups.destroy');
    });

    Route::middleware(['acl:admin'])->resource('branches', 'BranchesController');
});

Route::namespace('Api\v1')->group(function () {
    Route::resource('upload', 'UploadController')->only(['store']);
});


// Frontend area
Route::namespace('Frontend')->group(function () {
    // Co cau to chuc
    Route::get('/banchuyenmon', 'OrganizeStructureController@specialBoard')->name('front_end_special_board');
    Route::get('/banthuongtruc', 'OrganizeStructureController@standingCommittee')->name('front_end_standing_committee');
    Route::get('/bancovan', 'OrganizeStructureController@advisoryBoard')->name('front_end_standing_advisory_board');
    Route::get('/banchaphanh', 'OrganizeStructureController@executiveCommittee')->name('front_end_executive_committee');

    // Tin tuc
    Route::get('/news', 'NewsController@news')->name('front_end_news');
    Route::get('/news/{alias}', 'NewsController@detail')->name('front_end_news_detail');
    Route::get('/tintucvasukienhoi', 'NewsController@news')->name('front_end_news_1');
    Route::get('/tintucvasukienchihoi', 'NewsController@news2')->name('front_end_news_2');
    Route::get('/tintucvasukienclbthuochoi', 'NewsController@news3')->name('front_end_news_3');

    // Hoi vien
    Route::get('/hoivien', 'MemberController@hoivien')->name('front_end_hoivien');
    Route::get('/hoivienAjax', 'MemberController@ajax')->name('front_end_hoivien');

    // Van phong dai dien
    Route::get('/vanphongdaidien', 'BranchesController@hoivien')->name('front_end_vanphongdaidien');
    // Tin tuc
    Route::get('/vechungtoi', 'IntroductionsController@vechungtoi')->name('front_vechungtoi');
    Route::get('/tamnhinvasumenh', 'IntroductionsController@tamnhinvasumenh')->name('front_tamnhinvasumenh');
    Route::get('/quychehoatdong', 'IntroductionsController@quychehoatdong')->name('front_quychehoatdong');
    Route::get('/quydinh', 'IntroductionsController@quydinh')->name('front_quydinh');
    Route::get('/huongdandangky', 'IntroductionsController@huongdandangky')->name('front_huongdandangky');
    // Thông tin Giao thông
    Route::get('giaothong', 'NewsController@trafficList')->name('front_info_traffic');
    Route::get('giaothong/{id}', 'NewsController@trafficDetail')->where('id', '\d+')->name('front_info_traffic_detail');
    // Thông tin Lễ hội
    Route::get('lehoi', 'NewsController@festivalList')->name('front_info_lehoi');
    Route::get('lehoi/{id}', 'NewsController@festivalDetail')->where('id', '\d+')->name('front_info_lehoi_detail');
    // Thông tin Điểm đến
    Route::get('diemden', 'NewsController@destinationList')->name('front_info_diemden');
    Route::get('diemden/{id}', 'NewsController@destinationDetail')->where('id', '\d+')->name('front_info_diemden_detail');
    // Thông tin Trang phục
    Route::get('trangphuc', 'NewsController@costumeList')->name('front_info_trangphuc');
    Route::get('trangphuc/{id}', 'NewsController@costumeDetail')->where('id', '\d+')->name('front_info_trangphuc_detail');
    // Phong tục tập quán
    Route::get('phongtuctapquan', 'NewsController@customandpracticesList')->name
    ('front_info_customandpractices');
    Route::get('phongtuctapquan/{id}', 'NewsController@customandpracticesDetail')->where('id', '\d+')->name('front_info_customandpractices_detail');
});