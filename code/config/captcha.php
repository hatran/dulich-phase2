<?php

return [

    'characters' => '2346789ABCDEFGHJMNPQRTUXYZ',

    'default'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'lines'     => -1,
        'bgImage'   => false,
	'fontColors'=> ['#00000']
    ],

    'flat'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'lines'     => -1,
        'bgImage'   => false,
	'fontColors'=> ['#00000']
    ],

    'mini'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'lines'     => -1,
        'bgImage'   => false,
	'fontColors'=> ['#00000']
    ],

    'inverse'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'lines'     => -1,
        'bgImage'   => false,
	'fontColors'=> ['#00000']
    ]

];
