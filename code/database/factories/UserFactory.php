<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// $factory->define(App\User::class, function (Faker $faker) {
//     static $password;

//     return [
//         'name' => $faker->name,
//         'email' => $faker->unique()->safeEmail,
//         'password' => $password ?: $password = bcrypt('secret'),
//         'remember_token' => str_random(10),
//     ];
// });

$factory->define(App\Models\Question::class, function (Faker $faker) {

    return [
        'subject_knowledge_id' => 15,
        'question_rank' => 3,
        'code' => $faker->postcode,
        'content' => $faker->text,
        'answer1' => $faker->text,
        'answer2' => $faker->text,
        'answer3' => $faker->text,
        'answer4' => $faker->text,
        'answer_correct' => $faker->numberBetween($min = 1, $max = 4),
        'status' => 1,
        'is_deleted' => 0,
        'created_author' => $faker->name
    ];
});
