<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Question::class, 4)->create();
        // $branches = [
        //     'Branch 1',
        //     'Branch 2',
        // ];
        // $degrees = [
        //     'Degree 1',
        //     'Degree 2',
        // ];
        // $majors = [
        //     'Major 1',
        //     'Major 2',
        // ];

        // $languages = [
        //     'Lang 1',
        //     'Lang 2',
        // ];

        // $languageLevels = [
        //     'Level 1',
        //     'Level 2',
        // ];

        // $groupSizes = [
        //     'Size 1',
        //     'Size 2',
        // ];

        // $typeGuides = [
        //     'Size 1',
        //     'Size 2',
        // ];

        // foreach ($branches as $branch) {
        //     \App\Models\EducationBranch::create([
        //         'branchName' => $branch,
        //         'status' => 1,
        //     ]);
        // }

        // foreach ($degrees as $degree) {
        //     \App\Models\EducationDegree::create([
        //         'degree' => $degree,
        //         'status' => 1,
        //     ]);
        // }

        // foreach ($majors as $major) {
        //     \App\Models\Major::create([
        //         'majorName' => $major,
        //         'status' => 1,
        //     ]);
        // }

        // foreach ($languages as $language) {
        //     \App\Models\Language::create([
        //         'languageName' => $language,
        //         'status' => 1,
        //     ]);
        // }

        // foreach ($languageLevels as $languageLevel) {
        //     \App\Models\LanguageLevel::create([
        //         'levelName' => $languageLevel,
        //         'status' => 1,
        //     ]);
        // }

        // foreach ($groupSizes as $groupSize) {
        //     \App\Models\GroupSize::create([
        //         'groupSize' => $groupSize,
        //         'status' => 1,
        //     ]);
        // }

        // foreach ($typeGuides as $typeGuide) {
        //     \App\Models\TypeGuide::create([
        //         'typeName' => $typeGuide,
        //         'status' => 1,
        //     ]);
        // }
    }
}
