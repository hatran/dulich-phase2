<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('employees');
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 50);
            $table->string('profile_image', 50);
            $table->string('birthday', 100);
            $table->tinyInteger('gender')->nullable();
            $table->integer('option_id')->nullable();
            $table->string('email', 50)->nullable();
            $table->bigInteger('phone')->nullable();
            $table->string('address', 50)->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
            $table->string('company', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
