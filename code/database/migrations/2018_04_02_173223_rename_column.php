<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('employees', 'option_id'))
        {
            Schema::table('employees', function (Blueprint $table) {
                $table->renameColumn('option_id', 'option_code');
            });
        }
        if (Schema::hasColumn('employee_position', 'options_id'))
        {
            Schema::table('employee_position', function (Blueprint $table) {
                $table->renameColumn('options_id', 'option_code');
            });
        }
        if (!Schema::hasColumn('options', 'code'))
        {
            Schema::table('options', function (Blueprint $table) {
                $table->string('code', 50)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
