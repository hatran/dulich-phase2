<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsHelpInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('newshelpInformation', function(Blueprint $table){
            $table->increments('id');
            $table->integer('title');
            $table->string('thumnail_image');
            $table->text('short_description');
            $table->string('main_image');
            $table->text('content');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('information_type_id');
            $table->integer('branch_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

    }
}
