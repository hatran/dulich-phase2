<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguageSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languageSkills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('memberId')->unsigned();
            $table->integer('languageId')->unsigned();
            $table->integer('levelId')->unsigned();
            $table->integer('fileId')->unsigned()->nullable();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();

            $table->foreign('memberId')->references('id')->on('members')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('languageId')->references('id')->on('languages')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('levelId')->references('id')->on('languageLevels')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fileId')->references('id')->on('files')
                ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languageSkills');
    }
}
