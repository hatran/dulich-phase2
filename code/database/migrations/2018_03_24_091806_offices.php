<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Offices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('offices');
        Schema::create('offices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name', 255);
            $table->text('addr')->nullable();
            $table->string('phone');
            $table->string('email');
            $table->text('payment_info')->nullable();
            $table->text('note')->nullable();
            $table->text('images')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offices');
    }
}
