<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('memberId')->unsigned();
            $table->integer('typeGuideId')->unsigned();
            $table->integer('groupSizeId')->unsigned();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();

            $table->foreign('memberId')->references('id')->on('members')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('typeGuideId')->references('id')->on('typeGuides')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('groupSizeId')->references('id')->on('groupSizes')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elements');
    }
}
