<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');

            $table->string('fullName')->nullable();
            $table->dateTime('birthday')->nullable();
            $table->tinyInteger('gender')->nullable();

            $table->string('touristGuideCode')->nullable();
            $table->dateTime('expirationDate')->nullable();

            $table->string('cmtCccd')->nullable();
            $table->dateTime('dateIssued')->nullable();
            $table->string('issuedBy')->nullable();

            $table->string('permanentAddress', 1024)->nullable();
            $table->string('address', 1024)->nullable();

            $table->string('firstMobile', 50)->nullable();
            $table->string('secondMobile', 50)->nullable();
            $table->string('firstEmail')->nullable();
            $table->string('secondEmail')->nullable();

            $table->tinyInteger('typeOfTravelGuide')->nullable();
            $table->tinyInteger('typeOfPlace')->nullable();

            $table->integer('experienceYear')->nullable();
            $table->tinyInteger('experienceLevel')->nullable();

            $table->text('otherSkills')->nullable();
            $table->text('otherInformation')->nullable();
            $table->text('touristGuideLevel')->nullable();
            $table->text('achievements')->nullable();

            $table->string('emailToken')->nullable();
            $table->string('phoneToken')->nullable();
            $table->tinyInteger('emailVerified')->default(0);
            $table->tinyInteger('phoneVerified')->default(0);
            $table->tinyInteger('acceptTermsAndPolicies')->default(0);

            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
            $table->tinyInteger('status')->nullable();
        });

        Schema::create('memberAvatars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('memberId')->unsigned();
            $table->integer('fileId')->unsigned();
            $table->tinyInteger('current')->default(0);
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();

            $table->foreign('memberId')->references('id')->on('members')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fileId')->references('id')->on('files')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberAvatars');
        Schema::dropIfExists('members');
    }
}
