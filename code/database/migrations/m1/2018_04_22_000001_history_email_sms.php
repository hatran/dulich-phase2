<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryEmailSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_email_sms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type',255)->nullable();
            $table->string('email_to',255)->nullable();
            $table->string('email_name',255)->nullable();
            $table->string('subject',255)->nullable();
            $table->text('content')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_model');
    }
}
