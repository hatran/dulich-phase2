<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('memberId')->unsigned();
            $table->integer('branchId')->unsigned();
            $table->integer('degreeId')->unsigned();
            $table->integer('fileId')->unsigned()->nullable();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();

            $table->foreign('memberId')->references('id')->on('members')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('branchId')->references('id')->on('educationBranches')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('degreeId')->references('id')->on('educationDegrees')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fileId')->references('id')->on('files')
                ->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educations');
    }
}
