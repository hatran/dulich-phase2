<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workHistory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('memberId')->unsigned();
            $table->integer('elementId')->unsigned();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();

            $table->foreign('memberId')->references('id')->on('members')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('elementId')->references('id')->on('works')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workHistory');
    }
}
