<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmployeePostionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('employee_position', 'deleted_at'))
        {
            Schema::table('employee_position', function (Blueprint $table) {
                $sql = "ALTER TABLE employee_position MODIFY deleted_at TIMESTAMP NULL DEFAULT NULL";
                \Illuminate\Support\Facades\DB::connection()->getPdo()->exec($sql);
                $sql2 = "ALTER TABLE options MODIFY deleted_at TIMESTAMP NULL DEFAULT NULL";
                \Illuminate\Support\Facades\DB::connection()->getPdo()->exec($sql2);
                $sql3 = "ALTER TABLE employees MODIFY address varchar(255)";
                \Illuminate\Support\Facades\DB::connection()->getPdo()->exec($sql3);
            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_position', function (Blueprint $table) {
            //
        });
    }
}
