<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('helpInformation');
        Schema::create('helpInformation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->string('thumnail_image', 200);
            $table->text('short_description')->nullable();
            $table->string('main_image', 200)->nullable();
            $table->text('content')->nullable();
            $table->integer('status')->nullable();
            $table->timestamp('start_time')->nullable();
            $table->timestamp('end_time')->nullable();
            $table->integer('information_type_id')->nullable();
            $table->integer('branch_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpInformation');
    }
}
