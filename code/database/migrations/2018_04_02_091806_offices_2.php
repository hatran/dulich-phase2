<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Offices2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('branches');
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name', 255);
            $table->text('address')->nullable();
            $table->string('phone');
            $table->string('email');
            $table->text('payment_info')->nullable();
            $table->text('note')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('option_id')->nullable();
            $table->text('images')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branches');
    }
}
