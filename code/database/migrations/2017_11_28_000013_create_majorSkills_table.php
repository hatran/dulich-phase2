<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMajorSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('majorSkills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('memberId')->unsigned();
            $table->integer('majorId')->unsigned();
            $table->timestamp('createdAt')->nullable();
            $table->timestamp('updatedAt')->nullable();

            $table->foreign('memberId')->references('id')->on('members')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('majorId')->references('id')->on('majors')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('majorSkills');
    }
}
