ALTER TABLE `history`
ADD COLUMN `permission_id` INT(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `user_id`;

ALTER TABLE `member_evaluation`
ADD COLUMN `rank_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `member_id`;

ALTER TABLE `member_evaluation`
ADD UNIQUE INDEX `IDX_1` USING BTREE (`member_id`, `rank_id`);