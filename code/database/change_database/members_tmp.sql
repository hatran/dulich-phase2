-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 26, 2018 at 04:40 PM
-- Server version: 5.7.19
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vita`
--

-- --------------------------------------------------------

--
-- Table structure for table `members_tmp`
--

CREATE TABLE `members_tmp` (
  `id` int(10) UNSIGNED NOT NULL,
  `member_id` int(11) NOT NULL,
  `fullName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `touristGuideCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `cmtCccd` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateIssued` datetime DEFAULT NULL,
  `issuedBy` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanentAddress` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstMobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondMobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secondEmail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeOfTravelGuide` tinyint(4) DEFAULT NULL,
  `typeOfPlace` tinyint(4) DEFAULT NULL,
  `experienceYear` int(11) DEFAULT NULL,
  `experienceLevel` tinyint(4) DEFAULT NULL,
  `otherSkills` text COLLATE utf8mb4_unicode_ci,
  `otherInformation` text COLLATE utf8mb4_unicode_ci,
  `touristGuideLevel` text COLLATE utf8mb4_unicode_ci,
  `achievements` text COLLATE utf8mb4_unicode_ci,
  `emailToken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  `province_code` tinyint(4) NOT NULL,
  `guideLanguage` tinyint(4) DEFAULT NULL,
  `inboundOutbound` tinyint(4) DEFAULT NULL,
  `province` int(4) DEFAULT NULL,
  `branchId_tmp` int(10) DEFAULT NULL,
  `degreeId_tmp` int(10) DEFAULT NULL,
  `majorId_tmp` int(10) DEFAULT NULL,
  `languageId_tmp` int(10) DEFAULT NULL,
  `levelId_tmp` int(10) DEFAULT NULL,
  `elementId_tmp` int(10) DEFAULT NULL,
  `typeGuideId_tmp` int(10) DEFAULT NULL,
  `groupSizeId_tmp` int(10) DEFAULT NULL,
  `fileld_tmp` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members_tmp`
--
ALTER TABLE `members_tmp`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members_tmp`
--
ALTER TABLE `members_tmp`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
